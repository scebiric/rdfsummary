package fr.inria.oak.RDFSummary.data.queries.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueriesImpl;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfTablesQueriesImplTest {

	@Test
	public void insertDataProperties() {
		// Set up		
		RdfTablesQueries queries = new RdfTablesQueriesImpl();
		
		// Run
		String query = queries.insertDataProperties("test.property_nodes_possible", "test.g_enc_data");
		
		// Assert
		String expected = "INSERT INTO test.property_nodes_possible (SELECT DISTINCT p AS uri FROM test.g_enc_data);";
		assertEquals(expected, query);
	}
	
	@Test
	public void copySchemaPropertyNodes() {
		// Set up		
		RdfTablesQueries queries = new RdfTablesQueriesImpl();
		
		// Run
		String query = queries.copySchemaPropertyNodes("test.property_nodes", "test.property_nodes_schema");
		
		// Assert
		String expected = "INSERT INTO test.property_nodes (SELECT * FROM test.property_nodes_schema);";
		assertEquals(expected, query);
	}
	
	@Test
	public void getPropertyNodesFromDataTable() {
		// Set up		
		RdfTablesQueries queries = new RdfTablesQueriesImpl();
		
		// Run
		String query = queries.getPropertyNodesFromDataTable("test.property_nodes", "test.property_nodes_possible", "test.g_enc_data", Constant.SUBJECT);
		
		// Assert
		String expected = "INSERT INTO test.property_nodes (SELECT pnodes.uri AS uri FROM test.property_nodes_possible pnodes, test.g_enc_data data WHERE pnodes.uri=data.s);";
		assertEquals(expected, query);
	}
	
	@Test
	public void getPropertyNodesFromTypesTable() {
		// Set up		
		RdfTablesQueries queries = new RdfTablesQueriesImpl();
		
		// Run
		String query = queries.getPropertyNodesFromTypesTable("test.property_nodes", "test.property_nodes_possible", "test.g_enc_typ", Constant.SUBJECT);
		
		// Assert
		String expected = "INSERT INTO test.property_nodes (SELECT pnodes.uri AS uri FROM test.property_nodes_possible pnodes, test.g_enc_typ typ WHERE pnodes.uri=typ.s);";
		assertEquals(expected, query);
	}
	
	@Test
	public void getDistinctClassAndPropertyNodeCount() {
		// Set up		
		RdfTablesQueries queries = new RdfTablesQueriesImpl();
		
		// Run
		String query = queries.getDistinctClassAndPropertyNodeCount("test.classes", "test.properties");
		
		// Assert
		String expected = "SELECT COUNT(*) FROM (SELECT uri FROM test.classes UNION SELECT uri FROM test.properties) AS count;";
		assertEquals(expected, query);
	}

}
