package fr.inria.oak.RDFSummary.data.queries.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.queries.DmlQueriesImpl;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DmlQueriesImplTest {

	@Test
	public void existsValueInColumn() {
		// Set up		
		DmlQueries queries = new DmlQueriesImpl();
		
		// Run
		String query = queries.existsValueInColumn("table", "column", Integer.toString(2));
		
		// Assert
		String expected = "SELECT EXISTS (SELECT 1 FROM table WHERE table.column=2);";
		assertEquals(expected, query);
	}

}
