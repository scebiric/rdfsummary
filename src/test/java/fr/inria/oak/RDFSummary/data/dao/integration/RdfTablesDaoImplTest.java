package fr.inria.oak.RDFSummary.data.dao.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfTablesDaoImplTest {

	private static final CliqueTrovePostgresConfigurator Configurator = new CliqueTrovePostgresConfigurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String WEAK_PURE_CLIQUE_SUMMARY = SummaryType.WEAK;
	private static final String WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR = "/weak/";

	@Before
	public void setUp() throws BeansException, SQLException, ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();		
	}

	@Test
	public void extractAllPropertyNodes() throws BeansException, SQLException, ConfigurationException, ParametersException, DictionaryException, InexistentValueException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, InexistentKeyException, IOException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		// Set up
		final String dataFile = WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR.concat("property_nodes.nt");
		final String schemaFile = WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR.concat("property_nodes_schema.nt");
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_PURE_CLIQUE_SUMMARY, dataFile, schemaFile); 
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);

		// Run
		summarizer.summarize(params);

		// Assert
		dictionary = TestUtils.loadMemoryDictionary(context);
		DmlDao dmlDao = context.getBean(DmlDao.class);
		String propertyNodesTable = NameUtils.getPropertyNodesTableName(params.getSummarizationParams().getSchemaName());
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p1>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p2>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p3>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p4>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p6>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p7>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey("<p9>"))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey(Rdf.FULL_TYPE))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey(Rdfs.FULL_SUBCLASS))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey(Rdfs.FULL_SUBPROPERTY))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey(Rdfs.FULL_DOMAIN))));
		assertTrue(dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(dictionary.getKey(Rdfs.FULL_RANGE))));

		assertEquals(12, dmlDao.getRowCount(propertyNodesTable));
	}

	@Test
	public void weakSummary_classAndPropertyNodes() throws BerkeleyDbException, SQLException, ConfigurationException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException {
		// Set up
		final String dataFile = WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR.concat("summary_cls_props_nodes.nt");		
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_PURE_CLIQUE_SUMMARY, dataFile, null);
		params.getExportParams().setExportToRdfFile(false);
		params.getExportParams().setExportStatsToFile(false);
		params.getExportParams().setExportRepresentationTables(false);
		params.getExportParams().setGenerateDot(false);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run 
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		RdfSummary summary = summaryResult.getSummary();
		assertTrue(summary.isClassNode(dictionary.getKey("<c1>")));
		assertTrue(summary.isClassNode(dictionary.getKey("<c2>")));
		assertTrue(summary.isPropertyNode(dictionary.getKey("<p1>")));
		assertEquals(2, summary.getClassNodeCount());
		assertEquals(1, summary.getPropertyNodeCount()); 

		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s3>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o3>")));
	}

	@Test
	public void typedWeakSummary_classAndPropertyNodes() throws BerkeleyDbException, SQLException, ConfigurationException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException {
		// Set up
		final String dataFile = "/typweak/".concat("summary_cls_props_nodes.nt");		
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(SummaryType.TYPED_WEAK, dataFile, null);
		params.getExportParams().setExportToRdfFile(false);
		params.getExportParams().setExportStatsToFile(false);
		params.getExportParams().setExportRepresentationTables(false);
		params.getExportParams().setGenerateDot(false);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run 
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		RdfSummary summary = summaryResult.getSummary();
		assertTrue(summary.isClassNode(dictionary.getKey("<c1>")));
		assertTrue(summary.isClassNode(dictionary.getKey("<c2>")));
		assertTrue(summary.isPropertyNode(dictionary.getKey("<p1>")));
		assertEquals(2, summary.getClassNodeCount());
		assertEquals(1, summary.getPropertyNodeCount());

		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s3>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o3>")));
	}

	@Test
	public void strongSummary_classAndPropertyNodes() throws BerkeleyDbException, SQLException, ConfigurationException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException {
		// Set up
		final String dataFile = "/strong/".concat("summary_cls_props_nodes.nt");
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(SummaryType.STRONG, dataFile, null);
		params.getExportParams().setExportToRdfFile(false);
		params.getExportParams().setExportStatsToFile(false);
		params.getExportParams().setExportRepresentationTables(false);
		params.getExportParams().setGenerateDot(false);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run 
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		RdfSummary summary = summaryResult.getSummary();
		assertTrue(summary.isClassNode(dictionary.getKey("<c1>")));
		assertTrue(summary.isClassNode(dictionary.getKey("<c2>")));
		assertTrue(summary.isPropertyNode(dictionary.getKey("<p1>")));
		assertEquals(2, summary.getClassNodeCount());
		assertEquals(1, summary.getPropertyNodeCount());

		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s3>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o3>")));
	}

	@Test
	public void typedStrongSummary_classAndPropertyNodes() throws BerkeleyDbException, SQLException, ConfigurationException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException {
		// Set up
		final String dataFile = "/typstrong/".concat("summary_cls_props_nodes.nt");
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(SummaryType.TYPED_STRONG, dataFile, null);
		params.getExportParams().setExportToRdfFile(false);
		params.getExportParams().setExportStatsToFile(false);
		params.getExportParams().setExportRepresentationTables(false);
		params.getExportParams().setGenerateDot(false);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run 
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		RdfSummary summary = summaryResult.getSummary();
		assertTrue(summary.isClassNode(dictionary.getKey("<c1>")));
		assertTrue(summary.isClassNode(dictionary.getKey("<c2>")));
		assertTrue(summary.isPropertyNode(dictionary.getKey("<p1>")));
		assertEquals(2, summary.getClassNodeCount());
		assertEquals(1, summary.getPropertyNodeCount());

		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<s3>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o1>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o2>")));
		assertFalse(summary.isClassOrPropertyNode(dictionary.getKey("<o3>")));
	}
}
