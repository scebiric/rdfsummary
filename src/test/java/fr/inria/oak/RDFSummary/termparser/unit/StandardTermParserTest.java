package fr.inria.oak.RDFSummary.termparser.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.termparser.StandardTermParser;

public class StandardTermParserTest {

	@Test
	public void isURI_containsAccentedCharacters_false() {
		StandardTermParser parser = new StandardTermParser();
		String value = "<http://fr.dbpedia.org/resource/Loiret_(département)>";	
				
		boolean isIRI = parser.isIRI(value);
		
		assertFalse(isIRI);
	}
	
	@Test
	public void isLiteral_containsAccentedCharacters_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "\"Fozières\"";	
				
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	@Test
	public void isLiteral_notEnclosedInQuotes_false() {
		StandardTermParser parser = new StandardTermParser();
		String value1 = "blabla";
		String value2 = "\"blabla";
		String value3 = "blabla\"";
		
		boolean isLiteral1 = parser.isLiteral(value1);
		boolean isLiteral2 = parser.isLiteral(value2);
		boolean isLiteral3 = parser.isLiteral(value3);
		
		assertFalse(isLiteral1);
		assertFalse(isLiteral2);
		assertFalse(isLiteral3);
	}
	
	@Test
	public void isLiteral_planLiteral_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "\"blabla\"";
		
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	@Test
	public void isLiteral_planLiteralWithWhitespacesNumbersAndSpecialChars_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "\" bla  bla & ! % 456    \"";
		
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	@Test
	public void isLiteral_languageTaggedLiteral_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "\"bla@en\"";
		
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	
	@Test
	public void isLiteral_typedLiteral1_true() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^<http://www.w3.org/2001/XMLSchema#string>";
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	@Test
	public void isLiteral_typedLiteral1_false() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^http://www.w3.org/2001/XMLSchema#string";
		boolean isLiteral = parser.isLiteral(value);
		
		assertFalse(isLiteral);
	}
	
	@Test
	public void isLiteral_typedLiteral2_true() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^xsd:string";
		boolean isLiteral = parser.isLiteral(value);
		
		assertTrue(isLiteral);
	}
	
	@Test
	public void isLiteral_typedLiteralInvalid1_false() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^";
		boolean isLiteral = parser.isLiteral(value);
		
		assertFalse(isLiteral);
	}
	
	@Test
	public void isLiteral_typedLiteralInvalid2_false() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^<>";
		boolean isLiteral = parser.isLiteral(value);
		
		assertFalse(isLiteral);
	}
	
	@Test
	public void isLiteral_typedLiteralInvalid3_false() {
		StandardTermParser parser = new StandardTermParser();
		
		String value = "\"Pete Baron\"^^xsd:";
		boolean isLiteral = parser.isLiteral(value);
		
		assertFalse(isLiteral);
	}

	@Test
	public void isIRI_notEnclosedInAngleBrackets_false() {
		StandardTermParser parser = new StandardTermParser();
		String value1 = "blabla";
		String value2 = "<blabla";
		String value3 = "blabla>";
		
		boolean isIRI1 = parser.isIRI(value1);
		boolean isIRI2 = parser.isIRI(value2);
		boolean isIRI3 = parser.isIRI(value3);
		
		assertFalse(isIRI1);
		assertFalse(isIRI2);
		assertFalse(isIRI3);
	}
	
	@Test
	public void isIRI_enclosedInAngleBrackets_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "<blabla>";
		
		boolean isIRI = parser.isIRI(value);
		
		assertTrue(isIRI);
	}
	
	@Test
	public void isBlank_valid_true() {
		StandardTermParser parser = new StandardTermParser();
		String value = "_:bla";
		
		boolean isBlank = parser.isBlank(value);
		
		assertTrue(isBlank);
	}
	
	@Test
	public void isBlank_prefixOnly_false() {
		StandardTermParser parser = new StandardTermParser();
		String value = "_:";
		
		boolean isBlank = parser.isBlank(value);
		
		assertFalse(isBlank);
	}
}
