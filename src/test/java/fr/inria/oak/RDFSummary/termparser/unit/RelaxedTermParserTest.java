package fr.inria.oak.RDFSummary.termparser.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.termparser.RelaxedTermParser;

public class RelaxedTermParserTest {

	@Test
	public void isURI_containsAccentedCharacters_true() {
		RelaxedTermParser parser = new RelaxedTermParser();
		String value = "<http://fr.dbpedia.org/resource/Loiret_(département)>";	
				
		boolean isIRI = parser.isIRI(value);
		
		assertTrue(isIRI);
	}

}
