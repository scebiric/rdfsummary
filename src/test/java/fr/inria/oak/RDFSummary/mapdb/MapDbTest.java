package fr.inria.oak.RDFSummary.mapdb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import com.google.common.collect.Sets;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Ignore
public class MapDbTest {
	
	final String filename = "/Users/sejla/Desktop/test_mapdb.db";
	
	@Test
	public void DBMaker_make() {
		DB db = getDb();
		
		final String mapName = "map1";
		Map<Integer, String> map = db.get(mapName);
		if (map == null)
			map = db.createHashMap(mapName).counterEnable().make();
		
		assertEquals(0, map.size());
		
		map.put(1, "a");
		map.put(2, "b");
		assertEquals(2, map.size());
		
		db.commit();
		db.close();
		
		File file = new File(filename);
		db = getDb(file);
		map = db.get(mapName);
		assertNotNull(map);
		assertEquals(2, map.size());
		
		db.delete(mapName);
		map = db.get(mapName);
		assertNull(map);
	}
	
	@Test
	public void DBMaker_makeOrGet() {
		DB db = getDb();
		
		final String mapName = "map1";
		Map<Integer, String> map = db.createHashMap(mapName).counterEnable().makeOrGet();
		assertEquals(0, map.size());
		
		map.put(1, "a");
		map.put(2, "b");
		assertEquals(2, map.size());
		
		db.commit();
		db.close();
		
		final File file = new File(filename);
		db = getDb(file);
		map = db.createHashMap(mapName).counterEnable().makeOrGet();
		assertNotNull(map);
		assertEquals(2, map.size());
		
		db.delete(mapName);
		map = db.get(mapName);
		assertNull(map);
	}

	private DB getDb() {
		final File file = new File(filename);	
		return getDb(file);
	}

	private DB getDb(File file) {
		return DBMaker.newFileDB(file).transactionDisable().closeOnJvmShutdown().mmapFileEnable().asyncWriteEnable().make();
	}

	@Test
	public void navigableSet_tuple2() {
		final DB db = getDb();
		final String DATA_PROPERTIES_BY_SUBJECT = "dataPropertiesBySubject";
		
		NavigableSet<Tuple2<Integer, Integer>> dataPropertiesBySubject = db.createTreeSet(DATA_PROPERTIES_BY_SUBJECT).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(1, 2));
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(1, 3));
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(1, 3));
		
		assertEquals(2, dataPropertiesBySubject.size());
		
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(4, 5));
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(4, 6));
		assertEquals(4, dataPropertiesBySubject.size());
		
		Iterator<Integer> propsIterator = Fun.filter(dataPropertiesBySubject, 1).iterator();
		Set<Integer> props = Sets.newHashSet();
		while (propsIterator.hasNext()) {
			props.add(propsIterator.next());
		}
		assertEquals(2, props.size());
		assertTrue(props.contains(2));
		assertTrue(props.contains(3));
		
		db.delete(DATA_PROPERTIES_BY_SUBJECT);
		dataPropertiesBySubject = db.get(DATA_PROPERTIES_BY_SUBJECT);
		assertNull(dataPropertiesBySubject);
	}
	
	@Test
	public void navigableSet_order() {
		final DB db = getDb();
		final String SET = "set";
		
		NavigableSet<Tuple2<Integer, Integer>> set = db.createTreeSet(SET).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		set.add(new Tuple2<Integer, Integer>(2, 5));
		set.add(new Tuple2<Integer, Integer>(2, 1));
		set.add(new Tuple2<Integer, Integer>(2, 3));
		
		int first;
		int second;
		int third;
		Iterator<Integer> it = Fun.filter(set, 2).iterator();
		first = it.next();
		second = it.next();
		third = it.next();
		assertEquals(1, first);
		assertEquals(3, second);
		assertEquals(5, third);
		
		db.delete(SET);
		set = db.get(SET);
		assertNull(set);
	}
}
