package fr.inria.oak.RDFSummary.urigenerator.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.urigenerator.BracketsUriGeneratorImpl;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class UriGeneratorImplTest {
	
	@Test
	public void generateDataNodeUri_enclosedInAngleBrackets() {
		// Set up
		String schemaName = "schema";
		String summaryType = "summary_type";
		int dataNodeId = 124;
		UriGenerator uriGenerator = new BracketsUriGeneratorImpl(schemaName, summaryType);
		
		// Run
		String uri = uriGenerator.generateDataNodeUri(dataNodeId);
		
		// Assert
		assertTrue(uri.startsWith(Character.toString(Chars.LEFT_ANGLE_BRACKET)));
		assertTrue(uri.endsWith(Character.toString(Chars.RIGHT_ANGLE_BRACKET)));
	}

	@Test
	public void generateDataNodeUri() {
		// Set up
		String schemaName = "schema";
		String summaryType = "summary_type";
		int dataNodeId = 124;
		UriGenerator uriGenerator = new BracketsUriGeneratorImpl(schemaName, summaryType);
		
		// Run
		String uri = uriGenerator.generateDataNodeUri(dataNodeId);
		
		// Assert
		assertEquals("<http://schema/summary_type/data/d124>", uri);
	}
}
