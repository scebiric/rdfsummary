package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class StrongPureCliqueEquivalenceSummaryTest {

	private static final CliqueTrovePostgresConfigurator Configurator = new CliqueTrovePostgresConfigurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String STRONG_SUMMARY = SummaryType.STRONG;
	private static final String STRONG_DATASETS_DIR = "/strong/";
	
	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
	}
	
	@Test
	public void summarizeExample() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("example.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary(); 
		assertEquals(12, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount()); 
		
		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<Book>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<Journal>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<Spec>")));

		// Data nodes count
		assertEquals(9, summary.getSummaryDataNodes().size());

		// Edges
		assertEquals(9, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}
	
	@Test
	public void summarizeCommonSources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("cs.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		
		// Common source patterns
		// Pattern 1
		int dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		assertEquals(3, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s1>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s2>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s3>")));
		
		// Typing of the common source
		assertEquals(2, summary.getClassesForSummaryNode(dataNode).size());
		assertTrue(summary.getClassesForSummaryNode(dataNode).contains(dictionary.getKey("<c1>")));
		assertTrue(summary.getClassesForSummaryNode(dataNode).contains(dictionary.getKey("<c2>")));
		
		// Properties of the common source
		DataTriple dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p1>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p2>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p4>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p5>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Pattern 2
		dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s4>"));
		assertEquals(2, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s4>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s5>")));
		
		// Typing of the common source
		assertNull(summary.getClassesForSummaryNode(dataNode));
		
		// Properties of the common source
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p6>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p7>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p8>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Pattern 3
		dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s6>"));
		assertEquals(1, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<s6>")));
		
		// Typing of the common source
		assertNull(summary.getClassesForSummaryNode(dataNode));
		
		// Properties of the common source
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p9>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p10>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Data nodes count
		assertEquals(13, summary.getSummaryDataNodes().size());
		// Edges count		 
		assertEquals(10, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}
	
	@Test
	public void summarizeCommonTargets() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("ct.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		
		// Common target patterns
		// Pattern 1
		int dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		assertEquals(3, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o1>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o2>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o3>")));
		
		// Typing of the common target
		assertEquals(2, summary.getClassesForSummaryNode(dataNode).size());
		assertTrue(summary.getClassesForSummaryNode(dataNode).contains(dictionary.getKey("<c1>")));
		assertTrue(summary.getClassesForSummaryNode(dataNode).contains(dictionary.getKey("<c2>")));
		
		// Properties of the common target
		DataTriple dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p1>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p2>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p4>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p5>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Pattern 2
		dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		assertEquals(2, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o4>")));
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o5>")));
		
		// Typing of the common target
		assertNull(summary.getClassesForSummaryNode(dataNode));
		
		// Properties of the common target
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p6>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p7>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p8>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Pattern 3
		dataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o6>"));
		assertEquals(1, summary.getRepresentedInputDataNodes(dataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(dataNode).contains(dictionary.getKey("<o6>")));
		
		// Typing of the common target
		assertNull(summary.getClassesForSummaryNode(dataNode));
		
		// Properties of the common target
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p9>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForDataProperty(dictionary.getKey("<p10>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Data nodes count
		assertEquals(13, summary.getSummaryDataNodes().size());
		// Edges count		 
		assertEquals(10, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}
	
	/**
	 * Resources from G have the same source AND target clique as their representative data node in the summary.
	 */
	@Test
	public void summarizeExample2() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("example2.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		int o4Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o1Rep, o3Rep);
		assertEquals(o2Rep, o4Rep);
		assertNotEquals(o1Rep, o2Rep);
		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		
		assertEquals(0, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());		
		assertEquals(5, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
	}
	
	@Test
	public void summarizeExample3() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("example3.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o1Rep, o2Rep);
		assertEquals(o1Rep, o3Rep);
		
		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		assertEquals(0, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());		
		assertEquals(4, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
	}
	

	@Test
	public void summarizeTypedOnlyResources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("typedonly.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));

		assertEquals(4, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(1, summary.getSummaryDataNodes().size());
		// Edges 
		assertEquals(0, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		assertEquals(1, summary.getClassesForSummaryNodeMap().keys().length);
	}
	
	@Test
	public void classesAndPropertiesRepresentedByThemselves() throws ConfigurationException, BeansException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("classes_and_props.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());

		// Property nodes
		DmlDao dmlDao = context.getBean(DmlDao.class);
		assertTrue(TestUtils.containsPropertyNode(dmlDao, summaryResult.getPropertyNodesTable(), dictionary.getKey("<p1>")));
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getPropertyNodeCount());
		
		// Data nodes count
		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertTrue(s1Rep != o1Rep);
		assertTrue(s1Rep != o2Rep);
		assertTrue(s1Rep != o3Rep);
		assertTrue(o1Rep != o2Rep);
		assertTrue(o1Rep != o3Rep);
		assertTrue(o2Rep != o3Rep);
		assertEquals(4, summary.getSummaryDataNodes().size());

		// Edges 
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Total nodes
		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}
	
	@Test
	public void testRepresentingDataNodes() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(STRONG_SUMMARY, STRONG_DATASETS_DIR.concat("repr.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		StrongPureCliqueEquivalenceSummary summary = (StrongPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);
		final String qualifiedEncodedSummaryTable = NameUtils.getQualifiedEncodedSummaryTableName(summaryResult.getStorageLayout().getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		String encReprTable = NameUtils.getRepresentationTableName(qualifiedEncodedSummaryTable);

		// The same node represents s1 and s2, and only these nodes
		int s1Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		assertEquals(s1Repr, s2Repr);
		TIntSet set = summary.getRepresentedInputDataNodes(s1Repr);
		assertEquals(2, set.size());
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s1>"), s1Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s2>"), s2Repr));
	
		// Representatives of o1 to o4 are distinct and represent only one node
		int o1Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		int o4Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		set = new TIntHashSet();
		set.add(o1Repr);
		set.add(o2Repr);
		set.add(o3Repr);
		set.add(o4Repr);
		assertEquals(4, set.size()); // true if nodes are distinct
		// Represent only one node
		set = summary.getRepresentedInputDataNodes(o1Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o2Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o3Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o4Repr);
		assertEquals(1, set.size());
		// Entries exist in the table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o1>"), o1Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o2>"), o2Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o3>"), o3Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o4>"), o4Repr));
	}
}
