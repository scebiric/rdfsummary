package fr.inria.oak.RDFSummary.summary.noderepresenter.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.noderepresenter.TroveNodeRepresenterImpl;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveNodeRepresenterImplTest {

	private WeakPureCliqueEquivalenceSummary weakPureCliqueEquivSummary;
	private WeakTypeEquivalenceSummary weakTypeEquivSummary;
	private StrongEquivalenceSummary strongEquivSummary;
	
	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		weakPureCliqueEquivSummary = mock(WeakPureCliqueEquivalenceSummary.class);
		weakTypeEquivSummary = mock(WeakTypeEquivalenceSummary.class);
		strongEquivSummary = mock(StrongEquivalenceSummary.class);
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakPureCliqueEquivalenceSummary_propertySourceSameAsExistingSubjectRepresentativeNode_returnsSubjectRepresentativeNode() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		int subjectRepresentative = propertySource;		
		when(weakPureCliqueEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakPureCliqueEquivSummary, subject, dataProperty);
		
		// Assert
		assertEquals(subjectRepresentative, source);
	}

	@Test
	public void getRepresentativeSourceDataNode_weakPureCliqueEquivalenceSummary_propertySourceDifferentThanExistingSubjectRepresentativeNode_nodesUnifiedAndSubjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		int subjectRepresentative = 4;		
		when(weakPureCliqueEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		when(weakPureCliqueEquivSummary.unifyDataNodes(propertySource, subjectRepresentative)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakPureCliqueEquivSummary, subject, dataProperty);
		
		// Assert 
		verify(weakPureCliqueEquivSummary, times(1)).unifyDataNodes(propertySource, subjectRepresentative);
		assertEquals(subjectRepresentative, source);		
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakPureCliqueEquivalenceSummary_propertySourceExistsSubjectNotYetRepresented_representsSubjectByPropertySourceAndReturnsPropertySource() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		when(weakPureCliqueEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakPureCliqueEquivSummary, subject, dataProperty);
		
		// Assert
		verify(weakPureCliqueEquivSummary, times(1)).representInputDataNode(subject, propertySource);
		assertEquals(propertySource, source); 
	}

	@Test
	public void getRepresentativeSourceDataNode_weakPureCliqueEquivalenceSummary_subjectRepresentativeNodeExistsPropertyDoesNotYetHaveSourceInSummary_returnsSubjectRepresentative() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int subjectRepresentative = 4;
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakPureCliqueEquivSummary, subject, dataProperty);
		
		// Assert
		assertEquals(subjectRepresentative, source);
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakPureCliqueEquivalenceSummary_subjectNotYetRepresentedPropertyDoesNotYetHaveSourceInSummary_createsAndReturnsNewDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int sNode = 3;
		when(weakPureCliqueEquivSummary.createDataNode(subject)).thenReturn(sNode);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakPureCliqueEquivSummary, subject, dataProperty);
		
		// Assert
		verify(weakPureCliqueEquivSummary).createDataNode(subject);
		assertEquals(sNode, source);
	}
	
	//////////
	
	@Test
	public void getRepresentativeTargetDataNode_weakPureCliqueEquivalenceSummary_propertyTargetSameAsExistingObjectRepresentativeNode_returnsObjectRepresentativeNode() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		int objectRepresentative = propertyTarget;		
		when(weakPureCliqueEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakPureCliqueEquivSummary, object, dataProperty);
		
		// Assert
		assertEquals(objectRepresentative, target);
	}

	@Test
	public void getRepresentativeTargetDataNode_weakPureCliqueEquivalenceSummary_propertyTargetDifferentThanExistingObjectRepresentativeNode_nodesUnifiedAndobjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		int objectRepresentative = 4;		
		when(weakPureCliqueEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		when(weakPureCliqueEquivSummary.unifyDataNodes(propertyTarget, objectRepresentative)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakPureCliqueEquivSummary, object, dataProperty);
		
		// Assert 
		verify(weakPureCliqueEquivSummary, times(1)).unifyDataNodes(propertyTarget, objectRepresentative);
		assertEquals(objectRepresentative, target);		
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakPureCliqueEquivalenceSummary_propertyTargetExistsobjectNotYetRepresented_representsobjectByPropertyTargetAndReturnsPropertyTarget() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		when(weakPureCliqueEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakPureCliqueEquivSummary, object, dataProperty);
		
		// Assert
		verify(weakPureCliqueEquivSummary, times(1)).representInputDataNode(object, propertyTarget);
		assertEquals(propertyTarget, target); 
	}

	@Test
	public void getRepresentativeTargetDataNode_weakPureCliqueEquivalenceSummary_ObjectRepresentativeNodeExistsPropertyDoesNotYetHaveTargetInSummary_returnsobjectRepresentative() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int objectRepresentative = 4;
		when(weakPureCliqueEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakPureCliqueEquivSummary, object, dataProperty);
		
		// Assert
		assertEquals(objectRepresentative, target);
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakPureCliqueEquivalenceSummary_objectNotYetRepresentedPropertyDoesNotYetHaveTargetInSummary_createsAndReturnsNewDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int sNode = 3;
		when(weakPureCliqueEquivSummary.createDataNode(object)).thenReturn(sNode);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakPureCliqueEquivSummary, object, dataProperty);
		
		// Assert
		verify(weakPureCliqueEquivSummary).createDataNode(object);
		assertEquals(sNode, target);
	}

	//////////////////
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_propertySourceSameAsExistingSubjectRepresentativeNode_returnsSubjectRepresentativeNode() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		int subjectRepresentative = propertySource;		
		when(weakTypeEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert
		assertEquals(subjectRepresentative, source);
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_propertySourceDifferentThanExistingTypedSubjectRepresentativeNode_unificationNotInvokedAndSubjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		int subjectRepresentative = 4;		
		when(weakTypeEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		when(weakTypeEquivSummary.isTypedDataNode(subjectRepresentative)).thenReturn(true);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert 
		verify(weakTypeEquivSummary, times(0)).unifyDataNodes(propertySource, subjectRepresentative);
		verify(weakTypeEquivSummary, times(0)).unifyDataNodes(subjectRepresentative, propertySource);
		assertEquals(subjectRepresentative, source);		
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_propertySourceDifferentThanExistingUntypedSubjectRepresentativeNode_nodesUnifiedAndSubjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		int subjectRepresentative = 4;		
		when(weakTypeEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		when(weakTypeEquivSummary.isTypedDataNode(subjectRepresentative)).thenReturn(false);
		when(weakTypeEquivSummary.unifyDataNodes(propertySource, subjectRepresentative)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert 
		verify(weakTypeEquivSummary, times(1)).unifyDataNodes(propertySource, subjectRepresentative);
		assertEquals(subjectRepresentative, source);		
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_propertySourceExistsSubjectNotYetRepresented_representsSubjectByPropertySourceAndReturnsPropertySource() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int propertySource = 3;
		when(weakTypeEquivSummary.getSourceDataNodeForDataProperty(dataProperty)).thenReturn(propertySource);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert
		verify(weakTypeEquivSummary, times(1)).representInputDataNode(subject, propertySource);
		assertEquals(propertySource, source); 
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_subjectRepresentativeNodeExistsPropertyDoesNotYetHaveSourceInSummary_returnsSubjectRepresentative() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int subjectRepresentative = 4;
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(subject)).thenReturn(subjectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert
		assertEquals(subjectRepresentative, source);
	}
	
	@Test
	public void getRepresentativeSourceDataNode_weakTypeEquivalenceSummary_subjectNotYetRepresentedPropertyDoesNotYetHaveSourceInSummary_createsAndReturnsNewDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int sNode = 3;
		when(weakTypeEquivSummary.createDataNode(subject)).thenReturn(sNode);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int source = nodeRepresenter.getRepresentativeSourceDataNode(weakTypeEquivSummary, subject, dataProperty);
		
		// Assert
		verify(weakTypeEquivSummary).createDataNode(subject);
		assertEquals(sNode, source);
	}
	
	////////////////////
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_propertyTargetSameAsExistingObjectRepresentativeNode_returnsObjectRepresentativeNode() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		int objectRepresentative = propertyTarget;		
		when(weakTypeEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert
		assertEquals(objectRepresentative, target);
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_propertyTargetDifferentThanExistingTypedObjectRepresentativeNode_unificationNotInvokedAndObjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		int objectRepresentative = 4;		
		when(weakTypeEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		when(weakTypeEquivSummary.isTypedDataNode(objectRepresentative)).thenReturn(true);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert 
		verify(weakTypeEquivSummary, times(0)).unifyDataNodes(propertyTarget, objectRepresentative);
		verify(weakTypeEquivSummary, times(0)).unifyDataNodes(objectRepresentative, propertyTarget);
		assertEquals(objectRepresentative, target);		
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_propertyTargetDifferentThanExistingUntypedObjectRepresentativeNode_nodesUnifiedAndObjectRepresentativeReturned() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		int objectRepresentative = 4;		
		when(weakTypeEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		when(weakTypeEquivSummary.isTypedDataNode(objectRepresentative)).thenReturn(false);
		when(weakTypeEquivSummary.unifyDataNodes(propertyTarget, objectRepresentative)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert 
		verify(weakTypeEquivSummary, times(1)).unifyDataNodes(propertyTarget, objectRepresentative);
		assertEquals(objectRepresentative, target);		
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_propertyTargetExistsObjectNotYetRepresented_representsObjectByPropertyTargetAndReturnsPropertyTarget() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int propertyTarget = 3;
		when(weakTypeEquivSummary.getTargetDataNodeForDataProperty(dataProperty)).thenReturn(propertyTarget);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert
		verify(weakTypeEquivSummary, times(1)).representInputDataNode(object, propertyTarget);
		assertEquals(propertyTarget, target); 
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_objectRepresentativeNodeExistsPropertyDoesNotYetHaveTargetInSummary_returnsObjectRepresentative() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int objectRepresentative = 4;
		when(weakTypeEquivSummary.getSummaryDataNodeForInputDataNode(object)).thenReturn(objectRepresentative);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert
		assertEquals(objectRepresentative, target);
	}
	
	@Test
	public void getRepresentativeTargetDataNode_weakTypeEquivalenceSummary_objectNotYetRepresentedPropertyDoesNotYetHaveTargetInSummary_createsAndReturnsNewDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int object = 1;
		int dataProperty = 2;
		int sNode = 3;
		when(weakTypeEquivSummary.createDataNode(object)).thenReturn(sNode);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Call
		int target = nodeRepresenter.getRepresentativeTargetDataNode(weakTypeEquivSummary, object, dataProperty);
		
		// Assert
		verify(weakTypeEquivSummary).createDataNode(object);
		assertEquals(sNode, target);
	}
	
	////////////////
	
	@Test
	public void representInputDataNode_strongEquivalenceSummary_gDataNodeAlreadyRepresented_returnsRepresentativeOfGDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 2;
		when(strongEquivSummary.isClassOrPropertyNode(gDataNode)).thenReturn(false);
		when(strongEquivSummary.getSummaryDataNodeForInputDataNode(gDataNode)).thenReturn(sDataNode);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Run
		int representative = nodeRepresenter.representInputDataNode(strongEquivSummary, gDataNode);
		
		// Assert
		assertEquals(sDataNode, representative);
	}
	
	@Test
	public void representInputDataNode_strongEquivalenceSummary_gDataNodeUnrepresented_existsSDataNodeWithSameSourceAndTargetCliqueAsGDataNode_representsGDataNodeWithSDataNodeAndReturnsSDataNode() throws BerkeleyDbException, SQLException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 2;
		int sourceCliqueId = 3;
		int targetCliqueId = 4;
		TIntSet sourceCliqueDataNodes = new TIntHashSet();
		sourceCliqueDataNodes.add(sDataNode);
		sourceCliqueDataNodes.add(5);
		TIntSet targetCliqueDataNodes = new TIntHashSet();
		targetCliqueDataNodes.add(6);
		targetCliqueDataNodes.add(7);
		targetCliqueDataNodes.add(sDataNode);
		when(strongEquivSummary.isClassOrPropertyNode(gDataNode)).thenReturn(false);
		when(strongEquivSummary.getSourceCliqueIdBySubject(gDataNode)).thenReturn(sourceCliqueId);
		when(strongEquivSummary.getTargetCliqueIdByObject(gDataNode)).thenReturn(targetCliqueId);
		when(strongEquivSummary.getDataNodesForSourceCliqueId(sourceCliqueId)).thenReturn(sourceCliqueDataNodes);
		when(strongEquivSummary.getDataNodesForTargetCliqueId(targetCliqueId)).thenReturn(targetCliqueDataNodes);
		TroveNodeRepresenter nodeRepresenter = new TroveNodeRepresenterImpl();
		
		// Run
		int representative = nodeRepresenter.representInputDataNode(strongEquivSummary, gDataNode);
		
		// Assert
		assertEquals(sDataNode, representative);
		verify(strongEquivSummary, times(1)).representInputDataNode(gDataNode, sDataNode);
	}
	
	
	
}
