package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.AssertUtils;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.set.TIntSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureCliqueEquivalenceSummaryTest {

	private static final CliqueTrovePostgresConfigurator Configurator = new CliqueTrovePostgresConfigurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String WEAK_SUMMARY = SummaryType.WEAK;
	private static final String WEAK_DATASETS_DIR = "/weak/";

	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
	}

	@Test
	public void summarizeTypeTriples() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("types.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		assertEquals(5, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount()); 

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));

		// Data nodes count
		assertEquals(2, summary.getSummaryDataNodes().size());

		// Edges
		assertEquals(1, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeCommonSources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("cs.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(2, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));

		// Common source patterns
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));
		DataTriple dataTriple4 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p4>"));
		DataTriple dataTriple5 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonSubject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5)));

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c1>")));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getObject()).contains(dictionary.getKey("<c2>")));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple2.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple2.getSubject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple2.getObject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple3.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple3.getSubject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple3.getObject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple4.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple4.getSubject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple4.getObject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple5.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple5.getSubject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple5.getObject()));

		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(6, summary.getSummaryDataNodes().size());
		// Edges		 
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(2, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeCommonTargets() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("ct.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(2, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));

		// Common source patterns 
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));
		DataTriple dataTriple4 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p4>"));
		DataTriple dataTriple5 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonObject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5)));

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c2>")));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getObject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple2.getSubject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple2.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple2.getObject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple3.getSubject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple3.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple3.getObject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple4.getSubject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple4.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple4.getObject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple5.getSubject()));

		assertEquals(1, summary.getClassesForSummaryNode(dataTriple5.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple5.getObject()).contains(dictionary.getKey("<c1>")));

		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(6, summary.getSummaryDataNodes().size());
		// Edges	 
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(2, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizePropertyPaths() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("pp.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));

		// Path patterns 
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));		
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonSubject((Lists.newArrayList(dataTriple2, dataTriple3)));
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c1>")));

		assertNull(summary.getClassesForSummaryNode(dataTriple1.getObject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getSubject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getObject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getSubject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getObject()));

		assertEquals(5, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(4, summary.getSummaryDataNodes().size());
		// Edges
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(1, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeTypedOnlyResources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("typedonly.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));

		assertEquals(4, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(1, summary.getSummaryDataNodes().size());
		// Edges 
		assertEquals(0, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		assertEquals(1, summary.getClassesForSummaryNodeMap().keys().length);
	}

	@Test
	public void classesAndPropertiesRepresentedByThemselves() throws ConfigurationException, BeansException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("classes_and_props.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());

		// Property nodes
		DmlDao dmlDao = context.getBean(DmlDao.class);
		assertTrue(TestUtils.containsPropertyNode(dmlDao, summaryResult.getPropertyNodesTable(), dictionary.getKey("<p1>")));
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getPropertyNodeCount());

		// Data nodes count
		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertTrue(s1Rep != o1Rep);
		assertTrue(s1Rep != o2Rep);
		assertTrue(s1Rep != o3Rep);
		assertTrue(o1Rep != o2Rep);
		assertTrue(o1Rep != o3Rep);
		assertTrue(o2Rep != o3Rep);
		assertEquals(4, summary.getSummaryDataNodes().size());

		// Edges 
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Total nodes
		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}

	@Test
	public void testRepresentingDataNodes() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("pp.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);
		final String qualifiedEncodedSummaryTable = NameUtils.getQualifiedEncodedSummaryTableName(summaryResult.getStorageLayout().getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		String encReprTable = NameUtils.getRepresentationTableName(qualifiedEncodedSummaryTable);
		
		// p1Source in-memory
		int p1Source = summary.getSourceDataNodeForDataProperty(dictionary.getKey("<p1>"));
		TIntSet p1SourceReprSet = summary.getRepresentedInputDataNodes(p1Source);
		assertTrue(p1SourceReprSet.contains(dictionary.getKey("<s1>")));
		assertTrue(p1SourceReprSet.contains(dictionary.getKey("<s2>")));
		assertEquals(2, p1SourceReprSet.size());
		// p1Source in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s1>"), p1Source));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s2>"), p1Source));

		// p1Target in-memory
		int p1Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p1>"));
		TIntSet p1TargetReprSet = summary.getRepresentedInputDataNodes(p1Target);
		assertTrue(p1TargetReprSet.contains(dictionary.getKey("<o1>")));
		assertTrue(p1TargetReprSet.contains(dictionary.getKey("<o3>")));
		assertEquals(2, p1TargetReprSet.size());
		// p1Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o1>"), p1Target));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o3>"), p1Target));
		
		// p2Source = p1Target
		int p2Source = summary.getSourceDataNodeForDataProperty(dictionary.getKey("<p2>"));
		assertEquals(p1Target, p2Source);

		// p3Source = p1Target
		int p3Source = summary.getSourceDataNodeForDataProperty(dictionary.getKey("<p3>"));
		assertEquals(p1Target, p3Source);
		
		// p2Target in-memory
		int p2Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p2>"));
		TIntSet p2TargetReprSet = summary.getRepresentedInputDataNodes(p2Target);
		assertTrue(p2TargetReprSet.contains(dictionary.getKey("<o2>")));
		assertEquals(1, p2TargetReprSet.size());
		
		// p2Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o2>"), p2Target));
		
		// p3Target in-memory
		int p3Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p3>"));
		TIntSet p3TargetReprSet = summary.getRepresentedInputDataNodes(p3Target);
		assertTrue(p3TargetReprSet.contains(dictionary.getKey("<o4>")));
		assertEquals(1, p3TargetReprSet.size());
		
		// p3Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o4>"), p3Target));
	}
}
