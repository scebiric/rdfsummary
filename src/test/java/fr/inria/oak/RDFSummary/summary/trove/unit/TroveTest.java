package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTest {

	@Test
	public void mapIntKeyTIntSetValue_nonExistentKey_returnsNullSet() {
		// Set up
		TIntObjectMap<TIntSet> map = new TIntObjectHashMap<TIntSet>();
		
		// Run
		TIntSet set = map.get(1);
		
		// Assert
		assertNull(set);
	}

}
