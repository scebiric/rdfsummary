package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TroveRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakEquivalenceSummaryImplTest {
	
	private UriGenerator uriGenerator;
	private DictionaryDao dictDao;
	private BerkeleyDbHandler bdbHandler;

	private String classesDb = "testClassesDb";
	private String propertiesDb = "testPropertiesDb";

	@Before
	public void setUp() throws ConfigurationException, BerkeleyDbException, IOException, ParametersException {
		uriGenerator = mock(UriGenerator.class);
		dictDao = mock(DictionaryDao.class);
		bdbHandler = mock(BerkeleyDbHandler.class);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unifyUntypedDataNodes_classNodes_exceptionThrown() throws BerkeleyDbException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		Mockito.when(bdbHandler.get(classesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(classesDb, dataNode2, Boolean.class)).thenReturn(true);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary); 
		
		// Call
		summary.unifyDataNodes(dataNode1, dataNode2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unifyUntypedDataNodes_propertyNodes_exceptionThrown() throws BerkeleyDbException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		Mockito.when(bdbHandler.get(propertiesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode2, Boolean.class)).thenReturn(true);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary); 
		
		// Call
		summary.unifyDataNodes(dataNode1, dataNode2);
	}
	
	@Test
	public void unifyUntypedDataNodes_nodeWithHigherDegreeIsReturned() throws ConfigurationException, BerkeleyDbException, SQLException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		int dataNode3 = 30;
		int p1 = 1;
		int p2 = 2;
		int p3 = 3;		
		int r1 = 100;
		int r2 = 200;		
		Mockito.when(bdbHandler.get(classesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(classesDb, dataNode2, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode2, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary); 
		
		summary.setSourceDataNodeForDataProperty(p1, dataNode1);
		summary.setTargetDataNodeForDataProperty(p1, dataNode1);
		summary.setSourceDataNodeForDataProperty(p2, dataNode1);
		summary.setTargetDataNodeForDataProperty(p2, dataNode2);
		summary.setSourceDataNodeForDataProperty(p3, dataNode2);
		summary.setTargetDataNodeForDataProperty(p3, dataNode3);
		summary.representInputDataNode(r1, dataNode1);
		summary.representInputDataNode(r2, dataNode2);

		// Call
		int remainingNode = summary.unifyDataNodes(dataNode1, dataNode2);

		// Assert
		assertEquals(dataNode1, remainingNode);
	}

	@Test
	public void unifyUntypedDataNodes_vanishingNodeNotInSummary() throws ConfigurationException, BerkeleyDbException, SQLException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		int dataNode3 = 30;
		int p1 = 1;
		int p2 = 2;
		int p3 = 3;		
		int r1 = 100;
		int r2 = 200;		
		Mockito.when(bdbHandler.get(classesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(classesDb, dataNode2, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode2, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary);
				
		summary.setSourceDataNodeForDataProperty(p1, dataNode1);
		summary.setTargetDataNodeForDataProperty(p1, dataNode1);
		summary.setSourceDataNodeForDataProperty(p2, dataNode1);
		summary.setTargetDataNodeForDataProperty(p2, dataNode2);
		summary.setSourceDataNodeForDataProperty(p3, dataNode2);
		summary.setTargetDataNodeForDataProperty(p3, dataNode3);
		summary.representInputDataNode(r1, dataNode1);
		summary.representInputDataNode(r2, dataNode2);

		// Call
		summary.unifyDataNodes(dataNode1, dataNode2);

		// Assert
		assertFalse(summary.getSummaryDataNodes().contains(dataNode2));
	}

	@Test
	public void unifyUntypedDataNodes_remaininingNodeRepresentsResourcesOfVanishingNode() throws ConfigurationException, BerkeleyDbException, SQLException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		int dataNode3 = 30;
		int p1 = 1;
		int p2 = 2;
		int p3 = 3;		
		int r1 = 100;
		int r2 = 200;		
		Mockito.when(bdbHandler.get(classesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(classesDb, dataNode2, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode2, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary);
				
		summary.setSourceDataNodeForDataProperty(p1, dataNode1);
		summary.setTargetDataNodeForDataProperty(p1, dataNode1);
		summary.setSourceDataNodeForDataProperty(p2, dataNode1);
		summary.setTargetDataNodeForDataProperty(p2, dataNode2);
		summary.setSourceDataNodeForDataProperty(p3, dataNode2);
		summary.setTargetDataNodeForDataProperty(p3, dataNode3);
		summary.representInputDataNode(r1, dataNode1);
		summary.representInputDataNode(r2, dataNode2);

		// Call
		int remainingNode = summary.unifyDataNodes(dataNode1, dataNode2);

		// Assert
		assertEquals(2, summary.getRepresentedInputDataNodes(remainingNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(remainingNode).contains(r1));
		assertTrue(summary.getRepresentedInputDataNodes(remainingNode).contains(r2));
		assertEquals(remainingNode, summary.getSummaryDataNodeForInputDataNode(r1));
		assertEquals(remainingNode, summary.getSummaryDataNodeForInputDataNode(r2));
	}

	@Test
	public void unifyUntypedDataNodes_remaininingNodeAttachedToVanishingNodeProperties() throws ConfigurationException, BerkeleyDbException, SQLException {
		// Setup
		int dataNode1 = 10;
		int dataNode2 = 20;		
		int dataNode3 = 30;
		int p1 = 1;
		int p2 = 2;
		int p3 = 3;	
		int p4 = 4;
		int p5 = 5;
		int r1 = 100;
		int r2 = 200;		
		Mockito.when(bdbHandler.get(classesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(classesDb, dataNode2, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode1, Boolean.class)).thenReturn(false);
		Mockito.when(bdbHandler.get(propertiesDb, dataNode2, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary summary = new WeakEquivalenceSummaryImpl(rdfSummary);
				
		summary.setSourceDataNodeForDataProperty(p1, dataNode1);
		summary.setTargetDataNodeForDataProperty(p1, dataNode1);
		summary.setSourceDataNodeForDataProperty(p2, dataNode1);
		summary.setTargetDataNodeForDataProperty(p2, dataNode2);		
		summary.setSourceDataNodeForDataProperty(p3, dataNode2);
		summary.setTargetDataNodeForDataProperty(p3, dataNode3);
		summary.setSourceDataNodeForDataProperty(p4, dataNode2);
		summary.setTargetDataNodeForDataProperty(p5, dataNode2);
		summary.representInputDataNode(r1, dataNode1);
		summary.representInputDataNode(r2, dataNode2);

		// Call
		int remainingNode = summary.unifyDataNodes(dataNode1, dataNode2);

		// Assert
		assertEquals(remainingNode, summary.getSourceDataNodeForDataProperty(p1));
		assertEquals(remainingNode, summary.getTargetDataNodeForDataProperty(p1));
		assertEquals(remainingNode, summary.getSourceDataNodeForDataProperty(p2));
		assertEquals(remainingNode, summary.getTargetDataNodeForDataProperty(p2));
		assertEquals(remainingNode, summary.getSourceDataNodeForDataProperty(p3));
		assertEquals(dataNode3, summary.getTargetDataNodeForDataProperty(p3));	
		assertEquals(remainingNode, summary.getSourceDataNodeForDataProperty(p4));
		assertEquals(remainingNode, summary.getTargetDataNodeForDataProperty(p5));
	}
}
