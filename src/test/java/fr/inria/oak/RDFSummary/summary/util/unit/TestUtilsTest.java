package fr.inria.oak.RDFSummary.summary.util.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.SetUtils;
import fr.inria.oak.commons.rdfconjunctivequery.IRI;
import fr.inria.oak.commons.rdfconjunctivequery.Literal;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

public class TestUtilsTest {

	@Test
	public void equals_setsNotNullAndEqual_returnsTrue() {
		// Set up
		TIntSet set1 = new TIntHashSet(Lists.newArrayList(1, 2));
		TIntSet set2 = new TIntHashSet(Lists.newArrayList(2, 1));
		
		// Call
		boolean equal = SetUtils.equals(set1, set2);
		
		// Assert
		assertTrue(equal);
	}
	
	@Test
	public void equals_setsNotNullAndNotEqual_returnsFalse() {
		// Set up
		TIntSet set1 = new TIntHashSet(Lists.newArrayList(1, 2));
		TIntSet set2 = new TIntHashSet(Lists.newArrayList(2, 1, 3));
		
		// Call
		boolean equal = SetUtils.equals(set1, set2);
		
		// Assert
		assertFalse(equal);
	}
	
	@Test
	public void equals_bothSetsNull_returnsTrue() {
		// Set up
		TIntSet set1 = null;
		TIntSet set2 = null;
		
		// Call
		boolean equal = SetUtils.equals(set1, set2);
		
		// Assert
		assertTrue(equal);
	}
	
	@Test
	public void equals_firstSetNullSecondSetNotNull_returnsFalse() {
		// Set up
		TIntSet set1 = null;
		TIntSet set2 = new TIntHashSet(Lists.newArrayList(2, 1, 3));
		
		// Call
		boolean equal = SetUtils.equals(set1, set2);
		
		// Assert
		assertFalse(equal);
	}
	
	@Test
	public void equals_firstSetNotNullSecondSetNull_returnsFalse() {
		// Set up
		TIntSet set1 = new TIntHashSet(Lists.newArrayList(2, 1, 3));
		TIntSet set2 = null;
		
		// Call
		boolean equal = SetUtils.equals(set1, set2);
		
		// Assert
		assertFalse(equal);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getLiteralValue_parameterNotLiteralTerm_exceptionThrown() {
		// Call
		TestUtils.getIntegerValueFromLiteral(new IRI("bla"));
	}
	
	@Test
	public void getLiteralValue_parameterLiteralAndEnclosedInApostrophes_returnsIntValue() {
		// Set up
		int intValue = 16;
		String param = "'" + intValue + "'";
		
		// Call
		int value = TestUtils.getIntegerValueFromLiteral(new Literal(param));
		
		// Assert
		assertEquals(intValue, value);
	}
	
	@Test
	public void getLiteralValue_parameterLiteralIntegerAndNotEnclosedInApostrophes_returnsIntValue() {
		// Set up
		
		// Call
		int value = TestUtils.getIntegerValueFromLiteral(new Literal(Integer.toString(18)));
		
		// Assert
		assertEquals(18, value);
	}
}
