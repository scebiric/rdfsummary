package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TroveRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class WeakTypeEquivalenceSummaryImplTest {
	
	private UriGenerator uriGenerator;
	private DictionaryDao dictDao;
	private BerkeleyDbHandler bdbHandler;

	private String classesDb = "testClassesDb";
	private String propertiesDb = "testPropertiesDb";
	
	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		uriGenerator = mock(UriGenerator.class);
		dictDao = mock(DictionaryDao.class);
		bdbHandler = mock(BerkeleyDbHandler.class);
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreClassOrProperty_dataTripleExists() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		when(bdbHandler.get(classesDb, source, Boolean.class)).thenReturn(true);
		when(bdbHandler.get(classesDb, target, Boolean.class)).thenReturn(true);
		when(bdbHandler.get(propertiesDb, source, Boolean.class)).thenReturn(true);
		when(bdbHandler.get(propertiesDb, target, Boolean.class)).thenReturn(true);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		
		// Run
		summary.createDataTriple(source, property, target);
				
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target));
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreTypedDataNodes_dataTripleExists() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		TIntSet sourceClasses = new TIntHashSet();		
		TIntSet targetClasses = new TIntHashSet();
		sourceClasses.add(4);
		targetClasses.add(5);
		when(bdbHandler.get(classesDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classesDb, target, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(propertiesDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(propertiesDb, target, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		summary.createTypeTriples(source, sourceClasses);
		summary.createTypeTriples(target, targetClasses);
		summary.setHighestTypedDataNode(target);
		
		// Run
		summary.createDataTriple(source, property, target);
				
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target)); 
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreUntypedDataNodes_dataTripleExists() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		TIntSet sourceClasses = new TIntHashSet();		
		TIntSet targetClasses = new TIntHashSet();
		sourceClasses.add(4);
		targetClasses.add(5);
		when(bdbHandler.get(classesDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classesDb, target, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(propertiesDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(propertiesDb, target, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		
		// Run
		summary.createDataTriple(source, property, target);
				
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target));
	}
}
