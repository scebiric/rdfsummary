package fr.inria.oak.RDFSummary.summary.all;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.inria.oak.RDFSummary.data.dao.integration.RdfTablesDaoImplTest;
import fr.inria.oak.RDFSummary.data.queries.unit.DmlQueriesImplTest;
import fr.inria.oak.RDFSummary.data.queries.unit.RdfTablesQueriesImplTest;
import fr.inria.oak.RDFSummary.summary.noderepresenter.unit.TroveNodeRepresenterImplTest;
import fr.inria.oak.RDFSummary.summary.trove.integration.StrongPureCliqueEquivalenceSummaryTest;
import fr.inria.oak.RDFSummary.summary.trove.integration.StrongTypeEquivalenceSummaryTest;
import fr.inria.oak.RDFSummary.summary.trove.integration.TroveCliqueBuilderImplTest;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.unit.TroveTripleCreatorImplTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.PureCliqueEquivalenceSummaryImplTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.RdfSummaryImplTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.TroveTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.WeakEquivalenceSummaryImplTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.WeakPureCliqueEquivalenceSummaryImplTest;
import fr.inria.oak.RDFSummary.summary.trove.unit.WeakTypeEquivalenceSummaryImplTest;
import fr.inria.oak.RDFSummary.summary.util.FileUtilsTest;
import fr.inria.oak.RDFSummary.summary.util.unit.NameUtilsTest;
import fr.inria.oak.RDFSummary.summary.util.unit.RdfUtilsTest;
import fr.inria.oak.RDFSummary.summary.util.unit.TestUtilsTest;
import fr.inria.oak.RDFSummary.termparser.unit.RelaxedTermParserTest;
import fr.inria.oak.RDFSummary.termparser.unit.StandardTermParserTest;
import fr.inria.oak.RDFSummary.urigenerator.unit.UriGeneratorImplTest;

@RunWith(Suite.class)
@SuiteClasses({
	// Unit
	DmlQueriesImplTest.class,
	PureCliqueEquivalenceSummaryImplTest.class,
	RdfSummaryImplTest.class,
	RdfTablesQueriesImplTest.class,
	TroveNodeRepresenterImplTest.class,
	TroveTest.class,
	TroveTripleCreatorImplTest.class,
	UriGeneratorImplTest.class,
	WeakEquivalenceSummaryImplTest.class,
	WeakPureCliqueEquivalenceSummaryImplTest.class,
	WeakTypeEquivalenceSummaryImplTest.class,
	
	FileUtilsTest.class,
	NameUtilsTest.class,
	RdfUtilsTest.class,
	TestUtilsTest.class,
	
	RelaxedTermParserTest.class,
	StandardTermParserTest.class,
	
	// Integration
	RdfTablesDaoImplTest.class,
	StrongPureCliqueEquivalenceSummaryTest.class,
	StrongTypeEquivalenceSummaryTest.class,
	TroveCliqueBuilderImplTest.class,
	fr.inria.oak.RDFSummary.summary.trove.triplecreator.integration.TroveTripleCreatorImplTest.class,
	WeakPureCliqueEquivalenceSummaryImplTest.class,
	WeakTypeEquivalenceSummaryImplTest.class
})
public class AllTests {

}
