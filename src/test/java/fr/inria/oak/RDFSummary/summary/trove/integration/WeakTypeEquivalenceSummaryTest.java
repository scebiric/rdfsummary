package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.AssertUtils;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class WeakTypeEquivalenceSummaryTest {

	private static final CliqueTrovePostgresConfigurator Configurator = new CliqueTrovePostgresConfigurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String TYPED_WEAK_SUMMARY = SummaryType.TYPED_WEAK;
	private static final String TYPED_WEAK_DATASETS_DIR = "/typweak/";

	@Before
	public void setUp() throws BeansException, SQLException, ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
	}

	@Test
	public void summarizeTypeTriples() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("types.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();						
		// Class nodes		
		assertEquals(5, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		List<Integer> classes = TestUtils.getClassesFromTypeComponent(context, summaryResult.getStorageLayout().getEncodedSummaryTypesTable());
		assertTrue(classes.contains(dictionary.getKey("<c1>")));;
		assertTrue(classes.contains(dictionary.getKey("<c2>")));
		assertTrue(classes.contains(dictionary.getKey("<c3>")));
		assertTrue(classes.contains(dictionary.getKey("<c4>")));
		assertTrue(classes.contains(dictionary.getKey("<c5>")));

		// Data nodes count
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount());
		// All class sets are represented		
		int node1 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))));		
		int node2 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c2>"), dictionary.getKey("<c3>"), dictionary.getKey("<c4>"))));
		int node3 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c5>"))));
		assertNotEquals(0, node1);
		assertNotEquals(0, node2);
		assertNotEquals(0, node3);

		// Total nodes
		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());

		// Each data node uniquely represents a class set
		assertEquals(3, Sets.newHashSet(Lists.newArrayList(node1, node2, node3)).size());

		// Edges		
		assertEquals(0, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(6, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeCommonSources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("cs.nt"), null);		
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();
		// Class nodes		
		assertEquals(4, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		List<Integer> classes = TestUtils.getClassesFromTypeComponent(context, summaryResult.getStorageLayout().getEncodedSummaryTypesTable());
		assertTrue(classes.contains(dictionary.getKey("<c1>")));;
		assertTrue(classes.contains(dictionary.getKey("<c2>")));
		assertTrue(classes.contains(dictionary.getKey("<c3>")));
		assertTrue(classes.contains(dictionary.getKey("<c4>")));

		// All class sets are represented
		int node1 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))));
		int node2 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c3>"))));
		int node3 = summary.getSummaryDataNodeForClasses(TestUtils.getClassSet(Lists.newArrayList(dictionary.getKey("<c4>"))));
		assertNotEquals(0, node1);
		assertNotEquals(0, node2);
		assertNotEquals(0, node3);

		// Each data node uniquely represents a class set
		assertEquals(3, Utils.toSet(Lists.newArrayList(node1, node2, node3)).size());

		// Common source patterns
		DataTriple dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonSubject(Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject(), 
				dataTriple2.getSubject(), dataTriple2.getObject(), dataTriple3.getSubject(), dataTriple3.getObject()));

		dataTriple1 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), null, dictionary.getKey("<p9>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), null, dictionary.getKey("<p10>"));
		dataTriple3 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), dictionary.getKey("<p1>"));		
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonSubject(Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getObject(), dataTriple2.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), summary.getClassesForSummaryNode(dataTriple1.getSubject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), summary.getClassesForSummaryNode(dataTriple3.getObject()));


		dataTriple1 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), null, dictionary.getKey("<p3>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), null, dictionary.getKey("<p4>"));
		dataTriple3 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), null, dictionary.getKey("<p6>"));		
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		DataTriple dataTriple4 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), null, dictionary.getKey("<p7>"));
		DataTriple dataTriple5 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), null, dictionary.getKey("<p8>"));
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonSubject(Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getObject(), dataTriple2.getObject(),
				dataTriple3.getObject(), dataTriple4.getObject(), dataTriple5.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), summary.getClassesForSummaryNode(dataTriple1.getSubject()));

		assertEquals(18, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());

		// Data nodes count
		assertEquals(14, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount());
		// Edges		

		assertEquals(11, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeCommonTargets() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("ct.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();

		// Class nodes
		assertEquals(4, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		List<Integer> classes = TestUtils.getClassesFromTypeComponent(context, summaryResult.getStorageLayout().getEncodedSummaryTypesTable());
		assertTrue(classes.contains(dictionary.getKey("<c1>")));;
		assertTrue(classes.contains(dictionary.getKey("<c2>")));
		assertTrue(classes.contains(dictionary.getKey("<c3>")));
		assertTrue(classes.contains(dictionary.getKey("<c4>")));

		// Data nodes count
		assertEquals(14, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount());
		// All class sets are represented
		int node1 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))));
		int node2 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))));
		int node3 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))));
		assertNotEquals(0, node1);
		assertNotEquals(0, node2);
		assertNotEquals(0, node3);

		// Each data node uniquely represents a class set
		assertEquals(3, Utils.toSet(Lists.newArrayList(node1, node2, node3)).size());

		// Edges 
		assertEquals(11, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Common target patterns 
		DataTriple dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonObject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3)));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject(),
				dataTriple2.getSubject(), dataTriple2.getObject(), dataTriple3.getSubject(), dataTriple3.getObject()));

		dataTriple1 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), dictionary.getKey("<p9>"));
		dataTriple2 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), dictionary.getKey("<p10>"));
		dataTriple3 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), dictionary.getKey("<p1>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonObject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3)));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple2.getSubject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c3>"))), summary.getClassesForSummaryNode(dataTriple1.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), summary.getClassesForSummaryNode(dataTriple3.getSubject()));

		dataTriple1 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), dictionary.getKey("<p3>"));
		dataTriple2 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), dictionary.getKey("<p4>"));
		dataTriple3 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), dictionary.getKey("<p6>"));
		DataTriple dataTriple4 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), dictionary.getKey("<p7>"));
		DataTriple dataTriple5 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), dictionary.getKey("<p8>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonObject(Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5));
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple2.getSubject(),
				dataTriple3.getSubject(), dataTriple4.getSubject(), dataTriple5.getSubject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), summary.getClassesForSummaryNode(dataTriple1.getObject()));

		assertEquals(18, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}

	@Test
	public void summarizePropertyPaths() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("pp.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();
		// Class nodes
		assertEquals(10, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		List<Integer> classes = TestUtils.getClassesFromTypeComponent(context, summaryResult.getStorageLayout().getEncodedSummaryTypesTable());
		assertTrue(classes.contains(dictionary.getKey("<c1>")));;
		assertTrue(classes.contains(dictionary.getKey("<c2>")));
		assertTrue(classes.contains(dictionary.getKey("<c3>")));
		assertTrue(classes.contains(dictionary.getKey("<c4>")));
		assertTrue(classes.contains(dictionary.getKey("<c5>")));
		assertTrue(classes.contains(dictionary.getKey("<c6>")));
		assertTrue(classes.contains(dictionary.getKey("<c7>")));
		assertTrue(classes.contains(dictionary.getKey("<c8>")));
		assertTrue(classes.contains(dictionary.getKey("<c9>")));
		assertTrue(classes.contains(dictionary.getKey("<c10>")));

		// All class sets are represented
		int node1 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"))));
		int node2 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c2>"), dictionary.getKey("<c3>"))));
		int node3 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))));
		int node4 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c5>"))));
		int node5 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c6>"))));
		int node6 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c7>"))));
		int node7 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))));
		int node8 = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c10>"))));
		assertNotEquals(0, node1);
		assertNotEquals(0, node2);
		assertNotEquals(0, node3);
		assertNotEquals(0, node4);
		assertNotEquals(0, node5);
		assertNotEquals(0, node6);
		assertNotEquals(0, node7);
		assertNotEquals(0, node8);

		// Each data node uniquely represents a class set
		assertEquals(8, Utils.toSet(Lists.newArrayList(node1, node2, node3,
				node4, node5, node6, node7, node8)).size());

		// Property path patterns 

		// Property path of length 2 with all nodes untyped
		DataTriple dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p2>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject(),
				dataTriple2.getSubject(), dataTriple2.getObject()));

		// Property path of length 2 with all nodes typed
		dataTriple1 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c2>"), dictionary.getKey("<c3>"))), dictionary.getKey("<p3>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c2>"), dictionary.getKey("<c3>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), dictionary.getKey("<p4>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"))), summary.getClassesForSummaryNode(dataTriple1.getSubject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c2>"), dictionary.getKey("<c3>"))), summary.getClassesForSummaryNode(dataTriple1.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c4>"))), summary.getClassesForSummaryNode(dataTriple2.getObject()));

		// Property path star with typed joining node
		dataTriple1 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))), dictionary.getKey("<p12>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))), null, dictionary.getKey("<p13>"));
		DataTriple dataTriple3 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))), dictionary.getKey("<p14>"));
		DataTriple dataTriple4 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))), null, dictionary.getKey("<p15>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		assertEquals(dataTriple3.getObject(), dataTriple4.getSubject());
		assertEquals(dataTriple1.getObject(), dataTriple3.getObject());
		assertEquals(dataTriple2.getSubject(), dataTriple4.getSubject());
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple2.getObject(),
				dataTriple3.getSubject(), dataTriple4.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c8>"), dictionary.getKey("<c9>"))), summary.getClassesForSummaryNode(dataTriple1.getObject()));

		// Property path star with all nodes untyped
		dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p8>"));
		dataTriple2 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p9>"));
		dataTriple3 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p10>"));
		dataTriple4 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p11>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		assertEquals(dataTriple3.getObject(), dataTriple4.getSubject());
		assertEquals(dataTriple1.getObject(), dataTriple3.getObject());
		assertEquals(dataTriple2.getSubject(), dataTriple4.getSubject());
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject(),
				dataTriple2.getSubject(), dataTriple2.getObject(),
				dataTriple3.getSubject(),  dataTriple3.getObject(), 
				dataTriple4.getSubject(), dataTriple4.getObject()));

		// Same properties, all nodes untyped
		dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject()));
		assertEquals(dataTriple1.getSubject(), dataTriple1.getObject());
		int node = dataTriple1.getSubject();
		assertEquals(1, summary.getIncomingDegreeForDataNode(node));
		assertEquals(1, summary.getOutgoingDegreeForDataNode(node));

		// Same properties, all nodes of the same type
		dataTriple1 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c10>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c10>"))), dictionary.getKey("<p16>"));
		assertNotNull(dataTriple1);
		assertEquals(dataTriple1.getSubject(), dataTriple1.getObject());

		// Same properties, untyped start node, typed joining node and path end node (different types)
		dataTriple1 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c5>"))), dictionary.getKey("<p6>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c5>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c6>"))), dictionary.getKey("<p6>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject()));
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple1.getObject());
		assertNotEquals(dataTriple2.getSubject(), dataTriple2.getObject());

		// Same properties, untyped start node, typed joining node and path end node (same types)
		dataTriple1 = TestUtils.getDataTriple(summary, null, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c7>"))), dictionary.getKey("<p7>"));
		dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c7>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c7>"))), dictionary.getKey("<p7>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject()));
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple1.getObject());
		assertEquals(dataTriple2.getSubject(), dataTriple2.getObject());
		node = dataTriple1.getSubject();
		assertEquals(0, summary.getIncomingDegreeForDataNode(node));
		assertEquals(1, summary.getOutgoingDegreeForDataNode(node));

		assertEquals(33, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		// Data nodes count
		assertEquals(23, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount());

		// Edges	 
		assertEquals(18, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(10, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
	}

	@Test
	public void summarizeNonCooccurringProperties() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("noncoocprops.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();

		// Class nodes
		assertEquals(2, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		List<Integer> classes = TestUtils.getClassesFromTypeComponent(context, summaryResult.getStorageLayout().getEncodedSummaryTypesTable());
		assertTrue(classes.contains(dictionary.getKey("<c1>")));;
		assertTrue(classes.contains(dictionary.getKey("<c2>")));

		// Data nodes count
		assertEquals(4, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount());
		// All class sets are represented
		int node = summary.getSummaryDataNodeForClasses(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))));
		assertNotEquals(0, node);

		// Edges 
		assertEquals(2, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());		
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Non-cooccurring properties 
		DataTriple dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTriple(summary, new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"))), dictionary.getKey("<p2>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertTrue(dataTriple1.getSubject() != dataTriple2.getSubject());
		assertTrue(dataTriple1.getObject() != dataTriple2.getObject());
		AssertUtils.assertDataNodesUntyped(summary, Lists.newArrayList(dataTriple1.getSubject(), dataTriple1.getObject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"), dictionary.getKey("<c2>"))), summary.getClassesForSummaryNode(dataTriple2.getSubject()));
		assertEquals(new TIntHashSet(Lists.newArrayList(dictionary.getKey("<c1>"))), summary.getClassesForSummaryNode(dataTriple2.getObject()));

		assertEquals(6, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}

	@Test
	public void untypedStarPlusNonCooccurringProperties() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("all.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();
		assertEquals(9, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
		assertEquals(0, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount()); 
		assertEquals(9, summaryResult.getStatistics().getSummaryNodeStats().getDataNodeCount()); 
		assertEquals(0, summaryResult.getStatistics().getSummaryTripleStats().getSchemaTripleCount());
		assertEquals(6, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());

		DataTriple dataTriple1 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p3>"));
		DataTriple dataTriple4 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p4>"));
		DataTriple dataTriple5 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p5>"));
		DataTriple dataTriple6 = TestUtils.getDataTriple(summary, null, null, dictionary.getKey("<p6>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		assertNotNull(dataTriple6);
		assertTrue(dataTriple2.getSubject() == dataTriple4.getSubject());
		assertTrue(dataTriple1.getObject() == dataTriple3.getObject());
		int subject = dataTriple2.getSubject();
		assertNotEquals(subject, dataTriple5.getSubject());
		assertNotEquals(subject, dataTriple5.getObject());
		assertNotEquals(subject, dataTriple6.getSubject());
		assertNotEquals(subject, dataTriple6.getObject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple5.getSubject());
		assertNotEquals(dataTriple3.getSubject(), dataTriple5.getSubject());
		assertNotEquals(dataTriple2.getObject(), dataTriple5.getSubject());
		assertNotEquals(dataTriple4.getObject(), dataTriple5.getSubject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple5.getObject());
		assertNotEquals(dataTriple3.getSubject(), dataTriple5.getObject());
		assertNotEquals(dataTriple2.getObject(), dataTriple5.getObject());
		assertNotEquals(dataTriple4.getObject(), dataTriple5.getObject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple6.getSubject());
		assertNotEquals(dataTriple3.getSubject(), dataTriple6.getSubject());
		assertNotEquals(dataTriple2.getObject(), dataTriple6.getSubject());
		assertNotEquals(dataTriple4.getObject(), dataTriple6.getSubject());
		assertNotEquals(dataTriple1.getSubject(), dataTriple6.getObject());
		assertNotEquals(dataTriple3.getSubject(), dataTriple6.getObject());
		assertNotEquals(dataTriple2.getObject(), dataTriple6.getObject());
		assertNotEquals(dataTriple4.getObject(), dataTriple6.getObject());	
	}

	@Test
	public void classesAndPropertiesRepresentedByThemselves() throws ConfigurationException, BeansException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("classes_and_props.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());

		// Property nodes
		DmlDao dmlDao = context.getBean(DmlDao.class);
		assertTrue(TestUtils.containsPropertyNode(dmlDao, summaryResult.getPropertyNodesTable(), dictionary.getKey("<p1>")));
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getPropertyNodeCount());

		// Data nodes count
		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertTrue(s1Rep != o1Rep);
		assertTrue(s1Rep != o2Rep);
		assertTrue(s1Rep != o3Rep);
		assertTrue(o1Rep != o2Rep);
		assertTrue(o1Rep != o3Rep);
		assertTrue(o2Rep != o3Rep);
		assertEquals(4, summary.getSummaryDataNodes().size());

		// Edges 
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Total nodes
		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}

	@Test
	public void testRepresentingDataNodes() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(TYPED_WEAK_SUMMARY, TYPED_WEAK_DATASETS_DIR.concat("repr.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);

		WeakTypeEquivalenceSummary summary = (WeakTypeEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);
		final String qualifiedEncodedSummaryTable = NameUtils.getQualifiedEncodedSummaryTableName(summaryResult.getStorageLayout().getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		String encReprTable = NameUtils.getRepresentationTableName(qualifiedEncodedSummaryTable);

		// untyped p1Source in-memory
		int untypedP1Source = summary.getSourceDataNodeForDataProperty(dictionary.getKey("<p1>"));
		TIntSet set = summary.getRepresentedInputDataNodes(untypedP1Source);
		assertTrue(set.contains(dictionary.getKey("<s1>")));
		assertEquals(1, set.size());

		// untyped p1Source in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s1>"), untypedP1Source));

		// typed p1Source in-memory
		int typedP1Source = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		TIntSet classes = summary.getClassesForSummaryNode(typedP1Source);
		assertNotNull(classes);
		assertTrue(classes.contains(dictionary.getKey("<c1>")));
		assertEquals(1, classes.size());

		// typed p1Source in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s2>"), typedP1Source));

		// p1Target in-memory
		int p1Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p1>"));
		set = summary.getRepresentedInputDataNodes(p1Target);
		assertTrue(set.contains(dictionary.getKey("<o1>")));
		assertTrue(set.contains(dictionary.getKey("<o3>")));
		assertEquals(2, set.size());

		// p1Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o1>"), p1Target));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o3>"), p1Target));

		// p2Target in-memory
		int p2Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p2>"));
		set = summary.getRepresentedInputDataNodes(p2Target);
		assertTrue(set.contains(dictionary.getKey("<o2>")));
		assertEquals(1, set.size());
		
		// p2Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o2>"), p2Target));
		
		// p3Target in-memory
		int p3Target = summary.getTargetDataNodeForDataProperty(dictionary.getKey("<p3>"));
		set = summary.getRepresentedInputDataNodes(p3Target);
		assertTrue(set.contains(dictionary.getKey("<o4>")));
		assertEquals(1, set.size());
		
		// p3Target in table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o4>"), p3Target));
	}
}
