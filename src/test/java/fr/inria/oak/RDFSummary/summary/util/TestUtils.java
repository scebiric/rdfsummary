package fr.inria.oak.RDFSummary.summary.util;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.ParameterValidator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.config.params.loader.PostgresParameterLoader;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.util.SetUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.rdfconjunctivequery.Literal;
import fr.inria.oak.commons.rdfconjunctivequery.Term;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.procedure.TObjectProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

public class TestUtils {

	/**
	 * Loads the test configuration parameters from src/test/resources/config/testconfig.properties
	 * 
	 * @param summaryType
	 * @param datasetFile
	 * @param schemaFile 
	 * 
	 * @return The parameters for summarization within tests
	 * @throws ConfigurationException
	 * @throws ParametersException 
	 */
	public static PostgresSummarizerParams getTestCliqueSummarizerParams(String summaryType, String datasetFile, String schemaFile) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(datasetFile));

		PostgresParameterLoader paramLoader = new PostgresParameterLoader();
		File testConfig = new File(Constant.TEST_CONFIG_FILEPATH);
		
		PostgresSummarizerParams params = paramLoader.loadTestCliqueSummarizerParams(testConfig.getAbsolutePath(), summaryType, datasetFile, schemaFile);
		ParameterValidator.validatePostgresSummarizerParams(params);
		
		return params;
	}

	/**
	 * Loads the memory dictionary with all entries from the specified dictionary table
	 * 
	 * @param context
	 * @return The populated dictionary
	 * 
	 * @throws SQLException
	 * @throws DictionaryException
	 */
	public static Dictionary loadMemoryDictionary(final AnnotationConfigApplicationContext context) throws SQLException, DictionaryException {
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		return dictDao.loadDictionary();
	}

	/** 
	 * @param summary
	 * @param subjectClasses
	 * @param objectClasses
	 * @param dataProperty
	 * @return The data triple whose subject and object have the specified class sets and the given property, 
	 * or null if such a data triple does not exist
	 */
	public static DataTriple getDataTriple(final WeakTypeEquivalenceSummary summary, final TIntSet subjectClasses, final TIntSet objectClasses, final int dataProperty) {	
		Collection<DataTriple> dataTriples = summary.getDataTriplesForDataProperty(dataProperty); 
		for (DataTriple dataTriple : dataTriples) {
			TIntSet sc = summary.getClassesForSummaryNode(dataTriple.getSubject());
			TIntSet oc = summary.getClassesForSummaryNode(dataTriple.getObject());
			if (SetUtils.equals(subjectClasses, sc) && SetUtils.equals(objectClasses, oc))
				return dataTriple;		
		}	

		return null;
	}

	/**
	 * @param context
	 * @param encodedTypesTable
	 * @return The set of classes from the type component of the summary (non-schema classes)
	 * @throws BeansException
	 * @throws SQLException
	 */
	public static List<Integer> getClassesFromTypeComponent(final AnnotationConfigApplicationContext context, final String encodedTypesTable) throws BeansException, SQLException {
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);		
		return rdfTablesDao.getDistinctClassesFromTypeComponent(encodedTypesTable);
	}

	/**
	 * @param classes
	 * @return A {@link TIntHashSet} with the elements from the specified list
	 */
	public static TIntSet getClassSet(List<Integer> classes) {
		return new TIntHashSet(classes);
	}

	/**
	 * Retrieves the {@link RdfTablesDao} bean from the context and creates its {@link Statement} object with the specified fetch size.
	 * 
	 * @param context
	 * @param fetchSize
	 * @throws BeansException
	 * @throws SQLException
	 */
	public static void createStatement(final AnnotationConfigApplicationContext context, final int fetchSize) throws BeansException, SQLException {
		context.getBean(SqlConnectionHandler.class).createStatement(fetchSize, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
	}

	/**
	 * @param classesForDataNodeMap
	 * @return A set of distinct classes from all the sets in the value collection of the specified map 
	 */
	public static TIntSet getDistinctClassNodes(final TIntObjectMap<TIntSet> classesForDataNodeMap) {
		TIntSet classes = new TIntHashSet();
		classesForDataNodeMap.forEachValue(new TObjectProcedure<TIntSet>() {
			@Override
			public boolean execute(TIntSet classSet) {
				classes.addAll(classSet);
				return true;
			}
		});

		return classes;
	}

	/**
	 * @param sets
	 * @param set
	 * @return True if the specified set of sets, contains a set equal to the specified set, otherwise false.
	 */
	public static boolean contains(final List<TIntSet> sets, final TIntSet set) {
		Preconditions.checkNotNull(sets);
		Preconditions.checkNotNull(set);

		for (TIntSet setFromList : sets) {
			if (SetUtils.equals(setFromList, set))
				return true;
		}

		return false;
	}

	/**
	 * Assumes that a weak summary may have at most one data triple per distinct data property
	 * 
	 * @param summary 
	 * @param dataProperty
	 * @return Data triple comprising the property or null if such does not exist
	 */
	public static DataTriple getDataTripleForProperty(final WeakPureCliqueEquivalenceSummary summary, final int dataProperty) {
		Collection<DataTriple> dataTriples = summary.getDataTriplesForDataProperty(dataProperty);
		if (dataTriples.size() == 0)
			return null;

		return dataTriples.iterator().next();
	}

	/**
	 * Casts the term to Literal, removes the first and last character, and parses an integer from the resulting string.
	 * 
	 * @param term
	 * @return The integer value of the specified literal term.
	 */
	public static int getIntegerValueFromLiteral(final Term term) {
		Preconditions.checkNotNull(term);
		Preconditions.checkArgument(term instanceof Literal);

		String str = ((Literal) term).value;
		if (term.toString().startsWith("'") && term.toString().endsWith("'"))
			str = str.substring(1, str.length() - 1);

		return Integer.parseInt(str);
	}

	/**
	 * @param propertyNodesTable
	 * @param propertyNode
	 * @return True if propertyNodesTable contains propertyNode, otherwise false. 
	 * @throws SQLException 
	 */
	public static boolean containsPropertyNode(final DmlDao dmlDao, final String propertyNodesTable, final int propertyNode) throws SQLException { 
		return dmlDao.existsValueInColumn(propertyNodesTable, Constant.URI, Integer.toString(propertyNode));
	}
}
