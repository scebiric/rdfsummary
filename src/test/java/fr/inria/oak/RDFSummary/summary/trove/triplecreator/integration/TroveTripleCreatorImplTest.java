package fr.inria.oak.RDFSummary.summary.trove.triplecreator.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTripleCreatorImplTest {

	private static final CliqueTrovePostgresConfigurator Configurator = new CliqueTrovePostgresConfigurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;
	
	private static final String WEAK_PURE_CLIQUE_SUMMARY = SummaryType.WEAK;
	private static final String WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR = "/weak/";
	
	@Before
	public void setUp() throws BeansException, SQLException, ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
	}
	
	@Test
	public void weakPureCliqueEquivalenceSummary_representTypeTriple_subjectsOfSameDataPropertyOfSameType_summaryContainsOnlyOneTypeTriple() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		// Set up
		PostgresSummarizerParams params = TestUtils.getTestCliqueSummarizerParams(WEAK_PURE_CLIQUE_SUMMARY, WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR.concat("unique_type_triples.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		TroveCliquePostgresSummarizer summarizer = context.getBean(TroveCliquePostgresSummarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params);
				
		// Assert
		dictionary = TestUtils.loadMemoryDictionary(context);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();						
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		ResultSet resultSet = rdfTablesDao.distinctClassesFromTypeComponent(NameUtils.getEncodedTypesTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable())); 
		resultSet.next();
		int classIRI = resultSet.getInt(1);
		assertEquals(dictionary.getKey("<c1>").intValue(), classIRI);
		assertEquals(1, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
		int sDataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<x1>"));
		assertEquals(sDataNode, summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<x3>")));
		assertTrue(summary.existsTypeTriple(sDataNode, classIRI));
	}

}
