package fr.inria.oak.RDFSummary.summary.util;

import static org.junit.Assert.*;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import fr.inria.oak.RDFSummary.constants.Extension;

public class FileUtilsTest {

	@Test
	public void FilenameUtils_getExtension() {
		// Set up
		String filename = "/bla/bla2/foo.nt";
		
		// Run
		String extension = FilenameUtils.getExtension(filename);
		
		// Assert
		assertEquals(Extension.N_TRIPLES, extension);
	}
}
