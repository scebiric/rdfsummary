package fr.inria.oak.RDFSummary.summary.util.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.util.NameUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class NameUtilsTest {

	@Test
	public void getSchemaName() {
		// Set up
		String schemaName = "fooschema";
		String table = schemaName + "." + "footable";
		
		// Run 
		String value = NameUtils.getSchemaName(table);
		
		// Assert
		assertEquals(schemaName, value);
	}

	@Test
	public void getIndexName() {
		// Set up
		String qualifiedTableName = "schema.table";
		String columns = "spo";
		
		// Run
		String indexName = NameUtils.getIndexName(qualifiedTableName, columns);
		
		// Assert
		assertEquals("schema_table_idx_spo", indexName);
	}
}
