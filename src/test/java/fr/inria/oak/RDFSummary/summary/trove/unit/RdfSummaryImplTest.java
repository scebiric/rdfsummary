package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TroveRdfSummaryImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfSummaryImplTest {

	private UriGenerator uriGenerator;
	private DictionaryDao dictDao;
	
	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		uriGenerator = mock(UriGenerator.class);
		dictDao = mock(DictionaryDao.class);		
	}

	@Test
	public void createDataTriple_newProperty_dataTripleMappedToProperty() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int dataProperty = 2;
		int target = 3;
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);

		// Run
		DataTriple dataTriple = summary.createDataTriple(source, dataProperty, target);

		// Assert
		assertEquals(source, dataTriple.getSubject());
		assertEquals(dataProperty, dataTriple.getDataProperty());
		assertEquals(target, dataTriple.getObject());
		List<DataTriple> triples = Lists.newArrayList(summary.getDataTriplesForDataPropertyMap().get(dataProperty));
		assertEquals(1, triples.size());
		assertEquals(dataTriple, triples.get(0));
	}

	@Test
	public void createDataTriple_existingProperty_dataTripleMappedToProperty() throws BerkeleyDbException {
		// Set up
		int dataProperty = 2;
		int source1 = 1;
		int target1 = 3;
		int source2 = 4;
		int target2 = 5;
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		DataTriple dataTriple1 = summary.createDataTriple(source1, dataProperty, target1);

		// Run
		DataTriple dataTriple2 = summary.createDataTriple(source2, dataProperty, target2);

		// Assert
		assertEquals(source2, dataTriple2.getSubject());
		assertEquals(dataProperty, dataTriple2.getDataProperty());
		assertEquals(target2, dataTriple2.getObject());
		List<DataTriple> triples = Lists.newArrayList(summary.getDataTriplesForDataPropertyMap().get(dataProperty));
		assertTrue(triples.contains(dataTriple1));
		assertTrue(triples.contains(dataTriple2));
	}

	@Test
	public void existsDataTriple_tripleDoesNotExist_returnsFalse() throws BerkeleyDbException {
		// Set up
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);

		// Run
		boolean exists = summary.existsDataTriple(1, 2, 3);

		// Assert
		assertFalse(exists);
	}

	@Test
	public void existsDataTriple_tripleExists_returnsTrue() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int dataProperty = 2;
		int target = 3;
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		summary.createDataTriple(source, dataProperty, target);

		// Run
		boolean exists = summary.existsDataTriple(source, dataProperty, target);

		// Assert
		assertTrue(exists);
	}

	@Test
	public void representInputDataNode_newGDataNode_newEntryExistsInDirectAndInverseRepresentationMaps() {
		// Set up
		final int gDataNode = 1;
		final int sDataNode = 2;
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);

		// Call
		summary.representInputDataNode(gDataNode, sDataNode);

		// Assert
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(gDataNode));
		assertEquals(1, summary.getRepresentedInputDataNodes(sDataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(1));
	}

	@Test
	public void representInputDataNode_existingDataNode_newEntryExistsInDirectAndInverseRepresentationMaps() {
		// Set up
		final int gDataNode = 1;
		final int sDataNode = 2;
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		summary.representInputDataNode(3, 2);

		// Call
		summary.representInputDataNode(gDataNode, sDataNode);

		// Assert
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(gDataNode));
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(3));
		assertEquals(2, summary.getRepresentedInputDataNodes(sDataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(1));
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(3));
	}

	@Test
	public void createDataNode_gDataNodeRepresentedBySDataNodeAndReturnsSDataNode() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 2;
		String uri = "d2";
		when(dictDao.getHighestKey()).thenReturn(1);
		when(uriGenerator.generateDataNodeUri(2)).thenReturn(uri);
		when(dictDao.getExistingOrNewKey(uri)).thenReturn(sDataNode);
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		
		// Call
		int representative = summary.createDataNode(gDataNode);

		// Assert
		assertEquals(sDataNode, representative);
		assertEquals(sDataNode, summary.getSummaryDataNodeForInputDataNode(gDataNode));
	}
	
	@Test
	public void createDataNode_representativeOfGDataNodeStoredAsLastCreatedDataNode() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 2;
		String uri = "d2";
		when(dictDao.getHighestKey()).thenReturn(1);
		when(uriGenerator.generateDataNodeUri(2)).thenReturn(uri);
		when(dictDao.getExistingOrNewKey(uri)).thenReturn(sDataNode);
		RdfSummary summary = new TroveRdfSummaryImpl(uriGenerator, dictDao);
		
		// Call
		int representative = summary.createDataNode(gDataNode);

		// Assert
		assertEquals(representative, summary.getLastCreatedDataNode());
	}
}
