package fr.inria.oak.RDFSummary.summary.util.unit;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.inria.oak.RDFSummary.util.RdfUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfUtilsTest {

	@Test
	public void stripNamespace_nullString_returnsNull() {
		// Set up
		String str = null;
		
		// Run
		String newStr = RdfUtils.stripNamespace(str);
		
		// Assert
		assertNull(newStr);
	}

	@Test
	public void stripNamespace_blankString_returnsBlank() {
		// Set up
		String str = "";
		
		// Run
		String newStr = RdfUtils.stripNamespace(str);
		
		// Assert
		assertEquals(str, newStr);
	}
	
	@Test
	public void stripNamespace_noNamespaceNoEnclosingBrackets_returnsSameString() {
		// Set up
		String str = "d15";
		
		// Run
		String newStr = RdfUtils.stripNamespace(str);
		
		// Assert
		assertEquals(str, newStr);
	}
}
