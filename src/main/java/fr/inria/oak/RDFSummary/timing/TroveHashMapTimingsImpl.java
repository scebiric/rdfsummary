package fr.inria.oak.RDFSummary.timing;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveHashMapTimingsImpl implements Timings {

	private TObjectLongMap<String> timeByLabel;

	public TroveHashMapTimingsImpl() {
		timeByLabel = new TObjectLongHashMap<String>();
	}
	
	@Override
	public void addTime(String label, long time) {
		timeByLabel.put(label, time);
	}

	@Override
	public long getTime(String label) {
		return timeByLabel.get(label);
	}
}
