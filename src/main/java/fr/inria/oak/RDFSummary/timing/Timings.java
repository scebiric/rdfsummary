package fr.inria.oak.RDFSummary.timing;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Timings {

	public void addTime(String label, long time);
	
	public long getTime(String label);
}
