package fr.inria.oak.RDFSummary.timing;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfLoaderTiming {

	public static final String LOADING_TRIPLES_FROM_FILE = "Loading triples from file: ";
	public static final String ENCODING_DATASET_TRIPLES = "Encoding dataset triples: ";
	public static final String LOADING_SCHEMA_FROM_FILE = "Loading schema from file: ";
	public static final String LOADING_SCHEMA_FROM_TRIPLES_TABLE = "Loading schema from triples table: ";
	public static final String SATURATION = "Saturation: ";
	public static final String TOTAL_DATASET_LOADING = "Total dataset loading time: "; 
}
