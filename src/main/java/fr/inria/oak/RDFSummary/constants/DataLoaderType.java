package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DataLoaderType {
	public static final String PSQL_COPY = "copy";
	public static final String JENA = "jena";
}
