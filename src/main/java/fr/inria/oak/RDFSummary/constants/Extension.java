package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Extension {

	public static final String TXT = "txt";
	public static final String DOT = "dot";
	public static final String PDF = "pdf";
	public static final String PNG = "png";
	public static final String XLS = "xls";
	public static final String N_TRIPLES = "nt";
	public static final String DB = "db";
	public static final Object RDF = "rdf"; 
}
