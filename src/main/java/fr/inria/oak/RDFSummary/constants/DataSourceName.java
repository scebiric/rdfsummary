package fr.inria.oak.RDFSummary.constants;

/**
 * Supported data sources
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DataSourceName {
	public static String POSTGRES = "postgres";	
}
