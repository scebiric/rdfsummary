package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Chars {

	public static final char UNDERSCORE = '_';
	public static final char LEFT_PAREN = '(';
	public static final char RIGHT_PAREN = ')';
	public static final char DOT = '.';
	public static final char SEMICOLON = ';';
	public static final char SPACE = ' ';
	public static final char SLASH = '/';
	public static final char EQUALS = '=';
	public static final char STAR = '*';
	public static final char COMMA = ',';
	public static final char APOSTROPHE = '\'';
	public static final char QUESTION_MARK = '?';
	public static final char LEFT_ANGLE_BRACKET = '<';
	public static final char RIGHT_ANGLE_BRACKET = '>';
	public static final char COLON = ':';
	public static final char QUOTE = '\"';
	public static final char LEFT_SQUARE_BRACKET = '[';
	public static final char RIGHT_SQUARE_BRACKET = ']';
	public static final char LEFT_CURLY_BRACE =  '{';
	public static final char RIGHT_CURLY_BRACE =  '}';
	public static final char BACKSLASH = '\\';
	public static final char HASH = '#';
}
