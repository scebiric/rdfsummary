package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class HasAnswers {
	public static final int NO = 0;
	public static final int YES = 1;
}
