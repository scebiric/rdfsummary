package fr.inria.oak.RDFSummary.constants;

public class Rdfs {
	public static String SUBCLASS = "rdfs:subClassOf";
	public static String SUBPROPERTY = "rdfs:subPropertyOf";
	public static String DOMAIN = "rdfs:domain";
	public static String RANGE = "rdfs:range";
	
	public static String FULL_SUBCLASS = "<http://www.w3.org/2000/01/rdf-schema#subClassOf>";
	public static String FULL_SUBPROPERTY = "<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>";
	public static String FULL_DOMAIN = "<http://www.w3.org/2000/01/rdf-schema#domain>";
	public static String FULL_RANGE = "<http://www.w3.org/2000/01/rdf-schema#range>";
}
