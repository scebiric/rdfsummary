package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Equivalence {

	public static final String CLIQUE = "clique";
	public static final String BISIMULATION = "bisim";
}
