package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ColumnArray {

	public static final String[] TRIPLES = new String[] { Constant.SUBJECT, Constant.PROPERTY, Constant.OBJECT };
	public static final String[] TYPE_TRIPLES = new String[] { Constant.SUBJECT, Constant.OBJECT };
	public static final String[] ENC_REPRESENTATION_TABLE = new String[] { Constant.INPUT_DATA_NODE, Constant.SUMMARY_DATA_NODE };
	public static final String[] SUBJECT = new String[] { Constant.SUBJECT };
	public static final String[] PROPERTY = new String[] { Constant.PROPERTY };
	public static final String[] OBJECT = new String[] { Constant.OBJECT };

}
