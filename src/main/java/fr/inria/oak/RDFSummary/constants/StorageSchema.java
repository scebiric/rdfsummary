package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StorageSchema {

	public static final String TABLE_PER_ROLE_AND_CONCEPT = "TABLE_PER_ROLE_AND_CONCEPT";
	public static final String TRIPLES_TABLE = "TRIPLES_TABLE";
}
