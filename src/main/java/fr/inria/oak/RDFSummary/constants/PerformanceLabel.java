package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PerformanceLabel {

	public static final String SUMMARIZATION_TIME = "Summarization time";
	
	public static final String CLASS_NODES_FROM_SCHEMA = "Class nodes from schema component";
	public static final String CLASS_NODES_FROM_TYPE = "Class nodes from type component";
	public static final String CLASS_NODES_TOTAL = "Computing class nodes total";
	
	public static final String PROPERTY_NODES_FROM_SCHEMA = "Property nodes from schema component";
	public static final String COPY_PROPERTY_NODES = "Copying schema property nodes to schema properties table";
	public static final String EXTRACT_DATA_PROPERTIES = "Extracting data properties to possible property nodes table";
	public static final String INDEX_POSSIBLE_PROPERTY_NODES = "Indexing possible property nodes table";
	public static final String CLUSTER_POSSIBLE_PROPERTY_NODES = "Clustering possible property nodes table";
	public static final String PROPERTY_NODES_FROM_DATA_SUBJECTS = "Property nodes from data component subjects";
	public static final String PROPERTY_NODES_FROM_DATA_OBJECTS = "Property nodes from data component objects";
	public static final String PROPERTY_NODES_FROM_TYPE = "Property nodes from type component";
	public static final String PROPERTY_NODES_TOTAL = "Computing property nodes total";

	public static final String LOAD_CLASS_NODES_TO_MEMORY = "Loading class nodes to memory";
	public static final String LOAD_PROPERTY_NODES_TO_MEMORY = "Loading property nodes to memory";

	public static final String DATA_TRIPLES_RESULT_SET = "Select all data triples (without iterating)";
	public static final String TYPE_TRIPLES_RESULT_SET = "Select all type triples (without iterating)";
	public static final String SUMMARIZE_DATA = "Summarizing data triples";
	public static final String SUMMARIZE_TYPES = "Summarizing type triples";
	public static final String DISTINCT_SP_ORDER_BY_S = "Select distinct s,p order by s (without iterating)";
	public static final String DISTINCT_OP_ORDER_BY_O = "Select distinct o,p order by o (without iterating)";
	public static final String DISTINCT_SP_ORDER_BY_S_UNTYPED = "Select distinct s,p order by s, with s being untyped (without iterating)";
	public static final String DISTINCT_OP_ORDER_BY_O_UNTYPED = "Select distinct o,p order by o, with o being untyped (without iterating)";

	public static final String BUILD_SOURCE_CLIQUES = "Building source cliques";
	public static final String BUILD_TARGET_CLIQUES = "Building target cliques";
	public static final String COMPUTE_DISTINCT_NODES = "Computing distinct nodes";
	public static final String SELECT_DISTINCT_NODES = "Select all distinct nodes (without iterating)";
	public static final String REPRESENT_G_DATA_NODES = "Representing G data nodes (selecting all distinct data nodes, iterating and representing)";
	public static final String REPRESENT_DATA_TRIPLES = "Representing data triples";

	public static final String DISTINCT_CLASSES_COUNT = "Distinct classes count"; 
}
