package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummaryType {
	public static String WEAK = "weak"; // weak pure clique equivalence
	public static String TYPED_WEAK = "typweak"; // weak type equivalence
	public static String STRONG = "strong"; // strong pure clique equivalence
	public static String TYPED_STRONG = "typstrong"; // strong type equivalence
	
	public static String FORWARD_ONLY = "fw";
	public static String BACKWARD_ONLY = "bw";
	public static String FORWARD_BACKWARD = "fb";
}
