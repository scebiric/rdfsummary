package fr.inria.oak.RDFSummary.constants;

/**
 * TROVE: uses Trove collections implementation 
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummaryRepresentation {
	public static final String TROVE = "trove";
}
