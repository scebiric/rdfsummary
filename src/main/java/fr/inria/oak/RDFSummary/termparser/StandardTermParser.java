package fr.inria.oak.RDFSummary.termparser;

import java.text.Normalizer;

import com.google.common.base.Preconditions;

/**
 * Implementation of the TermParser interface
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StandardTermParser implements TermParser {

	protected final String blankPattern = "_:([a-zA-Z_0-9])+";
	protected final String iriPattern = "<([a-zA-Z_0-9:\\/.])+([a-zA-Z_0-9:\\/.?#-@&=%~])*>";
	protected final String literalPattern = "\"([a-zA-Z_0-9:\\/.?#-@&=!%\\s++\\(\\)\\[\\]{}<>~,])*\""
			+ "(\\^\\^((<[a-zA-Z_0-9:\\/.?#-@&=!%\\s++\\(\\)\\[\\]{}<>~,]+>)|(xsd:[a-zA-Z_0-9]+)))?";	
	//private final String literalPattern = "\"([a-zA-Z_0-9:\\/.?#-@&=!%\\s++\\(\\)\\[\\]{}<>~,])*\"(~[>\\),\\s++\\n])*"; // according to rdf CQParser.jj
	
	public boolean isLiteral(final String str) {
		Preconditions.checkNotNull(str);
		String normalized = Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		return normalized.matches(literalPattern);
	}
	
	public boolean isIRI(final String str) {
		Preconditions.checkNotNull(str);
		return str.matches(iriPattern);
	}

	public boolean isBlank(final String str) {
		Preconditions.checkNotNull(str);
		return str.matches(blankPattern);
	}

	public boolean isIRIOrBlank(final String str) {
		Preconditions.checkNotNull(str);
		return this.isIRI(str) || this.isBlank(str);
	}
}
