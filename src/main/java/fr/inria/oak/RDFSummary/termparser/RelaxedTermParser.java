package fr.inria.oak.RDFSummary.termparser;

import java.text.Normalizer;

import com.google.common.base.Preconditions;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RelaxedTermParser extends StandardTermParser {

	@Override
	public boolean isIRI(final String str) {
		Preconditions.checkNotNull(str);
		String normalized = Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		return normalized.matches(iriPattern);
	}
	
}
