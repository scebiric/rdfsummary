package fr.inria.oak.RDFSummary.termparser;

/**
 * Interface for parsing the IRIs from strings
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TermParser {
	public boolean isIRI(String str);
	
	public boolean isBlank(String str);
	
	public boolean isLiteral(String str);
	
	public boolean isIRIOrBlank(String str);
}
