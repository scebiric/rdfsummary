package fr.inria.oak.RDFSummary.experiments;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExperimentType {

	public static final String SUMMARIZATION_TIME = "SUM_TIME";
	public static final String COMPLETENESS = "COMPLETENESS";
	public static final String METRICS = "METRICS";
	public static final String PERFORMANCE = "PERFORMANCE"; 
}
