package fr.inria.oak.RDFSummary.experiments;

import java.io.FileNotFoundException;
import java.io.IOException;

import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessResult;
import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessRunResult;
import fr.inria.oak.RDFSummary.experiments.sumtime.SummarizationTimeResult;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceResult;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface ExperimentsExporter {

	/**
	 * @param completenessResult
	 * @param outputFilepath
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	public void exportToExcel(CompletenessRunResult completenessResult, String outputFilepath) throws FileNotFoundException, IOException;

	/**
	 * @param completenessResult
	 * @param outputFolder
	 * @param experimentTimestamp 
	 * @param run 
	 * @return The filepath to the file containing completeness experiments results
	 * @throws IOException 
	 */
	public String exportCompletenessRunToTxt(CompletenessRunResult completenessResult, String outputFolder, String experimentTimestamp, int run) throws IOException;

	/**
	 * @param timeResult
	 * @param outputFolder
	 * @param experimentTimestamp 
	 * @return The filepath to the file containing summarization time experiments results
	 * @throws IOException 
	 */
	public String exportSummarizationTimeToTxt(SummarizationTimeResult timeResult, String outputFolder, String experimentTimestamp) throws IOException;

	/**
	 * @param performanceResult
	 * @param outputFolder
	 * @param datasetName
	 * @param summaryType
	 * @param experimentTimestamp
	 * @param run 
	 * @return The filepath to the file containing performance results
	 * @throws IOException
	 */
	public String exportPerformanceResultsToTxt(PerformanceResult performanceResult, String outputFolder, String datasetName, String summaryType, String experimentTimestamp, int run) throws IOException;

	/**
	 * @param completenessResult
	 * @param outputFolder
	 * @param experimentTimestamp
	 * @return The filepath to the file containing completeness result
	 * @throws IOException 
	 */
	public String exportCompletenessResultToTxt(CompletenessResult completenessResult, String outputFolder, String experimentTimestamp) throws IOException;     	
}
