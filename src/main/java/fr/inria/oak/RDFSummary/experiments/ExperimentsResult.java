package fr.inria.oak.RDFSummary.experiments;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExperimentsResult {

	private final String summaryType;
	private final String datasetName;
	private final int numberOfRuns;
	private final int fetchSize;
	
	/**
	 * @param summaryType
	 * @param datasetName
	 * @param numberOfRuns
	 * @param fetchSize
	 */
	public ExperimentsResult(final String summaryType, final String datasetName, final int numberOfRuns, final int fetchSize) {
		this.summaryType = summaryType;
		this.datasetName = datasetName;
		this.numberOfRuns = numberOfRuns;
		this.fetchSize = fetchSize;
	}

	public String getSummaryType() {
		return summaryType;
	}
	
	public String getDatasetName() {
		return datasetName;
	}
	
	public int getNumberOfRuns() {
		return numberOfRuns;
	}
	
	public int getFetchSize() {
		return fetchSize;
	}

}
