package fr.inria.oak.RDFSummary.experiments.completeness.clique;

import fr.inria.oak.RDFSummary.experiments.ExperimentsResult;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class CompletenessRunResult extends ExperimentsResult {
	
	private long timeToSatGInMilliseconds;
	private long timeToSumSatGInMilliseconds;
	private long timeToSumGInMilliseconds;
	private long timeToSatSumGInMilliseconds;
	private long timeToSumSatSumGInMilliseconds;
	
	/**
	 * @param summaryType
	 * @param datasetName
	 * @param numberOfRuns
	 * @param fetchSize
	 */
	public CompletenessRunResult(final String summaryType, final String datasetName, final int numberOfRuns, final int fetchSize) {
		super(summaryType, datasetName, numberOfRuns, fetchSize);
	}
	
	public long getTimeToSaturateGInMilliseconds() {
		return timeToSatGInMilliseconds;
	}
	
	public long getTimeToSummarizeSaturatedGInMilliseconds() {
		return timeToSumSatGInMilliseconds;
	}
	
	public long getTotalTimeToSumSatGInMilliseconds() {
		return getTimeToSaturateGInMilliseconds() + getTimeToSummarizeSaturatedGInMilliseconds();
	}
	
	public void setTimeToSaturateG(final long value) {
		timeToSatGInMilliseconds = value;
	}
	
	public void setTimeToSummarizeSaturatedG(final long value) {
		timeToSumSatGInMilliseconds = value;
	}

	public void setTimeToSummarizeG(final long value) {
		timeToSumGInMilliseconds = value;
	}
	
	public long getTimeToSummarizeGInMilliseconds() {
		return timeToSumGInMilliseconds;
	}

	public void setTimeToSummarizeSaturatedSummaryOfG(final long summTime) {
		timeToSumSatSumGInMilliseconds = summTime;
	}
	
	public long getTimeToSummarizeSaturatedSummaryOfGInMilliseconds() {
		return timeToSumSatSumGInMilliseconds;
	}

	public void setTimeToSaturateSummarizedG(final long saturationTime) {
		timeToSatSumGInMilliseconds = saturationTime;
	}
	
	public long getTimeToSaturateSummarizedGInMilliseconds() {
		return timeToSatSumGInMilliseconds;
	}
	
	public long getTotalTimeToSumSatSumGInMilliseconds() {
		return timeToSumGInMilliseconds + timeToSatSumGInMilliseconds + timeToSumSatSumGInMilliseconds;
	}
}
