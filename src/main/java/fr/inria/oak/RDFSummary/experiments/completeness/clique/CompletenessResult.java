package fr.inria.oak.RDFSummary.experiments.completeness.clique;

import fr.inria.oak.RDFSummary.experiments.ExperimentsResult;

/**
 *
 * @author Sejla CEBIRIC
 *
 */
public class CompletenessResult extends ExperimentsResult {

	private final double averageSumSatTimeMs;
	private final double averageSumSatSumTimeMs;
	
	/**
	 * @param summaryType
	 * @param datasetName
	 * @param numberOfRuns
	 * @param fetchSize
	 * @param averageSumSatTimeMs
	 * @param averageSumSatSumTimeMs
	 */
	public CompletenessResult(final String summaryType, final String datasetName,
			final int numberOfRuns, final int fetchSize, final double averageSumSatTimeMs, final double averageSumSatSumTimeMs) {
		super(summaryType, datasetName, numberOfRuns, fetchSize);
		this.averageSumSatTimeMs = averageSumSatTimeMs;
		this.averageSumSatSumTimeMs = averageSumSatSumTimeMs;
	}

	public double getAverageSumSatTimeInMilliseconds() {
		return averageSumSatTimeMs;
	}
	
	public double getAverageSumSatSumTimeInMilliseconds() {
		return averageSumSatSumTimeMs;
	}

	public double getAverageSumSatTimeInSeconds() {
		return averageSumSatTimeMs / 1000;
	}
	
	public double getAverageSumSatSumTimeInSeconds() {
		return averageSumSatSumTimeMs / 1000;
	}
}
