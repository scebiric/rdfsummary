package fr.inria.oak.RDFSummary.experiments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessResult;
import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessRunResult;
import fr.inria.oak.RDFSummary.experiments.sumtime.SummarizationTimeResult;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceResult;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExperimentsExporterImpl implements ExperimentsExporter {

	private static final Logger log = Logger.getLogger(ExperimentsExporter.class);
		
	private static PrintWriter PrintWriter;
	private static StringBuilder Builder;
	
	private static final String QUERY = "Query: ";

	@Override
	public String exportSummarizationTimeToTxt(final SummarizationTimeResult timeResult, final String outputFolder, final String experimentTimestamp) throws IOException {
		Preconditions.checkNotNull(timeResult);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(experimentTimestamp));

		log.info("Exporting summarization time result...");		
		final String filename = NameUtils.getExperimentFilename(timeResult.getDatasetName(), timeResult.getSummaryType(), ExperimentType.SUMMARIZATION_TIME, experimentTimestamp);	
		final String filepath = Utils.getFilepath(outputFolder, filename, Extension.TXT);
		PrintWriter = new PrintWriter(new FileWriter(filepath));
		
		PrintWriter.println("Dataset: " + timeResult.getDatasetName());
		PrintWriter.println("Summary type: " + timeResult.getSummaryType());
		PrintWriter.println("Number of runs: " + timeResult.getNumberOfRuns());
		PrintWriter.println("Fetch size: " + timeResult.getFetchSize());
		PrintWriter.println("Average summarization time: " + timeResult.getAverageSummarizationTimeInSeconds() + Constant.SECONDS);		
		
		PrintWriter.close();
		log.info("Summarization time experiments results exported to: " + filepath);
		
		return filepath;
	}
	
	@Override
	public String exportPerformanceResultsToTxt(final PerformanceResult performanceResult, final String outputFolder, final String datasetName, final String summaryType, 
			final String experimentTimestamp, final int run) throws IOException {
		Preconditions.checkNotNull(performanceResult);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(experimentTimestamp));

		log.info("Exporting performance results...");		
		final String filename = NameUtils.getExperimentRunFilename(datasetName, summaryType, ExperimentType.PERFORMANCE, experimentTimestamp, run);	
		final String filepath = Utils.getFilepath(outputFolder, filename, Extension.TXT);
		PrintWriter = new PrintWriter(new FileWriter(filepath));
		
		final Map<String, Long> timingMap = performanceResult.getTimingMap();
		long time;
		String performanceLabel = null;
		String query = null;
		for (final Entry<String, Long> entry : timingMap.entrySet()) {
			performanceLabel = entry.getKey();
			time = entry.getValue();
			PrintWriter.println(getTimingLine(performanceLabel, time));
			query = performanceResult.getQueryByLabel(performanceLabel);
			if (query != null) {
				PrintWriter.println(getQueryLine(query));				
			}				
			PrintWriter.println();
		}				
		
		PrintWriter.close();
		log.info("Performance results exported to: " + filepath);
		
		return filepath;
	} 
	
	@Override
	public String exportCompletenessRunToTxt(final CompletenessRunResult completenessResult, final String outputFolder, final String experimentTimestamp, final int run) throws IOException {
		Preconditions.checkNotNull(completenessResult);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));

		log.info("Exporting completeness result...");
		final String filename = NameUtils.getExperimentRunFilename(completenessResult.getDatasetName(), completenessResult.getSummaryType(), ExperimentType.COMPLETENESS, experimentTimestamp, run);		
		final String filepath = Utils.getFilepath(outputFolder, filename, Extension.TXT);
		PrintWriter = new PrintWriter(new FileWriter(filepath));
		
		PrintWriter.println("Dataset: " + completenessResult.getDatasetName());
		PrintWriter.println("Summary type: " + completenessResult.getSummaryType());
		PrintWriter.println("Number of runs: " + completenessResult.getNumberOfRuns());
		PrintWriter.println("Fetch size: " + completenessResult.getFetchSize());
		
		PrintWriter.println("sum(G): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTimeToSummarizeGInMilliseconds())));
		PrintWriter.println("sat(G): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTimeToSaturateGInMilliseconds())));
		PrintWriter.println("sum(sat(G)): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTimeToSummarizeSaturatedGInMilliseconds())));
		PrintWriter.println("sat(sum(G)): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTimeToSaturateSummarizedGInMilliseconds())));
		PrintWriter.println("sum(sat(sum(G))): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTimeToSummarizeSaturatedSummaryOfGInMilliseconds())));
		PrintWriter.println("Total sum(sat(G)): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTotalTimeToSumSatGInMilliseconds())));
		PrintWriter.println("Total sum(sat(sum(G))): " + Utils.appendSeconds(Utils.millisecondsToSeconds(completenessResult.getTotalTimeToSumSatSumGInMilliseconds())));
		
		PrintWriter.close();
		log.info("Completeness run results exported to: " + filepath);
		
		return filepath;
	}
	
	@Override
	public String exportCompletenessResultToTxt(final CompletenessResult completenessResult, final String outputFolder, final String experimentTimestamp) throws IOException {
		Preconditions.checkNotNull(completenessResult);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));

		log.info("Exporting completeness result...");
		final String filename = NameUtils.getExperimentFilename(completenessResult.getDatasetName(), completenessResult.getSummaryType(), ExperimentType.COMPLETENESS, experimentTimestamp);		
		final String filepath = Utils.getFilepath(outputFolder, filename, Extension.TXT);
		PrintWriter = new PrintWriter(new FileWriter(filepath));
		
		PrintWriter.println("Dataset: " + completenessResult.getDatasetName());
		PrintWriter.println("Summary type: " + completenessResult.getSummaryType());
		PrintWriter.println("Number of runs: " + completenessResult.getNumberOfRuns());
		PrintWriter.println("Fetch size: " + completenessResult.getFetchSize());
		
		PrintWriter.println("Average sum(sat(G)) time: " + completenessResult.getAverageSumSatTimeInSeconds() + Constant.SECONDS);
		PrintWriter.println("Average sum(sat(sum((G))) time: " + completenessResult.getAverageSumSatSumTimeInSeconds() + Constant.SECONDS);
				
		PrintWriter.close();
		log.info("Completeness experiments results exported to: " + filepath);
		
		return filepath;
	}

	private String getTimingLine(final String performanceLabel, final long time) {
		Builder = new StringBuilder();
		Builder.append(performanceLabel).append(Chars.COLON).append(Chars.SPACE);
		Builder.append(Utils.appendSeconds(Utils.millisecondsToSeconds(time)));
		
		return Builder.toString();
	}
	
	private String getQueryLine(final String query) {
		Builder = new StringBuilder();
		Builder.append(QUERY).append(query);
				
		return Builder.toString();
	}

	@Override
	public void exportToExcel(final CompletenessRunResult completenessResult, final String outputFilepath) throws FileNotFoundException, IOException {
		Preconditions.checkNotNull(completenessResult);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFilepath));

		log.info("Exporting completeness result to Excel...");
		HSSFWorkbook workbook = null;
		FileInputStream inStream = null;
		final File inFile = new File(outputFilepath);
		if (!inFile.exists())
			workbook = new HSSFWorkbook();
		else {
			inStream = new FileInputStream(inFile);
			workbook = new HSSFWorkbook(inStream);
		}
			
		String sheetName = completenessResult.getSummaryType() + Chars.UNDERSCORE + Utils.getCurrentTime();
		final HSSFSheet sheet = workbook.createSheet(sheetName);

		final Map<String, Object[]> data = Maps.newHashMap();
		data.put("1", new Object[] { "sum(g)", "sat(g)", "sat(sum(g))", "Total sum(sat(g))", "Total sum(sat(sum(g)))" });
		data.put("2", new Object[] { completenessResult.getTimeToSummarizeGInMilliseconds(), completenessResult.getTimeToSaturateGInMilliseconds(), completenessResult.getTimeToSaturateSummarizedGInMilliseconds(),
				completenessResult.getTotalTimeToSumSatGInMilliseconds(), completenessResult.getTotalTimeToSumSatSumGInMilliseconds() });

		final Set<String> keySet = data.keySet();
		int rownum = 0;
		for (String key : keySet) {
			final Row row = sheet.createRow(rownum++);
			Object[] values = data.get(key);
			int cellNumber = 0;
			for (final Object value : values) {
				final Cell cell = row.createCell(cellNumber++);
				if (value instanceof String)
					cell.setCellValue((String) value);		
				else
					cell.setCellValue((long) value);
			}
		}

		final File outFile = new File(outputFilepath);
		final FileOutputStream outStream = new FileOutputStream(outFile);
		workbook.write(outStream);
		outStream.close();
		workbook.close();
		
		log.info("Completeness result exported to: " + outputFilepath);
	}
}
