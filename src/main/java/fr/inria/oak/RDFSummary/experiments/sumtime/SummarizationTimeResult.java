package fr.inria.oak.RDFSummary.experiments.sumtime;

import fr.inria.oak.RDFSummary.experiments.ExperimentsResult;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummarizationTimeResult extends ExperimentsResult {

	private final double averageSummarizationTimeMs;
	
	/**
	 * @param summaryType
	 * @param datasetName
	 * @param numberOfRuns
	 * @param fetchSize
	 * @param averageSummarizationTime
	 */
	public SummarizationTimeResult(final String summaryType, final String datasetName,
			final int numberOfRuns, final int fetchSize, final double averageSummarizationTime) {
		super(summaryType, datasetName, numberOfRuns, fetchSize);
		
		this.averageSummarizationTimeMs = averageSummarizationTime;
	}
	
	public double getAverageSummarizationTimeInMilliseconds() {
		return averageSummarizationTimeMs;
	}
	
	public double getAverageSummarizationTimeInSeconds() {
		return averageSummarizationTimeMs / 1000;
	}
}
