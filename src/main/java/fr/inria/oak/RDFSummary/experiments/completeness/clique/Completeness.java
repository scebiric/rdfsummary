package fr.inria.oak.RDFSummary.experiments.completeness.clique;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporter;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Completeness {

	private static Logger log = Logger.getLogger(Completeness.class);
	private static AnnotationConfigApplicationContext Context = null;

	/**
	 * Run completeness experiments for weak and strong summaries
	 */
	public static void main(String[] args) throws ConfigurationException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		ExperimentsExporter exporter = null;
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();	
		PostgresExperimentsParams params = configurator.loadExperimentsParameters(args);
		if (!params.logging()) {
			log.info("Logging turned off.");
			Logger.getRootLogger().removeAllAppenders();
			Logger.getRootLogger().addAppender(new NullAppender());
		}

		final int numberOfRuns = params.getNumberOfRuns();
		log.info("Number of runs: " + numberOfRuns);
		String timestamp = Utils.getCurrentTime();
		final String datasetName = params.getSummarizerParams().getSummarizationParams().getSchemaName();
		final String outputFolder = params.getSummarizerParams().getExportParams().getOutputFolder();
		final List<String> summaryTypes = params.getSummaryTypes(); 
		validateSummaryTypes(summaryTypes);

		for (final String summaryType : summaryTypes) {
			// Set up
			log.info("SUMMARY TYPE: " + summaryType);
			long totalSumSatTime = 0;
			long totalSumSatSumTime = 0;
			for (int i = 1; i <= params.getNumberOfRuns(); i++) {
				params = configurator.loadExperimentsParameters(args);
				params.getSummarizerParams().getSummarizationParams().setSummaryType(summaryType);
				params.setExperimentTimestamp(timestamp);
				
				Context = new AnnotationConfigApplicationContext();
				configurator.javaSetUpApplicationContext(Context, params);
				
				CompletenessRunResult runResult = new CompletenessRunResult(summaryType, datasetName, numberOfRuns, params.getSummarizerParams().getSummarizationParams().getFetchSize());

				// sum(sat(G))
				params.getSummarizerParams().getSummarizationParams().setSummaryType(summaryType);
				params.getSummarizerParams().getSummarizationParams().setSaturateInput(true);
				params.getSummarizerParams().getSummarizationParams().setRecomputeSaturation(true);
				TroveCliquePostgresSummarizer summarizer = new TroveCliquePostgresSummarizer();
				TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params.getSummarizerParams());		
				long satTime = summaryResult.getDatasetPreparationResult().getSaturationTime();
				long summTime = summaryResult.getStatistics().getSummarizationTime();		
				runResult.setTimeToSaturateG(satTime);
				runResult.setTimeToSummarizeSaturatedG(summTime);

				// sum(G)
				params.getSummarizerParams().getSummarizationParams().setSummaryType(summaryType);
				params.getSummarizerParams().getSummarizationParams().setSaturateInput(false);
				params.getSummarizerParams().getExportParams().setExportToRdfFile(true);
				summaryResult = summarizer.summarize(params.getSummarizerParams());
				summTime = summaryResult.getStatistics().getSummarizationTime();
				runResult.setTimeToSummarizeG(summTime);

				// sum(sat(sum(G)))
				params.getSummarizerParams().getSummarizationParams().setSummaryType(summaryType);
				params.getSummarizerParams().getSummarizationParams().setSaturateInput(true);
				params.getSummarizerParams().getSummarizationParams().setRecomputeSaturation(true);
				params.getSummarizerParams().getSummarizationParams().setDataTriplesFilepath(summaryResult.getSummaryDataFilepath());
				params.getSummarizerParams().getSummarizationParams().setSchemaName(summaryResult.getSummaryName());
				params.getSummarizerParams().getSummarizationParams().setDropSchema(true);
				summaryResult = summarizer.summarize(params.getSummarizerParams());
				summTime = summaryResult.getStatistics().getSummarizationTime();
				runResult.setTimeToSaturateSummarizedG(summaryResult.getDatasetPreparationResult().getSaturationTime());
				runResult.setTimeToSummarizeSaturatedSummaryOfG(summTime);

				totalSumSatTime += runResult.getTotalTimeToSumSatGInMilliseconds();
				totalSumSatSumTime += runResult.getTotalTimeToSumSatSumGInMilliseconds();

				log.info("Time to summarize G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeGInMilliseconds())));
				log.info("Time to saturate G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSaturateGInMilliseconds())));
				log.info("Time to summarize saturated G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeSaturatedGInMilliseconds())));
				log.info("Time to saturate summarized G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSaturateSummarizedGInMilliseconds())));
				log.info("Time to summarize saturated summary of G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeSaturatedSummaryOfGInMilliseconds())));
				log.info("Total sum(sat(G)) time: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTotalTimeToSumSatGInMilliseconds())));
				log.info("Total sum(sat(sum(G))) time: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTotalTimeToSumSatSumGInMilliseconds())));

				// Export
				exporter = Context.getBean(ExperimentsExporter.class);
				exporter.exportCompletenessRunToTxt(runResult, params.getSummarizerParams().getExportParams().getOutputFolder(), params.getExperimentTimestamp(), i);

				// Close context
				closeApplicationContext();
			}

			double averageSumSatTime = (double) totalSumSatTime / params.getNumberOfRuns();
			double averageSumSatSumTime = (double) totalSumSatSumTime / params.getNumberOfRuns();
			final int fetchSize = params.getSummarizerParams().getSummarizationParams().getFetchSize();
			final CompletenessResult completenessResult = new CompletenessResult(summaryType, datasetName, numberOfRuns, fetchSize, averageSumSatTime, averageSumSatSumTime);
			
			exporter.exportCompletenessResultToTxt(completenessResult, outputFolder, params.getExperimentTimestamp());
		}
	}

	/**
	 * @param summaryTypes
	 */
	private static void validateSummaryTypes(final List<String> summaryTypes) {
		for (final String summaryType : summaryTypes) {
			if (!summaryType.equals(SummaryType.WEAK) && !summaryType.equals(SummaryType.STRONG)) 
				throw new IllegalArgumentException("Invalid summary type. Allowed types for completeness experiments: weak and strong.");
		}
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}

}
