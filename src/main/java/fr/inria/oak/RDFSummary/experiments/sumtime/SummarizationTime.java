package fr.inria.oak.RDFSummary.experiments.sumtime;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporter;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.PostgresSummaryResult;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummarizationTime {

	private static Logger log = Logger.getLogger(SummarizationTime.class);
	private static AnnotationConfigApplicationContext Context = null;
	private static TroveCliquePostgresSummarizer Summarizer = null;

	public SummarizationTime() {
		Summarizer = new TroveCliquePostgresSummarizer();
	}

	/**
	 * Run summarization time experiments for weak and strong summaries
	 */
	public static void main(String[] args) throws ConfigurationException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresExperimentsParams expParams = configurator.loadExperimentsParameters(args);
		if (!expParams.logging()) {
			Logger.getRootLogger().removeAllAppenders();
			Logger.getRootLogger().addAppender(new NullAppender());
		}

		final int numberOfRuns = expParams.getNumberOfRuns();
		log.info("Number of runs: " + numberOfRuns);
		expParams.setExperimentTimestamp(Utils.getCurrentTime());
		
		ExperimentsExporter exporter = null;
		PerformanceLogger performanceLogger = null;
		final String schemaName = expParams.getSummarizerParams().getSummarizationParams().getSchemaName();
		final String outputFolder = expParams.getSummarizerParams().getExportParams().getOutputFolder();
		final List<String> summaryTypes = expParams.getSummaryTypes(); 
		
		// Summary type
		for (final String summaryType : summaryTypes) {
			expParams.getSummarizerParams().getSummarizationParams().setSummaryType(summaryType);
			log.info("SUMMARY TYPE: " + summaryType); 
			long totalTime = 0;
			// Run
			for (int i = 1; i <= expParams.getNumberOfRuns(); i++) {
				// Setup
				log.info("Run " + i);
				Context = new AnnotationConfigApplicationContext();
				configurator.javaSetUpApplicationContext(Context, expParams);
								
				// Summarize
				final PostgresSummaryResult  summaryResult = Summarizer.summarize(expParams.getSummarizerParams());
				totalTime += summaryResult.getStatistics().getSummarizationTime();
				
				exporter = Context.getBean(ExperimentsExporter.class);
				performanceLogger = Context.getBean(PerformanceLogger.class);
				exporter.exportPerformanceResultsToTxt(performanceLogger.getPerformanceResult(), outputFolder, schemaName, summaryType, expParams.getExperimentTimestamp(), i);
				
				// Close context
				closeApplicationContext();
			}

			double averageTime = (double) totalTime / expParams.getNumberOfRuns();
			final int fetchSize = expParams.getSummarizerParams().getSummarizationParams().getFetchSize();
			final SummarizationTimeResult timeResult = new SummarizationTimeResult(summaryType, schemaName, expParams.getNumberOfRuns(), fetchSize, averageTime);

			exporter.exportSummarizationTimeToTxt(timeResult, outputFolder, expParams.getExperimentTimestamp());
		}
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}

}
