package fr.inria.oak.RDFSummary.experiments.completeness.bisimulation;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.BisimulationMapDbPostgresConfigurator;
import fr.inria.oak.RDFSummary.config.ExperimentsConfigurator;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizationParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporter;
import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessResult;
import fr.inria.oak.RDFSummary.experiments.completeness.clique.CompletenessRunResult;
import fr.inria.oak.RDFSummary.main.BisimulationMapDbPostgresRunner;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationMapDbPostgresSummarizerResult;
import fr.inria.oak.RDFSummary.timing.RdfLoaderTiming;
import fr.inria.oak.RDFSummary.timing.SummaryTiming;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Completeness {

	private static Logger log = Logger.getLogger(Completeness.class);
	private static BisimulationMapDbPostgresConfigurator summarizerConfigurator = new BisimulationMapDbPostgresConfigurator();
	
	/**
	 * Run completeness experiments for weak and strong summaries 
	 * @throws SignatureException 
	 */
	public static void main(String[] args) throws ConfigurationException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException, SignatureException {
		final ExperimentsConfigurator expConfigurator = new ExperimentsConfigurator();
		final AnnotationConfigApplicationContext expContext = new AnnotationConfigApplicationContext();
		expConfigurator.javaSetUpApplicationContext(expContext);
		
		final MapDbPostgresExperimentsParams params = summarizerConfigurator.loadExperimentsParameters(args);
		final PostgresExperimentsParams postgresExpParams = params.getPostgresExperimentsParams();
		if (!postgresExpParams.logging()) {
			log.info("Logging turned off.");
			Logger.getRootLogger().removeAllAppenders();
			Logger.getRootLogger().addAppender(new NullAppender());
		}

		final ExperimentsExporter exporter = expContext.getBean(ExperimentsExporter.class);
		final String schemaName = params.getMapDbPostgresSummarizerParams().getPostgresSummarizerParams().getSummarizationParams().getSchemaName();
		final int fetchSize = params.getMapDbPostgresSummarizerParams().getPostgresSummarizerParams().getSummarizationParams().getFetchSize();
		final int numberOfRuns = postgresExpParams.getNumberOfRuns();
		log.info("Number of runs: " + numberOfRuns);
		String timestamp = Utils.getCurrentTime();
		final String datasetName = postgresExpParams.getSummarizerParams().getSummarizationParams().getSchemaName();
		final String outputFolder = postgresExpParams.getSummarizerParams().getExportParams().getOutputFolder();
		final List<String> summaryTypes = postgresExpParams.getSummaryTypes();
		
		for (final String summaryType : summaryTypes) {
			// Set up
			log.info("SUMMARY TYPE: " + summaryType);
			long totalSumSatTime = 0;
			long totalSumSatSumTime = 0;
			for (int i = 1; i <= postgresExpParams.getNumberOfRuns(); i++) {
				final CompletenessRunResult runResult = new CompletenessRunResult(summaryType, schemaName, numberOfRuns, fetchSize);
				sumSatG(args, summaryType, timestamp, runResult);
				final BisimulationMapDbPostgresSummarizerResult summaryResult = sumG(args, summaryType, timestamp, runResult);
				sumSatSumG(args, summaryType, timestamp, runResult, summaryResult.getSummaryTriplesFilepath(), summaryResult.getSummaryName());
				
				totalSumSatTime += runResult.getTotalTimeToSumSatGInMilliseconds();
				totalSumSatSumTime += runResult.getTotalTimeToSumSatSumGInMilliseconds();

				log.info("Time to summarize G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeGInMilliseconds())));
				log.info("Time to saturate G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSaturateGInMilliseconds())));
				log.info("Time to summarize saturated G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeSaturatedGInMilliseconds())));
				log.info("Time to saturate summarized G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSaturateSummarizedGInMilliseconds())));
				log.info("Time to summarize saturated summary of G: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTimeToSummarizeSaturatedSummaryOfGInMilliseconds())));
				log.info("Total sum(sat(G)) time: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTotalTimeToSumSatGInMilliseconds())));
				log.info("Total sum(sat(sum(G))) time: " + Utils.appendSeconds(Utils.millisecondsToSeconds(runResult.getTotalTimeToSumSatSumGInMilliseconds())));
				
				exporter.exportCompletenessRunToTxt(runResult, outputFolder, timestamp, i);
			}
			
			double averageSumSatTime = (double) totalSumSatTime / numberOfRuns;
			double averageSumSatSumTime = (double) totalSumSatSumTime / numberOfRuns;
			final CompletenessResult completenessResult = new CompletenessResult(summaryType, datasetName, numberOfRuns, fetchSize, averageSumSatTime, averageSumSatSumTime);
			
			// Export
			exporter.exportCompletenessResultToTxt(completenessResult, outputFolder, timestamp);
		}
		
		expContext.close();
	}

	private static void sumSatG(final String[] args, final String summaryType, final String timestamp, final CompletenessRunResult runResult) throws ConfigurationException, ParametersException, SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException, QueryException, SignatureException {
		// Set up
		final MapDbPostgresExperimentsParams mapDbPostgresParams = summarizerConfigurator.loadExperimentsParameters(args);
		final PostgresSummarizationParams summarizationParams = mapDbPostgresParams.getPostgresExperimentsParams().getSummarizerParams().getSummarizationParams();	
		final PostgresExperimentsParams postgresParams = mapDbPostgresParams.getPostgresExperimentsParams(); 
		postgresParams.setExperimentTimestamp(timestamp);
		summarizationParams.setSummaryType(summaryType);
		summarizationParams.setSaturateInput(true); // saturation on
		summarizationParams.setRecomputeSaturation(true); // recompute saturation

		// Run
		final BisimulationMapDbPostgresSummarizerResult summaryResult = runSummarization(mapDbPostgresParams.getMapDbPostgresSummarizerParams());		
		
		// Result
		final Timings timings = summaryResult.getTimings();
		long satTime = timings.getTime(RdfLoaderTiming.SATURATION);
		long summTime = timings.getTime(SummaryTiming.SUMMARIZATION_TIME);
		runResult.setTimeToSaturateG(satTime);
		runResult.setTimeToSummarizeSaturatedG(summTime);	
	}

	private static BisimulationMapDbPostgresSummarizerResult sumG(final String[] args, final String summaryType, final String timestamp, final CompletenessRunResult runResult) throws ConfigurationException, SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException, QueryException, ParametersException, SignatureException {
		// Set up
		final MapDbPostgresExperimentsParams mapDbPostgresParams = summarizerConfigurator.loadExperimentsParameters(args);
		final PostgresSummarizerParams summarizerParams = mapDbPostgresParams.getPostgresExperimentsParams().getSummarizerParams();
		final PostgresSummarizationParams summarizationParams = mapDbPostgresParams.getPostgresExperimentsParams().getSummarizerParams().getSummarizationParams();	
		final PostgresExperimentsParams postgresParams = mapDbPostgresParams.getPostgresExperimentsParams(); 
		postgresParams.setExperimentTimestamp(timestamp);
		summarizationParams.setSummaryType(summaryType);
		summarizationParams.setSaturateInput(false); // saturation off
		summarizerParams.getExportParams().setExportToRdfFile(true); // export summary to RDF file
		
		// Run
		final BisimulationMapDbPostgresSummarizerResult summaryResult = runSummarization(mapDbPostgresParams.getMapDbPostgresSummarizerParams());
		
		// Result
		final Timings timings = summaryResult.getTimings();
		long summTime = timings.getTime(SummaryTiming.SUMMARIZATION_TIME);
		runResult.setTimeToSummarizeG(summTime);
		
		return summaryResult;
	}
	
	private static void sumSatSumG(final String[] args, final String summaryType, final String timestamp, final CompletenessRunResult runResult, 
			final String summaryTriplesFilepath, final String summaryName) throws ConfigurationException, ParametersException, SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException, QueryException, SignatureException {
		// Set up
		final MapDbPostgresExperimentsParams mapDbPostgresParams = summarizerConfigurator.loadExperimentsParameters(args);
		final PostgresSummarizationParams summarizationParams = mapDbPostgresParams.getPostgresExperimentsParams().getSummarizerParams().getSummarizationParams();	
		final PostgresExperimentsParams postgresParams = mapDbPostgresParams.getPostgresExperimentsParams(); 
		postgresParams.setExperimentTimestamp(timestamp);
		summarizationParams.setSummaryType(summaryType);
		summarizationParams.setSaturateInput(true); // saturation on
		summarizationParams.setRecomputeSaturation(true); // recompute saturation
		summarizationParams.setDataTriplesFilepath(summaryTriplesFilepath); // summary is the input graph
		summarizationParams.setSchemaName(NameUtils.getSchemaNameFromSummaryName(summaryName));
		summarizationParams.setDropSchema(true); // reload, recompute all
		
		// Run
		final BisimulationMapDbPostgresSummarizerResult summaryResult = runSummarization(mapDbPostgresParams.getMapDbPostgresSummarizerParams());
		
		// Result
		final Timings timings = summaryResult.getTimings();
		long summTime = timings.getTime(SummaryTiming.SUMMARIZATION_TIME);
		long satTime = timings.getTime(RdfLoaderTiming.SATURATION);
		runResult.setTimeToSaturateSummarizedG(satTime);
		runResult.setTimeToSummarizeSaturatedSummaryOfG(summTime);
	}
	
	private static BisimulationMapDbPostgresSummarizerResult runSummarization(final MapDbPostgresSummarizerParams mapDbPostgresSummarizerParams) throws ConfigurationException, SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException, QueryException, SignatureException {
		final BisimulationMapDbPostgresRunner runner = new BisimulationMapDbPostgresRunner();
		
		return runner.summarize(mapDbPostgresSummarizerParams);
	}
}
