package fr.inria.oak.RDFSummary.main.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class JdbcTest {

	private static AnnotationConfigApplicationContext Context;
	private static RdfTablesDao RdfTablesDao;
	private static SqlConnectionHandler ConnHandler;

	/**
	 * @param rdfTablesDao
	 * @param connHandler
	 */
	public JdbcTest(RdfTablesDao rdfTablesDao, SqlConnectionHandler connHandler) {
		RdfTablesDao = rdfTablesDao;
		ConnHandler = connHandler;
	}

	public static void main(String[] args) throws ConfigurationException, SQLException, ParametersException {
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresSummarizerParams params = configurator.loadPostgresSummarizerParameters(args);	
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		ConnHandler.createStatement(params.getSummarizationParams().getFetchSize(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
//		Postgres.getSpoResultSet("bsbm30m.g_enc_data");
		RdfTablesDao.exportTableToFile("bsbm30m.g_enc_data", new String[] { "s", "p", "o" }, "/Users/Shared");
		Context.close();
	}

}
