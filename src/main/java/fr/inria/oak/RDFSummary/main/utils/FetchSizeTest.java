package fr.inria.oak.RDFSummary.main.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.beust.jcommander.internal.Lists;
import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.PostgresSummaryResult;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class FetchSizeTest {

	private static AnnotationConfigApplicationContext Context = null;

	public static void main(String[] args) throws ConfigurationException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresSummarizerParams sumParams = configurator.loadPostgresSummarizerParameters(args);	
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, sumParams);
		TroveCliquePostgresSummarizer summarizer = Context.getBean(TroveCliquePostgresSummarizer.class);

		// Run
		PostgresSummaryResult summaryResult = null;
		List<String> datasets = Lists.newArrayList("bsbm1m", "bsbm10m", "bsbm15m", "bsbm20m", "bsbm30m");
		List<Integer> fetchSizes = Lists.newArrayList(0, 10, 50, 100, 500, 1000, 10000, 30000, 50000, 100000, 1000000, 10000000);
		PrintWriter writer = new PrintWriter(new FileWriter(Utils.getFilepath(sumParams.getExportParams().getOutputFolder(), "bsbm_fetch_size", "txt")));
		writer.printf("%-15s %-15s %-15s", "Dataset", "Fetch size", "Summarization time");
		for (String dataset : datasets) {
			sumParams.getSummarizationParams().setSchemaName(dataset);;
			sumParams.getSummarizationParams().setDataTriplesFilepath(getBsbmFilepath(dataset));
			sumParams.getSummarizationParams().setDropSchema(false);
			for (int fetchSize : fetchSizes) {
				sumParams.getSummarizationParams().setFetchSize(fetchSize);
				summaryResult = summarizer.summarize(sumParams);
				writer.printf("\n%-15s %-15s %-15s", sumParams.getSummarizationParams().getSchemaName(), 
						fetchSize, summaryResult.getStatistics().getSummarizationTime());
			}
		}

		// Clean up
		Context.close();
		writer.close();

	}

	public static String getBsbmFilepath(final String datasetName) {
		return "/Users/sejla/Documents/datasets/bsbm/" + datasetName + ".nt";
	}

	public static String getEncodedDataTable(final String datasetName) {
		return datasetName + ".g_enc_data";
	}

}
