package fr.inria.oak.RDFSummary.main;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.BisimulationMapDbPostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresSummarizerParams;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationMapDbPostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationMapDbPostgresSummarizerResult;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationMapDbPostgresRunner {

	private static AnnotationConfigApplicationContext Context = null;

	public BisimulationMapDbPostgresSummarizerResult summarize(final MapDbPostgresSummarizerParams params) throws ConfigurationException, SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException, QueryException, SignatureException {
		// Setup
		BisimulationMapDbPostgresConfigurator configurator = new BisimulationMapDbPostgresConfigurator();
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		final BisimulationMapDbPostgresSummarizer summarizer = Context.getBean(BisimulationMapDbPostgresSummarizer.class);
		BisimulationMapDbPostgresSummarizerResult result = summarizer.run();

		// Close context
		Context.close();
		
		return result;
	}

	public static void main(String[] args) throws Exception {
		// Setup
		BisimulationMapDbPostgresConfigurator configurator = new BisimulationMapDbPostgresConfigurator();		
		final MapDbPostgresSummarizerParams params = configurator.loadMapDbPostgresSummarizerParameters(args); 
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		// Run
		final BisimulationMapDbPostgresSummarizer summarizer = Context.getBean(BisimulationMapDbPostgresSummarizer.class);
		summarizer.run();

		// Close context
		Context.close();
	}
}
