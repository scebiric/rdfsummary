package fr.inria.oak.RDFSummary.main.utils;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.SchemaParser;
import fr.inria.oak.commons.rdfconjunctivequery.IRI;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;

/**
 * Loading the RDF/XML schema file from the filepath specified in the 
 * configuration file and exporting to an N-triples file.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SchemaConversion {

	private static Logger log = Logger.getLogger(SchemaConversion.class);
	private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();; 

	public static void main(String[] args) throws ConfigurationException, SchemaException, IOException, ParametersException {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();
		PostgresSummarizerParams params = configurator.loadPostgresSummarizerParameters(args);
		configurator.javaSetUpApplicationContext(context, params);

		Preconditions.checkArgument(!StringUtils.isNullOrBlank(params.getSummarizationParams().getSchemaTriplesFilepath()), "RDF Schema filepath is required.");
		Timer timer = context.getBean(Timer.class);
		SchemaParser schemaParser = context.getBean(SchemaParser.class);
		TriplesWriter triplesWriter = context.getBean(TriplesWriter.class);
		timer.start();
		Schema schema = schemaParser.parseSchema(params.getSummarizationParams().getSchemaTriplesFilepath(), null, RdfUtils.getRDFLanguage(params.getSummarizationParams().getSchemaTriplesFilepath()));
		log.info("Retrieving all RDFS constraints...");
		Set<Triple> rdfsTriples = schema.getAllTriples();
		if (rdfsTriples != null) {
			log.info("Writing the RDFS constraints to N-triples file...");
			final String filepath = Utils.getFilepath(params.getExportParams().getOutputFolder(), "ntriples-schema", RdfFormat.N_TRIPLES); 
			triplesWriter.startFile(filepath);

			for (fr.inria.oak.commons.reasoning.rdfs.saturation.Triple triple : rdfsTriples)
				triplesWriter.write(((IRI) triple.subject).toString(), ((IRI) triple.property).toString(), ((IRI) triple.object).toString());

			triplesWriter.endFile();
			log.info("N-triples schema written to file: " + filepath);
		}			
		timer.stop();

		log.info("Schema RDF/XML to N-triples conversion done in " + timer.getTimeAsString());

		// Close application context
		context.close();
	}
}
