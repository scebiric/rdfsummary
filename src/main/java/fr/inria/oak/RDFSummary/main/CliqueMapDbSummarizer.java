package fr.inria.oak.RDFSummary.main;

import java.io.IOException;
import java.sql.SQLException;
import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.CliqueMapDbConfigurator;
import fr.inria.oak.RDFSummary.config.params.DiskSummarizerParams;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.Saturator;
import fr.inria.oak.RDFSummary.rdf.loader.RdfLoader;
import fr.inria.oak.RDFSummary.rdf.loader.schema.SchemaLoader;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.export.RdfExporter;
import fr.inria.oak.RDFSummary.summary.summarizer.disk.CliqueMapDbSummaryResult;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class CliqueMapDbSummarizer {

	private static AnnotationConfigApplicationContext Context = null;

	public CliqueMapDbSummaryResult summarize(final DiskSummarizerParams params) throws ConfigurationException, SQLException, IOException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, SignatureException {
		// Setup
		CliqueMapDbConfigurator configurator = new CliqueMapDbConfigurator();
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		// Run		
		CliqueMapDbSummaryResult result = run(params, Context);

		// Close context
		closeApplicationContext();

		return result;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// Setup
		CliqueMapDbConfigurator configurator = new CliqueMapDbConfigurator();		
		DiskSummarizerParams params = configurator.loadDiskSummarizerParameters(args); 
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		// Run
		run(params, Context);

		// Close context
		closeApplicationContext();
	}

	private static CliqueMapDbSummaryResult run(final DiskSummarizerParams params, final ApplicationContext context) throws SQLException, IOException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, SignatureException {		
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(context);

		// Load dataset
		final RdfLoader rdfLoader = Context.getBean(RdfLoader.class);
		rdfLoader.loadDataset();
		// Load schema
		if (!StringUtils.isNullOrBlank(params.getSummarizationParams().getSchemaTriplesFilepath())) {
			final SchemaLoader schemaLoader = Context.getBean(SchemaLoader.class);
			schemaLoader.loadSchemaFromFile();
		}	

		if (params.getSummarizationParams().saturateInput()) {
			final Saturator saturator = Context.getBean(Saturator.class);
			saturator.saturate(params.getSummarizationParams().getSaturationBatchSize());
		}

		// Summarize
		final fr.inria.oak.RDFSummary.summary.summarizer.BisimulationSummarizer summarizer = Context.getBean(fr.inria.oak.RDFSummary.summary.summarizer.BisimulationSummarizer.class);
		summarizer.summarize();

		// Export
		final RdfExporter exporter = Context.getBean(RdfExporter.class);
		exporter.exportToFiles(params.getExportParams(), params.getSummarizationParams().getDatasetName(), params.getSummarizationParams().getSummaryName());

		final MapDbManager dbManager = Context.getBean(MapDbManager.class);
		dbManager.commitAll();
		dbManager.closeAll();

		return null;
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}
}
