package fr.inria.oak.RDFSummary.main;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.cleaner.Cleaner;
import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.DataSourceName;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.DatasetPreparationResult;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummaryResult;
import fr.inria.oak.RDFSummary.summary.export.TroveSummaryExporter;
import fr.inria.oak.RDFSummary.summary.stats.clique.CliqueTrovePostgresStatsCollector;
import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.SummarizerUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Main class for running RDF graph summarization.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveCliquePostgresSummarizer {

	private static final Logger log = Logger.getLogger(TroveCliquePostgresSummarizer.class);
	private static AnnotationConfigApplicationContext Context = null;

	public TroveCliquePostgresSummaryResult summarize(PostgresSummarizerParams params) throws ConfigurationException, SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		// Run		
		TroveCliquePostgresSummaryResult result = run(params, Context);

		// Close context
		closeApplicationContext();

		return result;
	}
	
	public static void main(String[] args) throws Exception {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresSummarizerParams params = configurator.loadPostgresSummarizerParameters(args); 
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);

		// Run
		run(params, Context);

		// Close context
		closeApplicationContext();
	}

	private static TroveCliquePostgresSummaryResult run(final PostgresSummarizerParams params, final ApplicationContext context) throws SQLException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ConfigurationException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {		
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(context);
		Preconditions.checkArgument(params.getConnectionParams().getDataSourceName().equals(DataSourceName.POSTGRES));

		final DatasetPreparationResult datasetPreparationResult = SummarizerUtils.loadDatasetToPostgres(context, params);
		
		// Summarize
		fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummarizer summarizer = context.getBean(fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummarizer.class);
		final PostgresTableNames storageLayout = datasetPreparationResult.getPostgresTableNames();
		final String encodedDataTable = storageLayout.getEncodedDataTable();
		final String encodedTypesTable = storageLayout.getEncodedTypesTable();
		final String encodedSchemaTable = storageLayout.getEncodedSchemaTable();
		TroveCliquePostgresSummaryResult summaryResult = summarizer.summarize(params.getSummarizationParams().getSummaryType(), encodedDataTable, encodedTypesTable, encodedSchemaTable);
		log.info("Summarization time: " + summaryResult.getPerformanceResult().getTiming(PerformanceLabel.SUMMARIZATION_TIME) + Constant.MILLISECONDS);		

		String summaryName = NameUtils.getPostgresSummaryName(storageLayout.getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		summaryResult.setSummaryName(summaryName);
		summaryResult.setStorageLayout(storageLayout);
		summaryResult.setSchema(datasetPreparationResult.getSchema());
		summaryResult.setDatasetPreparationResult(datasetPreparationResult);

		// Export summary to Postgres
		TroveSummaryExporter troveExporter = context.getBean(TroveSummaryExporter.class);
		troveExporter.exportRdfSummaryToPostgres(summaryResult.getSummary(), params, summaryResult.getStorageLayout());

		// Create output folder if it doesn't exist
		File file = new File(params.getExportParams().getOutputFolder());
		if (!file.exists()) {
			file.mkdirs();
			log.info("Output folder created: " + params.getExportParams().getOutputFolder());
		}

		// Collect stats
		CliqueTrovePostgresStatsCollector statsCollector = context.getBean(CliqueTrovePostgresStatsCollector.class);
		Stats stats = statsCollector.collectStats(summaryResult.getSummary(), 
				storageLayout.getEncodedDataTable(), storageLayout.getEncodedTypesTable(), 
				storageLayout.getEncodedSummaryDataTable(), storageLayout.getEncodedSummaryTypesTable(),
				summaryResult.getClassNodesTable(), summaryResult.getPropertyNodesTable(), 
				datasetPreparationResult.getSchema(), summaryResult.getPerformanceResult().getTiming(PerformanceLabel.SUMMARIZATION_TIME));
		summaryResult.setStatistics(stats);

		// Export decoded triples to files
		if (params.getExportParams().generateDot() || params.getExportParams().exportToRdfFile()) {
			String dotFilepath = Utils.getFilepath(params.getExportParams().getOutputFolder(), summaryName, Extension.DOT);
			String rdfFilepath = Utils.getFilepath(params.getExportParams().getOutputFolder(), summaryName, RdfUtils.getFileExtensionByRDFFormat(RdfFormat.N_TRIPLES));
			troveExporter.exportDecodedTriplesToFiles(params, summaryResult.getSummary(), summaryResult.getSchema(), stats, storageLayout, 
					dotFilepath, summaryName, rdfFilepath);
			if (params.getExportParams().exportToRdfFile())
				summaryResult.setSummaryDataFilepath(rdfFilepath);
		}	

		// Write stats to file
		if (params.getExportParams().exportStatsToFile()) {
			String statsOutputFilename = NameUtils.getStatsFilename(summaryName);
			Utils.writeStatsToFile(params.getExportParams().getOutputFolder(), statsOutputFilename, summaryResult.getStorageLayout(), 
					summaryResult.getStatistics(), datasetPreparationResult.getTriplesLoadingTimes(), datasetPreparationResult.getEncodingTimes());
		}	

		// Release resources
		Cleaner cleaner = context.getBean(Cleaner.class);
		cleaner.releaseResources();

		return summaryResult;
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}
}
