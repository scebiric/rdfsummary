package fr.inria.oak.RDFSummary.main.utils;

import java.sql.SQLException;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;

/**
 * Quick dropping of Postgres schemas
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DropSchemas {

	private static Logger log = Logger.getLogger(DropTables.class);
	private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();; 

	public static void main(String[] args) throws ConfigurationException, SQLException, ParametersException {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresSummarizerParams params = configurator.loadPostgresSummarizerParameters(args);
		configurator.javaSetUpApplicationContext(context, params);
		DmlDao dmlDao = context.getBean(DmlDao.class);
		DdlDao ddlDao = context.getBean(DdlDao.class);
				
		List<String> schemas = dmlDao.getSchemasContainingString("test");
		for (String schema : schemas)
			dropSchema(schema, ddlDao);		

		// Close application context
		context.close();
	}

	private static void dropSchema(String schemaName, DdlDao ddlDao) throws SQLException {		
		ddlDao.dropSchema(schemaName);
		log.info("Dropped schema: " + schemaName);
	}

}
