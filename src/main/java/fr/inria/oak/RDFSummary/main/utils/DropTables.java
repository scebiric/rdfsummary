package fr.inria.oak.RDFSummary.main.utils;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.CliqueTrovePostgresConfigurator;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.util.NameUtils;

/**
 * Quick dropping of Postgres tables
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DropTables {

	private static Logger log = Logger.getLogger(DropTables.class);
	private static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();; 

	private static DmlDao dmlDao;
	private static DdlDao ddlDao;
	
	public static void main(String[] args) throws ConfigurationException, SQLException, ParametersException {
		// Setup
		CliqueTrovePostgresConfigurator configurator = new CliqueTrovePostgresConfigurator();		
		PostgresSummarizerParams params = configurator.loadPostgresSummarizerParameters(args);
		configurator.javaSetUpApplicationContext(context, params);
		dmlDao = context.getBean(DmlDao.class);
		ddlDao = context.getBean(DdlDao.class);
		
		final String schemaName = "demo";
		dropTables(schemaName, "g_enc_data_");
		dropTables(schemaName, "%pairs");
		dropTables(schemaName, "%tuples");
		dropTables(schemaName, "%sum");
		dropTables(schemaName, "%psd");
		dropTables(schemaName, "%pst");
						
		// Close application context
		context.close();
	}

	private static void dropTables(String schemaName, String str) throws SQLException {
		List<String> tables = dmlDao.getTablesByPrefix(schemaName, str, 0);
		for (String table : tables) { 
			String qualifiedTableName = NameUtils.qualify(schemaName, table);			
			ddlDao.dropTable(qualifiedTableName);
			log.info("Dropped table: " + qualifiedTableName);
		}
	}
}
