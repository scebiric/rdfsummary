package fr.inria.oak.RDFSummary.performancelogger;

import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PerformanceLoggerImpl implements PerformanceLogger {

	private static Map<String, Long> timingMap;
	private static Map<String, String> queriesMap;
	private static Timer summarizationTimer;
	
	/**
	 * @param timer
	 */
	public PerformanceLoggerImpl(final Timer timer) {
		summarizationTimer = timer;
		timingMap = Maps.newLinkedHashMap();
		queriesMap = Maps.newHashMap();
	}

	@Override
	public void reset() {
		timingMap.clear();
		queriesMap.clear();
		summarizationTimer.reset();		
	}

	@Override
	public void startSummarizationTimer() {
		summarizationTimer.start();
	}

	@Override
	public void stopSummarizationTimer() {
		summarizationTimer.stop();
	}

	@Override
	public PerformanceResult getPerformanceResult() {
		addTiming(PerformanceLabel.SUMMARIZATION_TIME, summarizationTimer.getTimeInMilliseconds());
		
		return new PerformanceResult(timingMap, queriesMap);
	}

	@Override
	public void addTiming(final String label, final long value) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(label));
		Preconditions.checkArgument(value >= 0);
		
		timingMap.put(label, value);
	}
	
	@Override
	public void addQuery(final String label, final String query) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(label));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(query));
		
		queriesMap.put(label, query);
	}

	@Override
	public Timer newTimer() {
		return new TimerImpl();
	}
}
