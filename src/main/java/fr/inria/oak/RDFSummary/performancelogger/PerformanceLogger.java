package fr.inria.oak.RDFSummary.performancelogger;

import fr.inria.oak.RDFSummary.timer.Timer;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PerformanceLogger {

	public void addTiming(final String label, final long value);
	
	public void addQuery(final String label, final String query);
	
	public PerformanceResult getPerformanceResult();   
	
	public void reset();

	public void startSummarizationTimer();

	public void stopSummarizationTimer();

	public Timer newTimer(); 
}
