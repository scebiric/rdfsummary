package fr.inria.oak.RDFSummary.writer;

import java.io.IOException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TriplesWriter {

	/**
	 * @param rdfFilepath
	 * @throws IOException
	 */
	public void startFile(final String rdfFilepath) throws IOException;
	
	public void endFile();
	
	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void write(String subject, String property, String object);
}
