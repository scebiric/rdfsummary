package fr.inria.oak.RDFSummary.writer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesWriterImpl implements TriplesWriter {

	private static PrintWriter PrintWriter = null; 
	private static final String TRIPLE_FORMAT = "%s %s %s .\n";
	
	@Override
	public void startFile(final String rdfFilepath) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(rdfFilepath));

		if (PrintWriter != null)
			throw new IOException("Another triples file has already been started. Invoke endFile.");
		
		PrintWriter = new PrintWriter(new FileWriter(rdfFilepath));
	}
	
	@Override
	public void write(String subject, String property, String object) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(subject));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(property));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(object));
		
		PrintWriter.printf(TRIPLE_FORMAT, subject, property, object);
	}
	
	@Override
	public void endFile() {
		PrintWriter.close(); 
		PrintWriter = null;
	}
}
