package fr.inria.oak.RDFSummary.cleaner;

import java.sql.SQLException;

/**
 * Releases resources such as database connections, statements etc.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Cleaner {

	public void releaseResources() throws SQLException;
}
