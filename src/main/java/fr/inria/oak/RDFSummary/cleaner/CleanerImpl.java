package fr.inria.oak.RDFSummary.cleaner;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDao;

/**
 * Implements {@link Cleaner}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class CleanerImpl implements Cleaner {

	private final SqlConnectionHandler connHandler;
	private final RdfTablesDao rdfTablesDao;
	private final DictionaryDao dictDao;
	private final SaturatorDao saturatorDao;
	
	/**
	 * @param connHandler
	 * @param ddlDao
	 * @param dictDao
	 * @param dmlDao
	 * @param rdfTablesDao
	 * @param saturatorDao
	 */
	public CleanerImpl(final SqlConnectionHandler connHandler, final DdlDao ddlDao, final DictionaryDao dictDao,
			final DmlDao dmlDao, final RdfTablesDao rdfTablesDao, final SaturatorDao saturatorDao) {
		this.connHandler = connHandler;
		this.rdfTablesDao = rdfTablesDao;
		this.dictDao = dictDao;
		this.saturatorDao = saturatorDao;
	}

	@Override
	public void releaseResources() throws SQLException {
		rdfTablesDao.releaseResources();
		dictDao.releaseResources();
		saturatorDao.releaseResources();
		connHandler.closeConnection();
	}
}
