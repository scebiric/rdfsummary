package fr.inria.oak.RDFSummary.rdf.loader.schema;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface SchemaLoader {

	/** 
	 * @throws SchemaException 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public void loadSchemaFromFile() throws FileNotFoundException, SchemaException, IOException, SQLException;
}
