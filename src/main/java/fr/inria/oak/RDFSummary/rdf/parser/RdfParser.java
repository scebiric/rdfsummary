package fr.inria.oak.RDFSummary.rdf.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfParser {

	/**
	 * Parses n-triples RDF files
	 * 
	 * @param dataFilepath
	 * @return Parsing time
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public long parse(final String dataFilepath) throws SQLException, FileNotFoundException, IOException;
}
