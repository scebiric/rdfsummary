package fr.inria.oak.RDFSummary.rdf.dataset.bisimulation;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.mapdb.DB;
import org.mapdb.Serializer;
import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.MapDbSchemaImpl;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresBisimulationRdfDatasetImpl implements PostgresBisimulationRdfDataset {

	private final TriplesDao triplesDao;

	private final Set<Integer> classes;
	private final Set<Integer> properties;
	private final Set<Integer> dataNodes;
	
	private final Schema schema;
	private final Map<String, Integer> rdfPropertyKeys;
	
	/**
	 * @param datasetDb
	 * @param classesSetName
	 * @param propertiesSetName
	 * @param dataNodesSetName
	 * @param triplesDao
	 */
	public PostgresBisimulationRdfDatasetImpl(final DB datasetDb, final String classesSetName, 
			final String propertiesSetName, final String dataNodesSetName, final TriplesDao triplesDao) {
		this.triplesDao = triplesDao;
		
		this.classes = datasetDb.createHashSet(classesSetName).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.properties = datasetDb.createHashSet(propertiesSetName).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.dataNodes = datasetDb.createHashSet(dataNodesSetName).counterEnable().serializer(Serializer.INTEGER).makeOrGet();		
		this.schema = new MapDbSchemaImpl(datasetDb);
		this.rdfPropertyKeys = Maps.newHashMap(); 
	}

	@Override
	public void addClass(final int classIRI) {
		classes.add(classIRI);
	}

	@Override
	public Iterator<Integer> getClasses() {
		return classes.iterator();
	}

	@Override
	public boolean isLoaded() throws SQLException {
		return triplesDao.getDataTriplesCount() > 0 || triplesDao.getTypesTriplesCount() > 0;
	}

	@Override
	public void addDataNode(final int dataNode) {
		dataNodes.add(dataNode);
	}

	@Override
	public Iterator<Integer> getDataNodes() {
		return dataNodes.iterator();
	}
	
	@Override
	public void addTypeTriple(final int subject, final int object) throws SQLException {
		triplesDao.insertTypeTriple(subject, object); 		
	}

	@Override
	public Schema getSchema() {
		return schema;
	}

	@Override
	public void addRdfProperty(String rdfProperty, int key) {
		rdfPropertyKeys.put(rdfProperty, key);
	}

	@Override
	public boolean isTypeProperty(int property) {
		return property == rdfPropertyKeys.get(Rdf.FULL_TYPE).intValue() || property == rdfPropertyKeys.get(Rdf.TYPE).intValue();
	}

	@Override
	public boolean isDomainProperty(int property) {
		return property == rdfPropertyKeys.get(Rdfs.FULL_DOMAIN).intValue()  || property == rdfPropertyKeys.get(Rdfs.DOMAIN).intValue();
	}

	@Override
	public boolean isRangeProperty(int property) {
		return property == rdfPropertyKeys.get(Rdfs.FULL_RANGE).intValue()  || property == rdfPropertyKeys.get(Rdfs.RANGE).intValue() ;
	}

	@Override
	public boolean isSubClassOfProperty(int property) {
		return property == rdfPropertyKeys.get(Rdfs.FULL_SUBCLASS).intValue()  || property == rdfPropertyKeys.get(Rdfs.SUBCLASS).intValue() ;
	}

	@Override
	public boolean isSubPropertyOfProperty(int property) {
		return property == rdfPropertyKeys.get(Rdfs.FULL_SUBPROPERTY).intValue()  || property == rdfPropertyKeys.get(Rdfs.SUBPROPERTY).intValue() ;
	}

	@Override
	public void addSchemaTriple(int subject, int property, int object) {
		if (isDomainProperty(property))
			addDomainClassByProperty(subject, object, property);
		
		else if (isRangeProperty(property))
			addRangeClassByProperty(subject, object, property);
		
		else if (isSubClassOfProperty(property))
			addSuperClassByClass(subject, object, property);
		
		else if (isSubPropertyOfProperty(property))
			addSuperPropertyByProperty(subject, object, property);
	}

	@Override
	public boolean isSchemaProperty(int property) {
		return isDomainProperty(property) || isRangeProperty(property) || isSubClassOfProperty(property) || isSubPropertyOfProperty(property);
	}

	@Override
	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty) {
		schema.addDomainClassByProperty(property, domain, rdfDomainProperty);
		addProperty(property);
		addClass(domain);
	}

	@Override
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty) {
		schema.addRangeClassByProperty(property, range, rdfRangeProperty);
		addProperty(property);
		addClass(range);
	}

	@Override
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty) {
		schema.addSuperClassByClass(classIRI, superClassIRI, rdfSubClassOfProperty);
		addClass(classIRI);
		addClass(superClassIRI);
	}

	@Override
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty) {
		schema.addSuperPropertyByProperty(property, superProperty, rdfSubPropertyOfProperty);
		addProperty(property);
		addProperty(superProperty);
	}

	@Override
	public int getClassesCount() {
		return classes.size();
	}
	
	@Override
	public int getDataNodeCount() {
		return dataNodes.size();
	}

	@Override
	public void addProperty(int property) {
		properties.add(property);	
	}

	@Override
	public Iterator<Integer> getProperties() {
		return properties.iterator();
	}

	@Override
	public boolean isClass(int iri) {
		return classes.contains(iri);
	}

	@Override
	public boolean isProperty(int iri) {
		return properties.contains(iri);
	}

	@Override
	public boolean isClassOrProperty(int iri) {
		return isClass(iri) || isProperty(iri);
	}

	@Override
	public Map<String, Integer> getRdfPropertyKeys() {
		return rdfPropertyKeys;
	}

	@Deprecated
	@Override
	public void addDataTriple(int subject, int property, int object) throws SQLException {
		triplesDao.insertDataTriple(subject, property, object);
	}

	@Override
	public ResultSet getPropertyObjectPairsBySubject(int gDataNode) throws SQLException {
		return triplesDao.getDataPropertyObjectPairsBySubject(gDataNode);
	}

	@Override
	public ResultSet getPropertySubjectPairsByObject(int gDataNode) throws SQLException {
		return triplesDao.getDataPropertySubjectPairsByObject(gDataNode);
	}

	@Override
	public ResultSet getTypeTriples() throws SQLException {
		return triplesDao.getTypeSubjectObjectPairs();
	}

	@Override
	public ResultSet getDataTriples() throws SQLException {
		return triplesDao.getDataTriples();
	}
	
	@Override
	public int getPropertiesCount() {
		return properties.size();
	}
}
