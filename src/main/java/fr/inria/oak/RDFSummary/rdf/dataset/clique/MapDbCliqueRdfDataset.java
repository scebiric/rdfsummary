package fr.inria.oak.RDFSummary.rdf.dataset.clique;

import java.util.Iterator;

import org.mapdb.Fun.Tuple3;

import fr.inria.oak.RDFSummary.rdf.dataset.MapDbRdfDataset;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface MapDbCliqueRdfDataset extends CliqueRdfDataset, MapDbRdfDataset {

	/**
	 * @param subject
	 * @param property
	 */
	public void addDataPropertyBySubject(int subject, int property);

	/**
	 * @param object
	 * @param property
	 */
	public void addDataPropertyByObject(int object, int property);
	
	/**
	 * @param subject
	 * @return An iterator over the set of data properties for the specified subject
	 */
	public Iterator<Integer> getDataPropertiesBySubject(int subject);

	/**
	 * @param object
	 * @return An iterator over the set of data properties for the specified object
	 */
	public Iterator<Integer> getDataPropertiesByObject(int object);
	
	/**
	 * @return An iterator over data triples
	 */
	public Iterator<Tuple3<Integer, Integer, Integer>> getDataTriples();
}
