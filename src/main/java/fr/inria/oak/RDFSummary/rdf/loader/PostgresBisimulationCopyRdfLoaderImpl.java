package fr.inria.oak.RDFSummary.rdf.loader;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.mapdb.Fun.Tuple2;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.config.params.SaturationParams;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.data.encoding.Encoding;
import fr.inria.oak.RDFSummary.data.index.Index;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturator;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.data.storage.splitter.StorageSplitter;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.rdf.loader.schema.PostgresSchemaLoader;
import fr.inria.oak.RDFSummary.rdf.parser.RdfParser;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;
import fr.inria.oak.RDFSummary.summary.export.RdfExporter;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timing.RdfLoaderTiming;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresBisimulationCopyRdfLoaderImpl implements RdfLoader {

	private static final Logger log = Logger.getLogger(RdfLoader.class);
	private final MessageBuilder messageBuilder;

	private final String dataTriplesFilepath;
	private final boolean dropSchema;
	private final String schemaName;
	private final PostgresTableNames tables;
	private final ConnectionParams connParams;
	private final SaturationParams saturationParams;

	private final PostgresBisimulationRdfDataset rdfDataset;
	private final boolean exportInput;
	private final RdfExporter datasetExporter;
	private final ExportParams exportParams;
	private final DdlDao ddlDao;
	private final DmlDao dmlDao;
	private final DictionaryDao dictDao;
	private final TriplesDao triplesDao;
	private final RdfParser rdfParser;
	private final Encoding encoding;
	private final PostgresSchemaLoader schemaLoader;
	private final PostgresSaturator postgresSaturator;
	private final StorageSplitter storageSplitter;
	private final Index index;
	private final Timer timer;
	private final Timings timings;

	private Tuple2<Integer, Integer> tuple;

	private static final int PREDEFINED_PROPS_COUNT = 10; // full and short props: 2 x (rdf type + 4 rdfs properties) 

	/**
	 * @param dataFilepath
	 * @param dropSchema
	 * @param schemaName
	 * @param tables
	 * @param connParams
	 * @param saturationParams
	 * @param rdfDataset
	 * @param exportInput
	 * @param datasetExporter
	 * @param exportParams
	 * @param ddlDao
	 * @param dmlDao
	 * @param dictDao
	 * @param triplesDao
	 * @param rdfParser
	 * @param encoding
	 * @param schemaLoader
	 * @param postgresSaturator
	 * @param storageSplitter
	 * @param index
	 * @param timer
	 * @param timings
	 */
	public PostgresBisimulationCopyRdfLoaderImpl(final String dataFilepath, final boolean dropSchema, final String schemaName, 
			final PostgresTableNames tables, final ConnectionParams connParams, final SaturationParams saturationParams,
			final PostgresBisimulationRdfDataset rdfDataset, final boolean exportInput, final RdfExporter datasetExporter, final ExportParams exportParams,
			final DdlDao ddlDao, final DmlDao dmlDao, final DictionaryDao dictDao, final TriplesDao triplesDao,
			final RdfParser rdfParser, final Encoding encoding, final PostgresSchemaLoader schemaLoader, 
			final PostgresSaturator postgresSaturator, final StorageSplitter storageSplitter, final Index index, 
			final Timer timer, final Timings timings) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataFilepath));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tables.getTriplesTable()));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tables.getEncodedTriplesTable()));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tables.getDictionaryTable()));

		this.dataTriplesFilepath = dataFilepath;
		this.dropSchema = dropSchema;
		this.schemaName = schemaName;
		this.tables = tables;
		this.connParams = connParams;
		this.saturationParams = saturationParams;

		this.rdfDataset = rdfDataset;
		this.exportInput = exportInput;
		this.datasetExporter = datasetExporter;
		this.exportParams = exportParams;
		this.ddlDao = ddlDao;
		this.dmlDao = dmlDao;
		this.dictDao = dictDao;
		this.triplesDao = triplesDao;
		this.rdfParser = rdfParser;
		this.encoding = encoding;
		this.schemaLoader = schemaLoader;
		this.postgresSaturator = postgresSaturator;
		this.storageSplitter = storageSplitter;
		this.index = index;
		this.timer = timer;
		this.timings = timings;

		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void loadDataset() throws IOException, SQLException, UnsupportedDatabaseEngineException, DictionaryException, SchemaException {
		timer.reset();
		log.info(messageBuilder.append("Loading RDF to Postgres from: ").append(dataTriplesFilepath).append(Constant.THREE_DOTS));

		if (dropSchema) 
			ddlDao.dropSchema(schemaName);

		timer.start();
		// Load data+type triples (non-encoded)
		if (dmlDao.existsTable(tables.getTriplesTable())) 
			log.info(tables.getTriplesTable() + " table already exists.");			
		else {
			final long dataAndTypesLoadingTime = rdfParser.parse(dataTriplesFilepath);
			timings.addTime(RdfLoaderTiming.LOADING_TRIPLES_FROM_FILE, dataAndTypesLoadingTime);
		}

		// Encode data and types
		if (dmlDao.existsTable(tables.getDictionaryTable())) 
			log.info("Data already encoded.");
		else {
			final long dataAndTypesEncodingTime = encoding.encodeDataAndTypes(connParams, tables.getTriplesTable(), tables.getEncodedTriplesTable(), tables.getDictionaryTable());
			timings.addTime(RdfLoaderTiming.ENCODING_DATASET_TRIPLES, dataAndTypesEncodingTime);
		}

		// Load schema
		schemaLoader.loadSchemaFromFile();
		schemaLoader.loadSchemaFromTriplesTable();

		// Saturate
		if (saturationParams.saturatedInput()) {
			final String saturatedEncodedTriplesTable = NameUtils.getSaturatedEncodedTriplesTableName(tables.getEncodedTriplesTable());
			final TroveSchema troveSchema = toTroveSchema(rdfDataset.getSchema());
			final Integer fullRdfTypeKey = dictDao.getExistingKey(Rdf.FULL_TYPE);
			final Integer shortRdfTypeKey = dictDao.getExistingKey(Rdf.TYPE);
			long satTime = postgresSaturator.saturate(saturatedEncodedTriplesTable, tables.getEncodedTriplesTable(), 
					saturationParams.recomputeSaturation(), troveSchema, fullRdfTypeKey, shortRdfTypeKey, saturationParams.getBatchSize());
			tables.setEncodedTriplesTable(saturatedEncodedTriplesTable);
			timings.addTime(RdfLoaderTiming.SATURATION, satTime);
		}

		// Load dictionary entries for rdf:type and RDFS properties to memory
		final Dictionary dictionary = dictDao.loadDictionaryForTypeAndRdfs();
		RdfLoaderUtils.loadRdfPropertiesWithBrackets();

		// Split storage		
		log.info("Splitting the encoded triples table...");		
		String createEncodedTypesTableTime = storageSplitter.createEncodedTypesTable(tables.getEncodedTypesTable(), tables, tables.getDictionaryTable());
		String createEncodedDataTableTime = storageSplitter.createEncodedDataTable(tables.getEncodedDataTable(), tables, dictionary);
		log.info("Type triples extracted in: " + createEncodedTypesTableTime);
		log.info("Data triples extracted in: " + createEncodedDataTableTime);

		// Create indexes
		index.createIndexes();		

		// Load classes, data properties and data nodes
		loadDataClasses();
		loadDataProperties();
		loadDataNodes();

		timer.stop();
		log.info(messageBuilder.append("Dataset loaded in: ").append(timer.getTimeAsString()));
		timings.addTime(RdfLoaderTiming.TOTAL_DATASET_LOADING, timer.getTimeInMilliseconds());

		if (exportInput) {
			triplesDao.prepareDataSelectStatements();
			triplesDao.prepareTypesSelectStatements();
			datasetExporter.exportToFiles(exportParams, schemaName, tables.getEncodedTriplesTable());
			triplesDao.closeDataSelectStatements();
			triplesDao.closeTypesSelectStatements();
		}
	}

	private void loadDataClasses() throws SQLException {
		if (rdfDataset.getClassesCount() - rdfDataset.getSchema().getClassesCount() > 0) {
			log.info("Data classes already loaded.");
			return;
		}

		log.info("Loading classes from data component...");
		final ResultSet classes = triplesDao.getDataClasses();
		while (classes.next()) {
			rdfDataset.addClass(classes.getInt(1)); 
		}
		classes.close();
	}

	private void loadDataProperties() throws SQLException {
		if (rdfDataset.getPropertiesCount() - PREDEFINED_PROPS_COUNT - rdfDataset.getSchema().getPropertyNodesCount() > 0) {
			log.info("Data properties already loaded.");
			return;
		}

		log.info("Loading data properties...");
		final ResultSet properties = triplesDao.getDataProperties();
		while (properties.next()) {
			rdfDataset.addProperty(properties.getInt(1));
		}
		properties.close();
	}

	private void loadDataNodes() throws SQLException {
		if (rdfDataset.getDataNodeCount() > 0) {
			log.info("Data nodes already loaded.");
			return;
		}

		log.info("Loading data nodes...");
		int node;
		final ResultSet dataTripleSubjects = triplesDao.getDataTripleSubjects();
		while (dataTripleSubjects.next()) {
			node = dataTripleSubjects.getInt(1);
			addIfDataNode(node);
		}
		dataTripleSubjects.close();

		final ResultSet dataTripleObjects = triplesDao.getDataTripleObjects();
		while (dataTripleObjects.next()) {
			node = dataTripleObjects.getInt(1);
			addIfDataNode(node);
		}
		dataTripleObjects.close();

		final ResultSet typeTripleSubjects = triplesDao.getTypeTripleSubjects();
		while (typeTripleSubjects.next()) {
			node = typeTripleSubjects.getInt(1);
			addIfDataNode(node);
		}
		typeTripleSubjects.close();
	}

	private void addIfDataNode(final int node) {
		if (!rdfDataset.isClassOrProperty(node))
			rdfDataset.addDataNode(node);
	}

	private TroveSchema toTroveSchema(final Schema schema) {
		Preconditions.checkNotNull(schema);

		Iterator<Tuple2<Integer, Integer>> triples;

		triples = schema.getAllDomainConstraints();
		final TIntObjectMap<TIntSet> domainsForProperty = getConstraints(triples);

		triples = schema.getAllRangeConstraints();
		final TIntObjectMap<TIntSet> rangesForProperty = getConstraints(triples);

		triples = schema.getAllSubClassOfConstraints();
		final TIntObjectMap<TIntSet> superClassesForClass = getConstraints(triples);

		triples = schema.getAllSubPropertyOfConstraints();
		final TIntObjectMap<TIntSet> superPropertiesForProperty = getConstraints(triples);

		return new TroveSchema(domainsForProperty, rangesForProperty, superClassesForClass, superPropertiesForProperty);
	}

	private TIntObjectMap<TIntSet> getConstraints(final Iterator<Tuple2<Integer, Integer>> tuples) {
		final TIntObjectMap<TIntSet> map = new TIntObjectHashMap<TIntSet>();

		while (tuples.hasNext()) {
			tuple = tuples.next();
			put(map, tuple.a.intValue(), tuple.b.intValue());
		}

		return map;
	}

	private void put(final TIntObjectMap<TIntSet> map, final int key, final int value) { 
		TIntSet valueSet = map.get(key);
		if (valueSet == null) {
			valueSet = new TIntHashSet();
			valueSet.add(value);
			map.put(key, valueSet);
		}
		else
			valueSet.add(value);
	}
}
