package fr.inria.oak.RDFSummary.rdf.schema.mapdb;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Serializer;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbSchemaImpl implements Schema {

	private final NavigableSet<Tuple2<Integer, Integer>> domainClassesByProperty;
	private final NavigableSet<Tuple2<Integer, Integer>> rangeClassesByProperty;
	private final NavigableSet<Tuple2<Integer, Integer>> superClassesByClass;
	private final NavigableSet<Tuple2<Integer, Integer>> superPropertiesByProperty;
	private final NavigableSet<Tuple3<Integer, Integer, Integer>> allSchemaTriples;
	private final Set<Integer> classes;
	private final Set<Integer> propertyNodes;
	
	private static final String DOMAIN_CLASSES_BY_PROPERTY = "domain_classes_by_property";
	private static final String RANGE_CLASSES_BY_PROPERTY = "range_classes_by_property";
	private static final String SUPER_CLASSES_BY_CLASS = "super_classes_by_class";
	private static final String SUPER_PROPERTIES_BY_PROPERTY = "super_properties_by_property";
	private static final String ALL_SCHEMA_TRIPLES = "all_schema_triples";
	private static final String CLASSES = "schema_classes";
	private static final String PROPERTIES = "schema_property_nodes";
	
	/**
	 * @param datasetDb
	 */
	public MapDbSchemaImpl(final DB datasetDb) { 
		domainClassesByProperty = datasetDb.createTreeSet(DOMAIN_CLASSES_BY_PROPERTY).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		rangeClassesByProperty = datasetDb.createTreeSet(RANGE_CLASSES_BY_PROPERTY).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		superClassesByClass = datasetDb.createTreeSet(SUPER_CLASSES_BY_CLASS).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		superPropertiesByProperty = datasetDb.createTreeSet(SUPER_PROPERTIES_BY_PROPERTY).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		allSchemaTriples = datasetDb.createTreeSet(ALL_SCHEMA_TRIPLES).counterEnable().serializer(BTreeKeySerializer.TUPLE3).makeOrGet();
		classes = datasetDb.createHashSet(CLASSES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.propertyNodes = datasetDb.createHashSet(PROPERTIES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
	}

	@Override
	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty) {
		domainClassesByProperty.add(new Tuple2<Integer, Integer>(property, domain));
		allSchemaTriples.add(new Tuple3<Integer, Integer, Integer>(property, rdfDomainProperty, domain));
		classes.add(domain);
		propertyNodes.add(property);
	}

	@Override
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty) {
		rangeClassesByProperty.add(new Tuple2<Integer, Integer>(property, range));
		allSchemaTriples.add(new Tuple3<Integer, Integer, Integer>(property, rdfRangeProperty, range));
		classes.add(range);
		propertyNodes.add(property);
	}

	@Override
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty) {
		superClassesByClass.add(new Tuple2<Integer, Integer>(classIRI, superClassIRI));
		allSchemaTriples.add(new Tuple3<Integer, Integer, Integer>(classIRI, rdfSubClassOfProperty, superClassIRI));
		classes.add(classIRI);
		classes.add(superClassIRI);
	}

	@Override
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty) {
		superPropertiesByProperty.add(new Tuple2<Integer, Integer>(property, superProperty));
		allSchemaTriples.add(new Tuple3<Integer, Integer, Integer>(property, rdfSubPropertyOfProperty, superProperty));
		propertyNodes.add(property);
		propertyNodes.add(superProperty);
	}

	@Override
	public Iterator<Integer> getDomainClassesByProperty(int property) {
		return Fun.filter(domainClassesByProperty, property).iterator();
	}

	@Override
	public Iterator<Integer> getRangeClassesByProperty(int property) {
		return Fun.filter(rangeClassesByProperty, property).iterator();
	}

	@Override
	public Iterator<Integer> getSuperClassesByClass(int classIRI) {
		return Fun.filter(superClassesByClass, classIRI).iterator();
	}

	@Override
	public Iterator<Integer> getSuperPropertiesByProperty(int property) {
		return Fun.filter(superPropertiesByProperty, property).iterator();
	}

	@Override
	public int getSize() {
		return domainClassesByProperty.size() + rangeClassesByProperty.size() + superClassesByClass.size() + superPropertiesByProperty.size();
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllTriples() {
		return allSchemaTriples.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllDomainConstraints() {
		return domainClassesByProperty.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllRangeConstraints() {
		return rangeClassesByProperty.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllSubClassOfConstraints() {
		return superClassesByClass.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllSubPropertyOfConstraints() {
		return superPropertiesByProperty.iterator();
	}

	@Override
	public int getClassesCount() {
		return classes.size();
	}

	@Override
	public int getPropertyNodesCount() {
		return propertyNodes.size();
	}
}
