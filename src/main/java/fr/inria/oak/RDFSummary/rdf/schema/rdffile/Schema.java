package fr.inria.oak.RDFSummary.rdf.schema.rdffile;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;
import com.hp.hpl.jena.vocabulary.RDFS;

import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.rdfconjunctivequery.IRI;
import fr.inria.oak.commons.reasoning.rdfs.BiMapSchemaValues;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;

/**
 * 
 * @author Sejla CEBIRIC
 * @author Damian BURSZTYN
 *
 */
public class Schema extends Ontology {

	private static final String LEFT_ANGLE = "<";
	private static final String RIGHT_ANGLE = ">";

	/** All classes (aka. concepts) defined in the schema. */
	private final Set<String> classes;

	/** All properties (aka. roles) defined in the schema. */
	private final Set<String> properties;

	/**
	 * Stores the class hierarchy.
	 *  - key : Class
	 *  - value : Set of all sub-classes, regardless of depth
	 */
	private final BiMapSchemaValues<String> subClassesBiMap;

	/**
	 * Stores the property hierarchy.
	 *  - key : Properties
	 *  - value : Set of all sub-properties, regardless of depth
	 */
	private final BiMapSchemaValues<String> subPropertiesBiMap;

	/**
	 *  - key : properties
	 *  - value : ranges of the given properties
	 */
	private final BiMapSchemaValues<String> rangesBiMap;

	/**
	 *  - key : properties
	 *  - value : domains of the given properties
	 */
	private final BiMapSchemaValues<String> domainsBiMap;

	/**
	 * Constructs an empty Schema instance
	 */
	public Schema() {
		this.classes = Sets.newHashSet();
		this.properties = Sets.newHashSet();
		this.subClassesBiMap = new BiMapSchemaValues<String>();
		this.subPropertiesBiMap = new BiMapSchemaValues<String>();
		this.rangesBiMap = new BiMapSchemaValues<String>();
		this.domainsBiMap = new BiMapSchemaValues<String>();
	}

	/**
	 * Parameterized constructor.
	 *
	 * @param classes
	 * @param properties
	 * @param subClasses
	 * @param subProperties
	 * @param ranges
	 * @param domains
	 */
	public Schema(final Set<String> classes, final Set<String> properties, final BiMapSchemaValues<String> subClasses, final BiMapSchemaValues<String> subProperties, final BiMapSchemaValues<String> ranges, final BiMapSchemaValues<String> domains) {
		this.classes = classes;
		this.properties = properties;
		this.subClassesBiMap = subClasses;
		this.subPropertiesBiMap = subProperties;
		this.rangesBiMap = ranges;
		this.domainsBiMap = domains;
	}

	@Override
	public boolean isRole(final String predicate) {
		return properties.contains(predicate);
	}

	@Override
	public boolean isConcept(final String predicate) {
		return classes.contains(predicate);
	}

	/**
	 * @param classIRI
	 */
	public void addClass(final String classIRI) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classIRI));
		
		classes.add(classIRI);
	}
	
	/**
	 * @param propertyIRI
	 */
	public void addProperty(final String propertyIRI) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyIRI));
		
		properties.add(propertyIRI);
	}
	
	/**
	 * @param subClass
	 * @param superClass
	 */
	public void addSubClassOfTriple(final String subClass, final String superClass) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(subClass));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(superClass));
		
		final Map<String, Set<String>> map = subClassesBiMap.getMap();
		Set<String> subClasses = map.get(superClass); 
		if (subClasses == null)
			subClasses = Sets.newHashSet();
		
		subClasses.add(subClass);
		subClassesBiMap.put(superClass, subClasses);
		classes.add(subClass);
		classes.add(superClass);
	}
	
	/**
	 * @param subProperty
	 * @param superProperty
	 */
	public void addSubPropertyOfTriple(final String subProperty, final String superProperty) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(subProperty));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(superProperty));
		
		final Map<String, Set<String>> map = subPropertiesBiMap.getMap();
		Set<String> subProperties = map.get(superProperty);
		if (subProperties == null)
			subProperties = Sets.newHashSet();
		
		subProperties.add(subProperty);
		subPropertiesBiMap.put(superProperty, subProperties);
		properties.add(subProperty);
		properties.add(superProperty);
	}
	
	/**
	 * @param propertyIRI
	 * @param classIRI
	 */
	public void addDomainTriple(final String propertyIRI, final String classIRI) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyIRI));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classIRI));
		
		final Map<String, Set<String>> map = domainsBiMap.getMap();
		Set<String> propertyIRIs = map.get(classIRI);
		if (propertyIRIs == null)
			propertyIRIs = Sets.newHashSet();
		
		propertyIRIs.add(propertyIRI);
		domainsBiMap.put(classIRI, propertyIRIs);
		properties.add(propertyIRI);
		classes.add(classIRI);
	}
	
	/**
	 * @param propertyIRI
	 * @param classIRI
	 */
	public void addRangeTriple(final String propertyIRI, final String classIRI) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyIRI));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classIRI));
		
		final Map<String, Set<String>> map = rangesBiMap.getMap();
		Set<String> propertyIRIs = map.get(classIRI);
		if (propertyIRIs == null)
			propertyIRIs = Sets.newHashSet();
		
		propertyIRIs.add(propertyIRI);
		rangesBiMap.put(classIRI, propertyIRIs);
		properties.add(propertyIRI);
		classes.add(classIRI);
	}
	
	/**
	 * Returns the given class, as well as all its sub-classes
	 * @param key
	 * @return Returns the given class, as well as all its sub-classes
	 */
	public Set<String> getSubClasses(final String key) {
		Set<String> result = this.subClassesBiMap.getMap().get(key);
		if (result == null) {
			result = new HashSet<String>();
		}
//		result.add(key);
		return result;
	}	

	/**
	 * Returns the given class, as well as all its super-classes
	 *
	 * @param key
	 * @return Returns the given class, as well as all its super-classes
	 */
	public Set<String> getSuperClasses(final String key) {
		Set<String> result = this.subClassesBiMap.getReverseMap().get(key);
		if (result == null) {
			result = new HashSet<String>();
		}
//		result.add(key);
		return result;
	}

	/**
	 * Returns all classes in the schema
	 *
	 * @return
	 */
	public Set<String> getAllClasses() {
		return this.classes;
	}

	/**
	 * Returns all properties in the schema
	 * @return
	 */
	public Set<String> getAllProperties() {
		return this.properties;
	}

	/**
	 * Returns all domains defined in the schema
	 * @return
	 */
	public Set<String> getAllDomains() {
		return this.domainsBiMap.getMap().keySet();
	}

	/**
	 * Returns all ranges defined in the schema
	 * @return
	 */
	public Set<String> getAllRanges() {
		return this.rangesBiMap.getMap().keySet();
	}

	/**
	 * Returns all RDFS triples
	 * 
	 * @return Returns all RDFS triples
	 */
	public Set<Triple> getAllTriples() {		
		Set<Triple> triples = Sets.newHashSet();

		// Sub-classes
		final Map<String, Set<String>> subClassesMap = this.subClassesBiMap.getMap();
		for (final String superClass : subClassesMap.keySet()) {
			IRI superClassIRI = new IRI(format(superClass));
			IRI rdfsSubClassIRI = new IRI(RDFS.subClassOf.toString());
			Set<String> subClasses = subClassesMap.get(superClass);			
			for (String subClass : subClasses)
				triples.add(new Triple(new IRI(format(subClass)), rdfsSubClassIRI, superClassIRI));
		}

		// Sub-properties
		final Map<String, Set<String>> subPropertiesMap = this.subPropertiesBiMap.getMap();
		for (final String superProperty : subPropertiesMap.keySet()) {
			IRI superPropertyIRI = new IRI(format(superProperty));
			IRI rdfsSubPropertyOfIRI = new IRI(RDFS.subPropertyOf.toString());		
			Set<String> subProperties = subPropertiesMap.get(superProperty);
			for (String subProperty : subProperties)
				triples.add(new Triple(new IRI(format(subProperty)), rdfsSubPropertyOfIRI, superPropertyIRI));	
		}

		// Ranges
		final Map<String, Set<String>> rangesMap = this.rangesBiMap.getMap();
		for (final String range : rangesMap.keySet()) {
			IRI rangeIRI = new IRI(format(range));
			IRI rdfsRangeIRI = new IRI(RDFS.range.toString());
			final Set<String> properties = rangesMap.get(range);			
			for (String property : properties)
				triples.add(new Triple(new IRI(format(property)), rdfsRangeIRI, rangeIRI));
		}

		// Domains
		final Map<String, Set<String>> domainsMap = this.domainsBiMap.getMap();
		for (final String domain : domainsMap.keySet()) {
			IRI domainIRI = new IRI(format(domain));
			IRI rdfsDomainIRI = new IRI(RDFS.domain.toString());
			final Set<String> properties = domainsMap.get(domain);
			for (String property : properties)
				triples.add(new Triple(new IRI(format(property)), rdfsDomainIRI, domainIRI));
		}

		return triples;		
	}	

	/**
	 * Removes the angle brackets if the specified string is enclosed in them
	 * 
	 * @param str
	 * @return The string without the angle brackets
	 */
	private String format(final String str) {
		if (StringUtils.isNullOrBlank(str))
			return str;
		if (str.startsWith(LEFT_ANGLE) && str.endsWith(RIGHT_ANGLE))
			return str.substring(1, str.length()-1);

		return str;
	}

	/**
	 * Returns the given property, as well as all its sub-properties
	 * @param key
	 * @return Returns the given property, as well as all its sub-properties
	 */
	public Set<String> getSubProperties(final String key) {
		Set<String> result = this.subPropertiesBiMap.getMap().get(key);
		if (result == null) {
			result = new HashSet<String>();
		}
//		result.add(key);
		return result;
	}

	/**
	 * Returns the given property, as well as all its super-properties
	 *
	 * @param key
	 * @return Returns the given property, as well as all its super-properties
	 */
	public Set<String> getSuperProperties(final String key) {
		Set<String> result = this.subPropertiesBiMap.getReverseMap().get(key);
		if (result == null) {
			result = new HashSet<String>();
		}
//		result.add(key);
		return result;
	}

	/**
	 * Returns all ranges allowed for the given property
	 *
	 * @param property
	 * @return Returns all ranges allowed for the given property
	 */
	public Set<String> getPropertiesForRange(final String range) {
		final Set<String> result = this.rangesBiMap.getMap().get(range);
		if (result == null) {
			return new HashSet<String>();
		}
		return result;
	}

	/**
	 * Returns the ranges for the given property
	 *
	 * @param property
	 * @return ranges for the given property
	 */
	public Set<String> getRangesForProperty(final String property) {
		final Set<String> result = this.rangesBiMap.getReverseMap().get(property);
		if (result == null) {
			return new HashSet<String>();
		}
		return result;
	}

	/**
	 * Returns all properties with the given domain
	 *
	 * @param domain
	 * @return properties with the given domain
	 */
	public Set<String> getPropertiesForDomain(final String domain) {
		final Set<String> result = this.domainsBiMap.getMap().get(domain);
		if (result == null) {
			return new HashSet<String>();
		}
		return result;
	}

	/**
	 * Returns the domains for the given property
	 *
	 * @param property
	 * @return domains for the given property
	 */
	public Set<String> getDomainsForProperty(final String property) {
		final Set<String> result = this.domainsBiMap.getReverseMap().get(property);
		if (result == null) {
			return new HashSet<String>();
		}
		return result;
	}


	/**
	 * Return the collection of parent for the given class/property
	 * @param property
	 * @return the flattened class/property hierarchy for the given key
	 */
	public Collection<String> getParents(final String resource) {
		final Collection<String> result = new HashSet<String>();
		result.addAll(this.subClassesBiMap.getReverseMap().get(resource));
		result.addAll(this.subPropertiesBiMap.getReverseMap().get(resource));
		return result;
	}



	/**
	 * @param key
	 * @return true, if the given class is defined in the schema
	 */
	public boolean hasClass(final String key) {
		return this.classes.contains(key);
	}

	/**
	 * @param key
	 * @return true, if the given property is defined in the schema
	 */
	public boolean hasProperty(final String key) {
		return this.properties.contains(key);
	}

	/**
	 * @param key
	 * @return true, if the class defined by key has at least one subclass
	 */
	public boolean hasSubClass(final String key) {
		return this.subClassesBiMap.getMap().get(key) != null && !this.subClassesBiMap.getMap().get(key).isEmpty();
	}

	/**
	 * @param key
	 * @return true, if the class defined by key has at least one superclass
	 */
	public boolean hasSuperClass(final String key) {
		return this.subClassesBiMap.getReverseMap().get(key) != null
				&& !this.subClassesBiMap.getReverseMap().get(key).isEmpty();
	}

	/**
	 * @param key
	 * @return true, if the property defined by key has at least one
	 *         subProperties
	 */
	public boolean hasSubProperty(final String key) {
		return this.subPropertiesBiMap.getMap().get(key) != null && !this.subPropertiesBiMap.getMap().get(key).isEmpty();
	}

	/**
	 * @param key
	 * @return true, if the property defined by key has at least one
	 *         superProperties
	 */
	public boolean hasSuperProperty(final String key) {
		return this.subPropertiesBiMap.getReverseMap().get(key) != null
				&& !this.subPropertiesBiMap.getReverseMap().get(key).isEmpty();
	}

	/**
	 * @param key
	 * @return true, if the class belongs to the domain of at least one property
	 */
	public boolean isDomain(final String clazz) {
		return this.domainsBiMap.getMap().containsKey(clazz);
	}

	/**
	 * @param key
	 * @return true, if the property has a domain
	 */
	public boolean hasDomain(final String property) {
		return this.domainsBiMap.getReverseMap().containsKey(property);
	}

	/**
	 * @param key
	 * @return true, if the class belongs to the range of at least one property
	 */
	public boolean isRange(final String clazz) {
		return this.rangesBiMap.getMap().containsKey(clazz);
	}

	/**
	 * @param key
	 * @return true, if the property has a range
	 */
	public boolean hasRange(final String property) {
		return this.rangesBiMap.getReverseMap().containsKey(property);
	}

	@Override
	public boolean equals(final Object o) {
		return 	(o instanceof Schema) &&
				classes.equals(((Schema) o).classes) &&
				properties.equals(((Schema) o).properties) &&
				subClassesBiMap.equals(((Schema) o).subClassesBiMap) &&
				subPropertiesBiMap.equals(((Schema) o).subPropertiesBiMap) &&
				domainsBiMap.equals(((Schema) o).domainsBiMap) &&
				rangesBiMap.equals(((Schema) o).rangesBiMap);
	}
	
	/**
	 * Returns a set with all the constants in the <code>schema</code>.
	 * @param schema
	 * @return schema constants
	 */
	public Set<String> getConstants() {
		Set<String> constants = Sets.newHashSet();
		constants.addAll(this.getAllClasses());
		constants.addAll(this.getAllProperties());

		return constants;
	}
}
