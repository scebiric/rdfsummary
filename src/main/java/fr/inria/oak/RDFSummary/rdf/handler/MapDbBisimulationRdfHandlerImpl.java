package fr.inria.oak.RDFSummary.rdf.handler;

import java.sql.SQLException;

import org.openrdf.model.Statement;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.helpers.AbstractRDFHandler;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.MapDbBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbBisimulationRdfHandlerImpl extends AbstractRDFHandler {

	private final Dictionary dictionary;
	private final MapDbBisimulationRdfDataset rdfDataset;	

	private Integer subject;
	private Integer property;
	private Integer object;

	/**
	 * @param dictionary
	 * @param rdfDataset
	 */
	public MapDbBisimulationRdfHandlerImpl(final Dictionary dictionary, final MapDbBisimulationRdfDataset rdfDataset) {
		this.dictionary = dictionary;
		this.rdfDataset = rdfDataset;
	}

	@Override
	public void handleStatement(final Statement st) throws RDFHandlerException {
		try {
			subject = DictionaryUtils.getKey(dictionary, st.getSubject().toString());
			property = DictionaryUtils.getKey(dictionary, st.getPredicate().toString());
			object = DictionaryUtils.getKey(dictionary, st.getObject().toString());

			rdfDataset.addProperty(property);
			if (rdfDataset.isTypeProperty(property)) {
				rdfDataset.addClass(object);
				rdfDataset.addTypeTriple(subject, object);
			}
			else if (rdfDataset.isSchemaProperty(property)) {
				rdfDataset.addSchemaTriple(subject, property, object);
			}
			else {
				rdfDataset.addDataNode(subject); 
				rdfDataset.addDataNode(object);
				rdfDataset.addPropertyObjectPairsBySubject(subject, property, object);
				rdfDataset.addPropertySubjectPairsByObject(subject, property, object);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RDFHandlerException(e.getMessage());
		} 
	}
}
