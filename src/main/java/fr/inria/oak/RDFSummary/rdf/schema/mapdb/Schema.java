package fr.inria.oak.RDFSummary.rdf.schema.mapdb;

import java.util.Iterator;

import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Schema {

	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty);
	
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty);
	
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty);
	
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty);
	
	public Iterator<Tuple2<Integer, Integer>> getAllDomainConstraints();
	
	public Iterator<Tuple2<Integer, Integer>> getAllRangeConstraints();
	
	public Iterator<Tuple2<Integer, Integer>> getAllSubClassOfConstraints();
	
	public Iterator<Tuple2<Integer, Integer>> getAllSubPropertyOfConstraints();
	
	public Iterator<Integer> getDomainClassesByProperty(int property);
	
	public Iterator<Integer> getRangeClassesByProperty(int property);
	
	public Iterator<Integer> getSuperClassesByClass(int classIRI);
	
	public Iterator<Integer> getSuperPropertiesByProperty(int property);
	
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllTriples(); 
	
	public int getSize();
	
	public int getClassesCount();
	
	public int getPropertyNodesCount();
}
