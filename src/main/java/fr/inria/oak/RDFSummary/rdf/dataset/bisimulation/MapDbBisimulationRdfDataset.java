package fr.inria.oak.RDFSummary.rdf.dataset.bisimulation;

import java.util.Iterator;

import org.mapdb.Fun.Tuple2;

import fr.inria.oak.RDFSummary.rdf.dataset.MapDbRdfDataset;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface MapDbBisimulationRdfDataset extends BisimulationRdfDataset, MapDbRdfDataset {

	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void addInferredPropertyObjectPairsBySubject(int subject, int property, int object);

	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void addInferredPropertySubjectPairsByObject(int subject, int property, int object);
	
	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void addPropertyObjectPairsBySubject(int subject, int property, int object);

	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void addPropertySubjectPairsByObject(int subject, int property, int object);
	
	/**
	 * @param subject
	 * @param object
	 */
	public void addInferredTypeTriple(int subject, int object);
	
	/**
	 * @return An iterator over the set of inferred type triples
	 */
	public Iterator<Tuple2<Integer, Integer>> getInferredTypeTriples();
	
	/**
	 * @param subject
	 * @return An iterator over the set of inferred (property,object) pairs for the specified subject
	 */
	public Iterator<Tuple2<Integer, Integer>> getInferredPropertyObjectPairsBySubject(int subject);
	
	/**
	 * @param object
	 * @return An iterator over the set of inferred (property,subject) pairs for the specified object
	 */
	public Iterator<Tuple2<Integer, Integer>> getInferredPropertySubjectPairsByObject(int object);
	
	/**
	 * @param subject
	 * @return An iterator over the set of (property,object) pairs for the specified subject
	 */
	public Iterator<Tuple2<Integer, Integer>> getPropertyObjectPairsBySubject(int subject);
	
	/**
	 * @param object
	 * @return An iterator over the set of (property,subject) pairs for the specified object
	 */
	public Iterator<Tuple2<Integer, Integer>> getPropertySubjectPairsByObject(int object);
}
