package fr.inria.oak.RDFSummary.rdf.dataset.bisimulation;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.ResultSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PostgresBisimulationRdfDataset extends BisimulationRdfDataset {

	@Deprecated
	public void addDataTriple(int subject, int property, int object) throws SQLException;

	public ResultSet getPropertyObjectPairsBySubject(int gDataNode) throws SQLException;

	public ResultSet getPropertySubjectPairsByObject(int gDataNode) throws SQLException;

	public ResultSet getDataTriples() throws SQLException;
	
	public ResultSet getTypeTriples() throws SQLException;
	
	public int getPropertiesCount();
}
