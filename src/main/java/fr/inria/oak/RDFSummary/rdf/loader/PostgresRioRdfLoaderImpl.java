package fr.inria.oak.RDFSummary.rdf.loader;

import java.io.IOException;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.ntriples.NTriplesParser;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresRioRdfLoaderImpl implements RdfLoader {

	private static final Logger log = Logger.getLogger(RdfLoader.class);
	private final MessageBuilder messageBuilder;
	
	private final RdfDataset rdfDataset;
	private final String dataFilepath;
	private final MapDbManager mapDbManager;
	private final SqlConnectionHandler connHandler;
	private final DmlDao dmlDao;
	private final DdlDao ddlDao;
	private final DictionaryDao dictDao;
	private final TriplesDao triplesDao;	
	private final Timer timer;
	private final boolean dropSchema;
	private final String schemaName;
	private final String dictionaryTable;
	private final String dataTriplesTable;
	private final String typeTriplesTable;
	private final String rdfDatasetDbName;
	private final String mapDbFolder;
	
	private final RDFParser parser;
	
	/**
	 * @param rdfDataset
	 * @param dataFilepath
	 * @param rdfHandler
	 * @param mapDbManager
	 * @param connHandler
	 * @param dmlDao
	 * @param ddlDao
	 * @param dictDao
	 * @param triplesDao
	 * @param timer
	 * @param dropSchema
	 * @param schemaName
	 * @param dictionaryTable
	 * @param dataTriplesTable
	 * @param typeTriplesTable
	 * @param rdfDatasetDbName
	 * @param mapDbFolder
	 */
	public PostgresRioRdfLoaderImpl(final RdfDataset rdfDataset, final String dataFilepath, final RDFHandler rdfHandler, final MapDbManager mapDbManager, 
			final SqlConnectionHandler connHandler,final DmlDao dmlDao, final DdlDao ddlDao,  final DictionaryDao dictDao,
			final TriplesDao triplesDao, final Timer timer, final boolean dropSchema, final String schemaName, 
			final String dictionaryTable, final String dataTriplesTable, final String typeTriplesTable,
			final String rdfDatasetDbName, final String mapDbFolder) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataFilepath));
		
		this.rdfDataset = rdfDataset;
		this.dataFilepath = dataFilepath;
		this.mapDbManager = mapDbManager;
		this.connHandler = connHandler;
		this.dmlDao = dmlDao;
		this.ddlDao = ddlDao;
		this.dictDao = dictDao;
		this.triplesDao = triplesDao;
		this.timer = timer;
		this.dropSchema = dropSchema;
		this.schemaName = schemaName;
		this.dictionaryTable = dictionaryTable;
		this.dataTriplesTable = dataTriplesTable;
		this.typeTriplesTable = typeTriplesTable;
		this.rdfDatasetDbName = rdfDatasetDbName;
		this.mapDbFolder = mapDbFolder;
		
		this.messageBuilder = new MessageBuilder();
		this.parser = new NTriplesParser();
		parser.setRDFHandler(rdfHandler);
	}

	@Override
	public void loadDataset() throws IOException, SQLException {
		timer.reset();
		if (rdfDataset.isLoaded()) {
			log.info("Dataset already loaded.");
			return;
		}
		
		boolean deleted = mapDbManager.deleteDb(rdfDatasetDbName, mapDbFolder);
		if (deleted)
			log.info(messageBuilder.append("Deleted db: ").append(rdfDatasetDbName).append(", in: ").append(mapDbFolder));
		
		timer.start();
		log.info(messageBuilder.append("Loading dataset from ").append(dataFilepath).append(Constant.THREE_DOTS));		
		if (dropSchema)
			ddlDao.dropSchema(schemaName);
		if (!dmlDao.existsSchema(schemaName))
			ddlDao.createPostgresSchema(schemaName);
				
		createTables();
		triplesDao.prepareInsertStatements();
		RdfLoaderUtils.loadRdfProperties();
		
		connHandler.disableAutoCommit();
		RdfLoaderUtils.parse(dataFilepath, parser);
		connHandler.enableAutoCommit();
		
		triplesDao.closeInsertStatements();
		timer.stop();
		log.info(messageBuilder.append("Dataset loaded in: ").append(timer.getTimeAsString()));
	}
	
	private void createTables() throws SQLException {
		if (!dmlDao.existsTable(dictionaryTable))
			this.dictDao.createDictionaryTable(dictionaryTable);
		
		if (dmlDao.existsTable(dataTriplesTable)) 
			log.info(messageBuilder.append(dataTriplesTable).append(" already exists."));
		else
			triplesDao.createDataTriplesTable();
		
		if (dmlDao.existsTable(typeTriplesTable)) 
			log.info(messageBuilder.append(typeTriplesTable).append(" already exists."));
		else
			triplesDao.createTypeTriplesTable();
	}
}
