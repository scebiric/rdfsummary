package fr.inria.oak.RDFSummary.rdf.dataset.bisimulation;

import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Serializer;

import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.rdf.RdfProperty;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.MapDbSchemaImpl;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;
import fr.inria.oak.RDFSummary.util.NameUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbBisimulationRdfDatasetImpl implements MapDbBisimulationRdfDataset {
	
	private final DB datasetDb;
	
	private final Set<Integer> classes;
	private final Set<Integer> properties;
	private final Set<Integer> dataNodes; 
	private final NavigableSet<Tuple2<Integer, Integer>> typeTriples;
	private final NavigableSet<Tuple2<Integer, Integer>> inferredTypeTriples;
	private final Schema schema;
	
	private final Map<String, Integer> rdfPropertyKeys;
	
	private static final String CLASSES = "classes";
	private static final String PROPERTIES = "properties";
	private static final String DATA_NODES = "data_nodes";
	private static final String TYPE_TRIPLES = "type_triples";
	private static final String INFERRED_TYPE_TRIPLES = "inferred_type_triples";
	
	private NavigableSet<Tuple2<Integer, Integer>> pairSet;
	
	/**
	 * @param datasetDb
	 */
	public MapDbBisimulationRdfDatasetImpl(final DB datasetDb) {	
		this.datasetDb = datasetDb;
		
		this.classes = datasetDb.createHashSet(CLASSES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.properties = datasetDb.createHashSet(PROPERTIES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.dataNodes = datasetDb.createHashSet(DATA_NODES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.typeTriples = datasetDb.createTreeSet(TYPE_TRIPLES).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		this.inferredTypeTriples = datasetDb.createTreeSet(INFERRED_TYPE_TRIPLES).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		this.schema = new MapDbSchemaImpl(datasetDb);
		
		this.rdfPropertyKeys = Maps.newHashMap();
	}

	@Override
	public void addClass(final int classIRI) {
		classes.add(classIRI);
	}

	@Override
	public Iterator<Integer> getClasses() {
		return classes.iterator();
	}

	@Override
	public boolean isLoaded() {
		return classes.size() > 0 || properties.size() > 0 || dataNodes.size() > 0 || typeTriples.size() > 0; 
	}

	@Override
	public void addDataNode(final int dataNode) {
		dataNodes.add(dataNode);
	}

	@Override
	public Iterator<Integer> getDataNodes() {
		return dataNodes.iterator();
	}
	
	@Override
	public void addTypeTriple(final int subject, final int object) {
		typeTriples.add(new Tuple2<Integer, Integer>(subject, object)); 		
	}
	
	@Override
	public void addInferredPropertyObjectPairsBySubject(int subject, int property, int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getSubjectInferredSetName(subject));
		pairSet.add(new Tuple2<Integer, Integer>(property, object));
	}
	
	@Override
	public void addPropertyObjectPairsBySubject(int subject, int property, int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getSubjectSetName(subject));
		pairSet.add(new Tuple2<Integer, Integer>(property, object));  
	}

	@Override
	public void addInferredPropertySubjectPairsByObject(int subject, int property, int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getObjectInferredSetName(object));
		pairSet.add(new Tuple2<Integer, Integer>(property, subject)); 
	}

	@Override
	public void addPropertySubjectPairsByObject(int subject, int property, int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getObjectSetName(object));
		pairSet.add(new Tuple2<Integer, Integer>(property, subject));
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getInferredTypeTriples() {
		return inferredTypeTriples.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getTypeTriples() {
		return typeTriples.iterator();		
	}

	@Override
	public Schema getSchema() {
		return schema;
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getInferredPropertyObjectPairsBySubject(int subject) {
		pairSet = datasetDb.getTreeSet(NameUtils.getSubjectInferredSetName(subject));
		
		return pairSet.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getPropertyObjectPairsBySubject(int subject) {
		pairSet = datasetDb.getTreeSet(NameUtils.getSubjectSetName(subject));
		
		return pairSet.iterator();
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getInferredPropertySubjectPairsByObject(int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getObjectInferredSetName(object));
		
		return pairSet.iterator();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getPropertySubjectPairsByObject(int object) {
		pairSet = datasetDb.getTreeSet(NameUtils.getObjectSetName(object));
		
		return pairSet.iterator();
	}

	@Override
	public void addRdfProperty(String rdfProperty, int key) {
		rdfPropertyKeys.put(rdfProperty, key);
	}

	@Override
	public boolean isTypeProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_TYPE).intValue() || property == rdfPropertyKeys.get(RdfProperty.TYPE).intValue();
	}

	@Override
	public boolean isDomainProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_DOMAIN).intValue()  || property == rdfPropertyKeys.get(RdfProperty.DOMAIN).intValue();
	}

	@Override
	public boolean isRangeProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_RANGE).intValue()  || property == rdfPropertyKeys.get(RdfProperty.RANGE).intValue() ;
	}

	@Override
	public boolean isSubClassOfProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_SUBCLASS).intValue()  || property == rdfPropertyKeys.get(RdfProperty.SUBCLASS).intValue() ;
	}

	@Override
	public boolean isSubPropertyOfProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_SUBPROPERTY).intValue()  || property == rdfPropertyKeys.get(RdfProperty.SUBPROPERTY).intValue() ;
	}

	@Override
	public void addSchemaTriple(int subject, int property, int object) {
		if (isDomainProperty(property))
			addDomainClassByProperty(subject, object, property);
		
		else if (isRangeProperty(property))
			addRangeClassByProperty(subject, object, property);
		
		else if (isSubClassOfProperty(property))
			addSuperClassByClass(subject, object, property);
		
		else if (isSubPropertyOfProperty(property))
			addSuperPropertyByProperty(subject, object, property);
	}

	@Override
	public boolean isSchemaProperty(int property) {
		return isDomainProperty(property) || isRangeProperty(property) || isSubClassOfProperty(property) || isSubPropertyOfProperty(property);
	}

	@Override
	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty) {
		schema.addDomainClassByProperty(property, domain, rdfDomainProperty);
		addProperty(property);
		addClass(domain);
	}

	@Override
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty) {
		schema.addRangeClassByProperty(property, range, rdfRangeProperty);
		addProperty(property);
		addClass(range);
	}

	@Override
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty) {
		schema.addSuperClassByClass(classIRI, superClassIRI, rdfSubClassOfProperty);
		addClass(classIRI);
		addClass(superClassIRI);
	}

	@Override
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty) {
		schema.addSuperPropertyByProperty(property, superProperty, rdfSubPropertyOfProperty);
		addProperty(property);
		addProperty(superProperty);
	}

	@Override
	public int getClassesCount() {
		return classes.size();
	}

	@Override
	public int getDataNodeCount() {
		return dataNodes.size();
	}

	@Override
	public void addProperty(int property) {
		properties.add(property);	
	}

	@Override
	public Iterator<Integer> getProperties() {
		return properties.iterator();
	}

	@Override
	public boolean isClass(int iri) {
		return classes.contains(iri);
	}

	@Override
	public boolean isProperty(int iri) {
		return properties.contains(iri);
	}

	@Override
	public boolean isClassOrProperty(int iri) {
		return isClass(iri) || isProperty(iri);
	}

	@Override
	public Map<String, Integer> getRdfPropertyKeys() {
		return rdfPropertyKeys;
	}

	@Override
	public void addInferredTypeTriple(int subject, int object) {
		inferredTypeTriples.add(new Tuple2<Integer, Integer>(subject, object));
	}
}
