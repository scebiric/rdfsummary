package fr.inria.oak.RDFSummary.rdf.parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresCopyRdfParserImpl implements RdfParser {
	
	private static final Logger log = Logger.getLogger(RdfParser.class);
	private static DdlDao DdlDao;
	private static DmlDao DmlDao;
	private static RdfTablesDao RdfTablesDao;
	private static Timer Timer;
	private final String schemaName;
	private final String triplesTable;
	
	/**
	 * @param ddlDao
	 * @param dmlDao
	 * @param rdfTablesDao
	 * @param timer
	 * @param schemaName
	 * @param triplesTable
	 */
	public PostgresCopyRdfParserImpl(final DdlDao ddlDao, final DmlDao dmlDao, final RdfTablesDao rdfTablesDao, final Timer timer,
			final String schemaName, final String triplesTable) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(triplesTable));
		
		DdlDao = ddlDao;
		DmlDao = dmlDao;
		RdfTablesDao = rdfTablesDao;
		Timer = timer;
		this.schemaName = schemaName;
		this.triplesTable = triplesTable;
	}

	/**
	 * {@inheritDoc}
	 * @throws IOException 
	 */
	@Override
	public long parse(final String dataFilepath) throws SQLException, IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataFilepath));
		
		Timer.reset();
		log.info("Loading triples to " + triplesTable + " Postgres table from: " + dataFilepath + "...");
		DdlDao.createPostgresSchema(schemaName);
		RdfTablesDao.createTriplesTableWithSkippedChars(triplesTable);
		InputStream inputStream = new FileInputStream(dataFilepath);
		Timer.start();
		DmlDao.copy(triplesTable, inputStream);
		Timer.stop();
		DdlDao.dropColumn(triplesTable, Constant.SKIPPED_CHARS);
		
		log.info("Loaded triples to " + triplesTable + " table in " + Timer.getTimeAsString());
		
		return Timer.getTimeInMilliseconds();
	}

}
