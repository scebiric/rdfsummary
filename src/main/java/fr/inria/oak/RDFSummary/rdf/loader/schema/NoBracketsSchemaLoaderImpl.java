package fr.inria.oak.RDFSummary.rdf.loader.schema;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.SchemaParser;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class NoBracketsSchemaLoaderImpl implements SchemaLoader {

	private static final Logger log = Logger.getLogger(SchemaLoader.class);
	
	private final String schemaFilepath;
	private final RdfDataset rdfDataset;
	private final Dictionary dictionary;
	private final SchemaParser schemaParser;
	private final Timer timer;
	
	private final MessageBuilder messageBuilder;
	
	/**
	 * @param schemaFilepath
	 * @param rdfDataset
	 * @param dictionary
	 * @param schemaParser
	 * @param timer
	 */
	public NoBracketsSchemaLoaderImpl(final String schemaFilepath, final RdfDataset rdfDataset, final Dictionary dictionary, final SchemaParser schemaParser, final Timer timer) {
		this.schemaFilepath = schemaFilepath;
		this.rdfDataset = rdfDataset;
		this.dictionary = dictionary;
		this.schemaParser = schemaParser;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void loadSchemaFromFile() throws SchemaException, IOException, SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaFilepath));
		
		timer.reset();
		timer.start();
		log.info(messageBuilder.append("Loading schema triples from ").append(schemaFilepath).append(Constant.THREE_DOTS));
		final File file = new File(schemaFilepath);
		if (!file.exists())
			throw new IOException(messageBuilder.append("The file ").append(schemaFilepath).append(" does not exist.").toString());
		
		RdfLoaderUtils.loadRdfProperties();
		
		final Schema schemaFromFile = schemaParser.parseSchema(schemaFilepath, null, RdfUtils.getRDFLanguage(schemaFilepath));
		final Set<String> constants = schemaFromFile.getConstants();
		for (final String constant : constants) {
			if (StringUtils.isNullOrBlank(constant))
				continue;
			
			if (dictionary.getKey(constant) == null)
				dictionary.insert(constant);
		}
		 
		int encodedSubject;
		int encodedProperty;
		int encodedObject;
		final Set<Triple> schemaTriples = schemaFromFile.getAllTriples();
		if (schemaTriples != null) { 
			for (final Triple triple : schemaTriples) {
				encodedSubject = dictionary.getKey(triple.subject.getContent());
				encodedProperty = dictionary.getKey(triple.property.getContent());
				encodedObject = dictionary.getKey(triple.object.getContent());
				
				rdfDataset.addSchemaTriple(encodedSubject, encodedProperty, encodedObject);
			}
		}
		
		timer.stop();
		log.info(messageBuilder.append("Schema triples loaded from file in: ").append(timer.getTimeAsString()));
	}
}
