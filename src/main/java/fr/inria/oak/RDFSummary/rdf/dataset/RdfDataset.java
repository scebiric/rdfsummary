package fr.inria.oak.RDFSummary.rdf.dataset;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfDataset {

	/**
	 * @param classIRI
	 */
	public void addClass(int classIRI);

	/**
	 * @param dataNode
	 */
	public void addDataNode(int dataNode);
	
	/**
	 * @param property
	 */
	public void addProperty(int property);
	
	/**
	 * @param subject
	 * @param object
	 * @throws SQLException 
	 */
	public void addTypeTriple(int subject, int object) throws SQLException;
	
	/**
	 * @param encodedSubject
	 * @param encodedProperty
	 * @param encodedObject
	 */
	public void addSchemaTriple(int encodedSubject, int encodedProperty, int encodedObject);

	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty);
	
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty);
	
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty);
	
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty);

	/**
	 * Stores the key for the RDF property
	 * 
	 * @param rdfProperty
	 * @param key
	 */
	public void addRdfProperty(String rdfProperty, int key);
	
	/**
	 * @return {@link Schema}
	 */
	public Schema getSchema();
	
	public boolean isClass(int iri);
	
	public boolean isProperty(int iri);
	
	public boolean isClassOrProperty(int iri);
	
	public boolean isLoaded() throws SQLException;

	public boolean isTypeProperty(int property);
	
	public boolean isDomainProperty(int property);
	
	public boolean isRangeProperty(int property);
	
	public boolean isSubClassOfProperty(int property);
	
	public boolean isSubPropertyOfProperty(int property);

	public boolean isSchemaProperty(int property);

	/**
	 * @return The number of distinct classes in the dataset (data+schema)
	 */
	public int getClassesCount(); 
	
	/**
	 * @return The number of data nodes in the dataset
	 */
	public int getDataNodeCount(); 	
	
	/**
	 * @return Map: key Rdf type and RDFS properties (full and short), value: their integer encoding
	 */
	public Map<String, Integer> getRdfPropertyKeys();
	
	/**
	 * @return An iterator over the set of class nodes
	 */
	public Iterator<Integer> getClasses();

	/**
	 * @return An iterator over the set of data nodes
	 */
	public Iterator<Integer> getDataNodes();
	
	/**
	 * @return An iterator over the set of property nodes
	 */
	public Iterator<Integer> getProperties(); 
}
