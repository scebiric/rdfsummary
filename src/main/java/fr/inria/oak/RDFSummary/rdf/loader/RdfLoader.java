package fr.inria.oak.RDFSummary.rdf.loader;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfLoader {

	/**
	 * @throws IOException
	 * @throws SQLException
	 * @throws UnsupportedDatabaseEngineException
	 * @throws DictionaryException
	 * @throws SchemaException
	 */
	public void loadDataset() throws IOException, SQLException, UnsupportedDatabaseEngineException, DictionaryException, SchemaException;
}
