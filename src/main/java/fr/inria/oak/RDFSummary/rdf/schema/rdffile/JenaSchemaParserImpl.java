package fr.inria.oak.RDFSummary.rdf.schema.rdffile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.reasoning.rdfs.BiMapSchemaValues;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Implementation of the SchemaParser interface based on the Jena OWL API.
 * The default string representation of a node in Jena quotes literals 
 * (including numbers) and appends the associated xsd:datatype.
 *   
 * However, Jena does not include angle brackets around the absolute datatype URI,
 * so the default string form will not parse within a standard SPARQL query.
 *   
 * Also, the Jena string representation of a variable doesn't include the 
 * preceding "?".
 * 
 * @author Damian BURSZTYN
 * @author Sejla CEBIRIC
 *
 */
public class JenaSchemaParserImpl implements SchemaParser {

	/** RDF model */
	protected Model model;

	protected Schema schema;

	/**
	 * Empty constructor
	 */
	public JenaSchemaParserImpl() {
		this.schema = new Schema();
	}

	/**
	 * Constructs a schema with default base URL and input format.
	 * 
	 * @param filepath Path to the RDFS file
	 * @throws FileNotFoundException 
	 */
	public JenaSchemaParserImpl(String filepath) throws FileNotFoundException {
		this(filepath, "", "");
	}

	/**
	 * Constructs a schema with default input format.
	 * 
	 * @param filepath Path to the RDFS file
	 * @param base The base URL
	 * @param lang
	 * @throws FileNotFoundException 
	 */
	public JenaSchemaParserImpl(String filepath, String base, String lang) throws FileNotFoundException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(filepath));

		model = ModelFactory.createDefaultModel();
		model.read(new FileReader(filepath), base, lang);
		this.schema = new Schema();
	}

	/**
	 * Constructs a schema with default base URL and input format.
	 * 
	 * @param reader the input reader
	 */
	public JenaSchemaParserImpl(Reader reader) {
		this(reader, "", "");
	}

	/**
	 * Constructs a schema with default input format.
	 * 
	 * @param reader the input reader
	 * @param base the base URL
	 */
	public JenaSchemaParserImpl(Reader reader, String base) {
		this(reader, base, "");
	}

	/**
	 * Constructs a schema with default input format.
	 * 
	 * @param reader the input reader
	 * @param base the base URL
	 * @param lang
	 */
	public JenaSchemaParserImpl(Reader reader, String base, String lang) {
		model = ModelFactory.createDefaultModel();
		model.read(reader, base, lang);
		this.schema = new Schema();
	}

	/**
	 * Reads in the schema from the specified file path and parses it to Schema object
	 * @param filepath
	 * @return The result of schema parsing
	 * @throws FileNotFoundException 
	 * @throws SchemaException 
	 */
	public Schema parseSchema(String filepath, String base, String lang) throws FileNotFoundException, SchemaException {
		model = ModelFactory.createDefaultModel();
		model.read(new FileReader(filepath), base, lang);

		return this.parseSchema();
	}

	/**
	 * Parses the whole RDF Schema from the input
	 * @throws SchemaException 
	 */
	@Override
	public Schema parseSchema() throws SchemaException {	
		// Classes
		Set<String> classes = backwardChain(model, RDF.type, RDFS.Class);
		// Properties
		Set<String> properties = backwardChain(model, RDF.type, RDF.Property);
		// Process the backward chaining

		// Sub-classes
		BiMapSchemaValues<String> subClassesBiMap = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcClasses = backwardChain(model, RDFS.subClassOf);
		for (final String superClass : bcClasses.keySet()) {	
			final Set<String> subClasses = flattenHierarchies(superClass, bcClasses, null);
			subClassesBiMap.put(superClass, subClasses);
			classes.add(superClass);
			classes.addAll(subClasses);
		}

		// Sub-properties
		BiMapSchemaValues<String> subPropertiesBiMap = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcProperties = backwardChain(model, RDFS.subPropertyOf);
		for (final String superProperty : bcProperties.keySet()) {
			final Set<String> subProperties = flattenHierarchies(superProperty, bcProperties, null);
			subPropertiesBiMap.put(superProperty, subProperties);
			properties.add(superProperty);
			properties.addAll(subProperties);
		}

		// Ranges
		BiMapSchemaValues<String> rangesBiMap = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcRanges = backwardChain(model, RDFS.range);
		for (final String range : bcRanges.keySet()) {
			final Set<String> propertiesSet = flattenHierarchies(range, bcRanges);
			rangesBiMap.put(range, propertiesSet);
			classes.add(range);
			properties.addAll(propertiesSet);			
		}

		// Domains
		BiMapSchemaValues<String> domainsBiMap = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcDomains = backwardChain(model, RDFS.domain);
		for (final String domain : bcDomains.keySet()) {
			final Set<String> propertiesSet = flattenHierarchies(domain, bcDomains);
			domainsBiMap.put(domain, propertiesSet);
			classes.add(domain);
			properties.addAll(propertiesSet);		
		}
		
		// Remove nulls
		classes.remove(null);
		properties.remove(null);
		
		final BiMapSchemaValues<String> subclasses = this.removeNulls(subClassesBiMap);
		final BiMapSchemaValues<String> subproperties = this.removeNulls(subPropertiesBiMap);
		final BiMapSchemaValues<String> domains = this.removeNulls(domainsBiMap);
		final BiMapSchemaValues<String> ranges = this.removeNulls(rangesBiMap);

		return new Schema(classes, properties, subclasses, subproperties, ranges, domains);
	}

	/**
	 * Browse an RDF-Schema and stores, for each object of a triple with the
	 * given property, its corresponding subject.
	 * @param model the parsed schema to be considered
	 * @param property the property on which to perform the backward chaining
	 * (e.g. subClassOf, subPropertyOf, range, domain)
	 * @return a map with object has keys, and a set of all subjects satisfying
	 * the property in the schema.
	 * @throws SchemaException
	 */
	protected Map<String, Set<String>> backwardChain(Model model, Property property) throws SchemaException {
		final Map<String, Set<String>> result = new HashMap<String, Set<String>>();

		// Selection of relevant tuple
		final StmtIterator iter = model.listStatements(new SimpleSelector(null, property, (RDFNode) null));
		Statement stmt = null;
		Resource subject = null;
		RDFNode object = null;
		String objectURI = null;
		Set<String> subSet = null;
		while (iter.hasNext()) {
			stmt = iter.next();
			subject = stmt.getSubject();
			object = stmt.getObject();
			if (object.isResource()/* && !object.isAnon()*/) {
				objectURI = ((Resource) object).getURI();
				subSet = result.get(objectURI);
				if (subSet == null) {
					subSet = new HashSet<String>();
				}
				subSet.add(subject.getURI());
				result.put(objectURI, subSet);
			} 
			else 
				throw new SchemaException("Object must be a resource in statement " + stmt);
		}

		return result;
	}

	/**
	 * Browse an RDF-Schema and stores, for each object of a triple with the
	 * given property, its corresponding subject.
	 * 
	 * @param model the parsed schema to be considered
	 * @param property the property on which to perform the backward chaining
	 * @param object the object
	 * (e.g. subClassOf, subPropertyOf, range, domain)
	 * @return a map with object has keys, and a set of all subjects satisfying
	 * the property in the schema.
	 */
	protected Set<String> backwardChain(Model model, Property property, Resource object) {
		final Set<String> result = new HashSet<String>();

		// Selection of relevant tuple
		final StmtIterator iter = model.listStatements(new SimpleSelector(null, property, object));
		Statement stmt = null;
		while (iter.hasNext()) {
			stmt = iter.next();			
			result.add(stmt.getSubject().getURI());
		}

		return result;
	}

	/**
	 * Recursively looks for sub-class/properties in the given map, and flattens
	 * @param property
	 * @return the flattened class/property hierarchy for the given key
	 */
	protected Set<String> flattenHierarchies(String key, Map<String, Set<String>> map, Set<String> history) {
		if (history == null) {
			history = new HashSet<String>();
		}
		final Set<String> items = map.get(key);
		if (items != null) {
			for (final String item : items) {
				// Too avoid cycle, we only go deeper if the sub-pro was not in history
				if (history.add(item)) {
					history.addAll(flattenHierarchies(item, map, history));
				}
			}
		}
		return history;
	}

	/**
	 * Looks for sub-class/properties of a given range or domain, and flatten
	 * their hirerachy
	 * @param property
	 * @return the flattened class/property hierarchy for the given key
	 */
	protected Set<String> flattenHierarchies(String key, Map<String, Set<String>> map) {
		final Set<String> result = new HashSet<String>();
		Set<String> subProperties = null;
		final Set<String> subClasses = schema.getSubClasses(key);
		Set<String> properties = map.get(key);
		if (properties != null) {
			result.addAll(properties);
		}
		if (subClasses != null) {
			for (final String subClass : subClasses) {
				properties = map.get(subClass);
				if (properties != null) {
					result.addAll(properties);
					for (final String property : properties) {
						subProperties = schema.getSubProperties(property);
						if (subProperties != null) {
							result.addAll(subProperties);
						}
					}
				}
			}
		}

		return result;
	}
	
	/**
	 * Removes entries comprising null keys or values.
	 * 
	 * @param biMap
	 * @return The new biMap
	 */
	private BiMapSchemaValues<String> removeNulls(final BiMapSchemaValues<String> biMap) {
		final BiMapSchemaValues<String> newBiMap = new BiMapSchemaValues<String>();
		for (String key : biMap.getMap().keySet()) {
			if (StringUtils.isNullOrBlank(key))
				continue;

			final Set<String> nonNullValues = Sets.newHashSet();
			final Set<String> values = biMap.getMap().get(key);
			for (String value : values)  {
				if (StringUtils.isNullOrBlank(value))
					continue;
				
				nonNullValues.add(value);
			}
			newBiMap.put(key, nonNullValues);
		}

		return newBiMap;
	}
}
