package fr.inria.oak.RDFSummary.rdf.handler;

import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.helpers.AbstractRDFHandler;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.rdf.dataset.clique.MapDbCliqueRdfDataset;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;

import java.sql.SQLException;

import org.openrdf.model.Statement;

public class MapDbCliqueRdfHandlerImpl extends AbstractRDFHandler {

	private final Dictionary dictionary;
	private final MapDbCliqueRdfDataset rdfDataset;	

	private Integer encodedSubject;
	private Integer encodedProperty;
	private Integer encodedObject;

	/**
	 * @param dictionary
	 * @param rdfDataset
	 */
	public MapDbCliqueRdfHandlerImpl(final Dictionary dictionary, final MapDbCliqueRdfDataset rdfDataset) {
		this.dictionary = dictionary;
		this.rdfDataset = rdfDataset;
	}

	@Override
	public void handleStatement(final Statement st) throws RDFHandlerException {
		try {
			encodedSubject = DictionaryUtils.getKey(dictionary, st.getSubject().toString());
			encodedProperty = DictionaryUtils.getKey(dictionary, st.getPredicate().toString());
			encodedObject = DictionaryUtils.getKey(dictionary, st.getObject().toString());

			rdfDataset.addProperty(encodedProperty);

			if (rdfDataset.isTypeProperty(encodedProperty)) {
				rdfDataset.addClass(encodedObject);
				rdfDataset.addTypeTriple(encodedSubject, encodedObject);
			}
			else if (rdfDataset.isSchemaProperty(encodedProperty)) {
				rdfDataset.addSchemaTriple(encodedSubject, encodedProperty, encodedObject);
			}
			else {
				rdfDataset.addDataNode(encodedSubject); 
				rdfDataset.addDataNode(encodedObject);
				rdfDataset.addDataPropertyBySubject(encodedSubject, encodedProperty);
				rdfDataset.addDataPropertyByObject(encodedObject, encodedProperty);
				rdfDataset.addDataTriple(encodedSubject, encodedProperty, encodedObject); 
			} 
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RDFHandlerException(e.getMessage());
		} 
	}
}