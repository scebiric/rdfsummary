package fr.inria.oak.RDFSummary.rdf.dataset.clique;

import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;
import org.mapdb.Serializer;

import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.rdf.RdfProperty;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.MapDbSchemaImpl;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbCliqueRdfDatasetImpl implements MapDbCliqueRdfDataset {
	
	private final Set<Integer> classes;
	private final Set<Integer> properties;
	private final Set<Integer> dataNodes; 
	private final NavigableSet<Tuple2<Integer, Integer>> dataPropertiesBySubject;
	private final NavigableSet<Tuple2<Integer, Integer>> dataPropertiesByObject;
	private final NavigableSet<Tuple3<Integer, Integer, Integer>> dataTriples;
	private final NavigableSet<Tuple2<Integer, Integer>> typeTriples;
	private final Schema schema;
	
	private final Map<String, Integer> rdfPropertyKeys;
	
	private static final String CLASSES = "classes";
	private static final String PROPERTIES = "properties";
	private static final String DATA_NODES = "data_nodes";
	private static final String DATA_PROPERTIES_BY_SUBJECT = "dataPropertiesBySubject";
	private static final String DATA_PROPERTIES_BY_OBJECT = "dataPropertiesByObject";
	private static final String DATA_TRIPLES = "data_triples";
	private static final String TYPE_TRIPLES = "type_triples";
	
	/**
	 * @param datasetDb
	 */
	public MapDbCliqueRdfDatasetImpl(final DB datasetDb) {	
		this.classes = datasetDb.createHashSet(CLASSES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.properties = datasetDb.createHashSet(PROPERTIES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.dataNodes = datasetDb.createHashSet(DATA_NODES).counterEnable().serializer(Serializer.INTEGER).makeOrGet();
		this.dataPropertiesBySubject = datasetDb.createTreeSet(DATA_PROPERTIES_BY_SUBJECT).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		this.dataPropertiesByObject = datasetDb.createTreeSet(DATA_PROPERTIES_BY_OBJECT).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		this.dataTriples = datasetDb.createTreeSet(DATA_TRIPLES).counterEnable().serializer(BTreeKeySerializer.TUPLE3).makeOrGet();
		this.typeTriples = datasetDb.createTreeSet(TYPE_TRIPLES).counterEnable().serializer(BTreeKeySerializer.TUPLE2).makeOrGet();
		this.schema = new MapDbSchemaImpl(datasetDb);
		
		this.rdfPropertyKeys = Maps.newHashMap();
	}

	@Override
	public void addClass(final int classIRI) {
		classes.add(classIRI);
	}

	@Override
	public Iterator<Integer> getClasses() {
		return classes.iterator();
	}

	@Override
	public void addDataPropertyBySubject(final int subject, final int property) {
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(subject, property));
	}

	@Override
	public Iterator<Integer> getDataPropertiesBySubject(final int subject) {
		return Fun.filter(dataPropertiesBySubject, subject).iterator();
	}

	@Override
	public void addDataPropertyByObject(final int object, final int property) {
		dataPropertiesByObject.add(new Tuple2<Integer, Integer>(object, property));
	}

	@Override
	public Iterator<Integer> getDataPropertiesByObject(int object) {
		return Fun.filter(dataPropertiesByObject, object).iterator();
	}

	@Override
	public boolean isLoaded() {
		return dataTriples.size() > 0 || typeTriples.size() > 0;
	}

	@Override
	public void addDataNode(final int dataNode) {
		dataNodes.add(dataNode);
	}

	@Override
	public Iterator<Integer> getDataNodes() {
		return dataNodes.iterator();
	}

	@Override
	public void addDataTriple(final int subject, final int property, final int object) {
		dataTriples.add(new Tuple3<Integer, Integer, Integer>(subject, property, object));		
	}

	@Override
	public void addTypeTriple(final int subject, final int object) {
		typeTriples.add(new Tuple2<Integer, Integer>(subject, object)); 		
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getDataTriples() {
		return dataTriples.iterator();		
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getTypeTriples() {
		return typeTriples.iterator();		
	}

	@Override
	public Schema getSchema() {
		return schema;
	}

	@Override
	public void addRdfProperty(String rdfProperty, int key) {
		rdfPropertyKeys.put(rdfProperty, key);
	}

	@Override
	public boolean isTypeProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_TYPE).intValue() || property == rdfPropertyKeys.get(RdfProperty.TYPE).intValue();
	}

	@Override
	public boolean isDomainProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_DOMAIN).intValue()  || property == rdfPropertyKeys.get(RdfProperty.DOMAIN).intValue();
	}

	@Override
	public boolean isRangeProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_RANGE).intValue()  || property == rdfPropertyKeys.get(RdfProperty.RANGE).intValue() ;
	}

	@Override
	public boolean isSubClassOfProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_SUBCLASS).intValue()  || property == rdfPropertyKeys.get(RdfProperty.SUBCLASS).intValue() ;
	}

	@Override
	public boolean isSubPropertyOfProperty(int property) {
		return property == rdfPropertyKeys.get(RdfProperty.FULL_SUBPROPERTY).intValue()  || property == rdfPropertyKeys.get(RdfProperty.SUBPROPERTY).intValue() ;
	}

	@Override
	public void addSchemaTriple(int encodedSubject, int encodedProperty, int encodedObject) {
		if (isDomainProperty(encodedProperty))
			addDomainClassByProperty(encodedSubject, encodedObject, encodedProperty);
		
		else if (isRangeProperty(encodedProperty))
			addRangeClassByProperty(encodedSubject, encodedObject, encodedProperty);
		
		else if (isSubClassOfProperty(encodedProperty))
			addSuperClassByClass(encodedSubject, encodedObject, encodedProperty);
		
		else if (isSubPropertyOfProperty(encodedProperty))
			addSuperPropertyByProperty(encodedSubject, encodedObject, encodedProperty);
	}

	@Override
	public boolean isSchemaProperty(int property) {
		return isDomainProperty(property) || isRangeProperty(property) || isSubClassOfProperty(property) || isSubPropertyOfProperty(property);
	}

	@Override
	public void addDomainClassByProperty(int property, int domain, int rdfDomainProperty) {
		schema.addDomainClassByProperty(property, domain, rdfDomainProperty);
		addProperty(property);
		addClass(domain);
	}

	@Override
	public void addRangeClassByProperty(int property, int range, int rdfRangeProperty) {
		schema.addRangeClassByProperty(property, range, rdfRangeProperty);
		addProperty(property);
		addClass(range);
	}

	@Override
	public void addSuperClassByClass(int classIRI, int superClassIRI, int rdfSubClassOfProperty) {
		schema.addSuperClassByClass(classIRI, superClassIRI, rdfSubClassOfProperty);
		addClass(classIRI);
		addClass(superClassIRI);
	}

	@Override
	public void addSuperPropertyByProperty(int property, int superProperty, int rdfSubPropertyOfProperty) {
		schema.addSuperPropertyByProperty(property, superProperty, rdfSubPropertyOfProperty);
		addProperty(property);
		addProperty(superProperty);
	}

	@Override
	public int getClassesCount() {
		return classes.size();
	}
	
	@Override
	public int getDataNodeCount() {
		return dataNodes.size();
	}

	@Override
	public void addProperty(int property) {
		properties.add(property);	
	}

	@Override
	public Iterator<Integer> getProperties() {
		return properties.iterator();
	}

	@Override
	public boolean isClass(int iri) {
		return classes.contains(iri);
	}

	@Override
	public boolean isProperty(int iri) {
		return properties.contains(iri);
	}

	@Override
	public boolean isClassOrProperty(int iri) {
		return isClass(iri) || isProperty(iri);
	}

	@Override
	public Map<String, Integer> getRdfPropertyKeys() {
		return rdfPropertyKeys;
	}
}
