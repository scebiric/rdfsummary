package fr.inria.oak.RDFSummary.rdf.loader;

import java.io.IOException;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.ntriples.NTriplesParser;

import com.google.common.base.Preconditions;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RioRdfLoaderImpl implements RdfLoader {

	private static final Logger log = Logger.getLogger(RdfLoader.class);
	
	private final RdfDataset rdfDataset;
	private final String dataFilepath;
	private final Timer timer;
	
	private final RDFParser parser;
	private final MessageBuilder messageBuilder;
	
	/**
	 * @param rdfDataset
	 * @param dataFilepath
	 * @param rdfHandler
	 * @param timer
	 */
	public RioRdfLoaderImpl(final RdfDataset rdfDataset, final String dataFilepath, final RDFHandler rdfHandler, final Timer timer) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataFilepath));
		
		this.rdfDataset = rdfDataset;
		this.dataFilepath = dataFilepath;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
		this.parser = new NTriplesParser();
		parser.setRDFHandler(rdfHandler);
	}

	@Override
	public void loadDataset() throws IOException, SQLException {
		timer.reset();
		if (rdfDataset.isLoaded()) {
			log.info("Dataset already loaded.");
			return;
		}
		
		timer.start();
		log.info(messageBuilder.append("Loading dataset from ").append(dataFilepath).append(Constant.THREE_DOTS));   
		
		RdfLoaderUtils.loadRdfProperties();
		RdfLoaderUtils.parse(dataFilepath, parser);
		timer.stop();
		log.info(messageBuilder.append("Dataset loaded in: ").append(timer.getTimeAsString()));
	}
}
