package fr.inria.oak.RDFSummary.rdf.dataset;

import java.util.Iterator;

import org.mapdb.Fun.Tuple2;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface MapDbRdfDataset {

	/**
	 * @return An iterator over type triples
	 */
	public Iterator<Tuple2<Integer, Integer>> getTypeTriples();
}
