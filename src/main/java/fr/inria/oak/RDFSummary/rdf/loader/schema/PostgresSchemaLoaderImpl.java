package fr.inria.oak.RDFSummary.rdf.loader.schema;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.Serializer;

import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timing.RdfLoaderTiming;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresSchemaLoaderImpl implements PostgresSchemaLoader {

	private static final Logger log = Logger.getLogger(PostgresSchemaLoader.class);
	
	private final SchemaLoader schemaLoader;
	private final PostgresBisimulationRdfDataset rdfDataset;
	private final TriplesDao triplesDao;
	private final Timer timer;
	private final Timings timings;
	
	private final Map<String, Boolean> schemaLoadedFromTriplesTable;
	
	private static final String SCHEMA_LOADED_FROM_TRIPLES_TABLE = "schema_loaded_from_triples_table";
	
	/**
	 * @param schemaLoader
	 * @param rdfDataset
	 * @param datasetDb
	 * @param triplesDao
	 * @param timer
	 * @param timings
	 */
	public PostgresSchemaLoaderImpl(final SchemaLoader schemaLoader, 
			final PostgresBisimulationRdfDataset rdfDataset, final DB datasetDb,
			final TriplesDao triplesDao, final Timer timer, final Timings timings) {
		this.schemaLoader = schemaLoader;
		this.rdfDataset = rdfDataset;
		this.triplesDao = triplesDao;
		this.timer = timer;
		this.timings = timings;
		
		this.schemaLoadedFromTriplesTable = datasetDb.createHashMap(SCHEMA_LOADED_FROM_TRIPLES_TABLE).counterEnable().keySerializer(Serializer.STRING).valueSerializer(Serializer.BOOLEAN).makeOrGet();	
	}
	
	@Override
	public void loadSchemaFromFile() throws FileNotFoundException, SchemaException, IOException, SQLException {
		schemaLoader.loadSchemaFromFile();
	}

	@Override
	public void loadSchemaFromTriplesTable() throws SQLException {
		final Boolean loaded = schemaLoadedFromTriplesTable.get(SCHEMA_LOADED_FROM_TRIPLES_TABLE);
		if (loaded != null && loaded) {
			log.info("Schema already loaded from table.");
			return;
		}
		
		log.info("Loading schema triples from the table of all triples...");
		timer.reset();
		timer.start();
		triplesDao.prepareSelectStatementsFromAllTriples();
		RdfLoaderUtils.loadRdfPropertiesWithBrackets();
		final Map<String, Integer> propertyKeys = rdfDataset.getRdfPropertyKeys();
		for (final String property : propertyKeys.keySet()) {
			if (property.equals(Rdf.FULL_TYPE) || property.equals(Rdf.TYPE))
				continue;

			getSchemaTriples(propertyKeys.get(property).intValue());

		}
		triplesDao.closeSelectStatementsFromAllTriples();
		timer.stop();
		timings.addTime(RdfLoaderTiming.LOADING_SCHEMA_FROM_TRIPLES_TABLE, timer.getTimeInMilliseconds());
		schemaLoadedFromTriplesTable.put(SCHEMA_LOADED_FROM_TRIPLES_TABLE, true);
	}
	
	private void getSchemaTriples(int rdfsProperty) throws SQLException {
		final ResultSet schemaTriples = triplesDao.getSubjectObjectPairsByPropertyFromAllTriples(rdfsProperty);
		while (schemaTriples.next()) {
			rdfDataset.addSchemaTriple(schemaTriples.getInt(1), rdfsProperty, schemaTriples.getInt(2));
		}
		schemaTriples.close();
	}
}
