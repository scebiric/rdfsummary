package fr.inria.oak.RDFSummary.rdf.schema.rdffile;

import java.io.FileNotFoundException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * SchemaParser interface provides functionalities for loading an RDF Schema
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface SchemaParser {

	/** Parses the whole RDF Schema 
	 * @throws SchemaException */
	public Schema parseSchema() throws SchemaException;
	
	/** Reads in the schema from the specified file path and parses it to Schema object 
	 * @throws FileNotFoundException 
	 * @throws SchemaException */
	public Schema parseSchema(String filepath, String base, String lang) throws FileNotFoundException, SchemaException;
}
