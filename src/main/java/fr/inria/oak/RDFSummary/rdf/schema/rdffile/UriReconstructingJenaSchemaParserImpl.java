package fr.inria.oak.RDFSummary.rdf.schema.rdffile;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.reasoning.rdfs.BiMapSchemaValues;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * Extends the {@link JenaSchemaParserImpl} to enclose all the URIs in angle brackets.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class UriReconstructingJenaSchemaParserImpl extends JenaSchemaParserImpl {
	
	private static final String LEFT_ANGLE = "<";
	private static final String RIGHT_ANGLE = ">";

	private static StringBuilder Builder;
	
	/**
	 * Empty constructor
	 */
	public UriReconstructingJenaSchemaParserImpl() {
		super();
	}

	/**
	 * Constructs a CustomJenaSchemaParser instance based on the specified file path to schema
	 * 
	 * @param filepath
	 * @throws FileNotFoundException 
	 */
	public UriReconstructingJenaSchemaParserImpl(final String filepath) throws FileNotFoundException {
		super(filepath);
	}

	@Override
	public Schema parseSchema() throws SchemaException {	
		// Classes
		final Set<String> classesTemp = backwardChain(model, RDF.type, RDFS.Class);
		// Properties
		final Set<String> propertiesTemp = backwardChain(model, RDF.type, RDF.Property);
		// Process the backward chaining

		// Sub-classes
		final BiMapSchemaValues<String> subClassesTemp = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcClasses = backwardChain(model, RDFS.subClassOf);
		for (final String superClass : bcClasses.keySet()) {	
			final Set<String> subClasses = flattenHierarchies(superClass, bcClasses, null);
			subClassesTemp.put(superClass, subClasses);
			classesTemp.add(superClass);
			classesTemp.addAll(subClasses);
		}

		// Sub-properties
		final BiMapSchemaValues<String> subPropertiesTemp = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcProperties = backwardChain(model, RDFS.subPropertyOf);
		for (final String superProperty : bcProperties.keySet()) {
			final Set<String> subProperties = flattenHierarchies(superProperty, bcProperties, null);
			subPropertiesTemp.put(superProperty, subProperties);
			propertiesTemp.add(superProperty);
			propertiesTemp.addAll(subProperties);
		}

		// Ranges
		final BiMapSchemaValues<String> rangesTemp = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcRanges = backwardChain(model, RDFS.range);
		for (final String range : bcRanges.keySet()) {
			final Set<String> propertiesSet = flattenHierarchies(range, bcRanges);
			rangesTemp.put(range, propertiesSet);
			classesTemp.add(range);
			propertiesTemp.addAll(propertiesSet);			
		}

		// Domains
		final BiMapSchemaValues<String> domainsTemp = new BiMapSchemaValues<String>();
		final Map<String, Set<String>> bcDomains = backwardChain(model, RDFS.domain);
		for (final String domain : bcDomains.keySet()) {
			final Set<String> propertiesSet = flattenHierarchies(domain, bcDomains);
			domainsTemp.put(domain, propertiesSet);
			classesTemp.add(domain);
			propertiesTemp.addAll(propertiesSet);		
		}

		Set<String> classes = Sets.newHashSet();
		for (String classURI : classesTemp)
			classes.add(this.encloseInAngleBrackets(classURI));

		Set<String> properties = Sets.newHashSet();
		for (String propertyURI : propertiesTemp)
			properties.add(this.encloseInAngleBrackets(propertyURI));

		final BiMapSchemaValues<String> subclasses = this.encloseValuesInAngleBrackets(subClassesTemp);
		final BiMapSchemaValues<String> subproperties = this.encloseValuesInAngleBrackets(subPropertiesTemp);
		final BiMapSchemaValues<String> domains = this.encloseValuesInAngleBrackets(domainsTemp);
		final BiMapSchemaValues<String> ranges = this.encloseValuesInAngleBrackets(rangesTemp);
	
		// Remove nulls
		classes.remove(null);
		properties.remove(null);

		return new Schema(classes, properties, subclasses, subproperties, ranges, domains);
	}

	/**
	 * Encloses each key and value from the specified biMap in angle brackets.
	 * Removes entries comprising null keys or values.
	 * 
	 * @param biMap
	 * @return The new biMap
	 */
	private BiMapSchemaValues<String> encloseValuesInAngleBrackets(BiMapSchemaValues<String> biMap) {
		BiMapSchemaValues<String> newBiMap = new BiMapSchemaValues<String>();
		for (String key : biMap.getMap().keySet()) {
			if (StringUtils.isNullOrBlank(key))
				continue;

			String newKey = this.encloseInAngleBrackets(key);
			Set<String> newValues = Sets.newHashSet();
			Set<String> values = biMap.getMap().get(key);
			for (String value : values)  {
				if (StringUtils.isNullOrBlank(value))
					continue;
				
				newValues.add(this.encloseInAngleBrackets(value));
			}
			newBiMap.put(newKey, newValues);
		}

		return newBiMap;
	}

	/**
	 * Encloses the specified string in angle brackets
	 * 
	 * @param str
	 * @return The new string enclosed in angle brackets
	 */
	private String encloseInAngleBrackets(String str) {
		if (StringUtils.isNullOrBlank(str))
			return str;

		Builder = new StringBuilder();
		Builder.append(LEFT_ANGLE).append(str).append(RIGHT_ANGLE);
		
		return Builder.toString();
	}

}
