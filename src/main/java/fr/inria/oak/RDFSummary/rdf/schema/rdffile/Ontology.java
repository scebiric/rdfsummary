package fr.inria.oak.RDFSummary.rdf.schema.rdffile;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public abstract class Ontology {
	
	/**
	 * Indicates if the <code>predicate</code> is a role (aka. property) in the
	 * ontology.
	 *
	 * @param predicate
	 * @return <code>true</code> if the given predicate is a role;
	 *         <code>false</code> otherwise
	 */
	public abstract boolean isRole(final String predicate);

	/**
	 * Indicates if the <code>predicate</code> is a concept (aka. class) in the
	 * ontology.
	 *
	 * @param predicate
	 * @return <code>true</code> if the given predicate is a concept;
	 *         <code>false</code> otherwise
	 */
	public abstract boolean isConcept(final String predicate);
}
