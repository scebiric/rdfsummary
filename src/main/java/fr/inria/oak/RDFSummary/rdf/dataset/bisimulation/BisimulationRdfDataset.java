package fr.inria.oak.RDFSummary.rdf.dataset.bisimulation;

import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationRdfDataset extends RdfDataset {
	
}
