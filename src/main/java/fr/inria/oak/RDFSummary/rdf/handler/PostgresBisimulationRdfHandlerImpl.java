package fr.inria.oak.RDFSummary.rdf.handler;

import java.sql.SQLException;

import org.openrdf.model.Statement;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.helpers.AbstractRDFHandler;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class PostgresBisimulationRdfHandlerImpl extends AbstractRDFHandler {

	private final Dictionary dictionary;
	private final PostgresBisimulationRdfDataset rdfDataset;

	private Integer subject;
	private Integer property;
	private Integer object;

	/**
	 * @param dictionary
	 * @param rdfDataset
	 */
	public PostgresBisimulationRdfHandlerImpl(final Dictionary dictionary, final PostgresBisimulationRdfDataset rdfDataset) {
		this.dictionary = dictionary;
		this.rdfDataset = rdfDataset;
	}

	@Override
	public void handleStatement(final Statement st) throws RDFHandlerException {
		try {
			subject = DictionaryUtils.getKey(dictionary, st.getSubject().toString());
			property = DictionaryUtils.getKey(dictionary, st.getPredicate().toString());
			object = DictionaryUtils.getKey(dictionary, st.getObject().toString());


			rdfDataset.addProperty(property);
			if (rdfDataset.isTypeProperty(property)) {
				rdfDataset.addClass(object);
				rdfDataset.addTypeTriple(subject, object);
			}
			else if (rdfDataset.isSchemaProperty(property)) {
				rdfDataset.addSchemaTriple(subject, property, object);
			}
			else {
				rdfDataset.addDataNode(subject); 
				rdfDataset.addDataNode(object);
				rdfDataset.addDataTriple(subject, property, object);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RDFHandlerException(e.getMessage());
		} 
	}
}
