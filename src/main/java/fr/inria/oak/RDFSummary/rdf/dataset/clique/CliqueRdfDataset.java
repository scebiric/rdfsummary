package fr.inria.oak.RDFSummary.rdf.dataset.clique;

import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface CliqueRdfDataset extends RdfDataset {
	
	/**
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void addDataTriple(int subject, int property, int object);
}
