package fr.inria.oak.RDFSummary.rdf.loader.schema;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PostgresSchemaLoader extends SchemaLoader {

	public void loadSchemaFromTriplesTable() throws SQLException; 
}
