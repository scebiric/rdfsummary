package fr.inria.oak.RDFSummary.urigenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface UriGenerator {

	/**
	 * @param dataNodeId
	 * @return A data node URI generated based on the specified id.
	 */
	public String generateDataNodeUri(int dataNodeId); 

}
