package fr.inria.oak.RDFSummary.urigenerator;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class NoBracketsUriGeneratorImpl implements UriGenerator {

	private final String schemaName;
	private final String summaryType;
	
	private static StringBuilder Builder;
	private static final String HTTP = "http";
	private static final String DATA = "data";
	
	/**
	 * @param schemaName
	 * @param summaryType
	 */
	public NoBracketsUriGeneratorImpl(final String schemaName, final String summaryType) {
		this.schemaName = schemaName;
		this.summaryType = summaryType;
	}

	@Override
	public String generateDataNodeUri(int dataNodeId) {
		Builder = new StringBuilder();
		Builder.append(HTTP).append(Chars.COLON).append(Chars.SLASH).append(Chars.SLASH);
		Builder.append(schemaName).append(Chars.SLASH).append(summaryType);
		Builder.append(Chars.SLASH).append(DATA).append(Chars.SLASH);
		Builder.append(Constant.DATA_NODE_PREFIX).append(dataNodeId);
		
		return Builder.toString();
	}

}
