package fr.inria.oak.RDFSummary.timer;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TimerImpl implements Timer {

	private final Stopwatch stopwatch;
	
	private static StringBuilder Builder;
	private static final String MS = " ms";
	
	public TimerImpl() {
		this.stopwatch = Stopwatch.createUnstarted();
	}
	
	public void start() {
		this.stopwatch.start();		
	}

	public void stop() {
		this.stopwatch.stop();		
	}

	public void reset() {
		this.stopwatch.reset();
	}

	public long getTimeInMilliseconds() {
		return this.stopwatch.elapsed(TimeUnit.MILLISECONDS);
	}

	public String getTimeAsString() {
		Builder = new StringBuilder(Long.toString(this.getTimeInMilliseconds()));
		Builder.append(MS);
		
		return Builder.toString();
	}	
}
