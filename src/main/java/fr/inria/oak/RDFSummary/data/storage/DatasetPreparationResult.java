package fr.inria.oak.RDFSummary.data.storage;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DatasetPreparationResult {

	private final PostgresTableNames tables;
	private final Schema schema;	
	private final TriplesLoadingTimes triplesLoadingTimes;
	private final EncodingTimes encodingTimes;
	private final long saturationTime;
	
	/**
	 * @param tables
	 * @param schema
	 * @param triplesLoadingTimes
	 * @param encodingTimes
	 */
	public DatasetPreparationResult(final PostgresTableNames tables, final Schema schema, final TriplesLoadingTimes triplesLoadingTimes, final EncodingTimes encodingTimes) {
		this.tables = tables;
		this.schema = schema;
		this.triplesLoadingTimes = triplesLoadingTimes;
		this.encodingTimes = encodingTimes;
		this.saturationTime = -1;
	}
	
	/**
	 * @param tables
	 * @param schema
	 * @param triplesLoadingTimes
	 * @param encodingTimes
	 * @param saturationTime
	 */
	public DatasetPreparationResult(final PostgresTableNames tables, final Schema schema, final TriplesLoadingTimes triplesLoadingTimes, final EncodingTimes encodingTimes, final long saturationTime) {
		this.tables = tables;
		this.schema = schema;
		this.triplesLoadingTimes = triplesLoadingTimes;
		this.encodingTimes = encodingTimes;
		this.saturationTime = saturationTime;
	}

	public PostgresTableNames getPostgresTableNames() {
		return tables;
	}
	
	public Schema getSchema() {
		return schema;
	}

	public TriplesLoadingTimes getTriplesLoadingTimes() {
		return triplesLoadingTimes;
	}
	
	public EncodingTimes getEncodingTimes() {
		return encodingTimes;
	}
	
	public long getSaturationTime() {
		Preconditions.checkArgument(saturationTime > -1);
		
		return saturationTime;
	}
}
