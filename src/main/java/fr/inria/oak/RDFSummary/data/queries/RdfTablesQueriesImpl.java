package fr.inria.oak.RDFSummary.data.queries;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.Dictionary;

/**
 * @author Sejla CEBIRIC
 *
 */
public class RdfTablesQueriesImpl implements RdfTablesQueries {

	private static StringBuilder Builder = null;

	private static final String URI = "uri";
	private static final String PNODES_ALIAS = "pnodes";
	private static final String SPO = "s, p, o";

	@Override
	public String distinctSpOrderByS(String dataTable) {
		return "SELECT DISTINCT s, p FROM " + dataTable + " ORDER BY s;";
	}

	@Override
	public String distinctOpOrderByO(String dataTable) {
		return "SELECT DISTINCT o, p FROM " + dataTable + " ORDER BY o;";
	}

	@Override
	public String distinctSpOrderBySWithSUntyped(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.s, data.p, typ.o AS " + Constant.TYPE_ALIAS + " " 
				+ "FROM " + dataTable + " data "
				+ "LEFT OUTER JOIN " + typesTable + " typ "
				+ "ON data.s=typ.s "
				+ "WHERE typ.o IS NULL "
				+ "ORDER BY data.s;";
	}

	@Override
	public String distinctOpOrderByOWithOUntyped(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.o, data.p, typ.o AS " + Constant.TYPE_ALIAS + " " 
				+ "FROM " + dataTable + " data "
				+ "LEFT OUTER JOIN " + typesTable + " typ "
				+ "ON data.o=typ.s "
				+ "WHERE typ.o IS NULL "
				+ "ORDER BY data.o;";
	}	

	@Override
	public String subjectTypesByProperties(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.p, type.o "
				+ "FROM " + dataTable + " data JOIN " + typesTable + " type ON data.s = type.s;";
	}

	@Override
	public String objectTypesByProperties(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.p, type.o "
				+ "FROM " + dataTable + " data JOIN " + typesTable + " type ON data.o = type.s;";
	}

	@Override
	public String createTriplesTable(String table) {
		return "CREATE TABLE " + table + "(" + Constant.SUBJECT + " text NOT NULL, " + Constant.PROPERTY + " text NOT NULL, " + Constant.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createTriplesTableWithSkippedChars(String table) {
		return "CREATE TABLE " + table + "(" + Constant.SUBJECT + " text NOT NULL, " + Constant.PROPERTY + " text NOT NULL, " + Constant.OBJECT +" text NOT NULL, " + Constant.SKIPPED_CHARS + " text);";
	}

	@Override
	public String createEncodedTriplesTable(String encodedTableName) {
		return "CREATE TABLE " + encodedTableName + "(" + Constant.SUBJECT + " integer NOT NULL, " + Constant.PROPERTY + " integer NOT NULL, " + Constant.OBJECT +" integer NOT NULL);";
	}

	@Override
	public String createEncodedTriplesTableWithId(String encodedTableName) {
		return "CREATE TABLE " + encodedTableName + "(id SERIAL UNIQUE NOT NULL, " + Constant.SUBJECT + " integer NOT NULL, " + Constant.PROPERTY + " integer NOT NULL, " + Constant.OBJECT +" integer NOT NULL);";
	}

	@Override
	public String createDataTriplesTable(String table) {
		return "CREATE TABLE " + table + "(id SERIAL UNIQUE NOT NULL, " + Constant.SUBJECT + " text NOT NULL, " + Constant.PROPERTY + " text NOT NULL, " + Constant.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createTypesTable(String typesTable) {
		return "CREATE TABLE " + typesTable + "(id SERIAL UNIQUE NOT NULL, " + Constant.SUBJECT + " text NOT NULL, " + Constant.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createEncodedTypesTableWithId(String encodedTypesTable) {
		return "CREATE TABLE " + encodedTypesTable + "(id SERIAL UNIQUE NOT NULL, " + Constant.SUBJECT + " integer NOT NULL, " + Constant.OBJECT +" integer NOT NULL);";
	}

	@Override
	public String createEncodedUriTable(String table) {
		return "CREATE TABLE " + table + "(uri integer NOT NULL);";
	}

	@Override
	public String populateTypesTable(String typesTable, String triplesTable) {
		return "INSERT INTO " + typesTable + " (s, o) (SELECT s, o FROM " + triplesTable + " WHERE p='" + Rdf.FULL_TYPE + "' OR p='" + Rdf.TYPE + "')";
	}

	@Override
	public String insert(String targetTable, String sourceTable, String sourceColumn, int propertyKey) {
		return "INSERT INTO " + targetTable + " (SELECT DISTINCT " + sourceColumn + " FROM " + sourceTable + " WHERE p=" + propertyKey + ");";
	}

	@Override
	public String insertClasses(String clsTable, String encodedTypesTable, String column) {
		return "INSERT INTO " + clsTable + " (SELECT DISTINCT " + column + " FROM " + encodedTypesTable + ");";
	}

	@Override
	public String populateEncodedTypesTable(String encodedTypesTable, String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null);

		return "INSERT INTO " + encodedTypesTable + " (s, o) (SELECT s, o FROM " + encodedTriplesTable + 
				" WHERE " + RdfUtils.getTypePropertyFilter(Constant.PROPERTY, fullTypeKey, shortTypeKey) + ");";
	}

	@Override
	public String populateDataTriplesTable(String dataTable, String triplesTable) {
		return "INSERT INTO " + dataTable + " (s, p, o) (SELECT s, p, o FROM " + triplesTable + " WHERE " + 
				"p <> '" + Rdf.FULL_TYPE + "' AND " + 
				"p <> '" + Rdf.TYPE + "' AND " + 
				"p <> '" + Rdfs.FULL_DOMAIN + "' AND " +
				"p <> '" + Rdfs.DOMAIN + "' AND " +
				"p <> '" + Rdfs.FULL_RANGE + "' AND " +
				"p <> '" + Rdfs.RANGE + "' AND " +
				"p <> '" + Rdfs.FULL_SUBCLASS + "' AND " +
				"p <> '" + Rdfs.SUBCLASS + "' AND " +
				"p <> '" + Rdfs.FULL_SUBPROPERTY + "' AND " +
				"p <> '" + Rdfs.SUBPROPERTY + "')";
	}

	@Override
	public String populateEncodedDataTriplesTable(String encodedDataTable, String encodedTriplesTable, Dictionary dictionary) {
		String query = "INSERT INTO " + encodedDataTable + " (s, p, o) (SELECT s, p, o FROM " + encodedTriplesTable;
		String filter = getDataPropertyFilter(Constant.PROPERTY, dictionary);
		if (!StringUtils.isNullOrBlank(filter))
			query += " WHERE " + filter;

		return query.concat(");");
	}

	@Override
	public String createClassesAndPropertiesTable(String clsPropsTable, String encodedDataTable, String encodedTypesTable) {
		return "CREATE TABLE " + clsPropsTable + 
				" AS (SELECT DISTINCT p AS uri FROM " + encodedDataTable + 
				" UNION SELECT DISTINCT o AS uri FROM " + encodedTypesTable + ");";
	}

	@Override
	public String createTypeSubjectsAndClassesTable(String subjClsTable, String summaryTypesTable) {
		return "CREATE TABLE " + subjClsTable + 
				" AS (SELECT DISTINCT o AS uri FROM " + summaryTypesTable + 
				" UNION SELECT DISTINCT s AS uri FROM " + summaryTypesTable + ");";
	}

	@Override
	public String createDataPropertiesTable(String propsTable, String encodedDataTable) {
		return "CREATE TABLE " + propsTable + 
				" AS (SELECT DISTINCT p AS uri FROM " + encodedDataTable + ");";
	}

	@Override
	public String createClassesTable(String clsTable, String encodedTypesTable) {
		return "CREATE TABLE " + clsTable + 
				" AS (SELECT DISTINCT o AS uri FROM " + encodedTypesTable + ");";
	}

	@Override
	public String distinctDataProperties(String encodedTriplesTable, Dictionary dictionary) {
		Builder = new StringBuilder("SELECT DISTINCT p FROM " + encodedTriplesTable);
		final String filter = getDataPropertyFilter(Constant.PROPERTY, dictionary);
		if (!StringUtils.isNullOrBlank(filter))
			Builder.append(Constant.WHERE).append(filter);

		Builder.append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String distinctClasses(String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null);

		Builder = new StringBuilder("SELECT DISTINCT o FROM " + encodedTriplesTable);
		Builder.append(Constant.WHERE).append(RdfUtils.getTypePropertyFilter(Constant.PROPERTY, fullTypeKey, shortTypeKey));
		Builder.append(Chars.SEMICOLON);

		return Builder.toString();
	}

	private String getDataPropertyFilter(String propertyColumn, Dictionary dictionary) {
		String filter = "";
		Integer type = DictionaryUtils.getKey(dictionary, Rdf.TYPE);
		Integer subclass = DictionaryUtils.getKey(dictionary, Rdfs.SUBCLASS);
		Integer subproperty = DictionaryUtils.getKey(dictionary, Rdfs.SUBPROPERTY);
		Integer domain = DictionaryUtils.getKey(dictionary, Rdfs.DOMAIN);
		Integer range = DictionaryUtils.getKey(dictionary, Rdfs.RANGE);
		Integer fullType = DictionaryUtils.getKey(dictionary, Rdf.FULL_TYPE);
		Integer fullSubclass = DictionaryUtils.getKey(dictionary, Rdfs.FULL_SUBCLASS);
		Integer fullSubproperty = DictionaryUtils.getKey(dictionary, Rdfs.FULL_SUBPROPERTY);
		Integer fullDomain = DictionaryUtils.getKey(dictionary, Rdfs.FULL_DOMAIN);
		Integer fullRange = DictionaryUtils.getKey(dictionary, Rdfs.FULL_RANGE);

		if (type != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + type.intValue() + Constant.AND);
		if (subclass != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + subclass.intValue() + Constant.AND);
		if (subproperty != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + subproperty.intValue() + Constant.AND);
		if (domain != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + domain.intValue() + Constant.AND);
		if (range != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + range.intValue() + Constant.AND);
		if (fullType != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + fullType.intValue() + Constant.AND);
		if (fullSubclass != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + fullSubclass.intValue() + Constant.AND);
		if (fullSubproperty != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + fullSubproperty.intValue() + Constant.AND);
		if (fullDomain != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + fullDomain.intValue() + Constant.AND);
		if (fullRange != null)
			filter = filter.concat(propertyColumn + Constant.NOT_EQUALS + fullRange.intValue() + Constant.AND);

		if (filter.endsWith(Constant.AND))
			filter = filter.substring(0, filter.length() - Constant.AND.length());

		return filter;
	}

	@Override
	public String createDecodedRepresentationTable(String reprTable, String encReprTable, String dictionaryTable) {
		return "CREATE TABLE " + reprTable + " AS "
				+ "(SELECT dict1.value AS " + Constant.INPUT_DATA_NODE + ", dict2.value AS " + Constant.SUMMARY_DATA_NODE + 
				" FROM " + encReprTable + " enc_repr" + 
				" JOIN " + dictionaryTable + " dict1 ON dict1.key=enc_repr." + Constant.INPUT_DATA_NODE +
				" JOIN " + dictionaryTable + " dict2 ON dict2.key=enc_repr." + Constant.SUMMARY_DATA_NODE + ");";
	}

	@Override
	public String createEncodedRepresentationTable(String table, String column1, String column2) {
		return "CREATE TABLE " + table + " (" + column1 + " integer NOT NULL, " + column2 + " integer NOT NULL);";
	}

	@Override
	public String existsCommonSource(String encodedTableName, int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.s = table2.s "
				+ "AND (table1.p = '" + p1 + "' AND table2.p = '" + p2 + 
				"' OR table1.p = '" + p2 + "' AND table2.p = '" + p1 + "'));";
	}

	@Override
	public String existsCommonTarget(String encodedTableName,  int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.o = table2.o "
				+ "AND (table1.p = '" + p1 + "' AND table2.p = '" + p2 + 
				"' OR table1.p = '" + p2 + "' AND table2.p = '" + p1 + "'));";
	}

	@Override
	public String existsPropertyPath(String encodedTableName,  int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.o = table2.s AND table1.p = '" + p1 + 
				"' AND table2.p = '" + p2 + "');";
	}

	@Override
	public String insertTriple(String table) {
		return "INSERT INTO " + table + " (s, p, o) VALUES (?, ?, ?);";
	}

	@Override
	public String isEncoded(String constant, String dictionaryTableName) {
		return "SELECT EXISTS (SELECT 1 FROM " + dictionaryTableName + " WHERE value = '" + constant + "');";
	}

	@Override
	public String createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable) {
		return "CREATE TABLE " + auxiliarySaturatedTriplesTable + " AS (SELECT s, p, o FROM " + triplesTable + ");";
	}

	@Override
	public String createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable) {
		return "CREATE TABLE " + saturatedTriplesTable + 
				" AS (SELECT s, p, o FROM " + auxiliarySaturatedTriplesTable + ");";
	}

	@Override
	public String decodeSummaryData(String dataTable, String encodedDataTable, String dictionaryTable) {
		return "CREATE TABLE " + dataTable + " AS (SELECT DISTINCT dict1.value AS s, dict2.value AS p, dict3.value AS o "
				+ "FROM " + encodedDataTable + " data "
				+ "JOIN " + dictionaryTable + " dict1 on data.s=dict1.key "
				+ "JOIN " + dictionaryTable + " dict2 on data.p=dict2.key "
				+ "JOIN " + dictionaryTable + " dict3 on data.o=dict3.key);";
	}

	@Override
	public String decodeSummaryTypes(String typesTable, String encodedTypesTable, String dictionaryTable) {
		return "CREATE TABLE " + typesTable + " AS (SELECT DISTINCT dict1.value AS s, dict2.value AS o "
				+ "FROM " + encodedTypesTable + " data "
				+ "JOIN " + dictionaryTable + " dict1 on data.s=dict1.key "
				+ "JOIN " + dictionaryTable + " dict2 on data.o=dict2.key);";
	}

	@Override
	public String classesFromTypeComponent(String encodedTypesTable) {
		return "SELECT DISTINCT o FROM " + encodedTypesTable + ";";
	}

	@Override
	public String copyTriplesToCsv(String table, String[] columns, String filepath) {
		Preconditions.checkNotNull(columns);
		Preconditions.checkArgument(columns.length > 0);

		Builder = new StringBuilder("COPY " + table + " (");
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(", ");

		Builder.append(columns[columns.length - 1]).append(") TO '").append(filepath).append("' WITH csv;");

		return Builder.toString();
	}

	@Override
	public String populateEncodedSummaryTable(String encodedSummary, String summaryEncodedDataTable, String summaryEncodedTypesTable,
			int typePropertyKey) {
		return "INSERT INTO " + encodedSummary + " (s, p, o) (SELECT s, p, o FROM " + summaryEncodedDataTable + 
				" UNION SELECT s, " + typePropertyKey + ", o FROM " + summaryEncodedTypesTable + ");";
	}

	@Override
	public String createDecodedSummaryTable(String summaryTable, String summaryDataTable, String summaryTypesTable) {
		return "CREATE TABLE " + summaryTable + " AS (SELECT s, p, o FROM " + summaryDataTable + 
				" UNION SELECT s, '" + Rdf.FULL_TYPE + "', o FROM " + summaryTypesTable + ");";
	}

	@Override
	public String createEncodedSoTable(String encodedSoTable, String decodedTriplesTable, String dictionaryTable) {
		return "CREATE TABLE " + encodedSoTable + " AS (SELECT dict1.key AS s, dict2.key AS o "
				+ "FROM " + decodedTriplesTable + " triples " 
				+ "JOIN " + dictionaryTable + " dict1 ON triples.s=dict1.value " 
				+ "JOIN " + dictionaryTable + " dict2 ON triples.o=dict2.value);";
	}

	@Override
	public String classSupport(int encodedClassIRI, String encodedDistinctNodesTable, String encodedTypesTable) {
		Builder = new StringBuilder("SELECT COUNT(*) FROM ");
		Builder.append(encodedDistinctNodesTable).append(Chars.SPACE).append(Constant.ENCODED_DISTINCT_NODES_TABLE);
		Builder.append(Constant.JOIN);
		Builder.append(encodedTypesTable).append(Chars.SPACE).append(Constant.TYPES);
		Builder.append(Constant.WHERE).append(Constant.ENCODED_DISTINCT_NODES_TABLE).append(Chars.DOT).append(URI);
		Builder.append(Chars.EQUALS).append(Constant.TYPES).append(Chars.DOT).append(Constant.OBJECT).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String dataPropertySupport(int encodedDataProperty, String encodedDataTable) {
		Builder = new StringBuilder("SELECT COUNT(").append(Constant.DATA).append(Chars.DOT).append(Constant.PROPERTY).append(Chars.RIGHT_PAREN);
		Builder.append(Constant.FROM).append(encodedDataTable).append(Chars.SPACE).append(Constant.DATA);
		Builder.append(Constant.WHERE).append(Constant.DATA).append(Chars.DOT).append(Constant.PROPERTY).append(Chars.EQUALS).append(encodedDataProperty);
		Builder.append(Constant.GROUP_BY).append(Constant.DATA).append(Chars.DOT).append(Constant.PROPERTY).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String exists(String sourceTable, String sourceColumn, int propertyKey) {
		Builder = new StringBuilder(Constant.SELECT);
		Builder.append(Constant.EXISTS).append(Chars.LEFT_PAREN);
		Builder.append(Constant.SELECT).append(1);
		Builder.append(Constant.FROM).append(sourceTable);
		Builder.append(Constant.WHERE).append(sourceColumn).append(Chars.EQUALS).append(propertyKey);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String insertDataProperties(String possiblePropertyNodesTable, String encodedDataTable) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(possiblePropertyNodesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);

		Builder.append(Constant.SELECT).append(Constant.DISTINCT).append(Constant.PROPERTY).append(Constant.AS).append(Constant.URI);
		Builder.append(Constant.FROM).append(encodedDataTable);

		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON); // closes INSERT INTO (

		return Builder.toString();
	}

	@Override
	public String copySchemaPropertyNodes(String propertiesTable, String schemaPropertiesTable) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		Builder.append(Constant.SELECT).append(Chars.STAR).append(Constant.FROM).append(schemaPropertiesTable);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String getPropertyNodesFromDataTable(String propertiesTable, String possiblePropertyNodesTable, String encodedDataTable, String dataColumn) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);

		Builder.append(Constant.SELECT).append(prefixedColumn(PNODES_ALIAS, Constant.URI)).append(Constant.AS).append(Constant.URI);
		Builder.append(Constant.FROM).append(possiblePropertyNodesTable).append(Chars.SPACE).append(PNODES_ALIAS).append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(encodedDataTable).append(Chars.SPACE).append(Constant.DATA);
		Builder.append(Constant.WHERE).append(prefixedColumn(PNODES_ALIAS, Constant.URI)).append(Chars.EQUALS).append(prefixedColumn(Constant.DATA, dataColumn));

		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();

	}

	@Override
	public String getPropertyNodesFromTypesTable(String propertiesTable, String possiblePropertyNodesTable, String encodedTypesTable, String typesColumn) {
		final String pnodesAlias = "pnodes";
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);

		Builder.append(Constant.SELECT).append(prefixedColumn(pnodesAlias, Constant.URI)).append(Constant.AS).append(Constant.URI);
		Builder.append(Constant.FROM).append(possiblePropertyNodesTable).append(Chars.SPACE).append(pnodesAlias).append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(encodedTypesTable).append(Chars.SPACE).append(Constant.TYPES);
		Builder.append(Constant.WHERE).append(prefixedColumn(pnodesAlias, Constant.URI)).append(Chars.EQUALS).append(prefixedColumn(Constant.TYPES, typesColumn));

		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	private String prefixedColumn(String alias, String columnName) {
		final StringBuilder builder = new StringBuilder(alias);
		builder.append(Chars.DOT).append(columnName);

		return builder.toString();
	}

	@Override
	public String getDistinctClassAndPropertyNodeCount(String classesTable, String propertiesTable) {
		Builder = new StringBuilder();
		Builder.append(Constant.SELECT).append(Constant.COUNT).append(Chars.LEFT_PAREN).append(Chars.STAR).append(Chars.RIGHT_PAREN);
		Builder.append(Constant.FROM).append(Chars.LEFT_PAREN);

		Builder.append(Constant.SELECT).append(Constant.URI).append(Constant.FROM).append(classesTable);
		Builder.append(Constant.UNION);
		Builder.append(Constant.SELECT).append(Constant.URI).append(Constant.FROM).append(propertiesTable);

		Builder.append(Chars.RIGHT_PAREN).append(Constant.AS).append("count").append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String getDistinctNodesFromDataTable(String encodedDistinctNodesTable, String encodedDataTable) {
		Builder = new StringBuilder("CREATE TABLE ");
		Builder.append(encodedDistinctNodesTable).append(Constant.AS).append(Chars.LEFT_PAREN);
		Builder.append("SELECT DISTINCT ").append(URI);
		Builder.append(" FROM (SELECT DISTINCT s AS ").append(URI).append(" FROM ").append(encodedDataTable);
		Builder.append(" UNION SELECT DISTINCT o AS ").append(URI).append(" FROM ").append(encodedDataTable).append(Chars.RIGHT_PAREN);
		Builder.append(Constant.AS).append(URI).append(Chars.SPACE);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String existsRepresentationMapping(String encodedRepresentationTable, int gDataNode, int sDataNode) {
		Builder = new StringBuilder();
		Builder.append(Constant.SELECT).append(Constant.EXISTS).append(Chars.LEFT_PAREN);
		Builder.append(Constant.SELECT).append(1);
		Builder.append(Constant.FROM).append(encodedRepresentationTable);
		Builder.append(Constant.WHERE).append(Constant.INPUT_DATA_NODE).append(Chars.EQUALS).append(gDataNode);
		Builder.append(Constant.AND).append(Constant.SUMMARY_DATA_NODE).append(Chars.EQUALS).append(sDataNode);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String getEncodedSchemaTriplesFromInputTable(String schemaTriplesFromInputTable, String encodedTriplesTable, int rdfsPropertyKey) {
		Builder = new StringBuilder();
		Builder.append(Constant.INSERT_INTO).append(schemaTriplesFromInputTable);
		Builder.append(Chars.SPACE).append(Chars.LEFT_PAREN).append(SPO).append(Chars.RIGHT_PAREN).append(Chars.SPACE).append(Chars.LEFT_PAREN); 
		Builder.append(Constant.SELECT).append(SPO);
		Builder.append(Constant.FROM).append(encodedTriplesTable);
		Builder.append(Constant.WHERE).append(Constant.PROPERTY).append(Chars.EQUALS).append(rdfsPropertyKey);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String copySchemaTriples(String encodedSchemaTable, String schemaTriplesFromInputTable) {
		Builder = new StringBuilder();
		Builder.append(Constant.INSERT_INTO).append(encodedSchemaTable);
		Builder.append(Chars.SPACE).append(Chars.LEFT_PAREN).append(SPO).append(Chars.RIGHT_PAREN).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		Builder.append(Constant.SELECT).append(SPO);
		Builder.append(Constant.FROM).append(schemaTriplesFromInputTable);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String selectSubjectObjectWhereProperty(String encodedTriplesTable, int property) {
		Builder = new StringBuilder();

		Builder.append(Constant.SELECT).append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE).append(Constant.OBJECT); 
		Builder.append(Constant.FROM).append(encodedTriplesTable);
		Builder.append(Constant.WHERE).append(Constant.PROPERTY).append(Chars.EQUALS).append(property);		
		Builder.append(Chars.SEMICOLON);

		return Builder.toString();
	}

	@Override
	public String getDistinctClassesCount(String classNodesTable) {
		Builder = new StringBuilder();

		Builder.append(Constant.SELECT);
		Builder.append(Constant.COUNT).append(Chars.LEFT_PAREN).append(Constant.DISTINCT).append(Constant.URI).append(Chars.RIGHT_PAREN);
		Builder.append(Constant.FROM).append(classNodesTable).append(Chars.SEMICOLON);

		return Builder.toString();
	}
}