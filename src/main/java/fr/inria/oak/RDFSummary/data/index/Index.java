package fr.inria.oak.RDFSummary.data.index;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Index {

	public void createIndexes() throws SQLException;
}
