package fr.inria.oak.RDFSummary.data.dao;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DdlDao {

	/** Creates an index with the specified name, on the specified table column 
	 * @return Index creation time in milliseconds
	 * @throws SQLException */
	public long createIndex(final String indexName, final boolean unique, final String table, final String columns) throws SQLException;

	/** Creates a Postgres schema with the specified name */
	public void createPostgresSchema(String postgresSchemaName) throws SQLException;
	
	/** Clusters the specified table using the specified index 
	 * @return 
	 * @throws SQLException */
	public long clusterTableUsingIndex(String tableName, String indexName) throws SQLException;
	
	/**
	 * Drops the column in the specified table
	 * 
	 * @param table
	 * @param column
	 * @throws SQLException 
	 */
	public void dropColumn(final String table, final String column) throws SQLException;
	
	/** Drops the specified table */
	public void dropTable(String qualifiedTableName) throws SQLException;
	
	/** Drops the specified table if it exists */
	public void dropTableIfExists(String qualifiedTableName) throws SQLException;
	
	/** Drops the specified index */
	public void dropIndex(String schemaName, String indexName) throws SQLException;
	
	/** Drops the specified schema */
	public void dropSchema(String schemaName) throws SQLException;
	
	/** 
	 * Restarts the sequence with the specified value
	 * 
	 * @param seqName
	 * @param value
	 * @throws SQLException 
	 */
	public void restartSequence(final String seqName, final int value) throws SQLException;

	/**
	 * Sets the minvalue of the sequence to the specified value.
	 * 
	 * @param seqName
	 * @param minValue
	 * @throws SQLException 
	 */
	public void setSequenceMinValue(final String seqName, final int minValue) throws SQLException;
}
