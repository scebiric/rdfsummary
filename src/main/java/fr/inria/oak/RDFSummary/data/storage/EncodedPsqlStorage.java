package fr.inria.oak.RDFSummary.data.storage;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Storing encoded data to PostgresSQL.
 * 
 * Extends {@link PsqlStorage}.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface EncodedPsqlStorage {
	
	public DatasetPreparationResult prepareDataset(ConnectionParams connParams, String schemaName, boolean dropSchema, String dictionaryTable,
			String inputTriplesTable, String dataTriplesFilepath, String schemaTriplesFilepath, boolean indexing) throws SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException;
	
	public DatasetPreparationResult prepareDataset(ConnectionParams connParams, String schemaName, boolean dropSchema, boolean dropSaturatedTable, String dictionaryTable,
			String inputTriplesTable, String dataTriplesFilepath, String schemaTriplesFilepath, boolean indexing, int saturationBatchSize) throws SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException;
}
