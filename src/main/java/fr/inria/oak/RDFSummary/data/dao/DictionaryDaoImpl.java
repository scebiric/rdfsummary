package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueries;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.dictionary.MemoryDictionary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DictionaryDaoImpl extends DaoImpl implements DictionaryDao {

	private static final Logger log = Logger.getLogger(DictionaryDao.class);

	private String dictionaryTable;
	private static DictionaryQueries DictQueries;

	private PreparedStatement selectKeyWhereValueStatement;
	private PreparedStatement selectValueWhereKeyStatement;
	private PreparedStatement selectKeyValueStatement;
	private PreparedStatement insertValueStatement;
	private Map<Integer, String> entriesMap = null;
	private Map<String, Integer> reverseEntriesMap = Maps.newHashMap();
	private Statement statement = null;

	/**
	 * Constructor of {@link DictionaryDaoImpl}
	 * 
	 * @param dictionaryTable
	 * @param connHandler
	 * @param fetchSize
	 * @param dictQueries
	 * @param timer
	 * @throws SQLException
	 */
	public DictionaryDaoImpl(final String dictionaryTable, final SqlConnectionHandler connHandler, final int fetchSize, 
			final DictionaryQueries dictQueries, final Timer timer) throws SQLException {
		super(connHandler, fetchSize, timer);
		this.dictionaryTable = dictionaryTable;
		DictQueries = dictQueries;
		selectKeyWhereValueStatement = connHandler.prepareStatement(DictQueries.selectKeyWhereValue(dictionaryTable));
		selectValueWhereKeyStatement = connHandler.prepareStatement(DictQueries.selectValueWhereKey(dictionaryTable));
		selectKeyValueStatement = connHandler.prepareStatement(DictQueries.selectKeyValue(dictionaryTable));
		insertValueStatement = connHandler.prepareStatement(DictQueries.insertValue(dictionaryTable));
		this.statement = getNewStatement();
	}

	@Override
	public void releaseResources() throws SQLException {
		if (statement != null)
			statement.close();
		if (selectKeyValueStatement != null)
			selectKeyValueStatement.close();
		if (selectValueWhereKeyStatement != null)
			selectValueWhereKeyStatement.close();
		if (selectKeyValueStatement != null)
			selectKeyValueStatement.close();
		if (insertValueStatement != null)
			insertValueStatement.close();
	}

	@Override
	public int getHighestKey() throws SQLException {
		query = DictQueries.getHighestKey(dictionaryTable);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		boolean hasNext = resultSet.next();
		if (!hasNext)
			throw new SQLException("Failed retrieving the highest dictionary key.");

		return Utils.getIntegerColumn(resultSet, 1);
	}

	@Override
	public int getExistingOrNewKey(final String value) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(value));

		Integer key = this.getExistingKey(value);
		if (key == null) 
			key = this.insertValue(value);

		return key.intValue();
	}

	@Override
	public Integer getExistingKey(final String value) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(value));	

		Integer key = null;
		selectKeyWhereValueStatement.setString(1, value);
		final java.sql.ResultSet resultSet = selectKeyWhereValueStatement.executeQuery();
		while (resultSet.next())
			key = resultSet.getInt(1);

		resultSet.close();

		return key;
	}

	@Override
	public String getValue(final int key) throws SQLException {
		selectValueWhereKeyStatement.setInt(1, key);
		final java.sql.ResultSet resultSet = selectValueWhereKeyStatement.executeQuery();
		String value = null;
		try {
			resultSet.next();
			value = resultSet.getString(1); 
			resultSet.close();
		} catch (Exception e) {
			log.debug("Key: " + key);
			e.printStackTrace();
			System.exit(0);
		}

		return value;
	}

	@Override
	public int insertValue(final String value) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(value));

		insertValueStatement.setString(1, value);
		final java.sql.ResultSet resultSet = insertValueStatement.executeQuery();
		while (resultSet.next())
			return resultSet.getInt(1);

		throw new SQLException("Insertion failed for the value: " + value);
	}

	@Override
	public void insertValues(final Set<String> constants) throws SQLException {
		Preconditions.checkNotNull(constants);

		int count = 0;
		Integer key = null;
		for (String constant : constants) {
			key = this.getExistingKey(constant);
			if (key == null) {
				this.insertValue(constant);
				count++;
			}
		}

		log.info("Inserted " + count + " new constants to the dictionary.");
	}

	@Override
	public Dictionary loadDictionary() throws SQLException {
		log.info("Loading the memory dictionary from: " + dictionaryTable + "...");
		Timer.reset();
		entriesMap = Maps.newHashMap();
		reverseEntriesMap = Maps.newHashMap();
		Timer.start();			
		this.loadDictionaryEntries(selectKeyValueStatement.executeQuery());
		Timer.stop();

		log.info("Loaded " + entriesMap.size() + " dictionary entries in " + Timer.getTimeAsString());

		return new MemoryDictionary(entriesMap, reverseEntriesMap);
	}

	@Override
	public Dictionary loadDictionaryForTypeAndRdfs() throws SQLException {
		log.info("Loading the memory dictionary for RDF/RDFS properties from: " + dictionaryTable + "...");
		Timer.reset();
		entriesMap = Maps.newHashMap();
		reverseEntriesMap = Maps.newHashMap();
		Timer.start();			
		query = DictQueries.loadDictionaryForTypeAndRdfs(dictionaryTable);
		this.loadDictionaryEntries(statement.executeQuery(query));
		Timer.stop();

		log.info("Loaded " + entriesMap.size() + " dictionary entries in " + Timer.getTimeAsString());

		return new MemoryDictionary(entriesMap, reverseEntriesMap);
	}

	@Override
	public Dictionary loadDictionaryForConstants(final Set<String> constants) throws SQLException {
		log.info("Loading the memory dictionary for all schema constants from: " + dictionaryTable + "...");
		Timer.reset();
		entriesMap = Maps.newHashMap();
		reverseEntriesMap = Maps.newHashMap();
		Timer.start();			
		query = DictQueries.loadDictionaryForConstants(constants, dictionaryTable);
		this.loadDictionaryEntries(statement.executeQuery(query));
		Timer.stop();

		log.info("Loaded " + entriesMap.size() + " dictionary entries in " + Timer.getTimeAsString());

		return new MemoryDictionary(entriesMap, reverseEntriesMap);
	}

	/**
	 * Loads the key-value pairs to the entries map and reverse entries map
	 * @param resultSet
	 * @throws SQLException
	 */
	private void loadDictionaryEntries(final ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			entriesMap.put(resultSet.getInt(1), resultSet.getString(2));
			reverseEntriesMap.put(resultSet.getString(2), resultSet.getInt(1));
		}
	}

	/**
	 * Sets the dictionary table table to the new value and creates the prepared statements for selection/insertion.
	 */
	@Override
	public void setupAccessToDictionaryTable(final String dictionaryTable) throws SQLException {
		log.info("Setting up access to dictionary table: " + dictionaryTable + "...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		this.dictionaryTable = dictionaryTable;
		selectKeyWhereValueStatement = connHandler.prepareStatement(DictQueries.selectKeyWhereValue(dictionaryTable));
		selectValueWhereKeyStatement = connHandler.prepareStatement(DictQueries.selectValueWhereKey(dictionaryTable));
		selectKeyValueStatement = connHandler.prepareStatement(DictQueries.selectKeyValue(dictionaryTable));
		insertValueStatement = connHandler.prepareStatement(DictQueries.insertValue(dictionaryTable));
	}

	@Override
	public void createDictionaryTable(final String dictionaryTable) throws SQLException {
		log.info("Creating dictionary table: " + dictionaryTable + "...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		query = DictQueries.createDictionaryTable(dictionaryTable);
		log.info(query);
		statement.executeUpdate(query);
	}

	@Override
	public int getDictionarySize() throws SQLException {
		log.info("Retrieving dictionary size...");
		query = DictQueries.getDictionarySize(dictionaryTable);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next())
			return resultSet.getInt(1);

		throw new SQLException("Failed retrieving dictionary size.");
	}

	@Override
	public String getCurrentDictionaryTable() {
		return dictionaryTable;
	}
}
