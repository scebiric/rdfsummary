package fr.inria.oak.RDFSummary.data.saturation.trove.saturator;

import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.saturation.Triple;
import fr.inria.oak.RDFSummary.data.saturation.TriplesBatch;
import fr.inria.oak.RDFSummary.data.saturation.trove.BatchSaturator;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.RdfProperty;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.MapDbBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.rdf.schema.mapdb.Schema;
import fr.inria.oak.RDFSummary.timer.Timer;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class BisimulationMapDbSaturatorImpl implements Saturator {

	private static final Logger log = Logger.getLogger(Saturator.class);
	
	private final BatchSaturator batchSaturator;
	private final MapDbBisimulationRdfDataset rdfDataset;
	private final Timer timer;
	
	private MessageBuilder messageBuilder;
	
	/**
	 * @param batchSaturator
	 * @param rdfDataset
	 * @param timer
	 */
	public BisimulationMapDbSaturatorImpl(final BatchSaturator batchSaturator, final MapDbBisimulationRdfDataset rdfDataset, final Timer timer) {
		this.batchSaturator = batchSaturator;
		this.rdfDataset = rdfDataset;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public long saturate(final int batchSize) {
		Preconditions.checkNotNull(rdfDataset.getSchema());
		Preconditions.checkArgument(batchSize >= 0); 
		
		log.info("Running saturation...");
		timer.reset();
		timer.start();
		Set<Triple> saturatedTriples;
		Iterator<Tuple2<Integer, Integer>> poPairs;
		Tuple2<Integer, Integer> poPair;
		int subject;
		int property;
		int object;
		
		final TroveSchema schema = getTroveSchema(rdfDataset.getSchema());
		final Integer fullRdfTypeKey = rdfDataset.getRdfPropertyKeys().get(RdfProperty.FULL_TYPE);
		final Integer shortRdfTypeKey = rdfDataset.getRdfPropertyKeys().get(RdfProperty.TYPE);
		final TriplesBatch batch = new TriplesBatch(fullRdfTypeKey, shortRdfTypeKey);
		final Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		while (gDataNodes.hasNext()) {
			subject = gDataNodes.next().intValue();
			poPairs = rdfDataset.getPropertyObjectPairsBySubject(subject);
			while (poPairs.hasNext()) {
				poPair = poPairs.next();
				property = poPair.a.intValue();
				object = poPair.b.intValue();
				batch.add(new Triple(subject, property, object));
				if (batch.size() == batchSize) {
					saturatedTriples = batchSaturator.saturateBatch(batch, schema);
					insert(saturatedTriples);
					batch.clear();
				}
			}
		}
		
		if (!batch.isEmpty()) {
			saturatedTriples = batchSaturator.saturateBatch(batch, schema);
			insert(saturatedTriples);
			batch.clear();
		}
		
		timer.stop(); 
		log.info(messageBuilder.append("Saturation done in: ").append(timer.getTimeAsString()));
		
		return timer.getTimeInMilliseconds();
	}

	private TroveSchema getTroveSchema(final Schema schema) {
		Preconditions.checkNotNull(schema);
		
		final TIntObjectMap<TIntSet> domainsForProperty = new TIntObjectHashMap<TIntSet>();
		final TIntObjectMap<TIntSet> rangesForProperty = new TIntObjectHashMap<TIntSet>();
		final TIntObjectMap<TIntSet> superClassesForClass = new TIntObjectHashMap<TIntSet>();
		final TIntObjectMap<TIntSet> superPropertiesForProperty = new TIntObjectHashMap<TIntSet>();
		
		Tuple3<Integer, Integer, Integer> triple;
		int subject;
		int property;
		int object;
		final Iterator<Tuple3<Integer, Integer, Integer>> triples = schema.getAllTriples();
		while (triples.hasNext()) {
			triple = triples.next();
			subject = triple.a.intValue();
			property = triple.b.intValue();
			object = triple.c.intValue();
			if (rdfDataset.isDomainProperty(property))
				put(domainsForProperty, subject, object);
			else if (rdfDataset.isRangeProperty(property))
				put(rangesForProperty, subject, object);
			else if (rdfDataset.isSubClassOfProperty(property))
				put(superClassesForClass, subject, object);
			else if (rdfDataset.isSubPropertyOfProperty(property))
				put(superPropertiesForProperty, subject, object);
		}
		
		return new TroveSchema(domainsForProperty, rangesForProperty, superClassesForClass, superPropertiesForProperty);
	}

	private void put(final TIntObjectMap<TIntSet> map, final int key, final int value) { 
		TIntSet values = map.get(key);
		if (values == null) {
			values = new TIntHashSet();
			values.add(value);
			map.put(key, values);
		}
		else
			values.add(value);
	}

	private void insert(final Set<Triple> saturatedTriples) {
		for (final Triple triple : saturatedTriples) {
			if (rdfDataset.isTypeProperty(triple.getProperty())) {
				// Type triple
				rdfDataset.addInferredTypeTriple(triple.getSubject(), triple.getObject());
			}
			else {
				// Data triple
				rdfDataset.addInferredPropertyObjectPairsBySubject(triple.getSubject(), triple.getProperty(), triple.getObject());
				rdfDataset.addInferredPropertySubjectPairsByObject(triple.getSubject(), triple.getProperty(), triple.getObject());
			}
		}
	}

}
