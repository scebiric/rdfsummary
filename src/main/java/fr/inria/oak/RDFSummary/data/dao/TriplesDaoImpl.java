package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.ColumnArray;
import fr.inria.oak.RDFSummary.data.JavaSqlResultSetImpl;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.TriplesQueries;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesDaoImpl extends DaoImpl implements TriplesDao {

	private static final Logger log = Logger.getLogger(TriplesDao.class);
	
	private final DmlDao dmlDao;
	private final TriplesQueries triplesQueries;
	private final PostgresTableNames tables;
	private final Timer timer;
		
	private String query;
	private PreparedStatement insertDataTriple;
	private PreparedStatement insertTypeTriple;
	private PreparedStatement spoData;
	private PreparedStatement poBySData;
	private PreparedStatement psByOData;
	private PreparedStatement soType;
	private PreparedStatement soByPAllTriples;
		
	/**
	 * @param connHandler
	 * @param fetchSize
	 * @param dmlDao
	 * @param triplesQueries
	 * @param tables
	 * @param timer
	 * @throws SQLException
	 */
	public TriplesDaoImpl(final SqlConnectionHandler connHandler, final int fetchSize, final DmlDao dmlDao,
			final TriplesQueries triplesQueries, final PostgresTableNames tables, final Timer timer) throws SQLException {
		super(connHandler, fetchSize, timer);
		this.dmlDao = dmlDao;
		this.triplesQueries = triplesQueries;		
		this.tables = tables;
		this.timer = timer;		
	}

	@Override
	public void releaseResources() throws SQLException {			
	}
	
	@Override
	public void prepareInsertStatements() throws SQLException {
		insertDataTriple = connHandler.prepareStatement(triplesQueries.insertDataTriple(tables.getEncodedDataTable()));		
		insertTypeTriple = connHandler.prepareStatement(triplesQueries.insertTypeTriple(tables.getEncodedTypesTable()));
	}
	
	@Override
	public void prepareDataSelectStatements() throws SQLException {
		spoData = connHandler.prepareStatement(triplesQueries.getAllDataTriples(tables.getEncodedDataTable()));
		poBySData = connHandler.prepareStatement(triplesQueries.getPropertyObjectPairsBySubject(tables.getEncodedDataTable()));
		psByOData = connHandler.prepareStatement(triplesQueries.getPropertySubjectPairsByObject(tables.getEncodedDataTable()));
	}

	@Override
	public void prepareTypesSelectStatements() throws SQLException {
		soType = connHandler.prepareStatement(triplesQueries.getAllSubjectsAndObjects(tables.getEncodedTypesTable()));
	}
	
	@Override
	public void prepareSelectStatementsFromAllTriples() throws SQLException {
		soByPAllTriples = connHandler.prepareStatement(triplesQueries.getSubjectAndObjectByProperty(tables.getEncodedTriplesTable()));
	}	

	@Override
	public void closeDataSelectStatements() throws SQLException {
		spoData.close();
		poBySData.close();
		psByOData.close();
	}

	@Override
	public void closeTypesSelectStatements() throws SQLException {
		soType.close();
	}
	
	@Override
	public void closeSelectStatementsFromAllTriples() throws SQLException {
		soByPAllTriples.close();
	}
	
	@Override
	public void createDataTriplesTable() throws SQLException {
		log.info("Creating data triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tables.getEncodedDataTable()));

		timer.reset();
		final Statement statement = getNewStatement();
		query = triplesQueries.createDataTriplesTable(tables.getEncodedDataTable());		
		log.info(query);
		timer.start();
		statement.executeUpdate(query);
		timer.stop();
		statement.close();				
	}

	@Override
	public void createTypeTriplesTable() throws SQLException {
		log.info("Creating type triples table...");
		timer.reset();
		final Statement statement = getNewStatement();
		query = triplesQueries.createTypesTable(tables.getEncodedTypesTable());		
		log.info(query);
		timer.start();
		statement.executeUpdate(query);
		timer.stop();
		statement.close();					
	}

	@Override
	public void insertDataTriple(int subject, int property, int object) throws SQLException {
		insertDataTriple.setInt(1, subject);
		insertDataTriple.setInt(2, property);
		insertDataTriple.setInt(3, object);
		insertDataTriple.executeUpdate();
	}
	
	@Override
	public void insertTypeTriple(int subject, int object) throws SQLException {
		insertTypeTriple.setInt(1, subject);
		insertTypeTriple.setInt(2, object);
		insertTypeTriple.executeUpdate();
	}

	@Override
	public void closeInsertStatements() throws SQLException {
		insertDataTriple.close();
		insertTypeTriple.close();
	}

	@Override
	public ResultSet getDataPropertyObjectPairsBySubject(int subject) throws SQLException {
		poBySData.setInt(1, subject);
	
		return new JavaSqlResultSetImpl(poBySData.executeQuery());
	}

	@Override
	public ResultSet getDataPropertySubjectPairsByObject(int object) throws SQLException {
		psByOData.setInt(1, object);
		
		return new JavaSqlResultSetImpl(psByOData.executeQuery());
	}
	
	@Override
	public ResultSet getTypeSubjectObjectPairs() throws SQLException {
		return new JavaSqlResultSetImpl(soType.executeQuery());
	}
	
	@Override
	public ResultSet getDataTriples() throws SQLException {
		return new JavaSqlResultSetImpl(spoData.executeQuery());
	}
	
	@Override
	public ResultSet getSubjectObjectPairsByPropertyFromAllTriples(final int property) throws SQLException {
		soByPAllTriples.setInt(1, property); 
		
		return new JavaSqlResultSetImpl(soByPAllTriples.executeQuery()); 
	}
	
	@Override
	public int getDataTriplesCount() throws SQLException {
		if (!dmlDao.existsTable(tables.getEncodedDataTable()))
			return 0;
		
		return dmlDao.getRowCount(tables.getEncodedDataTable());
	}

	@Override
	public int getTypesTriplesCount() throws SQLException {
		if (!dmlDao.existsTable(tables.getEncodedTypesTable()))
			return 0;
		
		return dmlDao.getRowCount(tables.getEncodedTypesTable());
	}

	@Override
	public ResultSet getDataClasses() throws SQLException {
		return dmlDao.getSelectionResultSet(ColumnArray.OBJECT, true, tables.getEncodedTypesTable(), null);
	}

	@Override
	public ResultSet getDataProperties() throws SQLException {
		return dmlDao.getSelectionResultSet(ColumnArray.PROPERTY, true, tables.getEncodedDataTable(), null);
	}

	@Override
	public ResultSet getDataTripleSubjects() throws SQLException {
		return dmlDao.getSelectionResultSet(ColumnArray.SUBJECT, true, tables.getEncodedDataTable(), null);
	}

	@Override
	public ResultSet getDataTripleObjects() throws SQLException {
		return dmlDao.getSelectionResultSet(ColumnArray.OBJECT, true, tables.getEncodedDataTable(), null);
	}

	@Override
	public ResultSet getTypeTripleSubjects() throws SQLException {
		return dmlDao.getSelectionResultSet(ColumnArray.SUBJECT, true, tables.getEncodedTypesTable(), null);
	}
}
