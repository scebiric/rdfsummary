package fr.inria.oak.RDFSummary.data.saturation.trove;

import java.util.Set;

import fr.inria.oak.RDFSummary.data.saturation.Triple;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TripleProducer {
	
	/**
	 * Saturates the specified triple according to the subPropertyOf rules defined in the schema.
	 * 
	 * Premise: (*, P1, *) & Schema has (P1, subPropertyOf, P2)
	 * Conclusion: ($1, P2, $2)
	 * 
	 * @param triple
	 * @param schema
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> subPropertyOf(final Triple triple, final TroveSchema schema);
		
	/**
	 * Saturates the specified triple according to the subClassOf rules defined in the schema.
	 * 
	 * Premise: (*, type, C1) & Schema has (C1, subClassOf, C2)
	 * Conclusion: ($1, type, C2)
	 * 
	 * @param triple
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> subClassOf(final Triple triple, final TroveSchema schema, final int typeProperty);
	
	/**
	 * Saturates the specified triple according to the domain rules defined in the schema.
	 * 
	 * Premise: (*, P, _) & Schema has (P, domain, C)
     * Produces: ($1, type, C)
	 * 
	 * @param triple
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> domain(final Triple triple, final TroveSchema schema, final int typeProperty);
	
	/**
	 * Saturates the specified triple according to the range rules defined in the schema.
	 * 
	 * Premise: (_, P, *) & Schema has (P, range, C)
	 * Produces: ($1, type, C)
	 * 
	 * @param triple
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> range(final Triple triple, final TroveSchema schema, final int typeProperty);
}