package fr.inria.oak.RDFSummary.data.queries;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DdlQueries extends QueryProvider {

	public String clusterTableUsingIndex(String table, String indexName);
	public String createIndex(String indexName, boolean unique, String table, String columns);
	public String createPostgresSchema(String schema);
	public String createPostgresSchemaIfNotExists(String schema);
	public String dropColumn(String table, String column);
	public String dropTable(String qualifiedTableName);
	public String dropTableIfExists(String qualifiedTableName);
	public String dropIndex(String schema, String indexName);
	public String dropSchema(String schema);
	public String restartSequence(String seqName, int value);
	public String setSequenceMinValue(String seqName, int minValue);
}
