package fr.inria.oak.RDFSummary.data.storage.splitter;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.commons.db.Dictionary;

/**
 * Splitting a dataset
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface StorageSplitter {
	
	/**
	 * Creates a table containing only type triples from the table of all triples
	 * 
	 * @param typesTableName
	 * @param storageLayout
	 * @return The type table creation time
	 * @throws SQLException
	 */
	public String createTypesTable(final String typesTableName, final PostgresTableNames storageLayout) throws SQLException;
	
	/**
	 * Creates a table containing only non-type, non-RDFS triples from the table of all triples
	 * 
	 * @param dataTableName
	 * @param storageLayout
	 * @return The data table creation time
	 * @throws SQLException
	 */
	public String createDataTable(final String dataTableName, final PostgresTableNames storageLayout) throws SQLException;
	
	/**
	 * Creates a table containing only encoded type triples from the table of all encoded triples
	 * 
	 * @param encodedTypesTableName
	 * @param storageLayout
	 * @param dictionaryTable
	 * 
	 * @return The encoded types table creation time
	 * @throws SQLException
	 */
	public String createEncodedTypesTable(final String encodedTypesTableName, final PostgresTableNames storageLayout, final String dictionaryTable) throws SQLException;
	
	/**
	 * Creates a table containing only non-type, non-RDFS encoded triples from the table of all encoded triples
	 * 
	 * @param encodedDataTableName
	 * @param storageLayout
	 * @param dictionary
	 * 
	 * @return The encoded data table creation time
	 * @throws SQLException
	 */
	public String createEncodedDataTable(final String encodedDataTableName, final PostgresTableNames storageLayout, final Dictionary dictionary) throws SQLException;
}
