package fr.inria.oak.RDFSummary.data.dictionary;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;


public class MapDbDictionaryImpl implements Dictionary {

	private final HTreeMap<Integer, String> dictionary;
	private final HTreeMap<String, Integer> reverseDictionary;
	private final HTreeMap<String, Integer> largestKey; // largest existing key
	
	private static final String DICTIONARY = "dict";
	private static final String REVERSE_DICTIONARY = "reverse_dict";
	private static final String LARGEST_KEY = "largest_key";

	private int seed; // next key (not yet in the database)
	
	/**
	 * @param dictDb
	 */
	public MapDbDictionaryImpl(final DB dictDb) {
		this.dictionary = dictDb.createHashMap(DICTIONARY).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.STRING).makeOrGet();
		this.reverseDictionary = dictDb.createHashMap(REVERSE_DICTIONARY).counterEnable().keySerializer(Serializer.STRING).valueSerializer(Serializer.INTEGER).makeOrGet();
		
		this.largestKey = dictDb.createHashMap(LARGEST_KEY).keySerializer(Serializer.STRING).valueSerializer(Serializer.INTEGER).makeOrGet();
		if (this.largestKey.size() == 0) {
			seed = 1;
			this.largestKey.put(LARGEST_KEY, seed);
		}
		else
			seed = this.largestKey.get(LARGEST_KEY);
	}

	@Override
	public int insert(String value) {
		int key = seed++;
		dictionary.put(key, value);
		reverseDictionary.put(value, key);
		this.largestKey.put(LARGEST_KEY, key);
		
		return key;
	}

	@Override
	public String getValue(int key) {
		return dictionary.get(key);
	}

	@Override
	public Integer getKey(String value) {
		return reverseDictionary.get(value);
	}

	@Override
	public int size() {
		return dictionary.size();
	}

	@Override
	public int getSeed() {
		return seed;
	}
}
