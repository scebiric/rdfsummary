package fr.inria.oak.RDFSummary.data.saturation.trove.saturator;

import java.sql.SQLException;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDao;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresSaturatorImpl implements PostgresSaturator {

	private static final Logger log = Logger.getLogger(PostgresSaturator.class);
	
	private static SaturatorDao SaturatorDao;
	private static DmlDao DmlDao;
	private static DdlDao DdlDao;
	private final Timer timer;
	
	/**
	 * @param saturatorDao
	 * @param dmlDao
	 * @param ddlDao
	 * @param timer
	 */
	public PostgresSaturatorImpl(final SaturatorDao saturatorDao, final DmlDao dmlDao, final DdlDao ddlDao, final Timer timer) {
		SaturatorDao = saturatorDao;
		DmlDao = dmlDao;
		DdlDao = ddlDao;
		this.timer = timer;
	}

	@Override
	public long saturate(final String saturatedEncodedTriplesTable, final String encodedTriplesTable, final boolean dropSaturatedTable, 
			final TroveSchema schema, final Integer fullRdfTypeKey, final Integer shortRdfTypeKey, final int batchSize) throws SQLException, DictionaryException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(saturatedEncodedTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));
		Preconditions.checkNotNull(schema);
		
		boolean existsSaturatedTable = DmlDao.existsTable(saturatedEncodedTriplesTable);
		if (existsSaturatedTable && !dropSaturatedTable) {
			log.info("The " + encodedTriplesTable + " is already saturated.");
			return 0;
		}
		else {
			if (existsSaturatedTable)
				DdlDao.dropTable(saturatedEncodedTriplesTable);
	
			log.info("Saturating the " + encodedTriplesTable + " table...");
			final String auxiliarySaturatedTriplesTable = NameUtils.getAuxiliarySaturatedTableName(encodedTriplesTable);
			DdlDao.dropTableIfExists(auxiliarySaturatedTriplesTable);
			DdlDao.dropTableIfExists(saturatedEncodedTriplesTable);
			
			timer.reset();
			timer.start();
			SaturatorDao.createAuxiliarySaturatedTriplesTable(auxiliarySaturatedTriplesTable, encodedTriplesTable);
			SaturatorDao.runBatchSaturation(encodedTriplesTable, auxiliarySaturatedTriplesTable, schema, fullRdfTypeKey, shortRdfTypeKey, batchSize);
			SaturatorDao.createSaturatedTriplesTable(saturatedEncodedTriplesTable, auxiliarySaturatedTriplesTable);
			timer.stop();
			
			DdlDao.dropTable(auxiliarySaturatedTriplesTable);
			log.info("Saturation done on " + encodedTriplesTable + " table in: " + timer.getTimeAsString());
						
			return timer.getTimeInMilliseconds();
		}
	}
}
