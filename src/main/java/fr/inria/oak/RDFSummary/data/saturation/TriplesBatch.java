package fr.inria.oak.RDFSummary.data.saturation;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesBatch {

	private final Integer fullRdfTypeKey;
	private final Integer shortRdfTypeKey;

	private final Set<Triple> typeTriples;
	private final Set<Triple> dataTriples;
		
	/**
	 * @param fullRdfTypeKey
	 * @param shortRdfTypeKey
	 */
	public TriplesBatch(final Integer fullRdfTypeKey, final Integer shortRdfTypeKey) {		
		this.fullRdfTypeKey = fullRdfTypeKey;
		this.shortRdfTypeKey = shortRdfTypeKey;
		this.typeTriples = Sets.newHashSet();
		this.dataTriples = Sets.newHashSet();		
	}

	public boolean add(final Triple triple) {		
		if (isTypeTriple(triple)) {			
			return typeTriples.add(triple);
		} 
		else {			
			return dataTriples.add(triple);
		}
	}

	public boolean addAll(final Collection<Triple> triples) {
		boolean changed = false;
		for (final Triple triple : triples) {
			if (add(triple)) {
				changed = true;
			}
		}
		
		return changed;
	}

	public Set<Triple> getTypeTriples() {
		return typeTriples;
	}

	public Set<Triple> getDataTriples() {
		return dataTriples;
	}

	public boolean containsTypeTriple(final Triple triple) {
		return typeTriples.contains(triple);
	}

	public boolean containsDataTriple(final Triple triple) {
		return dataTriples.contains(triple);
	}

	public boolean containsTriple(final Triple triple) {
		if (isTypeTriple(triple)) {
			return containsTypeTriple(triple);
		} 	
		else {
			return containsDataTriple(triple);
		}
	}

	public void clear() {
		typeTriples.clear();
		dataTriples.clear();
	}

	public int size() {
		return typeTriples.size() + dataTriples.size();
	}

	public boolean isEmpty() {
		return typeTriples.isEmpty() && dataTriples.isEmpty();
	}

	private final boolean isTypeTriple(final Triple triple) {
		if (fullRdfTypeKey == null && shortRdfTypeKey == null)
			return false;
		
		if (fullRdfTypeKey != null && fullRdfTypeKey == triple.getProperty())
			return true;
		
		if (shortRdfTypeKey != null && shortRdfTypeKey == triple.getProperty())
			return true;
					
		return false; 
	}

	public Integer getFullRdfTypeKey() {
		return fullRdfTypeKey;
	}
	
	public Integer getShortRdfTypeKey() {
		return shortRdfTypeKey;
	}
}
