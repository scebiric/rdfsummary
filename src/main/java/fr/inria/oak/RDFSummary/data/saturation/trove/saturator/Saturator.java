package fr.inria.oak.RDFSummary.data.saturation.trove.saturator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Saturator {

	/**
	 * @param batchSize
	 * @return The saturation time
	 */
	public long saturate(int batchSize);
}
