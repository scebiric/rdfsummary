package fr.inria.oak.RDFSummary.data.connectionhandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

/**
 * Implements {@link SqlConnectionHandler}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SqlConnectionHandlerImpl implements SqlConnectionHandler {

	private final Connection connection;

	/**
	 * Constructor of {@link SqlConnectionHandlerImpl}
	 * 
	 * @param connection
	 */
	public SqlConnectionHandlerImpl(final Connection connection) {
		this.connection = connection;
	}

	@Override
	public void disableAutoCommit() throws SQLException {
		connection.setAutoCommit(false);
	}

	@Override
	public void enableAutoCommit() throws SQLException {
		connection.setAutoCommit(true);
	}

	@Override
	public void commit() throws SQLException {
		connection.commit();
	}

	@Override
	public Statement createStatement(final int fetchSize, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		Statement statement = connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		statement.setFetchSize(fetchSize);
		return statement;
	}

	@Override
	public void closeConnection() throws SQLException {
		connection.close();
	}

	@Override
	public PreparedStatement prepareStatement(final String sql) throws SQLException {
		return connection.prepareStatement(sql);
	}

	@Override
	public CopyManager createCopyManager() throws SQLException {
		return new CopyManager((BaseConnection) connection);
	}

}
