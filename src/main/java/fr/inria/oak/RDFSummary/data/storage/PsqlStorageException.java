package fr.inria.oak.RDFSummary.data.storage;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PsqlStorageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 58537673310842955L;

	public PsqlStorageException() {
		// TODO Auto-generated constructor stub
	}

	public PsqlStorageException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PsqlStorageException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PsqlStorageException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PsqlStorageException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
