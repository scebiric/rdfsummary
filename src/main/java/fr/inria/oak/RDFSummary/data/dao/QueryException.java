package fr.inria.oak.RDFSummary.data.dao;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class QueryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3629839660915680978L;

	public QueryException() {
		// TODO Auto-generated constructor stub
	}

	public QueryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public QueryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public QueryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public QueryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
