package fr.inria.oak.RDFSummary.data;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class JavaSqlResultSetImpl implements ResultSet {

	private final java.sql.ResultSet resultSet;
	
	/**
	 * Constructor of {@link JavaSqlResultSetImpl}.
	 * 
	 * @param resultSet
	 */
	public JavaSqlResultSetImpl(final java.sql.ResultSet resultSet) {
		this.resultSet = resultSet;
	}
	
	@Override
	public void close() throws SQLException {
		resultSet.close();
	}

	@Override
	public boolean next() throws SQLException {
		return resultSet.next();
	}

	@Override
	public int getInt(int columnIndex) throws SQLException {
		return resultSet.getInt(columnIndex);
	}

	@Override
	public long getLong(int columnIndex) throws SQLException {
		return resultSet.getLong(columnIndex);
	}

	@Override
	public boolean getBoolean(int columnIndex) throws SQLException {
		return resultSet.getBoolean(columnIndex);
	}
	
	@Override
	public String getString(int columnIndex) throws SQLException {
		return resultSet.getString(columnIndex);
	}
}
