package fr.inria.oak.RDFSummary.data.index;

import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class CliquePostgresIndexImpl implements Index {

	private static final Logger log = Logger.getLogger(Index.class);
	
	private final String schemaName;
	private final PostgresTableNames tables;
	private final DmlDao dmlDao;

	/**
	 * @param schemaName
	 * @param tables
	 * @param dmlDao 
	 */
	public CliquePostgresIndexImpl(final String schemaName, final PostgresTableNames tables, final DmlDao dmlDao) {
		this.schemaName = schemaName;
		this.tables = tables;
		this.dmlDao = dmlDao;
	}

	/**
	 * @throws SQLException
	 */
	@Override
	public void createIndexes() throws SQLException {
		log.info("Indexing tables...");
		
		// Data table indexes
		final Map<String, String> indexes = Maps.newHashMap();
		indexes.put("spo", "s, p, o");
		indexes.put("osp", "o, s, p");
		indexes.put("sp", "s, p");
		indexes.put("op", "o, p");
		indexes.put("o", "o");
		Utils.createIndexesIfNotExists(indexes, false, tables.getEncodedDataTable(), schemaName);

		// Types table indexes
		indexes.clear();
		indexes.put("so", "s, o");
		Utils.createIndexesIfNotExists(indexes, false, tables.getEncodedTypesTable(), schemaName);

		// Schema table indexes
		if (!StringUtils.isNullOrBlank(tables.getEncodedSchemaTable()) && dmlDao.existsTable(tables.getEncodedSchemaTable())) {
			indexes.clear();
			indexes.put("spo", "s, p, o");
			indexes.put("pso", "p, s, o");
			indexes.put("so", "s, o");
			indexes.put("p", "p");
			Utils.createIndexesIfNotExists(indexes, false, tables.getEncodedSchemaTable(), schemaName);
		}
	}
}
