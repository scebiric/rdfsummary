package fr.inria.oak.RDFSummary.data.saturation.trove;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveSchema {

	private final TIntObjectMap<TIntSet> domainsForProperty;
	private final TIntObjectMap<TIntSet> rangesForProperty;
	private final TIntObjectMap<TIntSet> superClassesForClass;
	private final TIntObjectMap<TIntSet> superPropertiesForProperty;
	
	/**
	 * @param domainsForProperty
	 * @param rangesForProperty
	 * @param superClassesForClass
	 * @param superPropertiesForProperty
	 */
	public TroveSchema(final TIntObjectMap<TIntSet> domainsForProperty, final TIntObjectMap<TIntSet> rangesForProperty, 
			final TIntObjectMap<TIntSet> superClassesForClass, final TIntObjectMap<TIntSet> superPropertiesForProperty) {
		this.domainsForProperty = domainsForProperty;
		this.rangesForProperty = rangesForProperty;
		this.superClassesForClass = superClassesForClass;
		this.superPropertiesForProperty = superPropertiesForProperty;
	}

	public TIntSet getDomainsForProperty(final int property) {
		return domainsForProperty.get(property);
	}
	
	public TIntSet getRangesForProperty(final int property) {
		return rangesForProperty.get(property);
	}

	public TIntSet getSuperClassesForClass(final int classIRI) { 
		return superClassesForClass.get(classIRI);
	}

	public TIntSet getSuperPropertiesForProperty(int property) {
		return superPropertiesForProperty.get(property);
	}
}
