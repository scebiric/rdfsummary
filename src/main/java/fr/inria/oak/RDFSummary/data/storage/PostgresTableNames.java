package fr.inria.oak.RDFSummary.data.storage;

import java.util.List;

import com.beust.jcommander.internal.Lists;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * Storage layout encapsulates information on the existing tables in the database and their sizes.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTableNames {
	
	private String triplesTable;
	private String schemaTable;
	private String encodedTriplesTable;
	private String dataTable;
	private String typesTable;
	private String encodedDataTable;
	private String encodedTypesTable;
	private String encodedSchemaTable;
	private String summaryDataTable;
	private String summaryTypesTable;
	private String encodedSummaryDataTable;
	private String encodedSummaryTypesTable;
	private String representationTable;
	private String dictionaryTable;
	
	public String getTriplesTable() {
		return this.triplesTable;
	}
	
	public void setTriplesTable(String triplesTable) {
		this.triplesTable = triplesTable;
	}
	
	public String getSchemaTable() {
		return this.schemaTable;
	}

	public void setSchemaTable(String schemaTable) {
		this.schemaTable = schemaTable;
	}
		
	public String getEncodedTriplesTable() {
		return this.encodedTriplesTable;
	}

	public void setEncodedTriplesTable(String encodedTriplesTable) {
		this.encodedTriplesTable = encodedTriplesTable;
	}
	
	public String getEncodedDataTable() {
		return this.encodedDataTable;
	}
	
	public void setEncodedDataTable(String value) {
		this.encodedDataTable = value;
	}
	
	public String getEncodedTypesTable() {
		return this.encodedTypesTable;
	}
	
	public void setEncodedTypesTable(String value) {
		this.encodedTypesTable = value;
	}
	
	public String getEncodedSchemaTable() {
		return this.encodedSchemaTable;
	}
	
	public void setEncodedSchemaTable(String value) {
		this.encodedSchemaTable = value;
	}
	
	public String getTypesTable() {
		return this.typesTable;
	}
	
	public void setTypesTable(String typesTableName) {
		this.typesTable = typesTableName;
	}

	public String getDataTable() {
		return this.dataTable;
	}
	
	public void setDataTable(String dataTableName) {
		this.dataTable = dataTableName;
	}
	
	public String getSummaryDataTable() {
		return this.summaryDataTable;
	}
	
	public void setSummaryDataTable(String value) {
		this.summaryDataTable = value;
	}
	
	public String getSummaryTypesTable() {
		return this.summaryTypesTable;
	}
	
	public void setSummaryTypesTable(String value) {
		this.summaryTypesTable = value;
	}
	
	public String getEncodedSummaryDataTable() {
		return this.encodedSummaryDataTable;
	}
	
	public void setEncodedSummaryDataTable(String value) {
		this.encodedSummaryDataTable = value;
	}
	
	public String getEncodedSummaryTypesTable() {
		return this.encodedSummaryTypesTable;
	}
	
	public void setEncodedSummaryTypesTable(String value) {
		this.encodedSummaryTypesTable = value;
	}
	
	public String getRepresentationTable() {
		return this.representationTable;
	}
	
	public void setRepresentationTable(String value) {
		this.representationTable = value;
	}

	/**
	 * @return A list of tables from the storage layout
	 */
	public List<String> getTables() {
		List<String> tables = Lists.newArrayList();
		if (!StringUtils.isNullOrBlank(triplesTable))
			tables.add(triplesTable);
		if (!StringUtils.isNullOrBlank(schemaTable))
			tables.add(schemaTable);
		if (!StringUtils.isNullOrBlank(encodedTriplesTable))
			tables.add(encodedTriplesTable);
		if (!StringUtils.isNullOrBlank(dataTable))
			tables.add(dataTable);
		if (!StringUtils.isNullOrBlank(typesTable))
			tables.add(typesTable);
		if (!StringUtils.isNullOrBlank(encodedDataTable))
			tables.add(encodedDataTable);
		if (!StringUtils.isNullOrBlank(encodedTypesTable))
			tables.add(encodedTypesTable);
		if (!StringUtils.isNullOrBlank(encodedSchemaTable))
			tables.add(encodedSchemaTable);
		if (!StringUtils.isNullOrBlank(summaryDataTable))
			tables.add(summaryDataTable);
		if (!StringUtils.isNullOrBlank(summaryTypesTable))
			tables.add(summaryTypesTable);
		if (!StringUtils.isNullOrBlank(encodedSummaryDataTable))
			tables.add(encodedSummaryDataTable);
		if (!StringUtils.isNullOrBlank(encodedSummaryTypesTable))
			tables.add(encodedSummaryTypesTable);
		if (!StringUtils.isNullOrBlank(representationTable))
			tables.add(representationTable);
	
		return tables;
	}

	public String getDictionaryTable() {
		return dictionaryTable;
	}	
	
	public void setDictionaryTable(final String value) {
		this.dictionaryTable = value;
	}
}
