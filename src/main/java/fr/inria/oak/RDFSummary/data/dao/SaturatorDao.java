package fr.inria.oak.RDFSummary.data.dao;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface SaturatorDao extends DataAccessObject {

	/** Creates the auxiliary saturated triples table */
	public void createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable) throws SQLException;

	/** Creates the saturated triples table */
	public void createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable) throws SQLException;
	
	/**
	 * @param triplesTable
	 * @param auxiliarySaturatedTriplesTable
	 * @param schema
	 * @param fullRdfTypeKey
	 * @param shortRdfTypeKey
	 * @param batchSize
	 * @throws SQLException
	 * @throws DictionaryException
	 */
	public void runBatchSaturation(final String triplesTable, final String auxiliarySaturatedTriplesTable, 
			TroveSchema schema, final Integer fullRdfTypeKey, final Integer shortRdfTypeKey, int batchSize) throws SQLException, DictionaryException;
}
