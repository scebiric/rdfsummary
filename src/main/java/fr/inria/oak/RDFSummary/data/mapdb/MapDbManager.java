package fr.inria.oak.RDFSummary.data.mapdb;

import java.util.Set;

import org.mapdb.DB;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface MapDbManager {
	
	/**
	 * Creates a new db
	 * 
	 * @param dbName
	 * @param mapDbFolder
	 * @return A new DB instance
	 * @throws IllegalArgumentException if the database with the specified name and in the specified folder already exists
	 */
	public DB createDb(String dbName, String mapDbFolder);
	
	/**
	 * Deletes the specified db in the specified folder, if it exists.
	 * 
	 * @param dbName
	 * @param mapDbFolder
	 * @return True if the db was deleted, otherwise false.
	 */
	public boolean deleteDb(String dbName, String mapDbFolder);
	
	/**
	 * @param dbName
	 * @param mapDbFolder
	 * @return An existing or new {@link DB} with the specified name and in the specified folder
	 */
	public DB getDb(String dbName, String mapDbFolder);

	/**
	 * Closes the database with the specified name.
	 * 
	 * @param dbName
	 */
	public void close(String dbName);
	
	/**
	 * Commits the database with the specified name.
	 * 
	 * @param dbName
	 */
	public void commit(String dbName);
	
	/**
	 * Commits all databases
	 */
	public void commitAll();
	
	/**
	 * Closes all databases
	 */
	public void closeAll();

	/**
	 * Creates a new hash set in the specified database.
	 * If the set with the specified name already exists, it is first deleted from the database
	 * 
	 * @param dbName
	 * @param setName
	 * @return An empty hash set with the specified name in the given db
	 * @throws IllegalArgumentException if the database with the specified name does not exist
	 */
	public Set<Integer> createEmptyHashSet(String dbName, String setName);
	
	/**
	 * @param dbName
	 * @return True if the database with the specified name is open in the db manager, otherwise false
	 */
	public boolean isDatabaseOpen(String dbName);
}
