package fr.inria.oak.RDFSummary.data.queries;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DdlQueriesImpl implements DdlQueries {

	@Override
	public String createPostgresSchema(String postgresSchemaName) {
		return "CREATE SCHEMA " + postgresSchemaName + Chars.SEMICOLON;
	}
	
	@Override
	public String createPostgresSchemaIfNotExists(String postgresSchemaName) {
		return "CREATE SCHEMA IF NOT EXISTS " + postgresSchemaName + Chars.SEMICOLON;
	}
	
	@Override
	public String createIndex(String indexName, boolean unique, String table, String columns) {
		final StringBuilder builder = new StringBuilder();
		builder.append(Constant.CREATE).append(Chars.SPACE);
		if (unique)
			builder.append(Constant.UNIQUE).append(Chars.SPACE);
		builder.append(Constant.INDEX).append(Chars.SPACE);
		builder.append(indexName).append(Chars.SPACE).append(Constant.ON).append(Chars.SPACE).append(table).append(Chars.SPACE);
		builder.append(Chars.LEFT_PAREN).append(columns).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON); 
		
		return builder.toString();
	}
	
	@Override
	public String clusterTableUsingIndex(String table, String indexName) {
		return "CLUSTER VERBOSE " + table + " USING " + indexName + Chars.SEMICOLON;
	}
	
	@Override
	public String dropColumn(String table, String column) {
		return "ALTER TABLE " + table + " DROP COLUMN " + column;
	}
	
	@Override
	public String dropTable(String qualifiedTableName) {
		return "DROP TABLE " + qualifiedTableName + Chars.SEMICOLON;
	}

	@Override
	public String dropTableIfExists(String qualifiedTableName) {
		return "DROP TABLE IF EXISTS " + qualifiedTableName + Chars.SEMICOLON;
	}
	
	@Override
	public String dropIndex(String schemaName, String indexName) {
		return "DROP INDEX " + schemaName + Chars.DOT + indexName + Chars.SEMICOLON;
	}

	@Override
	public String dropSchema(String schemaName) {
		return "DROP SCHEMA IF EXISTS " + schemaName + " CASCADE;";
	}
	
	@Override
	public String restartSequence(String seqName, int value) {
		return "ALTER SEQUENCE " + seqName + " RESTART WITH " + value + Chars.SEMICOLON;
	}

	@Override
	public String setSequenceMinValue(String seqName, int minValue) {
		return "ALTER SEQUENCE " + seqName + " MINVALUE " + minValue + Chars.SEMICOLON;
	}
}
