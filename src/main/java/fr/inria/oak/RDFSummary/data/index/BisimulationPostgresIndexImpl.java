package fr.inria.oak.RDFSummary.data.index;

import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationPostgresIndexImpl implements Index {

	private static final Logger log = Logger.getLogger(Index.class);
	
	private final String schemaName;
	private final PostgresTableNames tables;

	/**
	 * @param schemaName
	 * @param tables
	 */
	public BisimulationPostgresIndexImpl(final String schemaName, final PostgresTableNames tables) {
		this.schemaName = schemaName;
		this.tables = tables;
	}

	/**
	 * @throws SQLException
	 */
	@Override
	public void createIndexes() throws SQLException {	
		log.info("Indexing tables...");
		
		// Data table indexes
		final Map<String, String> indexes = Maps.newHashMap();
		indexes.put("spo", "s, p, o");
		indexes.put("ops", "o, p, s");
		Utils.createIndexesIfNotExists(indexes, false, tables.getEncodedDataTable(), schemaName);

		// Dictionary table indexes
		indexes.clear();
		indexes.put("key", "key");
		indexes.put("value", "value");
		Utils.createIndexesIfNotExists(indexes, false, tables.getDictionaryTable(), schemaName);

		// Triples table indexes
		indexes.clear();
		indexes.put("pso", "p, s, o"); 
		Utils.createIndexesIfNotExists(indexes, false, tables.getEncodedTriplesTable(), schemaName);
	}
}
