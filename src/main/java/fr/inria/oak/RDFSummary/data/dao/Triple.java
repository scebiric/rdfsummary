package fr.inria.oak.RDFSummary.data.dao;

import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * The Triple class models an RDF triple.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Triple {
	private final String subject;
	private final String property;
	private final String object;
	private final Set<String> subjectClasses;
	private final Set<String> objectClasses;
	
	/**
	 * Constructs a triple with the specified subject, property and object.
	 * 
	 * @param subject
	 * @param property
	 * @param object
	 */
	public Triple(final String subject, final String property, final String object) {
		Preconditions.checkArgument(!StringUtils.isNull(subject));
		Preconditions.checkArgument(!StringUtils.isBlank(subject));
		Preconditions.checkArgument(!StringUtils.isNull(property));
		Preconditions.checkArgument(!StringUtils.isBlank(property));		
		Preconditions.checkArgument(!StringUtils.isNull(object));		
		this.subject = subject;
		this.property = property;
		this.object = object;
		this.subjectClasses = Sets.newHashSet();
		this.objectClasses = Sets.newHashSet();
	}
	
	/**
	 * @return The triple subject
	 */
	public String getSubject() {
		return this.subject;
	}
	
	/**
	 * @return The triple property
	 */
	public String getProperty() {
		return this.property;
	}
	
	/**
	 * @return The triple object
	 */
	public String getObject() {
		return this.object;
	}
	
	/**
	 * @return The set of subject classes
	 */
	public Set<String> getSubjectClasses() {
		return this.subjectClasses;
	}
	
	/**
	 * @return The set of object classes
	 */
	public Set<String> getObjectClasses() {
		return this.objectClasses;
	}
	
	public void addSubjectClass(String classIRI) {
		this.subjectClasses.add(classIRI);
	}
	
	public void addObjectClass(String classIRI) {
		this.objectClasses.add(classIRI);
	}
	
	public void addSubjectClasses(Set<String> classes) {
		this.subjectClasses.addAll(classes);
	}
	
	public void addObjectClasses(Set<String> classes) {
		this.objectClasses.addAll(classes);
	}
}
