package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.constants.ColumnArray;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.data.saturation.TriplesBatch;
import fr.inria.oak.RDFSummary.data.saturation.trove.BatchSaturator;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SaturatorDaoImpl extends DaoImpl implements SaturatorDao {

	private static final Logger log = Logger.getLogger(SaturatorDao.class);
	private static BatchSaturator BatchSaturator;
	private static RdfTablesQueries RdfTablesQueries;
	private static DmlQueries DmlQueries;
	
	private PreparedStatement insertTripleStatement = null; 
	private Statement statement = null;
	
	/**
	 * @param encodedBatchSaturator
	 * @param connHandler
	 * @param rdfTablesQueries
	 * @param dmlQueries
	 * @param timer
	 * @throws SQLException
	 */
	public SaturatorDaoImpl(final BatchSaturator encodedBatchSaturator, final SqlConnectionHandler connHandler, 
			final RdfTablesQueries rdfTablesQueries, final DmlQueries dmlQueries, final Timer timer) throws SQLException {
		super(connHandler, 0, timer);
		BatchSaturator = encodedBatchSaturator;
		RdfTablesQueries = rdfTablesQueries;
		DmlQueries = dmlQueries;
				
		this.statement = getNewStatement();
	}

	@Override
	public void releaseResources() throws SQLException {
		if (statement != null)
			statement.close();
		if (insertTripleStatement != null) 
			insertTripleStatement.close();
	}

	/**
	 * Creates the auxiliary saturated triples table
	 * @throws SQLException 
	 */
	public void createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable) throws SQLException {
		log.info("Creating table " + auxiliarySaturatedTriplesTable + "...");
		Timer.reset();
		query = RdfTablesQueries.createAuxiliarySaturatedTriplesTable(auxiliarySaturatedTriplesTable, triplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(auxiliarySaturatedTriplesTable + " table created in " + Timer.getTimeAsString());
	}

	/**
	 * Creates the saturated triples table
	 * @throws SQLException 
	 */
	public void createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable) throws SQLException {
		Timer.reset();
		query = RdfTablesQueries.createSaturatedTriplesTable(saturatedTriplesTable, auxiliarySaturatedTriplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(saturatedTriplesTable + " table created in " + Timer.getTimeAsString());
	}

	@Override
	public void runBatchSaturation(final String triplesTable, final String auxiliarySaturatedTriplesTable, 
			TroveSchema schema, final Integer fullRdfTypeKey, final Integer shortRdfTypeKey, int batchSize) throws SQLException, DictionaryException {
		log.info("Running batch saturation...");
		Timer.reset();
		log.info("Retrieving triples from the " + triplesTable + " table...");
		query = DmlQueries.select(ColumnArray.TRIPLES, false, triplesTable, null);
		log.info(query);

		Timer.start();
		Statement statement = connHandler.createStatement(batchSize, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
		final TriplesBatch batch = new TriplesBatch(fullRdfTypeKey, shortRdfTypeKey);	
		
		connHandler.disableAutoCommit();
		Set<fr.inria.oak.RDFSummary.data.saturation.Triple> saturatedTriples = null;
		insertTripleStatement = connHandler.prepareStatement(RdfTablesQueries.insertTriple(auxiliarySaturatedTriplesTable));
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			batch.add(new fr.inria.oak.RDFSummary.data.saturation.Triple(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3)));
			if (batch.size() == batchSize) {
				saturatedTriples = BatchSaturator.saturateBatch(batch, schema);
				insertEncodedSaturatedTriples(saturatedTriples);
				batch.clear();
			}
		}

		if (!batch.isEmpty()) {
			saturatedTriples = BatchSaturator.saturateBatch(batch, schema);
			insertEncodedSaturatedTriples(saturatedTriples);
			batch.clear();
		}
		insertTripleStatement.close();
		connHandler.enableAutoCommit();
		Timer.stop();

		log.info("Batch saturation done in " + Timer.getTimeAsString());
	}

	/**
	 * Inserts the triples to the auxiliary saturated triples table
	 * 
	 * @param saturatedTriples
	 * @throws SQLException 
	 */
	private void insertEncodedSaturatedTriples(Set<fr.inria.oak.RDFSummary.data.saturation.Triple> saturatedTriples) throws SQLException {
		for (fr.inria.oak.RDFSummary.data.saturation.Triple triple : saturatedTriples) {
			insertTripleStatement.setInt(1, triple.getSubject());
			insertTripleStatement.setInt(2, triple.getProperty());
			insertTripleStatement.setInt(3, triple.getObject());
			insertTripleStatement.executeUpdate();
		}
	}
}
