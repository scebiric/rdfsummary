package fr.inria.oak.RDFSummary.data.dao;

import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DdlQueries;
import fr.inria.oak.RDFSummary.timer.Timer;

/**
 * Implements {@link DdlDao}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DdlDaoImpl extends DaoImpl implements DdlDao {

	private static final Logger log = Logger.getLogger(DdlDao.class);
	private static DdlQueries DdlQueries;
	
	private Statement statement = null;
	
	/**
	 * Constructor of {@link DdlDaoImpl}.
	 * 
	 * @param connHandler
	 * @param fetchSize
	 * @param timer
	 * @param ddlQueries
	 * @throws SQLException
	 */
	public DdlDaoImpl(final SqlConnectionHandler connHandler, final int fetchSize, final Timer timer, final DdlQueries ddlQueries) throws SQLException {
		super(connHandler, fetchSize, timer);
		DdlQueries = ddlQueries;
		this.statement = getNewStatement();
	}	
	
	@Override
	public void releaseResources() throws SQLException {
		if (statement != null)
			statement.close();
	}
	
	@Override
	public void createPostgresSchema(String postgresSchemaName) throws SQLException {
		Timer.reset();
		query = DdlQueries.createPostgresSchema(postgresSchemaName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(postgresSchemaName + " schema created in " + Timer.getTimeAsString());
	}
	
	@Override
	public void dropTable(final String qualifiedTableName) throws SQLException {
		Timer.reset();
		query = DdlQueries.dropTable(qualifiedTableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(qualifiedTableName + " dropped");
	}

	@Override
	public void dropTableIfExists(final String qualifiedTableName) throws SQLException {
		Timer.reset();
		query = DdlQueries.dropTableIfExists(qualifiedTableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(qualifiedTableName + " dropped");
	}
	
	/**
	 * Creates an index with the specified name, on the specified table columns.
	 * Columns assumed to be specified as a single column, e.g. "s", or as multiple comma-separated columns, e.g. "s, p, o".
	 * 
	 * @param indexName
	 * @param unique
	 * @param table
	 * @param columns
	 * @return 
	 * @throws SQLException
	 */
	@Override
	public long createIndex(final String indexName, final boolean unique, final String table, final String columns) throws SQLException {
		Timer.reset();
		query = DdlQueries.createIndex(indexName, unique, table, columns);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info(indexName + " created in: " + Timer.getTimeAsString() + ".");
		
		return Timer.getTimeInMilliseconds();
	}

	/**  
	 * Clusters the specified table using the specified index
	 * @throws SQLException 
	 */
	@Override
	public long clusterTableUsingIndex(String tableName, String indexName) throws SQLException {
		Timer.reset();
		query = DdlQueries.clusterTableUsingIndex(tableName, indexName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info("Clustered the " + tableName + " on " + indexName + " index, in " + Timer.getTimeAsString());
		
		return Timer.getTimeInMilliseconds();
	}


	/**
	 * Drops the specified index
	 * @throws SQLException 
	 */
	@Override
	public void dropIndex(String schemaName, String indexName) throws SQLException {
		Timer.reset();
		query = DdlQueries.dropIndex(schemaName, indexName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(indexName + " index dropped (" + Timer.getTimeAsString() + ").");	
	}

	/**
	 * Drops the specified schema
	 * @throws SQLException 
	 */
	@Override
	public void dropSchema(String schemaName) throws SQLException {
		Timer.reset();
		query = DdlQueries.dropSchema(schemaName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(schemaName + " schema dropped (" + Timer.getTimeAsString() + ").");	
	}
	
	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public void dropColumn(String table, String column) throws SQLException {
		log.info("Dropping column " + column + " in " + table + "...");
		query = DdlQueries.dropColumn(table, column);
		log.info(query);
		Timer.reset();
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
	}
	
	@Override
	public void restartSequence(final String seqName, final int value) throws SQLException {
		query = DdlQueries.restartSequence(seqName, value);
		log.info(query);
		statement.executeUpdate(query);
	}

	@Override
	public void setSequenceMinValue(final String seqName, final int minValue) throws SQLException {
		query = DdlQueries.setSequenceMinValue(seqName, minValue);
		log.info(query);
		statement.executeUpdate(query);
	}
}
