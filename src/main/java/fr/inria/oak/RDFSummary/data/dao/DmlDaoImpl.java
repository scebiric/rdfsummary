package fr.inria.oak.RDFSummary.data.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.data.JavaSqlResultSetImpl;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DmlDaoImpl extends DaoImpl implements DmlDao {

	private static final Logger log = Logger.getLogger(DmlDao.class);
	private static DmlQueries DmlQueries;
	private static PerformanceLogger PerformanceLogger;

	private CopyManager copyManager = null;
	private Statement statement = null;

	/**
	 * @param connHandler
	 * @param fetchSize
	 * @param timer
	 * @param dmlQueries
	 * @param performanceLogger
	 * @throws SQLException
	 */
	public DmlDaoImpl(final SqlConnectionHandler connHandler, final int fetchSize, final Timer timer, final DmlQueries dmlQueries, final PerformanceLogger performanceLogger) throws SQLException {
		super(connHandler, fetchSize, timer);
		DmlQueries = dmlQueries;
		PerformanceLogger = performanceLogger;
		this.statement = getNewStatement();
	}	

	@Override
	public void releaseResources() throws SQLException {
		if (statement != null)
			statement.close();
	}

	@Override
	public ResultSet getSelectionResultSet(final String[] columns, final boolean distinct, final String fromTable, final String orderByColumn) throws SQLException {
		query = DmlQueries.select(columns, distinct, fromTable, orderByColumn);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info("Result set obtained in " + Timer.getTimeAsString());	

		return new JavaSqlResultSetImpl(resultSet);
	}

	@Override
	public ResultSet getSelectionResultSet(final String[] columns, final boolean distinct, final String fromTable, final String orderByColumn, String performanceLabel) throws SQLException {
		query = DmlQueries.select(columns, distinct, fromTable, orderByColumn);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info("Result set obtained in " + Timer.getTimeAsString());

		PerformanceLogger.addTiming(performanceLabel, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(performanceLabel, query);

		return new JavaSqlResultSetImpl(resultSet);
	}

	@Override
	public ResultSet getResultSet(final String query) throws SQLException {
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info("Result set obtained in " + Timer.getTimeAsString());	

		return new JavaSqlResultSetImpl(resultSet);
	}

	@Override
	public <T> void insert(String tableName, String[] columnNames, List<T> values) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));
		Preconditions.checkNotNull(columnNames);
		Preconditions.checkArgument(columnNames.length > 0);
		Preconditions.checkNotNull(values);
		Preconditions.checkArgument(values.size() > 0);

		query = DmlQueries.insert(tableName, columnNames, values);
		statement.executeUpdate(query);
	}

	@Override
	public int getRowCount(String qualifiedTableName) throws SQLException {
		int count = -1;
		query = DmlQueries.getRowCount(qualifiedTableName);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			count = resultSet.getInt(1);
		}
		log.info(qualifiedTableName + " has " + count + " entries.");

		return count;
	}

	private boolean getBoolean(final java.sql.ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			if (resultSet.getString(1).equals("t"))
				return true;
		}

		return false;
	}

	/**
	 * Checks if the specified schema exists in Postgres database
	 * @throws SQLException 
	 */
	@Override
	public boolean existsSchema(String postgresSchemaName) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(postgresSchemaName));

		query = DmlQueries.existsSchema(postgresSchemaName);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}

	/**
	 * Checks if the table exists in the schema. The parameter is specified as schemaName.tableName.
	 * 
	 * @param qualifiedTableName
	 * @return True if the specified table exists in the given schema, otherwise false.
	 * @throws SQLException 
	 */
	@Override
	public boolean existsTable(String qualifiedTableName) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT))); 

		query = DmlQueries.existsTable(qualifiedTableName);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}

	/**
	 * Checks if the specified index exists in the database
	 * @throws SQLException 
	 */
	public boolean existsIndex(String indexName, String schemaName) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(indexName));

		query = DmlQueries.existsIndex(schemaName, indexName);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}

	@Override
	public boolean existTablesWithPrefix(final String schemaName, final String tablePrefix) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tablePrefix));

		query = DmlQueries.existTablesWithPrefix(schemaName, tablePrefix);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}

	/**
	 * Checks if the specified table is clustered on the specified index
	 * @throws SQLException 
	 */
	@Override
	public boolean isClusteredUsingIndex(String qualifiedTableName, String indexName) throws SQLException {
		query = DmlQueries.isClusteredOnIndex(qualifiedTableName, indexName);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}

	/**
	 * Retrieves tables in the specified schema whose name starts with the specified prefix
	 * @throws SQLException 
	 */
	@Override
	public List<String> getTablesByPrefix(String schemaName, String prefix, int fetchSize) throws SQLException {
		List<String> tables = Lists.newArrayList();
		query = DmlQueries.getTablesByPrefix(schemaName, prefix);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {				
			tables.add(resultSet.getString(1));
		}	

		return tables;
	}

	/**
	 * Gets schemas which contain the specified string in their name
	 * @throws SQLException 
	 */
	@Override
	public List<String> getSchemasContainingString(String str) throws SQLException {
		List<String> schemas = Lists.newArrayList();
		query = DmlQueries.getSchemasContainingString(str);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {				
			schemas.add(resultSet.getString(1));
		}	

		return schemas;
	}

	/**
	 * Retrieves the name of the index used for clustering the specified table
	 * @throws SQLException 
	 */
	@Override
	public String getClusterIndex(String qualifiedTableName, int fetchSize) throws SQLException {
		String indexName = null;
		query = DmlQueries.getClusterIndex(qualifiedTableName);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			indexName = resultSet.getString(1);
		}

		return indexName;
	}

	/**
	 * {@inheritDoc} 
	 * @throws SQLException 
	 */
	@Override
	public void insert(String tableName, String[] columnNames, int[] values) throws SQLException {
		query = DmlQueries.insert(tableName, columnNames, values);
		statement.executeUpdate(query);
	}

	/**
	 * {@inheritDoc} 
	 */
	@Override
	public int getDistinctColumnValuesCount(String tableName, String columnName) throws SQLException, QueryException {
		query = DmlQueries.distinctColumnValuesCount(tableName, columnName);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			return resultSet.getInt(1);
		}

		throw new QueryException("Invalid query");
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 * @throws IOException 
	 */
	@Override
	public void copy(final String tableName, final InputStream inputStream) throws SQLException, IOException {
		log.info("Copying to " + tableName + "...");
		copyManager = connHandler.createCopyManager();
		query = DmlQueries.copy(tableName, inputStream);
		log.info(query);
		connHandler.disableAutoCommit();
		Timer.reset();
		Timer.start();
		copyManager.copyIn(query, inputStream);
		Timer.stop();
		connHandler.enableAutoCommit();

		log.info("Copying done in " + Timer.getTimeAsString());
	}

	@Override
	public String getSequenceName(final String table, final String column) throws SQLException {
		query = DmlQueries.getSequenceName(table, column);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) 
			return resultSet.getString(1);

		return null;		
	}

	@Override
	public ResultSet selectAllById(final String subgraphsTable, final int id) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(subgraphsTable));

		query = DmlQueries.selectAllById(subgraphsTable, id);
		return new JavaSqlResultSetImpl(statement.executeQuery(query));
	}

	@Override
	public boolean existsValueInColumn(final String table, final String column, final String value) throws SQLException {
		query = DmlQueries.existsValueInColumn(table, column, value);
		log.info(query);

		final java.sql.ResultSet resultSet = statement.executeQuery(query);

		return getBoolean(resultSet);
	}
}
