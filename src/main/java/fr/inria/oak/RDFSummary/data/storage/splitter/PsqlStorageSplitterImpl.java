package fr.inria.oak.RDFSummary.data.storage.splitter;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.Dictionary;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class PsqlStorageSplitterImpl implements StorageSplitter {
	private static final Logger log = Logger.getLogger(StorageSplitter.class);
	private static DmlDao DmlDao;
	private static RdfTablesDao RdfTablesDao;

	/**
	 * @param dmlDao
	 * @param rdfTablesDao
	 */
	public PsqlStorageSplitterImpl(final DmlDao dmlDao, final RdfTablesDao rdfTablesDao) {
		DmlDao = dmlDao;
		RdfTablesDao = rdfTablesDao;
	}

	/**
	 * Creates a table containing only type triples from the table of all triples
	 */
	@Override
	public String createTypesTable(final String typesTableName, final PostgresTableNames storageLayout) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTableName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getTriplesTable()));
		
		String populateTypesTableTime = null;
		if (DmlDao.existsTable(typesTableName)) {
			log.info(typesTableName + " already exists.");
			populateTypesTableTime = "0 ms";
		}
		else {	
			RdfTablesDao.createTypesTable(typesTableName);
			populateTypesTableTime = RdfTablesDao.populateTypesTable(typesTableName, storageLayout.getTriplesTable());
		}

		storageLayout.setTypesTable(typesTableName);

		return populateTypesTableTime;
	}
	
	/**
	 * Creates a table containing only non-type, non-RDFS triples from the table of all triples
	 */
	@Override
	public String createDataTable(final String dataTableName, final PostgresTableNames storageLayout) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTableName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getTriplesTable()));
		
		String populateDataTableTime = null;
		if (DmlDao.existsTable(dataTableName)) {
			log.info(dataTableName + " already exists.");
			populateDataTableTime = "0 ms";
		}
		else {
			RdfTablesDao.createDataTriplesTable(dataTableName);
			populateDataTableTime = RdfTablesDao.populateDataTriplesTable(dataTableName, storageLayout.getTriplesTable());
		}

		storageLayout.setDataTable(dataTableName);
		
		return populateDataTableTime;
	}
	
	/**
	 * Creates a table containing only encoded type triples from the table of all encoded triples
	 */
	@Override
	public String createEncodedTypesTable(String encodedTypesTableName, PostgresTableNames storageLayout, String dictionaryTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTypesTableName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getEncodedTriplesTable()));
		
		String populateTypesTableTime = null;
		if (DmlDao.existsTable(encodedTypesTableName)) {
			log.info(encodedTypesTableName + " already exists.");
			populateTypesTableTime = "0 ms";
		}
		else {
			RdfTablesDao.createEncodedTypesTableWithId(encodedTypesTableName);
			populateTypesTableTime = RdfTablesDao.populateEncodedTypesTable(encodedTypesTableName, storageLayout.getEncodedTriplesTable(), dictionaryTable);
		}

		storageLayout.setEncodedTypesTable(encodedTypesTableName);

		return populateTypesTableTime;
	}
	
	/**
	 * Creates a table containing only non-type, non-RDFS encoded triples from the table of all encoded triples
	 */
	@Override
	public String createEncodedDataTable(final String encodedDataTableName, final PostgresTableNames storageLayout, final Dictionary dictionary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTableName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getEncodedTriplesTable()));
		
		String createDataTableTime = null;
		if (DmlDao.existsTable(encodedDataTableName)) {
			log.info(encodedDataTableName + " already exists.");
			createDataTableTime = "0 ms";
		}
		else {
			RdfTablesDao.createEncodedTriplesTableWithId(encodedDataTableName);
			createDataTableTime = RdfTablesDao.populateEncodedDataTriplesTable(encodedDataTableName, storageLayout.getEncodedTriplesTable(), dictionary);
		}

		storageLayout.setEncodedDataTable(encodedDataTableName);
		
		return createDataTableTime;
	}
}
