package fr.inria.oak.RDFSummary.data.connectionhandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.copy.CopyManager;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface SqlConnectionHandler {

	/** Sets autocommit to false 
	 * @throws SQLException */
	public void disableAutoCommit() throws SQLException;
	
	/** Sets autocommit to true 
	 * @throws SQLException */
	public void enableAutoCommit() throws SQLException;
	
	/** Invokes commit 
	 * @throws SQLException */
	public void commit() throws SQLException;

	/**
	 * @param resultSetType
	 * @param resultSetConcurrency
	 * @param resultSetHoldability
	 * @return a new Statement object that will generate ResultSet objects with the given type, concurrency, and holdability
	 * @throws SQLException 
	 */
	public Statement createStatement(final int fetchSize, final int resultSetType, final int resultSetConcurrency, final int resultSetHoldability) throws SQLException;
	
	/**
	 * Closes the connection
	 * @throws SQLException 
	 */
	public void closeConnection() throws SQLException;

	/**
	 * Creates a PreparedStatement object for sending parameterized SQL statements to the database.
	 * 
	 * @param query
	 * @return a new default PreparedStatement object containing the pre-compiled SQL statement
	 * @throws SQLException 
	 */
	public PreparedStatement prepareStatement(final String sql) throws SQLException;

	/** 
	 * @return An instance of {@link CopyManager} 
	 * @throws SQLException 
	 */
	public CopyManager createCopyManager() throws SQLException;
}
