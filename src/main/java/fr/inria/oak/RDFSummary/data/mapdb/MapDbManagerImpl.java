package fr.inria.oak.RDFSummary.data.mapdb;

import java.io.File;
import java.util.Map;
import java.util.Set;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbManagerImpl implements MapDbManager {

	private final Map<String, DB> databasesByName;

	public MapDbManagerImpl() {
		databasesByName = Maps.newHashMap();
	}

	@Override
	public DB createDb(final String dbName, final String mapDbFolder) {
		Preconditions.checkArgument(!StringUtils.isNull(dbName));
		Preconditions.checkArgument(!StringUtils.isNull(mapDbFolder));
		
		final String dbFilepath = Utils.getFilepath(mapDbFolder, dbName, Extension.DB);
		final File file = new File(dbFilepath);
		if (file.exists())
			throw new IllegalArgumentException("The MapDB database exists: " + dbFilepath);
		
		final DB db = DBMaker.newFileDB(file).transactionDisable().closeOnJvmShutdown().mmapFileEnableIfSupported().asyncWriteEnable().make();
		databasesByName.put(dbName, db);
		
		return db;
	}
	
	@Override
	public boolean deleteDb(final String dbName, final String mapDbFolder) {
		Preconditions.checkArgument(!StringUtils.isNull(dbName));
		Preconditions.checkArgument(!StringUtils.isNull(mapDbFolder));
		
		final String dbFilepath = Utils.getFilepath(mapDbFolder, dbName, Extension.DB);
		final File file = new File(dbFilepath);
		
		return file.delete();
	}
	
	@Override
	public DB getDb(final String dbName, final String mapDbFolder) {
		Preconditions.checkArgument(!StringUtils.isNull(dbName));
		Preconditions.checkArgument(!StringUtils.isNull(mapDbFolder));
		
		DB db = databasesByName.get(dbName);
		if (db == null) {
			final String dbFilepath = Utils.getFilepath(mapDbFolder, dbName, Extension.DB);
			final File file = new File(dbFilepath);
			
			db = DBMaker.newFileDB(file).transactionDisable().closeOnJvmShutdown().mmapFileEnableIfSupported().asyncWriteEnable().make();
			databasesByName.put(dbName, db);
		}

		return db;
	}

	@Override
	public void close(final String dbName) {
		Preconditions.checkArgument(databasesByName.keySet().contains(dbName), "The database " + dbName + " does not exist.");
		
		databasesByName.get(dbName).close();
		databasesByName.remove(dbName);
	}

	@Override
	public void commit(final String dbName) {
		Preconditions.checkArgument(databasesByName.keySet().contains(dbName), "The database " + dbName + " does not exist.");
		
		databasesByName.get(dbName).commit();
	}

	@Override
	public void commitAll() {
		for (final String dbName : databasesByName.keySet())
			commit(dbName);
	}

	@Override
	public void closeAll() {
		final Set<String> dbNames = Sets.newHashSet(databasesByName.keySet());
		for (final String dbName : dbNames)
			close(dbName);
	}

	@Override
	public Set<Integer> createEmptyHashSet(final String dbName, final String setName) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dbName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(setName));
		Preconditions.checkArgument(isDatabaseOpen(dbName));
		
		final DB db = databasesByName.get(dbName);
		if (db.get(setName) != null)
			db.delete(setName);
		
		return db.createHashSet(setName).counterEnable().make();
	}

	@Override
	public boolean isDatabaseOpen(String dbName) {
		return databasesByName.keySet().contains(dbName);
	}
}
