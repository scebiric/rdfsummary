package fr.inria.oak.RDFSummary.data.storage;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class EncodingTimes {

	private final String dataAndTypesEncodingTime;
	private final String schemaEncodingTime;
		
	/**
	 * @param dataAndTypesEncodingTime
	 * @param schemaEncodingTime
	 */
	public EncodingTimes(final String dataAndTypesEncodingTime, final String schemaEncodingTime) {
		this.dataAndTypesEncodingTime = dataAndTypesEncodingTime;
		this.schemaEncodingTime = schemaEncodingTime;
	}
	
	public String getDataAndTypesEncodingTime() {
		return dataAndTypesEncodingTime;
	}
	
	public String getSchemaEncodingTime() {
		return schemaEncodingTime;
	}
}
