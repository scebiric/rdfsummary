package fr.inria.oak.RDFSummary.data.encoding;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.SchemaUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.miscellaneous.Props;
import fr.inria.oak.commons.rdfdb.dictionaryencoder.Config;
import fr.inria.oak.commons.rdfdb.dictionaryencoder.RDFGraphDictionaryEncoder;

/**
 * Implementation of the {@link Encoding} interface
 * 
 * @author Sejla CEBIRIC
 *
 */
public class EncodingImpl implements Encoding {

	private static final Logger log = Logger.getLogger(Encoding.class);
	private RDFGraphDictionaryEncoder rdfEncoder;
	private final DictionaryDao dictDao;
	private static Timer Timer;

	/**
	 * Constructor of {@link EncodingImpl}
	 * 
	 * @param dictDao
	 * @param timer
	 */
	public EncodingImpl(final DictionaryDao dictDao, final Timer timer) {
		this.dictDao = dictDao;
		Timer = timer; 
		this.rdfEncoder = null;
	}

	/**
	 * Encodes RDF data triple.
	 * Table names converted to lowercase since Postgres forces lowercase names of database objects.
	 * @return The encoding time
	 * @throws UnsupportedDatabaseEngineException 
	 * @throws SQLException 
	 * 
	 */
	public long encodeDataAndTypes(final ConnectionParams connParams, final String triplesTable, final String encodedTriplesTable, final String dictionaryTable) throws SQLException, UnsupportedDatabaseEngineException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(triplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		log.info("Encoding RDF data triples from " + triplesTable + " table...");
		Timer.reset();
		Map<String, String> propertiesMap = this.getConnectionPropertiesMap(connParams);
		propertiesMap.put("database.triples_table_name", triplesTable);
		propertiesMap.put("database.encoded_triples_table_name", encodedTriplesTable);
		propertiesMap.put("database.dictionary_table_name", dictionaryTable);
		this.rdfEncoder = this.getRDFEncoder(propertiesMap);	
		Timer.start();
		this.rdfEncoder.start();
		Timer.stop();
	
		log.info("Encoding of the RDF triples from " + triplesTable + " table done in " + Timer.getTimeAsString());
		
		return Timer.getTimeInMilliseconds();
	}

	/**
	 * Encodes the specified RDF Schema.
	 * @return The encoding time
	 * @throws UnsupportedDatabaseEngineException 
	 * @throws DictionaryException 
	 * @throws SQLException 
	 */
	public long encodeSchema(final ConnectionParams connParams, final Schema schema, final String dictionaryTable) throws UnsupportedDatabaseEngineException, SQLException, DictionaryException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkNotNull(schema, "Schema is null, cannot encode.");
		Timer.reset();
		log.info("Encoding schema...");
		Map<String, String> propertiesMap = this.getConnectionPropertiesMap(connParams);
		propertiesMap.put("database.dictionary_table_name", dictionaryTable);

		this.rdfEncoder = this.getRDFEncoder(propertiesMap);
		Timer.start();
		Set<String> constants = SchemaUtils.getConstants(schema);
		dictDao.insertValues(constants);
		Timer.stop();

		log.info("Schema encoding done in " + Timer.getTimeAsString());
		
		return Timer.getTimeInMilliseconds();
	}

	/**
	 * Populates a properties map with Postgres connection parameters
	 * @param input 
	 * 
	 * @return The properties map
	 */
	private Map<String, String> getConnectionPropertiesMap(final ConnectionParams connParams) {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(connParams.getDataSourceName()));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(connParams.getServerName()));
		Preconditions.checkNotNull(connParams.getPort());
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(connParams.getDatabaseName()));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(connParams.getUsername()));
		Preconditions.checkArgument(!StringUtils.isNull(connParams.getPassword()));

		Map<String, String> propertiesMap = Maps.newHashMap();
		propertiesMap.put("database.engine", this.getDatabaseEngine(connParams.getDataSourceName()));
		propertiesMap.put("database.host", connParams.getServerName());
		propertiesMap.put("database.port", Integer.toString(connParams.getPort()));
		propertiesMap.put("database.name", connParams.getDatabaseName());
		propertiesMap.put("database.user", connParams.getUsername());
		propertiesMap.put("database.password", connParams.getPassword());

		return propertiesMap;
	}

	/**
	 * Converting the data source name to the one expected by the oak-commons code
	 * 
	 * @param datasourceName
	 * @return The new data source name
	 */
	private String getDatabaseEngine(final String datasourceName) {
		if (datasourceName.equals("postgres"))
			return datasourceName.concat("QL").toUpperCase();

		return datasourceName.toUpperCase();
	}

	/**
	 * Creates an instance of RDFGraphDictionaryEncoder
	 * 
	 * @param propertiesMap
	 * @return The new instance of RDFGraphDictionaryEncoder
	 * @throws UnsupportedDatabaseEngineException 
	 */
	private RDFGraphDictionaryEncoder getRDFEncoder(final Map<String, String> propertiesMap) throws UnsupportedDatabaseEngineException {
		RDFGraphDictionaryEncoder encoder = null;	
		@SuppressWarnings("unchecked")
		Props properties = new Props(propertiesMap);
		Config dataEncoderConfig = new Config(properties);
		encoder = new RDFGraphDictionaryEncoder(dataEncoderConfig);

		return encoder;
	}
}
