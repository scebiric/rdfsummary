package fr.inria.oak.RDFSummary.data.saturation.trove;

import java.util.Set;
import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.data.saturation.Triple;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTripleProducerImpl implements TripleProducer {

	private TIntSet superProperties;
	private TIntSet superClasses;
	private TIntSet domains;
	private TIntSet ranges;
	
	private TIntIterator superPropertiesIterator;
	private TIntIterator superClassesIterator;
	private TIntIterator domainsIterator;
	private TIntIterator rangesIterator;
	
	public TroveTripleProducerImpl() {
	
	}

	@Override
	public Set<Triple> subPropertyOf(final Triple triple, final TroveSchema schema) {
		final Set<Triple> triples = Sets.newLinkedHashSet(); 

		superProperties = schema.getSuperPropertiesForProperty(triple.getProperty());
		if (superProperties != null) {
			superPropertiesIterator = superProperties.iterator();
			while (superPropertiesIterator.hasNext()) {
				triples.add(new Triple(triple.getSubject(), superPropertiesIterator.next(), triple.getObject()));		
			}
		}		
		
		return triples;		
	}

	@Override
	public Set<Triple> subClassOf(final Triple triple, final TroveSchema schema, int typeProperty) {
		final Set<Triple> triples = Sets.newLinkedHashSet(); 

		superClasses = schema.getSuperClassesForClass(triple.getObject());
		if (superClasses != null) {
			superClassesIterator = superClasses.iterator();
			while (superClassesIterator.hasNext()) {
				triples.add(new Triple(triple.getSubject(), triple.getProperty(), superClassesIterator.next()));
			}
		}		

		return triples;
		
	}

	@Override
	public Set<Triple> domain(final Triple triple, final TroveSchema schema, int typeProperty) {
		final Set<Triple> triples = Sets.newLinkedHashSet();
		
		domains = schema.getDomainsForProperty(triple.getProperty());
		if (domains != null) {
			domainsIterator = domains.iterator();
			while (domainsIterator.hasNext()) {				
				triples.add(new Triple(triple.getSubject(), typeProperty, domainsIterator.next()));
			}
		}
		
		return triples;
		
	}

	@Override
	public Set<Triple> range(final Triple triple, final TroveSchema schema, int typeProperty) {
		final Set<Triple> triples = Sets.newLinkedHashSet();
		
		ranges = schema.getRangesForProperty(triple.getProperty());
		if (ranges != null) {
			rangesIterator = ranges.iterator();
			while (rangesIterator.hasNext()) {				
				triples.add(new Triple(triple.getObject(), typeProperty, rangesIterator.next()));
			}
		}
		
		return triples;		
	}

}
