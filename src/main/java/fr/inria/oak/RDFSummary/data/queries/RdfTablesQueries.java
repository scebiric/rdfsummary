package fr.inria.oak.RDFSummary.data.queries;

import fr.inria.oak.commons.db.Dictionary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfTablesQueries extends QueryProvider {
	public String subjectTypesByProperties(String dataTable, String typesTable);
	public String objectTypesByProperties(String dataTable, String typesTable);
	
	public String createClassesAndPropertiesTable(String clsPropsTable, String encodedDataTable, String encodedTypesTable);
	public String createTypeSubjectsAndClassesTable(String subjClsTable, String summaryTypesTable);
	public String createDataPropertiesTable(String propsTable, String encodedDataTable);
	public String createClassesTable(String clsTable, String encodedTypesTable);
	public String distinctSpOrderByS(String dataTable);
	public String distinctOpOrderByO(String dataTable);
	public String distinctSpOrderBySWithSUntyped(String dataTable, String typesTable);
	public String distinctOpOrderByOWithOUntyped(String dataTable, String typesTable);
	
	public String existsCommonSource(String encodedTable, int p1, int p2);
	public String existsCommonTarget(String encodedTable, int p1, int p2);
	public String existsPropertyPath(String encodedTable, int p1, int p2);
	public String isEncoded(String constant, String dictionaryTableName);
	
	public String createTriplesTable(String table);
	public String createTriplesTableWithSkippedChars(String tableName);
	public String createEncodedTriplesTable(String encodedTable);
	public String createEncodedTriplesTableWithId(String encodedTable);
	public String createTypesTable(String typesTable);
	public String createEncodedTypesTableWithId(String encodedTypesTable);
	public String createDataTriplesTable(String table);
	public String populateTypesTable(String typesTable, String triplesTable);
	public String populateEncodedTypesTable(String encodedTypesTable, String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey);
	public String populateDataTriplesTable(String dataTable, String triplesTable);
	public String populateEncodedDataTriplesTable(String encodedDataTable, String encodedTriplesTable, Dictionary dictionary);
	
	public String createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable);
	public String createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable);
	
	public String populateEncodedSummaryTable(String encodedSummary, String summaryEncodedDataTable, String summaryEncodedTypesTable, int typePropertyKey);
	public String createDecodedSummaryTable(String summaryTable, String summaryDataTable, String summaryTypesTable);
	public String createEncodedRepresentationTable(String table, String column1, String column2);
	public String createDecodedRepresentationTable(String reprTable, String encReprTable, String dictionaryTable);
	public String decodeSummaryData(String dataTable, String encodedDataTable, String dictionaryTable);
	public String decodeSummaryTypes(String typesTable, String encodedTypesTable, String dictionaryTable);
	
	public String classesFromTypeComponent(String encodedTypesTable);
	public String createEncodedUriTable(String table);
	public String insert(String targetTable, String sourceTable, String sourceColumn, int propertyKey);
	public String insertClasses(String clsTable, String encodedTypesTable, String column);
	public String copyTriplesToCsv(String table, String[] columns, String filepath);
	public String insertTriple(String table);
	public String createEncodedSoTable(String encodedSoTable, String decodedTriplesTable, String dictionaryTable);
	public String distinctDataProperties(String encodedTriplesTable, Dictionary dictionary);
	public String distinctClasses(String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey);
	public String classSupport(int encodedClassIRI, String encodedDistinctNodesTable, String encodedTypesTable);
	public String dataPropertySupport(int encodedDataProperty, String encodedDataTable);
	public String exists(String sourceTable, String sourceColumn, int propertyKey);
	public String copySchemaPropertyNodes(String propertiesTable, String schemaPropertiesTable);
	public String getPropertyNodesFromDataTable(String propertiesTable, String possiblePropertyNodesTable, String encodedDataTable, String dataColumn);
	public String getPropertyNodesFromTypesTable(String propertiesTable, String possiblePropertyNodesTable, String encodedTypesTable, String typesColumn);
	public String getDistinctClassAndPropertyNodeCount(String classesTable, String propertiesTable);
	public String getDistinctNodesFromDataTable(String encodedDistinctNodesTable, String encodedDataTable);
	public String existsRepresentationMapping(String encodedRepresentationTable, int gDataNode, int sDataNode);
	public String insertDataProperties(String possiblePropertyNodesTable, String encodedDataTable);
	public String getEncodedSchemaTriplesFromInputTable(String schemaTriplesFromInputTable, String encodedTriplesTable, int rdfsPropertyKey);
	public String copySchemaTriples(String encodedSchemaTable, String schemaTriplesFromInputTable);
	public String selectSubjectObjectWhereProperty(String encodedTriplesTable, int property);
	public String getDistinctClassesCount(String classNodesTable);	   
}
