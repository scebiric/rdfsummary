package fr.inria.oak.RDFSummary.data.dictionary;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresDictionaryImpl implements Dictionary {

	private final DictionaryDao dictDao;
		
	/**
	 * @param dictDao
	 * @throws SQLException 
	 */
	public PostgresDictionaryImpl(final DictionaryDao dictDao) throws SQLException {
		this.dictDao = dictDao;		
	}

	@Override
	public int insert(String value) throws SQLException {
		return dictDao.insertValue(value);
	}

	@Override
	public Integer getKey(String value) throws SQLException {
		return dictDao.getExistingKey(value);
	}

	@Override
	public int getSeed() throws SQLException {
		return dictDao.getHighestKey() + 1;
	}

	@Override
	public String getValue(int key) throws SQLException {
		return dictDao.getValue(key);
	}

	@Override
	public int size() throws SQLException {
		return dictDao.getDictionarySize();
	}

}
