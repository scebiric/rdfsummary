package fr.inria.oak.RDFSummary.data.saturation.trove.saturator;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PostgresSaturator {
	
	/**
	 * Saturates the encoded triples table based on the given schema and stores 
	 * the saturated triples into the saturatedEncodedTriplesTable.
	 * 
	 * @param saturatedEncodedTriplesTable
	 * @param encodedTriplesTable
	 * @param dropSaturatedTable
	 * @param schema
	 * @param fullRdfTypeKey
	 * @param shortRdfTypeKey
	 * @param batchSize
	 * @return The saturation time in milliseconds
	 * @throws SQLException
	 * @throws DictionaryException
	 */
	public long saturate(String saturatedEncodedTriplesTable, String encodedTriplesTable, boolean dropSaturatedTable, 
			TroveSchema schema, Integer fullRdfTypeKey, Integer shortRdfTypeKey, int batchSize) throws SQLException, DictionaryException;
}
