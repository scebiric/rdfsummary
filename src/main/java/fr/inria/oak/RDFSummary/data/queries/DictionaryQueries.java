package fr.inria.oak.RDFSummary.data.queries;

import java.util.Set;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DictionaryQueries extends QueryProvider {

	public String selectKeyWhereValue(String dictionaryTable);
	public String selectValueWhereKey(String dictionaryTable);
	public String insertKeyValue(String dictionaryTable);
	public String insertValue(String dictionaryTable);
	public String selectKeyValue(String dictionaryTable);
	
	public String loadDictionaryForTypeAndRdfs(String dictionaryTable);
	public String loadDictionaryForConstants(Set<String> constants, String dictionaryTable);
	public String createDictionaryTable(String dictionaryTable);
	public String getDictionarySize(String dictionaryTable);
	public String getHighestKey(String dictionaryTable);
}
