package fr.inria.oak.RDFSummary.data.saturation.trove;

import java.util.Set;

import fr.inria.oak.RDFSummary.data.saturation.Triple;
import fr.inria.oak.RDFSummary.data.saturation.TriplesBatch;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BatchSaturator {

	/**
	 * Saturates the batch with respect to the specified schema.
	 * 
	 * @param batch
	 * @param schema
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> saturateBatch(TriplesBatch batch, TroveSchema schema);
	
	/**
	 * @param batch
	 * @param schema
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> saturateSubProperties(final TriplesBatch batch, final TroveSchema schema);
	
	/**
	 * @param batch
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> saturateSubClasses(final TriplesBatch batch, final TroveSchema schema, final int typeProperty);
	
	/**
	 * @param batch
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> saturateDomains(final TriplesBatch batch, final TroveSchema schema, final int typeProperty);
	
	/**
	 * @param batch
	 * @param schema
	 * @param typeProperty
	 * @return The set of produced encoded triples
	 */
	public Set<Triple> saturateRanges(final TriplesBatch batch, final TroveSchema schema, final int typeProperty);
}
