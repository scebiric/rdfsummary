package fr.inria.oak.RDFSummary.data.berkeleydb;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BerkeleyDbException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2236892361584205766L;

	public BerkeleyDbException() {
		// TODO Auto-generated constructor stub
	}

	public BerkeleyDbException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BerkeleyDbException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public BerkeleyDbException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BerkeleyDbException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
