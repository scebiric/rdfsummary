package fr.inria.oak.RDFSummary.data.saturation.trove;

import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.data.saturation.Triple;
import fr.inria.oak.RDFSummary.data.saturation.TriplesBatch;

/**
 * 
 * @author Sejla CEBIRIC
 * @author Damian BURSZTYN
 *
 */
public class BatchSaturatorImpl implements BatchSaturator {

	private final TripleProducer tripleProducer;
	
	private static final int NULL_INT = -1;

	/**
	 * @param tripleProducer
	 */
	public BatchSaturatorImpl(final TripleProducer tripleProducer) {
		this.tripleProducer = tripleProducer;
	}

	@Override
	public Set<Triple> saturateBatch(final TriplesBatch batch, final TroveSchema schema) {
		Preconditions.checkNotNull(batch);
		Preconditions.checkNotNull(schema);

		final Set<Triple> saturatedTriples = Sets.newHashSet();
		final int typeProperty = getTypeProperty(batch);
		
		saturatedTriples.addAll(saturateSubProperties(batch, schema));
		saturatedTriples.addAll(saturateSubClasses(batch, schema, typeProperty));
		saturatedTriples.addAll(saturateDomains(batch, schema, typeProperty));
		saturatedTriples.addAll(saturateRanges(batch, schema, typeProperty));

		return saturatedTriples;
	}

	@Override
	public Set<Triple> saturateSubProperties(final TriplesBatch batch, final TroveSchema schema) {
		final Set<Triple> saturatedTriples = Sets.newHashSet();
		final Set<Triple> unchecked = Sets.newHashSet();

		for (final Triple triple : batch.getDataTriples()) {
			final Set<Triple> producedTriples = tripleProducer.subPropertyOf(triple, schema);
			for (final Triple producedTriple : producedTriples) {
				if (!batch.containsTriple(producedTriple)) {
					unchecked.add(producedTriple);
				}
			}
		}
		
		Set<Triple> inProcess = null;
		// Transitive saturation
		while (!unchecked.isEmpty()) {
			saturatedTriples.addAll(unchecked);
			batch.addAll(unchecked);
			inProcess = Sets.newHashSet(unchecked);
			unchecked.clear();
			for (final Triple triple : inProcess) {
				final Set<Triple> producedTriples = tripleProducer.subPropertyOf(triple, schema);
				for (final Triple producedTriple : producedTriples) {
					if (!batch.containsTriple(producedTriple)) {
						unchecked.add(producedTriple);
					}
				}
			}
		}
		
		return saturatedTriples;
	}

	@Override
	public Set<Triple> saturateSubClasses(final TriplesBatch batch, final TroveSchema schema, final int typeProperty) {
		final Set<Triple> saturatedTriples = Sets.newHashSet();
		final Set<Triple> unchecked = Sets.newHashSet();

		for (final Triple triple : batch.getTypeTriples()) {
			final Set<Triple> producedTriples = tripleProducer.subClassOf(triple, schema, typeProperty);
			for (final Triple producedTriple : producedTriples) {
				if (!batch.containsTriple(producedTriple)) {
					unchecked.add(producedTriple);
				}
			}
		}
		
		Set<Triple> inProcess = null;
		// Transitive saturation
		while (!unchecked.isEmpty()) {
			saturatedTriples.addAll(unchecked);
			batch.addAll(unchecked);
			inProcess = Sets.newHashSet(unchecked);
			unchecked.clear();
			for (final Triple triple : inProcess) {
				final Set<Triple> producedTriples = tripleProducer.subClassOf(triple, schema, typeProperty);
				for (final Triple producedTriple : producedTriples) {
					if (!batch.containsTriple(producedTriple)) {
						unchecked.add(producedTriple);
					}
				}
			}
		}
		
		return saturatedTriples;
	}

	@Override
	public Set<Triple> saturateDomains(final TriplesBatch batch, final TroveSchema schema, final int typeProperty) {
		final Set<Triple> saturatedTriples = Sets.newHashSet();

		for (final Triple triple : batch.getDataTriples()) {			
			addTypeTriples(tripleProducer.domain(triple, schema, typeProperty), saturatedTriples, batch);
		}
		batch.addAll(saturatedTriples);

		return saturatedTriples;
	}

	@Override
	public Set<Triple> saturateRanges(final TriplesBatch batch, final TroveSchema schema, final int typeProperty) {
		final Set<Triple> saturatedTriples = Sets.newHashSet();

		for (final Triple triple : batch.getDataTriples()) {			
			addTypeTriples(tripleProducer.range(triple, schema, typeProperty), saturatedTriples, batch);
		}
		batch.addAll(saturatedTriples);

		return saturatedTriples;
	}
	
	/**
	 * Adds to saturated triples those produced type triples which are not already in the batch.
	 * 
	 * @param producedTriples
	 * @param saturatedTriples
	 * @param batch
	 */
	private void addTypeTriples(final Set<Triple> producedTriples, final Set<Triple> saturatedTriples, final TriplesBatch batch) {
		for (final Triple producedTriple : producedTriples) {
			if (!batch.containsTypeTriple(producedTriple)) {
				saturatedTriples.add(producedTriple);
			}
		}
	}

	private int getTypeProperty(final TriplesBatch batch) {
		if (batch.getFullRdfTypeKey() == null && batch.getShortRdfTypeKey() == null)
			return NULL_INT;
		
		if (batch.getFullRdfTypeKey() != null)
			return batch.getFullRdfTypeKey();
		
		return batch.getShortRdfTypeKey();
	}
}
