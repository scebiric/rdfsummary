package fr.inria.oak.RDFSummary.data.encoding;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;

/**
 * RDF triples and schema encoding interface
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Encoding {
	/**
	 * Encodes RDF data and type triples, which are stored in a single triples table.
	 * 
	 * @param connParams
	 * @param triplesTable
	 * @param encodedTriplesTable
	 * @param dictionaryTable
	 * @return The encoding time in milliseconds.
	 * @throws SQLException
	 * @throws UnsupportedDatabaseEngineException
	 */
	public long encodeDataAndTypes(final ConnectionParams connParams, final String triplesTable, final String encodedTriplesTable, final String dictionaryTable) throws SQLException, UnsupportedDatabaseEngineException;

	/**
	 * Encodes the specified RDF Schema.
	 * 
	 * @param connParams
	 * @param schema
	 * @param dictionaryTable
	 * @return The encoding time in milliseconds
	 * @throws UnsupportedDatabaseEngineException
	 * @throws SQLException
	 * @throws DictionaryException
	 */
	public long encodeSchema(final ConnectionParams connParams, final Schema schema, final String dictionaryTable) throws UnsupportedDatabaseEngineException, SQLException, DictionaryException;	
}
