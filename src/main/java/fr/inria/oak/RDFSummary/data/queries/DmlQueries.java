package fr.inria.oak.RDFSummary.data.queries;

import java.io.InputStream;
import java.util.List;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DmlQueries extends QueryProvider {
	
	public String existsSchema(String schema);
	public String existsTable(String qualifiedTableName);
	public String existsIndex(String schema, String indexName);
	public String existTablesWithPrefix(String schemaName, String tablePrefix);
	public String isClusteredOnIndex(String qualifiedTableName, String indexName);
	public String getClusterIndex(String qualifiedTableName);
	
	public String getRowCount(String qualifiedTableName);
	public String getTablesByPrefix(String schema, String prefix);
	public String getSchemasContainingString(String str);
	public String getSequenceName(String table, String column);
	
	public <T> String insert(String table, String[] columns, List<T> values);
	public String insertValues(String table, String[] columns);
	public String insert(String tableName, String[] columnNames, int[] values);
	
	public String select(String[] columns, boolean distinct, String fromTable, String orderByColumn);
	public String selectAllById(String table, int id);
	public String countGroupBy(String table, String column);
	public String distinctColumnValuesCount(String table, String column);
	public String copy(String tableName, InputStream inputStream);
	public String insertValuesPreparedStatement(String table, String[] columns);
	public String existsValueInColumn(String table, String column, String value);
}
