package fr.inria.oak.RDFSummary.data.queries;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesQueriesImpl implements TriplesQueries {

	private StringBuilder sb;
	
	private static final String  SERIAL_UNIQUE_NOT_NULL = "SERIAL UNIQUE NOT NULL";
	private static final String INTEGER_NOT_NULL = "integer NOT NULL";	

	@Override
	public String createDataTriplesTable(String dataTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.CREATE_TABLE).append(dataTriplesTable).append(Chars.SPACE);
		sb.append(Chars.LEFT_PAREN).append(Constant.ID).append(Chars.SPACE).append(SERIAL_UNIQUE_NOT_NULL).append(Chars.COMMA).append(Chars.SPACE); 
		sb.append(Constant.SUBJECT).append(Chars.SPACE).append(INTEGER_NOT_NULL).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.PROPERTY).append(Chars.SPACE).append(INTEGER_NOT_NULL).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.OBJECT).append(Chars.SPACE).append(INTEGER_NOT_NULL).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
				
		return sb.toString();
	}

	@Override
	public String createTypesTable(String typeTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.CREATE_TABLE).append(typeTriplesTable).append(Chars.SPACE);
		sb.append(Chars.LEFT_PAREN).append(Constant.ID).append(Chars.SPACE).append(SERIAL_UNIQUE_NOT_NULL).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.SUBJECT).append(Chars.SPACE).append(INTEGER_NOT_NULL).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.OBJECT).append(Chars.SPACE).append(INTEGER_NOT_NULL).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
				
		return sb.toString();
	}

	@Override
	public String insertDataTriple(String dataTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.INSERT_INTO).append(dataTriplesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		sb.append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.PROPERTY).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.OBJECT).append(Chars.RIGHT_PAREN);
		sb.append(Constant.VALUES).append(Chars.LEFT_PAREN);
		sb.append(Chars.QUESTION_MARK).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Chars.QUESTION_MARK).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Chars.QUESTION_MARK).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
				
		return sb.toString();
	}

	@Override
	public String insertTypeTriple(String typeTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.INSERT_INTO).append(typeTriplesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		sb.append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE);		
		sb.append(Constant.OBJECT).append(Chars.RIGHT_PAREN);
		sb.append(Constant.VALUES).append(Chars.LEFT_PAREN);
		sb.append(Chars.QUESTION_MARK).append(Chars.COMMA).append(Chars.SPACE);		
		sb.append(Chars.QUESTION_MARK).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
				
		return sb.toString();
	}

	@Override
	public String getPropertyObjectPairsBySubject(String dataTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.SELECT).append(Constant.DISTINCT);
		sb.append(Constant.PROPERTY).append(Chars.COMMA).append(Chars.SPACE).append(Constant.OBJECT);
		sb.append(Constant.FROM).append(dataTriplesTable);
		sb.append(Constant.WHERE).append(Constant.SUBJECT).append(Chars.EQUALS).append(Chars.QUESTION_MARK).append(Chars.SEMICOLON);
		
		return sb.toString();
	}

	@Override
	public String getPropertySubjectPairsByObject(String dataTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.SELECT).append(Constant.DISTINCT);
		sb.append(Constant.PROPERTY).append(Chars.COMMA).append(Chars.SPACE).append(Constant.SUBJECT);
		sb.append(Constant.FROM).append(dataTriplesTable);
		sb.append(Constant.WHERE).append(Constant.OBJECT).append(Chars.EQUALS).append(Chars.QUESTION_MARK).append(Chars.SEMICOLON);
				
		return sb.toString();
	}

	@Override
	public String getAllDataTriples(String dataTriplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.SELECT);
		sb.append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.PROPERTY).append(Chars.COMMA).append(Chars.SPACE);
		sb.append(Constant.OBJECT);
		sb.append(Constant.FROM).append(dataTriplesTable).append(Chars.SEMICOLON);
		
		return sb.toString();
	}
	
	@Override
	public String getAllSubjectsAndObjects(String triplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.SELECT);
		sb.append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE).append(Constant.OBJECT);
		sb.append(Constant.FROM).append(triplesTable).append(Chars.SEMICOLON);
		
		return sb.toString();
	}

	@Override
	public String getSubjectAndObjectByProperty(String triplesTable) {
		sb = new StringBuilder();
		sb.append(Constant.SELECT);
		sb.append(Constant.SUBJECT).append(Chars.COMMA).append(Chars.SPACE).append(Constant.OBJECT);
		sb.append(Constant.FROM).append(triplesTable);
		sb.append(Constant.WHERE).append(Constant.PROPERTY).append(Chars.EQUALS).append(Chars.QUESTION_MARK).append(Chars.SEMICOLON);
		
		return sb.toString();
	}
}
