package fr.inria.oak.RDFSummary.data.dao;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.ResultSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TriplesDao extends DataAccessObject {

	public void createDataTriplesTable() throws SQLException;
	
	public void createTypeTriplesTable() throws SQLException;

	public void insertDataTriple(int subject, int property, int object) throws SQLException;
	
	public void insertTypeTriple(int subject, int object) throws SQLException;

	public void prepareInsertStatements() throws SQLException;
	
	public void prepareDataSelectStatements() throws SQLException;

	public void prepareTypesSelectStatements() throws SQLException;
	
	public void prepareSelectStatementsFromAllTriples() throws SQLException;
	
	public void closeInsertStatements() throws SQLException;
	
	public void closeDataSelectStatements() throws SQLException;

	public void closeTypesSelectStatements() throws SQLException;
	
	public void closeSelectStatementsFromAllTriples() throws SQLException;

	public ResultSet getDataPropertyObjectPairsBySubject(int subject) throws SQLException;

	public ResultSet getDataPropertySubjectPairsByObject(int object) throws SQLException;

	public ResultSet getTypeSubjectObjectPairs() throws SQLException;

	public int getDataTriplesCount() throws SQLException;
	
	public int getTypesTriplesCount() throws SQLException;

	public ResultSet getDataClasses() throws SQLException;
	
	public ResultSet getDataProperties() throws SQLException;
	
	public ResultSet getDataTriples() throws SQLException;
	
	public ResultSet getDataTripleSubjects() throws SQLException;
	
	public ResultSet getDataTripleObjects() throws SQLException;
	
	public ResultSet getTypeTripleSubjects() throws SQLException;
	
	/**
	 * Retrieves schema constraints of the specified kind from the table comprising all triples.
	 * 
	 * @param rdfsProperty
	 * @return {@link ResultSet}
	 * @throws SQLException
	 */
	public ResultSet getSubjectObjectPairsByPropertyFromAllTriples(int rdfsProperty) throws SQLException;
}
