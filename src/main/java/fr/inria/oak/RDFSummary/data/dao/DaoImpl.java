package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public abstract class DaoImpl implements DataAccessObject {

	protected final SqlConnectionHandler connHandler;
	protected static Timer Timer;
	protected final int fetchSize;
	
	protected String query = null;

	/**
	 * Constructor of {@link DaoImpl}
	 * 
	 * @param connHandler
	 * @param fetchSize
	 * @param timer
	 * @throws SQLException 
	 */
	public DaoImpl(final SqlConnectionHandler connHandler, final int fetchSize, final Timer timer) throws SQLException {
		this.connHandler = connHandler;
		Timer = timer;
		this.fetchSize = fetchSize;		
	}
	
	protected Statement getNewStatement() throws SQLException {
		return connHandler.createStatement(fetchSize, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
	}
	
	public PreparedStatement prepare(final String query) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(query));
		
		return connHandler.prepareStatement(query);
	}
}
