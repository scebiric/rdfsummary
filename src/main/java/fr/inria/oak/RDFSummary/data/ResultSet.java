package fr.inria.oak.RDFSummary.data;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface ResultSet {

	/**
	 * Moves the cursor froward one row from its current position. A ResultSet cursor is initially positioned before the first row; 
	 * the first call to the method next makes the first row the current row; the second call makes the second row the current row, and so on.
	 * 
	 * @return true if the new current row is valid; false if there are no more rows
	 * @throws SQLException 
	 */
	public boolean next() throws SQLException;

	/**
	 * Retrieves the value of the designated column in the current row of this ResultSet object as an int.
	 * 
	 * @param columnIndex
	 * @return the column value; if the value is SQL NULL, the value returned is 0
	 */
	public int getInt(int columnIndex) throws SQLException;
	
	/**
	 * Retrieves the value of the designated column in the current row of this ResultSet object as a long.
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 */
	public long getLong(int columnIndex) throws SQLException;
	
	/**
	 * Retrieves the value of the designated column in the current row of this ResultSet object as a boolean
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 */
	public boolean getBoolean(int columnIndex) throws SQLException;
	
	/**
	 * Retrieves the value of the designated column in the current row of this ResultSet object as a String
	 * 
	 * @param columnIndex
	 * @return
	 * @throws SQLException
	 */
	public String getString(int columnIndex) throws SQLException;
	
	/**
	 * Releases this ResultSet object's database and JDBC resources immediately instead of waiting for this to happen when it is automatically closed.
	 * @throws SQLException 
	 */
	public void close() throws SQLException;
}
