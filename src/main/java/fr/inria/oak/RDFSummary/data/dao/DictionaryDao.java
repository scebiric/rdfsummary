package fr.inria.oak.RDFSummary.data.dao;

import java.sql.SQLException;
import java.util.Set;

import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DictionaryDao extends DataAccessObject {
	
	/**
	 * Inserts the specified value in the dictionary of the input dataset.
	 * If the value is already encoded, an exception will be thrown.
	 * 
	 * @param value
	 * @return The key for the value 
	 * @throws SQLException 
	 */
	public int insertValue(final String value) throws SQLException;

	/**
	 * Inserts the specified values in the dictionary of the input dataset.
	 * Values already in the dictionary are skipped.
	 * 
	 * @param constants
	 * @throws SQLException 
	 */
	public void insertValues(final Set<String> constants) throws SQLException;
	
	/**
	 * Tries to retrieve the key for the specified value from the dictionary table.
	 * If it does not exist, encodes the value and returns the newly generated key.
	 * 
	 * @param value
	 * @return The key for the specified value 
	 * @throws SQLException 
	 */
	public int getExistingOrNewKey(final String value) throws SQLException;

	/**
	 * Tries to retrieve the key for the specified value from the dictionary table.
	 * If it does not exist, returns null.
	 * 
	 * @param value
	 * @return The key for the specified value
	 * @throws SQLException 
	 */
	public Integer getExistingKey(final String value) throws SQLException;
	
	/**
	 * @param key
	 * @return The value for the specified key 
	 * @throws SQLException 
	 */
	public String getValue(final int key) throws SQLException;
	
	/** Loads all dictionary entries   
	 * @throws SQLException 
	 * @throws DictionaryException */
	public Dictionary loadDictionary() throws SQLException;
	
	/** Loads the dictionary entries for the rdf:type and rdfs properties (full namespace and prefixed) 
	 * @throws SQLException 
	 * @throws DictionaryException */
	public Dictionary loadDictionaryForTypeAndRdfs() throws SQLException, DictionaryException;

	/** Loads the dictionary entries for all schema constants 
	 * @param constants 
	 * @throws SQLException 
	 * @throws DictionaryException */
	public Dictionary loadDictionaryForConstants(final Set<String> constants) throws SQLException, DictionaryException;
	
	/**
	 * Sets the dictionary table
	 * @param dictionaryTable
	 * @throws SQLException 
	 */
	public void setupAccessToDictionaryTable(final String dictionaryTable) throws SQLException;

	/**
	 * Creates the dictionary table in Postgres
	 * @param dictionaryTable
	 * @throws SQLException 
	 */
	public void createDictionaryTable(final String dictionaryTable) throws SQLException;

	/**
	 * @return The number of entries in the dictionary
	 * @throws SQLException 
	 */
	public int getDictionarySize() throws SQLException;
	
	/**
	 * @return The name of the dictionary table which the DAO is currently accessing.
	 */
	public String getCurrentDictionaryTable();

	/**
	 * @return The key with the highest integer value
	 * @throws SQLException 
	 */
	public int getHighestKey() throws SQLException;
}
