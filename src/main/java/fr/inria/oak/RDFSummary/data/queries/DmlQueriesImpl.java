package fr.inria.oak.RDFSummary.data.queries;

import java.io.InputStream;
import java.util.List;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DmlQueriesImpl implements DmlQueries {

	private static StringBuilder Builder;
	
	@Override
	public String getRowCount(String qualifiedTableName) {
		Builder = new StringBuilder("SELECT COUNT(*) FROM ");
		Builder.append(qualifiedTableName).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String select(String[] columns, boolean distinct, String fromTable, String orderByColumn) {
		Builder = new StringBuilder(Constant.SELECT);
		if (distinct)
			Builder.append(Constant.DISTINCT);

		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(Chars.COMMA).append(Chars.SPACE);

		Builder.append(columns[columns.length - 1]).append(Constant.FROM).append(fromTable);
		if (!StringUtils.isNullOrBlank(orderByColumn))
			Builder.append(Constant.ORDER_BY).append(orderByColumn);
		
		Builder.append(Chars.SEMICOLON);

		return Builder.toString();
	}
	
	@Override
	public <T> String insert(String table, String[] columns, List<T> values) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(table).append(Chars.LEFT_PAREN);
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(Chars.COMMA).append(Chars.SPACE);

		Builder.append(columns[columns.length - 1]);
		Builder.append(Chars.RIGHT_PAREN).append(Constant.VALUES).append(Chars.LEFT_PAREN);
		for (T value : values) {
			if (value instanceof String)
				Builder.append(Chars.APOSTROPHE).append(value).append(Chars.APOSTROPHE).append(Chars.COMMA).append(Chars.SPACE);
			else
				Builder.append(value).append(Chars.COMMA).append(Chars.SPACE);		
		}
		
		Builder = new StringBuilder(Builder.toString().substring(0, Builder.toString().length() - 2));
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);

		return Builder.toString();
	}
	
	@Override
	public String existsIndex(String schemaName, String indexName) {
		return "SELECT EXISTS (SELECT 1 FROM pg_indexes "
				+ "WHERE schemaname = '" + schemaName + "' AND indexname = '" + indexName + Chars.APOSTROPHE + Chars.RIGHT_PAREN + Chars.SEMICOLON;
	}
	
	@Override
	public String isClusteredOnIndex(String qualifiedTableName, String indexName) {
		int index = qualifiedTableName.indexOf(Chars.DOT);
		if (index == -1)
			return null;

		String schema = qualifiedTableName.substring(0, index);
		String table = qualifiedTableName.substring(index + 1, qualifiedTableName.length());

		return "SELECT ix.indisclustered "
		+ "FROM pg_class t, pg_class i, pg_index ix, information_schema.tables tables " 
		+ "WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid AND t.relkind = 'r' AND tables.table_name = t.relname " 
		+ "AND tables.table_schema = '" + schema + "' AND t.relname = '" + table + "' AND i.relname = '" + indexName + "';";
	}

	@Override
	public String getClusterIndex(String qualifiedTableName) {
		int index = qualifiedTableName.indexOf(Chars.DOT);
		if (index == -1)
			return null;

		String schema = qualifiedTableName.substring(0, index);
		String table = qualifiedTableName.substring(index + 1, qualifiedTableName.length());

		return "SELECT DISTINCT i.relname "
		+ "FROM pg_class t, pg_class i, pg_index ix, information_schema.tables tables "
		+ "WHERE t.oid = ix.indrelid AND i.oid = ix.indexrelid AND t.relkind = 'r' AND tables.table_name = t.relname "
		+ "AND tables.table_schema = '" + schema + "' AND t.relname = '" + table + "' AND ix.indisclustered = 't';";
	}
	
	@Override
	public String getTablesByPrefix(String schemaName, String prefix) {
		return "SELECT table_name FROM information_schema.tables "
				+ "WHERE table_schema = '" + schemaName + "' AND "
				+ "table_name SIMILAR TO '" + prefix + "%';";
	}
	
	@Override
	public String getSchemasContainingString(String str) {
		return "SELECT schema_name FROM information_schema.schemata "
				+ "WHERE schema_name SIMILAR TO '%" + str + "%';";
	}
	
	@Override
	public String getSequenceName(String table, String column) {
		return "select pg_get_serial_sequence('" + table + Chars.APOSTROPHE + Chars.COMMA + Chars.SPACE + Chars.APOSTROPHE + column +Chars.APOSTROPHE + Chars.RIGHT_PAREN + Chars.SEMICOLON;
	}
	
	@Override
	public String countGroupBy(String table, String column) {
		return Constant.SELECT + column + ", COUNT(*) AS count FROM " + table + Constant.GROUP_BY + column + " ORDER BY count DESC;";
	}
	
	@Override
	public String distinctColumnValuesCount(String table, String column) {
		return "SELECT COUNT (DISTINCT " + column + Chars.RIGHT_PAREN + Chars.SPACE + Constant.FROM + table + Chars.SEMICOLON;
	}
	
	@Override
	public String copy(String tableName, InputStream inputStream) {
		return "COPY "+ tableName + " FROM STDIN WITH CSV DELIMITER '" + Chars.SPACE +"' QUOTE '" + "\"" + "' ESCAPE '" + "\\" + Chars.APOSTROPHE + Chars.SEMICOLON;
	}
	
	@Override
	public String insert(String table, String[] columns, int[] values) {
		Builder = new StringBuilder(Constant.INSERT_INTO + table + Chars.LEFT_PAREN);
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(Chars.COMMA).append(Chars.SPACE);

		Builder.append(columns[columns.length - 1]);
		Builder.append(Chars.RIGHT_PAREN).append(Constant.VALUES).append(Chars.LEFT_PAREN);
		for (int i = 0; i < values.length; i++) {
			Builder.append(values[i]).append(Chars.COMMA).append(Chars.SPACE);		
		}

		Builder = new StringBuilder(Builder.toString().substring(0, Builder.toString().length() - 2));
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String insertValues(final String table, final String[] columns) {
		Preconditions.checkNotNull(columns);
		Preconditions.checkArgument(columns.length > 0); 
		
		Builder = new StringBuilder(Constant.INSERT_INTO + table + Chars.SPACE + Chars.LEFT_PAREN);
		for (int i = 0; i < columns.length - 1; i++) 
			Builder.append(columns[i]).append(Chars.COMMA).append(Chars.SPACE);
		
		Builder.append(columns[columns.length - 1]).append(Constant.VALUES);
		
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(Chars.QUESTION_MARK).append(Chars.COMMA).append(Chars.SPACE);
		
		Builder.append(Chars.QUESTION_MARK).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String existsSchema(String postgresSchemaName) {
		return "SELECT EXISTS (SELECT 1 FROM information_schema.schemata "
				+ "WHERE schema_name = '" + postgresSchemaName + Chars.APOSTROPHE + Chars.RIGHT_PAREN + Chars.SEMICOLON;
	}

	@Override
	public String existsTable(String qualifiedTableName) {
		int index = qualifiedTableName.indexOf(Chars.DOT);
		if (index == -1)
			return null;

		String schema = qualifiedTableName.substring(0, index);
		String table = qualifiedTableName.substring(index + 1, qualifiedTableName.length());

		return "SELECT EXISTS (SELECT 1 FROM information_schema.tables "
		+ "WHERE table_schema = '" + schema + "' AND table_name = '" + table + Chars.APOSTROPHE + Chars.RIGHT_PAREN + Chars.SEMICOLON;
	}
	
	@Override
	public String existTablesWithPrefix(String schemaName, String tablePrefix) {
		return "SELECT EXISTS (SELECT 1 FROM information_schema.tables "
				+ "WHERE table_schema = '" + schemaName + "' AND "
				+ "table_name SIMILAR TO '" + tablePrefix + "%');";
	}

	@Override
	public String insertValuesPreparedStatement(String table, String[] columns) {
		Preconditions.checkNotNull(columns);
		Preconditions.checkArgument(columns.length > 0);
		
		Builder = new StringBuilder(Constant.INSERT_INTO).append(table).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(Chars.COMMA).append(Chars.SPACE);
		
		Builder.append(columns[columns.length - 1]).append(Chars.RIGHT_PAREN).append(Chars.SPACE);
		
		Builder.append("VALUES (");
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(Chars.QUESTION_MARK).append(Chars.COMMA).append(Chars.SPACE);
		
		Builder.append(Chars.QUESTION_MARK).append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String selectAllById(final String table, final int id) {
		return "SELECT * FROM " + table + " WHERE id = " + id + Chars.SEMICOLON;
	}

	@Override
	public String existsValueInColumn(String table, String column, String value) {
		Builder = new StringBuilder();
		Builder.append(Constant.SELECT).append(Constant.EXISTS).append(Chars.LEFT_PAREN);
		Builder.append(Constant.SELECT).append(1);
		Builder.append(Constant.FROM).append(table);
		Builder.append(Constant.WHERE).append(table).append(Chars.DOT).append(column).append(Chars.EQUALS).append(value);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
}
