package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public interface RdfTablesDao extends DataAccessObject {
		
	/**
	 * Selects the s and p columns, ordered by s
	 * 
	 * @param dataTable
	 * @return The result set
	 * @throws SQLException
	 */
	public ResultSet distinctSpOrderByS(String dataTable) throws SQLException;

	/**
	 * Selects the s and p columns, ordered by s
	 * 
	 * @param dataTable
	 * @return The result set
	 * @throws SQLException
	 */
	public ResultSet distinctOpOrderByO(String dataTable) throws SQLException;
	
	/**
	 * Selects the s and p columns, ordered by s, with s untyped
	 * 
	 * @param dataTable
	 * @param typesTable 
	 * @return The result set
	 * @throws SQLException 
	 */
	public ResultSet distinctSpOrderBySWithSUntyped(String dataTable, String typesTable) throws SQLException;

	/**
	 * Selects the o and p columns, ordered by o, with o untyped
	 * 
	 * @param dataTable
	 * @param typesTable
	 * @return The result set
	 * @throws SQLException 
	 */
	public ResultSet distinctOpOrderByOWithOUntyped(String dataTable, String typesTable) throws SQLException;
	
	/**
	 * Selects distinct properties from the specified table, which are different from 
	 * <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, rdf:type and RDFS properties (full and short URIs).
	 * 
	 * @param encodedTriplesTable
	 * @return The result set
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public ResultSet distinctDataProperties(String encodedTriplesTable) throws SQLException, DictionaryException;
	
	/**
	 * Selects distinct classes from the specified table.
	 * 
	 * @param encodedTriplesTable
	 * @param fullTypeKey
	 * @param shortTypeKey
	 * @return The result set
	 * @throws SQLException
	 */
	public ResultSet distinctClassesFromAllTriplesTable(final String encodedTriplesTable, final Integer fullTypeKey, final Integer shortTypeKey) throws SQLException;
	
	/** Creates a table with columns for subject, property and object (text)
	 * @throws SQLException */
	public void createTriplesTable(String tableName) throws SQLException;
	
	/**
	 * Creates a table with columns for subject, property, object and skipped characters (text)
	 * @throws SQLException 
	 */
	public void createTriplesTableWithSkippedChars(String tableName) throws SQLException;
	
	/** Creates a table with columns for subject, property and object (integer)
	 * @throws SQLException */
	public void createEncodedTriplesTable(String encodedTableName) throws SQLException;
	
	/** Creates a table with columns for id, subject, property and object (integer)
	 * @throws SQLException */
	public void createEncodedTriplesTableWithId(String encodedTableName) throws SQLException;
	
	/** Creates a table with columns for id (serial), subject, property and object (text)
	 * @throws SQLException */
	public void createDataTriplesTable(String tableName) throws SQLException;
	
	/** Creates the types table with columns for id (serial), subject and object (text) */
	public void createTypesTable(String typesTable) throws SQLException;
	
	/** Creates the types table with columns for id (serial), subject and object (integer) 
	 * @throws SQLException */
	public String createEncodedTypesTableWithId(String encodedTypesTable) throws SQLException;
	
	/** Populates the types table by selecting type triples from the triples table 
	 * @throws SQLException */
	public String populateTypesTable(String typesTable, String triplesTable) throws SQLException;
	
	/** Populates the encoded types table by selecting type triples from the encoded triples table 
	 * @throws SQLException */
	public String populateEncodedTypesTable(String encodedTypesTable, String encodedTriplesTable, String dictionaryTable) throws SQLException;
	
	/**	Populates the data table by selecting data (non-type, non-RDFS) triples from the triples table 
	 * @throws SQLException */
	public String populateDataTriplesTable(String dataTable, String triplesTable) throws SQLException;
	
	/**	Populates the encoded data table by selecting data (non-type, non-RDFS) triples from the encoded triples table 
	 * @throws SQLException */
	public String populateEncodedDataTriplesTable(String encodedDataTable, String encodedTriplesTable, Dictionary dictionary) throws SQLException;

	/** Creates a table with two integer columns 
	 * @param column1 
	 * @param column2 
	 * @throws SQLException */
	public void createEncodedRepresentationTable(String tableName, String column1, String column2) throws SQLException;
	
	/** Creates the representation table by joining the encoded table with the dictionary on both columns for the input and summary data node 
	 * @throws SQLException */
	public void createDecodedRepresentationTable(String reprTable, String encReprTable, String dictionaryTable) throws SQLException;
		
	/**
	 * Creates the data table by decoding the encoded data table using the dictionary table
	 * 
	 * @param statement 
	 * @param dataTable
	 * @param encodedDataTable
	 * @param dictionaryTable
	 * @throws SQLException 
	 */
	public void decodeSummaryData(String dataTable, String encodedDataTable, String dictionaryTable) throws SQLException;

	/**
	 * Creates the types table by decoding the encoded types table using the dictionary table
	 * 
	 * @param statement
	 * @param typesTable
	 * @param encodedTypesTable
	 * @param dictionaryTable
	 * @throws SQLException 
	 */
	public void decodeSummaryTypes(String typesTable, String encodedTypesTable, String dictionaryTable) throws SQLException;
	
	/**
	 * @param encodedTypesTable 
	 * @return The result set of classes from the type component of the summary (non-schema classes)
	 * @throws SQLException 
	 */
	public ResultSet distinctClassesFromTypeComponent(String encodedTypesTable) throws SQLException;
	
	/**
	 * @param encodedTypesTable
	 * @return The list of encoded classes from the type component of the summary (non-schema classes)
	 * @throws SQLException
	 */
	public List<Integer> getDistinctClassesFromTypeComponent(String encodedTypesTable) throws SQLException;
		
	/**
	 * Exports the specified table to a file in the output folder.
	 * 
	 * @param table
	 * @param columns 
	 * @param outputFolder
	 * @return 
	 * @throws SQLException 
	 */
	public String exportTableToFile(String table, String[] columns, String outputFolder) throws SQLException;

	/**
	 * Inserts an entry to the encoded representation table for all resources in the specified set.
	 * 
	 * @param insertPrepStatement 
	 * @param encReprTable
	 * @param encodedReprColumns
	 * @param representedResources
	 * @param encodedDataNode
	 * @throws SQLException 
	 */
	public void insertToEncodedRepresentationTable(PreparedStatement insertPrepStatement, String encReprTable, String[] encodedReprColumns,
			TIntSet representedResources, int encodedDataNode) throws SQLException;

	/**
	 * @param encodedSummary
	 * @param summaryEncodedDataTable
	 * @param summaryEncodedTypesTable
	 * @throws SQLException 
	 */
	public void populateEncodedSummaryTable(String encodedSummary, String summaryEncodedDataTable, String summaryEncodedTypesTable, int typePropertyKey) throws SQLException;

	/**
	 * @param summaryTable
	 * @param summaryType
	 * @param summaryDataTable
	 * @param summaryTypesTable
	 * @throws SQLException
	 */
	public void createDecodedSummaryTable(String summaryTable, String summaryDataTable, String summaryTypesTable) throws SQLException;

	/**
	 * Creates an encoded table with subject and object columns by joining the decoded triples table with the dictionary table
	 * 
	 * @param encodedSoTable
	 * @param decodedTriplesTable
	 * @param dictionaryTable
	 * @throws SQLException 
	 */
	public void createEncodedSoTable(String encodedSoTable, String decodedTriplesTable, String dictionaryTable) throws SQLException;

	/**
	 * @param classIRI
	 * @param encodedDataTable
	 * @param encodedTypesTable
	 * @return The number of distinct resources of the specified class appearing in the subject or object position of a data triple.
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public int getClassSupport(String classIRI, String encodedDataTable, String encodedTypesTable) throws SQLException, DictionaryException;

	/**
	 * @param dataProperty
	 * @param encodedDataTable
	 * @return The number of data triples having the specified data property.
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public int getDataPropertySupport(String dataProperty, String encodedDataTable) throws SQLException, DictionaryException;

	/**
	 * Extracts all class nodes from the type and schema components into the classesTable.
	 * 
	 * A node is a class if it appears in the object position of type triples,
	 * subject/object position of subclass triples,
	 * or object position of domain/range triples.
	 * 
	 * @param classNodesTable
	 * @param encodedTypesTable
	 * @param encodedSchemaTable
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public void extractAllClassNodes(String classNodesTable, String encodedTypesTable, String encodedSchemaTable) throws SQLException, DictionaryException;
	
	/**
	 * Extracts all property nodes from the data, type and schema components into the propertiesTable.
	 * 
	 * A node is a property if it is an RDF type or RDFS property, or if it appears 
	 * in the property position of data triples,
	 * subject/object position of subproperty triples,
	 * subject position of domain/range triples.
	 * 
	 * @param propertyNodesTable
	 * @param schemaPropertyNodesTable
	 * @param possiblePropertyNodesTable
	 * @param dataTable
	 * @param typesTable
	 * @param schemaTable
	 * @throws SQLException
	 * @throws DictionaryException
	 */
	public void extractAllPropertyNodes(String propertyNodesTable, String schemaPropertyNodesTable,
			String possiblePropertyNodesTable, String dataTable, String typesTable, String schemaTable) throws SQLException, DictionaryException;
	
	/**
	 * Puts the classes from the type component into the classes table.
	 * 
	 * @param classesTable
	 * @param encodedTypesTable
	 * @throws SQLException 
	 */
	public void extractClassNodesFromTypeComponent(final String classesTable, final String encodedTypesTable) throws SQLException;
	
	/**
	 * Puts the classes from schema into the classes table.
	 * 
	 * @param classesTable
	 * @param encodedSchemaTable
	 * @param propertyKeys
	 * @throws SQLException 
	 */
	public void extractClassNodesFromSchemaComponent(String classesTable, String encodedSchemaTable, Map<String, Integer> propertyKeys) throws SQLException;
	
	/**
	 * @param classesTable
	 * @param propertiesTable
	 * @return The number of distinct nodes from a union of class and property nodes.
	 * @throws SQLException 
	 */
	public int getDistinctClassAndPropertyNodeCount(String classesTable, String propertiesTable) throws SQLException;
	
	/**
	 * Selects distinct nodes from subject and object columns of the data table and
	 * stores them to encodedDistinctNodesTable. 
	 * 
	 * @param encodedDistinctNodesTable
	 * @param encodedDataTable
	 * @throws SQLException
	 */
	public void getDistinctNodesFromDataTable(String encodedDistinctNodesTable, String encodedDataTable) throws SQLException;

	/**
	 * @param encodedRepresentationTable
	 * @param gDataNode
	 * @param sDataNode
	 * @return True if there exists a row in the specified table, with the given values of nodes.
	 * @throws SQLException 
	 */
	public boolean existsRepresentationMapping(String encodedRepresentationTable, int gDataNode, int sDataNode) throws SQLException;

	/**
	 * Selects RDFS triples from the encodedTriplesTable and inserts them to schemaTriplesFromInputTable.
	 * 
	 * @param schemaTriplesFromInputTable
	 * @param encodedTriplesTable
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public void getEncodedSchemaTriplesFromInputTable(String schemaTriplesFromInputTable, String encodedTriplesTable) throws DictionaryException, SQLException;

	/**
	 * Copies triples from schemaTriplesFromInputTable to encodedSchemaTable.
	 * 
	 * @param encodedSchemaTable
	 * @param schemaTriplesFromInputTable
	 * @throws SQLException 
	 */
	public void copySchemaTriples(String encodedSchemaTable, String schemaTriplesFromInputTable) throws SQLException;

	/**
	 * @param schemaTriples
	 * @param schemaTable
	 * @param encodedSchemaTable
	 * @param dictionary
	 * @throws SQLException 
	 * @throws InexistentValueException 
	 * @throws DictionaryException 
	 */
	public void insertSchemaTriplesToTables(Set<Triple> schemaTriples, String schemaTable, String encodedSchemaTable, Dictionary dictionary) throws SQLException, DictionaryException, InexistentValueException;

	/**
	 * Drops the table if it exists and creates it with a single column uri column.
	 * 
	 * @param table
	 * @throws SQLException
	 */
	public void recreateEncodedUriTable(String table) throws SQLException;

	public TIntObjectMap<TIntSet> getDomains(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException;

	public TIntObjectMap<TIntSet> getRanges(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException;

	public TIntObjectMap<TIntSet> getSuperProperties(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException;
	
	public TIntObjectMap<TIntSet> getSuperClasses(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException;
	
	public int getDistinctClassesCount(String classNodesTable) throws SQLException; 
}
