package fr.inria.oak.RDFSummary.data.queries;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TriplesQueries {

	public String createDataTriplesTable(String dataTriplesTable);

	public String createTypesTable(String typeTriplesTable);

	public String insertDataTriple(String dataTriplesTable);

	public String insertTypeTriple(String typeTriplesTable);

	public String getPropertyObjectPairsBySubject(String dataTriplesTable);

	public String getPropertySubjectPairsByObject(String dataTriplesTable);

	public String getAllDataTriples(String dataTriplesTable);
	
	public String getAllSubjectsAndObjects(String triplesTable);
	
	public String getSubjectAndObjectByProperty(String triplesTable);

}
