package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.data.JavaSqlResultSetImpl;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.rdfconjunctivequery.IRI;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfTablesDaoImpl extends DaoImpl implements RdfTablesDao {

	private static final Logger log = Logger.getLogger(RdfTablesDao.class);

	private static RdfTablesQueries RdfTablesQueries;
	private static DictionaryDao DictDao;
	private static DdlDao DdlDao;
	private static DmlDao DmlDao;
	private static PerformanceLogger PerformanceLogger;
	 
	private TIntIterator iterator = null;
	private ResultSet resultSet = null;
	private Statement statement = null;
	private static StringBuilder Builder;
	private List<Integer> keys;

	private static final String SELECT_RESULT_SET_MESSAGE = "Result set obtained in ";
	private static final String TABLE_CREATION_MESSAGE = " table created in ";
	private static final String TABLE_POPULATED_MESSAGE = " table populated in ";

	/**
	 * Constructor of {@link RdfTablesDaoImpl}
	 * 
	 * @param connHandler
	 * @param fetchSize
	 * @param rdfTablesQueries
	 * @param dictDao
	 * @param ddlDao
	 * @param dmlDao
	 * @param timer
	 * @param performanceLogger
	 * @throws SQLException 
	 */
	public RdfTablesDaoImpl(final SqlConnectionHandler connHandler, final int fetchSize, final RdfTablesQueries rdfTablesQueries, 
			final DictionaryDao dictDao, final DdlDao ddlDao, final DmlDao dmlDao, final Timer timer, final PerformanceLogger performanceLogger) throws SQLException {
		super(connHandler, fetchSize, timer);
		RdfTablesQueries = rdfTablesQueries;
		DictDao = dictDao;
		DdlDao = ddlDao;
		DmlDao = dmlDao;
		PerformanceLogger = performanceLogger;
		
		keys = Lists.newArrayList();
		this.statement = getNewStatement();
	}

	@Override
	public void releaseResources() throws SQLException {
		if (statement != null)
			statement.close();		
	}

	@Override
	public void createTriplesTable(String tableName) throws SQLException {
		log.info("Creating triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));

		Timer.reset();
		query = RdfTablesQueries.createTriplesTable(tableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(tableName, Timer.getTimeAsString()));
	}

	@Override
	public void createTriplesTableWithSkippedChars(String tableName) throws SQLException {
		log.info("Creating triples table with skipped chars...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));

		Timer.reset();
		query = RdfTablesQueries.createTriplesTableWithSkippedChars(tableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(tableName, Timer.getTimeAsString()));
	}

	@Override
	public void createEncodedTriplesTable(String encodedTableName) throws SQLException {
		log.info("Creating encoded triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTableName));

		Timer.reset();
		query = RdfTablesQueries.createEncodedTriplesTable(encodedTableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(encodedTableName, Timer.getTimeAsString()));
	}

	@Override
	public void createEncodedTriplesTableWithId(String encodedTableName) throws SQLException {
		log.info("Creating encoded triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTableName));

		Timer.reset();
		query = RdfTablesQueries.createEncodedTriplesTableWithId(encodedTableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(encodedTableName, Timer.getTimeAsString()));
	}

	@Override
	public void createDataTriplesTable(String tableName) throws SQLException {
		log.info("Creating data table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));

		Timer.reset();
		query = RdfTablesQueries.createDataTriplesTable(tableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(tableName, Timer.getTimeAsString()));
	}

	@Override
	public void createTypesTable(String tableName) throws SQLException {
		log.info("Creating types table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));
		Timer.reset();
		query = RdfTablesQueries.createTypesTable(tableName);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(tableName, Timer.getTimeAsString()));
	}

	@Override
	public String createEncodedTypesTableWithId(String encodedTypesTable) throws SQLException {
		log.info("Creating encoded types table...");
		Timer.reset();
		query = RdfTablesQueries.createEncodedTypesTableWithId(encodedTypesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(encodedTypesTable, Timer.getTimeAsString()));
		return Timer.getTimeAsString();
	}

	@Override
	public String populateTypesTable(String typesTable, String triplesTable) throws SQLException {
		log.info("Populating the types table with type triples from the triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(triplesTable));

		Timer.reset();
		query = RdfTablesQueries.populateTypesTable(typesTable, triplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info(getTablePopulatedMessage(typesTable, Timer.getTimeAsString()));

		return Timer.getTimeAsString();
	}

	@Override
	public String populateEncodedTypesTable(String encodedTypesTable, String encodedTriplesTable, String dictionaryTable) throws SQLException {
		log.info("Populating the encoded types table with type triples from the encoded triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTypesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		Integer fullTypeKey = DictDao.getExistingKey(Rdf.FULL_TYPE);
		Integer shortTypeKey = DictDao.getExistingKey(Rdf.TYPE);
		if (fullTypeKey == null && shortTypeKey == null)
			return "0 ms";

		String query = RdfTablesQueries.populateEncodedTypesTable(encodedTypesTable, encodedTriplesTable, fullTypeKey, shortTypeKey);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTablePopulatedMessage(encodedTypesTable, Timer.getTimeAsString()));

		return Timer.getTimeAsString();
	}

	@Override
	public String populateDataTriplesTable(String dataTable, String triplesTable) throws SQLException {
		log.info("Populating the data table with data triples from the triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(triplesTable));

		Timer.reset();
		query = RdfTablesQueries.populateDataTriplesTable(dataTable, triplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTablePopulatedMessage(dataTable, Timer.getTimeAsString()));

		return Timer.getTimeAsString();
	}

	@Override
	public String populateEncodedDataTriplesTable(String encodedDataTable, String encodedTriplesTable, Dictionary dictionary) throws SQLException {
		log.info("Populating the encoded data table with data triples from the encoded triples table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));
		Preconditions.checkNotNull(dictionary);

		String query = RdfTablesQueries.populateEncodedDataTriplesTable(encodedDataTable, encodedTriplesTable, dictionary);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTablePopulatedMessage(encodedDataTable, Timer.getTimeAsString()));

		return Timer.getTimeAsString();
	}

	@Override
	public void insertSchemaTriplesToTables(Set<fr.inria.oak.commons.reasoning.rdfs.saturation.Triple> schemaTriples, String schemaTable, String encodedSchemaTable, Dictionary dictionary) throws SQLException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(schemaTriples);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTable));
		Preconditions.checkNotNull(dictionary);

		log.info("Inserting schema triples to schema tables...");
		connHandler.disableAutoCommit();
		final PreparedStatement schemaTableInsert = connHandler.prepareStatement(RdfTablesQueries.insertTriple(schemaTable));
		final PreparedStatement encodedSchemaTableInsert = connHandler.prepareStatement(RdfTablesQueries.insertTriple(encodedSchemaTable));
		fr.inria.oak.RDFSummary.data.dao.Triple myTriple = null;
		for (final fr.inria.oak.commons.reasoning.rdfs.saturation.Triple triple : schemaTriples) {
			myTriple = new fr.inria.oak.RDFSummary.data.dao.Triple(((IRI) triple.subject).toString(), ((IRI) triple.property).toString(), ((IRI) triple.object).toString());
			schemaTableInsert.setString(1, myTriple.getSubject());
			schemaTableInsert.setString(2, myTriple.getProperty());
			schemaTableInsert.setString(3, myTriple.getObject());
			schemaTableInsert.executeUpdate();

			encodedSchemaTableInsert.setInt(1, dictionary.getKey(myTriple.getSubject()));
			encodedSchemaTableInsert.setInt(2, dictionary.getKey(myTriple.getProperty()));
			encodedSchemaTableInsert.setInt(3, dictionary.getKey(myTriple.getObject()));
			encodedSchemaTableInsert.executeUpdate();
		}

		schemaTableInsert.close();
		encodedSchemaTableInsert.close();
		connHandler.enableAutoCommit();
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public ResultSet distinctSpOrderByS(final String dataTable) throws SQLException {
		query = RdfTablesQueries.distinctSpOrderByS(dataTable);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info(getSelectResultSetMessage(Timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.DISTINCT_SP_ORDER_BY_S, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.DISTINCT_SP_ORDER_BY_S, query);

		return new JavaSqlResultSetImpl(resultSet);
	}
	
	@Override
	public int getDistinctClassesCount(final String classNodesTable) throws SQLException {
		query = RdfTablesQueries.getDistinctClassesCount(classNodesTable);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		resultSet.next();
		int count = resultSet.getInt(1);
		resultSet.close();
		
		PerformanceLogger.addTiming(PerformanceLabel.DISTINCT_CLASSES_COUNT, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.DISTINCT_CLASSES_COUNT, query);

		return count;
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public ResultSet distinctOpOrderByO(final String dataTable) throws SQLException {
		query = RdfTablesQueries.distinctOpOrderByO(dataTable);
		log.info(query);		
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info(getSelectResultSetMessage(Timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.DISTINCT_OP_ORDER_BY_O, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.DISTINCT_OP_ORDER_BY_O, query);

		return new JavaSqlResultSetImpl(resultSet);
	}

	/**
	 * {@inheritDoc}
	 * @return 
	 * @throws SQLException 
	 */
	@Override
	public ResultSet distinctSpOrderBySWithSUntyped(String dataTable, String typesTable) throws SQLException {
		query = RdfTablesQueries.distinctSpOrderBySWithSUntyped(dataTable, typesTable);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info(getSelectResultSetMessage(Timer.getTimeAsString()));	
		PerformanceLogger.addTiming(PerformanceLabel.DISTINCT_SP_ORDER_BY_S_UNTYPED, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.DISTINCT_SP_ORDER_BY_S_UNTYPED, query);

		return new JavaSqlResultSetImpl(resultSet);
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public ResultSet distinctOpOrderByOWithOUntyped(String dataTable, String typesTable) throws SQLException {
		query = RdfTablesQueries.distinctOpOrderByOWithOUntyped(dataTable, typesTable);
		log.info(query);
		Timer.reset();
		Timer.start();
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		Timer.stop();
		log.info(getSelectResultSetMessage(Timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.DISTINCT_OP_ORDER_BY_O_UNTYPED, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.DISTINCT_OP_ORDER_BY_O_UNTYPED, query);

		return new JavaSqlResultSetImpl(resultSet);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createEncodedRepresentationTable(String tableName, String column1, String column2) throws SQLException {
		log.info("Creating the encoded representation table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(tableName));
		query = RdfTablesQueries.createEncodedRepresentationTable(tableName, column1, column2);
		log.info(query);
		Timer.reset();
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(tableName, Timer.getTimeAsString()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createDecodedRepresentationTable(String reprTable, String encReprTable, String dictionaryTable) throws SQLException {
		log.info("Decoding the representation table...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(reprTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encReprTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		query = RdfTablesQueries.createDecodedRepresentationTable(reprTable, encReprTable, dictionaryTable);
		log.info(query);
		Timer.reset();
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(reprTable, Timer.getTimeAsString()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decodeSummaryData(final String dataTable, final String encodedDataTable, final String dictionaryTable) throws SQLException {
		log.info("Decoding the summary data triples...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		Timer.reset();
		query = RdfTablesQueries.decodeSummaryData(dataTable, encodedDataTable, dictionaryTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info("Decoded summary data triples in " + Timer.getTimeAsString());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decodeSummaryTypes(final String typesTable, final String encodedTypesTable, final String dictionaryTable) throws SQLException {
		log.info("Decoding the summary type triples...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTypesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		Timer.reset();
		query = RdfTablesQueries.decodeSummaryTypes(typesTable, encodedTypesTable, dictionaryTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info("Decoded summary type triples in " + Timer.getTimeAsString());
	}

	/**
	 * {@inheritDoc} 
	 * @throws SQLException 
	 */
	@Override
	public ResultSet distinctClassesFromTypeComponent(final String encodedTypesTable) throws SQLException {
		query = RdfTablesQueries.classesFromTypeComponent(encodedTypesTable);
		log.info(query);
		return new JavaSqlResultSetImpl(statement.executeQuery(query));
	}

	/**
	 * {@inheritDoc} 
	 * @throws SQLException 
	 */
	@Override
	public List<Integer> getDistinctClassesFromTypeComponent(String encodedTypesTable) throws SQLException {
		List<Integer> classes = Lists.newArrayList();
		query = RdfTablesQueries.classesFromTypeComponent(encodedTypesTable);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next())
			classes.add(resultSet.getInt(1));

		return classes;
	}

	@Override
	public void extractAllClassNodes(String classNodesTable, String encodedTypesTable, String encodedSchemaTable) throws SQLException, DictionaryException {
		Preconditions.checkArgument(isEmptyTable(classNodesTable));

		log.info("Extracting class nodes...");
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		final Map<String, Integer> propertyKeys = Utils.getTypeAndRdfsPropertyKeys();
		this.extractClassNodesFromSchemaComponent(classNodesTable, encodedSchemaTable, propertyKeys);
		this.extractClassNodesFromTypeComponent(classNodesTable, encodedTypesTable);
		timer.stop();
		log.info("All class nodes extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.CLASS_NODES_TOTAL, timer.getTimeInMilliseconds());
	}

	@Override
	public void recreateEncodedUriTable(String classesTable) throws SQLException {
		DdlDao.dropTableIfExists(classesTable);
		query = RdfTablesQueries.createEncodedUriTable(classesTable);
		log.info(query);
		statement.executeUpdate(query);
	}

	@Override
	public void extractClassNodesFromTypeComponent(final String classesTable, final String encodedTypesTable) throws SQLException {
		log.info("Inserting classes from the type component to: " + classesTable + Constant.THREE_DOTS); 
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();

		query = RdfTablesQueries.insertClasses(classesTable, encodedTypesTable, Constant.OBJECT);
		log.info(query);
		statement.executeUpdate(query);

		timer.stop();
		log.info("Class nodes from type component extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.CLASS_NODES_FROM_TYPE, timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.CLASS_NODES_FROM_TYPE, query);
	}

	@Override
	public void extractClassNodesFromSchemaComponent(final String classesTable, final String encodedSchemaTable, final Map<String, Integer> propertyKeys) throws SQLException {
		log.info("Inserting classes from the schema component to: " + classesTable + Constant.THREE_DOTS);
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();

		if (propertyKeys.get(Rdfs.FULL_SUBCLASS) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.FULL_SUBCLASS));
		}
		if (propertyKeys.get(Rdfs.FULL_SUBCLASS) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.FULL_SUBCLASS));
		}
		if (propertyKeys.get(Rdfs.SUBCLASS) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.SUBCLASS));
		}
		if (propertyKeys.get(Rdfs.SUBCLASS) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.SUBCLASS));
		}
		if (propertyKeys.get(Rdfs.FULL_DOMAIN) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.FULL_DOMAIN));
		}
		if (propertyKeys.get(Rdfs.DOMAIN) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.DOMAIN));
		}
		if (propertyKeys.get(Rdfs.FULL_RANGE) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.FULL_RANGE));
		}
		if (propertyKeys.get(Rdfs.RANGE) != null) {
			insertToTargetTableColumnValuesWhereProperty(classesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.RANGE));
		}

		timer.stop();
		log.info("Class nodes from schema component extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.CLASS_NODES_FROM_SCHEMA, timer.getTimeInMilliseconds());
	}

	private void insertToTargetTableColumnValuesWhereProperty(String targetTable, String sourceTable, String sourceColumn, int propertyKey) throws SQLException {
		query = RdfTablesQueries.insert(targetTable, sourceTable, sourceColumn, propertyKey);
		log.info(query);
		statement.executeUpdate(query);
	}

	@Override
	public void extractAllPropertyNodes(String propertyNodesTable, String schemaPropertyNodesTable,
			String possiblePropertyNodesTable, String encodedDataTable, String encodedTypesTable, String encodedSchemaTable) throws SQLException, DictionaryException {
		Preconditions.checkArgument(isEmptyTable(propertyNodesTable));
		Preconditions.checkArgument(isEmptyTable(schemaPropertyNodesTable));
		Preconditions.checkArgument(isEmptyTable(possiblePropertyNodesTable));

		log.info("Extracting property nodes...");
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();		
		final Map<String, Integer> typeAndRdfsPropertyKeys = Utils.getTypeAndRdfsPropertyKeys();
		this.extractPropertyNodesFromSchemaComponent(schemaPropertyNodesTable, encodedSchemaTable, typeAndRdfsPropertyKeys);
		copySchemaPropertyNodes(propertyNodesTable, schemaPropertyNodesTable);

		insertPossiblePropertyNodes(possiblePropertyNodesTable, schemaPropertyNodesTable, encodedDataTable, typeAndRdfsPropertyKeys);
		final String indexName = NameUtils.getIndexName(possiblePropertyNodesTable, Constant.URI);
		final long indexTime = DdlDao.createIndex(indexName, false, possiblePropertyNodesTable, Constant.URI);
		final long clusterTime = DdlDao.clusterTableUsingIndex(possiblePropertyNodesTable, indexName);
		PerformanceLogger.addTiming(PerformanceLabel.INDEX_POSSIBLE_PROPERTY_NODES, indexTime);
		PerformanceLogger.addTiming(PerformanceLabel.CLUSTER_POSSIBLE_PROPERTY_NODES, clusterTime);

		getPropertyNodesFromDataTable(propertyNodesTable, possiblePropertyNodesTable, encodedDataTable, Constant.SUBJECT, PerformanceLabel.PROPERTY_NODES_FROM_DATA_SUBJECTS);
		getPropertyNodesFromDataTable(propertyNodesTable, possiblePropertyNodesTable, encodedDataTable, Constant.OBJECT, PerformanceLabel.PROPERTY_NODES_FROM_DATA_OBJECTS);
		getPropertyNodesFromTypesTable(propertyNodesTable, possiblePropertyNodesTable, encodedTypesTable, Constant.SUBJECT, PerformanceLabel.PROPERTY_NODES_FROM_TYPE);

		timer.stop();
		log.info("All property nodes extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.PROPERTY_NODES_TOTAL, timer.getTimeInMilliseconds());
	}

	private void copySchemaPropertyNodes(String propertiesTable, String schemaPropertiesTable) throws SQLException {
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		query = RdfTablesQueries.copySchemaPropertyNodes(propertiesTable, schemaPropertiesTable);
		log.info(query);
		statement.executeUpdate(query);

		timer.stop();
		log.info("Schema property nodes copied to schema properties table in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.COPY_PROPERTY_NODES, timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.COPY_PROPERTY_NODES, query);
	}

	private void getPropertyNodesFromDataTable(String propertiesTable, String possiblePropertyNodesTable, String encodedDataTable, String dataColumn, String performanceLabel) throws SQLException {
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		query = RdfTablesQueries.getPropertyNodesFromDataTable(propertiesTable, possiblePropertyNodesTable, encodedDataTable, dataColumn);
		log.info(query);
		statement.executeUpdate(query);

		timer.stop();
		log.info("Property nodes from data table column " + dataColumn + " extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(performanceLabel, timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(performanceLabel, query);
	}

	private void getPropertyNodesFromTypesTable(String propertiesTable, String possiblePropertyNodesTable, String encodedTypesTable, String typesColumn, String performanceLabel) throws SQLException {
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		query = RdfTablesQueries.getPropertyNodesFromTypesTable(propertiesTable, possiblePropertyNodesTable, encodedTypesTable, typesColumn);
		log.info(query);
		statement.executeUpdate(query);

		timer.stop();
		log.info("Property nodes from types table extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(performanceLabel, timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(performanceLabel, query);
	}

	/**
	 * Stores possible property nodes to a separate table.
	 * Possible property nodes are those properties which may appear in the subject/object position of a data triple, or in the subject position of a type triple.
	 * These may be properties appearing in property position of data triples, RDF type and RDFS properties. 
	 * 
	 * @param possiblePropertyNodesTable
	 * @param schemaPropertiesTable
	 * @param encodedDataTable
	 * @param typeAndRdfsPropertyKeys
	 * @throws SQLException 
	 */
	private void insertPossiblePropertyNodes(String possiblePropertyNodesTable, String schemaPropertiesTable, String encodedDataTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException {
		Preconditions.checkNotNull(typeAndRdfsPropertyKeys);

		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		query = RdfTablesQueries.insertDataProperties(possiblePropertyNodesTable, encodedDataTable);
		log.info(query);
		statement.executeUpdate(query);
		timer.stop();
		log.info("Data properties extracted to possible property nodes table in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.EXTRACT_DATA_PROPERTIES, timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.EXTRACT_DATA_PROPERTIES, query);

		// rdf:type, subClassOf, subPropertyOf, domain and range may also appear as nodes in the data and/or type component
		String[] columnNames = new String[] { Constant.URI };
		int[] values = new int[1];
		for (int propertyKey : typeAndRdfsPropertyKeys.values()) {
			values[0] = propertyKey;
			DmlDao.insert(possiblePropertyNodesTable, columnNames, values);
		}
	}

	private void extractPropertyNodesFromSchemaComponent(String propertiesTable, String encodedSchemaTable, Map<String, Integer> propertyKeys) throws SQLException {
		log.info("Putting property nodes from schema into the properties table: " + propertiesTable + Constant.THREE_DOTS);
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();

		if (propertyKeys.get(Rdfs.FULL_SUBPROPERTY) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.FULL_SUBPROPERTY));
		}
		if (propertyKeys.get(Rdfs.FULL_SUBPROPERTY) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.FULL_SUBPROPERTY));
		}
		if (propertyKeys.get(Rdfs.SUBPROPERTY) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.SUBPROPERTY));
		}
		if (propertyKeys.get(Rdfs.SUBPROPERTY) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.OBJECT, propertyKeys.get(Rdfs.SUBPROPERTY));
		}
		if (propertyKeys.get(Rdfs.FULL_DOMAIN) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.FULL_DOMAIN));
		}
		if (propertyKeys.get(Rdfs.DOMAIN) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.DOMAIN));
		}
		if (propertyKeys.get(Rdfs.FULL_RANGE) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.FULL_RANGE));
		}
		if (propertyKeys.get(Rdfs.RANGE) != null) {
			insertToTargetTableColumnValuesWhereProperty(propertiesTable, encodedSchemaTable, Constant.SUBJECT, propertyKeys.get(Rdfs.RANGE));
		}

		timer.stop();
		log.info("Property nodes from schema component extracted in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.PROPERTY_NODES_FROM_SCHEMA, timer.getTimeInMilliseconds());
	}

	private boolean isEmptyTable(final String table) throws SQLException {
		return DmlDao.getRowCount(table) == 0;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Exporting to a CSV file.
	 * The filename is equal to the table name.
	 * @return The filepath
	 * @throws SQLException 
	 */
	@Override
	public String exportTableToFile(final String table, final String[] columns, final String outputFolder) throws SQLException {
		String filepath = Utils.getFilepath(outputFolder, table, "csv");
		query = RdfTablesQueries.copyTriplesToCsv(table, columns, filepath);
		log.info(query);
		connHandler.disableAutoCommit();
		Timer.reset();
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		connHandler.enableAutoCommit();
		log.info("Copying done in " + Timer.getTimeAsString());

		return filepath;
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public void insertToEncodedRepresentationTable(final PreparedStatement insertPrepStatement, final String encReprTable, final String[] encodedReprColumns,
			final TIntSet representedResources, final int encodedDataNode) throws SQLException {
		insertPrepStatement.setInt(2, encodedDataNode);
		iterator = representedResources.iterator();
		while (iterator.hasNext()) {
			insertPrepStatement.setInt(1, iterator.next());
			insertPrepStatement.executeUpdate();
		}
	}

	@Override
	public void populateEncodedSummaryTable(final String encodedSummary, final String summaryEncodedDataTable, final String summaryEncodedTypesTable, int typePropertyKey) throws SQLException {
		Timer.reset();
		query = RdfTablesQueries.populateEncodedSummaryTable(encodedSummary, summaryEncodedDataTable, summaryEncodedTypesTable, typePropertyKey);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTablePopulatedMessage(encodedSummary, Timer.getTimeAsString()));
	}

	@Override
	public void createDecodedSummaryTable(final String summaryTable, final String summaryDataTable, final String summaryTypesTable) throws SQLException {
		Timer.reset();
		query = RdfTablesQueries.createDecodedSummaryTable(summaryTable, summaryDataTable, summaryTypesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(getTableCreationMessage(summaryTable, Timer.getTimeAsString()));
	}

	@Override
	public void createEncodedSoTable(final String encodedSoTable, final String decodedTriplesTable, final String dictionaryTable) throws SQLException {
		log.info("Creating encoded (s,o) table: " + encodedSoTable + Constant.THREE_DOTS);
		Timer.reset();
		query = RdfTablesQueries.createEncodedSoTable(encodedSoTable, decodedTriplesTable, dictionaryTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();
		log.info(getTableCreationMessage(encodedSoTable, Timer.getTimeAsString()));
	}

	@Override
	public ResultSet distinctDataProperties(final String encodedTriplesTable) throws SQLException, DictionaryException {
		log.info("Selecting distinct data properties...");
		final Dictionary dictionary = DictDao.loadDictionaryForTypeAndRdfs();
		query = RdfTablesQueries.distinctDataProperties(encodedTriplesTable, dictionary);
		log.info(query);

		return new JavaSqlResultSetImpl(statement.executeQuery(query));		
	}

	@Override
	public ResultSet distinctClassesFromAllTriplesTable(final String encodedTriplesTable, final Integer fullTypeKey, final Integer shortTypeKey) throws SQLException {
		log.info("Selecting distinct classes...");		
		if (fullTypeKey == null && shortTypeKey == null) {
			log.info("No classes found.");
			return null;
		}

		query = RdfTablesQueries.distinctClasses(encodedTriplesTable, fullTypeKey, shortTypeKey);
		log.info(query);

		return new JavaSqlResultSetImpl(statement.executeQuery(query));	
	}

	@Override
	public int getClassSupport(final String classIRI, final String encodedDataTable, final String encodedTypesTable) throws SQLException, DictionaryException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classIRI));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTypesTable));

		log.info("Getting support for class: " + classIRI + Constant.THREE_DOTS);

		Integer encodedClass = DictDao.getExistingKey(classIRI);
		if (encodedClass == null)
			throw new DictionaryException("Invalid class."); 

		if (!DmlDao.existsTable(Constant.ENCODED_DISTINCT_NODES_TABLE))
			getDistinctNodesFromDataTable(Constant.ENCODED_DISTINCT_NODES_TABLE, encodedDataTable);

		query = RdfTablesQueries.classSupport(encodedClass.intValue(), Constant.ENCODED_DISTINCT_NODES_TABLE, encodedTypesTable);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		resultSet.next();

		return Utils.getIntegerColumn(resultSet, 1);
	}

	@Override
	public void getDistinctNodesFromDataTable(String encodedDistinctNodesTable, String encodedDataTable) throws SQLException {
		log.info("Computing distinct nodes from " + encodedDataTable + Constant.THREE_DOTS);
		Timer.reset();
		Timer.start();
		query = RdfTablesQueries.getDistinctNodesFromDataTable(encodedDistinctNodesTable, encodedDataTable);
		log.info(query);
		statement.executeUpdate(query);
		Timer.stop();

		log.info("Distinct nodes computed in " + Timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.COMPUTE_DISTINCT_NODES, Timer.getTimeInMilliseconds());
		PerformanceLogger.addQuery(PerformanceLabel.COMPUTE_DISTINCT_NODES, query);
	}	

	@Override
	public int getDataPropertySupport(final String dataProperty, final String encodedDataTable) throws SQLException, DictionaryException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataProperty));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTable));

		log.info("Getting support for data property: " + dataProperty + Constant.THREE_DOTS);

		Integer encodedDataProperty = DictDao.getExistingKey(dataProperty);
		if (encodedDataProperty == null)
			throw new DictionaryException("Invalid data property."); 

		query = RdfTablesQueries.dataPropertySupport(encodedDataProperty, encodedDataTable);
		log.info(query);

		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		resultSet.next();

		return Utils.getIntegerColumn(resultSet, 1);
	}

	@Override
	public int getDistinctClassAndPropertyNodeCount(String classesTable, String propertiesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertiesTable));

		log.info("Computing the number of distinct class and property nodes...");

		query = RdfTablesQueries.getDistinctClassAndPropertyNodeCount(classesTable, propertiesTable);
		log.info(query);
		resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
		resultSet.next(); 
		int count = resultSet.getInt(1);
		resultSet.close();

		return count;
	}

	@Override
	public boolean existsRepresentationMapping(String encodedRepresentationTable, int gDataNode, int sDataNode) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedRepresentationTable));
		Preconditions.checkArgument(gDataNode > 0);
		Preconditions.checkArgument(sDataNode > 0);

		query = RdfTablesQueries.existsRepresentationMapping(encodedRepresentationTable, gDataNode, sDataNode);
		resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
		resultSet.next();
		boolean exists = resultSet.getBoolean(1);
		resultSet.close();

		return exists;
	}

	@Override
	public void getEncodedSchemaTriplesFromInputTable(String schemaTriplesFromInputTable, String encodedTriplesTable) throws DictionaryException, SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaTriplesFromInputTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));

		log.info("Retrieving schema triples from input table...");
		Timer.reset();
		Timer.start();
		final Map<String, Integer> propertyKeys = Utils.getTypeAndRdfsPropertyKeys();
		for (final String property : propertyKeys.keySet()) {
			if (property.equals(Rdf.FULL_TYPE) || property.equals(Rdf.TYPE))
				continue;

			query = RdfTablesQueries.getEncodedSchemaTriplesFromInputTable(schemaTriplesFromInputTable, encodedTriplesTable, propertyKeys.get(property).intValue());
			log.info(query);
			statement.executeUpdate(query);
		}
		Timer.stop();
		log.info("Schema triples from input table retrieved in: " + Timer.getTimeAsString());
	}

	@Override
	public void copySchemaTriples(String encodedSchemaTable, String schemaTriplesFromInputTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaTriplesFromInputTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTable));

		query = RdfTablesQueries.copySchemaTriples(encodedSchemaTable, schemaTriplesFromInputTable);
		log.info(query);
		statement.executeUpdate(query);
	}

	private String getTableCreationMessage(final String tableName, final String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(tableName).append(TABLE_CREATION_MESSAGE).append(timeAsString);

		return Builder.toString();
	}

	private String getTablePopulatedMessage(String typesTable, String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(typesTable).append(TABLE_POPULATED_MESSAGE).append(timeAsString);

		return Builder.toString();
	}

	private String getSelectResultSetMessage(String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(SELECT_RESULT_SET_MESSAGE).append(timeAsString);

		return Builder.toString();
	}

	@Override
	public TIntObjectMap<TIntSet> getDomains(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException {
		final TIntObjectMap<TIntSet> domainsByPropertyMap = new TIntObjectHashMap<TIntSet>();

		keys.clear();
		if (typeAndRdfsPropertyKeys.get(Rdfs.FULL_DOMAIN) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.FULL_DOMAIN));
		if (typeAndRdfsPropertyKeys.get(Rdfs.DOMAIN) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.DOMAIN)); 

		int propertyIRI = -1;
		int classIRI = -1;
		for (final int domainPropertyKey : keys) {
			query = RdfTablesQueries.selectSubjectObjectWhereProperty(encodedSchemaTable, domainPropertyKey);
			log.info(query);
			resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
			while (resultSet.next()) {
				propertyIRI = resultSet.getInt(1);
				classIRI = resultSet.getInt(2);
				TIntSet domains = domainsByPropertyMap.get(propertyIRI);
				if (domains == null) {
					domains = new TIntHashSet();
					domains.add(classIRI);
					domainsByPropertyMap.put(propertyIRI, domains);
				}
				else
					domains.add(classIRI);
			}
			resultSet.close();
		}

		return domainsByPropertyMap;
	}

	@Override
	public TIntObjectMap<TIntSet> getRanges(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException {
		final TIntObjectMap<TIntSet> rangesByPropertyMap = new TIntObjectHashMap<TIntSet>();

		keys.clear();
		if (typeAndRdfsPropertyKeys.get(Rdfs.FULL_RANGE) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.FULL_RANGE));
		if (typeAndRdfsPropertyKeys.get(Rdfs.RANGE) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.RANGE)); 

		int propertyIRI = -1;
		int classIRI = -1;
		for (final int rangePropertyKey : keys) {
			query = RdfTablesQueries.selectSubjectObjectWhereProperty(encodedSchemaTable, rangePropertyKey);
			resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
			while (resultSet.next()) {
				propertyIRI = resultSet.getInt(1);
				classIRI = resultSet.getInt(2);
				TIntSet ranges = rangesByPropertyMap.get(propertyIRI);
				if (ranges == null) {
					ranges = new TIntHashSet();
					ranges.add(classIRI);
					rangesByPropertyMap.put(propertyIRI, ranges);
				}
				else
					ranges.add(classIRI);
			}
			resultSet.close();
		}

		return rangesByPropertyMap;
	}

	@Override
	public TIntObjectMap<TIntSet> getSuperClasses(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException {
		final TIntObjectMap<TIntSet> superClassesByClass = new TIntObjectHashMap<TIntSet>();

		keys.clear();
		if (typeAndRdfsPropertyKeys.get(Rdfs.FULL_SUBCLASS) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.FULL_SUBCLASS));
		if (typeAndRdfsPropertyKeys.get(Rdfs.SUBCLASS) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.SUBCLASS)); 

		int subClass = -1;
		int superClass = -1;
		for (final int subClassPropertyKey : keys) {
			query = RdfTablesQueries.selectSubjectObjectWhereProperty(encodedSchemaTable, subClassPropertyKey);
			resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
			while (resultSet.next()) {
				subClass = resultSet.getInt(1);
				superClass = resultSet.getInt(2);
				TIntSet superClasses = superClassesByClass.get(subClass);;
				if (superClasses == null) {
					superClasses = new TIntHashSet();
					superClasses.add(superClass);
					superClassesByClass.put(subClass, superClasses);					
				}
				else
					superClasses.add(superClass);
			}
			resultSet.close();
		}

		return superClassesByClass;
	}

	@Override
	public TIntObjectMap<TIntSet> getSuperProperties(String encodedSchemaTable, Map<String, Integer> typeAndRdfsPropertyKeys) throws SQLException {
		final TIntObjectMap<TIntSet> superPropertiesByProperty = new TIntObjectHashMap<TIntSet>();

		keys.clear();
		if (typeAndRdfsPropertyKeys.get(Rdfs.FULL_SUBPROPERTY) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.FULL_SUBPROPERTY));
		if (typeAndRdfsPropertyKeys.get(Rdfs.SUBPROPERTY) != null)
			keys.add(typeAndRdfsPropertyKeys.get(Rdfs.SUBPROPERTY)); 

		int subProperty = -1;
		int superProperty = -1;
		for (final int subPropertyKey : keys) {
			query = RdfTablesQueries.selectSubjectObjectWhereProperty(encodedSchemaTable, subPropertyKey);
			resultSet = new JavaSqlResultSetImpl(statement.executeQuery(query));
			while (resultSet.next()) {
				subProperty = resultSet.getInt(1);
				superProperty = resultSet.getInt(2);
				TIntSet superProperties = superPropertiesByProperty.get(subProperty);
				if (superProperties == null) {
					superProperties = new TIntHashSet();
					superProperties.add(superProperty);
					superPropertiesByProperty.put(subProperty, superProperties);					
				}
				else
					superProperties.add(superProperty);
			}
			resultSet.close();
		}
		
		return superPropertiesByProperty;
	}


}