package fr.inria.oak.RDFSummary.data.queries;

import java.util.Set;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * Implements {@link DictionaryQueries}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DictionaryQueriesImpl implements DictionaryQueries {

	private static StringBuilder Builder;
	private static final String VALUE = "value = '";
	
	@Override
	public String selectKeyWhereValue(final String dictionaryTable) {
		Builder = new StringBuilder("SELECT key FROM ");
		Builder.append(dictionaryTable).append(" WHERE value = ?;");
				
		return Builder.toString();
	}

	@Override
	public String selectValueWhereKey(final String dictionaryTable) {
		Builder = new StringBuilder("SELECT value FROM ");
		Builder.append(dictionaryTable).append(" WHERE key = ?;");
						
		return Builder.toString();
	}

	@Override
	public String insertKeyValue(final String dictionaryTable) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(dictionaryTable).append(" (key, value) VALUES (?, ?);");
		
		return Builder.toString();
	}

	@Override
	public String insertValue(final String dictionaryTable) {
		Builder = new StringBuilder(Constant.INSERT_INTO);
		Builder.append(dictionaryTable).append(" (value) VALUES (?) RETURNING key;");
		
		return Builder.toString();
	}

	@Override
	public String selectKeyValue(final String dictionaryTable) {
		Builder = new StringBuilder("SELECT key, value FROM ");
		Builder.append(dictionaryTable).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String loadDictionaryForTypeAndRdfs(String dictionaryTable) {
		Builder = new StringBuilder("SELECT key, value FROM "); 
		Builder.append(dictionaryTable).append(Constant.WHERE);
		Builder.append(VALUE).append(Rdf.FULL_TYPE).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdf.TYPE).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.FULL_DOMAIN).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.DOMAIN).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.FULL_RANGE).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.RANGE).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.FULL_SUBCLASS).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.SUBCLASS).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.FULL_SUBPROPERTY).append(Chars.APOSTROPHE).append(Constant.OR);
		Builder.append(VALUE).append(Rdfs.SUBPROPERTY).append(Chars.APOSTROPHE).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String loadDictionaryForConstants(Set<String> constants, String dictionaryTable) {
		Preconditions.checkNotNull(constants);
		Preconditions.checkArgument(constants.size() > 0);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		Builder = new StringBuilder(Constant.WHERE);
		for (String constant : constants) 
			Builder.append(VALUE).append(constant).append(Chars.APOSTROPHE).append(Constant.OR);

		String whereClause = Builder.toString().substring(0, Builder.toString().length() - 4);
		Builder = new StringBuilder("SELECT key, value FROM ").append(dictionaryTable).append(whereClause).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String createDictionaryTable(String dictionaryTable) {
		Builder = new StringBuilder("CREATE TABLE ");
		Builder.append(dictionaryTable).append(" (key SERIAL UNIQUE NOT NULL, value text NOT NULL);");
		
		return Builder.toString();
	}

	@Override
	public String getDictionarySize(String dictionaryTable) {
		Builder = new StringBuilder("SELECT COUNT(*) FROM ");
		Builder.append(dictionaryTable).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String getHighestKey(String dictionaryTable) {
		Builder = new StringBuilder("SELECT MAX(key) FROM ");
		Builder.append(dictionaryTable).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
}
