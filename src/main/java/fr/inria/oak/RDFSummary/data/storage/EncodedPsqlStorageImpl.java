package fr.inria.oak.RDFSummary.data.storage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.constants.ColumnArray;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.encoding.Encoding;
import fr.inria.oak.RDFSummary.data.index.Index;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveSchema;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturator;
import fr.inria.oak.RDFSummary.data.storage.splitter.StorageSplitter;
import fr.inria.oak.RDFSummary.rdf.parser.RdfParser;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.SchemaParser;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.SchemaUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.StorageSchema;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 *
 * @author Sejla CEBIRIC
 *
 */
public class EncodedPsqlStorageImpl implements EncodedPsqlStorage {

	private static final Logger log = Logger.getLogger(EncodedPsqlStorage.class);

	private final RdfParser rdfParser;
	private final DdlDao ddlDao;
	private final DictionaryDao dictDao;
	private final Encoding encoding;
	private final PostgresSaturator postgresEncodedSaturator;
	private final StorageSplitter storageSplitter;
	private final Index index;
	private final DmlDao dmlDao;
	private final RdfTablesDao rdfTablesDao;
	private final SchemaParser schemaParser;
	private final Timer schemaTimer;

	/**
	 * @param dataLoader
	 * @param ddlDao
	 * @param dictDao
	 * @param encoding
	 * @param postgresEncodedSaturator
	 * @param storageSplitter
	 * @param index
	 * @param dmlDao
	 * @param rdfTablesDao
	 * @param schemaParser
	 * @param schemaTimer
	 */
	public EncodedPsqlStorageImpl(final RdfParser dataLoader, final DdlDao ddlDao, final DictionaryDao dictDao, final Encoding encoding, 
			final PostgresSaturator postgresEncodedSaturator, final StorageSplitter storageSplitter, final Index index,
			final DmlDao dmlDao, final RdfTablesDao rdfTablesDao, final SchemaParser schemaParser, final Timer schemaTimer) {
		rdfParser = dataLoader;
		this.ddlDao = ddlDao;
		this.dictDao = dictDao;
		this.encoding = encoding;
		this.postgresEncodedSaturator = postgresEncodedSaturator;
		this.storageSplitter = storageSplitter;
		this.index = index;
		this.dmlDao = dmlDao;
		this.rdfTablesDao = rdfTablesDao;
		this.schemaParser = schemaParser;
		this.schemaTimer = schemaTimer;
	}

	/**
	 * Table names converted to lowercase since Postgres forces lowercase names of database objects.
	 */
	@Override
	public DatasetPreparationResult prepareDataset(final ConnectionParams connParams, final String postgresSchemaName, final boolean dropSchema, final String dictionaryTable,
			final String inputTriplesTable, final String dataTriplesFilepath, final String schemaTriplesFilepath, final boolean indexing) throws SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(postgresSchemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(inputTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTriplesFilepath));	

		log.info("Storing the RDF dataset to Postgres in " + StorageSchema.TRIPLES_TABLE + " layout...");
		PostgresTableNames tables = new PostgresTableNames();

		// Load data+type triples (non-encoded)
		String triplesTable = NameUtils.getTriplesTableName(postgresSchemaName, inputTriplesTable);
		tables.setTriplesTable(triplesTable);
		String dataAndTypesLoadingTime = Constant.DEFAULT_TIME;
		if (dropSchema)
			ddlDao.dropSchema(postgresSchemaName);

		if (dmlDao.existsTable(triplesTable)) 
			log.info(triplesTable + " table already exists.");			
		else 
			dataAndTypesLoadingTime = Long.toString(rdfParser.parse(dataTriplesFilepath));

		// Encode data and types
		String encodedTriplesTable = NameUtils.getEncodedTriplesTableName(postgresSchemaName, inputTriplesTable);
		String dataAndTypesEncodingTime = encodeDataAndTypes(encodedTriplesTable, dictionaryTable, tables, connParams);

		// Load schema
		schemaTimer.reset();
		schemaTimer.start();
		String schemaTable = NameUtils.getSchemaTableName(tables.getTriplesTable());
		String encodedSchemaTable = NameUtils.getEncodedSchemaTableName(tables.getTriplesTable());
		final Schema schema = loadRdfSchema(schemaTriplesFilepath, schemaTable, encodedSchemaTable, tables);
		schemaTimer.stop();
		String schemaLoadingTime = schemaTimer.getTimeAsString();

		// Encode schema if it exists
		String schemaEncodingTime = encoding.encodeSchema(connParams, schema, dictionaryTable) + Constant.MILLISECONDS;

		// Export schema triples from memory to table(s)
		if (!dmlDao.existsTable(schemaTable)) 
			exportSchemaToTables(schema, schemaTable, encodedSchemaTable);

		// Load dictionary entries for rdf:type and RDFS properties to memory
		final Dictionary dictionary = dictDao.loadDictionaryForTypeAndRdfs();

		// Split storage		
		String encodedDataTableName = NameUtils.getEncodedDataTableName(postgresSchemaName, inputTriplesTable);
		String encodedTypesTableName = NameUtils.getEncodedTypesTableName(postgresSchemaName, inputTriplesTable);
		log.info("Splitting the encoded triples table...");		
		String createEncodedTypesTableTime = storageSplitter.createEncodedTypesTable(encodedTypesTableName, tables, dictionaryTable);
		String createEncodedDataTableTime = storageSplitter.createEncodedDataTable(encodedDataTableName, tables, dictionary);
		log.info("Type triples extracted in: " + createEncodedTypesTableTime);
		log.info("Data triples extracted in: " + createEncodedDataTableTime);

		// Index
		index.createIndexes();		

		return new DatasetPreparationResult(tables, schema, new TriplesLoadingTimes(dataAndTypesLoadingTime, schemaLoadingTime), new EncodingTimes(dataAndTypesEncodingTime, schemaEncodingTime));		
	}

	@Override
	public DatasetPreparationResult prepareDataset(final ConnectionParams connParams, 
			final String postgresSchemaName, final boolean dropSchema, final boolean dropSaturatedTable, 
			final String dictionaryTable, final String inputTriplesTable, 
			final String dataTriplesFilepath, final String schemaTriplesFilepath, 
			final boolean indexing, final int saturationBatchSize) throws SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(postgresSchemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(inputTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTriplesFilepath));	

		log.info("Storing the RDF dataset to Postgres in " + StorageSchema.TRIPLES_TABLE + " layout...");
		PostgresTableNames storageLayout = new PostgresTableNames();

		// Load data+type triples (non-encoded)
		String triplesTable = NameUtils.getTriplesTableName(postgresSchemaName, inputTriplesTable);
		storageLayout.setTriplesTable(triplesTable);
		String dataAndTypesLoadingTime = Constant.DEFAULT_TIME;
		if (dropSchema)
			ddlDao.dropSchema(postgresSchemaName);

		if (dmlDao.existsTable(triplesTable)) 
			log.info(triplesTable + " table already exists.");			
		else 
			dataAndTypesLoadingTime = Long.toString(rdfParser.parse(dataTriplesFilepath));

		// Encode data and types
		String encodedTriplesTable = NameUtils.getEncodedTriplesTableName(postgresSchemaName, inputTriplesTable);
		String dataAndTypesEncodingTime = encodeDataAndTypes(encodedTriplesTable, dictionaryTable, storageLayout, connParams);

		// Load schema
		schemaTimer.reset();
		schemaTimer.start();
		String schemaTable = NameUtils.getSchemaTableName(storageLayout.getTriplesTable());
		String encodedSchemaTable = NameUtils.getEncodedSchemaTableName(storageLayout.getTriplesTable());
		final Schema schema = loadRdfSchema(schemaTriplesFilepath, schemaTable, encodedSchemaTable, storageLayout);
		schemaTimer.stop();
		String schemaLoadingTime = schemaTimer.getTimeAsString();

		// Encode schema if it exists
		String schemaEncodingTime = encoding.encodeSchema(connParams, schema, dictionaryTable) + Constant.MILLISECONDS;

		// Export schema triples from memory to table(s)
		if (!dmlDao.existsTable(schemaTable)) 
			exportSchemaToTables(schema, schemaTable, encodedSchemaTable);

		// Saturate the encoded triples table
		if (schema == null || schema.getAllTriples().size() == 0)
			throw new PsqlStorageException("This method requires a schema in order to saturate the input.");

		final String saturatedEncodedTriplesTable = NameUtils.getSaturatedEncodedTriplesTableName(encodedTriplesTable);
		final TroveSchema encodedSchema = loadEncodedRdfSchema(encodedSchemaTable);
		final Integer fullRdfTypeKey = dictDao.getExistingKey(Rdf.FULL_TYPE);
		final Integer shortRdfTypeKey = dictDao.getExistingKey(Rdf.TYPE);
		long satTime = postgresEncodedSaturator.saturate(saturatedEncodedTriplesTable, encodedTriplesTable, dropSaturatedTable, encodedSchema, fullRdfTypeKey, shortRdfTypeKey, saturationBatchSize);
		storageLayout.setEncodedTriplesTable(saturatedEncodedTriplesTable);

		// Load dictionary entries for rdf:type and RDFS properties to memory
		final Dictionary dictionary = dictDao.loadDictionaryForTypeAndRdfs(); 

		// Split storage		
		String encodedDataTableName = NameUtils.getSaturatedEncodedDataTableName(saturatedEncodedTriplesTable);
		String encodedTypesTableName = NameUtils.getSaturatedEncodedTypesTableName(saturatedEncodedTriplesTable);
		log.info("Splitting the encoded triples table...");		
		String createEncodedTypesTableTime = storageSplitter.createEncodedTypesTable(encodedTypesTableName, storageLayout, dictionaryTable);
		String createEncodedDataTableTime = storageSplitter.createEncodedDataTable(encodedDataTableName, storageLayout, dictionary);
		log.info("Type triples extracted in: " + createEncodedTypesTableTime);
		log.info("Data triples extracted in: " + createEncodedDataTableTime);

		// // Index
		index.createIndexes();		

		return new DatasetPreparationResult(storageLayout, schema, new TriplesLoadingTimes(dataAndTypesLoadingTime, schemaLoadingTime), new EncodingTimes(dataAndTypesEncodingTime, schemaEncodingTime), satTime);
	}

	private TroveSchema loadEncodedRdfSchema(final String encodedSchemaTable) throws DictionaryException, SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTable));

		final Map<String, Integer> typeAndRdfsPropertyKeys = Utils.getTypeAndRdfsPropertyKeys();

		final TIntObjectMap<TIntSet> domainsForProperty = rdfTablesDao.getDomains(encodedSchemaTable, typeAndRdfsPropertyKeys);
		final TIntObjectMap<TIntSet> rangesForProperty = rdfTablesDao.getRanges(encodedSchemaTable, typeAndRdfsPropertyKeys);
		final TIntObjectMap<TIntSet> superClassesForClass = rdfTablesDao.getSuperClasses(encodedSchemaTable, typeAndRdfsPropertyKeys);
		final TIntObjectMap<TIntSet> superPropertiesForProperty = rdfTablesDao.getSuperProperties(encodedSchemaTable, typeAndRdfsPropertyKeys);

		return new TroveSchema(domainsForProperty, rangesForProperty, superClassesForClass, superPropertiesForProperty);
	}

	/**
	 * @param schema
	 * @param schemaTable
	 * @param encodedSchemaTable
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 */
	private void exportSchemaToTables(Schema schema, String schemaTable, String encodedSchemaTable) throws SQLException, DictionaryException, InexistentValueException {
		rdfTablesDao.createTriplesTable(schemaTable);
		rdfTablesDao.createEncodedTriplesTableWithId(encodedSchemaTable);

		log.info("Retrieving all triples from the schema...");
		final Set<fr.inria.oak.commons.reasoning.rdfs.saturation.Triple> schemaTriples = schema.getAllTriples();
		Set<String> constants = SchemaUtils.getConstants(schema);
		final Dictionary dictionary = dictDao.loadDictionaryForConstants(constants);
		if (schemaTriples != null) 
			rdfTablesDao.insertSchemaTriplesToTables(schemaTriples, schemaTable, encodedSchemaTable, dictionary);
	}

	/**
	 * @param schemaTriplesFilepath
	 * @param schemaTable
	 * @param encodedSchemaTable
	 * @param storageLayout
	 * @return Total schema loading time
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws SchemaException 
	 * @throws FileNotFoundException 
	 */
	private Schema loadRdfSchema(final String schemaTriplesFilepath, final String schemaTable, final String encodedSchemaTable, final PostgresTableNames storageLayout) throws SQLException, DictionaryException, InexistentValueException, SchemaException, FileNotFoundException {
		// Parse triples from schema file if specified
		Schema schema = null;
		if (StringUtils.isNullOrBlank(schemaTriplesFilepath))
			schema = new Schema();
		else {
			log.info("Parsing RDFS from: " + schemaTriplesFilepath);
			schema = schemaParser.parseSchema(schemaTriplesFilepath, null, RdfUtils.getRDFLanguage(schemaTriplesFilepath));
			log.info("Schema triples parsed from: " + schemaTriplesFilepath);
		}

		// Tables
		final String schemaTriplesFromInputTable = encodedSchemaTable.concat("_from_g");
		storageLayout.setSchemaTable(schemaTable);
		storageLayout.setEncodedSchemaTable(encodedSchemaTable);

		if (dmlDao.existsTable(schemaTable)) {
			log.info("Schema tables already exist.");
		}
		else {
			// Get schema triples from the input table
			ddlDao.dropTableIfExists(schemaTriplesFromInputTable);
			rdfTablesDao.createEncodedTriplesTableWithId(schemaTriplesFromInputTable);
			rdfTablesDao.getEncodedSchemaTriplesFromInputTable(schemaTriplesFromInputTable, storageLayout.getEncodedTriplesTable());
		}

		loadSchemaTriplesFromTable(schema, schemaTriplesFromInputTable);

		return schema;
	}

	public void loadSchemaTriplesFromTable(final Schema schema, final String encodedSchemaTriplesTable) throws SchemaException, SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTriplesTable));

		String subject = null;
		String property = null;
		String object = null;
		final ResultSet resultSet = dmlDao.getSelectionResultSet(ColumnArray.TRIPLES, false, encodedSchemaTriplesTable, null);
		while (resultSet.next()) {
			subject = dictDao.getValue(resultSet.getInt(1));
			property = dictDao.getValue(resultSet.getInt(2));
			object = dictDao.getValue(resultSet.getInt(3));

			if (property.equals(Rdfs.FULL_SUBCLASS) || property.equals(Rdfs.SUBCLASS))
				schema.addSubClassOfTriple(subject, object);
			else if (property.equals(Rdfs.FULL_SUBPROPERTY) || property.equals(Rdfs.SUBPROPERTY))
				schema.addSubPropertyOfTriple(subject, object);
			else if (property.equals(Rdfs.FULL_DOMAIN) || property.equals(Rdfs.DOMAIN))
				schema.addDomainTriple(subject, object);
			else if (property.equals(Rdfs.FULL_RANGE) || property.equals(Rdfs.RANGE))
				schema.addRangeTriple(subject, object);
			else 
				continue;
		}
		resultSet.close();
	}

	/**
	 * Encodes data and type triples which are assumed to be in a single triples table.
	 * Sets the encoded triples name in the storage layout.
	 * 
	 * @param encodedTriplesTableName
	 * @param dictionaryTable
	 * @param storageLayout
	 * @param connParams
	 * @return The encoding time.
	 * @throws SQLException
	 * @throws UnsupportedDatabaseEngineException
	 */
	private String encodeDataAndTypes(final String encodedTriplesTableName, final String dictionaryTable, final PostgresTableNames storageLayout,
			final ConnectionParams connParams) throws SQLException, UnsupportedDatabaseEngineException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTableName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getTriplesTable()));

		String dataAndTypesEncodingTime = Constant.DEFAULT_TIME;
		if (dmlDao.existsTable(dictionaryTable)) 
			log.info("Data already encoded.");
		else 
			dataAndTypesEncodingTime = encoding.encodeDataAndTypes(connParams, storageLayout.getTriplesTable(), encodedTriplesTableName, dictionaryTable) + Constant.MILLISECONDS;

		storageLayout.setEncodedTriplesTable(encodedTriplesTableName);

		return dataAndTypesEncodingTime;
	}
}
