package fr.inria.oak.RDFSummary.data.berkeleydb;

import java.util.Set;

import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BerkeleyDbHandler {
	
	/**
	 * Opens, and optionally creates, a Database.
	 * 
	 * @param dbName
	 * @param dbConfig
	 * @return Database handle.
	 */
	public Database openDatabase(final String dbName, final DatabaseConfig dbConfig);	
	
	/**
	 * Opens, and optionally creates, a Database.
	 * 
	 * @param transation
	 * @param dbName
	 * @param dbConfig
	 * @return Database handle.
	 */
	public Database openDatabase(final Transaction transation, final String dbName, final DatabaseConfig dbConfig);
	
	/**
	 * Discards the database handle.
	 * @param dbName
	 */
	public void closeDatabase(final String dbName);	
	
	/**
	 * @return The set of database names belonging to currently open databases
	 */
	public Set<String> getActiveDatabases();
	
	/**
	 * @param dbName
	 * @return The {@link Database} instance having the specified name
	 */
	public Database getDatabaseByName(final String dbName);

	/**
	 * Retrieves the key/data pair with the given key. If the matching key has duplicate values, the first data item in the set of duplicates is returned. Retrieval of duplicates requires the use of Cursor operations.
	 * 
	 * @param dbName
	 * @param transaction
	 * @param key - the key used as input. It must be initialized with a non-null byte array by the caller.
	 * @param data - the data returned as output. Its byte array does not need to be initialized by the caller. A partial data item may be specified to optimize for key only or partial data retrieval.
	 * @param lockMode
	 * @return OperationStatus.NOTFOUND if no matching key/data pair is found; otherwise, OperationStatus.SUCCESS.
	 */
	public OperationStatus get(final String dbName, final Transaction transaction, final DatabaseEntry key, final DatabaseEntry data, final LockMode lockMode);
	
	/**
	 * Retrieves the key/data pair with the given key. If the matching key has duplicate values, the first data item in the set of duplicates is returned. Retrieval of duplicates requires the use of Cursor operations.
	 * 
	 * @param dbName
	 * @param key - the key used as input. It must be initialized with a non-null byte array by the caller.
	 * @param data - the data returned as output. Its byte array does not need to be initialized by the caller. A partial data item may be specified to optimize for key only or partial data retrieval.
	 *  
	 * @return OperationStatus.NOTFOUND if no matching key/data pair is found; otherwise, OperationStatus.SUCCESS.
	 */
	public OperationStatus get(final String dbName, final DatabaseEntry key, final DatabaseEntry data);
	
	/**
	 * @param dbName
	 * @return A database cursor.
	 */
	public Cursor openCursor(final String dbName);
	
	/**
	 * Stores the (key,value) pairs in the specified database
	 * 
	 * @param dbName
	 * @param key
	 * @param value
	 */
	public void put(final String dbName, final DatabaseEntry key, final DatabaseEntry value);
	
	/**
	 * Stores the (key,value) pairs in the specified database
	 * 
	 * @param dbName
	 * @param transaction 
	 * @param key
	 * @param value
	 */
	public void put(final String dbName, final Transaction transaction, final DatabaseEntry key, final DatabaseEntry value);
	
	/**
	 * Stores the (key,value) pairs in the specified database.
	 * Supported key/value types are: String, integer, boolean, long and double.
	 * 
	 * @param dbName
	 * @param key
	 * @param value
	 * @throws BerkeleyDbException 
	 */
	public <KeyType, ValueType> void put(String dbName, KeyType key, ValueType value) throws BerkeleyDbException;

	/**
	 * @param dbName
	 * @param key
	 * @param valueType
	 * @return The value associated with the key in the specified database, or null if not found.
	 * @throws BerkeleyDbException
	 */
	public <KeyType, ValueType> ValueType get(String dbName, KeyType key, Class<ValueType> valueType) throws BerkeleyDbException; 
}
