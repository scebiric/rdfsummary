package fr.inria.oak.RDFSummary.data.berkeleydb;

import java.io.File;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.sleepycat.bind.tuple.BooleanBinding;
import com.sleepycat.bind.tuple.DoubleBinding;
import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.bind.tuple.LongBinding;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BerkeleyDbHandlerImpl implements BerkeleyDbHandler {

	private final Map<String, Database> databases;
	private final Environment environment;

	/**
	 * Constructor of {@link BerkeleyDbHandlerImpl}
	 * 
	 * @param envHomeDirectory
	 * @param envConfig
	 * @throws BerkeleyDbException 
	 */
	public BerkeleyDbHandlerImpl(final String envHomeDirectory, final EnvironmentConfig envConfig) throws BerkeleyDbException {
		File envHomeDirFile = new File(envHomeDirectory);
		if (!envHomeDirFile.exists()) {
			boolean created = envHomeDirFile.mkdirs();
			if (!created)
				throw new BerkeleyDbException("The directory " + envHomeDirectory + " does not exist and could not be created.");
		}

		environment = new Environment(envHomeDirFile, envConfig);
		databases = Maps.newHashMap();
	}

	@Override
	public Database openDatabase(final Transaction transation, final String dbName, final DatabaseConfig dbConfig) {
		Database db = environment.openDatabase(transation, dbName, dbConfig);
		databases.put(dbName, db);

		return db;
	}

	@Override
	public Database openDatabase(final String dbName, final DatabaseConfig dbConfig) {
		return openDatabase(null, dbName, dbConfig);
	}

	@Override
	public void closeDatabase(String dbName) {
		databases.get(dbName).close();
		databases.remove(dbName);
	}

	@Override
	public Set<String> getActiveDatabases() {
		return databases.keySet();
	}

	@Override
	public Database getDatabaseByName(final String dbName) {
		return databases.get(dbName);
	}

	@Override
	public void put(final String dbName, final DatabaseEntry key, final DatabaseEntry value) {
		put(dbName, null, key, value);
	}

	@Override
	public void put(final String dbName, final Transaction transaction, final DatabaseEntry key, final DatabaseEntry value) {
		databases.get(dbName).put(transaction, key, value);
	}

	@Override
	public <KeyType, ValueType> void put(String dbName, KeyType key, ValueType value) throws BerkeleyDbException {	
		final DatabaseEntry keyEntry = bindToEntry(key);
		final DatabaseEntry valueEntry = bindToEntry(value);

		put(dbName, null, keyEntry, valueEntry);
	}

	private <ParamType> DatabaseEntry bindToEntry(final ParamType param) throws BerkeleyDbException {
		Preconditions.checkNotNull(param);

		final DatabaseEntry entry = new DatabaseEntry();
		if (param instanceof String) {
			StringBinding.stringToEntry(param.toString(), entry);
			return entry;
		}

		if (param instanceof Integer) {
			IntegerBinding.intToEntry((Integer) param, entry);
			return entry;
		}

		if (param instanceof Boolean) {
			BooleanBinding.booleanToEntry((Boolean) param, entry);
			return entry;
		}

		if (param instanceof Long) {
			LongBinding.longToEntry((Long) param, entry);
			return entry;
		}

		if (param instanceof Double) {
			DoubleBinding.doubleToEntry((Double) param, entry);
			return entry;
		}

		throw new BerkeleyDbException("Unsupported type.");
	}
	
	private <ConstantType> ConstantType bindToConstant(final DatabaseEntry entry, final Class<ConstantType> constantType) throws BerkeleyDbException {
		Preconditions.checkNotNull(entry);
		Preconditions.checkNotNull(constantType);

		if (constantType == String.class) 
			return constantType.cast(StringBinding.entryToString(entry));
		if (constantType == Integer.class) 
			return constantType.cast(IntegerBinding.entryToInt(entry));
		if (constantType == Boolean.class)
			return constantType.cast(BooleanBinding.entryToBoolean(entry));
		if (constantType == Long.class)
			return constantType.cast(LongBinding.entryToLong(entry));
		if (constantType == Double.class) 
			return constantType.cast(DoubleBinding.entryToDouble(entry));

		throw new BerkeleyDbException("Unsupported type.");
	}

	@Override
	public <KeyType, ValueType> ValueType get(final String dbName, final KeyType key, final Class<ValueType> valueType) throws BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dbName));
		Preconditions.checkNotNull(key);

		final DatabaseEntry keyEntry = bindToEntry(key);
		final DatabaseEntry dataEntry = new DatabaseEntry();
		
		OperationStatus status = get(dbName, keyEntry, dataEntry);
		if (status == OperationStatus.SUCCESS) {
			return bindToConstant(dataEntry, valueType);
		}		

		return null;
	}

	@Override
	public OperationStatus get(final String dbName, final Transaction transaction, final DatabaseEntry key, final DatabaseEntry data, final LockMode lockMode) {
		Database db = databases.get(dbName);
		OperationStatus status = db.get(transaction, key, data, lockMode);
		
		// return databases.get(dbName).get(transaction, key, data, lockMode);
		return status;
	}

	@Override
	public OperationStatus get(final String dbName, final DatabaseEntry key, final DatabaseEntry data) {
		return get(dbName, null, key, data, null);
	}	

	@Override
	public Cursor openCursor(final String dbName) {		
		return databases.get(dbName).openCursor(null, null);
	}
}
