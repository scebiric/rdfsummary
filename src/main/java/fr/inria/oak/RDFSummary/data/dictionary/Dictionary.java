package fr.inria.oak.RDFSummary.data.dictionary;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Dictionary {

	/**
	 * Inserts the value to the dictionary.
	 * 
	 * @param value  
	 * @return The key assigned to the value
	 * @throws SQLException 
	 */
	public int insert(String value) throws SQLException;
	
	/**
	 * @param value
	 * @return The key for the specified value
	 * @throws SQLException 
	 */
	public Integer getKey(String value) throws SQLException;

	/**
	 * @return The next value to be used for the key (not yet in the database)
	 * @throws SQLException 
	 */
	public int getSeed() throws SQLException;
	
	/**
	 * @param key
	 * @return The value with the specified key
	 * @throws SQLException 
	 */
	public String getValue(int key) throws SQLException;

	/**
	 * @return The number of entries in the dictionary
	 * @throws SQLException 
	 */
	public int size() throws SQLException;
}
