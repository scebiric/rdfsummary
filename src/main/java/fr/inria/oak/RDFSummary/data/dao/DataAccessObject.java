package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DataAccessObject {
	
	public PreparedStatement prepare(String query) throws SQLException;
	
	/**
	 * Closes all resources
	 * @throws SQLException 
	 */
	public void releaseResources() throws SQLException;
}
