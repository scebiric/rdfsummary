package fr.inria.oak.RDFSummary.data.saturation;

import java.util.Objects;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Triple {

	private final int subject;
	private final int property;
	private final int object;
	
	private Triple triple = null;
	
	/**
	 * Constructs a triple with the specified subject, property and object.
	 * 
	 * @param subject
	 * @param property
	 * @param object
	 */
	public Triple(final int subject, final int property, final int object) {		
		this.subject = subject;
		this.property = property;
		this.object = object;
	}
	
	/**
	 * @return The triple subject
	 */
	public int getSubject() {
		return this.subject;
	}
	
	/**
	 * @return The triple property
	 */
	public int getProperty() {
		return this.property;
	}
	
	/**
	 * @return The triple object
	 */
	public int getObject() {
		return this.object;
	}

	@Override
	public int hashCode() {
		return Objects.hash(subject, property, object);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof Triple))
			return false;
		
		triple = (Triple) o;
		
		return subject == triple.subject && property == triple.property && object == triple.object;
	}
}
