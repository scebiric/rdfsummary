package fr.inria.oak.RDFSummary.messages;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MessageBuilder {

	private StringBuilder builder;
	
	public MessageBuilder() {
		builder = new StringBuilder();
	}

	public MessageBuilder append(final String s) {
		builder.append(s);
		
		return this;
	}
	
	@Override
	public String toString() {
		final String str = builder.toString();
		builder = new StringBuilder();
		
		return str;
	}
}
