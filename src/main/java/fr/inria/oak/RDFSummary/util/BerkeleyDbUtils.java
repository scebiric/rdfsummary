package fr.inria.oak.RDFSummary.util;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.sleepycat.bind.tuple.IntegerBinding;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.OperationStatus;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BerkeleyDbUtils {

	private static final Logger log = Logger.getLogger(BerkeleyDbUtils.class);
	private static BerkeleyDbHandler BerkeleyDbHandler;
	private static DatabaseConfig DbConfig;
	private static DmlDao DmlDao;
	
	private static ResultSet resultSet = null;
	private static String[] nodeTablesColumns = new String[] { Constant.URI };
	private static String[] propertyColumn = new String[] { Constant.PROPERTY };
	
	/**
	 * @param bdbHandler
	 * @param dbConfig
	 * @param dmlDao
	 */
	public BerkeleyDbUtils(final BerkeleyDbHandler bdbHandler, final DatabaseConfig dbConfig, final DmlDao dmlDao) {
		BerkeleyDbHandler = bdbHandler;
		DbConfig = dbConfig;
		DmlDao = dmlDao;
	}
	
	/**
	 * Assumes the database contains multimap key-value pairs and returns all the values as a set.
	 * 
	 * @param dbName
	 * @param key
	 * @return The set of values in the specified BerkeleyDb database.
	 */
	public static TIntSet getValuesByKey(final String dbName, final int key) {
		final TIntSet values = new TIntHashSet();		
		final Cursor cursor = BerkeleyDbHandler.openCursor(dbName);
		final DatabaseEntry keyEntry = new DatabaseEntry();
		IntegerBinding.intToEntry(key, keyEntry);
		DatabaseEntry dataEntry = new DatabaseEntry();
		
		OperationStatus status = cursor.getSearchKey(keyEntry, dataEntry, null);
		while (status == OperationStatus.SUCCESS) {
			values.add(IntegerBinding.entryToInt(dataEntry));
			dataEntry = new DatabaseEntry();
			status = cursor.getNextDup(keyEntry, dataEntry, null);
		}		
		cursor.close();
		
		return values;
	}

	
	/**
	 * Opens a new BerkeleyDb database with the specified configuration.
	 * Throws an exception if the database with the specified name has already been opened.
	 * 
	 * @param dbName
	 * @param dbConfig
	 * @throws BerkeleyDbException
	 */
	public static void openBerkeleyDbDatabase(final String dbName, final DatabaseConfig dbConfig) throws BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dbName));
		Preconditions.checkNotNull(dbConfig);
		
		if (BerkeleyDbHandler.getActiveDatabases().contains(dbName)) 
			throw new BerkeleyDbException("The database " + dbName + " is already opened.");

		log.info("Opening BerkeleyDb database: " + dbName + Constant.THREE_DOTS);
		BerkeleyDbHandler.openDatabase(dbName, dbConfig);
	}

	/**
	 * Closes the BerkeleyDb database.
	 * @param dbName
	 */
	public static void closeDatabase(final String dbName) {
		log.info("Closing BerkeleyDb database: " + dbName + Constant.THREE_DOTS);
		BerkeleyDbHandler.closeDatabase(dbName);
	}
	
	/**
	 * Selects distinct class and property nodes from the respective tables 
	 * and stores them into class and property Berkeley DB instances.
	 * 
	 * @param classesDb
	 * @param propertiesDb
	 * @param classesTable
	 * @param propertiesTable
	 * @throws BerkeleyDbException
	 * @throws SQLException
	 */
	public static void storeClassAndPropertyNodes(final String classesDb, final String propertiesDb, final String classesTable, final String propertiesTable) throws BerkeleyDbException, SQLException {
		log.info("Storing class and property nodes to Berkeley DB...");
		
		store(classesDb, classesTable);
		store(propertiesDb, propertiesTable);
	}

	private static void store(String dbName, String nodesTable) throws BerkeleyDbException, SQLException {
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		BerkeleyDbUtils.openBerkeleyDbDatabase(dbName, DbConfig);
		resultSet = DmlDao.getSelectionResultSet(nodeTablesColumns, true, nodesTable, null);
		while (resultSet.next())
			BerkeleyDbHandler.put(dbName, resultSet.getInt(1), true);
		
		resultSet.close();
		timer.stop();
		log.info("Copied nodes from " + nodesTable + " to BerkeleyDb in: " + timer.getTimeAsString()); 
	}

	public static void storeDataProperties(final String propertiesDb, final String encodedDataTable) throws SQLException, BerkeleyDbException {
		log.info("Storing data properties to Berkeley DB...");
		final Timer timer = new TimerImpl();
		timer.reset();
		timer.start();
		resultSet = DmlDao.getSelectionResultSet(propertyColumn, true, encodedDataTable, null);
		while (resultSet.next())
			BerkeleyDbHandler.put(propertiesDb, resultSet.getInt(1), true);
		
		resultSet.close();
		timer.stop();
		log.info("Stored data properties to BerkeleyDb in: " + timer.getTimeAsString());
	}
}
