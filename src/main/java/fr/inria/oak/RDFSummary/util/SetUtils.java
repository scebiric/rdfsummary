package fr.inria.oak.RDFSummary.util;

import com.google.common.base.Preconditions;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

public class SetUtils {
	private static TIntIterator iterator = null;
	
	/**
	 * This function computes the intersection of two sets.
	 * If the intersection is empty, an empty set is returned.
	 * 
	 * @param a
	 * @param b
	 * @return The set of elements contained in both sets.
	 */
	public static TIntHashSet intersect(final TIntSet a, final TIntSet b) {
		Preconditions.checkNotNull(a);
		Preconditions.checkNotNull(b);
		
		TIntHashSet intersection = new TIntHashSet();
		for (TIntIterator iterator = a.iterator(); iterator.hasNext();) {
			int x = iterator.next();
			if (b.contains(x)) {
				intersection.add(x);
			}
		}
		
		return intersection;
	}

	/**
	 * @param a
	 * @param b
	 * @return The first encountered element contained in both sets, or -1 if they have no common elements.
	 */
	public static int findFirstCommonElement(final TIntSet a, final TIntSet b) {
		if (a == null || b == null || a.size() == 0 || b.size() == 0)
			return -1;
			
		iterator = a.iterator();
		int value = -1;
		while (iterator.hasNext()) {
			value = iterator.next();
			if (b.contains(value))
				return value;
		}
		
		return -1;
	}
	
	/**
	 * @param set1
	 * @param set2
	 * @return True if the two sets are equal, otherwise false
	 */
	public static boolean equals(final TIntSet set1, final TIntSet set2) {
		if (set1 == null && set2 == null)
			return true;
		
		if (set1 == null && set2 != null || set2 == null && set1 != null)
			return false;
		
		return set1.equals(set2);
	}

}
