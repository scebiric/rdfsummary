package fr.inria.oak.RDFSummary.util;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class NameUtils {

	private static StringBuilder Builder;
	private static final String INDEX = "idx";
	private static final String RUN = "run";
	
	private static final String SUBJECT_SET_PREFIX = "s_";
	private static final String OBJECT_SET_PREFIX = "o_";
	private static final String SUBJECT_INFERRED_SET_PREFIX = "s_inf_";
	private static final String OBJECT_INFERRED_SET_PREFIX = "o_inf_";
	private static final String SATURATED = "sat";
	
	/**
	 * Adds the schema prefix to a table name
	 * 
	 * @param schemaName
	 * @param tableName
	 * @return The qualified table name
	 */
	public static String qualify(String schemaName, String tableName) {
		final StringBuilder StrBuilder = new StringBuilder(schemaName);
		StrBuilder.append(Chars.DOT).append(tableName);
		
		return StrBuilder.toString();
	}

	/**
	 * @param qualifiedTableName
	 * @param columns
	 * @return The generated index name
	 */
	public static String getIndexName(final String qualifiedTableName, final String columns) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(columns));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT)));
		
		final String schema = NameUtils.getSchemaName(qualifiedTableName);
		final int index = qualifiedTableName.indexOf(Chars.DOT);
		final String table = qualifiedTableName.substring(index+1);		

		Builder = new StringBuilder();
		Builder.append(schema).append(Chars.UNDERSCORE).append(table).append(Chars.UNDERSCORE);
		Builder.append(INDEX).append(Chars.UNDERSCORE).append(columns);
		
		return Builder.toString();
	}

	/**
	 * Retrieves the table name from the specified qualified name by removing the schema prefix. 
	 * 
	 * @param qualifiedTableName
	 * @return The table name without the schema prefix
	 */
	public static String getTableName(final String qualifiedTableName) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT)));
		Preconditions.checkArgument(!qualifiedTableName.contains(Character.toString(Chars.SPACE)));
		Preconditions.checkArgument(qualifiedTableName.length() >= 3);
		
		int index = qualifiedTableName.indexOf(Chars.DOT);
		return qualifiedTableName.substring(index+1);		
	}

	/**
	 * Retrieves the Postgres schema name from the qualified table name
	 * 
	 * @param qualifiedTableName
	 * @return The schema prefix
	 */
	public static String getSchemaName(final String qualifiedTableName) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT)));
		Preconditions.checkArgument(!qualifiedTableName.contains(Character.toString(Chars.SPACE)));
		Preconditions.checkArgument(qualifiedTableName.length() >= 3);
		
		int index = qualifiedTableName.indexOf(Chars.DOT);		
		return qualifiedTableName.substring(0, index);
	}

	/**
	 * Prefixes the storage layout filename with the dataset name
	 * 
	 * @param datasetName
	 * @return The output filename for the storage layout
	 */
	public static String getStorageLayoutFilename(String datasetName) {
		return qualify(datasetName, "storage-layout");
	}

	/**
	 * Prefixes the index layout filename with the dataset name
	 * 
	 * @param datasetName
	 * @return The output filename for the index layout
	 */
	public static String getIndexLayoutFilename(String datasetName) {
		return qualify(datasetName, "index-layout");
	}

	/**
	 * Generates a table name by adding the saturated suffix to the input table name,
	 * and qualifying the resulting string with the schema name.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The table name for saturated encoded triples
	 */
	public static String getSaturatedTableName(String schemaName, String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.SATURATED);
		
		return qualify(schemaName, Builder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the schema name
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated triples table name
	 */
	public static String getTriplesTableName(String schemaName, String inputTableName) {
		return qualify(schemaName, inputTableName);
	}

	/**
	 * Generates a table name by adding the schema suffix.
	 * 
	 * @param inputTableName
	 * @return The generated schema table name
	 */
	public static String getSchemaTableName(String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.SCHEMA);
		
		return Builder.toString();
	}

	/**
	 * Generates a table name by adding the encoded schema suffix.
	 * 
	 * @param inputTableName
	 * @return The generated encoded schema table name
	 */
	public static String getEncodedSchemaTableName(String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.ENCODED_SCHEMA);
		
		return Builder.toString();
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded triples suffix.
	 * 
	 * @param schemaName
	 * @param tableName
	 * @return The generated encoded triples table name
	 */
	public static String getEncodedTriplesTableName(String schemaName, String tableName) {
		Builder = new StringBuilder(tableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.ENCODED);
		
		return qualify(schemaName, Builder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded types suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated encoded types table name
	 */
	public static String getEncodedTypesTableName(String schemaName, String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.ENCODED_TYPES);
		
		return qualify(schemaName, Builder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded data suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated encoded data table name
	 */
	public static String getEncodedDataTableName(String schemaName, String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.ENCODED_DATA);
		
		return qualify(schemaName, Builder.toString());
	}

	public static String getSaturatedEncodedDataTableName(String saturatedEncodedTriplesTable) {
		Builder = new StringBuilder(saturatedEncodedTriplesTable);
		Builder.append(Chars.UNDERSCORE).append(Constant.DATA);
		
		return Builder.toString();
	}

	public static String getSaturatedEncodedTypesTableName(String saturatedEncodedTriplesTable) {
		Builder = new StringBuilder(saturatedEncodedTriplesTable);
		Builder.append(Chars.UNDERSCORE).append(Constant.TYPES);
		
		return Builder.toString();
	}
	
	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the data suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated data table name
	 */
	public static String getDataTableName(String schemaName, String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE);
		Builder.append(Constant.DATA);
		
		return qualify(schemaName, Builder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the types suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated types table name
	 */
	public static String getTypesTableName(String schemaName, String inputTableName) {
		Builder = new StringBuilder(inputTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.TYPES);
		
		return qualify(schemaName, Builder.toString());
	}
	
	public static String getClassNodesTableName(String schemaName) {
		return NameUtils.qualify(schemaName, "class_nodes");
	}
	
	public static String getPropertyNodesTableName(String schemaName) {
		return NameUtils.qualify(schemaName, "property_nodes");
	}
	
	public static String getSchemaPropertyNodesTableName(String schemaName) {
		return NameUtils.qualify(schemaName, "property_nodes_schema");
	}

	public static String getStatsFilename(String summaryName) {
		Builder = new StringBuilder(summaryName);
		Builder.append(Chars.UNDERSCORE).append("stats");
		
		return Builder.toString();
	}

	public static String getSaturatedEncodedTriplesTableName(final String encodedTriplesTableName) {
		Builder = new StringBuilder(encodedTriplesTableName);
		Builder.append(Chars.UNDERSCORE).append(Constant.SATURATED);
		
		return Builder.toString();
	}

	public static String getPostgresSummaryName(final String encodedTriplesTable, final String summaryType) {
		Builder = new StringBuilder(encodedTriplesTable);
		Builder.append(Chars.UNDERSCORE).append(summaryType);
		String str = Builder.toString();
		
		return str.replace(Chars.DOT, Chars.UNDERSCORE);
	}
	
	public static String getSummaryName(String datasetName, String summaryType, boolean saturateInput) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(datasetName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType)); 
		
		Builder = new StringBuilder();		
		Builder.append(summaryType).append(Chars.LEFT_PAREN);
		if (saturateInput)
			Builder.append(SATURATED).append(Chars.LEFT_PAREN).append(datasetName).append(Chars.RIGHT_PAREN);
		else
			Builder.append(datasetName);
		
		Builder.append(Chars.RIGHT_PAREN);
		
		return Builder.toString();
	}
	
	public static String getClassesDbName(String dataTable) {
		Builder = new StringBuilder(getSchemaName(dataTable));
		Builder.append(Chars.UNDERSCORE).append(Constant.CLASSES_DB);
		
		return Builder.toString();
	}
	
	public static String getPropertiesDbName(String dataTable) {
		Builder = new StringBuilder(getSchemaName(dataTable));
		Builder.append(Chars.UNDERSCORE).append(Constant.PROPERTIES_DB);
		
		return Builder.toString();
	}
	
	public static String underscoreConcat(String outputPrefix, String suffix) {
		Builder = new StringBuilder(outputPrefix);
		Builder.append(Chars.UNDERSCORE).append(suffix);
		
		return Builder.toString();
	}

	public static String addDotNodePrefix(final int id) {
		Builder = new StringBuilder(Constant.DOT_NODE_PREFIX);
		Builder.append(id);
		return Builder.toString();
	}

	public static String getEncodedSummaryDataTableName(String qualifiedSummaryName) { 
		return NameUtils.underscoreConcat(qualifiedSummaryName, Constant.DATA);
	}

	public static String getEncodedSummaryTypesTableName(String qualifiedSummaryName) {
		return NameUtils.underscoreConcat(qualifiedSummaryName, Constant.TYPES);
	}

	public static String getRepresentationTableName(String qualifiedSummaryName) {
		return NameUtils.underscoreConcat(qualifiedSummaryName, Constant.REPRESENTATION_TABLE);
	}

	public static String getQualifiedEncodedSummaryTableName(String encodedTriplesTable, String summaryType) {
		return underscoreConcat(encodedTriplesTable, summaryType);
	}

	public static String getExperimentFilename(String datasetName, String summaryType, String experimentType, String experimentTimestamp) {
		Builder = new StringBuilder();
		Builder.append(datasetName).append(Chars.UNDERSCORE);
		Builder.append(summaryType).append(Chars.UNDERSCORE);
		Builder.append(experimentTimestamp).append(Chars.UNDERSCORE);
		Builder.append(experimentType);
		
		return Builder.toString();
	}

	public static String getExperimentRunFilename(String datasetName, String summaryType, String experimentType, String experimentTimestamp, int run) {
		Preconditions.checkArgument(run > 0);
		
		Builder = new StringBuilder();
		Builder.append(datasetName).append(Chars.UNDERSCORE);
		Builder.append(summaryType).append(Chars.UNDERSCORE);
		Builder.append(experimentTimestamp).append(Chars.UNDERSCORE);
		Builder.append(experimentType).append(Chars.UNDERSCORE); 
		Builder.append(RUN).append(run);
		
		return Builder.toString();
	}

	public static String getAuxiliarySaturatedTableName(final String encodedTriplesTable) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTable));
		
		Builder = new StringBuilder();
		Builder.append(encodedTriplesTable).append(Chars.UNDERSCORE).append(Constant.AUX_SATURATED);
				
		return Builder.toString();
	}
	
	public static String getDbName(final String datasetName, final String suffix) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(datasetName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(suffix));
		
		Builder = new StringBuilder();
		Builder.append(datasetName).append(Chars.UNDERSCORE).append(suffix);
				
		return Builder.toString();
	}

	public static String getSubjectSetName(final int subject) {
		Builder = new StringBuilder();
		Builder.append(SUBJECT_SET_PREFIX).append(subject);
		
		return Builder.toString();
	}

	public static String getObjectSetName(final int object) { 
		Builder = new StringBuilder();
		Builder.append(OBJECT_SET_PREFIX).append(object);
		
		return Builder.toString();
	}

	public static String getSubjectInferredSetName(int subject) {
		Builder = new StringBuilder();
		Builder.append(SUBJECT_INFERRED_SET_PREFIX).append(subject);
		
		return Builder.toString();
	}
	
	public static String getObjectInferredSetName(int object) {
		Builder = new StringBuilder();
		Builder.append(OBJECT_INFERRED_SET_PREFIX).append(object);
		
		return Builder.toString();
	}

	public static String getDictionaryTableName(String psqlSchemaName) {
		 return qualify(psqlSchemaName, Constant.DICTIONARY_TABLE_NAME);
	}

	public static String getDataTableName(String schemaName, String triplesTableName, boolean saturatedInput) {
		Builder = new StringBuilder();
		Builder.append(triplesTableName);
		if (saturatedInput)
			Builder.append(Chars.UNDERSCORE).append(SATURATED);
		
		Builder.append(Chars.UNDERSCORE).append(Constant.DATA); 
		
		return qualify(schemaName, Builder.toString());
	}

	public static String getTypesTableName(String schemaName, String triplesTableName, boolean saturatedInput) {
		Builder = new StringBuilder();
		Builder.append(triplesTableName);
		if (saturatedInput)
			Builder.append(Chars.UNDERSCORE).append(SATURATED);
		
		Builder.append(Chars.UNDERSCORE).append(Constant.TYPES); 
		
		return qualify(schemaName, Builder.toString());
	}

	public static String getSchemaNameFromSummaryName(final String summaryName) {
		String newStr = summaryName.replace(Chars.LEFT_PAREN, Chars.UNDERSCORE); 
		
		return newStr.substring(0, newStr.length() - 1);
	}
}
