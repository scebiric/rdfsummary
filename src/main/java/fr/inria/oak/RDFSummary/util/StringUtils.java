package fr.inria.oak.RDFSummary.util;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StringUtils {	
	
	private static final String ESCAPED_QUOTATION_MARK = "\"";
	private static final String LEFT_ANGLE_BRACKET = Character.toString(Chars.LEFT_ANGLE_BRACKET);
	private static final String RIGHT_ANGLE_BRACKET = Character.toString(Chars.RIGHT_ANGLE_BRACKET);
	
	private static StringBuilder Builder;
	
	public static boolean isNullOrBlank(String param) { 
	    return param == null || param.trim().length() == 0;
	}
	
	public static boolean isNull(String param) { 
	    return param == null;
	}
	
	public static boolean isBlank(String param) { 
		if (isNull(param))
			return false;
		
	    return param.trim().length() == 0;
	}

	public static List<String> splitCommaSeparatedListAsStrings(String str) {
		return Arrays.asList(str.split("\\s*\\,\\s*"));
	}
	
	public static List<String> splitPipeSeparatedListAsStrings(String str) {
		return Arrays.asList(str.split("\\s*\\|\\s*"));
	}
	
	public static String trim(String str) {
		if (!isNullOrBlank(str))
			str = str.trim();
		
		return str;
	}
	
	public static String toLower(String str) {
		if (!isNullOrBlank(str))
			str = str.toLowerCase();
		
		return str;
	}	
	
	/**
	 * @param str
	 * @return The specified string enclosed in double quotes
	 */
	public static String addDoubleQuotes(String str) {
		return ESCAPED_QUOTATION_MARK.concat(str).concat(ESCAPED_QUOTATION_MARK);
	}

	/**
	 * @param str
	 * @return The specified string with the removed starting and/or ending bracket.
	 */
	public static String removeBrackets(String str) {
		if (StringUtils.isNullOrBlank(str))
			return str;
		
		String formatted = str;
		if (formatted.startsWith(LEFT_ANGLE_BRACKET))
			formatted = formatted.substring(1);
		if (formatted.endsWith(RIGHT_ANGLE_BRACKET))
			formatted = formatted.substring(0, formatted.length() - 1);
		
		return formatted;
	}
	
	public static String addBrackets(String str) {
		if (isNullOrBlank(str))
			return str;
		
		String formatted = str;
		if (!str.startsWith(LEFT_ANGLE_BRACKET))
			formatted = LEFT_ANGLE_BRACKET.concat(str);
		if (!str.endsWith(RIGHT_ANGLE_BRACKET))
			formatted = formatted.concat(RIGHT_ANGLE_BRACKET);
		
		return formatted;
	}

	/**
	 * It is assumed (required) that s1 ends with s2.
	 * 
	 * @param s1
	 * @param s2
	 * @return The substring of s1 which starts at index 0 and ends at s2.length() - 1, inclusive.
	 */
	public static String removeFromEnd(final String s1, final String s2) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(s1));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(s2));
		Preconditions.checkArgument(s1.endsWith(s2));
		
		return s1.substring(0, s1.length() - s2.length());
	}

	public static String addAngleBrackets(final String str) {
		Builder = new StringBuilder();
		Builder.append(Chars.LEFT_ANGLE_BRACKET).append(str).append(Chars.RIGHT_ANGLE_BRACKET);
		
		return Builder.toString();
	}
}
