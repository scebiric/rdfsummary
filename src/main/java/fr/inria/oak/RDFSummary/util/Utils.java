package fr.inria.oak.RDFSummary.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import com.beust.jcommander.internal.Maps;
import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Equivalence;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.storage.EncodingTimes;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.data.storage.TriplesLoadingTimes;
import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Utils {	
	protected static Logger log = Logger.getLogger(Utils.class);
	private static DictionaryDao DictDao;
	private static DmlDao DmlDao;
	private static DdlDao DdlDao;
	
	private static StringBuilder Builder;
	private static Integer key = null;
	
	private final static SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
	private static Calendar CalendarInstance;
	private static final String SLASH = "/";
	
	/**
	 * @param dictDao
	 * @param dmlDao
	 * @param ddlDao
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	public Utils(final DictionaryDao dictDao, final DmlDao dmlDao, final DdlDao ddlDao) throws DictionaryException, SQLException {
		DictDao = dictDao;
		DmlDao = dmlDao;
		DdlDao = ddlDao;
	}
	
	/**
	 * Concats the parameters
	 * 
	 * @param outputFolder
	 * @param filename
	 * @param extension
	 * @return The full filepath
	 */
	public static String getFilepath(final String outputFolder, final String filename, final String extension) {
		Builder = new StringBuilder();
		Builder.append(outputFolder).append(filename).append(Chars.DOT).append(extension);
		
		return Builder.toString();
	}

	/**
	 * Generates and opens a PDF file from the specified DOT file
	 * 
	 * @param outputFilepath
	 * @param pathToDot
	 */
	public static void dotToPdf(final String outputFilepath, final String pathToDot) {
		log.info("Converting dot to pdf...");
		String str = FilenameUtils.removeExtension(outputFilepath);
		
		// Convert dot to pdf
		Builder = new StringBuilder();
		Builder.append(pathToDot).append(" -Tpdf -o ").append(str).append(Chars.DOT).append(Extension.PDF).append(Chars.SPACE).append(str).append(Chars.DOT).append(Extension.DOT);
		ShellUtils.executeCommand(Builder.toString());
		
		// Open the pdf
		Builder = new StringBuilder();
		Builder.append("open ").append(str).append(Chars.DOT).append(Extension.PDF);
		ShellUtils.executeCommand(Builder.toString());
	}

	/**
	 * Generates and opens a PNG file from the specified DOT file
	 * 
	 * @param outputFilepath
	 * @param pathToDot
	 */
	public static void dotToPng(final String outputFilepath, final String pathToDot) {
		log.info("Converting dot to png...");
		String str = FilenameUtils.removeExtension(outputFilepath);
		
		// Convert dot to png
		Builder = new StringBuilder();
		Builder.append(pathToDot).append(" -Tpng -o ").append(str).append(Chars.DOT).append(Extension.PNG).append(Chars.SPACE).append(str).append(Chars.DOT).append(Extension.DOT);
		ShellUtils.executeCommand(Builder.toString());
		
		// Open the png
		Builder = new StringBuilder();
		Builder.append("open ").append(str).append(Chars.DOT).append(Extension.PNG);
		ShellUtils.executeCommand(Builder.toString());
	}
	
	/**
	 * Prints stack trace and exists the application
	 * 
	 * @param e
	 */
	public static void handleException(final Exception e) {
		Preconditions.checkNotNull(e);
		e.printStackTrace();
		System.exit(0);
	}

	/**
	 * @param collection
	 * @return The set with values of the collection
	 */
	public static <T> Set<T> toSet(Collection<T> collection) {
		Set<T> set = Sets.newHashSet();
		set.addAll(collection);

		return set;
	}

	/**
	 * Calculates the number of tablets based on the specified main table size and the size of one tablet
	 * 
	 * @param tableSize
	 * @param partitionSize
	 * @return The number of tablets
	 */
	public static int getNumberOfPartitions(int tableSize, int partitionSize) {
		Preconditions.checkArgument(tableSize >= 0);
		Preconditions.checkArgument(partitionSize >= 0);

		int partitions = 1;
		if (tableSize > partitionSize) {
			partitions = tableSize / partitionSize;
			if (tableSize % partitionSize > 0)
				partitions++;
		}

		return partitions;
	}

	/**
	 * @return A map containing keys for full and short subclass, domain and range RDFS properties
	 * @throws DictionaryException
	 * @throws SQLException 
	 * @throws InexistentValueException
	 */
	public static Map<String, Integer> getTypeAndRdfsPropertyKeys() throws DictionaryException, SQLException {	
		final Map<String, Integer> propertyKeys = Maps.newHashMap();
		putKeyIfExists(Rdf.FULL_TYPE, propertyKeys);
		putKeyIfExists(Rdf.TYPE, propertyKeys);
		putKeyIfExists(Rdfs.FULL_SUBCLASS, propertyKeys);
		putKeyIfExists(Rdfs.SUBCLASS, propertyKeys);
		putKeyIfExists(Rdfs.FULL_SUBPROPERTY, propertyKeys);
		putKeyIfExists(Rdfs.SUBPROPERTY, propertyKeys);
		putKeyIfExists(Rdfs.FULL_DOMAIN, propertyKeys);
		putKeyIfExists(Rdfs.DOMAIN, propertyKeys);
		putKeyIfExists(Rdfs.FULL_RANGE, propertyKeys);
		putKeyIfExists(Rdfs.RANGE, propertyKeys);
		
		return propertyKeys;
	}

	/**
	 * Retrieves from the dictionary the key for the specified property.
	 * If the key is not null, maps the property to the key in the given map.
	 * 
	 * @param property
	 * @param propertyKeys
	 * @throws SQLException
	 */
	private static void putKeyIfExists(final String property, Map<String, Integer> propertyKeys) throws SQLException {
		key = DictDao.getExistingKey(property);
		if (key != null)
			propertyKeys.put(property, key);
	}

	/**
	 * Prints the sets to console
	 * @param sets
	 */
	public static void printSets(final List<TIntSet> sets) {
		int i = 1;
		for (TIntSet set : sets) {
			System.out.println("Set " + i++);
			for (TIntIterator iterator = set.iterator(); iterator.hasNext();)
				System.out.println(iterator.next());

			System.out.println();	
		}
	}

	public static String getFormattedFilepath(String filepath) {
		if (!StringUtils.isNullOrBlank(filepath)) {
			if (!filepath.endsWith(Character.toString(Chars.SLASH)))
				filepath = filepath.concat(Character.toString(Chars.SLASH));
		}
		
		return filepath;
	}

	public static int getIntegerColumn(final ResultSet resultSet, final int columnIndex) throws SQLException {
		Preconditions.checkNotNull(resultSet);
		Preconditions.checkArgument(columnIndex > 0);
		
		return resultSet.getInt(columnIndex);
	}
	
	public static void writeStatsToFile(final String outputFolder, final String outputName, final PostgresTableNames storageLayout, 
			final Stats stats, final TriplesLoadingTimes triplesLoadingTimes, final EncodingTimes encodedTriplesLoadingTimes) throws IOException {
		log.info("Writing stats to file...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkNotNull(stats);
		Preconditions.checkNotNull(triplesLoadingTimes);
		Preconditions.checkNotNull(encodedTriplesLoadingTimes);

		final String filepath = Utils.getFilepath(outputFolder, outputName, Extension.TXT);
		final PrintWriter writer = new PrintWriter(new FileWriter(filepath));

		final int datasetTotalTriples = stats.getInputGraphTripleStats().getNonSchemaTripleCount() +
				stats.getSummaryTripleStats().getSchemaTripleCount();
		final int summaryTotalTriples = stats.getSummaryTripleStats().getNonSchemaTripleCount() +
				stats.getSummaryTripleStats().getSchemaTripleCount();
		
		writer.printf("Input data triples: %s", stats.getInputGraphTripleStats().getDataTripleCount());
		writer.printf("\nInput type triples: %s", stats.getInputGraphTripleStats().getTypeTripleCount());
		writer.printf("\nInput total triples: %s", datasetTotalTriples);
		writer.println();
		writer.printf("\nSummary data triples: %s", stats.getSummaryTripleStats().getDataTripleCount());
		writer.printf("\nSummary type triples: %s", stats.getSummaryTripleStats().getTypeTripleCount());
		writer.printf("\nSummary total triples: %s", summaryTotalTriples);
		writer.println();
		writer.printf("\nSummary data nodes: %s", stats.getSummaryNodeStats().getDataNodeCount());
		writer.println();
		writer.printf("\nDistinct data properties (input and summary): %s", stats.getDistinctDataPropertiesCount());
		writer.printf("\nDistinct classes (input and summary): %s", stats.getDistinctClassesCount());
		writer.printf("\nSchema triples (input and summary): %s", stats.getSummaryTripleStats().getSchemaTripleCount());
		writer.println();
		writer.printf("\nSummarization time: %s sec", Utils.millisecondsToSeconds(stats.getSummarizationTime()));
		writer.close();
		log.info("Stats written to file: " + filepath);	
	}

	/**
	 * @param indexes
	 * @param tableName
	 * @param schemaName
	 * @throws SQLException 
	 */
	public static void createIndexesIfNotExists(final Map<String, String> indexes, boolean unique, final String tableName, final String schemaName) throws SQLException {
		String indexName = null;
		for (final Entry<String, String> index : indexes.entrySet()) {
			indexName = NameUtils.getIndexName(tableName, index.getKey());
			if (!DmlDao.existsIndex(indexName, schemaName))
				DdlDao.createIndex(indexName, unique, tableName, index.getValue());
		}
	}
	
	/**
	 * Deletes the whole directory if it exists.
	 * 
	 * @param directory
	 * @throws IOException
	 */
	public static void deleteDirectory(final String directory) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(directory));
		
		final File dirFile = new File(directory);
		if (dirFile.exists())
			FileUtils.deleteDirectory(dirFile);
	}
	
	public static String appendMilliseconds(final long time) {
		Builder = new StringBuilder();
		Builder.append(time).append(Constant.MILLISECONDS);
		
		return Builder.toString();
	}

	public static String appendSeconds(final double time) {
		Builder = new StringBuilder();
		Builder.append(time).append(Constant.SECONDS);
		
		return Builder.toString();
	}
	
	public static double millisecondsToSeconds(final long ms) {
		return ms / 1000.0;
	}
	
	public static String getCurrentTime() {
		CalendarInstance = Calendar.getInstance();
		
		return DateFormat.format(CalendarInstance.getTime());
	}
	
	public static String getFilepathToDataset(final String testDatasetsFolder, final String datasetFile) {
		return System.getProperty("user.dir") + testDatasetsFolder + datasetFile;
	}

	public static String concat(final List<String> list) {
		Builder = new StringBuilder();
		for (final String s : list) 
			Builder.append(s);
		
		return Builder.toString();
	}

	public static String getMapDbFolder(String mapDbFolder, String datasetName, String graphType) {
		Builder = new StringBuilder();
		Builder.append(mapDbFolder).append(datasetName).append(SLASH).append(graphType).append(SLASH);
		
		return Builder.toString();		
	}

	public static void mkDirs(String folder) {
		final File file = new File(folder);
		if (!file.exists())
			file.mkdirs();
	}
	
	/**
	 * @param summaryTypesList
	 * @param equivalence 
	 * @return The list of summary types
	 */
	public static List<String> loadSummaryTypes(final String summaryTypesList, String equivalence) {
		final List<String> summaryTypes = StringUtils.splitPipeSeparatedListAsStrings(summaryTypesList);
		if (summaryTypes == null || summaryTypes.size() == 0)
			throw new IllegalArgumentException("Summary types must be specified, in a pipe-separated list.");

		for (final String summaryType : summaryTypes) {
			if (StringUtils.isNullOrBlank(summaryType))
				throw new IllegalArgumentException("Summary type cannot be null or blank.");

			if (equivalence.equals(Equivalence.CLIQUE)) {
				if (!summaryType.equals(SummaryType.WEAK) && !summaryType.equals(SummaryType.TYPED_WEAK) &&
						!summaryType.equals(SummaryType.STRONG) && !summaryType.equals(SummaryType.TYPED_STRONG))
					throw new IllegalArgumentException("Invalid clique summary type: " + summaryType);
			}
			else if (equivalence.equals(Equivalence.BISIMULATION)) {
				if (!summaryType.equals(SummaryType.FORWARD_ONLY) &&
						!summaryType.equals(SummaryType.BACKWARD_ONLY) &&
						!summaryType.equals(SummaryType.FORWARD_BACKWARD))
					throw new IllegalArgumentException("Invalid bisimulation summary type: " + summaryType);
			}
		}

		return summaryTypes;
	}
}
