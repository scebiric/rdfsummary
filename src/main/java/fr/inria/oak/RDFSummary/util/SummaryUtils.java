package fr.inria.oak.RDFSummary.util;

import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummaryUtils {

	public static int getRepresentative(final int gNode, final PostgresBisimulationRdfDataset rdfDataset, final BisimulationRdfSummary summary) { 
		if (rdfDataset.isClassOrProperty(gNode)) 
			return gNode; 
		
		return summary.getSummaryDataNodeByGDataNode(gNode);
	}
}
