//package fr.inria.oak.RDFSummary.util;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.Collection;
//import java.util.List;
//
//import javax.naming.OperationNotSupportedException;
//
//import org.apache.commons.configuration.ConfigurationException;
//import org.apache.log4j.Logger;
//
//import com.beust.jcommander.internal.Lists;
//
//import fr.inria.oak.RDFSummary.config.ParameterLoader;
//import fr.inria.oak.RDFSummary.config.ParametersException;
//import fr.inria.oak.RDFSummary.constants.RdfFormat;
//import fr.inria.oak.RDFSummary.data.dao.SaturatorDao;
//import fr.inria.oak.RDFSummary.data.saturation.SaturationGenerator;
//import fr.inria.oak.RDFSummary.rdf.EncodedTriple;
//import fr.inria.oak.RDFSummary.rdf.Triple;
//import fr.inria.oak.RDFSummary.rdfschema.Schema;
//import fr.inria.oak.RDFSummary.summary.Exporter;
//import fr.inria.oak.RDFSummary.timer.Timer;
//import fr.inria.oak.RDFSummary.writer.Writer;
//import fr.inria.oak.commons.db.DictionaryException;
//import fr.inria.oak.commons.db.InexistentValueException;
//import fr.inria.oak.commons.dictionary.MemoryDictionary;
//
///**
// * 
// * @author Sejla CEBIRIC
// *
// */
//public class SaturationUtils {
//	private static Logger log = Logger.getLogger(SaturationUtils.class);
//	private static SaturationGenerator SaturationGenerator;
//	private static SaturatorDao SaturatorDao;
//	private static Writer Writer;
//	private static Timer Timer;
//
//	/**
//	 * Constructor of {@link SaturationUtils}
//	 * 
//	 * @param saturationGenerator
//	 * @param saturatorDao
//	 * @param exporter
//	 * @param writer
//	 * @param timer
//	 */
//	public SaturationUtils(SaturationGenerator saturationGenerator, SaturatorDao saturatorDao, Exporter exporter, Writer writer, Timer timer) {
//		SaturationGenerator = saturationGenerator;
//		SaturatorDao = saturatorDao;
//		Writer = writer;
//		Timer = timer;
//	}
//
//	/**
//	 * Exports the list of (possibly saturated) encoded triples to RDF and DOT 
//	 * 
//	 * @param encodedTriplesTable
//	 * @param schemaTable
//	 * @param encodedRDFSTable
//	 * @param dictionary
//	 * @param outputFolder
//	 * @param outputName
//	 * @param pathToDotExecutable
//	 * @param dotStyleSettingsFilepath
//	 * @param convertDotToPdf
//	 * @param fetchSize
//	 * @throws IOException 
//	 * @throws SQLException 
//	 * @throws ParametersException 
//	 * @throws ConfigurationException 
//	 * @throws OperationNotSupportedException 
//	 */
//	public static void export(String encodedTriplesTable, String schemaTable, String encodedRDFSTable, MemoryDictionary dictionary, String outputFolder, String outputName, 
//			String pathToDotExecutable, String dotStyleSettingsFilepath, boolean convertDotToPdf, int fetchSize) throws IOException, SQLException, ConfigurationException, ParametersException, OperationNotSupportedException {
//
//		// Export to RDF
//		List<EncodedTriple> encodedDataTriples = SaturatorDao.getEncodedTriples(encodedTriplesTable, dictionary, fetchSize);
//		List<Triple> schemaTriples = SaturatorDao.getTriples(schemaTable, fetchSize);
//		Writer.writeSaturationTriplesToRDF(encodedDataTriples, schemaTriples, outputFolder, outputName, RdfFormat.N_TRIPLES);
//
//		// Generate DOT
//		ParameterLoader paramsLoader = new ParameterLoader();
//		paramsLoader.load(dotStyleSettingsFilepath);
//		SaturatorDao.getEncodedTriples(encodedRDFSTable, dictionary, fetchSize);
//	}
//}
