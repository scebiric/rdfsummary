package fr.inria.oak.RDFSummary.util;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DictionaryUtils {

	private static Logger log = Logger.getLogger(DictionaryUtils.class);
	
	private static Integer key = null;
			
	/**
	 * @param dictionary
	 * @param value
	 * @return The key for the given value, or null if the value is not encoded in the dictionary
	 */
	public static Integer getKey(final Dictionary dictionary, final String value) {
		Integer key = null;
		try {
			key = dictionary.getKey(value);
		} catch (DictionaryException e) {
			log.debug("Value: " + value);
			Utils.handleException(e);
		} catch (InexistentValueException e) {
			key = null;
		}

		return key;
	}

	/**
	 * @param dictionary
	 * @param key
	 * @return The value for the given key, or null if the value is not encoded in the dictionary
	 */
	public static String getValue(final Dictionary dictionary, final Integer key) {
		String value = null;

		try {
			if (key != null)
				value = dictionary.getValue(key.intValue());
		} catch (DictionaryException e) {
			log.debug("Key: " + key);
			Utils.handleException(e);
		} catch (InexistentKeyException e) {
			value = null;
		}

		return value;
	}
	
	public static int getKey(final fr.inria.oak.RDFSummary.data.dictionary.Dictionary dictionary, final String value) throws SQLException {
		key = dictionary.getKey(value);
		if (key == null)
			key = dictionary.insert(value);
		
		return key.intValue();
	}
}
