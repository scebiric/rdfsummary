package fr.inria.oak.RDFSummary.util;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.context.ApplicationContext;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.data.storage.DatasetPreparationResult;
import fr.inria.oak.RDFSummary.data.storage.EncodedPsqlStorage;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummarizerUtils {
	
	public static DatasetPreparationResult loadDatasetToPostgres(final ApplicationContext context, final PostgresSummarizerParams params) throws SQLException, UnsupportedDatabaseEngineException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException {
		EncodedPsqlStorage psqlStorage = context.getBean(EncodedPsqlStorage.class);	
		final ConnectionParams connParams = params.getConnectionParams();
		final String schemaName = params.getSummarizationParams().getSchemaName();
		final boolean dropSchema = params.getSummarizationParams().dropSchema();
		final boolean recomputeSaturation = params.getSummarizationParams().recomputeSaturation();
		final String dictionaryTable = params.getSummarizationParams().getDictionaryTable();
		final String triplesTable = params.getSummarizationParams().getTriplesTable();
		final String dataTriplesFilepath = params.getSummarizationParams().getDataTriplesFilepath();
		final String schemaTriplesFilepath = params.getSummarizationParams().getSchemaTriplesFilepath();
		final boolean indexing = params.getSummarizationParams().indexing();
		final int batchSize = params.getSummarizationParams().getSaturationBatchSize();

		if (params.getSummarizationParams().saturateInput()) {
			return psqlStorage.prepareDataset(connParams, schemaName, dropSchema, recomputeSaturation, 
					dictionaryTable, triplesTable, dataTriplesFilepath, schemaTriplesFilepath, indexing, batchSize);
		}

		return psqlStorage.prepareDataset(connParams, schemaName, dropSchema, dictionaryTable, triplesTable, 
				dataTriplesFilepath, schemaTriplesFilepath, indexing);
	}

}
