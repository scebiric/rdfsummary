package fr.inria.oak.RDFSummary.util;

import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfUtils {

	private static Logger log = Logger.getLogger(RdfUtils.class);
	private static final String MATCH = "Triple matches";
	private static final String NOT_MATCH = "Triple doesn't match.";
	private static final String SHORT_SUBCLASS = "subclass";
	private static final String SHORT_SUBPROPERTY = "subproperty";
	private static final String SHORT_DOMAIN = "domain";
	private static final String SHORT_RANGE = "range";
	private static final String SHORT_TYPE = "rdf:type";
	
	private static StringBuilder Builder;

	public static void logMatchingInfo(boolean matches) {
		if (matches)
			log.info(MATCH);
		else
			log.info(NOT_MATCH);
	}
	
	/**
	 * @param filename
	 * @return The Jena RDF language based on the filename extension
	 */
	public static String getRDFLanguage(String filename) {
		Map<String, String> extensionLanguageMap = Maps.newHashMap();
		extensionLanguageMap = Maps.newHashMap();
		extensionLanguageMap.put("ttl", "Turtle");
		extensionLanguageMap.put("nt", RdfFormat.N_TRIPLES);
		extensionLanguageMap.put("n3", "N3");
		extensionLanguageMap.put("nq", "N-Quads");
		extensionLanguageMap.put("trig", "TriG");
		extensionLanguageMap.put("rdf", "RDF");
		extensionLanguageMap.put("owl", "RDF/XML");
		extensionLanguageMap.put("jsonld", "JSON-LD");
		extensionLanguageMap.put("trdf", "RDF Thrift");
		extensionLanguageMap.put("rt", "RDF Thrift");
		extensionLanguageMap.put("rj", "RDF/JSON");
		extensionLanguageMap.put("trix", "TriX");
		String ext = Files.getFileExtension(filename);

		return extensionLanguageMap.get(ext);
	}

	/**
	 * @param filename
	 * @return The Jena RDF language based on the filename extension
	 */
	public static String getFileExtensionByRDFFormat(String rdfFormat) {
		Map<String, String> languageExtensionMap = Maps.newHashMap();
		languageExtensionMap = Maps.newHashMap();
		languageExtensionMap.put("Turtle", "ttl");
		languageExtensionMap.put(RdfFormat.N_TRIPLES, "nt");
		languageExtensionMap.put("N3", "n3");
		languageExtensionMap.put("N-Quads", "nq");
		languageExtensionMap.put("TriG", "trig");
		languageExtensionMap.put("RDF/XML", "rdf");
		languageExtensionMap.put("RDF/XML", "owl");
		languageExtensionMap.put("JSON-LD", "jsonld");
		languageExtensionMap.put("RDF Thrift", "trdf");
		languageExtensionMap.put("RDF Thrift", "rt");
		languageExtensionMap.put("RDF/JSON", "rj");
		languageExtensionMap.put("TriX", "trix");

		return languageExtensionMap.get(rdfFormat);
	}

	public static String getShortRDFSProperty(final String propertyIRI) {
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_SUBCLASS)) || propertyIRI.equals(Rdfs.SUBCLASS))
			return SHORT_SUBCLASS;
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_SUBPROPERTY)) || propertyIRI.equals(Rdfs.SUBPROPERTY))
			return SHORT_SUBPROPERTY;
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_DOMAIN)) || propertyIRI.equals(Rdfs.DOMAIN))
			return SHORT_DOMAIN;
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_RANGE)) || propertyIRI.equals(Rdfs.RANGE))
			return SHORT_RANGE;

		throw new IllegalArgumentException("Illegal property IRI: " + propertyIRI);
	}

	/**
	 * @param str
	 * @return The given string with the namespace removed
	 */
	public static String stripNamespace(String str) {	
		if (StringUtils.isNullOrBlank(str))
			return str;

		if (str.startsWith(Character.toString(Chars.LEFT_ANGLE_BRACKET)) && str.endsWith(Character.toString(Chars.RIGHT_ANGLE_BRACKET)))
			str = StringUtils.removeBrackets(str);

		if (str.equals(Rdf.FULL_TYPE))
			return SHORT_TYPE;

		int index = str.lastIndexOf(Chars.HASH);
		if (index == -1) {
			index = str.lastIndexOf(Chars.SLASH);
			if (index == -1)
				return str;
		}

		return str.substring(index+1, str.length());
	}

	/**
	 * @param propertyIRI
	 * @return True if the specified property is an RDFS property, otherwise false
	 */
	public static boolean isRDFSProperty(String propertyIRI) {
		return propertyIRI.equals(Rdfs.SUBCLASS) || propertyIRI.equals(Rdfs.FULL_SUBCLASS) ||
				propertyIRI.equals(Rdfs.SUBPROPERTY) || propertyIRI.equals(Rdfs.FULL_SUBPROPERTY) ||
				propertyIRI.equals(Rdfs.DOMAIN) || propertyIRI.equals(Rdfs.FULL_DOMAIN) ||
				propertyIRI.equals(Rdfs.RANGE) || propertyIRI.equals(Rdfs.FULL_RANGE);
	}

	/**
	 * @param propertyIRI
	 * @return True if the specified property is an RDF type property, otherwise false
	 */
	public static boolean isRDFTypeProperty(String propertyIRI) {
		return propertyIRI.equals(Rdf.TYPE) || propertyIRI.equals(Rdf.FULL_TYPE);
	}
	
	public boolean isRdfTypeProperty(final int property, final Integer fullRdfTypeKey, final Integer shortRdfTypeKey) throws DictionaryException, SQLException {
		if (fullRdfTypeKey == null && shortRdfTypeKey == null)
			return false;
		
		if (fullRdfTypeKey != null && fullRdfTypeKey == property)
			return true;
		
		if (shortRdfTypeKey != null && shortRdfTypeKey == property)
			return true;
		
		return false;
	}
	
	public static String getTypePropertyFilter(final String propertyColumn, final Integer fullTypeKey, final Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null); 
		
		Builder = new StringBuilder();
		if (fullTypeKey != null)
			Builder.append(propertyColumn).append(Constant.EQUALS).append(Integer.toString(fullTypeKey.intValue())).append(Constant.OR);
		if (shortTypeKey != null)
			Builder.append(propertyColumn).append(Chars.EQUALS).append(Integer.toString(shortTypeKey.intValue()));
		
		String filter = Builder.toString();
		if (filter.endsWith(Constant.OR))
			filter = filter.substring(0, filter.length() - Constant.OR.length());		
		
		return filter;
	}
}
