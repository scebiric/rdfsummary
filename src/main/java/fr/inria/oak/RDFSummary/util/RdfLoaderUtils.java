package fr.inria.oak.RDFSummary.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.openrdf.rio.RDFParser;

import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.RdfProperty;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfLoaderUtils {

	private static RdfDataset RdfDataset;
	private static Dictionary Dictionary;
	
	private static MessageBuilder MessageBuilder;
	
	/**
	 * @param rdfDataset
	 * @param dictionary
	 */
	public RdfLoaderUtils(final RdfDataset rdfDataset, final Dictionary dictionary) {
		RdfDataset = rdfDataset;
		Dictionary = dictionary;
		
		MessageBuilder = new MessageBuilder();
	}

	public static void loadRdfProperties() throws SQLException {
		final Set<String> rdfProperties = Sets.newHashSet();
		
		rdfProperties.add(RdfProperty.FULL_TYPE);
		rdfProperties.add(RdfProperty.TYPE);
		rdfProperties.add(RdfProperty.FULL_DOMAIN);
		rdfProperties.add(RdfProperty.DOMAIN);
		rdfProperties.add(RdfProperty.FULL_RANGE);
		rdfProperties.add(RdfProperty.RANGE);
		rdfProperties.add(RdfProperty.FULL_SUBCLASS);
		rdfProperties.add(RdfProperty.SUBCLASS);
		rdfProperties.add(RdfProperty.FULL_SUBPROPERTY);		
		rdfProperties.add(RdfProperty.SUBPROPERTY);		
		
		for (final String rdfProperty : rdfProperties) {
			Integer key = Dictionary.getKey(rdfProperty);
			if (key == null)
				key = Dictionary.insert(rdfProperty);
			
			RdfDataset.addRdfProperty(rdfProperty, key.intValue());
			RdfDataset.addProperty(key.intValue());
		}
	}
	
	public static void loadRdfPropertiesWithBrackets() throws SQLException {
		final Set<String> rdfProperties = Sets.newHashSet();
		
		rdfProperties.add(Rdf.FULL_TYPE);
		rdfProperties.add(Rdf.TYPE);
		rdfProperties.add(Rdfs.FULL_DOMAIN);
		rdfProperties.add(Rdfs.DOMAIN);
		rdfProperties.add(Rdfs.FULL_RANGE);
		rdfProperties.add(Rdfs.RANGE);
		rdfProperties.add(Rdfs.FULL_SUBCLASS);
		rdfProperties.add(Rdfs.SUBCLASS);
		rdfProperties.add(Rdfs.FULL_SUBPROPERTY);		
		rdfProperties.add(Rdfs.SUBPROPERTY);		
		
		for (final String rdfProperty : rdfProperties) {
			Integer key = Dictionary.getKey(rdfProperty);
			if (key == null)
				key = Dictionary.insert(rdfProperty);
			
			RdfDataset.addRdfProperty(rdfProperty, key.intValue());
			RdfDataset.addProperty(key.intValue());
		}
	}

	public static void parse(final String dataFilepath, final RDFParser parser) throws IOException {
		final File file = new File(dataFilepath);
		if (!file.exists())
			throw new IOException(MessageBuilder.append("The file ").append(dataFilepath).append(" does not exist.").toString());
		if (!FilenameUtils.getExtension(dataFilepath).equals(Extension.N_TRIPLES))
			throw new IOException(MessageBuilder.append("The only supported format is n-triples.").toString());
		
		final InputStream inputStream = new FileInputStream(file);
		parser.parse(inputStream, dataFilepath);
	}
}
