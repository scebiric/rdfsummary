package fr.inria.oak.RDFSummary.util;

import java.util.HashSet;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.reasoning.rdfs.BiMapSchemaValues;

/**
 * 
 * @author Damian BURSZTYN
 * @author Sejla CEBIRIC
 *
 */
public class SchemaUtils {

	/**
	 * Encodes <code>schema</code> using <code>dictionary</code> into a new (encoded) schema.
	 * @param schema
	 * @param dictionary
	 * @return encoded schema
	 * @throws InexistentValueException
	 * @throws DictionaryException
	 */
	public static Schema encode(final Schema schema, final Dictionary dictionary) throws DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(schema);
		Schema encodedSchema = null;
		Set<String> classes = new HashSet<String>();
		BiMapSchemaValues<String> subClasses = new BiMapSchemaValues<String>();
		for(final String klass: schema.getAllClasses()) {
			Set<String> subClassesForClass = new HashSet<String>();
			for(final String subClass: schema.getSubClasses(klass)) {
				if (!klass.equals(subClass)) {
					subClassesForClass.add(dictionary.getKey(subClass).toString());
				}
			}

			classes.add(dictionary.getKey(klass).toString());
			if (!subClassesForClass.isEmpty()) {
				subClasses.put(dictionary.getKey(klass).toString(), subClassesForClass);
			}
		}

		Set<String> properties = new HashSet<String>();
		BiMapSchemaValues<String> subProperties = new BiMapSchemaValues<String>();
		for(final String property: schema.getAllProperties()) {
			Set<String> subPropertiesForProperty = new HashSet<String>();
			for(final String subProperty: schema.getSubProperties(property)) {
				if (!property.equals(subProperty)) {
					subPropertiesForProperty.add(dictionary.getKey(subProperty).toString());
				}
			}

			properties.add(dictionary.getKey(property).toString());
			if (!subPropertiesForProperty.isEmpty()) {
				subProperties.put(dictionary.getKey(property).toString(), subPropertiesForProperty);
			}
		}

		BiMapSchemaValues<String> ranges = new BiMapSchemaValues<String>();
		for(final String range: schema.getAllRanges()) {
			Set<String> propertiesForRange = new HashSet<String>();
			for(final String property: schema.getPropertiesForRange(range)) {
				propertiesForRange.add(dictionary.getKey(property).toString());
			}
			ranges.put(dictionary.getKey(range).toString(), propertiesForRange);
		}

		BiMapSchemaValues<String> domains = new BiMapSchemaValues<String>();
		for(final String domain: schema.getAllDomains()) {
			Set<String> propertiesForDomain = new HashSet<String>();
			for(final String property: schema.getPropertiesForDomain(domain)) {
				propertiesForDomain.add(dictionary.getKey(property).toString());
			}
			domains.put(dictionary.getKey(domain).toString(), propertiesForDomain);
		}

		encodedSchema = new Schema(classes, properties, subClasses, subProperties, ranges, domains);

		return encodedSchema;
	}

	/**
	 * Decodes <code>schema</code> using <code>dictionary</code> into a new (decoded) schema.
	 * @param schema
	 * @param dictionary
	 * @return decoded schema
	 * @throws DictionaryException
	 * @throws InexistentKeyException
	 * @throws NumberFormatException
	 */
	public static Schema decode(final Schema schema, final Dictionary dictionary) throws NumberFormatException, DictionaryException, InexistentKeyException {
		Schema decodedSchema = null;
		Set<String> classes = new HashSet<String>();
		BiMapSchemaValues<String> subClasses = new BiMapSchemaValues<String>();
		for(final String klass: schema.getAllClasses()) {
			Set<String> subClassesForClass = new HashSet<String>();
			for(final String subClass: schema.getSubClasses(klass)) {
				if (!klass.equals(subClass)) {
					subClassesForClass.add(dictionary.getValue(Integer.valueOf(subClass)));
				}
			}

			classes.add(dictionary.getValue(Integer.valueOf(klass)));
			if (!subClassesForClass.isEmpty()) {
				subClasses.put(dictionary.getValue(Integer.valueOf(klass)), subClassesForClass);
			}
		}

		Set<String> properties = new HashSet<String>();
		BiMapSchemaValues<String> subProperties = new BiMapSchemaValues<String>();
		for(final String property: schema.getAllProperties()) {
			Set<String> subPropertiesForProperty = new HashSet<String>();
			for(final String subProperty: schema.getSubProperties(property)) {
				if (!property.equals(subProperty)) {
					subPropertiesForProperty.add(dictionary.getValue(Integer.valueOf(subProperty)));
				}
			}

			properties.add(dictionary.getValue(Integer.valueOf(property)));
			if (!subPropertiesForProperty.isEmpty()) {
				subProperties.put(dictionary.getValue(Integer.valueOf(property)), subPropertiesForProperty);
			}
		}

		BiMapSchemaValues<String> ranges = new BiMapSchemaValues<String>();
		for(final String range: schema.getAllRanges()) {
			Set<String> propertiesForRange = new HashSet<String>();
			for(final String property: schema.getPropertiesForRange(range)) {
				propertiesForRange.add(dictionary.getValue(Integer.valueOf(property)));
			}
			ranges.put(dictionary.getValue(Integer.valueOf(range)), propertiesForRange);
		}

		BiMapSchemaValues<String> domains = new BiMapSchemaValues<String>();
		for(final String domain: schema.getAllDomains()) {
			Set<String> propertiesForDomain = new HashSet<String>();
			for(final String property: schema.getPropertiesForDomain(domain)) {
				propertiesForDomain.add(dictionary.getValue(Integer.valueOf(property)));
			}
			domains.put(dictionary.getValue(Integer.valueOf(domain)), propertiesForDomain);
		}

		decodedSchema = new Schema(classes, properties, subClasses, subProperties, ranges, domains);

		return decodedSchema;
	}

	/** 
	 * Retrieves the set of constants from the schema object and adds to it the constants for RDF type and RDFS properties.
	 * 
	 * @param schema
	 * @return All schema constants plus RDF type and RDFS properties
	 */
	public static Set<String> getConstants(final Schema schema) {
		Set<String> constants = Sets.newHashSet();
		if (schema != null)
			constants.addAll(schema.getConstants());
				
		constants.add(Rdf.FULL_TYPE);
		constants.add(Rdfs.FULL_SUBCLASS);
		constants.add(Rdfs.FULL_SUBPROPERTY);
		constants.add(Rdfs.FULL_DOMAIN);
		constants.add(Rdfs.FULL_RANGE);
		constants.add(Rdf.TYPE);
		constants.add(Rdfs.SUBCLASS);
		constants.add(Rdfs.SUBPROPERTY);
		constants.add(Rdfs.DOMAIN);
		constants.add(Rdfs.RANGE);
		
		return constants;
	}
}
