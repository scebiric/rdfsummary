package fr.inria.oak.RDFSummary.util;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DotUtils {

	/**
	 * @param nodeKey
	 * @return The nodeKey prefixed with dot node prefix
	 */
	public static String getDotNodeId(int nodeKey) {
		return NameUtils.addDotNodePrefix(nodeKey);
	}

}
