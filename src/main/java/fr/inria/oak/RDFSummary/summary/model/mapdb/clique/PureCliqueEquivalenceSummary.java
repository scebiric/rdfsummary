package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PureCliqueEquivalenceSummary extends CliqueRdfSummary {

	/** 
	 * Creates a summary data node representing all gDataNodes   
	 * @throws SQLException 
	 */
	public int createDataNode(Iterator<Integer> gDataNodes) throws SQLException;

	/**
	 * Creates a type triple with sNode as the subject/source and the class IRI as the object 
	 * @param sNode
	 * @param classIRI
	 */
	public void createTypeTriple(int sNode, int classIRI);

	/**
	 * Creates type triples from sDataNode to all the classes.
	 * @param sDataNode
	 * @param classes
	 * 
	 */
	public void createTypeTriples(int sDataNode, Iterator<Integer> classes);

	/** Returns true if sNode has the specified class in the summary, otherwise false */
	public boolean existsTypeTriple(int sNode, int classIRI); 
}
