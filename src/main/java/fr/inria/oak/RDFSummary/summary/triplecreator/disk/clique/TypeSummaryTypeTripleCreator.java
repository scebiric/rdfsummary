package fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique;

import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TypeSummaryTypeTripleCreator {

	/**
	 * @param gNode
	 * @param classes 
	 */
	public void representTypeTriples(int gNode, TIntSet classes);
}
