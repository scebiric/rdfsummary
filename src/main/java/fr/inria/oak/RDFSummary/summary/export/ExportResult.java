package fr.inria.oak.RDFSummary.summary.export;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExportResult {
	
	private String dotFilepath;
	private String rdfFilepath;
	
	public ExportResult() {
	}
	
	public String getDotFilepath() {
		return dotFilepath;
	}
	
	public String getRdfFilepath() {
		return rdfFilepath;
	}
	
	public void setDotFilepath(final String value) {
		dotFilepath = value;
	}
	
	public void setRdfFilepath(final String value) {
		rdfFilepath = value;
	}
}
