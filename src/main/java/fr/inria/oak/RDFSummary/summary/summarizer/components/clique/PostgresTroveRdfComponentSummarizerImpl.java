package fr.inria.oak.RDFSummary.summary.summarizer.components.clique;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.summary.cliquebuilder.trove.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.triplecreator.trove.clique.TroveTripleCreator;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTroveRdfComponentSummarizerImpl implements TroveRdfComponentSummarizer {

	private final static Logger log = Logger.getLogger(TroveRdfComponentSummarizer.class);
	
	private static DmlDao DmlDao;
	private static DdlDao DdlDao;
	private static RdfTablesDao RdfTablesDao;
	private static TroveTripleCreator TripleCreator;
	private static TroveNodeRepresenter NodeRepresenter;
	private static TroveCliqueBuilder CliqueBuilder;
	private static PerformanceLogger PerformanceLogger;
	
	private final Timer componentTimer;
	private ResultSet result;
	private String[] dataColumns = new String[] { Constant.SUBJECT, Constant.PROPERTY, Constant.OBJECT };
	private String[] typeColumns = new String[] { Constant.SUBJECT, Constant.OBJECT };
	private static StringBuilder Builder;
	
	private static final String DATA_MESSAGE = "Data triples summarized in: ";
	private static final String TYPES_MESSAGE = "Type triples summarized in: ";
	
	/**
	 * @param dmlDao
	 * @param ddlDao
	 * @param rdfTablesDao
	 * @param tripleCreator
	 * @param nodeRepresenter
	 * @param cliqueBuilder
	 * @param performanceLogger
	 */
	public PostgresTroveRdfComponentSummarizerImpl(final DmlDao dmlDao, final DdlDao ddlDao, final RdfTablesDao rdfTablesDao,
			final TroveTripleCreator tripleCreator, final TroveNodeRepresenter nodeRepresenter,
			final TroveCliqueBuilder cliqueBuilder, final PerformanceLogger performanceLogger) {
		DmlDao = dmlDao;
		DdlDao = ddlDao;
		RdfTablesDao = rdfTablesDao;
		TripleCreator = tripleCreator;
		NodeRepresenter = nodeRepresenter;
		CliqueBuilder = cliqueBuilder;
		PerformanceLogger = performanceLogger;
		componentTimer = PerformanceLogger.newTimer();
	}

	@Override
	public void summarizeDataTriples(final WeakPureCliqueEquivalenceSummary summary, final String dataTable) throws SQLException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		componentTimer.reset();
		componentTimer.start();
		result = getDataTriples(dataTable);
		log.info("Representing data triples...");
		while (result.next()) {			
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
		componentTimer.stop();
		log.info(getDataSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_DATA, componentTimer.getTimeInMilliseconds());
	}

	@Override
	public void summarizeDataTriples(final WeakTypeEquivalenceSummary summary, final String dataTable) throws SQLException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		componentTimer.reset();
		componentTimer.start();
		result = getDataTriples(dataTable);
		log.info("Representing data triples...");
		while (result.next()) {			
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
		componentTimer.stop();
		log.info(getDataSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_DATA, componentTimer.getTimeInMilliseconds());
	}

	@Override
	public void summarizeDataTriples(final StrongPureCliqueEquivalenceSummary summary, final String dataTable) throws SQLException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		componentTimer.reset();
		componentTimer.start();		
		CliqueBuilder.buildSourceCliques(dataTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, summary);
		summary.clearSourceCliques();
		summary.clearTargetCliques();
		
		representDistinctDataNodesFromInput(dataTable, summary);
		summary.clearCliqueMaps();

		representDataTriples(dataTable, summary);
		
		componentTimer.stop();
		log.info(getDataSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_DATA, componentTimer.getTimeInMilliseconds());
	}

	@Override
	public void summarizeDataTriples(final StrongTypeEquivalenceSummary summary, final String dataTable, final String typesTable) throws SQLException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		
		componentTimer.reset(); 
		componentTimer.start();
		CliqueBuilder.buildSourceCliques(dataTable, typesTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, typesTable, summary);
		
		summary.clearSourceCliques();
		summary.clearTargetCliques();
		
		representDistinctDataNodesFromInput(dataTable, summary);
		summary.clearCliqueMaps();

		representDataTriples(dataTable, summary);
		
		componentTimer.stop();
		log.info(getDataSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_DATA, componentTimer.getTimeInMilliseconds());
	}
	
	private void representDataTriples(final String dataTable, final StrongEquivalenceSummary summary) throws SQLException {
		log.info("Representing data triples...");
		final Timer representTimer = PerformanceLogger.newTimer();
		representTimer.start();
		result = getDataTriples(dataTable);
		while (result.next()) {
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
		representTimer.stop();
		PerformanceLogger.addTiming(PerformanceLabel.REPRESENT_DATA_TRIPLES, representTimer.getTimeInMilliseconds());
	}
	
	private void representDistinctDataNodesFromInput(final String dataTable, final StrongEquivalenceSummary summary) throws SQLException {
		log.info("Representing distinct data nodes from " + dataTable + "...");
		final String schemaName = NameUtils.getSchemaName(dataTable);
		final String encodedDistinctNodesTable = NameUtils.qualify(schemaName, Constant.ENCODED_DISTINCT_NODES_TABLE);
		
		PerformanceLogger.stopSummarizationTimer();
		DdlDao.dropTableIfExists(encodedDistinctNodesTable);
		PerformanceLogger.startSummarizationTimer();
		
		RdfTablesDao.getDistinctNodesFromDataTable(encodedDistinctNodesTable, dataTable);
		
		int node = -1;
		final Timer representTimer = PerformanceLogger.newTimer();
		representTimer.start();
		result = DmlDao.getSelectionResultSet(new String[] { Character.toString(Chars.STAR) }, false, encodedDistinctNodesTable, null, PerformanceLabel.SELECT_DISTINCT_NODES);
		while (result.next()) {
			node = result.getInt(1);
			if (!summary.isClassOrPropertyNode(node))
				NodeRepresenter.representInputDataNode(summary, node);
		}
		result.close();
		representTimer.stop();
		PerformanceLogger.addTiming(PerformanceLabel.REPRESENT_G_DATA_NODES, representTimer.getTimeInMilliseconds());
	}

	@Override
	public void summarizeTypeTriples(final PureCliqueEquivalenceSummary summary, final String typesTable) throws SQLException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));

		log.info("Retrieving type triples...");
		componentTimer.reset();
		componentTimer.start();
		result = DmlDao.getSelectionResultSet(typeColumns, false, typesTable, null, PerformanceLabel.TYPE_TRIPLES_RESULT_SET); 		
		log.info("Representing type triples...");
		int subject = -1;
		int classIRI = -1;
		TIntSet typedOnlyDataNodes = new TIntHashSet();
		TIntSet classesOfTypedOnlyDataNodes = new TIntHashSet();
		while (result.next()) {			
			subject = result.getInt(1);
			classIRI = result.getInt(2);
			boolean represented = TripleCreator.representTypeTriple(summary, subject, classIRI);
			if (!represented) {
				// The subject is a data node and it has only type properties
				typedOnlyDataNodes.add(subject);
				classesOfTypedOnlyDataNodes.add(classIRI);
			}
		}
		result.close();

		if (typedOnlyDataNodes.size() > 0) {
			log.info("Representing typed-only resources...");
			TripleCreator.representTypeTriples(summary, typedOnlyDataNodes, classesOfTypedOnlyDataNodes);
		} 
		
		componentTimer.stop();
		log.info(getTypeSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_TYPES, componentTimer.getTimeInMilliseconds());
	}

	@Override
	public void summarizeTypeTriples(final TypeEquivalenceSummary summary, final String typesTable) throws SQLException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	

		log.info("Retrieving type triples...");
		componentTimer.reset();
		componentTimer.start();
		result = DmlDao.getSelectionResultSet(typeColumns, false, typesTable, Constant.SUBJECT, PerformanceLabel.TYPE_TRIPLES_RESULT_SET);
		boolean hasResults = result.next();
		if (hasResults) {		
			log.info("Representing type triples...");
			int subject = result.getInt(1);
			TIntSet classes = new TIntHashSet();
			classes.add(result.getInt(2));							
			while (result.next()) {					
				if (result.getInt(1) == subject) 					
					classes.add(result.getInt(2));
				else {							
					TripleCreator.representTypeTriples(summary, subject, classes);

					// New subject
					subject = result.getInt(1);
					classes = new TIntHashSet();
					classes.add(result.getInt(2));						
				}				
			}

			if (classes.size() > 0) 
				TripleCreator.representTypeTriples(summary, subject, classes);
		}
		else 
			log.info("No classes found.");
		
		result.close();
		componentTimer.stop();
		log.info(getTypeSummarizationMessage(componentTimer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.SUMMARIZE_TYPES, componentTimer.getTimeInMilliseconds());
	}

	private ResultSet getDataTriples(final String dataTable) throws SQLException {
		return DmlDao.getSelectionResultSet(dataColumns, false, dataTable, null, PerformanceLabel.DATA_TRIPLES_RESULT_SET);
	}
	
	private String getDataSummarizationMessage(final String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(DATA_MESSAGE).append(timeAsString);
		
		return Builder.toString();
	}
	
	private String getTypeSummarizationMessage(String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(TYPES_MESSAGE).append(timeAsString);
		
		return Builder.toString();
	}
}
