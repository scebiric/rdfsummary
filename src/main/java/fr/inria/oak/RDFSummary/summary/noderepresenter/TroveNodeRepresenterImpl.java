package fr.inria.oak.RDFSummary.summary.noderepresenter;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.util.SetUtils;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveNodeRepresenterImpl implements TroveNodeRepresenter {

	/**
	 * The representative data node of gDataNode will have the same source AND target clique as gDataNode.
	 * There can be at most one such node in a strong equivalence summary.
	 * 
	 * @param gDataNode
	 * @return The summary data node representing gDataNode
	 * @throws SQLException 
	 * 
	 */
	public int representInputDataNode(final StrongEquivalenceSummary summary, final int gDataNode) throws SQLException {
		int sDataNode = summary.getSummaryDataNodeForInputDataNode(gDataNode);
		if (sDataNode > 0)
			return sDataNode;

		int sourceCliqueId = summary.getSourceCliqueIdBySubject(gDataNode);
		TIntSet sourceCliqueDataNodes = summary.getDataNodesForSourceCliqueId(sourceCliqueId);

		int targetCliqueId = summary.getTargetCliqueIdByObject(gDataNode);
		TIntSet targetCliqueDataNodes = summary.getDataNodesForTargetCliqueId(targetCliqueId);

		sDataNode = SetUtils.findFirstCommonElement(sourceCliqueDataNodes, targetCliqueDataNodes);
		if (sDataNode > 0) 
			summary.representInputDataNode(gDataNode, sDataNode);
		else {
			sDataNode = summary.createDataNode(gDataNode);
			summary.storeDataNodeForSourceCliqueId(sourceCliqueId, sDataNode);
			summary.storeDataNodeForTargetCliqueId(targetCliqueId, sDataNode);
		}	

		return sDataNode;
	}
	
	/**
	 * Obtains a data node representing the subject and the property source.
	 * In a pure clique equivalence summary, the types have not yet been summarized, so all data nodes are considered untyped. 
	 * There should be only one untyped data node source per data property. 
	 * If the subject differs from an already existing (untyped) source of dataProperty, the two nodes will be unified.
	 * 
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the property source 
	 * @throws SQLException 
	 * 
	 */
	@Override
	public int getRepresentativeSourceDataNode(final WeakPureCliqueEquivalenceSummary summary, final int subject, final int dataProperty) throws SQLException {		
		int propertySource = summary.getSourceDataNodeForDataProperty(dataProperty);		
		int subjectNode = summary.getSummaryDataNodeForInputDataNode(subject);

		if (propertySource > 0 && subjectNode > 0) {
			if (propertySource == subjectNode)
				return subjectNode;
			else 
				return summary.unifyDataNodes(propertySource, subjectNode);
		}

		if (propertySource > 0 && subjectNode == 0) {
			summary.representInputDataNode(subject, propertySource);
			return propertySource;
		}

		if (propertySource == 0 && subjectNode > 0) {
			return subjectNode;
		}

		// propertySource == 0 && subjectNode == 0
		return summary.createDataNode(subject);
	}

	/**
	 * Obtains a data node representing the object and the property target.
	 * In a pure clique equivalence summary, the types have not yet been summarized, so all data nodes are considered untyped.
	 * There should be only one untyped data node target per data property. 
	 * If the object is untyped and differs from an already existing (untyped) target of dataProperty, the two nodes will be unified.
	 * 
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the property target 
	 * @throws SQLException 
	 * 
	 */
	@Override
	public int getRepresentativeTargetDataNode(final WeakPureCliqueEquivalenceSummary summary, final int object, final int dataProperty) throws SQLException {
		int propertyTarget = summary.getTargetDataNodeForDataProperty(dataProperty);
		int objectNode = summary.getSummaryDataNodeForInputDataNode(object);

		if (propertyTarget > 0 && objectNode > 0) {	
			if (propertyTarget == objectNode)
				return objectNode;
			else 
				return summary.unifyDataNodes(propertyTarget, objectNode);
		}

		if (propertyTarget > 0 && objectNode == 0) {
			summary.representInputDataNode(object, propertyTarget);
			return propertyTarget;
		}

		if (propertyTarget == 0 && objectNode > 0) {
			return objectNode;
		}

		// untypedPropertyTarget == 0 && objectNode == 0
		return summary.createDataNode(object);
	}

	/**
	 * Obtains a data node representing the subject and the property source.
	 * In a type equivalence summary, types having already been summarized, a distinction can be made between typed and untyped data nodes.
	 * 
	 * There should be only one untyped data node source per data property. 
	 * If the subject is typed, its (typed) representative is returned.
	 * If the subject is untyped, its representative will be the untyped source of dataProperty. If the subject 
	 * differs from an already existing (untyped) source of dataProperty, the two nodes will be unified.
	 * 
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the property source 
	 * @throws SQLException 
	 * 
	 */
	@Override
	public int getRepresentativeSourceDataNode(final WeakTypeEquivalenceSummary summary, final int subject, final int dataProperty) throws SQLException {
		int propertySource = summary.getSourceDataNodeForDataProperty(dataProperty);		
		int subjectNode = summary.getSummaryDataNodeForInputDataNode(subject);

		if (propertySource > 0 && subjectNode > 0) {
			if (summary.isTypedDataNode(subjectNode) || propertySource == subjectNode)
				return subjectNode;
			else  
				return summary.unifyDataNodes(propertySource, subjectNode);
		}

		if (propertySource > 0 && subjectNode == 0) {
			summary.representInputDataNode(subject, propertySource);
			return propertySource;
		}

		if (propertySource == 0 && subjectNode > 0) {
			return subjectNode;
		}

		// propertySource == 0 && subjectNode == 0
		return summary.createDataNode(subject);
	}

	/**
	 * Obtains a data node representing the object and the property target.
	 * In a type equivalence summary, types having already been summarized, a distinction can be made between typed and untyped data nodes.
	 * 
	 * There should be only one untyped data node target per data property. 
	 * If the object is typed, its (typed) representative is returned.
	 * If the object is untyped, its representative will be the untyped target of dataProperty. If the object 
	 * differs from an already existing (untyped) target of dataProperty, the two nodes will be unified.
	 * 
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the property target 
	 * @throws SQLException 
	 * 
	 */
	@Override
	public int getRepresentativeTargetDataNode(final WeakTypeEquivalenceSummary summary, final int object, final int dataProperty) throws SQLException {
		int propertyTarget = summary.getTargetDataNodeForDataProperty(dataProperty);
		int objectNode = summary.getSummaryDataNodeForInputDataNode(object);

		if (propertyTarget > 0 && objectNode > 0) {
			if (summary.isTypedDataNode(objectNode) || propertyTarget == objectNode)
				return objectNode;
			else  
				return summary.unifyDataNodes(propertyTarget, objectNode);
		}

		if (propertyTarget > 0 && objectNode == 0) {
			summary.representInputDataNode(object, propertyTarget);
			return propertyTarget;
		}

		if (propertyTarget == 0 && objectNode > 0) {
			return objectNode;
		}

		// untypedPropertyTarget == 0 && objectNode == 0
		return summary.createDataNode(object);
	}
}
