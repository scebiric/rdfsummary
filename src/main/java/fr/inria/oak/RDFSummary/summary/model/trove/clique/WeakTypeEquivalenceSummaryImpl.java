package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakTypeEquivalenceSummaryImpl implements WeakTypeEquivalenceSummary {

	private final WeakEquivalenceSummary weakEquivSummary;
	private final TypeEquivalenceSummary typeEquivSummary;
	private final RdfSummary rdfSummary;

	/**
	 * @param weakEquivalenceSummary
	 * @param typeEquivalenceSummary
	 * @param rdfSummary
	 */
	public WeakTypeEquivalenceSummaryImpl(final WeakEquivalenceSummary weakEquivalenceSummary, 
			final TypeEquivalenceSummary typeEquivalenceSummary, final RdfSummary rdfSummary) {
		this.weakEquivSummary = weakEquivalenceSummary;
		this.typeEquivSummary = typeEquivalenceSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target);
	}
	
	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		typeEquivSummary.createTypeTriples(sNode, classes);
	}
	
	@Override
	public int unifyDataNodes(int dataNode1, int dataNode2) {		
		return weakEquivSummary.unifyDataNodes(dataNode1, dataNode2);
	}

	@Override
	public int getDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getDegreeForDataNode(dataNode);
	}

	@Override
	public int getIncomingDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getIncomingDegreeForDataNode(dataNode);
	}

	@Override
	public int getOutgoingDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getOutgoingDegreeForDataNode(dataNode);
	}

	@Override
	public void setSourceDataNodeForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setSourceDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public void setTargetDataNodeForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setTargetDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public int getSourceDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getSourceDataNodeForDataProperty(dataProperty);
	}

	@Override
	public int getTargetDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getTargetDataNodeForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode) {
		return weakEquivSummary.getDataPropertiesForSourceDataNode(dataNode);
	}

	@Override
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode) {
		return weakEquivSummary.getDataPropertiesForTargetDataNode(dataNode);
	}

	@Override
	public boolean isTypedDataNode(int sDataNode) {
		return typeEquivSummary.isTypedDataNode(sDataNode);
	}

	@Override
	public void setHighestTypedDataNode(int sNode) {
		typeEquivSummary.setHighestTypedDataNode(sNode);
	}

	@Override
	public int getSummaryDataNodeForClasses(TIntSet classes) {
		return typeEquivSummary.getSummaryDataNodeForClasses(classes);
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}
	
	@Override
	public void removeDataPropertiesForSourceDataNode(int source) {
		weakEquivSummary.removeDataPropertiesForSourceDataNode(source);
	}

	@Override
	public void removeDataPropertiesForTargetDataNode(int target) {
		weakEquivSummary.removeDataPropertiesForTargetDataNode(target);
	}

	@Override
	public void storeSummaryDataNodeByClassSet(TIntSet classes, int sDataNode) {
		typeEquivSummary.storeSummaryDataNodeByClassSet(classes, sDataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}

	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}

	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}

	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
