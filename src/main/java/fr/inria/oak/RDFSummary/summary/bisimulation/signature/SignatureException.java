package fr.inria.oak.RDFSummary.summary.bisimulation.signature;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SignatureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2209570255232288370L;

	public SignatureException() {
		// TODO Auto-generated constructor stub
	}

	public SignatureException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SignatureException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SignatureException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SignatureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
