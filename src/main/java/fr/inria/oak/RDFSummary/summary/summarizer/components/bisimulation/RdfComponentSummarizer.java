package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfComponentSummarizer {

	public void summarizeDataTriples() throws SQLException, SignatureException;

	public void summarizeTypeTriples() throws SQLException;
}
