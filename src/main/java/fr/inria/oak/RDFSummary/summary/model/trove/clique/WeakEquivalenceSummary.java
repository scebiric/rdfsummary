package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface WeakEquivalenceSummary extends RdfSummary {
	
	/** 
	 * Unifies the two data nodes 
	 *   
	 * @param dataNode1
	 * @param dataNode2
	 * @return The remaining node
	 *  
	 *  
	 */
	public int unifyDataNodes(int dataNode1, int dataNode2);
	
	/** Returns the degree of the specified data node as the sum of incoming and outgoing data edges 
	 *  */
	public int getDegreeForDataNode(int dataNode);
	
	/** Returns the incoming degree of the specified data node as the number of incoming data edges 
	 *  */
	public int getIncomingDegreeForDataNode(int dataNode);
	
	/** Returns the outgoing degree of the specified data node as the number of outgoing data edges 
	 *  */
	public int getOutgoingDegreeForDataNode(int dataNode);
	
	/** Stores the specified data node as the source of the data property 
	 *  */
	public void setSourceDataNodeForDataProperty(int dataProperty, int dataNode);
	
	/** Stores the specified data node as the target of the data property 
	 *  */
	public void setTargetDataNodeForDataProperty(int dataProperty, int dataNode);
	
	/** Returns the source node for the specified data property or 0 if there is no such node */
	public int getSourceDataNodeForDataProperty(int dataProperty);
	
	/** Returns the target node for the specified data property or 0 if there is no such node */
	public int getTargetDataNodeForDataProperty(int dataProperty);	
	
	/** Returns the set of data properties of which the specified summary data node is the source 
	 *  */
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode);
	
	/** Returns the set of data properties of which the specified summary data node is the target 
	 *  */
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode);
	
	/**
	 * Removes the mapping of data properties for the specified source node.
	 * @param source
	 */
	public void removeDataPropertiesForSourceDataNode(int source);
	
	/**
	 * Removes the mapping of data properties for the specified target node.
	 * @param target
	 */
	public void removeDataPropertiesForTargetDataNode(int target);
}
