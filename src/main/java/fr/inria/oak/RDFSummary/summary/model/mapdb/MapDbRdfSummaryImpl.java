package fr.inria.oak.RDFSummary.summary.model.mapdb;

import java.util.Iterator;
import java.util.NavigableSet;

import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbRdfSummaryImpl implements RdfSummary {

	private final NavigableSet<Tuple3<Integer, Integer, Integer>> dataTriples;
	private final NavigableSet<Tuple2<Integer, Integer>> classesBySummaryNode; // summary node can be any node
	
	/**
	 * @param summaryDb
	 * @param dataTriplesSetName
	 * @param typeTriplesSetName
	 */
	public MapDbRdfSummaryImpl(final DB summaryDb, final String dataTriplesSetName, final String typeTriplesSetName) {
		dataTriples = summaryDb.createTreeSet(dataTriplesSetName).counterEnable().serializer(BTreeKeySerializer.TUPLE3).make();
		classesBySummaryNode = summaryDb.createTreeSet(typeTriplesSetName).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		dataTriples.add(new Tuple3<Integer, Integer, Integer>(source, dataProperty, target));
	}
	
	@Override
	public Iterator<Integer> getClassesBySummaryNode(final int sNode) {
		return Fun.filter(classesBySummaryNode, sNode).iterator();
	}
	
	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		return dataTriples.iterator();
	}
	
	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		classesBySummaryNode.add(new Tuple2<Integer, Integer>(sNode, classIRI));
	}
	
	@Override
	public void addClassesBySummaryNode(final int sNode, final Iterator<Integer> classes) {
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.hasNext());
		
		while (classes.hasNext())
			addClassBySummaryNode(sNode, classes.next());
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		return classesBySummaryNode.iterator();
	}
	
	@Override
	public int getDataTriplesCount() {
		return dataTriples.size();
	}

	@Override
	public int getTypeTriplesCount() {
		return classesBySummaryNode.size();
	}
}
