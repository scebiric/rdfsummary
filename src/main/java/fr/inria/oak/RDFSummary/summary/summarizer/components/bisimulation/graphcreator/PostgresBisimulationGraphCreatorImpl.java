package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.Serializer;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.SummaryUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresBisimulationGraphCreatorImpl implements BisimulationGraphCreator {

	protected final static Logger log = Logger.getLogger(BisimulationGraphCreator.class);

	protected final DB summaryDB;
	protected final BisimulationRdfSummary summary;
	protected final PostgresBisimulationRdfDataset rdfDataset;
	protected final UriGenerator uriGenerator;
	protected final Dictionary dictionary;
	protected final String sDictionaryDataNodeByHashName;
	protected final Timer timer;

	protected final MessageBuilder messageBuilder;

	private int subject;
	private int property;
	private int object;
	private int subjectRepresentative;
	private int objectRepresentative;
	private ResultSet typeTriples;
	private Integer sDataNode = null;
	private Integer typedOnlyRepresentative = null;
	
	/**
	 * @param summaryDB
	 * @param summary
	 * @param rdfDataset
	 * @param uriGenerator
	 * @param dictionary
	 * @param sDictionaryDataNodeByHashName
	 * @param timer
	 */
	public PostgresBisimulationGraphCreatorImpl(final DB summaryDB, final BisimulationRdfSummary summary, final PostgresBisimulationRdfDataset rdfDataset,
			final UriGenerator uriGenerator, final Dictionary dictionary, final String sDictionaryDataNodeByHashName, final Timer timer) {
		this.summaryDB = summaryDB;
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.uriGenerator = uriGenerator;
		this.dictionary = dictionary;
		this.sDictionaryDataNodeByHashName = sDictionaryDataNodeByHashName;
		this.timer = timer;

		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void createDataNodes() throws SQLException {
		log.info("Generating and encoding data node URIs..."); 
		timer.reset();
		timer.start();
		int sDictionaryDataNode;
		final Map<Integer, Integer> sDictionaryDataNodeById = summaryDB.createHashMap(sDictionaryDataNodeByHashName).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
		final Set<Integer> sIdDataNodes = summary.getDataNodes();
		for (final int id : sIdDataNodes) {
			sDictionaryDataNode = createNewDataNode();
			sDictionaryDataNodeById.put(id, sDictionaryDataNode);
		}

		final Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		int gDataNode;
		int sHashDataNode;
		while (gDataNodes.hasNext()) {
			gDataNode = gDataNodes.next();
			sHashDataNode = summary.getSummaryDataNodeByGDataNode(gDataNode);
			sDictionaryDataNode = sDictionaryDataNodeById.get(sHashDataNode).intValue();
			summary.setSummaryDataNodeByGDataNode(gDataNode, sDictionaryDataNode);
		}
		timer.stop();
		log.info(messageBuilder.append("Data nodes created in: ").append(timer.getTimeAsString()));
	}

	/** 
	 * The tree set ensures uniqueness of the inserted triples.
	 * @throws SQLException 
	 */
	@Override
	public void createDataTriples() throws SQLException {
		log.info("Creating summary data triples...");
		timer.reset();
		timer.start();
		final ResultSet dataTriples = rdfDataset.getDataTriples();
		while (dataTriples.next()) {
			subject = dataTriples.getInt(1);
			property = dataTriples.getInt(2);
			object = dataTriples.getInt(3);
			
			subjectRepresentative = SummaryUtils.getRepresentative(subject, rdfDataset, summary);
			objectRepresentative = SummaryUtils.getRepresentative(object, rdfDataset, summary);
			
			summary.createDataTriple(subjectRepresentative, property, objectRepresentative);
		}
		dataTriples.close();
		timer.stop();
		log.info(messageBuilder.append("Data triples created in: ").append(timer.getTimeAsString())); 
	}

	@Override
	public void createTypeTriples() throws SQLException {
		typeTriples = rdfDataset.getTypeTriples();
		while (typeTriples.next()) {
			subject = typeTriples.getInt(1);
			object = typeTriples.getInt(2);
			summary.addClassBySummaryNode(getTypeTripleSubjectRepresentative(subject), object);
		}
	}

	private int getTypeTripleSubjectRepresentative(final int subject) throws SQLException {
		// Class or property
		if (rdfDataset.isClassOrProperty(subject))
			return subject;
		
		// Data node
		sDataNode = summary.getSummaryDataNodeByGDataNode(subject);
		if (sDataNode != null)
			return sDataNode.intValue();
		
		// Typed-only data node
		if (typedOnlyRepresentative == null) {
			typedOnlyRepresentative = createNewDataNode();
		}
		summary.setSummaryDataNodeByGDataNode(subject, typedOnlyRepresentative.intValue());
			
		return typedOnlyRepresentative.intValue();
	}

	private int createNewDataNode() throws SQLException {
		return dictionary.insert(uriGenerator.generateDataNodeUri(dictionary.getSeed()));
	}
}
