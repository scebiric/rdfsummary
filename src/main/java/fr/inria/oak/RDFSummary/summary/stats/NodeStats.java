package fr.inria.oak.RDFSummary.summary.stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class NodeStats {

	private final int dataNodeCount;
	private int classNodeCount;
	private int propertyNodeCount;
	private int totalDistinctNodeCount;
	
	/**
	 * @param dataNodeCount
	 */
	public NodeStats(final int dataNodeCount) {
		this.dataNodeCount = dataNodeCount;
	}

	public int getDataNodeCount() {
		return dataNodeCount;
	}
	
	public int getClassNodeCount() {
		return classNodeCount;
	}
	
	public int getPropertyNodeCount() {
		return propertyNodeCount;
	}
	
	public int getTotalDistinctNodeCount() {
		return totalDistinctNodeCount;
	}
	
	public void setClassNodeCount(final int value) {
		classNodeCount = value;
	}
	
	public void setPropertyNodeCount(final int value) {
		propertyNodeCount = value;
	}
	
	public void setTotalDistinctNodeCount(final int value) {
		totalDistinctNodeCount = value;
	}
}
