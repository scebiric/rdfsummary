package fr.inria.oak.RDFSummary.summary.stats.bisimulation;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.NodeStats;
import fr.inria.oak.RDFSummary.summary.stats.RdfGraphStats;
import fr.inria.oak.RDFSummary.summary.stats.StatsCollector;
import fr.inria.oak.RDFSummary.summary.stats.TripleStats;
import fr.inria.oak.RDFSummary.timing.SummaryTiming;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationMapDbPostgresStatsCollectorImpl implements StatsCollector {
	private static Logger log = Logger.getLogger(StatsCollector.class);
			
	private final RdfGraphStats rdfGraphStats;
	private final BisimulationStats bisimulationStats;
	private final PostgresBisimulationRdfDataset rdfDataset;
	private final BisimulationRdfSummary summary;
	private final Timings timings;
	private final PostgresTableNames tables;
	private final DmlDao dmlDao;
	private final String outputFolder;
	private final String statsFilename;
	
	/**
	 * @param rdfGraphStats
	 * @param bisimulationStats
	 * @param rdfDataset
	 * @param summary
	 * @param timings
	 * @param tables
	 * @param dmlDao
	 * @param outputFolder
	 * @param statsFilename
	 */
	public BisimulationMapDbPostgresStatsCollectorImpl(final RdfGraphStats rdfGraphStats, final BisimulationStats bisimulationStats,
			final PostgresBisimulationRdfDataset rdfDataset, final BisimulationRdfSummary summary, final Timings timings,
			final PostgresTableNames tables, final DmlDao dmlDao, final String outputFolder, final String statsFilename) {
		this.rdfGraphStats = rdfGraphStats;
		this.bisimulationStats = bisimulationStats;
		this.rdfDataset = rdfDataset;
		this.summary = summary;
		this.timings = timings;
		this.tables = tables;
		this.dmlDao = dmlDao;
		this.outputFolder = outputFolder;
		this.statsFilename = statsFilename;
	}

	@Override
	public void collectInputGraphStats() throws SQLException, QueryException {
		final int dataTripleCount = dmlDao.getRowCount(tables.getEncodedDataTable());
		final int typeTripleCount = dmlDao.getRowCount(tables.getEncodedTypesTable());
		final TripleStats tripleStats = new TripleStats(rdfDataset.getSchema().getSize(), dataTripleCount, typeTripleCount);
		final NodeStats nodeStats = new NodeStats(rdfDataset.getDataNodeCount());
		
		rdfGraphStats.setTripleStats(tripleStats);
		rdfGraphStats.setNodeStats(nodeStats);
		rdfGraphStats.setDistinctClassesCount(rdfDataset.getClassesCount());
		rdfGraphStats.setDistinctPropertiesCount(getDistinctDataPropertiesCount());
	}

	@Override
	public void collectBisimulationStats() throws SQLException, QueryException {
		final TripleStats tripleStats = new TripleStats(rdfDataset.getSchema().getSize(), 
				summary.getDataTriplesCount(), summary.getTypeTriplesCount());
		final NodeStats nodeStats = new NodeStats(summary.getDataNodeCount());
		
		bisimulationStats.setTripleStats(tripleStats);
		bisimulationStats.setNodeStats(nodeStats);
		bisimulationStats.setDistinctClassesCount(rdfDataset.getClassesCount());
		bisimulationStats.setDistinctPropertiesCount(getDistinctDataPropertiesCount());
	}

	@Override
	public void exportStats() throws IOException {
		log.info("Writing stats to file...");
		Utils.mkDirs(outputFolder);
		final String filepath = Utils.getFilepath(outputFolder, statsFilename, Extension.TXT);
		final PrintWriter writer = new PrintWriter(new FileWriter(filepath));

		writer.printf("Input data triples: %s", rdfGraphStats.getTripleStats().getDataTripleCount());
		writer.printf("\nInput type triples: %s", rdfGraphStats.getTripleStats().getTypeTripleCount());
		writer.printf("\nInput total triples: %s", rdfGraphStats.getTripleStats().getTotalTripleCount());
		writer.println();
		writer.printf("\nSummary data triples: %s", bisimulationStats.getTripleStats().getDataTripleCount());
		writer.printf("\nSummary type triples: %s", bisimulationStats.getTripleStats().getTypeTripleCount());
		writer.printf("\nSummary total triples: %s", bisimulationStats.getTripleStats().getTotalTripleCount());
		writer.println();
		writer.printf("\nInput data nodes: %s", rdfGraphStats.getNodeStats().getDataNodeCount());
		writer.printf("\nSummary data nodes: %s", bisimulationStats.getNodeStats().getDataNodeCount());
		writer.println();
		writer.printf("\nDistinct data properties (input and summary): %s", rdfGraphStats.getDistinctPropertiesCount());
		writer.printf("\nDistinct classes (input and summary): %s", rdfGraphStats.getDistinctClassesCount());
		writer.printf("\nSchema triples (input and summary): %s", rdfGraphStats.getTripleStats().getSchemaTripleCount());
		writer.println();
		writer.printf("\nBisimulation iterations: %s", bisimulationStats.getIterationsCount());
		writer.printf("\nSummarization time: %s sec", Utils.millisecondsToSeconds(timings.getTime(SummaryTiming.SUMMARIZATION_TIME)));
		writer.close();
		log.info("Stats written to file: " + filepath);
	}
	
	private int getDistinctDataPropertiesCount() throws SQLException, QueryException {
		return dmlDao.getDistinctColumnValuesCount(tables.getEncodedDataTable(), Constant.PROPERTY);
	}
}
