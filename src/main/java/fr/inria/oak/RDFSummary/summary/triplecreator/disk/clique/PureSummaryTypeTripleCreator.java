package fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique;

import java.sql.SQLException;
import java.util.Iterator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PureSummaryTypeTripleCreator {
	
	/**
	 * @param gNode
	 * @param classIRI
	 * @return True if the triple has been represented, otherwise false
	 *  
	 */
	public boolean representTypeTriple(int gNode, int classIRI);
	
	/**
	 * @param gDataNodes
	 * @param classes  
	 * @throws SQLException 
	 */
	public void representTypeTriples(Iterator<Integer> gDataNodes, Iterator<Integer> classes) throws SQLException;
}
