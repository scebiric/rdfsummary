package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation;

import java.sql.SQLException;
import java.util.Iterator;

import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.BisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.BisimulationSignature;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.bisimulation.BisimulationStats;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator.BisimulationGraphCreator;
import fr.inria.oak.RDFSummary.timer.Timer;
import gnu.trove.iterator.TIntIntIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresBisimulationRdfComponentSummarizerImpl implements RdfComponentSummarizer {

	private final static Logger log = Logger.getLogger(RdfComponentSummarizer.class);
	
	private final BisimulationRdfSummary summary;
	private final BisimulationRdfDataset rdfDataset;
	private final TriplesDao triplesDao; 
	private final BisimulationSignature signature;
	private final BisimulationGraphCreator graphCreator;
	private final BisimulationStats stats;
	private final Timer componentTimer;
	private final Timer timer;
	
	private final MessageBuilder messageBuilder;
	
	/**
	 * @param summary
	 * @param rdfDataset
	 * @param triplesDao
	 * @param signature
	 * @param graphCreator
	 * @param stats
	 * @param componentTimer
	 * @param timer
	 */
	public PostgresBisimulationRdfComponentSummarizerImpl(final BisimulationRdfSummary summary, final BisimulationRdfDataset rdfDataset,
			final TriplesDao triplesDao,
			final BisimulationSignature signature, final BisimulationGraphCreator graphCreator, final BisimulationStats stats, 
			final Timer componentTimer, final Timer timer) {
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.triplesDao = triplesDao;
		this.signature = signature;
		this.graphCreator = graphCreator;
		this.stats = stats;
		this.componentTimer = componentTimer;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void summarizeDataTriples() throws SQLException, SignatureException {
		log.info("Summarizing data triples...");
		componentTimer.reset();
		componentTimer.start();
		triplesDao.prepareDataSelectStatements();
		
		Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		while (gDataNodes.hasNext()) {
			summary.setSummaryDataNodeByGDataNode(gDataNodes.next(), Constant.INITIAL_BLOCK_ID);
		}
		
		log.info("Computing partitions...");
		timer.reset();
		timer.start();		
		int partitionsCountOld = 0;
		int partitionsCountNew = 1;
		int iterationsCount = 1;
		TIntIntMap sDataNodeByGDataNode;
		TIntIntIterator it;
		
		while (partitionsCountOld != partitionsCountNew) {
			log.info(messageBuilder.append("Iteration: ").append(Integer.toString(iterationsCount)));
			sDataNodeByGDataNode = new TIntIntHashMap(); 
			signature.clearSignatures();
			gDataNodes = rdfDataset.getDataNodes();
			while (gDataNodes.hasNext()) {
				final int gDataNode = gDataNodes.next();
				int sDataNode = signature.getSummaryNode(gDataNode);	
				sDataNodeByGDataNode.put(gDataNode, sDataNode);
			}
			
			it = sDataNodeByGDataNode.iterator();
			while (it.hasNext()) {
				it.advance();
				summary.setSummaryDataNodeByGDataNode(it.key(), it.value());
			}
			
			partitionsCountOld = partitionsCountNew;
			partitionsCountNew = summary.getDataNodeCount();
			iterationsCount++;
		}
		timer.stop();
		log.info(messageBuilder.append("Partitions computed in: ").append(timer.getTimeAsString()));
		log.info(messageBuilder.append("Bisimulation iterations: ").append(Integer.toString(iterationsCount)));
		
		graphCreator.createDataNodes();
		graphCreator.createDataTriples();
		
		triplesDao.closeDataSelectStatements();
		componentTimer.stop();
		log.info(messageBuilder.append("Total data triples summarization time: ").append(componentTimer.getTimeAsString()));
		
		stats.setIterationsCount(iterationsCount);
	}

	@Override
	public void summarizeTypeTriples() throws SQLException {
		log.info("Summarizing type triples...");
		componentTimer.reset();
		componentTimer.start();
		triplesDao.prepareTypesSelectStatements();
		graphCreator.createTypeTriples();
		triplesDao.closeTypesSelectStatements();
		componentTimer.stop();
		log.info(messageBuilder.append("Type triples summarized in: ").append(componentTimer.getTimeAsString()));	
	}

}
