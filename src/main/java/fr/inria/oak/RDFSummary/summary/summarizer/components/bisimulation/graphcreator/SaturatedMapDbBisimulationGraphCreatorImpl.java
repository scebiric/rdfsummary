package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator;

import java.util.Iterator;

import org.mapdb.DB;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.MapDbBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class SaturatedMapDbBisimulationGraphCreatorImpl extends MapDbBisimulationGraphCreatorImpl {

	/**
	 * @param summaryDB
	 * @param summary
	 * @param rdfDataset
	 * @param uriGenerator
	 * @param dictionary
	 * @param timer
	 */
	public SaturatedMapDbBisimulationGraphCreatorImpl(final DB summaryDB, final BisimulationRdfSummary summary, final MapDbBisimulationRdfDataset rdfDataset,
			final UriGenerator uriGenerator, final Dictionary dictionary, final Timer timer) {
		super(summaryDB, summary, rdfDataset, uriGenerator, dictionary, timer);
	}

	@Override
	public void createDataTriples() {
		log.info("Creating summary data triples...");
		timer.reset();
		timer.start();
		final Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		while (gDataNodes.hasNext()) {
			subject = gDataNodes.next();
			subjectRepresentative = summary.getSummaryDataNodeByGDataNode(subject);
			poPairs = rdfDataset.getPropertyObjectPairsBySubject(subject);
			createDataTriples(poPairs);
			poPairs = rdfDataset.getInferredPropertyObjectPairsBySubject(subject);
			createDataTriples(poPairs);
		}
		timer.stop();
		log.info(messageBuilder.append("Data triples created in: ").append(timer.getTimeAsString())); 
	}
	
	@Override
	public void createTypeTriples() {
		typeTriples = rdfDataset.getTypeTriples();
		createTypeTriples(typeTriples);
		typeTriples = rdfDataset.getInferredTypeTriples();
		createTypeTriples(typeTriples);
	}
}
