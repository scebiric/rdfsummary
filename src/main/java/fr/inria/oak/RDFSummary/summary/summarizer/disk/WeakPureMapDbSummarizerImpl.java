package fr.inria.oak.RDFSummary.summary.summarizer.disk;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.clique.MapDbCliqueRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationSummarizer;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.DataTripleCreator;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.PureSummaryTypeTripleCreator;
import fr.inria.oak.RDFSummary.timer.Timer;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureMapDbSummarizerImpl implements BisimulationSummarizer {

	private static final Logger log = Logger.getLogger(BisimulationSummarizer.class);
	
	private final DB summaryDb;
	private final WeakPureCliqueEquivalenceSummary summary;
	private final MapDbCliqueRdfDataset rdfDataset;
	private final DataTripleCreator dataTripleCreator;
	private final PureSummaryTypeTripleCreator typeTripleCreator;
	private final Timer totalTimer;
	private final Timer dataTimer;
	private final Timer typesTimer;
	
	private final MessageBuilder messageBuilder;
	
	private static final String TYPED_ONLY_DATA_NODES = "typed_only_data_nodes";
	private static final String CLASSES_OF_TYPED_ONLY_DATA_NODES = "classes_of_typed_only_data_nodes";
	
	/**
	 * @param summaryDb
	 * @param summary
	 * @param rdfDataset
	 * @param dataTripleCreator
	 * @param typeTripleCreator
	 * @param totalTimer;
	 * @param dataTimer
	 * @param typesTimer
	 */
	public WeakPureMapDbSummarizerImpl(final DB summaryDb, final WeakPureCliqueEquivalenceSummary summary, final MapDbCliqueRdfDataset rdfDataset, 
			final DataTripleCreator dataTripleCreator, final PureSummaryTypeTripleCreator typeTripleCreator, 
			final Timer totalTimer, final Timer dataTimer, final Timer typesTimer) {
		this.summaryDb = summaryDb;
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.dataTripleCreator = dataTripleCreator;
		this.typeTripleCreator = typeTripleCreator;
		this.totalTimer = totalTimer;
		this.dataTimer = dataTimer;
		this.typesTimer = typesTimer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void summarize() throws SQLException {
		totalTimer.reset();
		totalTimer.start();		
		log.info(getSummaryTypeMessage(SummaryType.WEAK));
		summarizeDataTriples();
		summarizeTypeTriples();
		summary.unifyDataNodes();				
		totalTimer.stop();
		
		log.info(messageBuilder.append("Total summarization time: ").append(totalTimer.getTimeAsString()));
	}

	private void summarizeDataTriples() throws SQLException {
		log.info("Summarizing data triples..."); 
		dataTimer.reset();
		dataTimer.start();
		final Iterator<Tuple3<Integer, Integer, Integer>> dataTriples = rdfDataset.getDataTriples();
		Tuple3<Integer, Integer, Integer> dataTriple = null;
		while (dataTriples.hasNext()) {
			dataTriple = dataTriples.next();
			dataTripleCreator.representDataTriple(dataTriple.a.intValue(), dataTriple.b.intValue(), dataTriple.c.intValue());
		}
		dataTimer.stop();
		log.info(messageBuilder.append("Data triples summarized in: ").append(dataTimer.getTimeAsString()));
	}
	
	private void summarizeTypeTriples() throws SQLException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkNotNull(rdfDataset);
		
		log.info("Summarizing type triples..."); 
		typesTimer.reset();
		typesTimer.start();
		final Iterator<Tuple2<Integer, Integer>> typeTriples = rdfDataset.getTypeTriples();
		Tuple2<Integer, Integer> typeTriple = null;
		final Set<Integer> typedOnlyDataNodes = summaryDb.createHashSet(TYPED_ONLY_DATA_NODES).counterEnable().make();
		final Set<Integer> classesOfTypedOnlyDataNodes = summaryDb.createHashSet(CLASSES_OF_TYPED_ONLY_DATA_NODES).counterEnable().make();
		int subject;
		int classIRI;
		while (typeTriples.hasNext()) {			
			typeTriple = typeTriples.next();
			subject = typeTriple.a.intValue();
			classIRI = typeTriple.b.intValue();
			boolean represented = typeTripleCreator.representTypeTriple(subject, classIRI);
			if (!represented) {
				// The subject is a data node and it has only type properties
				typedOnlyDataNodes.add(subject);
				classesOfTypedOnlyDataNodes.add(classIRI);
			}
		}
	
		if (typedOnlyDataNodes.size() > 0) {
			log.info("Representing typed-only resources...");
			typeTripleCreator.representTypeTriples(typedOnlyDataNodes.iterator(), classesOfTypedOnlyDataNodes.iterator());
		} 
		typesTimer.stop();
		log.info(messageBuilder.append("Type triples summarized in: ").append(typesTimer.getTimeAsString()));
	}
	
	private String getSummaryTypeMessage(final String summaryType) {
		messageBuilder.append("Building the ").append(summaryType).append(" summary...");
	
		return messageBuilder.toString();
	}
}
