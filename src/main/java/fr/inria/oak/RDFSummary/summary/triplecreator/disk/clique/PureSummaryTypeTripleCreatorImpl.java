package fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique;

import java.sql.SQLException;
import java.util.Iterator;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.PureCliqueEquivalenceSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PureSummaryTypeTripleCreatorImpl implements PureSummaryTypeTripleCreator {

	private final PureCliqueEquivalenceSummary summary;
	private final RdfDataset rdfDataset;

	/**
	 * @param summary
	 * @param rdfDataset
	 */
	public PureSummaryTypeTripleCreatorImpl(final PureCliqueEquivalenceSummary summary, final RdfDataset rdfDataset) {
		this.summary = summary;
		this.rdfDataset = rdfDataset;
	}

	/**
	 * Returns true if the type triple has been represented in the summary, otherwise false.
	 * 
	 * Assumes that data triples have already been summarized.
	 * Therefore, if gNode is a data node and has some data properties in G, then it has already been represented by 
	 * a data node in the summary when summarizing data triples.
	 * Otherwise, gNode doesn't have any data properties in G,
	 * i.e. it is typed-only, and will be represented later, thus the method returns false. 
	 * 
	 * If the type triple from G should be represented,
	 * a corresponding type triple is created in the summary, with the representative of gNode
	 * as the source and the specified class IRI as the target.
	 * If gNode is a class or a property, it is represented by itself.
	 *  
	 * The method ensures that the summary type triples are unique.
	 * 
	 * Same as for the strong summary. 
	 * 
	 */
	@Override
	public boolean representTypeTriple(final int gNode, final int classIRI) {
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkArgument(classIRI > 0);

		if (rdfDataset.isClassOrProperty(gNode)) {
			summary.createTypeTriple(gNode, classIRI); // uniqueness of type triples ensured by MapDB

			return true;
		}
		else {
			Integer sDataNode = summary.getSummaryDataNodeByGDataNode(gNode);
			if (sDataNode == null) 
				return false;

			summary.createTypeTriple(sDataNode, classIRI);

			return true;
		}
	}

	/**
	 * A single data node is created, representing all gDataNodes and having all the specified classes.
	 * @throws SQLException 
	 * @
	 * 
	 */
	@Override
	public void representTypeTriples(final Iterator<Integer> gDataNodes, final Iterator<Integer> classes) throws SQLException {
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.hasNext());
		Preconditions.checkArgument(gDataNodes.hasNext());

		int sDataNode = summary.createDataNode(gDataNodes);
		// Uniqueness: Since the data node is newly created and the classes in the specified set are unique there couldn't have
		// already existed a type edge to any given class
		summary.createTypeTriples(sDataNode, classes);
	}

}
