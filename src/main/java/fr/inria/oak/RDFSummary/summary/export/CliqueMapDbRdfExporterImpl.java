package fr.inria.oak.RDFSummary.summary.export;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.rdf.RdfProperty;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.summary.export.dot.DotSummaryViz;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.CliqueRdfSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.DotUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class CliqueMapDbRdfExporterImpl implements RdfExporter {

	private static final Logger log = Logger.getLogger(RdfExporter.class);

	private final String summaryDbName;
	private final MapDbManager dbManager;
	private final CliqueRdfSummary summary;
	private final RdfDataset rdfDataset;
	private final Dictionary dictionary;	
	private final DotSummaryViz dotViz;
	private final TriplesWriter triplesWriter;
	private final Timer timer;

	private static final String DOT_NODES = "dot_nodes";
	private static final String SHORT_TYPE = "rdf:type";

	/**
	 * @param summaryDbName
	 * @param dbManager
	 * @param summary
	 * @param rdfDataset
	 * @param dictionary
	 * @param dotViz
	 * @param triplesWriter
	 * @param timer
	 */
	public CliqueMapDbRdfExporterImpl(final String summaryDbName, final MapDbManager dbManager, 
			final CliqueRdfSummary summary, final RdfDataset rdfDataset, 
			final Dictionary dictionary, 
			final DotSummaryViz dotViz, final TriplesWriter triplesWriter, final Timer timer) {
		this.summaryDbName = summaryDbName;
		this.dbManager = dbManager;
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.dictionary = dictionary;
		this.dotViz = dotViz;
		this.triplesWriter = triplesWriter;
		this.timer = timer;
	}

	@Override
	public ExportResult exportToFiles(final ExportParams exportParams, final String datasetName, final String summaryName) throws IOException, SQLException {
		Preconditions.checkNotNull(exportParams);
		Preconditions.checkArgument(exportParams.generateDot() || exportParams.exportToRdfFile());
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(datasetName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryName));

		timer.reset();
		timer.start();
		log.info("Exporting decoded summary triples to files...");
		Utils.mkDirs(exportParams.getOutputFolder());		
		final String dotFilepath = Utils.getFilepath(exportParams.getOutputFolder(), summaryName, Extension.DOT);
		final String rdfFilepath = Utils.getFilepath(exportParams.getOutputFolder(), summaryName, Extension.N_TRIPLES);

		DotStyleParams dotParams = null;
		if (exportParams.generateDot()) {
			dotParams = exportParams.getDotStyleParams();		

			// Start DOT file
			String graphLabel = "";
			if (dotParams.showGraphLabel()) 
				graphLabel = dotViz.getGraphLabel(datasetName, null);

			dotViz.startDotGraph(dotFilepath, summaryName, graphLabel);
		}

		if (exportParams.exportToRdfFile()) {
			// Start n-triples file
			triplesWriter.startFile(rdfFilepath);
		}

		final Set<Integer> dotNodes = dbManager.createEmptyHashSet(summaryDbName, DOT_NODES);

		// Write data triples
		int subject;
		int property;
		int object;
		String decodedProperty;		
		String edgeLabel;
		String edgeColor = null;
		String edgeStyle = null;		
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getDataPropertyEdgeColor());;
			edgeStyle = dotParams.getDataPropertyEdgeStyle();
		}

		final Iterator<Tuple3<Integer, Integer, Integer>> dataTriples = summary.getAllDataTriples();
		Tuple3<Integer, Integer, Integer> dataTriple;
		while (dataTriples.hasNext()) {
			dataTriple = dataTriples.next();
			subject = dataTriple.a.intValue();
			property = dataTriple.b.intValue();
			object = dataTriple.c.intValue();

			if (exportParams.generateDot()) {				
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				edgeLabel = dotViz.getEdgeLabel(stripNamespace(property));
				dotViz.writeToFile(dotViz.getDotTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), edgeColor, edgeStyle, edgeLabel));
			}

			if (exportParams.exportToRdfFile()) {
				triplesWriter.write(StringUtils.addAngleBrackets(dictionary.getValue(subject)), StringUtils.addAngleBrackets(dictionary.getValue(property)), StringUtils.addAngleBrackets(dictionary.getValue(object)));
			}	
		}	

		// Write type triples
		edgeLabel = "\"\"";
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getTypeEdgeColor());
			edgeStyle = dotParams.getTypeEdgeStyle();
		}

		final Iterator<Tuple2<Integer, Integer>> typeTriples = summary.getAllTypeTriples();
		Tuple2<Integer, Integer> typeTriple;
		while (typeTriples.hasNext()) {
			typeTriple = typeTriples.next();
			subject = typeTriple.a.intValue();				
			object = typeTriple.b.intValue();

			if (exportParams.generateDot()) {				
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				dotViz.writeToFile(dotViz.getDotTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), edgeColor, edgeStyle, edgeLabel));
			}

			if (exportParams.exportToRdfFile()) {				
				triplesWriter.write(StringUtils.addAngleBrackets(dictionary.getValue(subject)), Rdf.FULL_TYPE, StringUtils.addAngleBrackets(dictionary.getValue(object)));
			}	
		}

		// Write schema triples
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getSchemaEdgeColor());
		}
		
		final Iterator<Tuple3<Integer, Integer, Integer>> schemaTriples = rdfDataset.getSchema().getAllTriples();
		Tuple3<Integer, Integer, Integer> schemaTriple;
		while (schemaTriples.hasNext()) {
			schemaTriple = schemaTriples.next();
			subject = schemaTriple.a.intValue();
			property = schemaTriple.b.intValue();
			object = schemaTriple.c.intValue();

			decodedProperty = dictionary.getValue(property);
			
			if (exportParams.generateDot()) {
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				dotViz.writeToFile(dotViz.getSchemaTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), decodedProperty, edgeColor, dotParams.getSchemaLabelColor()));
			}

			if (exportParams.exportToRdfFile()) {								 
				triplesWriter.write(StringUtils.addAngleBrackets(dictionary.getValue(subject)), StringUtils.addAngleBrackets(decodedProperty), StringUtils.addAngleBrackets(dictionary.getValue(object)));
			}
		}		

		if (exportParams.generateDot()) 
			dotViz.endDotGraph();

		if (exportParams.exportToRdfFile())
			triplesWriter.endFile();

		timer.stop();
		if (exportParams.generateDot() || exportParams.exportToRdfFile())
			log.info("Decoded summary triples exported to file(s) in " + timer.getTimeAsString());
		if (exportParams.generateDot())
			log.info("Summary DOT filepath: " + dotFilepath);
		if (exportParams.exportToRdfFile())
			log.info("Summary RDF filepath: " + rdfFilepath);

		if (exportParams.generateDot() && exportParams.convertDotToPdf())
			Utils.dotToPdf(dotFilepath, exportParams.getDotExecutableFilepath());

		if (exportParams.generateDot() && exportParams.convertDotToPng())
			Utils.dotToPng(dotFilepath, exportParams.getDotExecutableFilepath());
		
		final ExportResult exportResult = new ExportResult();
		if (exportParams.generateDot())
			exportResult.setDotFilepath(dotFilepath);
		if (exportParams.exportToRdfFile())
			exportResult.setRdfFilepath(rdfFilepath);
		
		return exportResult;
	}
	
	private void writeDotNodeIfNotExists(final int summaryNode, final Set<Integer> dotNodes, final DotStyleParams dotParams) throws SQLException {
		if (!dotNodes.contains(summaryNode)) { 
			dotViz.writeToFile(getDotNodeEntry(summaryNode, stripNamespace(summaryNode), dotParams));
			dotNodes.add(summaryNode);
		}		
	}

	private String stripNamespace(final int key) throws SQLException {
		String str = dictionary.getValue(key);
		
		if (StringUtils.isNullOrBlank(str))
			throw new IllegalArgumentException("Invalid key: " + key);
	
		if (str.equals(RdfProperty.FULL_TYPE))
			return SHORT_TYPE;

		int index = str.lastIndexOf(Chars.HASH);
		if (index == -1) {
			index = str.lastIndexOf(Chars.SLASH);
			if (index == -1)
				return str;
		}

		return str.substring(index+1, str.length());
	}

	/**
	 * @param nodeId
	 * @param label
	 * @param dotParams
	 * @return Dot entry for the node based on whether it is a data, class, property or class and property node.
	 */
	private String getDotNodeEntry(int nodeId, String label, DotStyleParams dotParams) {
		if (!rdfDataset.isClassOrProperty(nodeId)) {
			if (!label.startsWith(Constant.DATA_NODE_PREFIX)) 
				throw new IllegalArgumentException("Invalid data node: " + nodeId + ". Data node names must start with the data node prefix: " + Constant.DATA_NODE_PREFIX);

			// Data node
			return dotViz.getDotNode(nodeId, label, dotParams.getDataNodeShape(), dotParams.getDataNodeStyle(), dotParams.getDataNodeColor());
		}
		else if (rdfDataset.isClass(nodeId) && !rdfDataset.isProperty(nodeId)) {
			// Class node
			return dotViz.getDotNode(nodeId, label, dotParams.getClassNodeShape(), dotParams.getClassNodeStyle(), dotParams.getClassNodeColor());
		}
		else if (rdfDataset.isProperty(nodeId) && !rdfDataset.isClass(nodeId)) {
			// Property node
			return dotViz.getDotNode(nodeId, label, dotParams.getPropertyNodeShape(), dotParams.getPropertyNodeStyle(), dotParams.getPropertyNodeColor());
		}

		// Class and property node
		return dotViz.getDotNode(nodeId, label, dotParams.getClassAndPropertyNodeShape(), dotParams.getClassAndPropertyNodeStyle(), dotParams.getClassAndPropertyNodeColor()); 
	}


}
