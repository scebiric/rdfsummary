package fr.inria.oak.RDFSummary.summary.export;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.ColumnArray;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.summary.export.dot.DotSummaryViz;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.DotUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class TroveSummaryExporterImpl implements TroveSummaryExporter {

	private static final Logger log = Logger.getLogger(TroveSummaryExporter.class);

	private static DotSummaryViz DotSummaryViz;
	private static TriplesWriter TriplesWriter;
	private final SqlConnectionHandler connHandler;
	private final RdfTablesDao rdfTablesDao;
	private final DictionaryDao dictDao;
	private static DdlDao DdlDao;
	private static DmlDao DmlDao;
	private static DmlQueries DmlQueries;
	private static Timer Timer;

	private ResultSet resultSet = null;	

	/**
	 * Constructor of {@link TroveSummaryExporterImpl}
	 * 
	 * @param dotSummaryViz
	 * @param triplesWriter
	 * @param connHandler
	 * @param rdfTablesDao
	 * @param dictDao
	 * @param ddlDao
	 * @param dmldao
	 * @param dmlQueries
	 * @param timer
	 */
	public TroveSummaryExporterImpl(final DotSummaryViz dotSummaryViz, final TriplesWriter triplesWriter,
			final SqlConnectionHandler connHandler, final RdfTablesDao rdfTablesDao, final DictionaryDao dictDao, final DdlDao ddlDao, final DmlDao dmldao,
			final DmlQueries dmlQueries, final Timer timer) {
		DotSummaryViz = dotSummaryViz;
		TriplesWriter = triplesWriter;
		this.connHandler = connHandler;
		this.rdfTablesDao = rdfTablesDao;
		this.dictDao = dictDao;
		DdlDao = ddlDao;
		DmlDao = dmldao;
		DmlQueries = dmlQueries;
		Timer = timer;
	}

	@Override
	public void exportRdfSummaryToPostgres(final RdfSummary summary, final PostgresSummarizerParams params, final PostgresTableNames storageLayout) throws SQLException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(summary instanceof RdfSummary);
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(storageLayout);

		log.info("Exporting summary to PostgreSQL...");
		Timer.reset();
		Timer.start();
		final String qualifiedEncodedSummaryName = NameUtils.getQualifiedEncodedSummaryTableName(storageLayout.getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		final String summaryEncodedDataTable = this.createEmptyEncodedDataTable(qualifiedEncodedSummaryName, storageLayout);
		final String summaryEncodedTypesTable = this.createEmptyEncodedTypesTable(qualifiedEncodedSummaryName, storageLayout);

		String encReprTable = null;
		if (params.getExportParams().exportRepresentationTables()) {
			encReprTable = this.createEmptyEncodedRepresentationTable(qualifiedEncodedSummaryName);
			this.insertEncodedRepresentationMappings(encReprTable, ColumnArray.ENC_REPRESENTATION_TABLE, summary);
		}

		connHandler.disableAutoCommit();
		this.insertEncodedSummaryTypeTriples(summaryEncodedTypesTable, summary, params);
		this.insertEncodedSummaryDataTriples(summaryEncodedDataTable, summary, params);
		this.exportAllEncodedSummaryTriplesToTable(qualifiedEncodedSummaryName, summaryEncodedDataTable, summaryEncodedTypesTable);
		connHandler.enableAutoCommit();

		final String qualifiedDecodedSummaryName = NameUtils.underscoreConcat(storageLayout.getTriplesTable(), params.getSummarizationParams().getSummaryType());
		this.decodeSummaryTriples(qualifiedDecodedSummaryName, params.getSummarizationParams().getDictionaryTable(), storageLayout);
		if (params.getExportParams().exportRepresentationTables())
			this.decodeRepresentationTable(encReprTable, qualifiedDecodedSummaryName, params.getSummarizationParams().getDictionaryTable());

		Timer.stop();

		log.info("Summary exported to PostgreSQL in " + Timer.getTimeAsString());
	}

	/**
	 * @param encReprTable
	 * @param reprColumns
	 * @param summary
	 * @throws SQLException
	 */
	private void insertEncodedRepresentationMappings(final String encReprTable, final String[] reprColumns, final RdfSummary summary) throws SQLException {
		log.info("Inserting representation mappings...");
		final PreparedStatement insertPrepStatement = connHandler.prepareStatement(DmlQueries.insertValuesPreparedStatement(encReprTable, reprColumns));
		final TIntSet sDataNodes = summary.getSummaryDataNodes();
		TIntSet representedNodes = null;
		int sNode = -1;
		final TIntIterator iterator = sDataNodes.iterator();
		while (iterator.hasNext()) {
			sNode = iterator.next();
			representedNodes = summary.getRepresentedInputDataNodes(sNode);
			rdfTablesDao.insertToEncodedRepresentationTable(insertPrepStatement, encReprTable, reprColumns, representedNodes, sNode);
		}

		insertPrepStatement.close();
	}

	private String createEmptyEncodedDataTable(final String qualifiedSummaryName, final PostgresTableNames storageLayout) throws SQLException {
		log.info("Creating empty encoded data table...");
		final String summaryEncodedDataTable = NameUtils.getEncodedSummaryDataTableName(qualifiedSummaryName); 
		DdlDao.dropTableIfExists(summaryEncodedDataTable);
		rdfTablesDao.createEncodedTriplesTableWithId(summaryEncodedDataTable);
		storageLayout.setEncodedSummaryDataTable(summaryEncodedDataTable);

		return summaryEncodedDataTable;
	}

	private String createEmptyEncodedTypesTable(final String qualifiedSummaryName, final PostgresTableNames storageLayout) throws SQLException {
		log.info("Creating empty encoded types table...");
		final String summaryEncodedTypesTable = NameUtils.getEncodedSummaryTypesTableName(qualifiedSummaryName);
		DdlDao.dropTableIfExists(summaryEncodedTypesTable);
		rdfTablesDao.createEncodedTypesTableWithId(summaryEncodedTypesTable);
		storageLayout.setEncodedSummaryTypesTable(summaryEncodedTypesTable);

		return summaryEncodedTypesTable;
	}

	private String createEmptyEncodedRepresentationTable(final String qualifiedSummaryName) throws SQLException {
		log.info("Creating empty encoded representation table...");
		final String encReprTable = NameUtils.getRepresentationTableName(qualifiedSummaryName); 
		DdlDao.dropTableIfExists(encReprTable);
		rdfTablesDao.createEncodedRepresentationTable(encReprTable, Constant.INPUT_DATA_NODE, Constant.SUMMARY_DATA_NODE);

		return encReprTable;
	}

	/**
	 * @param summaryEncodedTypesTable
	 * @param summary
	 * @param params
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws SQLException
	 * 
	 */
	private void insertEncodedSummaryTypeTriples(final String summaryEncodedTypesTable, final RdfSummary summary, final PostgresSummarizerParams params) throws DictionaryException, InexistentValueException, SQLException {
		log.info("Inserting encoded summary type triples...");	
		TIntIterator classesIterator = null;
		TIntSet classes = null;
		int[] values = new int[2];

		TIntObjectMap<TIntSet> classesForSummaryNodeMap = summary.getClassesForSummaryNodeMap();
		for (int sNode : classesForSummaryNodeMap.keys()) {
			classes = classesForSummaryNodeMap.get(sNode);
			if (classes != null) {
				classesIterator = classes.iterator();
				while (classesIterator.hasNext()) {
					values[0] = sNode;
					values[1] = classesIterator.next();
					DmlDao.insert(summaryEncodedTypesTable, ColumnArray.TYPE_TRIPLES, values);
				}
			}
		}
	}

	/**
	 * @param summaryEncodedDataTable 
	 * @param summary
	 * @param params
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws SQLException
	 */
	private void insertEncodedSummaryDataTriples(final String summaryEncodedDataTable, final RdfSummary summary, final PostgresSummarizerParams params) throws DictionaryException, InexistentValueException, SQLException {
		log.info("Inserting encoded summary data triples...");
		TIntObjectMap<Collection<DataTriple>> dataTriplesForPropertyMap = summary.getDataTriplesForDataPropertyMap();
		Collection<DataTriple> dataTriples = null;
		int[] tripleAtoms = new int[3];
		for (int dataProperty : dataTriplesForPropertyMap.keys()) {
			dataTriples = dataTriplesForPropertyMap.get(dataProperty);
			for (DataTriple dataTriple : dataTriples) {
				tripleAtoms[0] = dataTriple.getSubject();
				tripleAtoms[1] = dataTriple.getDataProperty();
				tripleAtoms[2] = dataTriple.getObject();
				DmlDao.insert(summaryEncodedDataTable, ColumnArray.TRIPLES, tripleAtoms); 
			}
		}
	}

	/**
	 * Creates a single table comprising both data and type encoded summary triples
	 * 
	 * @param qualifiedEncodedSummaryName
	 * @param summaryEncodedDataTable
	 * @param summaryEncodedTypesTable
	 * @throws SQLException 
	 */
	private void exportAllEncodedSummaryTriplesToTable(final String encodedSummaryTable, final String summaryEncodedDataTable, final String summaryEncodedTypesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryEncodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryEncodedTypesTable));

		DdlDao.dropTableIfExists(encodedSummaryTable);
		rdfTablesDao.createEncodedTriplesTableWithId(encodedSummaryTable);

		final int typePropertyKey = dictDao.getExistingOrNewKey(Rdf.FULL_TYPE);
		rdfTablesDao.populateEncodedSummaryTable(encodedSummaryTable, summaryEncodedDataTable, summaryEncodedTypesTable, typePropertyKey);
	}

	/**
	 * Recreates a decoded representation table based on the encoded one and the dictionary table
	 * 
	 * @param encReprTable
	 * @param qualifiedSummaryName
	 * @param datasetDictionaryTable
	 * @throws SQLException
	 */
	private void decodeRepresentationTable(final String encReprTable, final String qualifiedSummaryName, final String datasetDictionaryTable) throws SQLException {
		String reprTable = NameUtils.getRepresentationTableName(qualifiedSummaryName);
		DdlDao.dropTableIfExists(reprTable);
		rdfTablesDao.createDecodedRepresentationTable(reprTable, encReprTable, datasetDictionaryTable);
	}

	/**
	 * Decodes the encoded summary data and types tables by joining them with the dictionary table in Postgres.
	 * The decoded tables' names are then saved in the storage layout.
	 * Before decoding, if the decoded tables already exist they will be dropped.
	 * 
	 * @param qualifiedSummaryName
	 * @param datasetDictionaryTable
	 * @param summaryType 
	 * @param storageLayout
	 * @throws SQLException
	 */
	private void decodeSummaryTriples(final String qualifiedSummaryName, final String datasetDictionaryTable, final PostgresTableNames storageLayout) throws SQLException {
		log.info("Decoding summary data and type triples in PostgreSQL...");
		final String summaryDataTable = NameUtils.underscoreConcat(qualifiedSummaryName, Constant.DATA);
		final String summaryTypesTable = NameUtils.underscoreConcat(qualifiedSummaryName, Constant.TYPES);
		String summaryTable = qualifiedSummaryName;

		DdlDao.dropTableIfExists(summaryDataTable);
		DdlDao.dropTableIfExists(summaryTypesTable);
		DdlDao.dropTableIfExists(summaryTable);
		rdfTablesDao.decodeSummaryData(summaryDataTable, storageLayout.getEncodedSummaryDataTable(), datasetDictionaryTable);
		rdfTablesDao.decodeSummaryTypes(summaryTypesTable, storageLayout.getEncodedSummaryTypesTable(), datasetDictionaryTable);
		rdfTablesDao.createDecodedSummaryTable(summaryTable, summaryDataTable, summaryTypesTable);

		storageLayout.setSummaryDataTable(summaryDataTable);
		storageLayout.setSummaryTypesTable(summaryTypesTable);
	}

	/**
	 * Encodes each summary data node.
	 * Loads decoded summary triples from PostgreSQL and writes them to a .dot file and/or n-triples file.
	 * Only one triple from PostgreSQL is loaded to memory at a time.
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws DictionaryException 
	 * @throws InexistentValueException 
	 *  
	 */
	@Override
	public void exportDecodedTriplesToFiles(final PostgresSummarizerParams params, final RdfSummary summary, final Schema schema, 
			final Stats stats, final PostgresTableNames storageLayout, final String dotFilepath, final String graphName, final String rdfFilepath) throws ConfigurationException, ParametersException, SQLException, IOException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(stats);
		Preconditions.checkNotNull(schema);
		Preconditions.checkArgument(params.getExportParams().generateDot() || params.getExportParams().exportToRdfFile());

		Timer.reset();
		Timer.start();

		log.info("Exporting decoded summary triples to files...");
		DotStyleParams dotParams = null;
		if (params.getExportParams().generateDot()) {
			dotParams = params.getExportParams().getDotStyleParams();		

			// Start DOT file
			String graphLabel = "";
			if (dotParams.showGraphLabel()) 
				graphLabel = DotSummaryViz.getGraphLabel(params.getSummarizationParams().getSchemaName(), stats);

			DotSummaryViz.startDotGraph(dotFilepath, graphName, graphLabel);
		}

		if (params.getExportParams().exportToRdfFile()) {
			// Start n-triples file
			TriplesWriter.startFile(rdfFilepath);
		}

		// Write nodes
		if (params.getExportParams().generateDot()) {
			writeNodesToDot(summary.getSummaryDataNodes(), dotParams, summary);
			writeNodesToDot(summary.getClassNodes(), dotParams, summary);
			writeNodesToDot(summary.getPropertyNodes(), dotParams, summary); 
		}

		// Write data triples
		int source = -1;
		int target = -1;
		String edgeLabel = null;
		String edgeColor = null;
		String edgeStyle = null;
		if (params.getExportParams().generateDot()) {
			edgeColor = DotSummaryViz.getEdgeColor(dotParams.getDataPropertyEdgeColor());
			edgeStyle = dotParams.getDataPropertyEdgeStyle();
		}

		resultSet = DmlDao.getSelectionResultSet(ColumnArray.TRIPLES, false, storageLayout.getEncodedSummaryDataTable(), null);
		while (resultSet.next()) {
			if (params.getExportParams().generateDot()) {
				source = resultSet.getInt(1);
				target = resultSet.getInt(3);
				edgeLabel = DotSummaryViz.getEdgeLabel(stripNamespace(resultSet.getInt(2)));
				DotSummaryViz.writeToFile(DotSummaryViz.getDotTriple(DotUtils.getDotNodeId(source), DotUtils.getDotNodeId(target), edgeColor, edgeStyle, edgeLabel));
			}

			if (params.getExportParams().exportToRdfFile()) {
				TriplesWriter.write(dictDao.getValue(resultSet.getInt(1)), dictDao.getValue(resultSet.getInt(2)), dictDao.getValue(resultSet.getInt(3)));
			}	
		}
		resultSet.close();

		// Write type triples
		edgeLabel = "\"\"";
		if (params.getExportParams().generateDot()) {
			edgeColor = DotSummaryViz.getEdgeColor(dotParams.getTypeEdgeColor());
			edgeStyle = dotParams.getTypeEdgeStyle();
		}
		resultSet = DmlDao.getSelectionResultSet(ColumnArray.TYPE_TRIPLES, false, storageLayout.getEncodedSummaryTypesTable(), null);
		while (resultSet.next()) {
			if (params.getExportParams().generateDot()) {
				source = resultSet.getInt(1);
				target = resultSet.getInt(2);
				DotSummaryViz.writeToFile(DotSummaryViz.getDotTriple(DotUtils.getDotNodeId(source), DotUtils.getDotNodeId(target), edgeColor, edgeStyle, edgeLabel));
			}

			if (params.getExportParams().exportToRdfFile()) {
				TriplesWriter.write(dictDao.getValue(resultSet.getInt(1)), Rdf.FULL_TYPE, dictDao.getValue(resultSet.getInt(2)));
			}	
		}
		resultSet.close();

		// Write schema triples
		if (params.getExportParams().generateDot()) {
			edgeColor = DotSummaryViz.getEdgeColor(dotParams.getSchemaEdgeColor());
		}
		final Set<Triple> schemaTriples = schema.getAllTriples();
		if (schemaTriples.size() > 0) {
			final Dictionary schemaDictionary = dictDao.loadDictionaryForConstants(schema.getConstants());
			for (Triple schemaTriple : schemaTriples) {
				if (params.getExportParams().generateDot()) {
					source = schemaDictionary.getKey(schemaTriple.subject.toString());
					target = schemaDictionary.getKey(schemaTriple.object.toString());
					DotSummaryViz.writeToFile(DotSummaryViz.getSchemaTriple(DotUtils.getDotNodeId(source), DotUtils.getDotNodeId(target), schemaTriple.property.toString(), edgeColor, dotParams.getSchemaLabelColor()));
				}

				if (params.getExportParams().exportToRdfFile())
					TriplesWriter.write(schemaTriple.subject.toString(), schemaTriple.property.toString(), schemaTriple.object.toString());
			}
		}

		if (params.getExportParams().generateDot()) 
			DotSummaryViz.endDotGraph();

		if (params.getExportParams().exportToRdfFile())
			TriplesWriter.endFile();

		Timer.stop();
		if (params.getExportParams().generateDot() || params.getExportParams().exportToRdfFile())
			log.info("Decoded summary triples exported to file(s) in " + Timer.getTimeAsString());
		if (params.getExportParams().generateDot())
			log.info("Summary DOT filepath: " + dotFilepath);
		if (params.getExportParams().exportToRdfFile())
			log.info("Summary RDF filepath: " + rdfFilepath);

		if (params.getExportParams().generateDot() && params.getExportParams().convertDotToPdf())
			Utils.dotToPdf(dotFilepath, params.getExportParams().getDotExecutableFilepath());

		if (params.getExportParams().generateDot() && params.getExportParams().convertDotToPng())
			Utils.dotToPng(dotFilepath, params.getExportParams().getDotExecutableFilepath());
	}

	/**
	 * @param nodes
	 * @param dotParams
	 * @param summary
	 * @throws SQLException
	 */
	private void writeNodesToDot(TIntSet nodes, DotStyleParams dotParams, RdfSummary summary) throws SQLException {
		final TIntIterator iterator = nodes.iterator();
		int node = -1;
		while (iterator.hasNext()) {	 
			node = iterator.next();
			DotSummaryViz.writeToFile(getDotNodeEntry(node, getDotNodeLabel(node), dotParams, summary));
		}
	}

	/**
	 * @param propertyKey
	 * @return The decoded value of propertyKey, stripped of its namespace if it has one.
	 * @throws SQLException
	 */
	private String stripNamespace(int propertyKey) throws SQLException {
		return RdfUtils.stripNamespace(dictDao.getValue(propertyKey));
	}

	/**
	 * @param nodeKey
	 * @return The decoded value of nodeKey, stripped of its namespace if it has one.
	 * @throws SQLException
	 * 
	 */
	private String getDotNodeLabel(int nodeKey) throws SQLException {
		return RdfUtils.stripNamespace(dictDao.getValue(nodeKey));
	}

	/**
	 * @param nodeId
	 * @param label
	 * @param dotParams
	 * @param summary
	 * @return Dot entry for the node based on whether it is a data, class, property or class and property node.
	 * 
	 */
	private String getDotNodeEntry(int nodeId, String label, DotStyleParams dotParams, RdfSummary summary) {
		if (!summary.isClassOrPropertyNode(nodeId)) {
			if (!label.startsWith(Constant.DATA_NODE_PREFIX)) 
				throw new IllegalArgumentException("Invalid data node: " + nodeId + ". Data node names must start with the data node prefix: " + Constant.DATA_NODE_PREFIX);

			// Data node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getDataNodeShape(), dotParams.getDataNodeStyle(), dotParams.getDataNodeColor());
		}
		else if (summary.isClassNode(nodeId) && !summary.isPropertyNode(nodeId)) {
			// Class node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getClassNodeShape(), dotParams.getClassNodeStyle(), dotParams.getClassNodeColor());
		}
		else if (summary.isPropertyNode(nodeId) && !summary.isClassNode(nodeId)) {
			// Property node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getPropertyNodeShape(), dotParams.getPropertyNodeStyle(), dotParams.getPropertyNodeColor());
		}

		// Class and property node
		return DotSummaryViz.getDotNode(nodeId, label, dotParams.getClassAndPropertyNodeShape(), dotParams.getClassAndPropertyNodeStyle(), dotParams.getClassAndPropertyNodeColor()); 
	}
}
