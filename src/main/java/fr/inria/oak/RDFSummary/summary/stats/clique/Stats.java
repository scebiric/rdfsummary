package fr.inria.oak.RDFSummary.summary.stats.clique;

import fr.inria.oak.RDFSummary.summary.stats.NodeStats;
import fr.inria.oak.RDFSummary.summary.stats.TripleStats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Stats {
	
	private final NodeStats summaryNodeStats;
	private final TripleStats summaryTripleStats;
	private final TripleStats inputGraphTripleStats;
	private final int distinctDataPropertiesCount;
	private final int distinctClassesCount;
	private final long summarizationTime;
		
	/**
	 * @param summaryNodeStats
	 * @param summaryTripleStats
	 * @param inputGraphTripleStats
	 * @param distinctDataPropertiesCount
	 * @param distinctClassesCount
	 * @param summarizationTime
	 */
	public Stats(final NodeStats summaryNodeStats, TripleStats summaryTripleStats, final TripleStats inputGraphTripleStats, 
			final int distinctDataPropertiesCount, final int distinctClassesCount, final long summarizationTime) {	
		this.summaryNodeStats = summaryNodeStats;
		this.summaryTripleStats = summaryTripleStats;
		this.inputGraphTripleStats = inputGraphTripleStats;
		this.distinctDataPropertiesCount = distinctDataPropertiesCount;
		this.distinctClassesCount = distinctClassesCount;
		this.summarizationTime = summarizationTime;
	}
	
	public NodeStats getSummaryNodeStats() {
		return summaryNodeStats;
	}
	
	public TripleStats getSummaryTripleStats() {
		return summaryTripleStats;
	}
	
	public TripleStats getInputGraphTripleStats() {
		return inputGraphTripleStats;
	}
	
	public int getDistinctDataPropertiesCount() {
		return distinctDataPropertiesCount;
	}
	
	public long getSummarizationTime() {
		return summarizationTime;
	}

	public int getDistinctClassesCount() {
		return distinctClassesCount;
	}
}
