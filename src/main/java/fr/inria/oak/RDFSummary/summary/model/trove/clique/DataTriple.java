package fr.inria.oak.RDFSummary.summary.model.trove.clique;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DataTriple {

	/** Edge subject node */
	private int subject;
	
	/** Edge property */
	private final int dataProperty;
	
	/** Edge object node */
	private int object;
	
	/**
	 * Instantiates a summary edge
	 * 
	 * @param subject
	 * @param dataProperty
	 * @param object
	 */
	public DataTriple(final int subject, final int propertyIRI, final int object) {		
		this.subject = subject;
		this.dataProperty = propertyIRI;
		this.object = object;
	}
	
	/**
	 * @return The edge subject node
	 */
	public int getSubject() {
		return this.subject;
	}
	
	/**
	 * @return The edge object node
	 */
	public int getObject() {
		return this.object;
	}	
	
	/**
	 * @return The edge property
	 */
	public int getDataProperty() {
		return this.dataProperty;
	}
	
	/**
	 * Sets the subject node of the edge
	 * @param subject
	 */
	public void setSubject(final int subject) {
		this.subject = subject;
	}
	
	/**
	 * @param object
	 */
	public void setObject(final int object) {
		this.object = object;
	}

}
