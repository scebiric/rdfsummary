package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongPureCliqueEquivalenceSummaryImpl implements StrongPureCliqueEquivalenceSummary {

	private final StrongEquivalenceSummary strongEquivSummary;
	private final PureCliqueEquivalenceSummary pureCliqueEquivSummary;
	private final RdfSummary rdfSummary;
	
	/**
	 * @param strongEquivalenceSummary
	 * @param pureCliqueEquivalenceSummary
	 * @param rdfSummary
	 */
	public StrongPureCliqueEquivalenceSummaryImpl(final StrongEquivalenceSummary strongEquivalenceSummary,
			final PureCliqueEquivalenceSummary pureCliqueEquivalenceSummary,
			final RdfSummary rdfSummary) {
		this.strongEquivSummary = strongEquivalenceSummary;
		this.pureCliqueEquivSummary = pureCliqueEquivalenceSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public List<TIntSet> getSourceCliques() {
		return strongEquivSummary.getSourceCliques();
	}

	@Override
	public List<TIntSet> getTargetCliques() {
		return strongEquivSummary.getTargetCliques();
	}

	@Override
	public int getSourceCliqueIdBySubject(int subject) {
		return strongEquivSummary.getSourceCliqueIdBySubject(subject);
	}

	@Override
	public int getTargetCliqueIdByObject(int object) {
		return strongEquivSummary.getTargetCliqueIdByObject(object);
	}

	@Override
	public void setSourceCliqueIdForSubject(int subject, int sourceClique) {
		strongEquivSummary.setSourceCliqueIdForSubject(subject, sourceClique);
	}

	@Override
	public void setTargetCliqueIdForObject(int object, int targetClique) {
		strongEquivSummary.setTargetCliqueIdForObject(object, targetClique);
	}

	@Override
	public TIntSet getDataNodesForSourceCliqueId(int sourceCliqueId) {
		return strongEquivSummary.getDataNodesForSourceCliqueId(sourceCliqueId);
	}

	@Override
	public TIntSet getDataNodesForTargetCliqueId(int targetCliqueId) {
		return strongEquivSummary.getDataNodesForTargetCliqueId(targetCliqueId);
	}

	@Override
	public void storeDataNodeForSourceCliqueId(int sourceCliqueId, int dataNode) {
		strongEquivSummary.storeDataNodeForSourceCliqueId(sourceCliqueId, dataNode);
	}

	@Override
	public void storeDataNodeForTargetCliqueId(int targetCliqueId, int dataNode) {
		strongEquivSummary.storeDataNodeForTargetCliqueId(targetCliqueId, dataNode);
	}

	@Override
	public void clearSourceCliques() {
		strongEquivSummary.clearSourceCliques();
	}

	@Override
	public void clearTargetCliques() {
		strongEquivSummary.clearTargetCliques();
	}

	@Override
	public void clearCliqueMaps() {
		strongEquivSummary.clearCliqueMaps();
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target); 
	}

	@Override
	public void createTypeTriple(int sNode, int classIRI) {
		pureCliqueEquivSummary.createTypeTriple(sNode, classIRI);
	}

	@Override
	public void createTypeTriples(int sDataNode, TIntSet classes) {
		pureCliqueEquivSummary.createTypeTriples(sDataNode, classes);
	}
	
	@Override
	public boolean existsTypeTriple(int sNode, int classIRI) {
		return pureCliqueEquivSummary.existsTypeTriple(sNode, classIRI);
	}

	@Override
	public int createDataNode(TIntSet gDataNodes) throws SQLException {
		return pureCliqueEquivSummary.createDataNode(gDataNodes);
	}
	
	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
