package fr.inria.oak.RDFSummary.summary.export;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;

import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * {@link Exporter} functionalities for an integer representation of the 
 * summary based on Trove collections and an encoded input RDF dataset.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveSummaryExporter {
	
	/**
	 * Exports {@link RdfSummary} to PostgreSQL
	 * 
	 * @param summary
	 * @param params
	 * @param storageLayout
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException 
	 * @throws BerkeleyDbException 
	 */
	public void exportRdfSummaryToPostgres(RdfSummary summary, PostgresSummarizerParams params, PostgresTableNames storageLayout) throws SQLException, DictionaryException, InexistentValueException, BerkeleyDbException;
	
	/**
	 * Exports decoded summary triples to DOT and/or an RDF n-triples files
	 * 
	 * @param params
	 * @param summary
	 * @param schema
	 * @param stats
	 * @param storageLayout
	 * @param dotFilepath
	 * @param graphName
	 * @param rdfFilepath
	 * @throws ConfigurationException
	 * @throws ParametersException
	 * @throws SQLException
	 * @throws IOException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws BerkeleyDbException
	 */
	public void exportDecodedTriplesToFiles(PostgresSummarizerParams params, RdfSummary summary, Schema schema, Stats stats,
			PostgresTableNames storageLayout, final String dotFilepath, final String graphName, final String rdfFilepath) throws ConfigurationException, ParametersException, SQLException, IOException, DictionaryException, InexistentValueException, BerkeleyDbException;
}
