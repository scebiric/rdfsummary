package fr.inria.oak.RDFSummary.summary.summarizer.components.clique;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveRdfComponentSummarizer {
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(WeakPureCliqueEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(WeakTypeEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(StrongPureCliqueEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @param typesTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(StrongTypeEquivalenceSummary summary, String dataTable, String typesTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param typesTable
	 *  
	 * @throws SQLException 
	 */
	public void summarizeTypeTriples(PureCliqueEquivalenceSummary summary, String typesTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param typesTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeTypeTriples(TypeEquivalenceSummary summary, String typesTable) throws SQLException;
}
