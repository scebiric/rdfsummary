package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveRdfSummaryImpl implements RdfSummary {

	private final UriGenerator uriGenerator;
	private final DictionaryDao dictDao;
	private final TIntSet classNodes;
	private final TIntSet propertyNodes;

	/** The mapping of summary nodes and their classes */
	private final TIntObjectMap<TIntSet> classesForSNode;
	
	/** The mapping of data nodes from G and their representative summary data node */
	private final TIntIntMap sDataNodeForGDataNode;

	/** The mapping of summary data nodes and all the data nodes from G which they represent */
	private final TIntObjectMap<TIntSet> gDataNodesForSDataNode;

	/** The multimap of data edges/triples by data properties */
	private final TIntObjectMap<Collection<DataTriple>> dataTriplesForDataProperty;

	private int lastCreatedDataNode = -1;
	
	/**
	 * @param uriGenerator
	 * @param dictDao	 
	 */
	public TroveRdfSummaryImpl(final UriGenerator uriGenerator, final DictionaryDao dictDao) {
		this.uriGenerator = uriGenerator;
		this.dictDao = dictDao;
		
		classNodes = new TIntHashSet();
		propertyNodes = new TIntHashSet();
		classesForSNode = new TIntObjectHashMap<TIntSet>();
		sDataNodeForGDataNode = new TIntIntHashMap();
		gDataNodesForSDataNode = new TIntObjectHashMap<TIntSet>();
		dataTriplesForDataProperty = new TIntObjectHashMap<Collection<DataTriple>>();
	}
	
	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		DataTriple dataTriple = new DataTriple(source, dataProperty, target);
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples == null) {
			dataTriples = Lists.newArrayList();
			dataTriples.add(dataTriple);
			dataTriplesForDataProperty.put(dataProperty, dataTriples);
		}
		else
			dataTriples.add(dataTriple);
		
		return dataTriple;
	}
	
	@Override
	public boolean existsDataTriple(final int subject, final int dataProperty, final int object) {
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples != null) {
			for (DataTriple dataTriple : dataTriples) {
				if (dataTriple.getSubject() == subject && dataTriple.getObject() == object)
					return true;
			}
		}

		return false;
	}
	
	/**
	 * If the specified node is untyped, null is returned.
	 */
	@Override
	public TIntSet getClassesForSummaryNode(final int sNode) {
		return classesForSNode.get(sNode);
	}
	
	@Override
	public int getNextNodeId() throws SQLException {
		return dictDao.getHighestKey() + 1;
	}
	
	@Override
	public TIntSet getSummaryDataNodes() {
		return gDataNodesForSDataNode.keySet();		
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(final int gDataNode) { 
		return sDataNodeForGDataNode.get(gDataNode);
	}
	
	/**
	 * @return The created data node
	 * @throws SQLException
	 * @throws BerkeleyDbException 
	 */
	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		int sDataNode = dictDao.getExistingOrNewKey(uriGenerator.generateDataNodeUri(getNextNodeId()));	 
		this.representInputDataNode(gDataNode, sDataNode);
		lastCreatedDataNode = sDataNode;
		
		return sDataNode;
	}

	@Override
	public int getLastCreatedDataNode() {
		return lastCreatedDataNode;
	}

	@Override
	public void representInputDataNode(final int gDataNode, final int sDataNode) {		
		sDataNodeForGDataNode.put(gDataNode, sDataNode);
		TIntSet representedDataNodes = gDataNodesForSDataNode.get(sDataNode);
		if (representedDataNodes == null) {
			representedDataNodes = new TIntHashSet();
			representedDataNodes.add(gDataNode);
			gDataNodesForSDataNode.put(sDataNode, representedDataNodes);
		}
		else
			representedDataNodes.add(gDataNode);
	}

	@Override
	public int getSummaryDataNodeCount() {
		final TIntSet dataNodes = this.getSummaryDataNodes();
		if (dataNodes == null)
			return 0;

		return dataNodes.size();
	}

	@Override
	public int getSummaryDataNodeSupport(final int sDataNode) {
		if (!gDataNodesForSDataNode.keySet().contains(sDataNode))
			throw new IllegalArgumentException("Invalid summary data node.");
		
		return gDataNodesForSDataNode.get(sDataNode).size(); 
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(final int sDataNode) {
		if (!gDataNodesForSDataNode.keySet().contains(sDataNode))
			throw new IllegalArgumentException("Invalid summary data node.");
		
		return gDataNodesForSDataNode.get(sDataNode);
	}
	
	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return dataTriplesForDataProperty;
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(final int dataProperty) {
		return dataTriplesForDataProperty.get(dataProperty);
	}
	
	@Override
	public void addClassNode(int iri) {
		classNodes.add(iri);
	}
	
	@Override
	public void addPropertyNode(int iri) {
		propertyNodes.add(iri);
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return classNodes.contains(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return propertyNodes.contains(iri);
	}
	
	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return isClassNode(iri) || isPropertyNode(iri);		
	}

	@Override
	public void setLastCreatedDataNode(final int dataNode) {
		lastCreatedDataNode = dataNode;
	}

	/**
	 * Removes sDataNode as a representative node
	 */
	@Override
	public void unrepresentAllNodesForSummaryDataNode(final int sDataNode) {
		gDataNodesForSDataNode.remove(sDataNode);
	}
	
	@Override
	public void storeClassesBySummaryNode(final int sNode, final TIntSet classes) {
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		
		this.classesForSNode.put(sNode, classes);
	}

	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return classesForSNode;
	}
	
	@Override
	public int getClassNodeCount() {
		return classNodes.size();
	}

	@Override
	public int getPropertyNodeCount() {
		return propertyNodes.size();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return classNodes;
	}

	@Override
	public TIntSet getPropertyNodes() {
		return propertyNodes;
	}
}
