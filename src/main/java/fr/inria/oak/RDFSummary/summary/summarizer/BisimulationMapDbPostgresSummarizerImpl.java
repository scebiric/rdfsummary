package fr.inria.oak.RDFSummary.summary.summarizer;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.rdf.loader.RdfLoader;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.export.ExportResult;
import fr.inria.oak.RDFSummary.summary.export.RdfExporter;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.StatsCollector;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationMapDbPostgresSummarizerImpl implements BisimulationMapDbPostgresSummarizer {

	private final RdfLoader rdfLoader;
	private final BisimulationSummarizer summarizer;
	private final BisimulationRdfSummary summary;
	private final RdfExporter exporter;
	private final StatsCollector statsCollector;
	private final MapDbManager dbManager;
	private final Timings timings;

	private final ExportParams exportParams;
	private final String datasetName;
	private final String summaryName;

	/**
	 * @param rdfLoader
	 * @param summary
	 * @param summarizer
	 * @param exporter
	 * @param statsCollector
	 * @param dbManager
	 * @param timings
	 * @param exportParams
	 * @param datasetName
	 * @param summaryName
	 */
	public BisimulationMapDbPostgresSummarizerImpl(final RdfLoader rdfLoader, 
			final BisimulationSummarizer summarizer, final BisimulationRdfSummary summary,
			final RdfExporter exporter, final StatsCollector statsCollector, final MapDbManager dbManager, final Timings timings,
			final ExportParams exportParams, final String datasetName, final String summaryName) {
		this.rdfLoader = rdfLoader;
		this.summarizer = summarizer;
		this.summary = summary;
		this.exporter = exporter;
		this.statsCollector = statsCollector;
		this.dbManager = dbManager;
		this.timings = timings;
		this.exportParams = exportParams; 
		this.datasetName = datasetName;
		this.summaryName = summaryName;
	}

	@Override
	public BisimulationMapDbPostgresSummarizerResult run() throws SQLException, QueryException, IOException, UnsupportedDatabaseEngineException, DictionaryException, SchemaException, SignatureException {
		rdfLoader.loadDataset();
		summarizer.summarize();

		ExportResult exportResult = null;
		if (exportParams.generateDot() || exportParams.exportToRdfFile())
			exportResult = exporter.exportToFiles(exportParams, datasetName, summaryName);

		// Collect stats
		if (exportParams.exportStatsToFile()) {
			statsCollector.collectInputGraphStats();
			statsCollector.collectBisimulationStats();
			statsCollector.exportStats();
		}

		dbManager.commitAll();
		dbManager.closeAll();

		String rdfFilepath = null;
		if (exportResult != null)
			rdfFilepath = exportResult.getRdfFilepath();
		
		return new BisimulationMapDbPostgresSummarizerResult(summary, summaryName, rdfFilepath, timings);
	}

}
