package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation;

import java.sql.SQLException;
import java.util.Iterator;
import org.apache.log4j.Logger;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.MapDbBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.BisimulationSignature;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator.BisimulationGraphCreator;
import fr.inria.oak.RDFSummary.timer.Timer;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class BisimulationRdfComponentSummarizerImpl implements RdfComponentSummarizer {

	private final static Logger log = Logger.getLogger(RdfComponentSummarizer.class);
	
	private final BisimulationRdfSummary summary;
	private final MapDbBisimulationRdfDataset rdfDataset;
	private final BisimulationSignature signature;
	private final BisimulationGraphCreator graphCreator;
	private final Timer componentTimer;
	private final Timer timer;
	
	private final MessageBuilder messageBuilder;
	
	/**
	 * @param summary
	 * @param rdfDataset
	 * @param signature
	 * @param graphCreator
	 * @param componentTimer
	 * @param timer
	 */
	public BisimulationRdfComponentSummarizerImpl(final BisimulationRdfSummary summary, final MapDbBisimulationRdfDataset rdfDataset, 
			final BisimulationSignature signature, final BisimulationGraphCreator graphCreator, 
			final Timer componentTimer, final Timer timer) {
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.signature = signature;
		this.graphCreator = graphCreator;
		this.componentTimer = componentTimer;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void summarizeDataTriples() throws SQLException {
		log.info("Summarizing data triples...");
		componentTimer.reset();
		componentTimer.start();
		Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		while (gDataNodes.hasNext()) {
			summary.setSummaryDataNodeByGDataNode(gDataNodes.next(), Constant.INITIAL_BLOCK_ID);
		}
		
		log.info("Computing partitions...");
		timer.reset();
		timer.start();
		int partitionsCountOld = 0;
		int partitionsCountNew = 1;
		int iterationsCount = 0;
		int gDataNode;
		while (partitionsCountOld != partitionsCountNew) {
			gDataNodes = rdfDataset.getDataNodes();
			while (gDataNodes.hasNext()) {
				gDataNode = gDataNodes.next();
				int sDataNode = signature.getSummaryNode(gDataNode);
				summary.setSummaryDataNodeByGDataNode(gDataNode, sDataNode);
			}
			partitionsCountOld = partitionsCountNew;
			partitionsCountNew = summary.getDataNodeCount();
			iterationsCount++;
		}
		timer.stop();
		log.info(messageBuilder.append("Partitions computed in: ").append(timer.getTimeAsString()));
		
		graphCreator.createDataNodes();
		graphCreator.createDataTriples();	
		componentTimer.stop();
		log.info(messageBuilder.append("Total data triples summarization time: ").append(componentTimer.getTimeAsString()));
		log.info(messageBuilder.append("Bisimulation iterations: ").append(Integer.toString(iterationsCount)));
	}
	
	@Override
	public void summarizeTypeTriples() throws SQLException {
		log.info("Summarizing type triples...");
		componentTimer.reset();
		componentTimer.start();
		graphCreator.createTypeTriples();	
		componentTimer.stop();
		log.info(messageBuilder.append("Type triples summarized in: ").append(componentTimer.getTimeAsString()));
	}
}
