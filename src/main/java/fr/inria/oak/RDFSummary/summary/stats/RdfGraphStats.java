package fr.inria.oak.RDFSummary.summary.stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfGraphStats {

	public TripleStats getTripleStats();

	public NodeStats getNodeStats();

	public int getDistinctPropertiesCount();

	public int getDistinctClassesCount();

	public void setTripleStats(TripleStats stats);

	public void setNodeStats(NodeStats stats);

	public void setDistinctPropertiesCount(int distinctPropertiesCount);

	public void setDistinctClassesCount(int distinctClassesCount);
}
