package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.util.List;

import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface StrongEquivalenceSummary extends RdfSummary {
	
	/** Returns a collection of source cliques in the summary */
	public List<TIntSet> getSourceCliques();
	
	/** Returns a collection of target cliques in the summary */
	public List<TIntSet> getTargetCliques();
	
	/** Returns the source clique for the specified subject, or 0 if it does not have a source clique */
	public int getSourceCliqueIdBySubject(int subject);
	
	/** Returns the target clique for the specified object, or 0 if it does not have a target clique */
	public int getTargetCliqueIdByObject(int object);
	
	/** Maps the subject to the source clique */
	public void setSourceCliqueIdForSubject(int subject, int sourceClique);
	
	/** Maps the object to the target clique */
	public void setTargetCliqueIdForObject(int object, int targetClique);
	
	/** Returns the set of data nodes having the source clique with the specified ID, or null if there are no such data nodes */
	public TIntSet getDataNodesForSourceCliqueId(int sourceCliqueId);
	
	/** Returns the set of data nodes having the target clique with the specified ID, or null if there are no such data nodes */
	public TIntSet getDataNodesForTargetCliqueId(int targetCliqueId);
	
	/** Stores the data node for the specified source clique ID 
	 *  */
	public void storeDataNodeForSourceCliqueId(int sourceCliqueId, int dataNode);
	
	/** Stores the data node for the specified target clique ID 
	 *  */
	public void storeDataNodeForTargetCliqueId(int targetCliqueId, int dataNode); 
	
	/** Clears the list of source cliques */
	public void clearSourceCliques();

	/** Clears the list of target cliques */
	public void clearTargetCliques();
	
	/** Clears various maps for cliques */
	public void clearCliqueMaps();
}
