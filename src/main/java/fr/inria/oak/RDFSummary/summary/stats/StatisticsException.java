package fr.inria.oak.RDFSummary.summary.stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StatisticsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -870751874788173381L;

	public StatisticsException() {
		// TODO Auto-generated constructor stub
	}

	public StatisticsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public StatisticsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public StatisticsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public StatisticsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
