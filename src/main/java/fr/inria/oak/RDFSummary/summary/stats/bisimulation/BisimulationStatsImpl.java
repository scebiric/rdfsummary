package fr.inria.oak.RDFSummary.summary.stats.bisimulation;

import fr.inria.oak.RDFSummary.summary.stats.NodeStats;
import fr.inria.oak.RDFSummary.summary.stats.RdfGraphStats;
import fr.inria.oak.RDFSummary.summary.stats.TripleStats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationStatsImpl implements BisimulationStats {

	private final RdfGraphStats rdfGraphStats;
	private int iterations;
	
	/**
	 * @param rdfGraphStats
	 */
	public BisimulationStatsImpl(final RdfGraphStats rdfGraphStats) {
		this.rdfGraphStats = rdfGraphStats;
	}	
	
	@Override
	public int getIterationsCount() {
		return iterations;
	}

	@Override
	public void setIterationsCount(final int count) {
		iterations = count;
	}

	@Override
	public TripleStats getTripleStats() {
		return rdfGraphStats.getTripleStats();
	}

	@Override
	public NodeStats getNodeStats() {
		return rdfGraphStats.getNodeStats();
	}

	@Override
	public int getDistinctPropertiesCount() {
		return rdfGraphStats.getDistinctPropertiesCount();
	}

	@Override
	public int getDistinctClassesCount() {
		return rdfGraphStats.getDistinctClassesCount();
	}

	@Override
	public void setTripleStats(TripleStats stats) {
		rdfGraphStats.setTripleStats(stats);
	}

	@Override
	public void setNodeStats(NodeStats stats) {
		rdfGraphStats.setNodeStats(stats);
	}

	@Override
	public void setDistinctPropertiesCount(int distinctPropertiesCount) {
		rdfGraphStats.setDistinctPropertiesCount(distinctPropertiesCount);
	}

	@Override
	public void setDistinctClassesCount(int distinctClassesCount) {
		rdfGraphStats.setDistinctClassesCount(distinctClassesCount);
	}
}
