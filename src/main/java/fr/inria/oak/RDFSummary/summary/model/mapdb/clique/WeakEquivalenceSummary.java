package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface WeakEquivalenceSummary extends CliqueRdfSummary {
	
	/** 
	 * Unifies the two data nodes 
	 *   
	 * @param dataNode1
	 * @param dataNode2
	 * @return The remaining node  
	 */
	public int unifyDataNodes(int dataNode1, int dataNode2);
	
	public void unifyDataNodes() throws SQLException; 
	
	/** Stores the specified data node as the source of the data property 
	 *  */
	public void setSubjectForDataProperty(int dataProperty, int dataNode);
	
	/** Stores the specified data node as the target of the data property 
	 *  */
	public void setObjectForDataProperty(int dataProperty, int dataNode);
	
	/** Returns the source node for the specified data property or null if there is no such node */
	public Integer getSubjectForDataProperty(int dataProperty);
	
	/** Returns the target node for the specified data property or null if there is no such node */
	public Integer getObjectForDataProperty(int dataProperty);	
	
	/**
	 * @param dataNode
	 * @return An iterator over the set of data properties of which the specified summary data node is the source
	 */
	public Iterator<Integer> getDataPropertiesBySubject(int dataNode);
	
	/**
	 * @param dataNode
	 * @return An iterator over the set of data properties of which the specified summary data node is the target
	 */
	public Iterator<Integer> getDataPropertiesByObject(int dataNode);
}
