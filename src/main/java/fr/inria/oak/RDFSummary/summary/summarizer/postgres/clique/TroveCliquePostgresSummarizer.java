package fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique;

import java.sql.SQLException;

import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveCliquePostgresSummarizer {

	/** 
	 * @param summaryType
	 * @param dataTable
	 * @param typesTable
	 * @param schemaTable
	 * @return {@link TroveCliquePostgresSummaryResult}
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws InexistentKeyException
	 *  
	 */
	public TroveCliquePostgresSummaryResult summarize(String summaryType, String dataTable, String typesTable, String schemaTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException;
	
	/**
	 * Generates a {@link WeakSummary} on Postgres
	 * 
	 * @param dataTriplesLocation
	 * @param typeTriplesLocation
	 * @return The summary result
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 *  
	 */
	public TroveCliquePostgresSummaryResult generateWeakPureCliqueEquivalenceSummary(String dataTriplesLocation, String typeTriplesLocation) throws SQLException, DictionaryException, InexistentValueException;

	/**
	 * Generates a {@link TypedWeakSummary} on Postgres
	 * 
	 * @param dataTriplesLocation
	 * @param typeTriplesLocation
	 * @return The summary result
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws InexistentKeyException
	 *  
	 */
	public TroveCliquePostgresSummaryResult generateWeakTypeEquivalenceSummary(String dataTriplesLocation, String typeTriplesLocation) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException;
	
	/**
	 * Generates a {@link StrongSummary} on Postgres
	 * 
	 * @param dataTable
	 * @param typesTable
	 * @return The summary result
	 * @throws SQLException
	 *  
	 */
	public TroveCliquePostgresSummaryResult generateStrongPureCliqueEquivalenceSummary(String dataTable, String typesTable) throws SQLException;
	
	/**
	 * Generates a typed {@link TypedStrongSummary} on Postgres
	 * 
	 * @param dataTable
	 * @param typesTable
	 * @return The summary result
	 * @throws SQLException
	 *  
	 */
	public TroveCliquePostgresSummaryResult generateStrongTypeEquivalenceSummary(String dataTable, String typesTable) throws SQLException;
}
