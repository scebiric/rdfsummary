package fr.inria.oak.RDFSummary.summary.noderepresenter;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveNodeRepresenter {

	/**
	 * @param summary
	 * @param gDataNode
	 * @return The representative of gDataNode.
	 * @throws SQLException
	 * 
	 */
	public int representInputDataNode(final StrongEquivalenceSummary summary, final int gDataNode) throws SQLException;
	
	/**
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the source of dataProperty.
	 * @throws SQLException
	 * 
	 */
	public int getRepresentativeSourceDataNode(final WeakPureCliqueEquivalenceSummary summary, final int subject, final int dataProperty) throws SQLException;
	
	/**
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the target of dataProperty.
	 * @throws SQLException
	 * 
	 */
	public int getRepresentativeTargetDataNode(final WeakPureCliqueEquivalenceSummary summary, final int object, final int dataProperty) throws SQLException;
	
	/**
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the source of dataProperty.
	 * @throws SQLException
	 * 
	 */
	public int getRepresentativeSourceDataNode(final WeakTypeEquivalenceSummary summary, final int subject, final int dataProperty) throws SQLException;
	
	/**
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the target of dataProperty.
	 * @throws SQLException
	 * 
	 */
	public int getRepresentativeTargetDataNode(final WeakTypeEquivalenceSummary summary, final int object, final int dataProperty) throws SQLException;
}
