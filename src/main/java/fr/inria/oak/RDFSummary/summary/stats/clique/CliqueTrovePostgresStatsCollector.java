package fr.inria.oak.RDFSummary.summary.stats.clique;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.StatisticsException;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * Stats info on the input dataset
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface CliqueTrovePostgresStatsCollector {
	
	/**
	 * @param summary
	 * @param encodedInputDataTable
	 * @param encodedInputTypesTable
	 * @param encodedSummaryDataTable
	 * @param encodedSummaryTypesTable
	 * @param schema
	 * @param summarizationTime
	 * @return {@link Stats}
	 * @throws DictionaryException
	 * @throws SQLException
	 * @throws QueryException
	 */
	public Stats collectStats(final RdfSummary summary, final String encodedInputDataTable, final String encodedInputTypesTable,
			final String encodedSummaryDataTable, final String encodedSummaryTypesTable,
			final String classNodesTable, final String propertyNodesTable,
			final Schema schema, final long summarizationTime) throws DictionaryException, SQLException, QueryException;
	
	/**
	 * @param classIRI
	 * @param encodedDataTable
	 * @param encodedTypesTable
	 * @return The number of distinct resources of the specified class appearing in the subject or object position of a data triple.
	 * @throws SQLException
	 * @throws StatisticsException
	 * @throws DictionaryException 
	 */
	public int getClassSupport(String classIRI, String encodedDataTable, String encodedTypesTable) throws SQLException, StatisticsException, DictionaryException;
	
	/**
	 * @param dataProperty
	 * @param encodedDataTable
	 * @return The number of data triples having the specified data property.
	 * @throws SQLException
	 * @throws StatisticsException
	 * @throws DictionaryException 
	 */
	public int getDataPropertySupport(final String dataProperty, final String encodedDataTable) throws SQLException, StatisticsException, DictionaryException;
}
