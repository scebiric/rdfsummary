package fr.inria.oak.RDFSummary.summary.bisimulation.signature;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.TreeSet;

import org.mapdb.Fun.Tuple2;

import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.trie.TST;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.trie.Trie;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.util.SummaryUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTrieBisimulationSignatureImpl implements BisimulationSignature {

	private final String summaryType;
	private final PostgresBisimulationRdfDataset rdfDataset;
	private final BisimulationRdfSummary summary;

	private Trie<Integer> trie;
	private int seed;
	
	private TreeSet<Tuple2<Integer, Integer>> pairsTreeSet;
	private Iterator<Tuple2<Integer, Integer>> pairsIterator;
	private ResultSet pairsResultSet;
	private Tuple2<Integer, Integer> pair;
	private int property;
	private int gNode;
	private StringBuilder sb;
	
	/**
	 * @param summaryType
	 * @param rdfDataset
	 * @param summary
	 */
	public PostgresTrieBisimulationSignatureImpl(final String summaryType, final PostgresBisimulationRdfDataset rdfDataset, final BisimulationRdfSummary summary) {
		this.summaryType = summaryType;
		this.rdfDataset = rdfDataset;
		this.summary = summary;
		
		this.trie = new TST<Integer>();
		this.seed = Constant.INITIAL_SEED;
	}

	@Override
	public int getSummaryNode(int gDataNode) throws SQLException {
		pairsTreeSet = Sets.newTreeSet(); // Tree set ensures that the pairs are sorted and unique
		if (summaryType.equals(SummaryType.FORWARD_ONLY) || summaryType.equals(SummaryType.FORWARD_BACKWARD)) {
			pairsTreeSet = computeForwardSignature(gDataNode);
		}
		if (summaryType.equals(SummaryType.BACKWARD_ONLY) || summaryType.equals(SummaryType.FORWARD_BACKWARD)) {
			pairsTreeSet = computeBackwardSignature(gDataNode);
		}
		
		final String signature = getSignature(gDataNode, pairsTreeSet);
		Integer id = trie.get(signature);		
		if (id == null) {
			id = seed++;
			trie.put(signature, id);
		}
		
		return id.intValue();
	}

	@Override
	public TreeSet<Tuple2<Integer, Integer>> computeForwardSignature(final int gDataNode) throws SQLException {
		pairsResultSet = rdfDataset.getPropertyObjectPairsBySubject(gDataNode);
		addPairsToTreeSet(pairsResultSet);
		pairsResultSet.close();

		return pairsTreeSet;
	}

	@Override
	public TreeSet<Tuple2<Integer, Integer>> computeBackwardSignature(final int gDataNode) throws SQLException {
		pairsResultSet = rdfDataset.getPropertySubjectPairsByObject(gDataNode);
		addPairsToTreeSet(pairsResultSet);
		pairsResultSet.close();

		return pairsTreeSet;
	}

	private void addPairsToTreeSet(final ResultSet pairsResultSet) throws SQLException {
		while (pairsResultSet.next()) {
			property = pairsResultSet.getInt(1);
			gNode = pairsResultSet.getInt(2);
			pairsTreeSet.add(new Tuple2<Integer, Integer>(property, SummaryUtils.getRepresentative(gNode, rdfDataset, summary)));
		}
	}
	
	private String getSignature(final int gDataNode, final TreeSet<Tuple2<Integer, Integer>> pairs) {
		sb = new StringBuilder();
		sb.append(summary.getSummaryDataNodeByGDataNode(gDataNode).intValue());
		pairsIterator = pairs.iterator();
		while (pairsIterator.hasNext()) {
			pair = pairsIterator.next();
			sb.append(Chars.DOT).append(pair.a.intValue());
			sb.append(Chars.DOT).append(pair.b.intValue());
		}
		 
		return sb.toString();
	}

	@Override
	public void clearSignatures() {
		trie = null;
		trie = new TST<Integer>();
	}
}
