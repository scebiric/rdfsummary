package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PureCliqueEquivalenceSummaryImpl implements PureCliqueEquivalenceSummary {

	private final CliqueRdfSummary rdfSummary;
	private final UriGenerator uriGenerator;
	private final Dictionary dictionary;

	/**
	 * @param rdfSummary
	 * @param uriGenerator
	 * @param dictionary
	 */
	public PureCliqueEquivalenceSummaryImpl(final CliqueRdfSummary rdfSummary, final UriGenerator uriGenerator, final Dictionary dictionary) {
		this.rdfSummary = rdfSummary;
		this.uriGenerator = uriGenerator;
		this.dictionary = dictionary;
	}

	@Override
	public int createDataNode(final Iterator<Integer> gDataNodes) throws SQLException {
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkArgument(gDataNodes.hasNext());
	
		int sDataNode = dictionary.insert(uriGenerator.generateDataNodeUri(dictionary.getSeed()));
		rdfSummary.setLastCreatedDataNode(sDataNode);
		while (gDataNodes.hasNext()) {
			rdfSummary.representInputDataNode(gDataNodes.next(), sDataNode);
		}
		
		return sDataNode;
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		rdfSummary.createDataTriple(source, dataProperty, target); 
	}
	
	@Override
	public void createTypeTriple(final int sNode, final int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}
	
	@Override
	public boolean existsTypeTriple(final int sNode, final int classIRI) {
		Iterator<Integer> classes = rdfSummary.getClassesBySummaryNode(sNode);
		while (classes.hasNext()) {
			if (classIRI == classes.next())
				return true;
		}
			
		return false;
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public Integer getSummaryDataNodeByGDataNode(final int gDataNode) {
		return rdfSummary.getSummaryDataNodeByGDataNode(gDataNode);
	}

	@Override
	public Iterator<Integer> getRepresentedDataNodes(final int sDataNode) {
		return rdfSummary.getRepresentedDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public Iterator<Integer> getClassesBySummaryNode(final int sNode) {
		return rdfSummary.getClassesBySummaryNode(sNode);
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		return rdfSummary.getAllTypeTriples();
	}

	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		return rdfSummary.getAllDataTriples();
	}

	@Override
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createTypeTriples(int sDataNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sDataNode, classes);
	}
	
	@Override
	public int getDataTriplesCount() {
		return rdfSummary.getDataTriplesCount();
	}

	@Override
	public int getTypeTriplesCount() {
		return rdfSummary.getTypeTriplesCount();
	}
}
