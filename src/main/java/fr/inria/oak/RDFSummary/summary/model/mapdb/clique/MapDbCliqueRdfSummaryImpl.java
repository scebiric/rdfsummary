package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;
import org.mapdb.Serializer;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbCliqueRdfSummaryImpl implements CliqueRdfSummary {

	private final RdfSummary rdfSummary;
	private final UriGenerator uriGenerator;
	private final Dictionary dictionary;
 
	private final Map<Integer, Integer> sDataNodeByGDataNode; // summary node can only be a data node
	private final NavigableSet<Tuple2<Integer, Integer>> gNodesBySummaryNode; // data nodes only
	
	private static final String S_NODE_BY_G_DATA_NODE = "clique_node_by_g_data_node";
	private static final String G_NODES_BY_SUMMARY_NODE = "g_nodes_by_summary_node";
		
	private int lastCreatedDataNode = -1;
		
	/**
	 * @param summaryDb
	 * @param summaryType
	 * @param rdfSummary
	 * @param uriGenerator 
	 * @param dictionary
	 */
	public MapDbCliqueRdfSummaryImpl(final DB summaryDb, final String summaryType, final RdfSummary rdfSummary, final UriGenerator uriGenerator, final Dictionary dictionary) {
		this.rdfSummary = rdfSummary;
		this.uriGenerator = uriGenerator;
		this.dictionary = dictionary;
			
		sDataNodeByGDataNode = summaryDb.createHashMap(S_NODE_BY_G_DATA_NODE).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
		gNodesBySummaryNode = summaryDb.createTreeSet(G_NODES_BY_SUMMARY_NODE).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();
	}
	
	@Override
	public Integer getSummaryDataNodeByGDataNode(final int gDataNode) { 
		return sDataNodeByGDataNode.get(gDataNode);
	}

	@Override
	public int getSummaryDataNodeSupport(final int sDataNode) {
		final Iterator<Integer> extentIterator = Fun.filter(gNodesBySummaryNode, sDataNode).iterator();
		if (!extentIterator.hasNext())
			throw new IllegalArgumentException("Invalid summary data node: " + sDataNode);
		
		int support = 0;
		while (extentIterator.hasNext())
			support++;
			
		return support; 
	}

	@Override
	public Iterator<Integer> getRepresentedDataNodes(final int sDataNode) {
		final Iterator<Integer> extentIterator = Fun.filter(gNodesBySummaryNode, sDataNode).iterator();
		if (!extentIterator.hasNext())
			throw new IllegalArgumentException("Invalid summary data node: " + sDataNode);
		
		return extentIterator;
	}
	
	@Override
	public void setLastCreatedDataNode(final int dataNode) {
		lastCreatedDataNode = dataNode;
	}
	
	/**
	 * @return The created data node
	 * @throws SQLException 
	 */
	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		int sDataNode = dictionary.insert(uriGenerator.generateDataNodeUri(dictionary.getSeed()));	 
		lastCreatedDataNode = sDataNode;
		representInputDataNode(gDataNode, sDataNode);
		
		return sDataNode;
	}

	@Override
	public int getLastCreatedDataNode() {
		return lastCreatedDataNode;
	}

	@Override
	public void representInputDataNode(final int gDataNode, final int sDataNode) {	
		sDataNodeByGDataNode.put(gDataNode, sDataNode);
		gNodesBySummaryNode.add(new Tuple2<Integer, Integer>(sDataNode, gDataNode));
	}

	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}

	@Override
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		rdfSummary.createDataTriple(source, dataProperty, target);
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		return rdfSummary.getAllDataTriples();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		return rdfSummary.getAllTypeTriples();
	}

	@Override
	public Iterator<Integer> getClassesBySummaryNode(int sNode) {
		return rdfSummary.getClassesBySummaryNode(sNode);
	}
	
	@Override
	public int getDataTriplesCount() {
		return rdfSummary.getDataTriplesCount();
	}

	@Override
	public int getTypeTriplesCount() {
		return rdfSummary.getTypeTriplesCount();
	}
}
