package fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.storage.DatasetPreparationResult;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * The result of building a summary
 * 
 * @author Sejla CEBIRIC  
 */
public abstract class PostgresSummaryResult {
	
	protected final String summaryType;
	protected String summaryName;
	protected Stats stats;
	protected Schema schema;
	protected DatasetPreparationResult datasetPreparationResult;
	protected String summaryDataFilepath;
	
	/**
	 * @param summaryType
	 * @param classNodesTable
	 * @param propertyNodesTable
	 */
	public PostgresSummaryResult(final String summaryType) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		this.summaryType = summaryType;
		this.summaryName = null;
		this.stats = null;
		this.schema = null;
		this.datasetPreparationResult = null;
	}

	public String getSummaryType() {
		return this.summaryType;
	}
	
	public Stats getStatistics() {
		return this.stats;
	}
	
	public void setStatistics(final Stats stats) {
		Preconditions.checkNotNull(stats);
		this.stats = stats;
	}
	
	public Schema getSchema() {
		return this.schema;
	}
	
	public void setSchema(final Schema schema) {
		this.schema = schema;
	}
	
	public DatasetPreparationResult getDatasetPreparationResult() {
		return datasetPreparationResult;
	}
	
	public void setDatasetPreparationResult(final DatasetPreparationResult value) {
		datasetPreparationResult = value;
	}
	
	public String getSummaryDataFilepath() {
		return summaryDataFilepath;
	}
	
	public void setSummaryDataFilepath(final String value) {
		summaryDataFilepath = value;
	}
	
	public String getSummaryName() {
		return summaryName;
	}
	
	public void setSummaryName(final String value) {
		summaryName = value;
	}
}
