package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationGraphCreator {

	public void createDataNodes() throws SQLException;

	/**
	 * For each data node in G, the method retrieves its representative summary node,
	 * and all (p,o) pairs such that the triple s p o is in G.
	 * 
	 * The data triple f(s) p f(o) is added to the summary.
	 * @throws SQLException 
	 */
	public void createDataTriples() throws SQLException;
	
	/**
	 * For each type triple s type c, the type triple f(s) type c is added to the summary.
	 * 
	 * @throws SQLException
	 */
	public void createTypeTriples() throws SQLException;
}
