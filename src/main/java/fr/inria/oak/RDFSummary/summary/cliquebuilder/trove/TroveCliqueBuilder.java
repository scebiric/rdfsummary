package fr.inria.oak.RDFSummary.summary.cliquebuilder.trove;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummary;
import gnu.trove.set.TIntSet;

/**
 * Creates cliques in a {@link StrongSummary}.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveCliqueBuilder {

	/**
	 * Builds all source cliques from the specified data table
	 * 
	 * @param dataTable
	 * @param summary
	 * @throws SQLException
	 */
	public void buildSourceCliques(String dataTable, StrongPureCliqueEquivalenceSummary summary) throws SQLException;
	
	/**
	 * Builds all target cliques from the specified data table
	 * 
	 * @param dataTable
	 * @param summary
	 * @throws SQLException 
	 */
	public void buildTargetCliques(String dataTable, StrongPureCliqueEquivalenceSummary summary) throws SQLException;
	
	/**
	 * Builds source cliques from the specified data table for untyped subjects
	 * 
	 * @param dataTable
	 * @param typesTable
	 * @param summary
	 * @throws SQLException
	 */
	public void buildSourceCliques(String dataTable, String typesTable, StrongTypeEquivalenceSummary summary) throws SQLException;
	
	/**
	 * Builds target cliques from the specified data table for untyped objects
	 * 
	 * @param dataTable
	 * @param typesTable
	 * @param summary
	 * @throws SQLException 
	 */
	public void buildTargetCliques(String dataTable, String typesTable, StrongTypeEquivalenceSummary summary) throws SQLException;
	
	/**
	 * Computes the source clique for the specified subject and its set of (outgoing) properties
	 * 
	 * @param summary
	 * @param subject
	 * @param sProps
	 */
	public void computeSourceClique(StrongEquivalenceSummary summary, int subject, TIntSet sProps);

	/**
	 * Computes the target clique for the specified object and its set of (incoming) properties
	 * 
	 * @param summary
	 * @param object
	 * @param oProps
	 */
	public void computeTargetClique(StrongEquivalenceSummary summary, int object, TIntSet oProps);
}
