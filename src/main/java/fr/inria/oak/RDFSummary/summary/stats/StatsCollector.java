package fr.inria.oak.RDFSummary.summary.stats;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dao.QueryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface StatsCollector {

	public void collectInputGraphStats() throws SQLException, QueryException;
	
	public void collectBisimulationStats() throws SQLException, QueryException;
	
	public void exportStats() throws IOException;
}
