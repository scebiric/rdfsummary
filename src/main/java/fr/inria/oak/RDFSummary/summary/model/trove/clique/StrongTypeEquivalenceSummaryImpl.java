package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongTypeEquivalenceSummaryImpl implements StrongTypeEquivalenceSummary {

	private final StrongEquivalenceSummary strongSummary;
	private final TypeEquivalenceSummary typeEquivSummary;
	private final RdfSummary rdfSummary;

	/**
	 * @param strongEquivalenceSummary
	 * @param typeEquivalenceSummary
	 * @param rdfSummary
	 */
	public StrongTypeEquivalenceSummaryImpl(final StrongEquivalenceSummary strongEquivalenceSummary,
			final TypeEquivalenceSummary typeEquivalenceSummary,
			final RdfSummary rdfSummary) {
		this.strongSummary = strongEquivalenceSummary;
		this.typeEquivSummary = typeEquivalenceSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		typeEquivSummary.createTypeTriples(sNode, classes);
	}
	
	@Override
	public List<TIntSet> getSourceCliques() {
		return strongSummary.getSourceCliques();
	}

	@Override
	public List<TIntSet> getTargetCliques() {
		return strongSummary.getTargetCliques();
	}

	@Override
	public int getSourceCliqueIdBySubject(int subject) {
		return strongSummary.getSourceCliqueIdBySubject(subject);
	}

	@Override
	public int getTargetCliqueIdByObject(int object) {
		return strongSummary.getTargetCliqueIdByObject(object);
	}

	@Override
	public void setSourceCliqueIdForSubject(int subject, int sourceClique) {
		strongSummary.setSourceCliqueIdForSubject(subject, sourceClique);
	}

	@Override
	public void setTargetCliqueIdForObject(int object, int targetClique) {
		strongSummary.setTargetCliqueIdForObject(object, targetClique);
	}

	@Override
	public TIntSet getDataNodesForSourceCliqueId(int sourceCliqueId) {
		return strongSummary.getDataNodesForSourceCliqueId(sourceCliqueId);
	}

	@Override
	public TIntSet getDataNodesForTargetCliqueId(int targetCliqueId) {
		return strongSummary.getDataNodesForTargetCliqueId(targetCliqueId);
	}

	@Override
	public void storeDataNodeForSourceCliqueId(int sourceCliqueId, int dataNode) {
		strongSummary.storeDataNodeForSourceCliqueId(sourceCliqueId, dataNode);
	}

	@Override
	public void storeDataNodeForTargetCliqueId(int targetCliqueId, int dataNode) {
		strongSummary.storeDataNodeForTargetCliqueId(targetCliqueId, dataNode);
	}

	@Override
	public void clearSourceCliques() {
		strongSummary.clearSourceCliques();
	}

	@Override
	public void clearTargetCliques() {
		strongSummary.clearTargetCliques();
	}

	@Override
	public void clearCliqueMaps() {
		strongSummary.clearCliqueMaps();
	}

	@Override
	public boolean isTypedDataNode(int sDataNode) {
		return typeEquivSummary.isTypedDataNode(sDataNode);
	}

	@Override
	public void setHighestTypedDataNode(int sNode) {
		typeEquivSummary.setHighestTypedDataNode(sNode);
	}

	@Override
	public int getSummaryDataNodeForClasses(TIntSet classes) {
		return typeEquivSummary.getSummaryDataNodeForClasses(classes);
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}
	
	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target);
	}
	
	@Override
	public void storeSummaryDataNodeByClassSet(TIntSet classes, int sDataNode) {
		typeEquivSummary.storeSummaryDataNodeByClassSet(classes, sDataNode);
	}
	
	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
