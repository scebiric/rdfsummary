package fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceResult;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummary;

/**
 * The result of building a Trove summary
 * 
 * @author Sejla CEBIRIC * 
 */
public class TroveCliquePostgresSummaryResult extends PostgresSummaryResult {

	private final RdfSummary summary;	
	private final PerformanceResult performanceResult;
	private final String classNodesTable;
	private final String propertyNodesTable;
	private PostgresTableNames storageLayout;
	
	/**
	 * @param summaryType
	 * @param summary
	 * @param classNodesTable
	 * @param propertyNodesTable
	 * @param performanceResult
	 */
	public TroveCliquePostgresSummaryResult(final String summaryType, final RdfSummary summary, final String classNodesTable, final String propertyNodesTable, final PerformanceResult performanceResult) {
		super(summaryType);
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(summary instanceof PureCliqueEquivalenceSummary || summary instanceof TypeEquivalenceSummary);
		Preconditions.checkArgument(summary instanceof WeakEquivalenceSummary || summary instanceof StrongEquivalenceSummary);
		Preconditions.checkNotNull(performanceResult);
		
		this.summary = summary;
		this.performanceResult = performanceResult;
		this.storageLayout = null;
		this.classNodesTable = classNodesTable;
		this.propertyNodesTable = propertyNodesTable;
	}
	
	public RdfSummary getSummary() {
		return this.summary;
	}
	
	public PerformanceResult getPerformanceResult() {
		return this.performanceResult;
	}
	
	public String getClassNodesTable() {
		return classNodesTable;
	}
	
	public String getPropertyNodesTable() {
		return propertyNodesTable;
	}
	
	public PostgresTableNames getStorageLayout() {
		return this.storageLayout;
	}
	
	public void setStorageLayout(final PostgresTableNames storageLayout) {
		this.storageLayout = storageLayout;
	}
}
