package fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.rdf.dataset.RdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.WeakPureCliqueEquivalenceSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureDataTripleCreatorImpl implements DataTripleCreator {

	private final WeakPureCliqueEquivalenceSummary summary;
	private final RdfDataset rdfDataset;
	
	private boolean createDataTriple = false;
	
	/**
	 * @param summary
	 * @param rdfDataset 
	 */
	public WeakPureDataTripleCreatorImpl(final WeakPureCliqueEquivalenceSummary summary, final RdfDataset rdfDataset, Dictionary dictionary) {
		this.summary = summary;		
		this.rdfDataset = rdfDataset;
	}

	@Override
	public void representDataTriple(final int subject, final int property, final int object) throws SQLException {
		boolean subjectIsClassOrProperty = rdfDataset.isClassOrProperty(subject);
		boolean objectIsClassOrProperty = rdfDataset.isClassOrProperty(object);

//		String p = dictionary.getValue(property);
//		if (p.equals("http://www.w3.org/2000/01/rdf-schema#comment") || p.equals("http://rdf.insee.fr/def/geo#nom")) {
//			String o = dictionary.getValue(object);
//			System.out.println(p + " " + o);
//		}
		
		int source = subject;
		if (!subjectIsClassOrProperty)
			source = getRepresentativeSourceDataNode(subject, property);
		int target = object;
		if (!objectIsClassOrProperty)
			target = getRepresentativeTargetDataNode(object, property);

		if (createDataTriple)
			summary.createDataTriple(source, property, target);
	}
	
	@Override
	public int getRepresentativeSourceDataNode(int subject, int dataProperty) throws SQLException {
		Integer propertySource = summary.getSubjectForDataProperty(dataProperty);		
		Integer subjectNode = summary.getSummaryDataNodeByGDataNode(subject);

		if (propertySource == null) // if null, it follows that propertyTarget will also be null (one edge per distinct property in weak summaries)
			createDataTriple = true;
		else
			createDataTriple = false;
		
		if (propertySource != null && subjectNode != null) {
			if (propertySource.intValue() == subjectNode.intValue())
				return subjectNode;
			else 
				return summary.unifyDataNodes(propertySource, subjectNode);
		}

		if (propertySource != null && subjectNode == null) {
			summary.representInputDataNode(subject, propertySource);
			return propertySource;
		}
		
		if (propertySource == null && subjectNode != null) {
			summary.setSubjectForDataProperty(dataProperty, subjectNode);			
			return subjectNode;
		}

		// propertySource == null && subjectNode == null
		subjectNode = summary.createDataNode(subject);
		summary.setSubjectForDataProperty(dataProperty, subjectNode);
		
		return subjectNode;
	}

	@Override
	public int getRepresentativeTargetDataNode(int object, int dataProperty) throws SQLException {
		Integer propertyTarget = summary.getObjectForDataProperty(dataProperty);
		Integer objectNode = summary.getSummaryDataNodeByGDataNode(object);

		if (propertyTarget != null && objectNode != null) {	
			if (propertyTarget.intValue() == objectNode.intValue())
				return objectNode;
			else 
				return summary.unifyDataNodes(propertyTarget, objectNode);
		}

		if (propertyTarget != null && objectNode == null) {
			summary.representInputDataNode(object, propertyTarget);
			return propertyTarget;
		}

		if (propertyTarget == null && objectNode != null) {
			summary.setObjectForDataProperty(dataProperty, objectNode);
			return objectNode;
		}

		// untypedPropertyTarget == null && objectNode == null
		objectNode = summary.createDataNode(object);
		summary.setObjectForDataProperty(dataProperty, objectNode);
		
		return objectNode;
	}
}
