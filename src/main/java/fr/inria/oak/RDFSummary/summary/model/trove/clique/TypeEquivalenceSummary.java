package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TypeEquivalenceSummary extends RdfSummary {
	
	/** Creates a type triple from sNode to each of the specified classes 
	 * */
	public void createTypeTriples(int sNode, TIntSet classes);
	
	/** Returns the summary data node having all the specified classes or 0 if such node does not exist */
	public int getSummaryDataNodeForClasses(TIntSet classes);
	
	/** Returns true if the specified summary data node is typed, otherwise false 
	 * 
	 * */
	public boolean isTypedDataNode(int sDataNode);

	/** Stores the highest typed summary node (class, property or data node) 
	 * 
	 * */
	public void setHighestTypedDataNode(int sNode);
	
	/**
	 * Stores the summary data node representing input data nodes having all the specified classes.
	 * 
	 * @param classes
	 * @param sDataNode
	 * 
	 */
	public void storeSummaryDataNodeByClassSet(TIntSet classes, int sDataNode);
}
