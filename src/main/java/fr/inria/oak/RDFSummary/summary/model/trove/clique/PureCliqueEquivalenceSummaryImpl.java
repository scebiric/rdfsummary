package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PureCliqueEquivalenceSummaryImpl implements PureCliqueEquivalenceSummary {

	private final RdfSummary rdfSummary;
	private final UriGenerator uriGenerator;
	private final DictionaryDao dictDao;
	
	private TIntSet classes;
	private TIntIterator iterator = null;

	/**
	 * @param rdfSummary
	 * @param uriGenerator
	 * @param dictDao
	 */
	public PureCliqueEquivalenceSummaryImpl(final RdfSummary rdfSummary, final UriGenerator uriGenerator, final DictionaryDao dictDao) {
		this.rdfSummary = rdfSummary;
		this.uriGenerator = uriGenerator;
		this.dictDao = dictDao;
	}

	@Override
	public int createDataNode(final TIntSet gDataNodes) throws SQLException {
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkArgument(gDataNodes.size() > 0);
	
		int sDataNode = dictDao.getExistingOrNewKey(uriGenerator.generateDataNodeUri(rdfSummary.getNextNodeId()));
		rdfSummary.setLastCreatedDataNode(sDataNode);
		iterator = gDataNodes.iterator();
		while (iterator.hasNext()) {
			rdfSummary.representInputDataNode(iterator.next(), sDataNode);
		}
		
		return sDataNode;
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target); 
	}
	
	@Override
	public void createTypeTriple(final int sNode, final int classIRI) {
		TIntSet classes = rdfSummary.getClassesForSummaryNode(sNode);
		if (classes != null)
			classes.add(classIRI);
		else {
			classes = new TIntHashSet();
			classes.add(classIRI);
			rdfSummary.storeClassesBySummaryNode(sNode, classes);
		}
	}

	@Override
	public void createTypeTriples(int sDataNode, TIntSet classes) {
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		
		rdfSummary.storeClassesBySummaryNode(sDataNode, classes);
	}
	
	@Override
	public boolean existsTypeTriple(final int sNode, final int classIRI) {
		classes = rdfSummary.getClassesForSummaryNode(sNode);

		return classes != null && classes.contains(classIRI);
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}

	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}

	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
