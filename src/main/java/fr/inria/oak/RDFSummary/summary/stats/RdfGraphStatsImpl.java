package fr.inria.oak.RDFSummary.summary.stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfGraphStatsImpl implements RdfGraphStats {

	private TripleStats tripleStats;
	private NodeStats nodeStats;
	private int distinctProperties;
	private int distinctClasses;
	
	@Override
	public TripleStats getTripleStats() {
		return tripleStats;
	}
	
	@Override
	public NodeStats getNodeStats() {
		return nodeStats;
	}
	
	@Override
	public int getDistinctPropertiesCount() {
		return distinctProperties;
	}
	
	@Override
	public int getDistinctClassesCount() {
		return distinctClasses;
	}

	@Override
	public void setTripleStats(final TripleStats stats) {
		tripleStats = stats;
	}

	@Override
	public void setNodeStats(final NodeStats stats) {
		nodeStats = stats;
	}

	@Override
	public void setDistinctPropertiesCount(final int count) {
		distinctProperties = count;
	}

	@Override
	public void setDistinctClassesCount(int count) {
		distinctClasses = count;
	}
}
