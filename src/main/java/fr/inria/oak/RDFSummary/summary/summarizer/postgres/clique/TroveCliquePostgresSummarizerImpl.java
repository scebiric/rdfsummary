package fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TroveRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.components.clique.TroveRdfComponentSummarizer;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveCliquePostgresSummarizerImpl implements TroveCliquePostgresSummarizer {

	private static final Logger log = Logger.getLogger(TroveCliquePostgresSummarizer.class);
	private final SqlConnectionHandler connHandler;
	private static TroveRdfComponentSummarizer RdfComponentSummarizer;
	private static UriGenerator UriGenerator;
	private static DmlDao DmlDao;
	private static DictionaryDao DictDao;
	private static RdfTablesDao RdfTablesDao;
	private static PerformanceLogger PerformanceLogger;
	
	private static StringBuilder Builder;
	private RdfSummary rdfSummary = null;
	private WeakPureCliqueEquivalenceSummary weakPureSummary = null;
	private WeakTypeEquivalenceSummary weakTypeSummary = null;
	private StrongPureCliqueEquivalenceSummary strongPureSummary = null;
	private StrongTypeEquivalenceSummary strongTypeSummary = null;
	
	private String classNodesTable = null;
	private String propertyNodesTable = null;
	private final Timer timer;
	
	/**
	 * @param connHandler
	 * @param rdfComponentSummarizer
	 * @param uriGenerator
	 * @param dmlDao
	 * @param dictDao
	 * @param rdfTablesDao
	 * @param performanceLogger
	 */
	public TroveCliquePostgresSummarizerImpl(final SqlConnectionHandler connHandler, final TroveRdfComponentSummarizer rdfComponentSummarizer,
			final UriGenerator uriGenerator, final DmlDao dmlDao, final DictionaryDao dictDao, final RdfTablesDao rdfTablesDao, final PerformanceLogger performanceLogger) {
		this.connHandler = connHandler;
		RdfComponentSummarizer = rdfComponentSummarizer;
		UriGenerator = uriGenerator;
		DmlDao = dmlDao;
		DictDao = dictDao;
		RdfTablesDao = rdfTablesDao;
		PerformanceLogger = performanceLogger;
		timer = PerformanceLogger.newTimer();
	}

	/**
	 * Tables assumed encoded. 
	 */
	@Override
	public TroveCliquePostgresSummaryResult summarize(final String summaryType, final String dataTable, final String typesTable, final String schemaTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.WEAK) || summaryType.equals(SummaryType.TYPED_WEAK) ||
				summaryType.equals(SummaryType.STRONG) || summaryType.equals(SummaryType.TYPED_STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		
		PerformanceLogger.reset();
		rdfSummary = new TroveRdfSummaryImpl(UriGenerator, DictDao);
		final String schemaName = NameUtils.getSchemaName(dataTable);
		classNodesTable = NameUtils.getClassNodesTableName(schemaName);
		propertyNodesTable = NameUtils.getPropertyNodesTableName(schemaName);
		final String schemaPropertyNodesTable = NameUtils.getSchemaPropertyNodesTableName(schemaName);
		final String possiblePropertyNodesTable = propertyNodesTable + "_possible";
		
		RdfTablesDao.recreateEncodedUriTable(classNodesTable);	
		RdfTablesDao.recreateEncodedUriTable(propertyNodesTable);
		RdfTablesDao.recreateEncodedUriTable(schemaPropertyNodesTable);
		RdfTablesDao.recreateEncodedUriTable(possiblePropertyNodesTable);
		
		PerformanceLogger.startSummarizationTimer();
		RdfTablesDao.extractAllClassNodes(classNodesTable, typesTable, schemaTable);
		RdfTablesDao.extractAllPropertyNodes(propertyNodesTable, schemaPropertyNodesTable, possiblePropertyNodesTable, dataTable, typesTable, schemaTable);
		loadClassNodes(rdfSummary, classNodesTable);
		loadPropertyNodes(rdfSummary, propertyNodesTable);
		
		TroveCliquePostgresSummaryResult result = null;
		if (summaryType.equals(SummaryType.WEAK)) 
			result = this.generateWeakPureCliqueEquivalenceSummary(dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_WEAK)) 
			result = this.generateWeakTypeEquivalenceSummary(dataTable, typesTable);
		if (summaryType.equals(SummaryType.STRONG))
			result = this.generateStrongPureCliqueEquivalenceSummary(dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_STRONG))
			result = this.generateStrongTypeEquivalenceSummary(dataTable, typesTable);
		
		PerformanceLogger.stopSummarizationTimer();
		
		return result;
	}

	/**
	 * Loads class nodes from the table into the summary.
	 * 
	 * @param rdfSummary
	 * @param classNodesTable
	 * @throws SQLException
	 */
	private void loadClassNodes(RdfSummary rdfSummary, String classNodesTable) throws SQLException {
		timer.reset();
		timer.start();
		final ResultSet resultSet = DmlDao.getSelectionResultSet(new String[] { Constant.URI }, true, classNodesTable, null);
		while (resultSet.next()) { 
			rdfSummary.addClassNode(resultSet.getInt(1));
		}
		timer.stop();
		log.info("Class nodes loaded to memory in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.LOAD_CLASS_NODES_TO_MEMORY, timer.getTimeInMilliseconds());
	}
	
	/**
	 * Loads property nodes from the table into the summary.
	 * 
	 * @param rdfSummary
	 * @param propertyNodesTable
	 * @throws SQLException
	 */
	private void loadPropertyNodes(RdfSummary rdfSummary, String propertyNodesTable) throws SQLException {
		timer.reset();
		timer.start();
		final ResultSet resultSet = DmlDao.getSelectionResultSet(new String[] { Constant.URI }, true, propertyNodesTable, null);
		while (resultSet.next()) {
			rdfSummary.addPropertyNode(resultSet.getInt(1));
		}
		timer.stop();
		log.info("Property nodes loaded to memory in: " + timer.getTimeAsString());
		PerformanceLogger.addTiming(PerformanceLabel.LOAD_PROPERTY_NODES_TO_MEMORY, timer.getTimeInMilliseconds());
	}
	
	@Override
	public TroveCliquePostgresSummaryResult generateWeakPureCliqueEquivalenceSummary(final String dataTable,
			final String typesTable) throws SQLException, DictionaryException, InexistentValueException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));
		
		log.info(getSummaryTypeMessage(SummaryType.WEAK));
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		weakPureSummary = new WeakPureCliqueEquivalenceSummaryImpl(weakEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(weakPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(weakPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveCliquePostgresSummaryResult(SummaryType.WEAK, weakPureSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	public TroveCliquePostgresSummaryResult generateStrongPureCliqueEquivalenceSummary(final String dataTable, final String typesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(SummaryType.STRONG));
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		strongPureSummary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(strongPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(strongPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveCliquePostgresSummaryResult(SummaryType.STRONG, strongPureSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	@Override
	public TroveCliquePostgresSummaryResult generateWeakTypeEquivalenceSummary(final String dataTable, final String typesTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(SummaryType.TYPED_WEAK));
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		weakTypeSummary = new WeakTypeEquivalenceSummaryImpl(weakEquivSummary, typeEquivSummary, rdfSummary); 

		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(weakTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(weakTypeSummary, dataTable);					
		connHandler.enableAutoCommit();
		
		return new TroveCliquePostgresSummaryResult(SummaryType.TYPED_WEAK, weakTypeSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}

	@Override
	public TroveCliquePostgresSummaryResult generateStrongTypeEquivalenceSummary(final String dataTable, final String typesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(SummaryType.TYPED_STRONG));
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		strongTypeSummary = new StrongTypeEquivalenceSummaryImpl(strongEquivSummary, typeEquivSummary, rdfSummary); 
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(strongTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(strongTypeSummary, dataTable, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveCliquePostgresSummaryResult(SummaryType.TYPED_STRONG, strongTypeSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	private String getSummaryTypeMessage(final String summaryType) {
		Builder = new StringBuilder("Building the ");
		Builder.append(summaryType).append(" summary...");		
		
		return Builder.toString();
	}
}
