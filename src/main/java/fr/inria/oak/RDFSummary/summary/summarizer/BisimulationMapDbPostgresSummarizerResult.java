package fr.inria.oak.RDFSummary.summary.summarizer;

import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.timing.Timings;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationMapDbPostgresSummarizerResult {

	private final BisimulationRdfSummary summary;
	private final String summaryName;
	private final String summaryTriplesFilepath;
	private final Timings timings;
	
	/**
	 * @param summary
	 * @param summaryName
	 * @param summaryTriplesFilepath
	 * @param timings
	 */
	public BisimulationMapDbPostgresSummarizerResult(final BisimulationRdfSummary summary,
			final String summaryName, final String summaryTriplesFilepath, final Timings timings) {
		this.summary = summary;
		this.summaryName = summaryName;
		this.summaryTriplesFilepath = summaryTriplesFilepath;
		this.timings = timings;
	}
	
	public BisimulationRdfSummary getSummary() {
		return summary;
	}
	
	public String getSummaryName() {
		return summaryName;
	}
	
	public String getSummaryTriplesFilepath() {
		return summaryTriplesFilepath;
	}
	
	public Timings getTimings() {
		return timings;
	}
}
