package fr.inria.oak.RDFSummary.summary.export.dot;

import java.io.IOException;

import fr.inria.oak.RDFSummary.summary.stats.clique.Stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DotSummaryViz {

	/**
	 * @param id
	 * @param label
	 * @param shape
	 * @param style
	 * @param color
	 * @return
	 */
	public String getDotNode(int id, String label, String shape, String style, String color);

	/**
	 * @param schemaName
	 * @param stats
	 * @return
	 */
	public String getGraphLabel(String schemaName, Stats stats);

	/**
	 * @param edgeColor
	 * @return
	 */
	public String getEdgeColor(String edgeColor);

	/**
	 * @param shortPropertyIRI
	 * @return
	 */
	public String getEdgeLabel(String shortPropertyIRI);

	/**
	 * @param sourceLabel
	 * @param targetLabel
	 * @param edgeColor
	 * @param edgeStyle
	 * @param edgeLabel
	 * @return
	 */
	public String getDotTriple(String sourceLabel, String targetLabel, String edgeColor, String edgeStyle, String edgeLabel);

	/**
	 * @param sourceLabel
	 * @param targetLabel
	 * @param property
	 * @param edgeColor
	 * @param schemaLabelColor
	 * @return
	 */
	public String getSchemaTriple(String sourceLabel, String targetLabel, final String property, final String edgeColor, final String schemaLabelColor);

	/**
	 * @param dotFilepath
	 * @param graphName
	 * @param graphLabel
	 * @throws IOException
	 */
	public void startDotGraph(final String dotFilepath, final String graphName, final String graphLabel) throws IOException;

	/**
	 * Writes an entry to a line of the dot file
	 * 
	 * @param entry
	 */
	public void writeToFile(final String entry);

	public void endDotGraph();
}
