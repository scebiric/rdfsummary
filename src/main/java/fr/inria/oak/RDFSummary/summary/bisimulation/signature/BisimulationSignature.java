package fr.inria.oak.RDFSummary.summary.bisimulation.signature;

import java.sql.SQLException;
import java.util.TreeSet;

import org.mapdb.Fun.Tuple2;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationSignature {
	
	/**
	 * Computes the representative of gDataNode based on its signature.
	 * 
	 * @param gDataNode
	 * @return The representative of gDataNode
	 * @throws SQLException 
	 */
	public int getSummaryNode(int gDataNode) throws SQLException; 
	
	/**
	 * @param gDataNode
	 * @return {@link TreeSet} of (p,o) pairs such that gDataNode is the subject of the triple s p o
	 * @throws SQLException 
	 */
	public TreeSet<Tuple2<Integer, Integer>> computeForwardSignature(int gDataNode) throws SQLException;
	
	/**
	 * @param gDataNode
	 * @return {@link TreeSet} of (p,s) pairs such that gDataNode is the object of the triple s p o
	 * @throws SQLException 
	 */
	public TreeSet<Tuple2<Integer, Integer>> computeBackwardSignature(int gDataNode) throws SQLException;
	
	public void clearSignatures();
}
