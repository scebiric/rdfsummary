package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.util.Iterator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TypeEquivalenceSummary extends CliqueRdfSummary {
	
	/**
	 * Creates a type triple from sNode to each of the specified classes
	 * 
	 * @param sNode
	 * @param classes
	 */
	public void createTypeTriples(int sNode, java.util.Iterator<Integer> classes);
	
	/** Returns the summary data node having all the specified classes or null if such node does not exist */
	public Integer getSummaryDataNodeByClasses(Iterator<Integer> classes);
	
	/** Returns true if the specified summary data node is typed, otherwise false 
	 * 
	 * */
	public boolean isTypedDataNode(int sDataNode);

	/** Stores the highest typed summary node (class, property or data node) 
	 * 
	 * */
	public void setHighestTypedDataNode(int sNode);
	
	/**
	 * Stores the summary data node representing input data nodes having all the specified classes.
	 * 
	 * @param classes
	 * @param sDataNode
	 * 
	 */
	public void storeSummaryDataNodeByClassSet(Iterator<Integer> classes, int sDataNode);
}
