package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface WeakPureCliqueEquivalenceSummary extends WeakEquivalenceSummary, PureCliqueEquivalenceSummary {
}
