package fr.inria.oak.RDFSummary.summary.bisimulation.signature.trie;

/**
 * 
 * @author Sejla CEBIRIC
 * @param <Value>
 *
 */
public interface Trie<Value> {

	 public int size();
	 
	 public boolean contains(String key);
	 
	 public Value get(String key);
	 
	 public void put(String key, Value val);
	 
	 public String longestPrefixOf(String query);
	 
	 public Iterable<String> keys();
}
