package fr.inria.oak.RDFSummary.summary.summarizer;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationSummarizer {

	public void summarize() throws SQLException, SignatureException;
}
