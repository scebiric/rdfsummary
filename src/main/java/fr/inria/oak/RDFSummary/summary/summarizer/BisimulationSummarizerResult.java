package fr.inria.oak.RDFSummary.summary.summarizer;

import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationSummarizerResult {

	private final BisimulationRdfSummary bisimulationSummary;
	private final int bisimulationIterations;
	
	/**
	 * @param bisimulationSummary
	 * @param bisimulationIterations
	 */
	public BisimulationSummarizerResult(final BisimulationRdfSummary bisimulationSummary, final int bisimulationIterations) {
		this.bisimulationSummary = bisimulationSummary;
		this.bisimulationIterations = bisimulationIterations;
	}
	
	public BisimulationRdfSummary getBisimulationSummary() {
		return bisimulationSummary;
	}
	
	public int getBisimulationIterations() {
		return bisimulationIterations;
	}
}
