package fr.inria.oak.RDFSummary.summary.cliquebuilder.trove;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.PerformanceLabel;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.SetUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Implements {@link TroveCliqueBuilder}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveCliqueBuilderImpl implements TroveCliqueBuilder {

	private static final Logger log = Logger.getLogger(TroveCliqueBuilder.class);

	private static RdfTablesDao RdfTablesDao;
	private static PerformanceLogger PerformanceLogger;

	private final Timer timer;
	private static StringBuilder Builder;
	private ResultSet result = null;
	private TIntSet properties = null;

	private static final String BUILD_SOURCE_CLIQUES_MESSAGE = "Source cliques built in: ";
	private static final String BUILD_TARGET_CLIQUES_MESSAGE = "Target cliques built in: ";
	/**
	 * @param rdfTablesDao
	 * @param performanceLogger
	 */
	public TroveCliqueBuilderImpl(final RdfTablesDao rdfTablesDao, final PerformanceLogger performanceLogger) {
		RdfTablesDao = rdfTablesDao;
		PerformanceLogger = performanceLogger;

		timer = PerformanceLogger.newTimer();
	}

	@Override
	public void buildSourceCliques(final String dataTable, final StrongPureCliqueEquivalenceSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkNotNull(summary);

		log.info("Summarizing source cliques...");
		timer.reset();
		timer.start();
		result = RdfTablesDao.distinctSpOrderByS(dataTable);
		if (!result.next()) 
			log.info("No source cliques.");
		else 
			this.computeSourceCliques(summary, result);
	
		result.close();
		timer.stop();
		log.info(getSourceCliquesMessage(timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.BUILD_SOURCE_CLIQUES, timer.getTimeInMilliseconds());
	}

	@Override
	public void buildSourceCliques(final String dataTable, final String typesTable, final StrongTypeEquivalenceSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkNotNull(summary);

		log.info("Summarizing source cliques...");
		timer.reset();
		timer.start();
		result = RdfTablesDao.distinctSpOrderBySWithSUntyped(dataTable, typesTable);
		if (!result.next()) 
			log.info("No source cliques for untyped subjects.");
		else
			this.computeSourceCliques(summary, result);
		
		result.close();
		timer.stop();
		log.info(getSourceCliquesMessage(timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.BUILD_SOURCE_CLIQUES, timer.getTimeInMilliseconds());
	}

	@Override
	public void buildTargetCliques(final String dataTable, final StrongPureCliqueEquivalenceSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkNotNull(summary);

		log.info("Summarizing target cliques...");
		timer.reset();
		timer.start();
		result = RdfTablesDao.distinctOpOrderByO(dataTable);
		if (!result.next()) 
			log.info("No target cliques.");
		else
			this.computeTargetCliques(summary, result);
		
		result.close();
		timer.stop();
		log.info(getTargetCliquesMessage(timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.BUILD_TARGET_CLIQUES, timer.getTimeInMilliseconds());
	}

	@Override
	public void buildTargetCliques(final String dataTable, final String typesTable, final StrongTypeEquivalenceSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkNotNull(summary);

		log.info("Summarizing target cliques...");
		timer.reset();
		timer.start();
		result = RdfTablesDao.distinctOpOrderByOWithOUntyped(dataTable, typesTable);
		if (!result.next()) 
			log.info("No target cliques for untyped objects.");
		else
			this.computeTargetCliques(summary, result);
		
		result.close();
		timer.stop();
		log.info(getTargetCliquesMessage(timer.getTimeAsString()));
		PerformanceLogger.addTiming(PerformanceLabel.BUILD_TARGET_CLIQUES, timer.getTimeInMilliseconds());
	}

	/**
	 * Assumes rows are ordered by subject.
	 * 
	 * @param summary
	 * @param resultSet
	 * @throws SQLException
	 */
	private void computeSourceCliques(final StrongEquivalenceSummary summary, final ResultSet result) throws SQLException {
		int subject = result.getInt(1);
		properties = new TIntHashSet();
		properties.add(result.getInt(2));							
		while (result.next()) {		
			if (result.getInt(1) == subject) 					
				properties.add(result.getInt(2));
			else {	
				this.computeSourceClique(summary, subject, properties);
				subject = result.getInt(1);
				properties = new TIntHashSet();
				properties.add(result.getInt(2));						
			}				
		}

		if (properties.size() > 0) 
			this.computeSourceClique(summary, subject, properties);
	}

	/**
	 * Assumes rows are ordered by object.
	 * 
	 * @param summary
	 * @param result
	 * @throws SQLException
	 */
	private void computeTargetCliques(final StrongEquivalenceSummary summary, final ResultSet result) throws SQLException {
		int object = result.getInt(1);
		properties = new TIntHashSet();
		properties.add(result.getInt(2));							
		while (result.next()) {					
			if (result.getInt(1) == object) 					
				properties.add(result.getInt(2));
			else {	
				this.computeTargetClique(summary, object, properties);
				object = result.getInt(1);
				properties = new TIntHashSet();
				properties.add(result.getInt(2));						
			}				
		}

		if (properties.size() > 0) 
			this.computeTargetClique(summary, object, properties);	
	}

	@Override
	public void computeSourceClique(final StrongEquivalenceSummary summary, final int subject, final TIntSet sProps) {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(sProps);

		List<TIntSet> sourceCliques = summary.getSourceCliques();
		TIntSet sourceClique = null;
		int commonElement = -1;
		for (TIntSet clique : sourceCliques) {
			commonElement = SetUtils.findFirstCommonElement(clique, sProps);
			if (commonElement == -1)
				continue;

			sourceClique = clique;
			break; // There is only one clique with which sProps can intersect and we have found it	
		}

		if (sourceClique == null) {
			sourceClique = new TIntHashSet(sProps);
			sourceCliques.add(sourceClique);
		}
		else
			sourceClique.addAll(sProps);

		summary.setSourceCliqueIdForSubject(subject, sourceCliques.indexOf(sourceClique) + 1);
	}

	@Override
	public void computeTargetClique(final StrongEquivalenceSummary summary, final int object, final TIntSet oProps) {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(oProps);

		List<TIntSet> targetCliques = summary.getTargetCliques();
		TIntSet targetClique = null;
		int commonElement = -1;
		for (TIntSet clique : targetCliques) {
			commonElement = SetUtils.findFirstCommonElement(clique, oProps);
			if (commonElement == -1)
				continue;

			targetClique = clique;
			break; // There is only one clique with which sProps can intersect and we have found it	
		}

		if (targetClique == null) {
			targetClique = new TIntHashSet(oProps);
			targetCliques.add(targetClique);
		}
		else
			targetClique.addAll(oProps);

		summary.setTargetCliqueIdForObject(object, targetCliques.indexOf(targetClique) + 1);
	}

	private String getSourceCliquesMessage(String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(BUILD_SOURCE_CLIQUES_MESSAGE).append(timeAsString);

		return Builder.toString();
	}

	private String getTargetCliquesMessage(String timeAsString) {
		Builder = new StringBuilder();
		Builder.append(BUILD_TARGET_CLIQUES_MESSAGE).append(timeAsString);

		return Builder.toString();
	}
}
