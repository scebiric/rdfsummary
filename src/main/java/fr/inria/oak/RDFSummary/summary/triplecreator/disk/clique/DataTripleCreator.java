package fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique;

import java.sql.SQLException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DataTripleCreator {

	/**
	 * @param subject
	 * @param property
	 * @param object
	 * @throws SQLException 
	 */
	public void representDataTriple(int subject, int property, int object) throws SQLException;
	
	/**
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the source of dataProperty
	 * @throws SQLException 
	 */
	public int getRepresentativeSourceDataNode(final int subject, final int dataProperty) throws SQLException;
	
	/**
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the target of dataProperty
	 * @throws SQLException 
	 */
	public int getRepresentativeTargetDataNode(final int object, final int dataProperty) throws SQLException;
}
