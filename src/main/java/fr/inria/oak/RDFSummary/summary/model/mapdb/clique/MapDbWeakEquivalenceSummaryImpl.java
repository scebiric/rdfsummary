package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;

import org.apache.log4j.Logger;
import org.mapdb.BTreeKeySerializer;
import org.mapdb.DB;
import org.mapdb.Fun;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.unionfind.WeightedQuickUnionUF;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;

import org.mapdb.Serializer;

import com.google.common.collect.Maps;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbWeakEquivalenceSummaryImpl implements WeakEquivalenceSummary {

	private static final Logger log = Logger.getLogger(WeakEquivalenceSummary.class);

	private final CliqueRdfSummary rdfSummary;
	private final UriGenerator uriGenerator;
	private final Dictionary dictionary;
	private final Timer timer;

	private final Map<Integer, Integer> subjectByDataProperty;
	private final Map<Integer, Integer> objectByDataProperty;
	private final NavigableSet<Tuple2<Integer, Integer>> dataPropertiesBySubject;
	private final NavigableSet<Tuple2<Integer, Integer>> dataPropertiesByObject;
	private final NavigableSet<Tuple2<Integer, Integer>> nodePairsToUnify;
	private final NavigableSet<Tuple3<Integer, Integer, Integer>> dataTriplesUpdated;
	private final NavigableSet<Tuple2<Integer, Integer>> classesBySummaryNodeUpdated; // summary node can be any node

	private static final String SUBJECT_BY_DATA_PROPERTY = "subject_by_data_property";
	private static final String OBJECT_BY_DATA_PROPERTY = "object_by_data_property";
	private static final String DATA_PROPERTIES_BY_SUBJECT = "data_properties_by_subject";
	private static final String DATA_PROPERTIES_BY_OBJECT = "data_properties_by_object";
	private static final String NODE_PAIRS_TO_UNIFY = "node_pairs_to_unify";
	private static final String DATA_TRIPLES_UPDATED = "data_triples_weak_summary_updated";
	private static final String CLASSES_BY_SUMMARY_NODE_UPDATED = "classes_by_summary_node_updated";

	private final MessageBuilder messageBuilder;
	private final int[] pair;
	private int seed;
	private Map<Integer, Integer> idBySummaryNode;

	/**
	 * @param summaryDb
	 * @param rdfSummary
	 * @param uriGenerator
	 * @param dictionary
	 * @param timer
	 */
	public MapDbWeakEquivalenceSummaryImpl(final DB summaryDb, final CliqueRdfSummary rdfSummary, final UriGenerator uriGenerator, final Dictionary dictionary, final Timer timer) {
		this.rdfSummary = rdfSummary;	
		this.uriGenerator = uriGenerator;
		this.dictionary = dictionary;
		this.timer = timer;

		subjectByDataProperty = summaryDb.createHashMap(SUBJECT_BY_DATA_PROPERTY).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
		objectByDataProperty = summaryDb.createHashMap(OBJECT_BY_DATA_PROPERTY).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
		dataPropertiesBySubject = summaryDb.createTreeSet(DATA_PROPERTIES_BY_SUBJECT).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();
		dataPropertiesByObject = summaryDb.createTreeSet(DATA_PROPERTIES_BY_OBJECT).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();
		nodePairsToUnify = summaryDb.createTreeSet(NODE_PAIRS_TO_UNIFY).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();
		dataTriplesUpdated = summaryDb.createTreeSet(DATA_TRIPLES_UPDATED).counterEnable().serializer(BTreeKeySerializer.TUPLE3).make();
		classesBySummaryNodeUpdated = summaryDb.createTreeSet(CLASSES_BY_SUMMARY_NODE_UPDATED).counterEnable().serializer(BTreeKeySerializer.TUPLE2).make();

		messageBuilder = new MessageBuilder();
		pair = new int[2];
		seed = 0;
		idBySummaryNode = Maps.newHashMap();
	}	

	@Override
	public int unifyDataNodes(final int dataNode1, final int dataNode2) {	
		pair[0] = dataNode1;
		pair[1] = dataNode2;
		Arrays.sort(pair);
		
		nodePairsToUnify.add(new Tuple2<Integer, Integer>(getId(pair[0]), getId(pair[1])));

		return pair[1];
	}

	private int getId(final int summaryNode) {
		Integer id = idBySummaryNode.get(summaryNode);
		if (id == null) {
			id = seed++;
			idBySummaryNode.put(summaryNode, id);
		}	
		
		return id.intValue();
	}

	@Override
	public void unifyDataNodes() throws SQLException {
		timer.reset();
		
		if (nodePairsToUnify.size() == 0) {
			log.info("No nodes to unify.");
			return;
		}
		
		timer.start();
		log.info("Unifying data nodes...");
		final int n = idBySummaryNode.size();
		WeightedQuickUnionUF uf = new WeightedQuickUnionUF(n);
		final Iterator<Tuple2<Integer, Integer>> pairs = nodePairsToUnify.iterator();
		Tuple2<Integer, Integer> pair;
		while (pairs.hasNext()) {
			pair = pairs.next();
			uf.union(pair.a.intValue(), pair.b.intValue());
		}
		
		// Each component corresponds to a distinct new summary node
		final TIntIntMap summaryNodeByComponent = new TIntIntHashMap();
		for (int i = 0; i < uf.count(); i++) {
			final int sDataNode = dictionary.insert(uriGenerator.generateDataNodeUri(dictionary.getSeed()));
			summaryNodeByComponent.put(i, sDataNode);
		}
		
		// All nodes in a component are replaced by the same (new) summary node
		// vanishing node (sNode) -> nodeId (from 0 to n-1) -> component -> new sNode
		final TIntIntMap summaryNodeByVanishingNode = new TIntIntHashMap();
		int id;
		int component;
		int newSummaryNode;
		for (final int vanishingNode : idBySummaryNode.keySet()) {
			id = idBySummaryNode.get(vanishingNode).intValue();
			component = uf.find(id);
			newSummaryNode = summaryNodeByComponent.get(component);
			summaryNodeByVanishingNode.put(vanishingNode, newSummaryNode);
		}
		
		updateDataTriples(summaryNodeByVanishingNode);
		updateTypeTriples(summaryNodeByVanishingNode);
		timer.stop();

		log.info(messageBuilder.append("Unification time: ").append(timer.getTimeAsString()));
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		if (dataTriplesUpdated.size() == 0)
			return rdfSummary.getAllDataTriples();
		
		return dataTriplesUpdated.iterator();				
	}
	
	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		if (classesBySummaryNodeUpdated.size() == 0)
			return rdfSummary.getAllTypeTriples();
		
		return classesBySummaryNodeUpdated.iterator();
	}

	private Iterator<Tuple3<Integer, Integer, Integer>> updateDataTriples(final TIntIntMap summaryNodeByVanishingNode) {
		final Iterator<Tuple3<Integer, Integer, Integer>> tripleIterator = rdfSummary.getAllDataTriples();
		Tuple3<Integer, Integer, Integer> tuple;
		int subject;
		int object;
		while (tripleIterator.hasNext()) {
			tuple = tripleIterator.next();
			subject = summaryNodeByVanishingNode.get(tuple.a.intValue());
			object = summaryNodeByVanishingNode.get(tuple.c.intValue());
			if (subject == 0)
				subject = tuple.a.intValue();
			if (object == 0)
				object = tuple.c.intValue();

			dataTriplesUpdated.add(new Tuple3<Integer, Integer, Integer>(subject, tuple.b.intValue(), object));
		}

		return dataTriplesUpdated.iterator();
	}

	private void updateTypeTriples(final TIntIntMap newNodeByVanishingNode) {
		final Iterator<Tuple2<Integer, Integer>> tripleIterator = rdfSummary.getAllTypeTriples();
		Tuple2<Integer, Integer> tuple;
		int subject;
		while (tripleIterator.hasNext()) {
			tuple = tripleIterator.next();
			subject = newNodeByVanishingNode.get(tuple.a.intValue());
			if (subject == 0)
				subject = tuple.a.intValue();

			classesBySummaryNodeUpdated.add(new Tuple2<Integer, Integer>(subject, tuple.b.intValue()));
		}
	}
	
	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}

	@Override
	public void setSubjectForDataProperty(final int dataProperty, final int dataNode) {
		subjectByDataProperty.put(dataProperty, dataNode);
		dataPropertiesBySubject.add(new Tuple2<Integer, Integer>(dataNode, dataProperty));
	}

	@Override
	public void setObjectForDataProperty(final int dataProperty, final int dataNode) {
		objectByDataProperty.put(dataProperty, dataNode);
		dataPropertiesByObject.add(new Tuple2<Integer, Integer>(dataNode, dataProperty));
	}

	@Override
	public Integer getSubjectForDataProperty(final int dataProperty) {
		return subjectByDataProperty.get(dataProperty);
	}

	@Override
	public Integer getObjectForDataProperty(final int dataProperty) {
		return objectByDataProperty.get(dataProperty);
	}

	@Override
	public Iterator<Integer> getDataPropertiesBySubject(int dataNode) {
		return Fun.filter(dataPropertiesBySubject, dataNode).iterator();
	}

	@Override
	public Iterator<Integer> getDataPropertiesByObject(int dataNode) {
		return Fun.filter(dataPropertiesByObject, dataNode).iterator();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		rdfSummary.createDataTriple(source, dataProperty, target);
	}

	@Override
	public Iterator<Integer> getRepresentedDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedDataNodes(sDataNode);
	}

	@Override
	public Integer getSummaryDataNodeByGDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeByGDataNode(gDataNode);
	}

	@Override
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sNode, classes);
	}

	@Override
	public Iterator<Integer> getClassesBySummaryNode(int sNode) {
		return rdfSummary.getClassesBySummaryNode(sNode);
	}
	
	@Override
	public int getDataTriplesCount() {
		return rdfSummary.getDataTriplesCount();
	}

	@Override
	public int getTypeTriplesCount() {
		return rdfSummary.getTypeTriplesCount();
	}
}
