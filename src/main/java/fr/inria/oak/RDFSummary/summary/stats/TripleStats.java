package fr.inria.oak.RDFSummary.summary.stats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TripleStats {

	private final int schemaTripleCount; 
	private final int dataTripleCount;
	private final int typeTripleCount;
		
	/**
	 * @param schemaTripleCount
	 * @param dataTripleCount
	 * @param typeTripleCount
	 */
	public TripleStats(final int schemaTripleCount, final int dataTripleCount, final int typeTripleCount) {
		this.schemaTripleCount = schemaTripleCount;
		this.dataTripleCount = dataTripleCount;
		this.typeTripleCount = typeTripleCount;
	}

	public int getSchemaTripleCount() {
		return schemaTripleCount;
	}
	
	public int getDataTripleCount() {
		return dataTripleCount;
	}
	
	public int getTypeTripleCount() {
		return typeTripleCount;
	}
	
	public int getNonSchemaTripleCount() {
		return dataTripleCount + typeTripleCount;
	}
	
	public int getTotalTripleCount() {
		return dataTripleCount + typeTripleCount + schemaTripleCount;
	}
}
