package fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation;

import java.util.Set;

import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationRdfSummary extends RdfSummary {
	
	/**
	 * @return The number of distinct data nodes in the summary
	 */
	public int getDataNodeCount();
	
	/**
	 * @return An iterator over the set of data nodes
	 */
	public Set<Integer> getDataNodes();
	
	/**
	 * @param gDataNode
	 * @return sDataNode representing gDataNode, or null if there is no such node
	 */
	public Integer getSummaryDataNodeByGDataNode(int gDataNode);
	
	/**
	 * @param gDataNode
	 * @param sDataNode
	 */
	public void setSummaryDataNodeByGDataNode(int gDataNode, int sDataNode);
}
