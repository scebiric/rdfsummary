package fr.inria.oak.RDFSummary.summary.export;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.Fun.Tuple3;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Extension;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.export.dot.DotSummaryViz;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.DotUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresBisimulationDatasetRdfExporterImpl implements RdfExporter {

	private static final Logger log = Logger.getLogger(RdfExporter.class);

	private final String dbName;
	private final MapDbManager dbManager;
	private final PostgresBisimulationRdfDataset rdfDataset;
	private final Dictionary dictionary;	
	private final DotSummaryViz dotViz;
	private final TriplesWriter triplesWriter;
	private final Timer timer;

	private static final String DOT_NODES = "dot_nodes";
	private StringBuilder sb;

	/**
	 * @param dbName
	 * @param dbManager
	 * @param rdfDataset
	 * @param dictionary
	 * @param dotViz
	 * @param triplesWriter
	 * @param uriGenerator
	 * @param timer
	 */
	public PostgresBisimulationDatasetRdfExporterImpl(final String dbName, final MapDbManager dbManager, 
			final PostgresBisimulationRdfDataset rdfDataset, final Dictionary dictionary, 
			final DotSummaryViz dotViz, final TriplesWriter triplesWriter, final Timer timer) {
		this.dbName = dbName;
		this.dbManager = dbManager;
		this.rdfDataset = rdfDataset;
		this.dictionary = dictionary;
		this.dotViz = dotViz;
		this.triplesWriter = triplesWriter;
		this.timer = timer;
	}

	@Override
	public ExportResult exportToFiles(final ExportParams exportParams, final String inputGraphName, final String outputGraphName) throws IOException, SQLException {
		Preconditions.checkNotNull(exportParams);
		Preconditions.checkArgument(exportParams.generateDot() || exportParams.exportToRdfFile());
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(inputGraphName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputGraphName));

		timer.reset();
		timer.start();
		log.info("Exporting RDF dataset to files...");
		Utils.mkDirs(exportParams.getOutputFolder());		
		final String dotFilepath = Utils.getFilepath(exportParams.getOutputFolder(), outputGraphName, Extension.DOT);
		final String rdfFilepath = Utils.getFilepath(exportParams.getOutputFolder(), outputGraphName, Extension.N_TRIPLES);

		DotStyleParams dotParams = null;
		if (exportParams.generateDot()) {
			dotParams = exportParams.getDotStyleParams();		

			// Start DOT file
			String graphLabel = "";
//			if (dotParams.showGraphLabel()) 
//				graphLabel = dotViz.getGraphLabel(inputGraphName, null);

			dotViz.startDotGraph(dotFilepath, getGraphName(outputGraphName), graphLabel);
		}

		if (exportParams.exportToRdfFile()) {
			// Start n-triples file
			triplesWriter.startFile(rdfFilepath);
		}

		final Set<Integer> dotNodes = dbManager.createEmptyHashSet(dbName, DOT_NODES);

		// Write data triples
		int subject;
		int property;
		int object;
		String decodedProperty;		
		String edgeLabel;
		String edgeColor = null;
		String edgeStyle = null;		
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getDataPropertyEdgeColor());;
			edgeStyle = dotParams.getDataPropertyEdgeStyle();
		}

		final ResultSet dataTriples = rdfDataset.getDataTriples();
		while (dataTriples.next()) {
			subject = dataTriples.getInt(1);
			property = dataTriples.getInt(2);
			object = dataTriples.getInt(3);
			
			if (exportParams.generateDot()) {				
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				edgeLabel = dotViz.getEdgeLabel(RdfUtils.stripNamespace(dictionary.getValue(property)));
				dotViz.writeToFile(dotViz.getDotTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), edgeColor, edgeStyle, edgeLabel));
			}

			if (exportParams.exportToRdfFile()) {
				triplesWriter.write(dictionary.getValue(subject), dictionary.getValue(property), dictionary.getValue(object));
			}	
		}
		dataTriples.close();

		// Write type triples
		edgeLabel = "\"\"";
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getTypeEdgeColor());
			edgeStyle = dotParams.getTypeEdgeStyle();
		}

		final ResultSet typeTriples = rdfDataset.getTypeTriples();
		while (typeTriples.next()) {
			subject = typeTriples.getInt(1);
			object = typeTriples.getInt(2);

			if (exportParams.generateDot()) {				
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				dotViz.writeToFile(dotViz.getDotTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), edgeColor, edgeStyle, edgeLabel));
			}

			if (exportParams.exportToRdfFile()) {				
				triplesWriter.write(dictionary.getValue(subject), Rdf.FULL_TYPE, dictionary.getValue(object));
			}	
		}

		// Write schema triples
		if (exportParams.generateDot()) {
			edgeColor = dotViz.getEdgeColor(dotParams.getSchemaEdgeColor());
		}
		
		final Iterator<Tuple3<Integer, Integer, Integer>> schemaTriples = rdfDataset.getSchema().getAllTriples();
		Tuple3<Integer, Integer, Integer> schemaTriple;
		while (schemaTriples.hasNext()) {
			schemaTriple = schemaTriples.next();
			subject = schemaTriple.a.intValue();
			property = schemaTriple.b.intValue();
			object = schemaTriple.c.intValue();

			decodedProperty = dictionary.getValue(property);
			
			if (exportParams.generateDot()) {
				writeDotNodeIfNotExists(subject, dotNodes, dotParams);
				writeDotNodeIfNotExists(object, dotNodes, dotParams);

				dotViz.writeToFile(dotViz.getSchemaTriple(DotUtils.getDotNodeId(subject), DotUtils.getDotNodeId(object), decodedProperty, edgeColor, dotParams.getSchemaLabelColor()));
			}

			if (exportParams.exportToRdfFile()) {								 
				triplesWriter.write(dictionary.getValue(subject), decodedProperty, dictionary.getValue(object));
			}
		}		

		if (exportParams.generateDot()) 
			dotViz.endDotGraph();

		if (exportParams.exportToRdfFile())
			triplesWriter.endFile();

		timer.stop();
		if (exportParams.generateDot() || exportParams.exportToRdfFile())
			log.info("Decoded triples exported to file(s) in " + timer.getTimeAsString());
		if (exportParams.generateDot())
			log.info("DOT filepath: " + dotFilepath);
		if (exportParams.exportToRdfFile())
			log.info("RDF filepath: " + rdfFilepath);

		if (exportParams.generateDot() && exportParams.convertDotToPdf())
			Utils.dotToPdf(dotFilepath, exportParams.getDotExecutableFilepath());

		if (exportParams.generateDot() && exportParams.convertDotToPng())
			Utils.dotToPng(dotFilepath, exportParams.getDotExecutableFilepath());
		
		final ExportResult exportResult = new ExportResult();
		if (exportParams.generateDot())
			exportResult.setDotFilepath(dotFilepath);
		if (exportParams.exportToRdfFile())
			exportResult.setRdfFilepath(rdfFilepath);
		
		return exportResult;
	}

	private void writeDotNodeIfNotExists(final int summaryNode, final Set<Integer> dotNodes, final DotStyleParams dotParams) throws SQLException {
		if (!dotNodes.contains(summaryNode)) { 
			dotViz.writeToFile(getDotNodeEntry(summaryNode, dotParams));
			dotNodes.add(summaryNode);
		}		
	}

	/**
	 * @param nodeId
	 * @param dotParams
	 * @return Dot entry for the node based on whether it is a data, class, property or class and property node.
	 * @throws SQLException 
	 */
	private String getDotNodeEntry(int nodeId, DotStyleParams dotParams) throws SQLException {
		if (!rdfDataset.isClassOrProperty(nodeId)) {
			// Data node			
			return dotViz.getDotNode(nodeId, RdfUtils.stripNamespace(dictionary.getValue(nodeId)), 
					dotParams.getDataNodeShape(), dotParams.getDataNodeStyle(), dotParams.getDataNodeColor());
		}
		else if (rdfDataset.isClass(nodeId) && !rdfDataset.isProperty(nodeId)) {
			// Class node
			return dotViz.getDotNode(nodeId, RdfUtils.stripNamespace(dictionary.getValue(nodeId)), dotParams.getClassNodeShape(), dotParams.getClassNodeStyle(), dotParams.getClassNodeColor());
		}
		else if (rdfDataset.isProperty(nodeId) && !rdfDataset.isClass(nodeId)) {
			// Property node
			return dotViz.getDotNode(nodeId, RdfUtils.stripNamespace(dictionary.getValue(nodeId)), dotParams.getPropertyNodeShape(), dotParams.getPropertyNodeStyle(), dotParams.getPropertyNodeColor());
		}

		// Class and property node
		return dotViz.getDotNode(nodeId, RdfUtils.stripNamespace(dictionary.getValue(nodeId)), dotParams.getClassAndPropertyNodeShape(), dotParams.getClassAndPropertyNodeStyle(), dotParams.getClassAndPropertyNodeColor()); 
	}
	
	private String getGraphName(final String summaryName) {
		sb = new StringBuilder();
		sb.append(Chars.QUOTE).append(summaryName).append(Chars.QUOTE);
		
		return sb.toString();
	}
}
