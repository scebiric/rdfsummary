package fr.inria.oak.RDFSummary.summary.triplecreator.trove.clique;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.WeakTypeEquivalenceSummary;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveTripleCreator {

	/**
	 * @param summary
	 * @param gNode
	 * @param classIRI
	 * @return True if the triple has been represented, otherwise false
	 *  
	 */
	public boolean representTypeTriple(PureCliqueEquivalenceSummary summary, int gNode, int classIRI);
	
	/**
	 * @param summary
	 * @param gDataNodes
	 * @param classes
	 * @throws SQLException 
	 *  
	 */
	public void representTypeTriples(PureCliqueEquivalenceSummary summary, TIntSet gDataNodes, TIntSet classes) throws SQLException;
	
	/**
	 * @param summary
	 * @param gNode
	 * @param classes
	 * @throws SQLException
	 *  
	 */
	public void representTypeTriples(TypeEquivalenceSummary summary, int gNode, TIntSet classes) throws SQLException;
	
	/**
	 * @param summary
	 * @param subject
	 * @param property
	 * @param object
	 *  
	 */
	public void representDataTriple(StrongEquivalenceSummary summary, int subject, int property, int object);
	
	/**
	 * @param summary
	 * @param subject
	 * @param property
	 * @param object
	 * @throws SQLException
	 *  
	 */
	public void representDataTriple(WeakPureCliqueEquivalenceSummary summary, int subject, int property, int object) throws SQLException;
	
	/**
	 * @param summary
	 * @param subject
	 * @param property
	 * @param object
	 * @throws SQLException
	 *  
	 */
	public void representDataTriple(WeakTypeEquivalenceSummary summary, int subject, int property, int object) throws SQLException;
}
