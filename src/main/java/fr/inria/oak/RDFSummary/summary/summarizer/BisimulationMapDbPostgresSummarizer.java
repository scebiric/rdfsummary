package fr.inria.oak.RDFSummary.summary.summarizer;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationMapDbPostgresSummarizer {

	/**
	 * @throws SQLException
	 * @throws QueryException
	 * @throws IOException
	 * @throws UnsupportedDatabaseEngineException
	 * @throws DictionaryException
	 * @throws SchemaException
	 * @throws SignatureException 
	 */
	public BisimulationMapDbPostgresSummarizerResult run() throws SQLException, QueryException, IOException, UnsupportedDatabaseEngineException, DictionaryException, SchemaException, SignatureException;
}
