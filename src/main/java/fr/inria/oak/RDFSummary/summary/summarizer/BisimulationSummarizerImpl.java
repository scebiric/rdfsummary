package fr.inria.oak.RDFSummary.summary.summarizer;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.SignatureException;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.RdfComponentSummarizer;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timing.SummaryTiming;
import fr.inria.oak.RDFSummary.timing.Timings;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class BisimulationSummarizerImpl implements BisimulationSummarizer {

	private static final Logger log = Logger.getLogger(BisimulationSummarizer.class);
	
	private final String summaryType;
	private final RdfComponentSummarizer componentSummarizer;
	private final Timer totalTimer;
	private final Timings timings;
	
	private final MessageBuilder messageBuilder;
	
	/**
	 * @param summaryType
	 * @param componentSummarizer
	 * @param totalTimer
	 * @param timings
	 */
	public BisimulationSummarizerImpl(final String summaryType, final RdfComponentSummarizer componentSummarizer, final Timer totalTimer, final Timings timings) {
		this.summaryType = summaryType;
		this.componentSummarizer = componentSummarizer;
		this.totalTimer = totalTimer;
		this.timings = timings;
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void summarize() throws SQLException, SignatureException {
		totalTimer.reset();
		totalTimer.start();		
		log.info(messageBuilder.append("Building the ").append(summaryType).append(" bisimulation summary..."));
		componentSummarizer.summarizeDataTriples();
		componentSummarizer.summarizeTypeTriples();				
		totalTimer.stop();
		
		log.info(messageBuilder.append("Summarization time: ").append(totalTimer.getTimeAsString()));
		timings.addTime(SummaryTiming.SUMMARIZATION_TIME, totalTimer.getTimeInMilliseconds());
	}
}
