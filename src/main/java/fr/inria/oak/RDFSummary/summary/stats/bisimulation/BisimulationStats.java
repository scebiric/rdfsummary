package fr.inria.oak.RDFSummary.summary.stats.bisimulation;

import fr.inria.oak.RDFSummary.summary.stats.RdfGraphStats;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface BisimulationStats extends RdfGraphStats {
	
	public int getIterationsCount();
	
	public void setIterationsCount(int count);
}
