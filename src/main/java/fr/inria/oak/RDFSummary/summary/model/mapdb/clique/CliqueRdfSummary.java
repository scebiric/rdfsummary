package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;

import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface CliqueRdfSummary extends RdfSummary {
	
	/**
	 * Creates a summary data node representing gDataNode 
	 * @param gDataNode
	 * @return The created node
	 * @throws SQLException 
	 */
	public int createDataNode(int gDataNode) throws SQLException;	
	
	/** Returns the last created data node */
	public int getLastCreatedDataNode();
	
	/**
	 * Sets the specified node as the last created data node.
	 * @param dataNode
	 */
	public void setLastCreatedDataNode(int dataNode);
	
	/**
	 * @param sDataNode
	 * @return Returns an iterator over the set of G data nodes represented by the specified summary data node
	 * @throws IllegalArgumentException if sDataNode doesn't represent any nodes
	 */
	public Iterator<Integer> getRepresentedDataNodes(int sDataNode);
	
	/** Get the number of resources from the input graph represented by the specified data node */
	public int getSummaryDataNodeSupport(int sDataNode);
	
	/** Retrieves the summary data node representing the gDataNode, or null if gDataNode is unrepresented */
	public Integer getSummaryDataNodeByGDataNode(int gDataNode);
		
	/** Stores sDataNode as the representative of gDataNode */
	public void representInputDataNode(int gDataNode, int sDataNode);
}
