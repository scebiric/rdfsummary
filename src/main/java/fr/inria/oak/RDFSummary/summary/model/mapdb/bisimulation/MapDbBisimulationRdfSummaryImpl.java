package fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.mapdb.DB;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;
import org.mapdb.Serializer;

import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbBisimulationRdfSummaryImpl implements BisimulationRdfSummary {

	private final RdfSummary rdfSummary;
	private final Map<Integer, Integer> sDataNodeByGDataNode;
	
	/**
	 * @param rdfSummary
	 * @param summaryDb
	 * @param representationMapName
	 */
	public MapDbBisimulationRdfSummaryImpl(final RdfSummary rdfSummary, final DB summaryDb, final String representationMapName) {
		this.rdfSummary = rdfSummary;
		sDataNodeByGDataNode = summaryDb.createHashMap(representationMapName).counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
	}
	
	@Override
	public Integer getSummaryDataNodeByGDataNode(final int gDataNode) throws NullPointerException {
		return sDataNodeByGDataNode.get(gDataNode);
	}

	@Override
	public int getDataNodeCount() {
		return Sets.newHashSet(sDataNodeByGDataNode.values()).size();
	}
	
	@Override
	public void setSummaryDataNodeByGDataNode(final int gDataNode, final int sNode) {
		sDataNodeByGDataNode.put(gDataNode, sNode);
	}

	@Override
	public Set<Integer> getDataNodes() {
		return Sets.newHashSet(sDataNodeByGDataNode.values());
	}

	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}

	@Override
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		rdfSummary.createDataTriple(source, dataProperty, target);
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		return rdfSummary.getAllDataTriples();
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		return rdfSummary.getAllTypeTriples();
	}

	@Override
	public Iterator<Integer> getClassesBySummaryNode(int sNode) {
		return rdfSummary.getClassesBySummaryNode(sNode);
	}

	@Override
	public int getDataTriplesCount() {
		return rdfSummary.getDataTriplesCount();
	}

	@Override
	public int getTypeTriplesCount() {
		return rdfSummary.getTypeTriplesCount();
	}
}
