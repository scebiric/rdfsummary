package fr.inria.oak.RDFSummary.summary.bisimulation.signature;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;

import org.mapdb.Fun.Tuple2;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.util.SummaryUtils;

/**
 * HashMap collisions leading to faulty summaries
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated 
public class PostgresTroveHashMapBisimulationSignatureImpl implements BisimulationSignature {

	private Map<Vector<Integer>, Integer> idBySignature;
	private int seed;
	
	private final String summaryType;
	private final PostgresBisimulationRdfDataset rdfDataset;
	private final BisimulationRdfSummary summary;

	private TreeSet<Tuple2<Integer, Integer>> pairsTreeSet;
	private Iterator<Tuple2<Integer, Integer>> pairsIterator;
	private ResultSet pairsResultSet;
	private Tuple2<Integer, Integer> pair;
	private int property;
	private int gNode;
	
	/**
	 * @param summaryType
	 * @param rdfDataset
	 * @param summary
	 */
	public PostgresTroveHashMapBisimulationSignatureImpl(final String summaryType, final PostgresBisimulationRdfDataset rdfDataset, final BisimulationRdfSummary summary) {
		this.summaryType = summaryType;
		this.rdfDataset = rdfDataset;
		this.summary = summary;
		
		this.idBySignature = Maps.newHashMap();
		this.seed = Constant.INITIAL_SEED;
	}

	@Override
	public int getSummaryNode(int gDataNode) throws SQLException {
		pairsTreeSet = Sets.newTreeSet(); // Tree set ensures that the pairs are sorted and unique
		if (summaryType.equals(SummaryType.FORWARD_ONLY) || summaryType.equals(SummaryType.FORWARD_BACKWARD)) {
			pairsTreeSet = computeForwardSignature(gDataNode);
		}

		if (summaryType.equals(SummaryType.BACKWARD_ONLY) || summaryType.equals(SummaryType.FORWARD_BACKWARD)) {
			pairsTreeSet = computeBackwardSignature(gDataNode);
		}
		
		final Vector<Integer> signature = new Vector<Integer>();
		signature.add(summary.getSummaryDataNodeByGDataNode(gDataNode).intValue());
		
		pairsIterator = pairsTreeSet.iterator();
		while (pairsIterator.hasNext()) {
			pair = pairsIterator.next();
			signature.add(pair.a.intValue());
			signature.add(pair.b.intValue());
		}
		
		Integer id = idBySignature.get(signature);
		if (id == null) {
			id = seed++;
			idBySignature.put(signature, id.intValue());
		}
		
		return id.intValue();
	}

	@Override
	public TreeSet<Tuple2<Integer, Integer>> computeForwardSignature(final int gDataNode) throws SQLException {
		pairsResultSet = rdfDataset.getPropertyObjectPairsBySubject(gDataNode);
		addPairsToTreeSet(pairsResultSet);
		pairsResultSet.close();

		return pairsTreeSet;
	}

	@Override
	public TreeSet<Tuple2<Integer, Integer>> computeBackwardSignature(final int gDataNode) throws SQLException {
		pairsResultSet = rdfDataset.getPropertySubjectPairsByObject(gDataNode);
		addPairsToTreeSet(pairsResultSet);
		pairsResultSet.close();

		return pairsTreeSet;
	}

	private void addPairsToTreeSet(final ResultSet pairsResultSet) throws SQLException {
		while (pairsResultSet.next()) {
			property = pairsResultSet.getInt(1);
			gNode = pairsResultSet.getInt(2);
			pairsTreeSet.add(new Tuple2<Integer, Integer>(property, SummaryUtils.getRepresentative(gNode, rdfDataset, summary)));
		}
	}

	@Override
	public void clearSignatures() {
		idBySignature.clear();
	}
}
