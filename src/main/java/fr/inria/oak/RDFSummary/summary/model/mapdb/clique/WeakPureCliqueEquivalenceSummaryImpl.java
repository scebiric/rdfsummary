package fr.inria.oak.RDFSummary.summary.model.mapdb.clique;

import java.sql.SQLException;
import java.util.Iterator;
import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureCliqueEquivalenceSummaryImpl implements WeakPureCliqueEquivalenceSummary {

	private final WeakEquivalenceSummary weakEquivSummary;
	private final PureCliqueEquivalenceSummary pureCliqueEquivSummary;
	private final CliqueRdfSummary rdfSummary;
	
	/**
	 * @param weakEquivSummary
	 * @param pureCliqueEquivSummary
	 * @param rdfSummary
	 */
	public WeakPureCliqueEquivalenceSummaryImpl(final WeakEquivalenceSummary weakEquivSummary, PureCliqueEquivalenceSummary pureCliqueEquivSummary,
			final CliqueRdfSummary rdfSummary) {
		this.weakEquivSummary = weakEquivSummary;
		this.pureCliqueEquivSummary = pureCliqueEquivSummary;
		this.rdfSummary = rdfSummary;
	}
	
	@Override
	public int unifyDataNodes(int dataNode1, int dataNode2) {
		return weakEquivSummary.unifyDataNodes(dataNode1, dataNode2);
	}

	@Override
	public void unifyDataNodes() throws SQLException {
		weakEquivSummary.unifyDataNodes();
	}
	
	@Override
	public void setSubjectForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setSubjectForDataProperty(dataProperty, dataNode);
	}

	@Override
	public void setObjectForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setObjectForDataProperty(dataProperty, dataNode);
	}

	@Override
	public Integer getSubjectForDataProperty(int dataProperty) {
		return weakEquivSummary.getSubjectForDataProperty(dataProperty);
	}

	@Override
	public Integer getObjectForDataProperty(int dataProperty) {
		return weakEquivSummary.getObjectForDataProperty(dataProperty);
	}

	
	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void createTypeTriple(int sNode, int classIRI) {
		pureCliqueEquivSummary.createTypeTriple(sNode, classIRI);
	}
	
	@Override
	public boolean existsTypeTriple(int sNode, int classIRI) {
		return pureCliqueEquivSummary.existsTypeTriple(sNode, classIRI);
	}

	@Override
	public Iterator<Integer> getDataPropertiesBySubject(int dataNode) {
		return weakEquivSummary.getDataPropertiesBySubject(dataNode);
	}

	@Override
	public Iterator<Integer> getDataPropertiesByObject(int dataNode) {
		return weakEquivSummary.getDataPropertiesByObject(dataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) {
		rdfSummary.createDataTriple(source, dataProperty, target);
	}

	@Override
	public void addClassBySummaryNode(int sNode, int classIRI) {
		rdfSummary.addClassBySummaryNode(sNode, classIRI);
	}

	@Override
	public Iterator<Integer> getClassesBySummaryNode(int sNode) {
		return rdfSummary.getClassesBySummaryNode(sNode);
	}

	@Override
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples() {
		return weakEquivSummary.getAllTypeTriples();
	}

	@Override
	public Iterator<Integer> getRepresentedDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedDataNodes(sDataNode);
	}

	@Override
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples() {
		return weakEquivSummary.getAllDataTriples();
	}

	@Override
	public Integer getSummaryDataNodeByGDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeByGDataNode(gDataNode);
	}

	@Override
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes) {
		rdfSummary.addClassesBySummaryNode(sNode, classes);
	}

	@Override
	public int createDataNode(Iterator<Integer> gDataNodes) throws SQLException {
		return pureCliqueEquivSummary.createDataNode(gDataNodes);
	}

	@Override
	public void createTypeTriples(int sDataNode, Iterator<Integer> classes) {
		pureCliqueEquivSummary.createTypeTriples(sDataNode, classes);
	}

	@Override
	public int getDataTriplesCount() {
		return rdfSummary.getDataTriplesCount();
	}

	@Override
	public int getTypeTriplesCount() {
		return rdfSummary.getTypeTriplesCount();
	}
}
