package fr.inria.oak.RDFSummary.summary.model.mapdb;

import java.util.Iterator;

import org.mapdb.Fun.Tuple2;
import org.mapdb.Fun.Tuple3;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfSummary {

	/**
	 * Creates a type triple with sNode as the subject/source and the class IRI as the object 
	 * @param sNode
	 * @param classIRI
	 */
	public void addClassBySummaryNode(int sNode, int classIRI);
	
	/**
	 * Maps the summary node (data, class or property) and its classes.
	 * 
	 * @param sNode
	 * @param classes
	 */
	public void addClassesBySummaryNode(int sNode, Iterator<Integer> classes);
	
	/**
	 * Creates a data triple (source, dataProperty, target).
	 * @param source
	 * @param dataProperty
	 * @param target
	 */
	public void createDataTriple(final int source, final int dataProperty, final int target);
	
	/**
	 * @return An iterator over data triples (s, p, o)
	 */
	public Iterator<Tuple3<Integer, Integer, Integer>> getAllDataTriples();
	
	/**
	 * @return An iterator over type triples (s,o)
	 */
	public Iterator<Tuple2<Integer, Integer>> getAllTypeTriples();
	
	/**
	 * @param sNode
	 * @return An iterator over the class set of the specified node
	 */
	public Iterator<Integer> getClassesBySummaryNode(int sNode);
	
	public int getDataTriplesCount();
	
	public int getTypeTriplesCount();
}
