package fr.inria.oak.RDFSummary.summary.stats.clique;

import java.sql.SQLException;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.Schema;
import fr.inria.oak.RDFSummary.summary.model.trove.clique.RdfSummary;
import fr.inria.oak.RDFSummary.summary.stats.NodeStats;
import fr.inria.oak.RDFSummary.summary.stats.StatisticsException;
import fr.inria.oak.RDFSummary.summary.stats.TripleStats;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class CliqueTrovePostgresStatsCollectorImpl implements CliqueTrovePostgresStatsCollector {

	private static RdfTablesDao RdfTablesDao;
	private static DmlDao DmlDao;
	
	/**
	 * Constructor of {@link CliqueTrovePostgresStatsCollectorImpl}
	 * 
	 * @param rdfTablesDao
	 * @param dmlDao
	 */
	public CliqueTrovePostgresStatsCollectorImpl(final RdfTablesDao rdfTablesDao, final DmlDao dmlDao) {
		RdfTablesDao = rdfTablesDao;
		DmlDao = dmlDao;
	}

	@Override
	public Stats collectStats(final RdfSummary summary, final String encodedInputDataTable, final String encodedInputTypesTable,
			final String encodedSummaryDataTable, final String encodedSummaryTypesTable, 
			final String classNodesTable, final String propertyNodesTable,
			final Schema schema, final long summarizationTime) throws DictionaryException, SQLException, QueryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(schema);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedInputDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedInputTypesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSummaryDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSummaryTypesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));
		
		int schemaTripleCount = schema.getAllTriples().size();
		int inputDataTripleCount = DmlDao.getRowCount(encodedInputDataTable);
		int inputTypeTripleCount = DmlDao.getRowCount(encodedInputTypesTable);
		TripleStats inputGraphTripleStats = new TripleStats(schemaTripleCount, inputDataTripleCount, inputTypeTripleCount);
		
		int summaryDataTripleCount = DmlDao.getRowCount(encodedSummaryDataTable);
		int summaryTypeTripleCount = DmlDao.getRowCount(encodedSummaryTypesTable);
		TripleStats summaryTripleStats = new TripleStats(schemaTripleCount, summaryDataTripleCount, summaryTypeTripleCount);	
		
		int summaryDataNodeCount = summary.getSummaryDataNodeCount(); 
		int summaryClassNodeCount = DmlDao.getDistinctColumnValuesCount(classNodesTable, Constant.URI);
		int summaryPropertyNodeCount = DmlDao.getDistinctColumnValuesCount(propertyNodesTable, Constant.URI); 
		int totalDistinctSummaryNodeCount = summaryDataNodeCount + RdfTablesDao.getDistinctClassAndPropertyNodeCount(classNodesTable, propertyNodesTable); // a node can be a class and a property at the same time
		
		NodeStats summaryNodeStats = new NodeStats(summaryDataNodeCount);
		summaryNodeStats.setClassNodeCount(summaryClassNodeCount);
		summaryNodeStats.setPropertyNodeCount(summaryPropertyNodeCount);
		summaryNodeStats.setTotalDistinctNodeCount(totalDistinctSummaryNodeCount);
		int distinctDataPropertiesCount = DmlDao.getDistinctColumnValuesCount(encodedSummaryDataTable, Constant.PROPERTY);
		int distinctClassesCount = RdfTablesDao.getDistinctClassesCount(classNodesTable);
		
		return new Stats(summaryNodeStats, summaryTripleStats, inputGraphTripleStats, 
				distinctDataPropertiesCount, distinctClassesCount, summarizationTime);
	}
	
	@Override
	public int getClassSupport(String classIRI, String encodedDataTable, String encodedTypesTable) throws SQLException, StatisticsException, DictionaryException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classIRI));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTypesTable));

		return RdfTablesDao.getClassSupport(classIRI, encodedDataTable, encodedTypesTable);
	}

	@Override
	public int getDataPropertySupport(final String dataProperty, final String encodedDataTable) throws SQLException, StatisticsException, DictionaryException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataProperty));
		Preconditions.checkArgument(!RdfUtils.isRDFSProperty(dataProperty) && !RdfUtils.isRDFTypeProperty(dataProperty));

		return RdfTablesDao.getDataPropertySupport(dataProperty, encodedDataTable);
	}
}
