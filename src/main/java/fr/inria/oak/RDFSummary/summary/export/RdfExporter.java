package fr.inria.oak.RDFSummary.summary.export;

import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.config.params.ExportParams;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfExporter {

	/**
	 * Exports decoded triples to RDF and/or DOT files
	 * 
	 * @param exportParams
	 * @param inputGraphName
	 * @param outputGraphName
	 * @return 
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public ExportResult exportToFiles(ExportParams exportParams, String inputGraphName, String outputGraphName) throws IOException, SQLException;
}
