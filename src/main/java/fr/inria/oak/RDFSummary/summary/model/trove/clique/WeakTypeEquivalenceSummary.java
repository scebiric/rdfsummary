package fr.inria.oak.RDFSummary.summary.model.trove.clique;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface WeakTypeEquivalenceSummary extends WeakEquivalenceSummary, TypeEquivalenceSummary {

}
