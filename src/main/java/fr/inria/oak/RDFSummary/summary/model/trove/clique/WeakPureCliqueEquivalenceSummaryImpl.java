package fr.inria.oak.RDFSummary.summary.model.trove.clique;

import java.sql.SQLException;
import java.util.Collection;

import fr.inria.oak.RDFSummary.summary.model.trove.clique.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureCliqueEquivalenceSummaryImpl implements WeakPureCliqueEquivalenceSummary {

	private final WeakEquivalenceSummary weakEquivSummary;
	private final PureCliqueEquivalenceSummary pureCliqueEquivSummary;
	private final RdfSummary rdfSummary;
	
	/**
	 * @param weakEquivSummary
	 * @param pureCliqueEquivSummary
	 * @param rdfSummary
	 */
	public WeakPureCliqueEquivalenceSummaryImpl(final WeakEquivalenceSummary weakEquivSummary, PureCliqueEquivalenceSummary pureCliqueEquivSummary,
			final RdfSummary rdfSummary) {
		this.weakEquivSummary = weakEquivSummary;
		this.pureCliqueEquivSummary = pureCliqueEquivSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target);
	}
	
	@Override
	public int unifyDataNodes(int dataNode1, int dataNode2) {
		return weakEquivSummary.unifyDataNodes(dataNode1, dataNode2);
	}

	@Override
	public int getDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getDegreeForDataNode(dataNode);
	}

	@Override
	public int getIncomingDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getIncomingDegreeForDataNode(dataNode);
	}

	@Override
	public int getOutgoingDegreeForDataNode(int dataNode) {
		return weakEquivSummary.getOutgoingDegreeForDataNode(dataNode);
	}

	@Override
	public void setSourceDataNodeForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setSourceDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public void setTargetDataNodeForDataProperty(int dataProperty, int dataNode) {
		weakEquivSummary.setTargetDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public int getSourceDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getSourceDataNodeForDataProperty(dataProperty);
	}

	@Override
	public int getTargetDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getTargetDataNodeForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode) {
		return weakEquivSummary.getDataPropertiesForSourceDataNode(dataNode);
	}

	@Override
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode) {
		return weakEquivSummary.getDataPropertiesForTargetDataNode(dataNode);
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}
	
	@Override
	public int createDataNode(TIntSet gDataNodes) throws SQLException {
		return pureCliqueEquivSummary.createDataNode(gDataNodes);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void createTypeTriple(int sNode, int classIRI) {
		pureCliqueEquivSummary.createTypeTriple(sNode, classIRI);
	}

	@Override
	public void createTypeTriples(int sDataNode, TIntSet classes) {
		pureCliqueEquivSummary.createTypeTriples(sDataNode, classes);
	}
	
	@Override
	public boolean existsTypeTriple(int sNode, int classIRI) {
		return pureCliqueEquivSummary.existsTypeTriple(sNode, classIRI);
	}

	@Override
	public void removeDataPropertiesForSourceDataNode(int source) {
		weakEquivSummary.removeDataPropertiesForSourceDataNode(source);
	}

	@Override
	public void removeDataPropertiesForTargetDataNode(int target) {
		weakEquivSummary.removeDataPropertiesForTargetDataNode(target);
	}
	
	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
