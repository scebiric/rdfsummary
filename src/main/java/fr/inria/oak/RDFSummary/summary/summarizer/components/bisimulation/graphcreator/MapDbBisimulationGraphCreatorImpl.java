package fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.mapdb.Serializer;
import org.mapdb.Fun.Tuple2;

import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.MapDbBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Deprecated
public class MapDbBisimulationGraphCreatorImpl implements BisimulationGraphCreator {

	protected final static Logger log = Logger.getLogger(BisimulationGraphCreator.class);
	
	protected final DB summaryDB;
	protected final BisimulationRdfSummary summary;
	protected final MapDbBisimulationRdfDataset rdfDataset;
	protected final UriGenerator uriGenerator;
	protected final Dictionary dictionary;
	protected final Timer timer;
	
	protected final MessageBuilder messageBuilder;
	
	int subject;
	int subjectRepresentative;
	int property;
	int object;
	Iterator<Tuple2<Integer, Integer>> poPairs;
	Iterator<Tuple2<Integer, Integer>> typeTriples;
	Tuple2<Integer, Integer> poPair;
	Tuple2<Integer, Integer> typeTriple;
	
	/**
	 * @param summaryDB
	 * @param summary
	 * @param rdfDataset
	 * @param uriGenerator
	 * @param dictionary
	 * @param timer
	 */
	public MapDbBisimulationGraphCreatorImpl(final DB summaryDB, final BisimulationRdfSummary summary, final MapDbBisimulationRdfDataset rdfDataset,
			final UriGenerator uriGenerator, final Dictionary dictionary, final Timer timer) {
		this.summaryDB = summaryDB;
		this.summary = summary;
		this.rdfDataset = rdfDataset;
		this.uriGenerator = uriGenerator;
		this.dictionary = dictionary;
		this.timer = timer;
		
		this.messageBuilder = new MessageBuilder();
	}

	@Override
	public void createDataNodes() throws SQLException {
		log.info("Generating and encoding data node URIs..."); 
		timer.reset();
		timer.start();
		int sDictionaryDataNode;
		final Map<Integer, Integer> sDictionaryDataNodeByHash = summaryDB.createHashMap("hashToDictionaryNode").counterEnable().keySerializer(Serializer.INTEGER).valueSerializer(Serializer.INTEGER).make();
		final Set<Integer> sHashDataNodes = summary.getDataNodes();
		for (final int sHashDataNode : sHashDataNodes) {
			sDictionaryDataNode = dictionary.insert(uriGenerator.generateDataNodeUri(dictionary.getSeed()));
			sDictionaryDataNodeByHash.put(sHashDataNode, sDictionaryDataNode);
		}

		final Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		int gDataNode;
		int sHashDataNode;
		while (gDataNodes.hasNext()) {
			gDataNode = gDataNodes.next();
			sHashDataNode = summary.getSummaryDataNodeByGDataNode(gDataNode);
			sDictionaryDataNode = sDictionaryDataNodeByHash.get(sHashDataNode).intValue();
			summary.setSummaryDataNodeByGDataNode(gDataNode, sDictionaryDataNode);
		}
		timer.stop();
		log.info(messageBuilder.append("Data nodes created in: ").append(timer.getTimeAsString()));
	}
	
	/** 
	 * The tree set ensures uniqueness of the inserted triples.
	 */
	@Override
	public void createDataTriples() {
		log.info("Creating summary data triples...");
		timer.reset();
		timer.start();
		final Iterator<Integer> gDataNodes = rdfDataset.getDataNodes();
		while (gDataNodes.hasNext()) {
			subject = gDataNodes.next();
			subjectRepresentative = summary.getSummaryDataNodeByGDataNode(subject);
			poPairs = rdfDataset.getPropertyObjectPairsBySubject(subject);
			createDataTriples(poPairs);
		}
		timer.stop();
		log.info(messageBuilder.append("Data triples created in: ").append(timer.getTimeAsString())); 
	}
	
	protected void createDataTriples(final Iterator<Tuple2<Integer, Integer>> poPairs) {
		while (poPairs.hasNext()) {
			poPair = poPairs.next();
			property = poPair.a.intValue();
			object = poPair.b.intValue();
			summary.createDataTriple(subjectRepresentative, property, summary.getSummaryDataNodeByGDataNode(object));
		}
	}

	@Override
	public void createTypeTriples() {
		typeTriples = rdfDataset.getTypeTriples();
		createTypeTriples(typeTriples);
	}

	protected void createTypeTriples(final Iterator<Tuple2<Integer, Integer>> typeTriples) {
		while (typeTriples.hasNext()) {
			typeTriple = typeTriples.next();
			subject = typeTriple.a.intValue();
			object = typeTriple.b.intValue();
			summary.addClassBySummaryNode(summary.getSummaryDataNodeByGDataNode(subject), object);
		}
	}
}
