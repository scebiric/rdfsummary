package fr.inria.oak.RDFSummary.config.beans;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.EnvironmentConfig;

import fr.inria.oak.RDFSummary.cleaner.Cleaner;
import fr.inria.oak.RDFSummary.cleaner.CleanerImpl;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.connectionhandler.SqlConnectionHandlerImpl;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DdlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDao;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDaoImpl;
import fr.inria.oak.RDFSummary.data.encoding.Encoding;
import fr.inria.oak.RDFSummary.data.encoding.EncodingImpl;
import fr.inria.oak.RDFSummary.data.queries.DdlQueries;
import fr.inria.oak.RDFSummary.data.queries.DdlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueries;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.queries.DmlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueriesImpl;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturator;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturatorImpl;
import fr.inria.oak.RDFSummary.data.storage.splitter.PsqlStorageSplitterImpl;
import fr.inria.oak.RDFSummary.data.storage.splitter.StorageSplitter;
import fr.inria.oak.RDFSummary.experiments.sumtime.SummarizationTime;
import fr.inria.oak.RDFSummary.main.utils.JdbcTest;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLogger;
import fr.inria.oak.RDFSummary.performancelogger.PerformanceLoggerImpl;
import fr.inria.oak.RDFSummary.rdf.parser.RdfParser;
import fr.inria.oak.RDFSummary.rdf.parser.PostgresCopyRdfParserImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.urigenerator.BracketsUriGeneratorImpl;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.PostgresDataSource;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
@ComponentScan({ "fr.inria.oak.RDFSummary.config.beans" })
@Profile({ ProfileName.BISIMULATION_MAPDB_POSTGRES, ProfileName.CLIQUE_TROVE_POSTGRES })
public class PostgresBeanConfig {
	
	@Autowired
	private BeanConfig beanConfig;
	
	@Autowired
	private Properties props;
	
	@Bean
	public PostgresDataSource postgresDataSource() {
		return new PostgresDataSource(props.getServerName(), props.getPort(), props.getDatabaseName(), props.getUsername(), props.getPassword());
	}

	@Bean
	public RdfTablesDao rdfTablesDaoImpl() throws SQLException, DictionaryException {	
		return new RdfTablesDaoImpl(sqlConnectionHandlerImpl(), props.getFetchSize(), rdfTablesQueriesImpl(), 
				dictDaoImpl(), ddlDaoImpl(), dmlDaoImpl(), beanConfig.timerImpl(), performanceLoggerImpl());		
	}

	@Bean 
	public DmlQueries dmlQueriesImpl() {
		return new DmlQueriesImpl();
	}
	
	@Bean
	public DdlQueries ddlQueriesImpl() {
		return new DdlQueriesImpl();
	}
	
	@Bean 
	public DmlDao dmlDaoImpl() throws NumberFormatException, SQLException {
		return new DmlDaoImpl(sqlConnectionHandlerImpl(), props.getFetchSize(), beanConfig.timerImpl(), dmlQueriesImpl(), performanceLoggerImpl());
	}
	
	@Bean
	public DdlDao ddlDaoImpl() throws NumberFormatException, SQLException {
		return new DdlDaoImpl(sqlConnectionHandlerImpl(), props.getFetchSize(), beanConfig.timerImpl(), ddlQueriesImpl());
	}
	
	@Bean
	public SqlConnectionHandler sqlConnectionHandlerImpl() throws SQLException {
		PostgresDataSource ds = postgresDataSource();
		Connection conn = ds.getConnection();
		return new SqlConnectionHandlerImpl(conn);
	}
	
	@Bean
	public DictionaryDao dictDaoImpl() throws SQLException {
		return new DictionaryDaoImpl(props.getDictionaryTable(), sqlConnectionHandlerImpl(), 
				props.getFetchSize(), dictionaryQueryProviderImpl(), beanConfig.timerImpl());
	}
	
	@Bean
	public DictionaryQueries dictionaryQueryProviderImpl() {
		return new DictionaryQueriesImpl();
	}

	@Bean
	public RdfTablesQueries rdfTablesQueriesImpl() {
		return new RdfTablesQueriesImpl();
	}

	@Bean
	public Encoding encodingImpl() throws SQLException, DictionaryException {
		return new EncodingImpl(dictDaoImpl(), beanConfig.timerImpl());
	}

	@Bean
	public RdfParser postgresCopyRdfParserImpl() throws SQLException, DictionaryException {
		return new PostgresCopyRdfParserImpl(ddlDaoImpl(), dmlDaoImpl(), rdfTablesDaoImpl(), beanConfig.timerImpl(), 
				props.getSchemaName(), props.getTriplesTable());
	}	
	
	@Bean
	public EnvironmentConfig envConfig() {
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);

		return envConfig;
	}

	@Bean
	public DatabaseConfig dbConfig() {
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setAllowCreate(true);
		
		return dbConfig;
	}

	@Bean
	public StorageSplitter postgresStorageSplitterImpl() throws SQLException, DictionaryException {
		return new PsqlStorageSplitterImpl(dmlDaoImpl(), rdfTablesDaoImpl());
	}

	@Bean
	public SummarizationTime experiments() throws NumberFormatException, ParametersException, SQLException, DictionaryException {
		return new SummarizationTime();
	}

	@Bean
	public Utils utils() throws SQLException, DictionaryException {
		return new Utils(dictDaoImpl(), dmlDaoImpl(), ddlDaoImpl());
	}

	@Bean
	public JdbcTest jdbcTest() throws SQLException, DictionaryException {
		return new JdbcTest(rdfTablesDaoImpl(), sqlConnectionHandlerImpl());
	}
	
	@Bean
	public Cleaner cleanerImpl() throws SQLException, DictionaryException {
		return new CleanerImpl(sqlConnectionHandlerImpl(), ddlDaoImpl(), dictDaoImpl(), dmlDaoImpl(), rdfTablesDaoImpl(), saturatorDaoImpl());
	}
	
	@Bean 
	public UriGenerator bracketsUriGeneratorImpl() {
		return new BracketsUriGeneratorImpl(props.getSchemaName(), props.getSummaryType());
	}
	
	@Bean
	public PerformanceLogger performanceLoggerImpl() {
		return new PerformanceLoggerImpl(beanConfig.timerImpl());
	}
	
	@Bean
	public PostgresSaturator postgresSaturatorImpl() throws NumberFormatException, SQLException {
		return new PostgresSaturatorImpl(saturatorDaoImpl(), dmlDaoImpl(), ddlDaoImpl(), beanConfig.timerImpl());
	}
	
	@Bean
	public SaturatorDao saturatorDaoImpl() throws SQLException {
		return new SaturatorDaoImpl(beanConfig.batchSaturatorImpl(), sqlConnectionHandlerImpl(), rdfTablesQueriesImpl(), dmlQueriesImpl(), beanConfig.timerImpl());
	}
}
