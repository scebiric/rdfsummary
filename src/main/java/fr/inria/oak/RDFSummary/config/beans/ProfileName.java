package fr.inria.oak.RDFSummary.config.beans;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ProfileName {

	public static final String CLIQUE_MAPDB = "CLIQUE_MAPDB";
	public static final String BISIMULATION_MAPDB_POSTGRES = "BISIMULATION_MAPDB_POSTGRES";
	public static final String CLIQUE_TROVE_POSTGRES = "CLIQUE_TROVE_POSTGRES";
	public static final String EXPERIMENTS = "EXPERIMENTS";
}
