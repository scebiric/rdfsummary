package fr.inria.oak.RDFSummary.config.params;

import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExportParams {

	private final String outputFolder;
	private String dotExecutableFilepath;
	private String dotStyleParamsFilepath;
	private String saturationDotParamsFilepath = null;
	private boolean generateDot = false;
	private boolean convertDotToPdf = false;
	private boolean convertDotToPng = false;
	private boolean exportToRdfFile = false;
	private boolean exportStatsToFile = false;
	private boolean exportRepresentationTables = false;
	private DotStyleParams dotStyleParams = null;
	private boolean exportInput = false;
	
	/**
	 * Constructor of {@link ExportParams}
	 * 
	 * @param outputFolder
	 */
	public ExportParams(final String outputFolder) {
		this.outputFolder = Utils.getFormattedFilepath(outputFolder);
	}

	public String getDotExecutableFilepath() {
		return  dotExecutableFilepath;
	}
	
	public String getDotStyleParamsFilepath() {
		return dotStyleParamsFilepath;
	}
	
	public String getSaturationDotParamsFilepath() {
		return saturationDotParamsFilepath;
	}
	
	public boolean generateDot() {
		return generateDot;
	}
	
	public boolean convertDotToPdf() {
		return convertDotToPdf;
	}
	
	public boolean convertDotToPng() {
		return convertDotToPng;
	}
	
	public boolean exportToRdfFile() {
		return exportToRdfFile;
	}
	
	public boolean exportStatsToFile() {
		return exportStatsToFile;
	}
	
	public boolean exportRepresentationTables() {
		return exportRepresentationTables;
	}
	
	public String getOutputFolder() {
		return outputFolder;
	}	
	
	public void setGenerateDot(boolean value) {
		generateDot = value;
	}
	
	public void setConvertDotToPdf(boolean value) {
		convertDotToPdf = value;
	}
	
	public void setConvertDotToPng(boolean value) {
		convertDotToPng  = value;
	}

	public void setExportToRdfFile(boolean value) {
		exportToRdfFile = value;
	}

	public void setExportStatsToFile(boolean value) {
		exportStatsToFile  = value;
	}

	public void setSaturationDotParamsFilepath(String value) {
		saturationDotParamsFilepath = value;
	}
	
	public void setExportRepresentationTables(boolean value) {
		exportRepresentationTables = value;
	}
	
	public void setDotExecutableFilepath(String filepath) {
		dotExecutableFilepath = filepath;
	}
	
	public void setDotStyleParamsFilepath(String filepath) {
		dotStyleParamsFilepath = filepath;
	}
	
	public DotStyleParams getDotStyleParams() {
		return dotStyleParams;
	}
	
	public void setDotStyleParams(DotStyleParams value) {
		dotStyleParams = value;
	}
	
	public boolean exportInput() {
		return exportInput;
	}
	
	public void setExportInput(final boolean value) {
		exportInput = value;
	}
}
