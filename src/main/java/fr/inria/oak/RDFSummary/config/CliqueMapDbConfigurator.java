package fr.inria.oak.RDFSummary.config;

import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertiesPropertySource;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.beans.BeanConfig;
import fr.inria.oak.RDFSummary.config.beans.CliqueMapDbBeanConfig;
import fr.inria.oak.RDFSummary.config.beans.ProfileName;
import fr.inria.oak.RDFSummary.config.params.DiskSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.DiskParameterLoader;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@ComponentScan(value = {"fr.inria.oak.RDFSummary.config"})
@Deprecated
public class CliqueMapDbConfigurator {

	private final DiskParameterLoader diskParamLoader = new DiskParameterLoader();

	/**
	 * @param args
	 * @return {@link DiskSummarizerParams}
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 */
	public DiskSummarizerParams loadDiskSummarizerParameters(String[] args) throws ConfigurationException, ParametersException {
		DiskSummarizerParams params = null;
		if (args.length > 0) 
			params = diskParamLoader.loadDiskSummarizerParams(args);			
		else 
			params = diskParamLoader.loadDiskSummarizerParams("config/config-summarizer-disk.properties");

		ParameterValidator.validateDiskSummarizerParams(params);

		return params;
	}

	/**
	 * @param context
	 * @param params
	 * @throws ConfigurationException
	 */
	public void javaSetUpApplicationContext(final AnnotationConfigApplicationContext context, final DiskSummarizerParams params) throws ConfigurationException {
		Preconditions.checkNotNull(context);
		Preconditions.checkNotNull(params);

		Properties properties = diskParamLoader.loadProperties(params);
		if (properties == null)
			throw new ConfigurationException("Properties cannot be null.");

		this.registerBeans(context, properties);
	}

	/**
	 * @param context
	 * @param properties
	 */
	private void registerBeans(AnnotationConfigApplicationContext context, Properties properties) {
		PropertiesPropertySource propertiesSource = new PropertiesPropertySource("props", properties);
		context.getEnvironment().getPropertySources().addLast(propertiesSource);
		context.getEnvironment().setActiveProfiles(ProfileName.CLIQUE_MAPDB);
		context.register(BeanConfig.class);
		context.register(CliqueMapDbBeanConfig.class);
		context.refresh();
	}
}
