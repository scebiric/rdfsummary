package fr.inria.oak.RDFSummary.config.params.loader;

import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FilenameUtils;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.config.params.CmdParams;
import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizationParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Equivalence;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * Loads parameters from a configuration file or the command line
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresParameterLoader extends ParameterLoader {

	private static JCommander jc;
	private static StringBuilder Builder;

	/**
	 * Loading {@link PostgresSummarizerParams} from the external configuration file.
	 * @param configFilepath 
	 * @return {@link PostgresSummarizerParams} instance
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresSummarizerParams loadPostgresSummarizerParams(String configFilepath) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading Postgres summarizer parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			ConnectionParams connParams = this.loadConnectionParams(config);
			PostgresSummarizationParams summParams = this.loadSummarizationParams(config);
			ExportParams exportParams = this.loadExportParams(config);

			return new PostgresSummarizerParams(connParams, summParams, exportParams);
		}
		else 
			throw new ParameterException("The properties file is empty.");
	} 

	/**
	 * @param config
	 * @return {@link ConnectionParams}
	 */
	private ConnectionParams loadConnectionParams(final Configuration config) {		
		return new ConnectionParams(config.getString(ParameterName.DATA_SOURCE_NAME), 
				config.getString(ParameterName.SERVER_NAME), config.getInteger(ParameterName.PORT, null), 
				config.getString(ParameterName.DATABASE_NAME), 
				config.getString(ParameterName.USERNAME), config.getString(ParameterName.PASSWORD));
	}

	/**
	 * @param config
	 * @return {@link PostgresSummarizationParams}
	 */
	private PostgresSummarizationParams loadSummarizationParams(final Configuration config) {
		PostgresSummarizationParams summParams = new PostgresSummarizationParams(config.getString(ParameterName.EQUIVALENCE),
				config.getString(ParameterName.SUMMARY_TYPE), 
				config.getString(ParameterName.PSQL_SCHEMA_NAME), config.getString(ParameterName.DATA_FILEPATH));

		summParams.setSchemaTriplesFilepath(config.getString(ParameterName.SCHEMA_FILEPATH));
		summParams.setDropSchema(config.getBoolean(ParameterName.DROP_SCHEMA, false));
		summParams.setIndexing(config.getBoolean(ParameterName.INDEXING, false));		
		summParams.setFetchSize(config.getInt(ParameterName.FETCH_SIZE, 50000));
		summParams.setSaturateInput(config.getBoolean(ParameterName.SATURATE_INPUT, false));
		summParams.setRecomputeSaturation(config.getBoolean(ParameterName.RECOMPUTE_SATURATION, false));
		summParams.setSaturationBatchSize(config.getInt(ParameterName.SATURATION_BATCH_SIZE, 100));

		return summParams;
	}

	/**
	 * Reads the specified command line parameters into the SummarizerParams class
	 * @param args
	 * @return {@link PostgresSummarizerParams}
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresSummarizerParams loadPostgresSummarizerParams(String[] args) throws ConfigurationException, ParametersException {
		log.info("Loading command line parameters...");
		CmdParams cmdParams = new CmdParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return this.loadPostgresSummarizerParams(cmdParams.CONFIG_FILEPATH);
		}
		else 
			return this.loadPostgresSummarizerParams(cmdParams);
	}

	/**
	 * Loading {@link PostgresExperimentsParams} from the external configuration file.
	 * @param configFilepath 
	 * @return {@link PostgresExperimentsParams} instance
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresExperimentsParams loadExperimentsParams(String configFilepath) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading experiments parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {	
			PostgresSummarizerParams summarizerParams = new PostgresSummarizerParams(this.loadConnectionParams(config), 
					this.loadSummarizationParams(config), this.loadExportParams(config));
			final List<String> summaryTypes = Utils.loadSummaryTypes(config.getString(ParameterName.SUMMARY_TYPES), 
					summarizerParams.getSummarizationParams().getNodeEquivalenceRelation());
			final int numberOfRuns = config.getInt(ParameterName.NUMBER_OF_RUNS);
			final boolean logging = config.getBoolean(ParameterName.LOGGING, false);
			PostgresExperimentsParams expParams = new PostgresExperimentsParams(summarizerParams, summaryTypes, numberOfRuns);
			expParams.setLogging(logging);

			return expParams;
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	/**
	 * Reads the specified command line parameters into the ExperimentsParams class
	 * @param args
	 * @return {@link PostgresExperimentsParams}
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresExperimentsParams loadExperimentsParams(String[] args) throws ConfigurationException, ParametersException {
		log.info("Loading command line parameters...");
		CmdParams cmdParams = new CmdParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return loadExperimentsParams(cmdParams.CONFIG_FILEPATH);
		}
		else
			return loadExperimentsParamsFromCmd(cmdParams);
	}

	public PostgresExperimentsParams loadExperimentsParamsFromCmd(CmdParams cmdParams) throws ConfigurationException, ParametersException {
		final List<String> summaryTypes = Utils.loadSummaryTypes(cmdParams.SUMMARY_TYPES, cmdParams.EQUIVALENCE);
		PostgresExperimentsParams expParams = new PostgresExperimentsParams(loadPostgresSummarizerParams(cmdParams), 
				summaryTypes, cmdParams.NUMBER_OF_RUNS);
		expParams.setLogging(cmdParams.LOGGING);

		return expParams;
	}

	/**
	 * Loads {@link PostgresSummarizerParams} from {@link CmdParams} 
	 * @param cmdParams
	 * @return {@link PostgresSummarizerParams} instance
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 */
	public PostgresSummarizerParams loadPostgresSummarizerParams(final CmdParams cmdParams) throws ConfigurationException, ParametersException {
		ConnectionParams connParams = new ConnectionParams(cmdParams.DATA_SOURCE_NAME, cmdParams.SERVER_NAME, 
				cmdParams.PORT, cmdParams.DATABASE_NAME, cmdParams.USERNAME, cmdParams.PASSWORD);

		PostgresSummarizationParams summParams = new PostgresSummarizationParams(cmdParams.EQUIVALENCE, cmdParams.SUMMARY_TYPE, cmdParams.PSQL_SCHEMA_NAME, cmdParams.DATA_FILEPATH);
		summParams.setDropSchema(cmdParams.DROP_SCHEMA);
		summParams.setSchemaTriplesFilepath(cmdParams.RDFS_FILEPATH);
		summParams.setIndexing(cmdParams.INDEXING);		
		summParams.setFetchSize(cmdParams.FETCH_SIZE);
		summParams.setSaturateInput(cmdParams.SATURATE_INPUT);
		summParams.setRecomputeSaturation(cmdParams.RECOMPUTE_SATURATION);
		summParams.setSaturationBatchSize(cmdParams.SATURATION_BATCH_SIZE);

		ExportParams exportParams = loadExportParams(cmdParams);

		if (!StringUtils.isNullOrBlank(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH))
			exportParams.setDotStyleParams(loadDotStyleParams(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH));

		log.info("Summarizer parameters loaded from the command line.");
		return new PostgresSummarizerParams(connParams, summParams, exportParams);
	}

	private static void displayUsage() {
		if (jc != null) {
			jc.usage();
		}
	}



	/**
	 * Loads those parameters to properties which are needed to inject beans when setting up the application context
	 * 
	 * @param summParams
	 * @return The {@link Properties} object
	 */
	public Properties loadProperties(PostgresSummarizerParams summParams) {
		Preconditions.checkNotNull(summParams);
		Properties properties = new Properties();
		properties.put(ParameterName.SUMMARY_TYPE, summParams.getSummarizationParams().getSummaryType());
		properties.put(ParameterName.OUTPUT_FOLDER, summParams.getExportParams().getOutputFolder());
		properties.put(ParameterName.DATA_SOURCE_NAME, summParams.getConnectionParams().getDataSourceName());
		properties.put(ParameterName.SERVER_NAME, summParams.getConnectionParams().getServerName());
		properties.put(ParameterName.PORT, summParams.getConnectionParams().getPort());
		properties.put(ParameterName.DATABASE_NAME, summParams.getConnectionParams().getDatabaseName());
		properties.put(ParameterName.USERNAME, summParams.getConnectionParams().getUsername());
		properties.put(ParameterName.PASSWORD, summParams.getConnectionParams().getPassword());
		properties.put(ParameterName.PSQL_SCHEMA_NAME, summParams.getSummarizationParams().getSchemaName());
		properties.put(ParameterName.DROP_SCHEMA, summParams.getSummarizationParams().dropSchema());
		properties.put(ParameterName.FETCH_SIZE, summParams.getSummarizationParams().getFetchSize());
		properties.put(ParameterName.DICTIONARY_TABLE, summParams.getSummarizationParams().getDictionaryTable());
		properties.put(ParameterName.TRIPLES_TABLE, summParams.getSummarizationParams().getTriplesTable());
		properties.put(ParameterName.SATURATION_BATCH_SIZE, summParams.getSummarizationParams().getSaturationBatchSize());
		properties.put(ParameterName.SATURATE_INPUT, summParams.getSummarizationParams().saturateInput());
		properties.put(ParameterName.RECOMPUTE_SATURATION, summParams.getSummarizationParams().recomputeSaturation());
		properties.put(ParameterName.EQUIVALENCE, summParams.getSummarizationParams().getNodeEquivalenceRelation());

		properties.put(ParameterName.CONVERT_DOT_TO_PDF, summParams.getExportParams().convertDotToPdf());
		properties.put(ParameterName.CONVERT_DOT_TO_PNG, summParams.getExportParams().convertDotToPng());
		properties.put(ParameterName.EXPORT_INPUT, summParams.getExportParams().exportInput());
		properties.put(ParameterName.EXPORT_REPRESENTATION_TABLES, summParams.getExportParams().exportRepresentationTables());
		properties.put(ParameterName.EXPORT_STATS_TO_FILE, summParams.getExportParams().exportStatsToFile());
		properties.put(ParameterName.EXPORT_TO_RDF_FILE, summParams.getExportParams().exportToRdfFile());
		properties.put(ParameterName.GENERATE_DOT, summParams.getExportParams().generateDot());

		if (!StringUtils.isNullOrBlank(summParams.getExportParams().getDotExecutableFilepath()))
			properties.put(ParameterName.DOT_EXECUTABLE_FILEPATH, summParams.getExportParams().getDotExecutableFilepath());

		if (!StringUtils.isNullOrBlank(summParams.getExportParams().getDotStyleParamsFilepath()))
			properties.put(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH, summParams.getExportParams().getDotStyleParamsFilepath());

		if (!StringUtils.isNullOrBlank(summParams.getExportParams().getSaturationDotParamsFilepath()))
			properties.put(ParameterName.SATURATION_DOT_PARAMETERS_FILEPATH, summParams.getExportParams().getSaturationDotParamsFilepath());

		return properties;
	}

	/**
	 * Loads those parameters to properties which are needed to inject beans when setting up the application context
	 * 
	 * @param expParams
	 * @return The {@link Properties} object
	 */
	public Properties loadProperties(PostgresExperimentsParams expParams) {
		return this.loadProperties(expParams.getSummarizerParams());
	}

	public PostgresSummarizerParams loadTestCliqueSummarizerParams(final String configFilepath, final String summaryType, final String datasetFile, final String schemaFile) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading summarizer parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			ConnectionParams connParams = this.loadConnectionParams(config);
			PostgresSummarizationParams summParams = this.loadTestCliqueSummarizationParams(config, summaryType, datasetFile, schemaFile); 
			ExportParams exportParams = this.loadExportParams(config);
			log.info("Parameters loaded from the configuration file.");

			return new PostgresSummarizerParams(connParams, summParams, exportParams);
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	/**
	 * @param config
	 * @param summaryType
	 * @param datasetFile
	 * @param schemaFile
	 * @return {@link PostgresSummarizationParams}
	 */
	private PostgresSummarizationParams loadTestCliqueSummarizationParams(final Configuration config, final String summaryType, final String datasetFile, String schemaFile) {
		final String dataFilepath = Utils.getFilepathToDataset(Constant.TEST_DATASETS_FOLDER, datasetFile);
		final String schemaName = getTestSchemaName(summaryType, dataFilepath);
		PostgresSummarizationParams summParams = new PostgresSummarizationParams(Equivalence.CLIQUE, summaryType, schemaName, dataFilepath);

		summParams.setDropSchema(config.getBoolean(ParameterName.DROP_SCHEMA, false));
		summParams.setIndexing(config.getBoolean(ParameterName.INDEXING, false));		
		summParams.setFetchSize(config.getInt(ParameterName.FETCH_SIZE, 50000));
		summParams.setSaturateInput(config.getBoolean(ParameterName.SATURATE_INPUT, false));
		summParams.setRecomputeSaturation(config.getBoolean(ParameterName.RECOMPUTE_SATURATION, false));
		summParams.setSaturationBatchSize(config.getInt(ParameterName.SATURATION_BATCH_SIZE, 100));

		summParams.setSchemaTriplesFilepath(config.getString(ParameterName.SCHEMA_FILEPATH));
		if (!StringUtils.isNullOrBlank(schemaFile)) {
			final String schemaFilepath = Utils.getFilepathToDataset(Constant.TEST_DATASETS_FOLDER, schemaFile);
			summParams.setSchemaTriplesFilepath(schemaFilepath);
		}

		return summParams;
	}

	public String getTestSchemaName(final String summaryType, final String dataTripesFilepath) {
		Builder = new StringBuilder("test_");
		Builder.append(summaryType).append(Chars.UNDERSCORE).append(FilenameUtils.getBaseName(dataTripesFilepath));

		return Builder.toString();
	}
}
