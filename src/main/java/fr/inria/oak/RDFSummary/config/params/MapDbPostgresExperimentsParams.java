package fr.inria.oak.RDFSummary.config.params;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbPostgresExperimentsParams {

	private final PostgresExperimentsParams postgresExpParams;
	private final String mapDbFolder;
	
	/**
	 * @param postgresExpParams
	 * @param mapDbFolder
	 */
	public MapDbPostgresExperimentsParams(final PostgresExperimentsParams postgresExpParams, final String mapDbFolder) {
		this.postgresExpParams = postgresExpParams;
		this.mapDbFolder = mapDbFolder;
	}

	public PostgresExperimentsParams getPostgresExperimentsParams() {
		return postgresExpParams;
	}
	
	public String getMapDbFolder() {
		return mapDbFolder;
	}
	
	public MapDbPostgresSummarizerParams getMapDbPostgresSummarizerParams() {
		return new MapDbPostgresSummarizerParams(postgresExpParams.getSummarizerParams(), mapDbFolder);
	}
}
