package fr.inria.oak.RDFSummary.config.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.experiments.ExperimentsExporter;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporterImpl;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
public class ExperimentsBeanConfig {

	@Bean
	public ExperimentsExporter experimentsExporterImpl() {
		return new ExperimentsExporterImpl();
	}
}
