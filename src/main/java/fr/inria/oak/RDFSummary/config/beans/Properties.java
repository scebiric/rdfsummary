package fr.inria.oak.RDFSummary.config.beans;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.config.params.SaturationParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParameterLoader;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.Equivalence;
import fr.inria.oak.RDFSummary.data.storage.PostgresTableNames;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
public class Properties {

	@Autowired
	private Environment env;

	private ParameterLoader paramLoader = new ParameterLoader();

	private static final String DATASET = "dataset";

	public String getDatasetDbName() {
		return NameUtils.getDbName(getDatasetName(), DATASET);
	}

	public String getDatasetMapDbFolder() {
		return Utils.getMapDbFolder(env.getProperty(ParameterName.MAPDB_FOLDER), getDatasetName(), DATASET);
	}

	public String getDatasetName() {
		return env.getProperty(ParameterName.DATASET_NAME);
	}

	public String getSchemaName() {
		return env.getProperty(ParameterName.PSQL_SCHEMA_NAME);
	}

	public String getSummaryType() {
		return env.getProperty(ParameterName.SUMMARY_TYPE);
	}

	public String getTriplesTable() {
		return NameUtils.getTriplesTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE));
	}

	public String getSummaryName() {
		return env.getProperty(ParameterName.SUMMARY_NAME);
	}

	public String getMapDbFolder() {
		return env.getProperty(ParameterName.MAPDB_FOLDER);
	}

	public int getFetchSize() {
		return Integer.parseInt(env.getProperty(ParameterName.FETCH_SIZE));
	}

	public String getDataTriplesFilePath() {
		return env.getProperty(ParameterName.DATA_FILEPATH);
	}

	public boolean dropSchema() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.DROP_SCHEMA));
	}

	public String getDictionaryTable() {
		return env.getProperty(ParameterName.DICTIONARY_TABLE);
	}

	public ConnectionParams getConnectionParams() {
		return new ConnectionParams(env.getProperty(ParameterName.DATA_SOURCE_NAME), 
				env.getProperty(ParameterName.SERVER_NAME), Integer.parseInt(env.getProperty(ParameterName.PORT)), 
				env.getProperty(ParameterName.DATABASE_NAME), 
				env.getProperty(ParameterName.USERNAME), env.getProperty(ParameterName.PASSWORD));
	}

	public PostgresTableNames getPostgresTableNames() {
		final String dictionaryTable = NameUtils.getDictionaryTableName(getSchemaName());
		final String triplesTable = getTriplesTable();
		final String encodedTriplesTable = NameUtils.getEncodedTriplesTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE));
		final String saturatedEncodedTriplesTable = NameUtils.getSaturatedEncodedTriplesTableName(encodedTriplesTable);
		String encodedDataTableName = null;
		String encodedTypesTableName = null;
		if (getEquivalence().equals(Equivalence.BISIMULATION)) {
			encodedDataTableName = NameUtils.getDataTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE), saturatedInput());
			encodedTypesTableName = NameUtils.getTypesTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE), saturatedInput());
		}
		else if (getEquivalence().equals(Equivalence.CLIQUE)) {
			if (saturatedInput()) {
				encodedDataTableName = NameUtils.getSaturatedEncodedDataTableName(saturatedEncodedTriplesTable);
				encodedTypesTableName = NameUtils.getSaturatedEncodedTypesTableName(saturatedEncodedTriplesTable);
			}
			else {
				encodedDataTableName = NameUtils.getEncodedDataTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE));
				encodedTypesTableName = NameUtils.getEncodedTypesTableName(getSchemaName(), env.getProperty(ParameterName.TRIPLES_TABLE));
			}
		}

		final PostgresTableNames tables = new PostgresTableNames();
		tables.setDictionaryTable(dictionaryTable);
		tables.setTriplesTable(triplesTable);
		tables.setEncodedTriplesTable(encodedTriplesTable);
		tables.setEncodedDataTable(encodedDataTableName);
		tables.setEncodedTypesTable(encodedTypesTableName);

		return tables;
	}

	private String getEquivalence() {
		return env.getProperty(ParameterName.EQUIVALENCE);
	}

	public String getSchemaTriplesFilepath() {
		return env.getProperty(ParameterName.SCHEMA_FILEPATH);
	}

	public String getServerName() {
		return env.getProperty(ParameterName.SERVER_NAME);
	}

	public int getPort() {
		return Integer.parseInt(env.getProperty(ParameterName.PORT));
	}

	public String getDatabaseName() {
		return env.getProperty(ParameterName.DATABASE_NAME);
	}

	public String getUsername() {
		return env.getProperty(ParameterName.USERNAME);
	}

	public String getPassword() {
		return env.getProperty(ParameterName.PASSWORD);
	}

	public String getOutputFolder() {
		return env.getProperty(ParameterName.OUTPUT_FOLDER);
	}

	public boolean saturatedInput() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.SATURATE_INPUT));
	}

	public SaturationParams getSaturationParams() {
		return new SaturationParams(saturatedInput(), recomputeSaturation(), getBatchSize());
	}

	private boolean recomputeSaturation() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.RECOMPUTE_SATURATION));
	}

	private int getBatchSize() {
		return Integer.parseInt(env.getProperty(ParameterName.SATURATION_BATCH_SIZE));
	}

	public ExportParams getExportParams() throws ConfigurationException, ParametersException {
		final ExportParams exportParams = new ExportParams(getOutputFolder());
		exportParams.setConvertDotToPdf(convertDotToPdf());
		exportParams.setConvertDotToPng(convertDotToPng());
		exportParams.setDotExecutableFilepath(getDotExecutableFilepath());
		exportParams.setDotStyleParams(getDotStyleParams());
		exportParams.setDotStyleParamsFilepath(getDotStyleParamsFilepath());
		exportParams.setExportRepresentationTables(exportRepresentationTables());
		exportParams.setExportStatsToFile(exportStatsToFile());
		exportParams.setExportToRdfFile(exportToRdfFile());
		exportParams.setGenerateDot(generateDot());
		exportParams.setSaturationDotParamsFilepath(getSaturationDotParamsFilepath());
		exportParams.setExportInput(exportInput());

		return exportParams;
	}

	public boolean exportInput() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.EXPORT_INPUT));
	}

	public boolean convertDotToPdf() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.CONVERT_DOT_TO_PDF));
	}

	public boolean convertDotToPng() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.CONVERT_DOT_TO_PNG));
	}

	public String getDotExecutableFilepath() {
		return env.getProperty(ParameterName.DOT_EXECUTABLE_FILEPATH);
	}

	public DotStyleParams getDotStyleParams() throws ConfigurationException, ParametersException {
		return paramLoader.loadDotStyleParams(getDotStyleParamsFilepath());
	}

	public String getDotStyleParamsFilepath() {
		return env.getProperty(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH);
	}

	public boolean exportRepresentationTables() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.EXPORT_REPRESENTATION_TABLES));
	}

	private boolean exportStatsToFile() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.EXPORT_STATS_TO_FILE));
	}

	public boolean exportToRdfFile() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.EXPORT_TO_RDF_FILE));
	}

	public boolean generateDot() {
		return Boolean.parseBoolean(env.getProperty(ParameterName.GENERATE_DOT));
	}

	public String getSaturationDotParamsFilepath() {
		return env.getProperty(ParameterName.SATURATION_DOT_PARAMETERS_FILEPATH);
	}
}
