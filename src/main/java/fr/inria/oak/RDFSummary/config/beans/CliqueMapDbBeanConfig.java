package fr.inria.oak.RDFSummary.config.beans;

import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.openrdf.rio.RDFHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.data.dictionary.MapDbDictionaryImpl;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManagerImpl;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.clique.MapDbCliqueRdfDataset;
import fr.inria.oak.RDFSummary.rdf.dataset.clique.MapDbCliqueRdfDatasetImpl;
import fr.inria.oak.RDFSummary.rdf.handler.MapDbCliqueRdfHandlerImpl;
import fr.inria.oak.RDFSummary.rdf.loader.RioRdfLoaderImpl;
import fr.inria.oak.RDFSummary.rdf.loader.RdfLoader;
import fr.inria.oak.RDFSummary.summary.export.RdfExporter;
import fr.inria.oak.RDFSummary.summary.export.CliqueMapDbRdfExporterImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.MapDbRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.CliqueRdfSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.MapDbCliqueRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.MapDbWeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.clique.WeakPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.DataTripleCreator;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.PureSummaryTypeTripleCreator;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.PureSummaryTypeTripleCreatorImpl;
import fr.inria.oak.RDFSummary.summary.triplecreator.disk.clique.WeakPureDataTripleCreatorImpl;
import fr.inria.oak.RDFSummary.urigenerator.NoBracketsUriGeneratorImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
@Profile(ProfileName.CLIQUE_MAPDB)
public class CliqueMapDbBeanConfig {

	private static final Logger log = Logger.getLogger(CliqueMapDbBeanConfig.class);

	@Autowired
	private Environment env;

	@Autowired
	private BeanConfig beanConfig;

	private static final String DICTIONARY = "dict";	
	private static final String DATASET = "dataset";
	private static final String SUMMARY = "summary";

	private final MessageBuilder messageBuilder = new MessageBuilder();

	@Bean
	public MapDbCliqueRdfDataset mapDbCliqueRdfDatasetImpl() {
		return new MapDbCliqueRdfDatasetImpl(rdfDatasetDB());
	}

	@Bean
	public CliqueRdfSummary mapDbCliqueRdfSummaryImpl() {
		return new MapDbCliqueRdfSummaryImpl(summaryDB(), env.getProperty(ParameterName.SUMMARY_TYPE), mapDbRdfSummaryImpl(), noBracketsUriGeneratorImpl(), mapDbDictionaryImpl());
	}

	@Bean
	public DataTripleCreator weakPureDataTripleCreatorImpl() { 
		return new WeakPureDataTripleCreatorImpl(weakPureCliqueEquivalenceSummaryImpl(), mapDbCliqueRdfDatasetImpl(), mapDbDictionaryImpl());
	}

	@Bean
	public Dictionary mapDbDictionaryImpl() {		
		return new MapDbDictionaryImpl(dictionaryDB());
	}

	@Bean 
	public RDFHandler mapDbCliqueRdfHandlerImpl() {
		return new MapDbCliqueRdfHandlerImpl(mapDbDictionaryImpl(), mapDbCliqueRdfDatasetImpl());
	}

	@Bean
	public RdfLoader rioRdfLoaderImpl() {
		return new RioRdfLoaderImpl(mapDbCliqueRdfDatasetImpl(), getDataTriplesFilepath(), mapDbCliqueRdfHandlerImpl(), beanConfig.timerImpl());
	}

	@Bean
	public PureCliqueEquivalenceSummary pureCliqueEquivalenceSummaryImpl() {
		return new PureCliqueEquivalenceSummaryImpl(mapDbCliqueRdfSummaryImpl(), noBracketsUriGeneratorImpl(), mapDbDictionaryImpl());
	}

	@Bean
	public PureSummaryTypeTripleCreator pureSummaryTypeTripleCreatorImpl() {
		return new PureSummaryTypeTripleCreatorImpl(pureCliqueEquivalenceSummaryImpl(), mapDbCliqueRdfDatasetImpl());
	}

	@Bean
	public RdfExporter cliqueMapDbRdfExporterImpl() { 
		return new CliqueMapDbRdfExporterImpl(env.getProperty(ParameterName.SUMMARY_NAME), mapDbManagerImpl(), weakPureCliqueEquivalenceSummaryImpl(), mapDbCliqueRdfDatasetImpl(), 
				mapDbDictionaryImpl(), beanConfig.dotSummaryVizImpl(), beanConfig.triplesWriterImpl(), beanConfig.timerImpl());
	}

	@Bean
	public WeakEquivalenceSummary mapDbWeakEquivalenceSummaryImpl() {
		return new MapDbWeakEquivalenceSummaryImpl(summaryDB(), mapDbCliqueRdfSummaryImpl(), noBracketsUriGeneratorImpl(), mapDbDictionaryImpl(), beanConfig.timerImpl());
	}

	@Bean
	public WeakPureCliqueEquivalenceSummary weakPureCliqueEquivalenceSummaryImpl() {
		return new WeakPureCliqueEquivalenceSummaryImpl(mapDbWeakEquivalenceSummaryImpl(), pureCliqueEquivalenceSummaryImpl(), mapDbCliqueRdfSummaryImpl());
	}

	@Bean 
	public DB rdfDatasetDB() {
		final String datasetName = env.getProperty(ParameterName.DATASET_NAME);		
		final String datasetMapDbFolder = Utils.getMapDbFolder(env.getProperty(ParameterName.MAPDB_FOLDER), datasetName, DATASET);		
		final String datasetDbName = NameUtils.getDbName(datasetName, DATASET);
		Utils.mkDirs(datasetMapDbFolder);

		return mapDbManagerImpl().getDb(datasetDbName, datasetMapDbFolder);
	}

	@Bean
	public DB dictionaryDB() {
		final String datasetName = env.getProperty(ParameterName.DATASET_NAME);			 
		final String dictDbName = NameUtils.getDbName(datasetName, DICTIONARY);
		final String dictMapDbFolder = env.getProperty(ParameterName.MAPDB_FOLDER);
		Utils.mkDirs(dictMapDbFolder);

		return mapDbManagerImpl().getDb(dictDbName, dictMapDbFolder);
	}

	@Bean 
	public DB summaryDB() {
		final String datasetName = env.getProperty(ParameterName.DATASET_NAME);
		final String summaryMapDbFolder = Utils.getMapDbFolder(env.getProperty(ParameterName.MAPDB_FOLDER), datasetName, SUMMARY);
		final String summaryName = env.getProperty(ParameterName.SUMMARY_NAME);
		boolean deleted = mapDbManagerImpl().deleteDb(summaryName, summaryMapDbFolder);
		if (deleted)
			log.info(messageBuilder.append("Deleted db: ").append(summaryName).append(", in: ").append(summaryMapDbFolder));

		Utils.mkDirs(summaryMapDbFolder);

		return mapDbManagerImpl().createDb(summaryName, summaryMapDbFolder);
	}

	@Bean
	public MapDbManager mapDbManagerImpl() {
		return new MapDbManagerImpl();
	}

	@Bean
	public RdfSummary mapDbRdfSummaryImpl() {
		return new MapDbRdfSummaryImpl(summaryDB(), "data_triples", "type_triples");
	}

	@Bean 
	public UriGenerator noBracketsUriGeneratorImpl() {
		return new NoBracketsUriGeneratorImpl(env.getProperty(ParameterName.DATASET_NAME), env.getProperty(ParameterName.SUMMARY_TYPE));
	}

	private String getDataTriplesFilepath() {
		return env.getProperty(ParameterName.DATA_FILEPATH);
	}
}
