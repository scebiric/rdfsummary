package fr.inria.oak.RDFSummary.config.params;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SaturationParams {

	private final boolean saturatedInput;
	private final boolean recomputeSaturation;
	private final int batchSize;
	
	/**
	 * @param saturatedInput
	 * @param recomputeSaturation
	 * @param batchSize
	 */
	public SaturationParams(final boolean saturatedInput, final boolean recomputeSaturation, final int batchSize) {
		this.saturatedInput = saturatedInput;
		this.recomputeSaturation = recomputeSaturation;
		this.batchSize = batchSize;
	}
	
	public boolean saturatedInput() {
		return saturatedInput;
	}
	
	public boolean recomputeSaturation() {
		return recomputeSaturation;
	}
	
	public int getBatchSize() {
		return batchSize;
	}
}
