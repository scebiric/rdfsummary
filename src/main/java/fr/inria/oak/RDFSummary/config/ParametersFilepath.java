package fr.inria.oak.RDFSummary.config;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParametersFilepath {
	
	public static final String POSTGRES = "config/config-summarizer-postgres.properties";
	public static final String EXPERIMENTS =  "config/config-experiments.properties";  
}
