package fr.inria.oak.RDFSummary.config.beans;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.index.CliquePostgresIndexImpl;
import fr.inria.oak.RDFSummary.data.index.Index;
import fr.inria.oak.RDFSummary.data.storage.EncodedPsqlStorage;
import fr.inria.oak.RDFSummary.data.storage.EncodedPsqlStorageImpl;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporter;
import fr.inria.oak.RDFSummary.experiments.ExperimentsExporterImpl;
import fr.inria.oak.RDFSummary.summary.cliquebuilder.trove.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.cliquebuilder.trove.TroveCliqueBuilderImpl;
import fr.inria.oak.RDFSummary.summary.export.TroveSummaryExporterImpl;
import fr.inria.oak.RDFSummary.summary.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.noderepresenter.TroveNodeRepresenterImpl;
import fr.inria.oak.RDFSummary.summary.stats.clique.CliqueTrovePostgresStatsCollector;
import fr.inria.oak.RDFSummary.summary.stats.clique.CliqueTrovePostgresStatsCollectorImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.components.clique.PostgresTroveRdfComponentSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.components.clique.TroveRdfComponentSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.postgres.clique.TroveCliquePostgresSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.triplecreator.trove.clique.TroveTripleCreator;
import fr.inria.oak.RDFSummary.summary.triplecreator.trove.clique.TroveTripleCreatorImpl;
import fr.inria.oak.commons.db.DictionaryException;

/** 
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
@Profile(ProfileName.CLIQUE_TROVE_POSTGRES)
public class CliqueTrovePostgresBeanConfig {

	@Autowired
	private BeanConfig beanConfig;
	
	@Autowired
	private PostgresBeanConfig postgresConfig;
	
	@Autowired
	private Properties props;
	
	@Bean
	public CliqueTrovePostgresStatsCollector cliqueTrovePostgresStatsCollectorImpl() throws SQLException, DictionaryException {
		return new CliqueTrovePostgresStatsCollectorImpl(postgresConfig.rdfTablesDaoImpl(), postgresConfig.dmlDaoImpl());
	}
	
	@Bean
	public EncodedPsqlStorage encodedPsqlStorageImpl() throws NumberFormatException, SQLException, DictionaryException, ParametersException {
		return new EncodedPsqlStorageImpl(postgresConfig.postgresCopyRdfParserImpl(), postgresConfig.ddlDaoImpl(), 
				postgresConfig.dictDaoImpl(), postgresConfig.encodingImpl(),
				postgresConfig.postgresSaturatorImpl(), postgresConfig.postgresStorageSplitterImpl(), cliquePostgresIndexImpl(), 
				postgresConfig.dmlDaoImpl(), postgresConfig.rdfTablesDaoImpl(), beanConfig.uriReconstructingJenaSchemaParserImpl(), beanConfig.timerImpl());
	}
	
	@Bean
	public ExperimentsExporter experimentsExporterImpl() {
		return new ExperimentsExporterImpl();
	}
	
	@Bean
	public Index cliquePostgresIndexImpl() throws NumberFormatException, SQLException {
		return new CliquePostgresIndexImpl(props.getSchemaName(), props.getPostgresTableNames(), postgresConfig.dmlDaoImpl());
	}
	
	@Bean
	public TroveCliqueBuilder troveCliqueBuilderImpl() throws SQLException, DictionaryException {		
		return new TroveCliqueBuilderImpl(postgresConfig.rdfTablesDaoImpl(), postgresConfig.performanceLoggerImpl());
	}

	@Bean
	public fr.inria.oak.RDFSummary.summary.export.TroveSummaryExporter troveSummaryExporterImpl() throws SQLException, DictionaryException {
		return new TroveSummaryExporterImpl(beanConfig.dotSummaryVizImpl(), beanConfig.triplesWriterImpl(), 
				postgresConfig.sqlConnectionHandlerImpl(), postgresConfig.rdfTablesDaoImpl(), postgresConfig.dictDaoImpl(), 
				postgresConfig.ddlDaoImpl(), postgresConfig.dmlDaoImpl(), postgresConfig.dmlQueriesImpl(), beanConfig.timerImpl());
	}
	
	@Bean
	public fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer summarizer() throws SQLException, DictionaryException, NumberFormatException, ParametersException {
		return new fr.inria.oak.RDFSummary.main.TroveCliquePostgresSummarizer();
	}
	
	@Bean
	public TroveCliquePostgresSummarizer troveCliquePostgresSummarizerImpl() throws SQLException, NumberFormatException, DictionaryException {
		return new TroveCliquePostgresSummarizerImpl(postgresConfig.sqlConnectionHandlerImpl(), postgresTroveRdfComponentSummarizerImpl(), postgresConfig.bracketsUriGeneratorImpl(), 
				postgresConfig.dmlDaoImpl(), postgresConfig.dictDaoImpl(), postgresConfig.rdfTablesDaoImpl(), postgresConfig.performanceLoggerImpl());
	}
	
	@Bean
	public TroveRdfComponentSummarizer postgresTroveRdfComponentSummarizerImpl() throws NumberFormatException, SQLException, DictionaryException {
		return new PostgresTroveRdfComponentSummarizerImpl(postgresConfig.dmlDaoImpl(), postgresConfig.ddlDaoImpl(), postgresConfig.rdfTablesDaoImpl(), 
				troveTripleCreatorImpl(), troveNodeRepresenterImpl(), troveCliqueBuilderImpl(), postgresConfig.performanceLoggerImpl());
	}
	
	@Bean
	public TroveTripleCreator troveTripleCreatorImpl() {
		return new TroveTripleCreatorImpl(troveNodeRepresenterImpl());
	}

	@Bean
	public TroveNodeRepresenter troveNodeRepresenterImpl() {
		return new TroveNodeRepresenterImpl();
	}
}
