package fr.inria.oak.RDFSummary.config.params;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DiskSummarizerParams {

	private final DiskSummarizationParams summarizationParams;
	private final ExportParams exportParams;	
	
	/**
	 * @param summarizationParams
	 * @param exportParams
	 */
	public DiskSummarizerParams(final DiskSummarizationParams summarizationParams, final ExportParams exportParams) {
		this.summarizationParams = summarizationParams;
		this.exportParams = exportParams;
	}

	public DiskSummarizationParams getSummarizationParams() {
		return summarizationParams;
	}
	
	public ExportParams getExportParams() {
		return exportParams;
	}
}
