package fr.inria.oak.RDFSummary.config.params;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ConnectionParams {
	private final String dataSourceName;
	private final String serverName;
	private final int port;
	private final String databaseName;
	private final String username;
	private final String password;
	
	/**
	 * Constructor of {@link ConnectionParams}
	 * 
	 * @param dataSourceName
	 * @param serverName
	 * @param port
	 * @param databaseName
	 * @param username
	 * @param password
	 */
	public ConnectionParams(String dataSourceName, String serverName, int port, String databaseName, String username, String password) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataSourceName), "Data source name is required.");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(serverName), "Server name is required.");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(databaseName), "Database name is required.");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(username), "Username is required.");
		Preconditions.checkArgument(!StringUtils.isNull(password), "Password cannot be null.");
		
		this.dataSourceName = dataSourceName;
		this.serverName = serverName;
		this.port = port;
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
	}
	
	public String getDataSourceName() {
		return this.dataSourceName;
	}
	
	public String getServerName() {
		return this.serverName;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public String getDatabaseName() {
		return this.databaseName;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
}
