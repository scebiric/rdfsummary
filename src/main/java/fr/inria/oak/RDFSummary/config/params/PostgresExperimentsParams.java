package fr.inria.oak.RDFSummary.config.params;

import java.util.List;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresExperimentsParams {

	private String experimentTimestamp;
	private final PostgresSummarizerParams summarizerParams;
	private final List<String> summaryTypes;
	private final int numberOfRuns;
	private boolean logging;
	
	/**
	 * Constructor of {@link PostgresExperimentsParams}
	 * 
	 * @param summarizerParams
	 * @param summaryTypes
	 * @param numberOfRuns
	 */
	public PostgresExperimentsParams(final PostgresSummarizerParams summarizerParams, final List<String> summaryTypes, final int numberOfRuns) {
		this.summarizerParams = summarizerParams;
		this.summaryTypes = summaryTypes;
		this.numberOfRuns = numberOfRuns;
		this.logging = false;
	}
	
	public PostgresSummarizerParams getSummarizerParams() {
		return summarizerParams;
	}
	
	public List<String> getSummaryTypes() {
		return summaryTypes;
	}
	
	public int getNumberOfRuns() {
		return numberOfRuns;
	}
	
	public boolean logging() {
		return logging;
	}
	
	public void setLogging(boolean value) {
		logging = value;
	}

	public void setExperimentTimestamp(final String currentTime) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(currentTime));
		
		experimentTimestamp = currentTime;
	}
	
	public String getExperimentTimestamp() {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(experimentTimestamp));
		
		return experimentTimestamp;
	}
}