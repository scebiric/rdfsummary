package fr.inria.oak.RDFSummary.config.params;

import java.io.File;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DiskSummarizationParams extends SummarizationParams {

	private final String datasetName;
	private final String mapDbFolder;
	
	/**
	 * @param summaryType
	 * @param dataTriplesFilepath
	 * @param datasetName
	 * @param mapDbFolder
	 * @param equivalence
	 */
	public DiskSummarizationParams(final String summaryType, final String dataTriplesFilepath, 
			final String datasetName, final String mapDbFolder, final String equivalence) {
		super(equivalence, summaryType, dataTriplesFilepath);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTriplesFilepath));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(datasetName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(mapDbFolder));
		
		this.datasetName = datasetName;
		this.mapDbFolder = Utils.getFormattedFilepath(mapDbFolder);
		final File file = new File(this.mapDbFolder);
		if (!file.exists())
			file.mkdir();
	}

	public String getDatasetName() {
		return datasetName;
	}
	
	public String getMapDbFolder() {
		return mapDbFolder;
	}
	
	public String getSummaryName() {
		return NameUtils.getSummaryName(datasetName, super.getSummaryType(), saturateInput());
	}
}
