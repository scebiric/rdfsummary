package fr.inria.oak.RDFSummary.config.params;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresSummarizerParams {

	private final ConnectionParams connParams;
	private final PostgresSummarizationParams summarizationParams;
	private final ExportParams exportParams;	

	/**
	 * Constructor of {@link PostgresSummarizerParams}
	 * 
	 * @param connParams
	 * @param summarizationParams
	 * @param exportParams
	 */
	public PostgresSummarizerParams(final ConnectionParams connParams, final PostgresSummarizationParams summarizationParams, final ExportParams exportParams) {
		this.connParams = connParams;
		this.summarizationParams = summarizationParams;
		this.exportParams = exportParams;
	}
	
	public ConnectionParams getConnectionParams() {
		return connParams;
	}
	
	public PostgresSummarizationParams getSummarizationParams() {
		return summarizationParams;
	}
	
	public ExportParams getExportParams() {
		return exportParams;
	}
}
