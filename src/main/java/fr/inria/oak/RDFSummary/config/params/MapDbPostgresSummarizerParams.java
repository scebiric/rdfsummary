package fr.inria.oak.RDFSummary.config.params;

import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbPostgresSummarizerParams {

	private final PostgresSummarizerParams postgresParams;
	private final String mapDbFolder; 
	
	/**
	 * @param postgresParams
	 * @param mapDbFolder
	 */
	public MapDbPostgresSummarizerParams(final PostgresSummarizerParams postgresParams, final String mapDbFolder) {
		this.postgresParams = postgresParams;
		this.mapDbFolder = Utils.getFormattedFilepath(mapDbFolder);
	}

	public PostgresSummarizerParams getPostgresSummarizerParams() {
		return postgresParams;
	}
	
	public String getMapDbFolder() {
		return mapDbFolder;
	}
	
	public String getSummaryName() {
		return NameUtils.getSummaryName(postgresParams.getSummarizationParams().getSchemaName(),
				postgresParams.getSummarizationParams().getSummaryType(), postgresParams.getSummarizationParams().saturateInput());				 
	}
	
	public String getDatasetName() {
		return postgresParams.getSummarizationParams().getSchemaName();
	}
}
