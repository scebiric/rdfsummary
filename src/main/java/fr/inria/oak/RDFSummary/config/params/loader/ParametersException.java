package fr.inria.oak.RDFSummary.config.params.loader;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParametersException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5932226720244252075L;

	public ParametersException() {
		// TODO Auto-generated constructor stub
	}

	public ParametersException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ParametersException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ParametersException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ParametersException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
