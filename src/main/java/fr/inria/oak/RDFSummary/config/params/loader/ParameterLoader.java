package fr.inria.oak.RDFSummary.config.params.loader;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.config.params.CmdParams;
import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParameterLoader {

	protected static final Logger log = Logger.getLogger(ParameterLoader.class);
	
	/**
	 * @param config
	 * @return {@link ExportParams}
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 */
	public ExportParams loadExportParams(final Configuration config) throws ConfigurationException, ParametersException {
		ExportParams exportParams = new ExportParams(config.getString(ParameterName.OUTPUT_FOLDER));

		exportParams.setGenerateDot(config.getBoolean(ParameterName.GENERATE_DOT, false));
		exportParams.setConvertDotToPdf(config.getBoolean(ParameterName.CONVERT_DOT_TO_PDF, false));
		exportParams.setConvertDotToPng(config.getBoolean(ParameterName.CONVERT_DOT_TO_PNG, false));
		exportParams.setExportToRdfFile(config.getBoolean(ParameterName.EXPORT_TO_RDF_FILE, false));
		exportParams.setExportInput(config.getBoolean(ParameterName.EXPORT_INPUT, false));
		exportParams.setExportStatsToFile(config.getBoolean(ParameterName.EXPORT_STATS_TO_FILE, false));
		exportParams.setExportRepresentationTables(config.getBoolean(ParameterName.EXPORT_REPRESENTATION_TABLES, false));
		exportParams.setDotExecutableFilepath(config.getString(ParameterName.DOT_EXECUTABLE_FILEPATH));
		exportParams.setDotStyleParamsFilepath(config.getString(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH));
		
		if (exportParams.generateDot())
			exportParams.setDotStyleParams(loadDotStyleParams(exportParams.getDotStyleParamsFilepath()));

		return exportParams;
	}

	public ExportParams loadExportParams(final String filepath) throws ConfigurationException, ParametersException {
		Configuration config = new PropertiesConfiguration(filepath);
		ExportParams exportParams = null;
		if (!config.isEmpty()) {
			exportParams = new ExportParams(config.getString(ParameterName.OUTPUT_FOLDER));

			exportParams.setGenerateDot(config.getBoolean(ParameterName.GENERATE_DOT, false));
			exportParams.setExportInput(config.getBoolean(ParameterName.EXPORT_INPUT, false));
			exportParams.setConvertDotToPdf(config.getBoolean(ParameterName.CONVERT_DOT_TO_PDF, false));
			exportParams.setConvertDotToPng(config.getBoolean(ParameterName.CONVERT_DOT_TO_PNG, false));
			exportParams.setExportToRdfFile(config.getBoolean(ParameterName.EXPORT_TO_RDF_FILE, false));
			exportParams.setExportStatsToFile(config.getBoolean(ParameterName.EXPORT_STATS_TO_FILE, false));
			exportParams.setExportRepresentationTables(config.getBoolean(ParameterName.EXPORT_REPRESENTATION_TABLES, false));
			exportParams.setDotExecutableFilepath(config.getString(ParameterName.DOT_EXECUTABLE_FILEPATH));
			exportParams.setDotStyleParamsFilepath(config.getString(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH));
			
			if (exportParams.generateDot())
				exportParams.setDotStyleParams(loadDotStyleParams(exportParams.getDotStyleParamsFilepath()));
		}
		else
			throw new ParametersException("The export parameters file is empty.");

		return exportParams;
	}
	
	/**
	 * @param filepath
	 * @return The dot parameters loaded from a configuration file at the specified filepath
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 */
	public DotStyleParams loadDotStyleParams(final String filepath) throws ParametersException, ConfigurationException {
		log.info("Loading dot configuration parameters from: " + filepath);
		DotStyleParams dotParams = null;
		Configuration config = new PropertiesConfiguration(filepath);
		if (!config.isEmpty()) {
			boolean showGraphLabel = config.getBoolean("SHOW_GRAPH_LABEL", true);
			String classNodeShape = config.getString("CLASS_NODE_SHAPE");
			String classNodeStyle = config.getString("CLASS_NODE_STYLE");
			String classNodeColor = config.getString("CLASS_NODE_COLOR");
			String dataNodeShape = config.getString("DATA_NODE_SHAPE");
			String dataNodeStyle = config.getString("DATA_NODE_STYLE");
			String dataNodeColor = config.getString("DATA_NODE_COLOR");
			boolean descriptiveDataNodeLabel = config.getBoolean("DESCRIPTIVE_DATA_NODE_LABEL", false);
			String propertyNodeShape = config.getString("PROPERTY_NODE_SHAPE");
			String propertyNodeStyle = config.getString("PROPERTY_NODE_STYLE");
			String propertyNodeColor = config.getString("PROPERTY_NODE_COLOR");
			String classAndPropertyNodeShape = config.getString("CLASS_AND_PROPERTY_NODE_SHAPE");
			String classAndPropertyNodeStyle = config.getString("CLASS_AND_PROPERTY_NODE_STYLE");
			String classAndPropertyNodeColor = config.getString("CLASS_AND_PROPERTY_NODE_COLOR");
			String dataPropertyEdgeStyle = config.getString("DATA_PROPERTY_EDGE_STYLE");
			String dataPropertyEdgeColor = config.getString("DATA_PROPERTY_EDGE_COLOR");
			String typeEdgeStyle = config.getString("TYPE_EDGE_STYLE");
			String typeEdgeColor = config.getString("TYPE_EDGE_COLOR");
			String schemaEdgeColor = config.getString("SCHEMA_EDGE_COLOR");
			String schemaLabelColor = config.getString("SCHEMA_LABEL_COLOR");

			dotParams = new DotStyleParams(showGraphLabel, classNodeShape, classNodeStyle, classNodeColor, 
					dataNodeShape, dataNodeStyle, dataNodeColor, descriptiveDataNodeLabel, 
					propertyNodeShape, propertyNodeStyle, propertyNodeColor, 
					classAndPropertyNodeShape, classAndPropertyNodeStyle, classAndPropertyNodeColor,
					dataPropertyEdgeStyle, dataPropertyEdgeColor, typeEdgeStyle, typeEdgeColor, schemaEdgeColor, schemaLabelColor);
		}
		else
			throw new ParametersException("The dot configuration parameters file is empty.");

		return dotParams;
	}

	/**
	 * @param cmdParams
	 * @return {@link ExportParams}
	 */
	public ExportParams loadExportParams(CmdParams cmdParams) {
		final ExportParams exportParams = new ExportParams(cmdParams.OUTPUT_FOLDER);
		exportParams.setGenerateDot(cmdParams.GENERATE_DOT);
		exportParams.setConvertDotToPdf(cmdParams.CONVERT_DOT_TO_PDF);
		exportParams.setConvertDotToPng(cmdParams.CONVERT_DOT_TO_PNG);
		exportParams.setExportInput(cmdParams.EXPORT_INPUT); 
		exportParams.setExportToRdfFile(cmdParams.EXPORT_TO_RDF_FILE);
		exportParams.setExportStatsToFile(cmdParams.EXPORT_STATS_TO_FILE);
		exportParams.setExportRepresentationTables(cmdParams.EXPORT_REPRESENTATION_TABLES);
		exportParams.setDotExecutableFilepath(cmdParams.DOT_EXECUTABLE_FILEPATH); 
		exportParams.setDotStyleParamsFilepath(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH);
		
		return exportParams;
	}
}
