package fr.inria.oak.RDFSummary.config.params.loader;

import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.config.ParameterValidator;
import fr.inria.oak.RDFSummary.config.params.CmdParams;
import fr.inria.oak.RDFSummary.config.params.DiskSummarizationParams;
import fr.inria.oak.RDFSummary.config.params.DiskSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.Equivalence;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DiskParameterLoader extends ParameterLoader {

	private static JCommander jc;

	/**
	 * @param configFilepath
	 * @return {@link DiskSummarizerParams}
	 * @throws ConfigurationException
	 * @throws ParametersException
	 */
	public DiskSummarizerParams loadDiskSummarizerParams(String configFilepath) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading summarizer parameters from " + configFilepath + Constant.THREE_DOTS);
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			DiskSummarizationParams summParams = this.loadDiskSummarizationParams(config);
			ExportParams exportParams = this.loadExportParams(config);
			log.info("Parameters loaded from the configuration file.");

			return new DiskSummarizerParams(summParams, exportParams);
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	/**
	 * @param config
	 * @return {@link DiskSummarizationParams}
	 */
	private DiskSummarizationParams loadDiskSummarizationParams(final Configuration config) {
		DiskSummarizationParams summParams = new DiskSummarizationParams(config.getString(ParameterName.SUMMARY_TYPE), 
				config.getString(ParameterName.DATA_FILEPATH), config.getString(ParameterName.DATASET_NAME), 
				config.getString(ParameterName.MAPDB_FOLDER), config.getString(ParameterName.EQUIVALENCE)); 

		summParams.setSchemaTriplesFilepath(config.getString(ParameterName.SCHEMA_FILEPATH));
		summParams.setSaturateInput(config.getBoolean(ParameterName.SATURATE_INPUT, false));
		summParams.setRecomputeSaturation(config.getBoolean(ParameterName.RECOMPUTE_SATURATION, false));
		summParams.setSaturationBatchSize(config.getInt(ParameterName.SATURATION_BATCH_SIZE, 100));

		return summParams;
	}

	/**
	 * Reads the specified command line parameters into the SummarizerParams class
	 * @param args
	 * @return {@link DiskSummarizerParams}
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public DiskSummarizerParams loadDiskSummarizerParams(String[] args) throws ConfigurationException, ParametersException {
		log.info("Loading command line parameters...");
		CmdParams cmdParams = new CmdParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return this.loadDiskSummarizerParams(cmdParams.CONFIG_FILEPATH);
		}
		else 
			return this.loadDiskSummarizerParams(cmdParams);
	}

	/**
	 * @param cmdParams
	 * @return {@link DiskSummarizerParams}
	 * @throws ConfigurationException
	 * @throws ParametersException
	 */
	private DiskSummarizerParams loadDiskSummarizerParams(final CmdParams cmdParams) throws ConfigurationException, ParametersException {
		DiskSummarizationParams summParams = new DiskSummarizationParams(cmdParams.SUMMARY_TYPE, cmdParams.DATA_FILEPATH, cmdParams.DATASET_NAME, cmdParams.MAPDB_FOLDER, cmdParams.EQUIVALENCE);
		summParams.setSchemaTriplesFilepath(cmdParams.RDFS_FILEPATH);
		summParams.setSaturateInput(cmdParams.SATURATE_INPUT);
		summParams.setRecomputeSaturation(cmdParams.RECOMPUTE_SATURATION);
		summParams.setSaturationBatchSize(cmdParams.SATURATION_BATCH_SIZE);

		ExportParams exportParams = loadExportParams(cmdParams);

		if (!StringUtils.isNullOrBlank(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH))
			exportParams.setDotStyleParams(loadDotStyleParams(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH));

		log.info("Summarizer parameters loaded from the command line.");
		return new DiskSummarizerParams(summParams, exportParams);
	}

	private static void displayUsage() {
		if (jc != null) {
			jc.usage();
		}
	}

	/**
	 * Loads those parameters to properties which are needed to inject beans when setting up the application context
	 * 
	 * @param summParams
	 * @return The {@link Properties} object
	 */
	public Properties loadProperties(final DiskSummarizerParams summParams) {
		Preconditions.checkNotNull(summParams);
		final Properties properties = new Properties();

		properties.put(ParameterName.DATASET_NAME, summParams.getSummarizationParams().getDatasetName());
		properties.put(ParameterName.MAPDB_FOLDER, summParams.getSummarizationParams().getMapDbFolder());
		properties.put(ParameterName.EQUIVALENCE, summParams.getSummarizationParams().getNodeEquivalenceRelation());
		properties.put(ParameterName.SUMMARY_NAME, summParams.getSummarizationParams().getSummaryName());
		properties.put(ParameterName.SATURATE_INPUT, summParams.getSummarizationParams().saturateInput());
		properties.put(ParameterName.DATA_FILEPATH, summParams.getSummarizationParams().getDataTriplesFilepath());
		properties.put(ParameterName.SCHEMA_FILEPATH, summParams.getSummarizationParams().getSchemaTriplesFilepath());
		
		if (!StringUtils.isNullOrBlank(summParams.getSummarizationParams().getSummaryType()))	
			properties.put(ParameterName.SUMMARY_TYPE, summParams.getSummarizationParams().getSummaryType());
		
		return properties;
	}

	public DiskSummarizerParams loadTestSummarizerParams(final String configFilepath, final String summaryType, final String datasetFile, final String datasetName, final String schemaFile, final String mapDbFolder) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading summarizer parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			DiskSummarizationParams summParams = this.loadTestSummarizationParams(config, summaryType, datasetFile, datasetName, schemaFile, mapDbFolder); 
			ExportParams exportParams = this.loadExportParams(config);
			log.info("Parameters loaded from the configuration file.");

			final DiskSummarizerParams params = new DiskSummarizerParams(summParams, exportParams);
			ParameterValidator.validateDiskSummarizerParams(params);

			return params;
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	/**
	 * @param config
	 * @param summaryType
	 * @param datasetFile
	 * @param schemaFile
	 * @return {@link DiskSummarizationParams}
	 */
	private DiskSummarizationParams loadTestSummarizationParams(final Configuration config, final String summaryType, 
			final String datasetFile, final String datasetName, final String schemaFile, final String mapDbFolder) {
		final String dataFilepath = Utils.getFilepathToDataset(Constant.TEST_DATASETS_FOLDER, datasetFile);
		DiskSummarizationParams summParams = new DiskSummarizationParams(summaryType, dataFilepath, datasetName, mapDbFolder, Equivalence.CLIQUE);

		summParams.setSaturateInput(config.getBoolean(ParameterName.SATURATE_INPUT, false));
		summParams.setRecomputeSaturation(config.getBoolean(ParameterName.RECOMPUTE_SATURATION, false));
		summParams.setSaturationBatchSize(config.getInt(ParameterName.SATURATION_BATCH_SIZE, 100));
		summParams.setSchemaTriplesFilepath(config.getString(ParameterName.SCHEMA_FILEPATH));

		if (!StringUtils.isNullOrBlank(schemaFile)) {
			final String schemaFilepath = Utils.getFilepathToDataset(Constant.TEST_DATASETS_FOLDER, schemaFile);
			summParams.setSchemaTriplesFilepath(schemaFilepath);
		}

		return summParams;
	}
}
