package fr.inria.oak.RDFSummary.config;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertiesPropertySource;

import java.util.Properties;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.beans.BeanConfig;
import fr.inria.oak.RDFSummary.config.beans.PostgresBeanConfig;
import fr.inria.oak.RDFSummary.config.beans.ProfileName;
import fr.inria.oak.RDFSummary.config.beans.CliqueTrovePostgresBeanConfig;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.config.params.loader.PostgresParameterLoader;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@ComponentScan(value = {"fr.inria.oak.RDFSummary.config"})
public class CliqueTrovePostgresConfigurator {

	private final PostgresParameterLoader postgresParamLoader = new PostgresParameterLoader();

	/**
	 * Loads summarizer parameters from either the command line or a configuration file
	 * 
	 * @param args
	 * @return The loaded parameters
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresSummarizerParams loadPostgresSummarizerParameters(String[] args) throws ConfigurationException, ParametersException {
		PostgresSummarizerParams summarizerParams = null;
		if (args.length > 0) 
			summarizerParams = postgresParamLoader.loadPostgresSummarizerParams(args);			
		else 
			summarizerParams = postgresParamLoader.loadPostgresSummarizerParams(ParametersFilepath.POSTGRES);

		ParameterValidator.validatePostgresSummarizerParams(summarizerParams);

		return summarizerParams;
	}

	/**
	 * Loads experiments parameters from either the command line or a configuration file
	 * 
	 * @param args
	 * @return The loaded parameters
	 * @throws ConfigurationException 
	 * @throws ParametersException 
	 */
	public PostgresExperimentsParams loadExperimentsParameters(String[] args) throws ConfigurationException, ParametersException {
		PostgresExperimentsParams expParams = null;
		if (args.length > 0)
			expParams = postgresParamLoader.loadExperimentsParams(args);
		else
			expParams = postgresParamLoader.loadExperimentsParams(ParametersFilepath.EXPERIMENTS);

		return expParams;
	}	

	/**
	 * Sets up application context based on {@link PostgresSummarizerParams}
	 * @param context 
	 * @param params
	 * @return The {@link AnnotationConfigApplicationContext}
	 * @throws ConfigurationException 
	 */
	public void javaSetUpApplicationContext(final AnnotationConfigApplicationContext context, final PostgresSummarizerParams params) throws ConfigurationException {
		Preconditions.checkNotNull(context);
		Preconditions.checkNotNull(params);

		Properties properties = postgresParamLoader.loadProperties(params);
		if (properties == null)
			throw new ConfigurationException("Properties cannot be null.");

		this.registerBeans(context, properties);
	}

	/**
	 * Sets up application context based on {@link PostgresExperimentsParams}
	 * @param context 
	 * @param params
	 * @return The {@link AnnotationConfigApplicationContext}
	 * @throws ConfigurationException 
	 */
	public void javaSetUpApplicationContext(final AnnotationConfigApplicationContext context, final PostgresExperimentsParams params) throws ConfigurationException {
		Preconditions.checkNotNull(context);
		Preconditions.checkNotNull(params);
		
		Properties properties = postgresParamLoader.loadProperties(params);
		if (properties == null)
			throw new ConfigurationException("Properties cannot be null.");

		registerBeans(context, properties);
	}
	
	/**
	 * Registers beans in the context based on the properties and refreshes the context
	 * 
	 * @param context
	 * @param properties
	 */
	private void registerBeans(AnnotationConfigApplicationContext context, Properties properties) {
		PropertiesPropertySource propertiesSource = new PropertiesPropertySource("props", properties);
		context.getEnvironment().getPropertySources().addLast(propertiesSource);
		context.getEnvironment().setActiveProfiles(ProfileName.CLIQUE_TROVE_POSTGRES);
		context.register(BeanConfig.class);
		context.register(PostgresBeanConfig.class);
		context.register(CliqueTrovePostgresBeanConfig.class);
		context.refresh();
	}
}
