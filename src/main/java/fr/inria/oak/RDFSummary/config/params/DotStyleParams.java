package fr.inria.oak.RDFSummary.config.params;

/**
 * Configuration parameters of dot visualization
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DotStyleParams {

	private final boolean showGraphLabel;
	
	private final String classNodeShape;
	private final String classNodeStyle;
	private final String classNodeColor;
	
	private final String dataNodeShape;
	private final String dataNodeStyle;
	private final String dataNodeColor;
	private final boolean descriptiveDataNodeLabel;
	
	private final String propertyNodeShape;
	private final String propertyNodeStyle;
	private final String propertyNodeColor;
	
	private final String classAndPropertyNodeShape;
	private final String classAndPropertyNodeStyle;
	private final String classAndPropertyNodeColor;
	
	private final String dataPropertyEdgeStyle;
	private final String dataPropertyEdgeColor;
	private final String typeEdgeStyle;
	private final String typeEdgeColor;
	private final String schemaEdgeColor;
	private final String schemaLabelColor;
	
	public DotStyleParams(boolean showGraphLabel, String classNodeShape, String classNodeStyle, String classNodeColor,
			String dataNodeShape, String dataNodeStyle, String dataNodeColor, boolean descriptiveDataNodeLabel,
			String propertyNodeShape, String propertyNodeStyle, String propertyNodeColor,
			String classAndPropertyNodeShape, String classAndPropertyNodeStyle, String classAndPropertyNodeColor,
			String dataPropertyEdgeStyle, String dataPropertyEdgeColor, 
			String typeEdgeStyle, String typeEdgeColor, 
			String schemaEdgeColor, String schemaLabelColor) {
		this.showGraphLabel = showGraphLabel;
		this.classNodeShape = classNodeShape;
		this.classNodeStyle = classNodeStyle;
		this.classNodeColor = classNodeColor;
		this.dataNodeShape = dataNodeShape;
		this.dataNodeStyle = dataNodeStyle;
		this.dataNodeColor = dataNodeColor;
		this.descriptiveDataNodeLabel = descriptiveDataNodeLabel;
		this.propertyNodeShape = propertyNodeShape;
		this.propertyNodeStyle = propertyNodeStyle;
		this.propertyNodeColor = propertyNodeColor;
		this.classAndPropertyNodeShape = classAndPropertyNodeShape;
		this.classAndPropertyNodeStyle = classAndPropertyNodeStyle;
		this.classAndPropertyNodeColor = classAndPropertyNodeColor;
		this.dataPropertyEdgeStyle = dataPropertyEdgeStyle;
		this.dataPropertyEdgeColor = dataPropertyEdgeColor;
		this.typeEdgeStyle = typeEdgeStyle;
		this.typeEdgeColor = typeEdgeColor;
		this.schemaEdgeColor = schemaEdgeColor;
		this.schemaLabelColor = schemaLabelColor;
	}
	
	public boolean showGraphLabel() {
		return this.showGraphLabel;
	}
	
	public String getClassNodeShape() {
		return this.classNodeShape;
	}
	
	public String getClassNodeStyle() {
		return this.classNodeStyle;
	}
	
	public String getClassNodeColor() {
		return this.classNodeColor;
	}
	
	public String getDataNodeShape() {
		return this.dataNodeShape;
	}
	
	public String getDataNodeStyle() {
		return this.dataNodeStyle;
	}
	
	public String getDataNodeColor() {
		return this.dataNodeColor;
	}
	
	public boolean descriptiveDataNodeLabel() {
		return this.descriptiveDataNodeLabel;
	}
	
	public String getPropertyNodeShape() {
		return this.propertyNodeShape;
	}
	
	public String getPropertyNodeStyle() {
		return this.propertyNodeStyle;
	}
	
	public String getPropertyNodeColor() {
		return this.propertyNodeColor;
	}
	
	public String getClassAndPropertyNodeShape() {
		return this.classAndPropertyNodeShape;
	}
	
	public String getClassAndPropertyNodeStyle() {
		return this.classAndPropertyNodeStyle;
	}
	
	public String getClassAndPropertyNodeColor() {
		return this.classAndPropertyNodeColor;
	}
	
	public String getDataPropertyEdgeStyle() {
		return this.dataPropertyEdgeStyle;
	}
	
	public String getDataPropertyEdgeColor() {
		return this.dataPropertyEdgeColor;
	}
	
	public String getTypeEdgeStyle() {
		return this.typeEdgeStyle;
	}
	
	public String getTypeEdgeColor() {
		return this.typeEdgeColor;
	}
	
	public String getSchemaEdgeColor() {
		return this.schemaEdgeColor;
	}
	
	public String getSchemaLabelColor() {
		return this.schemaLabelColor;
	}
}
