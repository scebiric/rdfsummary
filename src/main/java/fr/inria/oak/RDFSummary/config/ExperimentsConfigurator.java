package fr.inria.oak.RDFSummary.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.beans.ExperimentsBeanConfig;
import fr.inria.oak.RDFSummary.config.beans.ProfileName;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExperimentsConfigurator {

	public void javaSetUpApplicationContext(final AnnotationConfigApplicationContext context) {
		Preconditions.checkNotNull(context);

		registerBeans(context);
	}

	private void registerBeans(final AnnotationConfigApplicationContext context) {
		context.getEnvironment().setActiveProfiles(ProfileName.EXPERIMENTS);
		context.register(ExperimentsBeanConfig.class);
		context.refresh();
	}

}
