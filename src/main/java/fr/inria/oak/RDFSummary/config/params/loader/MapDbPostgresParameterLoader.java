package fr.inria.oak.RDFSummary.config.params.loader;

import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.ParameterName;
import fr.inria.oak.RDFSummary.config.params.CmdParams;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.PostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MapDbPostgresParameterLoader extends ParameterLoader {

	private static JCommander jc;
	private final PostgresParameterLoader postgresParamLoader;

	public MapDbPostgresParameterLoader() {
		postgresParamLoader = new PostgresParameterLoader();
	}

	/**
	 * @param configFilepath
	 * @return {@link MapDbPostgresSummarizerParams}
	 * @throws ConfigurationException
	 * @throws ParametersException
	 */
	public MapDbPostgresSummarizerParams loadMapDbPostgresSummarizerParams(final String configFilepath) throws ConfigurationException, ParametersException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading MapDB/Postgres summarizer parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			final PostgresSummarizerParams postgresParams = postgresParamLoader.loadPostgresSummarizerParams(configFilepath);
			final String mapDbFolder = config.getString(ParameterName.MAPDB_FOLDER);

			return new MapDbPostgresSummarizerParams(postgresParams, mapDbFolder);
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	public MapDbPostgresSummarizerParams loadMapDbPostgresSummarizerParams(String[] args) throws ConfigurationException, ParametersException {
		log.info("Loading command line parameters...");
		CmdParams cmdParams = new CmdParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return loadMapDbPostgresSummarizerParams(cmdParams.CONFIG_FILEPATH);
		}
		else 
			return loadMapDbPostgresSummarizerParams(cmdParams);
	}

	/**
	 * @param cmdParams
	 * @return {@link MapDbPostgresSummarizerParams}
	 * @throws ConfigurationException
	 * @throws ParametersException
	 */
	public MapDbPostgresSummarizerParams loadMapDbPostgresSummarizerParams(final CmdParams cmdParams) throws ConfigurationException, ParametersException {
		final PostgresSummarizerParams postgresParams = postgresParamLoader.loadPostgresSummarizerParams(cmdParams);
		final MapDbPostgresSummarizerParams params = new MapDbPostgresSummarizerParams(postgresParams, cmdParams.MAPDB_FOLDER);

		log.info("Summarizer parameters loaded from the command line.");

		return params;
	}
	
	public MapDbPostgresExperimentsParams loadMapDbPostgresExperimentsParams(String configFilepath) throws ConfigurationException, ParametersException {
		final PostgresExperimentsParams postgresExpParams = postgresParamLoader.loadExperimentsParams(configFilepath);
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {	
			final String mapDbFolder = config.getString(ParameterName.MAPDB_FOLDER);
			final MapDbPostgresExperimentsParams mapDbPostgresParams = new MapDbPostgresExperimentsParams(postgresExpParams, mapDbFolder);
			
			return mapDbPostgresParams;
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	public MapDbPostgresExperimentsParams loadMapDbPostgresExperimentsParams(String[] args) throws ConfigurationException, ParametersException {
		CmdParams cmdParams = new CmdParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);
		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}
		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return loadMapDbPostgresExperimentsParams(cmdParams.CONFIG_FILEPATH);
		}
		else {
			final PostgresExperimentsParams postgresParams = postgresParamLoader.loadExperimentsParamsFromCmd(cmdParams);
			
			return new MapDbPostgresExperimentsParams(postgresParams, cmdParams.MAPDB_FOLDER); 
		}
	}


	private static void displayUsage() {
		Preconditions.checkNotNull(jc);

		jc.usage();
	}

	/**
	 * @param params
	 * @return {@link Properties}
	 */
	public Properties loadProperties(final MapDbPostgresSummarizerParams params) {
		Preconditions.checkNotNull(params);
		Properties properties = postgresParamLoader.loadProperties(params.getPostgresSummarizerParams());
		properties.put(ParameterName.DATASET_NAME, params.getDatasetName());
		properties.put(ParameterName.SUMMARY_TYPE, params.getPostgresSummarizerParams().getSummarizationParams().getSummaryType());
		properties.put(ParameterName.SUMMARY_NAME, params.getSummaryName());
		properties.put(ParameterName.MAPDB_FOLDER, params.getMapDbFolder());
		properties.put(ParameterName.DATA_FILEPATH, params.getPostgresSummarizerParams().getSummarizationParams().getDataTriplesFilepath());
		if (!StringUtils.isNullOrBlank(params.getPostgresSummarizerParams().getSummarizationParams().getSchemaTriplesFilepath()))
			properties.put(ParameterName.SCHEMA_FILEPATH, params.getPostgresSummarizerParams().getSummarizationParams().getSchemaTriplesFilepath());

		return properties;
	}
}
