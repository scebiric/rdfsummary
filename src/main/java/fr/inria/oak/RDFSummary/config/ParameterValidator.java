package fr.inria.oak.RDFSummary.config;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.DiskSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.PostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.constants.DataSourceName;
import fr.inria.oak.RDFSummary.constants.Equivalence;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParameterValidator {

	private static final Logger log = Logger.getLogger(ParameterValidator.class);

	private static MessageBuilder messageBuilder = new MessageBuilder();
	private static String[] allowedValues;

	/**
	 * Validates the parameters loaded to the Parameters class
	 * @param params 
	 * @throws ParametersException 
	 */
	public static void validatePostgresSummarizerParams(final PostgresSummarizerParams params) throws ParametersException {
		Preconditions.checkNotNull(params);
		log.info("Validating parameters...");

		validateEquivalenceAndSummaryType(params.getSummarizationParams().getNodeEquivalenceRelation(), params.getSummarizationParams().getSummaryType());

		required(ParameterName.DATA_SOURCE_NAME, params.getConnectionParams().getDataSourceName());
		allowedValues = new String[] { DataSourceName.POSTGRES };

		if (params.getConnectionParams().getDataSourceName().equals(DataSourceName.POSTGRES)) {
			required(ParameterName.SERVER_NAME, params.getConnectionParams().getServerName());
			required(ParameterName.USERNAME, params.getConnectionParams().getUsername());
			required(ParameterName.PASSWORD, params.getConnectionParams().getPassword());
			required(ParameterName.DATABASE_NAME, params.getConnectionParams().getDatabaseName());
			required(ParameterName.PSQL_SCHEMA_NAME, params.getSummarizationParams().getSchemaName());
			required(ParameterName.DATA_FILEPATH, params.getSummarizationParams().getDataTriplesFilepath());			
		}

		required(ParameterName.OUTPUT_FOLDER, params.getExportParams().getOutputFolder());

		if (params.getExportParams().generateDot()) {
			required(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH, params.getExportParams().getDotStyleParamsFilepath());
		}

		if (params.getExportParams().convertDotToPdf() || params.getExportParams().convertDotToPng()) {
			required(ParameterName.DOT_EXECUTABLE_FILEPATH, params.getExportParams().getDotExecutableFilepath());
		}

		log.info("Parameters validated.");
	}

	public static void validateMapDbPostgresSummarizerParams(final MapDbPostgresSummarizerParams params) throws ParametersException {
		validatePostgresSummarizerParams(params.getPostgresSummarizerParams());
		required(ParameterName.MAPDB_FOLDER, params.getMapDbFolder());
	}
	
	public static void validateDiskSummarizerParams(final DiskSummarizerParams params) throws ParametersException {
		Preconditions.checkNotNull(params);
		log.info("Validating parameters...");

		validateEquivalenceAndSummaryType(params.getSummarizationParams().getNodeEquivalenceRelation(), params.getSummarizationParams().getSummaryType());
		required(ParameterName.OUTPUT_FOLDER, params.getExportParams().getOutputFolder());

		if (params.getExportParams().generateDot()) {
			required(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH, params.getExportParams().getDotStyleParamsFilepath());
		}

		if (params.getExportParams().convertDotToPdf() || params.getExportParams().convertDotToPng()) {
			required(ParameterName.DOT_EXECUTABLE_FILEPATH, params.getExportParams().getDotExecutableFilepath());
		}

		log.info("Parameters validated.");
	}

	private static void validateEquivalenceAndSummaryType(final String equivalence, final String summaryType) throws ParametersException {
		allowedValues = new String[] { Equivalence.CLIQUE, Equivalence.BISIMULATION };
		validValue(ParameterName.EQUIVALENCE, allowedValues, equivalence);

		required(ParameterName.SUMMARY_TYPE, summaryType);
		if (equivalence.equals(Equivalence.CLIQUE)) {
			allowedValues = new String[] { SummaryType.WEAK, SummaryType.STRONG, SummaryType.TYPED_WEAK, SummaryType.TYPED_STRONG };
			validValue(ParameterName.SUMMARY_TYPE, allowedValues, summaryType);
		}
		else if (equivalence.equals(Equivalence.BISIMULATION)) {
			allowedValues = new String[] { SummaryType.FORWARD_ONLY, SummaryType.BACKWARD_ONLY, SummaryType.FORWARD_BACKWARD };
			validValue(ParameterName.SUMMARY_TYPE, allowedValues, summaryType);
		}
	}
	
	public static void required(final String label, final String value) throws ParametersException {
		if (StringUtils.isNullOrBlank(value))
			throw new ParametersException("Required: " + label);
	}

	public static void validValue(final String label, final String[] allowedValues, final String value) throws ParametersException { 
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(label));
		Preconditions.checkNotNull(allowedValues);
		Preconditions.checkArgument(allowedValues.length > 0);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(value));

		for (final String allowedValue : allowedValues) {
			if (value.equals(allowedValue))
				return;
		}	

		messageBuilder.append("Invalid value of ").append(label).append(". Valid values: ");
		for (final String allowedValue : allowedValues) {
			messageBuilder.append(allowedValue).append(" | ");
		}

		throw new ParametersException(messageBuilder.toString());
	}
}
