package fr.inria.oak.RDFSummary.config.params;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummarizationParams {
	
	private final String equivalence;
	private String summaryType;
	private String dataTriplesFilepath;
	private String schemaTriplesFilepath;
	private boolean saturateInput = false;
	private boolean recomputeSaturation = false;
	private int saturationBatchSize;
	
	/**
	 * @param equivalence
	 * @param summaryType
	 * @param dataTriplesFilepath
	 */
	public SummarizationParams(final String equivalence, final String summaryType, final String dataTriplesFilepath) {
		this.equivalence = StringUtils.toLower(equivalence);
		this.summaryType = StringUtils.toLower(summaryType);
		this.dataTriplesFilepath = dataTriplesFilepath;
	}

	public String getNodeEquivalenceRelation() {
		return equivalence;
	}
	
	public String getSummaryType() {
		return summaryType;
	}
	
	public String getDataTriplesFilepath() {
		return dataTriplesFilepath;
	}

	public String getSchemaTriplesFilepath() {
		return schemaTriplesFilepath;
	}
	
	public boolean saturateInput() {
		return saturateInput;
	}
	
	public boolean recomputeSaturation() {
		return recomputeSaturation;
	}

	public int getSaturationBatchSize() {
		return saturationBatchSize;
	}
	
	public void setSchemaTriplesFilepath(String value) {
		schemaTriplesFilepath = value;
	}
	
	public void setSummaryType(String value) {
		summaryType = value;
	}

	public void setDataTriplesFilepath(String value) {
		dataTriplesFilepath = value;
	}
	
	public void setSaturateInput(boolean value) {
		saturateInput = value;
	}

	public void setRecomputeSaturation(boolean value) {
		recomputeSaturation = value;
	}
	
	public void setSaturationBatchSize(int value) {
		saturationBatchSize = value;
	}
}
