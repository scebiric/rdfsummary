package fr.inria.oak.RDFSummary.config.beans;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.mapdb.DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;
import fr.inria.oak.RDFSummary.data.dao.TriplesDao;
import fr.inria.oak.RDFSummary.data.dao.TriplesDaoImpl;
import fr.inria.oak.RDFSummary.data.dictionary.Dictionary;
import fr.inria.oak.RDFSummary.data.dictionary.PostgresDictionaryImpl;
import fr.inria.oak.RDFSummary.data.index.BisimulationPostgresIndexImpl;
import fr.inria.oak.RDFSummary.data.index.Index;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManager;
import fr.inria.oak.RDFSummary.data.mapdb.MapDbManagerImpl;
import fr.inria.oak.RDFSummary.data.queries.TriplesQueries;
import fr.inria.oak.RDFSummary.data.queries.TriplesQueriesImpl;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturator;
import fr.inria.oak.RDFSummary.data.saturation.trove.saturator.PostgresSaturatorImpl;
import fr.inria.oak.RDFSummary.messages.MessageBuilder;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDataset;
import fr.inria.oak.RDFSummary.rdf.dataset.bisimulation.PostgresBisimulationRdfDatasetImpl;
import fr.inria.oak.RDFSummary.rdf.loader.PostgresBisimulationCopyRdfLoaderImpl;
import fr.inria.oak.RDFSummary.rdf.loader.RdfLoader;
import fr.inria.oak.RDFSummary.rdf.loader.schema.BracketsSchemaLoaderImpl;
import fr.inria.oak.RDFSummary.rdf.loader.schema.PostgresSchemaLoader;
import fr.inria.oak.RDFSummary.rdf.loader.schema.PostgresSchemaLoaderImpl;
import fr.inria.oak.RDFSummary.rdf.loader.schema.SchemaLoader;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.BisimulationSignature;
import fr.inria.oak.RDFSummary.summary.bisimulation.signature.PostgresTrieBisimulationSignatureImpl;
import fr.inria.oak.RDFSummary.summary.export.BisimulationMapDbRdfExporterImpl;
import fr.inria.oak.RDFSummary.summary.export.PostgresBisimulationDatasetRdfExporterImpl;
import fr.inria.oak.RDFSummary.summary.export.RdfExporter;
import fr.inria.oak.RDFSummary.summary.model.mapdb.MapDbRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.model.mapdb.RdfSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.BisimulationRdfSummary;
import fr.inria.oak.RDFSummary.summary.model.mapdb.bisimulation.MapDbBisimulationRdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.stats.RdfGraphStats;
import fr.inria.oak.RDFSummary.summary.stats.RdfGraphStatsImpl;
import fr.inria.oak.RDFSummary.summary.stats.StatsCollector;
import fr.inria.oak.RDFSummary.summary.stats.bisimulation.BisimulationStats;
import fr.inria.oak.RDFSummary.summary.stats.bisimulation.BisimulationStatsImpl;
import fr.inria.oak.RDFSummary.summary.stats.bisimulation.BisimulationMapDbPostgresStatsCollectorImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationMapDbPostgresSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationMapDbPostgresSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.BisimulationSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.PostgresBisimulationRdfComponentSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.RdfComponentSummarizer;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator.BisimulationGraphCreator;
import fr.inria.oak.RDFSummary.summary.summarizer.components.bisimulation.graphcreator.PostgresBisimulationGraphCreatorImpl;
import fr.inria.oak.RDFSummary.timing.Timings;
import fr.inria.oak.RDFSummary.timing.TroveHashMapTimingsImpl;
import fr.inria.oak.RDFSummary.urigenerator.BracketsUriGeneratorImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfLoaderUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
@ComponentScan({ "fr.inria.oak.RDFSummary.config.beans" })
@Profile(ProfileName.BISIMULATION_MAPDB_POSTGRES)
public class BisimulationMapDbPostgresBeanConfig {

	private static final Logger log = Logger.getLogger(BisimulationMapDbPostgresBeanConfig.class);

	@Autowired
	private Properties props;

	@Autowired
	private BeanConfig config;

	@Autowired
	private PostgresBeanConfig postgresConfig;

	private final MessageBuilder messageBuilder = new MessageBuilder();

	@Bean 
	public DB rdfDatasetDB() throws IOException {	
		final String datasetMapDbFolder = props.getDatasetMapDbFolder();		
		final String datasetDbName = props.getDatasetDbName();
		boolean dropSchema = props.dropSchema();
		if (dropSchema) {
			final File folder = new File(datasetMapDbFolder);
			FileUtils.deleteDirectory(folder);
			log.info(messageBuilder.append("Deleted dataset directory: ").append(datasetMapDbFolder));
		}
		Utils.mkDirs(datasetMapDbFolder);

		return mapDbManagerImpl().getDb(datasetDbName, datasetMapDbFolder);
	}

	@Bean 
	public DB summaryDB() throws IOException {
		final String datasetName = props.getDatasetName();
		final String summaryMapDbFolder = Utils.getMapDbFolder(props.getMapDbFolder(), datasetName, props.getSummaryType());
		final String summaryName = props.getSummaryName();
		final File folder = new File(summaryMapDbFolder);
		FileUtils.deleteDirectory(folder);
		log.info(messageBuilder.append("Deleted summary directory: ").append(summaryMapDbFolder));

		Utils.mkDirs(summaryMapDbFolder);

		return mapDbManagerImpl().createDb(summaryName, summaryMapDbFolder);
	}

	@Bean
	public RdfSummary mapDbRdfSummaryImpl() throws IOException {
		final String dataTriplesSetName = props.getSummaryName().concat("_data_triples");
		final String typeTriplesSetName = props.getSummaryName().concat("_type_triples");
		return new MapDbRdfSummaryImpl(summaryDB(), dataTriplesSetName, typeTriplesSetName);
	}

	@Bean
	public BisimulationGraphCreator postgresBisimulationGraphCreatorImpl() throws NumberFormatException, SQLException, IOException {
		final String sDictionaryDataNodeByHashName = props.getSummaryName().concat("_hashToDictionaryNode");
		
		return new PostgresBisimulationGraphCreatorImpl(summaryDB(), mapDbBisimulationRdfSummaryImpl(), 
				postgresBisimulationRdfDatasetImpl(), bracketsUriGeneratorImpl(), postgresDictionaryImpl(), 
				sDictionaryDataNodeByHashName, config.timerImpl());
	}

	@Bean
	public BisimulationMapDbPostgresSummarizer bisimulationMapDbPostgresSummarizerImpl() throws NumberFormatException, SQLException, DictionaryException, IOException, ConfigurationException, ParametersException {		
		return new BisimulationMapDbPostgresSummarizerImpl(postgresBisimulationCopyRdfLoaderImpl(), 
				bisimulationSummarizerImpl(), mapDbBisimulationRdfSummaryImpl(), bisimulationMapDbRdfExporterImpl(), 
				bisimulationMapDbPostgresStatsCollectorImpl(), mapDbManagerImpl(), troveHashMapTimingsImpl(),
				props.getExportParams(), props.getDatasetName(), props.getSummaryName());
	}

	@Bean
	public BisimulationSignature postgresBisimulationSignatureImpl() throws NumberFormatException, SQLException, IOException {
		return new PostgresTrieBisimulationSignatureImpl(props.getSummaryType(), postgresBisimulationRdfDatasetImpl(), mapDbBisimulationRdfSummaryImpl());
	}

	@Bean
	public BisimulationStats bisimulationStatsImpl() {
		return new BisimulationStatsImpl(bisimulationRdfGraphStatsImpl());
	}

	@Bean
	public Dictionary postgresDictionaryImpl() throws SQLException {
		return new PostgresDictionaryImpl(postgresConfig.dictDaoImpl());
	}

	@Bean
	public Index bisimulationPostgresIndexImpl() {
		return new BisimulationPostgresIndexImpl(props.getSchemaName(), props.getPostgresTableNames());
	}

	@Bean
	public RdfComponentSummarizer postgresBisimulationRdfComponentSummarizerImpl() throws NumberFormatException, SQLException, IOException {
		return new PostgresBisimulationRdfComponentSummarizerImpl(mapDbBisimulationRdfSummaryImpl(), postgresBisimulationRdfDatasetImpl(), 
				triplesDaoImpl(), postgresBisimulationSignatureImpl(), postgresBisimulationGraphCreatorImpl(), 
				bisimulationStatsImpl(), config.timerImpl(), config.timerImpl());
	}

	@Bean
	public PostgresBisimulationRdfDataset postgresBisimulationRdfDatasetImpl() throws NumberFormatException, SQLException, IOException {
		final String classesSetName = props.getDatasetDbName().concat("_classes");
		final String propertiesSetName = props.getDatasetDbName().concat("_properties");
		final String dataNodesSetName = props.getDatasetDbName().concat("_data_nodes");

		return new PostgresBisimulationRdfDatasetImpl(rdfDatasetDB(), classesSetName, propertiesSetName, dataNodesSetName, triplesDaoImpl());
	}

	private PostgresSchemaLoader postgresBracketsSchemaLoaderImpl() throws NumberFormatException, SQLException, IOException {
		return new PostgresSchemaLoaderImpl(bracketsSchemaLoaderImpl(), postgresBisimulationRdfDatasetImpl(), rdfDatasetDB(), 
				triplesDaoImpl(), config.timerImpl(), troveHashMapTimingsImpl());
	}

	@Bean
	public RdfLoader postgresBisimulationCopyRdfLoaderImpl() throws NumberFormatException, SQLException, DictionaryException, IOException, ConfigurationException, ParametersException {
		return new PostgresBisimulationCopyRdfLoaderImpl(props.getDataTriplesFilePath(), props.dropSchema(), props.getSchemaName(), 
				props.getPostgresTableNames(), props.getConnectionParams(), props.getSaturationParams(), 
				postgresBisimulationRdfDatasetImpl(), props.exportInput(), 
				postgresBisimulationDatasetRdfExporterImpl(), props.getExportParams(),
				postgresConfig.ddlDaoImpl(), postgresConfig.dmlDaoImpl(), postgresConfig.dictDaoImpl(), triplesDaoImpl(),
				postgresConfig.postgresCopyRdfParserImpl(), postgresConfig.encodingImpl(), postgresBracketsSchemaLoaderImpl(), postgresSaturatorImpl(), 
				postgresConfig.postgresStorageSplitterImpl(), bisimulationPostgresIndexImpl(), config.timerImpl(), troveHashMapTimingsImpl());
	}

	@Bean
	public RdfLoaderUtils rdfLoaderUtils() throws NumberFormatException, SQLException, IOException {
		return new RdfLoaderUtils(postgresBisimulationRdfDatasetImpl(), postgresDictionaryImpl());
	}

	@Bean
	public SchemaLoader bracketsSchemaLoaderImpl() throws NumberFormatException, SQLException, IOException {
		return new BracketsSchemaLoaderImpl(props.getSchemaTriplesFilepath(), postgresBisimulationRdfDatasetImpl(), 
				postgresDictionaryImpl(), config.uriReconstructingJenaSchemaParserImpl(), config.timerImpl(), troveHashMapTimingsImpl());
	}

	@Bean
	public BisimulationSummarizer bisimulationSummarizerImpl() throws NumberFormatException, SQLException, IOException {
		return new BisimulationSummarizerImpl(props.getSummaryType(), postgresBisimulationRdfComponentSummarizerImpl(), config.timerImpl(), troveHashMapTimingsImpl());
	}

	@Bean
	public TriplesDao triplesDaoImpl() throws NumberFormatException, SQLException {
		return new TriplesDaoImpl(postgresConfig.sqlConnectionHandlerImpl(), props.getFetchSize(), 
				postgresConfig.dmlDaoImpl(), triplesQueriesImpl(), props.getPostgresTableNames(), config.timerImpl());
	}

	@Bean
	public TriplesQueries triplesQueriesImpl() {
		return new TriplesQueriesImpl();
	}

	@Bean
	public BisimulationRdfSummary mapDbBisimulationRdfSummaryImpl() throws IOException {
		final String representationMapName = props.getSummaryName().concat("_s_data_node_by_g_data_node");
		return new MapDbBisimulationRdfSummaryImpl(mapDbRdfSummaryImpl(), summaryDB(), representationMapName);
	}

	@Bean
	public MapDbManager mapDbManagerImpl() {
		return new MapDbManagerImpl();
	}

	@Bean
	public PostgresSaturator postgresSaturatorImpl() throws NumberFormatException, SQLException {
		return new PostgresSaturatorImpl(postgresConfig.saturatorDaoImpl(), postgresConfig.dmlDaoImpl(), postgresConfig.ddlDaoImpl(), config.timerImpl());
	}

	@Bean
	public RdfExporter bisimulationMapDbRdfExporterImpl() throws NumberFormatException, SQLException, IOException {
		return new BisimulationMapDbRdfExporterImpl(props.getSummaryName(), mapDbManagerImpl(), 
				mapDbBisimulationRdfSummaryImpl(), postgresBisimulationRdfDatasetImpl(), 
				postgresDictionaryImpl(), config.dotSummaryVizImpl(), config.triplesWriterImpl(), config.timerImpl());
	}

	@Bean
	public RdfExporter postgresBisimulationDatasetRdfExporterImpl() throws NumberFormatException, SQLException, IOException {
		return new PostgresBisimulationDatasetRdfExporterImpl(props.getDatasetDbName(), mapDbManagerImpl(), 
				postgresBisimulationRdfDatasetImpl(), postgresDictionaryImpl(), 
				config.dotSummaryVizImpl(), config.triplesWriterImpl(), config.timerImpl());
	}

	@Bean
	public RdfGraphStats bisimulationRdfGraphStatsImpl() {
		return new RdfGraphStatsImpl();
	}

	@Bean
	public RdfGraphStats inputRdfGraphStatsImpl() {
		return new RdfGraphStatsImpl();
	}

	@Bean
	public StatsCollector bisimulationMapDbPostgresStatsCollectorImpl() throws NumberFormatException, SQLException, IOException {
		return new BisimulationMapDbPostgresStatsCollectorImpl(inputRdfGraphStatsImpl(), bisimulationStatsImpl(),
				postgresBisimulationRdfDatasetImpl(), mapDbBisimulationRdfSummaryImpl(), troveHashMapTimingsImpl(),
				props.getPostgresTableNames(), postgresConfig.dmlDaoImpl(), props.getOutputFolder(), NameUtils.getStatsFilename(props.getSummaryName()));
	}

	@Bean
	public Timings troveHashMapTimingsImpl() {
		return new TroveHashMapTimingsImpl();
	}

	@Bean 
	public UriGenerator bracketsUriGeneratorImpl() {
		return new BracketsUriGeneratorImpl(props.getSchemaName(), props.getSummaryType());
	}
}
