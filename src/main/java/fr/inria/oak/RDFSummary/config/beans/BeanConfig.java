package fr.inria.oak.RDFSummary.config.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.data.saturation.trove.BatchSaturator;
import fr.inria.oak.RDFSummary.data.saturation.trove.BatchSaturatorImpl;
import fr.inria.oak.RDFSummary.data.saturation.trove.TripleProducer;
import fr.inria.oak.RDFSummary.data.saturation.trove.TroveTripleProducerImpl;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.JenaSchemaParserImpl;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.SchemaParser;
import fr.inria.oak.RDFSummary.rdf.schema.rdffile.UriReconstructingJenaSchemaParserImpl;
import fr.inria.oak.RDFSummary.summary.export.dot.DotSummaryViz;
import fr.inria.oak.RDFSummary.summary.export.dot.DotSummaryVizImpl;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.RDFSummary.util.SchemaUtils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;
import fr.inria.oak.RDFSummary.writer.TriplesWriterImpl;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;

/** 
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
public class BeanConfig {

	@Bean
	public BatchSaturator batchSaturatorImpl() {
		return new BatchSaturatorImpl(troveTripleProducerImpl());
	}
	
	@Bean
	public DotSummaryViz dotSummaryVizImpl() {
		return new DotSummaryVizImpl();
	}
	
	@Bean
	public SchemaParser jenaSchemaParserImpl() {
		return new JenaSchemaParserImpl();
	}

	@Bean
	public SchemaParser uriReconstructingJenaSchemaParserImpl() {
		return new UriReconstructingJenaSchemaParserImpl();
	}

	@Bean
	public SchemaUtils schemaUtils() {
		return new SchemaUtils();
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Timer timerImpl() {
		return new TimerImpl();
	}
	
	@Bean
	public TripleProducer troveTripleProducerImpl() {
		return new TroveTripleProducerImpl();
	}
	
	@Bean
	public TriplesWriter triplesWriterImpl() {
		return new TriplesWriterImpl();
	}
}
