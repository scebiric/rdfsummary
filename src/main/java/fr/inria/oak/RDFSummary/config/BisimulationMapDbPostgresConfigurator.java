package fr.inria.oak.RDFSummary.config;

import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertiesPropertySource;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.beans.BeanConfig;
import fr.inria.oak.RDFSummary.config.beans.BisimulationMapDbPostgresBeanConfig;
import fr.inria.oak.RDFSummary.config.beans.PostgresBeanConfig;
import fr.inria.oak.RDFSummary.config.beans.ProfileName;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.MapDbPostgresSummarizerParams;
import fr.inria.oak.RDFSummary.config.params.loader.MapDbPostgresParameterLoader;
import fr.inria.oak.RDFSummary.config.params.loader.ParametersException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@ComponentScan(value = {"fr.inria.oak.RDFSummary.config"})
public class BisimulationMapDbPostgresConfigurator {

	private final MapDbPostgresParameterLoader mapDbPostgresParamLoader = new MapDbPostgresParameterLoader();
	
	public MapDbPostgresSummarizerParams loadMapDbPostgresSummarizerParameters(String[] args) throws ConfigurationException, ParametersException {
		MapDbPostgresSummarizerParams params = null;
		if (args.length > 0) 
			params = mapDbPostgresParamLoader.loadMapDbPostgresSummarizerParams(args);			
		else 
			params = mapDbPostgresParamLoader.loadMapDbPostgresSummarizerParams(ParametersFilepath.POSTGRES);
		
		ParameterValidator.validateMapDbPostgresSummarizerParams(params);
	
		return params;
	}
	
	public MapDbPostgresExperimentsParams loadExperimentsParameters(String[] args) throws ConfigurationException, ParametersException { 
		MapDbPostgresExperimentsParams mapPostgresExpParams = null;
		if (args.length > 0)
			mapPostgresExpParams = mapDbPostgresParamLoader.loadMapDbPostgresExperimentsParams(args);
		else
			mapPostgresExpParams = mapDbPostgresParamLoader.loadMapDbPostgresExperimentsParams(ParametersFilepath.EXPERIMENTS);
		
		return mapPostgresExpParams;
	}

	public void javaSetUpApplicationContext(AnnotationConfigApplicationContext context, MapDbPostgresSummarizerParams params) throws ConfigurationException {
		Preconditions.checkNotNull(context);
		Preconditions.checkNotNull(params);

		Properties properties = mapDbPostgresParamLoader.loadProperties(params);
		if (properties == null)
			throw new ConfigurationException("Properties cannot be null.");

		registerBeans(context, properties);
	}

	private void registerBeans(AnnotationConfigApplicationContext context, Properties properties) {
		PropertiesPropertySource propertiesSource = new PropertiesPropertySource("props", properties);
		context.getEnvironment().getPropertySources().addLast(propertiesSource);
		context.getEnvironment().setActiveProfiles(ProfileName.BISIMULATION_MAPDB_POSTGRES);
		context.register(BeanConfig.class);
		context.register(PostgresBeanConfig.class);
		context.register(BisimulationMapDbPostgresBeanConfig.class);
		context.refresh();
	}
}
