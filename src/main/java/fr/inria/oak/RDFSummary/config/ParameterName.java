package fr.inria.oak.RDFSummary.config;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParameterName {
	public static final String SUMMARY_TYPE = "SUMMARY_TYPE";
	public static final String OUTPUT_FOLDER = "OUTPUT_FOLDER";
	public static final String POSTGRES_OUTPUT_FOLDER = "POSTGRES_OUTPUT_FOLDER";
	public static final String DOT_EXECUTABLE_FILEPATH = "DOT_EXECUTABLE_FILEPATH";
	public static final String DOT_STYLE_PARAMETERS_FILEPATH = "DOT_STYLE_PARAMETERS_FILEPATH";
	public static final String SATURATION_DOT_PARAMETERS_FILEPATH = "SATURATION_DOT_PARAMETERS_FILEPATH";
	public static final String GENERATE_DOT = "GENERATE_DOT";
	public static final String CONVERT_DOT_TO_PDF = "CONVERT_DOT_TO_PDF";
	public static final String CONVERT_DOT_TO_PNG = "CONVERT_DOT_TO_PNG";
	public static final String DATA_SOURCE_NAME = "DATA_SOURCE_NAME";
	public static final String SERVER_NAME = "SERVER_NAME";
	public static final String PORT = "PORT";
	public static final String DATABASE_NAME = "DATABASE_NAME";
	public static final String USERNAME = "USERNAME";
	public static final String PASSWORD = "PASSWORD";
	public static final String PSQL_SCHEMA_NAME = "PSQL_SCHEMA_NAME";
	public static final String DROP_SCHEMA = "DROP_SCHEMA";
	public static final String DATA_FILEPATH = "DATA_FILEPATH";
	public static final String SCHEMA_FILEPATH = "SCHEMA_FILEPATH";
	public static final String SUMMARY_REPRESENTATION = "SUMMARY_REPRESENTATION";
	public static final String BERKELEY_DB_DIR = "BERKELEY_DB_DIR";
	public static final String INDEXING = "INDEXING";
	public static final String PARTITION = "PARTITION";
	public static final String PARTITION_SIZE = "PARTITION_SIZE";
	public static final String TRIPLES_TABLE = "TRIPLES_TABLE";
	public static final String DICTIONARY_TABLE = "DICTIONARY_TABLE";
	public static final String SUMMARY_DICTIONARY_TABLE = "SUMMARY_DICTIONARY_TABLE";
	public static final String FETCH_SIZE = "FETCH_SIZE";
	public static final String EXPORT_TO_RDF_FILE = "EXPORT_TO_RDF_FILE";
	public static final String EXPORT_STATS_TO_FILE = "EXPORT_STATS_TO_FILE";
	public static final String READ_FROM_FILES = "READ_FROM_FILES";
	public static final String EXPORT_REPRESENTATION_TABLES = "EXPORT_REPRESENTATION_TABLES";
	public static final String NUMBER_OF_RUNS = "NUMBER_OF_RUNS";
	public static final String LOGGING = "LOGGING";	
	public static final String DROP_OUTPUT_TABLES = "DROP_OUTPUT_TABLES";
	public static final String SATURATE_INPUT = "SATURATE_INPUT";
	public static final String SATURATION_BATCH_SIZE = "SATURATION_BATCH_SIZE";
	public static final String RECOMPUTE_SATURATION = "RECOMPUTE_SATURATION";
	public static final String SUMMARY_TYPES = "SUMMARY_TYPES";
	public static final String DATASET_NAME = "DATASET_NAME";
	public static final String MAPDB_FOLDER = "MAPDB_FOLDER";
	public static final String EQUIVALENCE = "EQUIVALENCE";
	public static final String SUMMARY_NAME = "SUMMARY_NAME";
	public static final String PARAMETERS_FILEPATH = "PARAMETERS_FILEPATH";
	public static final String EXPORT_INPUT = "EXPORT_INPUT";
}
