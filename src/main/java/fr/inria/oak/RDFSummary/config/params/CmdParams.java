package fr.inria.oak.RDFSummary.config.params;

import com.beust.jcommander.Parameter;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class CmdParams {

	@Parameter(names = { "-h", "-help"}, help = true)
	public boolean HELP;

	@Parameter(names = { "-config" }, description = "Filepath to the configuration file containing RDFSummary parameters")
	public String CONFIG_FILEPATH;

	@Parameter(names = { "-equiv" }, description = "The RDF node equivalence relation: clique or bisim")
	public String EQUIVALENCE;
	
	@Parameter(names = { "-summary", "-sum" }, description = "The summary type")
	public String SUMMARY_TYPE;

	@Parameter(names = { "-summaries" }, description = "Types of summaries to run in experiments")
	public String SUMMARY_TYPES;
	
	@Parameter(names = { "-o", "-output" }, description = "Output folder")
	public String OUTPUT_FOLDER = "./src/main/resources/data/";

	@Parameter(names = { "-dotexecfilepath", "-dotexecfp" }, description = "Filepath to DOT executable")
	public String DOT_EXECUTABLE_FILEPATH;

	@Parameter(names = { "-dotparams" }, description = "Filepath to DOT configuration properties file")
	public String DOT_STYLE_PARAMETERS_FILEPATH;

	@Parameter(names = { "-gendot" }, arity = 1, description = "Export to a .dot file (true/false); default true")
	public boolean GENERATE_DOT = true;

	@Parameter(names = { "-dottopdf" }, arity = 1, description = "Convert DOT to PDF and open the PDF (true/false); default false")
	public boolean CONVERT_DOT_TO_PDF = false;

	@Parameter(names = { "-dottopng" }, arity = 1, description = "Convert DOT to PNG and open the PNG (true/false); default false")
	public boolean CONVERT_DOT_TO_PNG = false;
	
	@Parameter(names = { "-satdotparams" }, description = "Filepath to DOT configuration properties file for saturated graphs")
	public String SATURATION_DOT_PARAMETERS_FILEPATH;

	// DataSource parameters

	@Parameter(names = { "-datasource" }, description = "Data source name (virtuoso or postgres)")
	public String DATA_SOURCE_NAME;

	@Parameter(names = { "-s", "-server" }, description = "Server name")
	public String SERVER_NAME;

	@Parameter(names = { "-p", "-port" }, description = "Port")
	public int PORT;

	@Parameter(names = { "-db" }, description = "Database name")
	public String DATABASE_NAME;

	@Parameter(names = { "-u", "-user" }, description = "Username")
	public String USERNAME;

	@Parameter(names = { "-pass" }, description = "Password")
	public String PASSWORD;

	// Postgres parameters

	@Parameter(names = { "-psqlschema" }, description = "PostgreSQL schema name")
	public String PSQL_SCHEMA_NAME;

	@Parameter(names = { "-dropschema" }, arity = 1, description = "Drops the Postgres schema (true/false); default false")
	public boolean DROP_SCHEMA = false;

	@Parameter(names = { "-data" }, description = "Filepath to the file containing data triples")
	public String DATA_FILEPATH;

	@Parameter(names = { "-rdfschema" }, description = "Filepath to the file containing RDF Schema triples")
	public String RDFS_FILEPATH;

	@Parameter(names = { "-indexing" }, description = "Index the input triples table and the partitioned tables (true/false); default false")
	public boolean INDEXING = false;

	@Parameter(names = { "-fetch" }, description = "Fetch size for JDBC statements; default 50000")
	public int FETCH_SIZE = 50000;

	// Other parameters
	@Parameter(names = { "-rdfexport" }, arity = 1, description = "Export to a file in an RDF format (true/false); default false")
	public boolean EXPORT_TO_RDF_FILE = false;

	@Parameter(names = { "-exportstats" }, arity = 1, description = "Export statistics to a file (true/false); default false")
	public boolean EXPORT_STATS_TO_FILE = false;	

	@Parameter(names = { "-exprepr" }, arity = 1, description = "Export representation tables to Postgres (true/false); default false.")
	public boolean EXPORT_REPRESENTATION_TABLES = false;

	@Parameter(names = { "-runs" }, description = "The number of runs for the summarization experiments; default 5")
	public int NUMBER_OF_RUNS = 5;
	
	@Parameter(names = { "-log" }, arity = 1, description = "Logging (true/false); default false.")
	public boolean LOGGING = false;
	
	@Parameter(names = { "-satbatch" }, arity = 1, description = "Saturation batch size; default 100")
	public int SATURATION_BATCH_SIZE = 100;
	
	@Parameter(names = { "-satinput" }, arity = 1, description = "Saturate input before summarizing it (true/false); default false.")
	public boolean SATURATE_INPUT = false;
	
	@Parameter(names = { "-resat" }, arity = 1, description = "Recompute saturation (true/false); default false.")
	public boolean RECOMPUTE_SATURATION = false;
	
	@Parameter(names = { "-datasetname" }, description = "Dataset name")
	public String DATASET_NAME;
	
	@Parameter(names = { "-mapdbfolder" }, description = "Filepath to a folder for MapDb files")
	public String MAPDB_FOLDER;

	@Parameter(names = { "-expinput" }, arity = 1, description = "Export input graph to RDF and DOT (true/false); default false.")
	public boolean EXPORT_INPUT = false;
}
