package fr.inria.oak.RDFSummary.config.params;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresSummarizationParams extends SummarizationParams {

	private boolean dropSchema = false;
	private boolean indexing = false;
	private String psqlSchemaName;
	private int fetchSize;

	/**
	 * @param equivalence
	 * @param summaryType
	 * @param psqlSchemaName
	 * @param dataTriplesFilepath
	 */
	public PostgresSummarizationParams(final String equivalence, final String summaryType, final String psqlSchemaName, final String dataTriplesFilepath) {
		super(equivalence, summaryType, dataTriplesFilepath);
		this.psqlSchemaName = StringUtils.toLower(psqlSchemaName);
	}

	public boolean dropSchema() {
		return dropSchema;
	}

	public boolean indexing() {
		return indexing;
	}

	public String getSchemaName() {
		return psqlSchemaName;
	}

	public String getTriplesTable() {
		return Constant.TRIPLES_TABLE_NAME;
	}

	public String getDictionaryTable() {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(psqlSchemaName));
		
		return NameUtils.getDictionaryTableName(psqlSchemaName);
	}

	public int getFetchSize() {
		return fetchSize;
	}
	
	public void setIndexing(boolean value) {
		indexing = value;
	}

	public void setDropSchema(boolean value) {
		dropSchema = value;
	}

	public void setFetchSize(int value) {
		fetchSize = value;
	}

	public void setSchemaName(String value) {
		psqlSchemaName = value;
	}
}
