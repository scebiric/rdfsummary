package fr.inria.oak.RDFSummary.config;

import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.params.CommandLineParams;
import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.ExperimentsParams;
import fr.inria.oak.RDFSummary.config.params.ExportParams;
import fr.inria.oak.RDFSummary.config.params.SummarizationParams;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.DataLoader;
import fr.inria.oak.RDFSummary.constants.DataSourceName;
import fr.inria.oak.RDFSummary.constants.ParameterName;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * Loads parameters from a configuration file or the command line
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ParameterLoader {

	private static final Logger log = Logger.getLogger(ParameterLoader.class);
	private static JCommander jc;

	/**
	 * Loading {@link SummarizerParams} from the external configuration file.
	 * @param configFilepath 
	 * @return {@link SummarizerParams} instance
	 * @throws ConfigurationException 
	 */
	public SummarizerParams loadSummarizerParamsFromConfigFile(String configFilepath) throws ConfigurationException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading summarizer parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {
			ConnectionParams connParams = this.loadConnectionParams(config);
			SummarizationParams summParams = this.loadSummarizationParams(config);
			ExportParams exportParams = this.loadExportParams(config);
			log.info("Parameters loaded from the configuration file.");

			return new SummarizerParams(connParams, summParams, exportParams);
		}
		else 
			throw new ParameterException("The properties file is empty.");
	} 

	/**
	 * Loading {@link ExperimentsParams} from the external configuration file.
	 * @param configFilepath 
	 * @return {@link ExperimentsParams} instance
	 * @throws ConfigurationException 
	 */
	public ExperimentsParams loadExperimentsParamsFromConfigFile(String configFilepath) throws ConfigurationException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(configFilepath));

		log.info("Loading experiments parameters from " + configFilepath + "...");
		Configuration config = new PropertiesConfiguration(configFilepath);
		if (!config.isEmpty()) {	
			SummarizerParams summarizerParams = new SummarizerParams(this.loadConnectionParams(config), 
					this.loadSummarizationParams(config), this.loadExportParams(config));
			int numberOfRuns = config.getInt(ParameterName.NUMBER_OF_RUNS);
			boolean logging = config.getBoolean(ParameterName.LOGGING, false);
			ExperimentsParams expParams = new ExperimentsParams(summarizerParams, numberOfRuns);
			expParams.setLogging(logging);
			log.info("Parameters loaded from the configuration file.");

			return expParams;
		}
		else 
			throw new ParameterException("The properties file is empty.");
	}

	/**
	 * @param config
	 * @return {@link ConnectionParams}
	 */
	private ConnectionParams loadConnectionParams(final Configuration config) {		
		return new ConnectionParams(config.getString(ParameterName.DATA_SOURCE_NAME), 
				config.getString(ParameterName.SERVER_NAME), config.getInteger(ParameterName.PORT, null), 
				config.getString(ParameterName.DATABASE_NAME), 
				config.getString(ParameterName.USERNAME), config.getString(ParameterName.PASSWORD));
	}

	/**
	 * @param config
	 * @return {@link SummarizationParams}
	 */
	private SummarizationParams loadSummarizationParams(final Configuration config) {
		SummarizationParams summParams = new SummarizationParams(config.getString(ParameterName.SUMMARY_TYPE), 
				config.getString(ParameterName.PSQL_SCHEMA_NAME), config.getString(ParameterName.DATA_FILEPATH), 
				config.getString(ParameterName.DATA_LOADER), config.getString(ParameterName.BERKELEY_DB_DIR));

		summParams.setSchemaTriplesFilepath(config.getString(ParameterName.SCHEMA_FILEPATH));
		summParams.setTriplesTable(config.getString(ParameterName.TRIPLES_TABLE));
		summParams.setDictionaryTableName(config.getString(ParameterName.DICTIONARY_TABLE));
		summParams.setDropSchema(config.getBoolean(ParameterName.DROP_SCHEMA, false));
		summParams.setIndexing(config.getBoolean(ParameterName.INDEXING, false));		
		summParams.setFetchSize(config.getInt(ParameterName.FETCH_SIZE, 50000));
		summParams.setSaturateInput(config.getBoolean(ParameterName.SATURATE_INPUT, false));
		summParams.setSaturationBatchSize(config.getInt(ParameterName.SATURATION_BATCH_SIZE, 100));
		
		return summParams;
	}

	/**
	 * @param config
	 * @return {@link ExportParams}
	 */
	private ExportParams loadExportParams(final Configuration config) {
		ExportParams exportParams = new ExportParams(config.getString(ParameterName.OUTPUT_FOLDER));

		exportParams.setGenerateDot(config.getBoolean(ParameterName.GENERATE_DOT, false));
		exportParams.setConvertDotToPdf(config.getBoolean(ParameterName.CONVERT_DOT_TO_PDF, false));
		exportParams.setConvertDotToPng(config.getBoolean(ParameterName.CONVERT_DOT_TO_PNG, false));
		exportParams.setExportToRdfFile(config.getBoolean(ParameterName.EXPORT_TO_RDF_FILE, false));
		exportParams.setExportStatsToFile(config.getBoolean(ParameterName.EXPORT_STATS_TO_FILE, false));
		exportParams.setExportRepresentationTables(config.getBoolean(ParameterName.EXPORT_REPRESENTATION_TABLES, false));
		exportParams.setDotExecutableFilepath(config.getString(ParameterName.DOT_EXECUTABLE_FILEPATH));
		exportParams.setDotStyleParamsFilepath(config.getString(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH));

		return exportParams;
	}

	/**
	 * Reads the specified command line parameters into the SummarizerParams class
	 * @param args
	 * @return {@link SummarizerParams}
	 * @throws ConfigurationException 
	 */
	public SummarizerParams loadSummarizerParamsFromCommandLine(String[] args) throws ConfigurationException {
		log.info("Loading command line parameters...");
		CommandLineParams cmdParams = new CommandLineParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return this.loadSummarizerParamsFromConfigFile(cmdParams.CONFIG_FILEPATH);
		}
		else 
			return this.loadSummarizerParamsFromCommandLine(cmdParams);
	}

	/**
	 * Reads the specified command line parameters into the ExperimentsParams class
	 * @param args
	 * @return {@link ExperimentsParams}
	 * @throws ConfigurationException 
	 */
	public ExperimentsParams loadExperimentsParamsFromCommandLine(String[] args) throws ConfigurationException {
		log.info("Loading command line parameters...");
		CommandLineParams cmdParams = new CommandLineParams();
		jc = new JCommander(cmdParams);		
		jc.parse(args);

		if (cmdParams.HELP) {
			displayUsage();
			System.exit(0);
		}

		if (!StringUtils.isNullOrBlank(cmdParams.CONFIG_FILEPATH)) {
			if (!cmdParams.CONFIG_FILEPATH.endsWith("/"))
				cmdParams.CONFIG_FILEPATH = cmdParams.CONFIG_FILEPATH.concat("/");

			return this.loadExperimentsParamsFromConfigFile(cmdParams.CONFIG_FILEPATH);
		}
		else {
			ExperimentsParams expParams = new ExperimentsParams(this.loadSummarizerParamsFromCommandLine(cmdParams), cmdParams.NUMBER_OF_RUNS);
			expParams.setLogging(cmdParams.LOGGING);
			log.info("Experiments parameters loaded from the command line.");

			return expParams;
		}
	}

	/**
	 * Loads {@link SummarizerParams} from {@link CommandLineParams} 
	 * @param cmdParams
	 * @return {@link SummarizerParams} instance
	 */
	private SummarizerParams loadSummarizerParamsFromCommandLine(final CommandLineParams cmdParams) {
		ConnectionParams connParams = new ConnectionParams(cmdParams.DATA_SOURCE_NAME, cmdParams.SERVER_NAME, 
				cmdParams.PORT, cmdParams.DATABASE_NAME, cmdParams.USERNAME, cmdParams.PASSWORD);

		SummarizationParams summParams = new SummarizationParams(cmdParams.SUMMARY_TYPE, cmdParams.DATASET_NAME, 
				cmdParams.DATA_FILEPATH, cmdParams.DATA_LOADER, cmdParams.BERKELEY_DB_DIR);
		summParams.setDropSchema(cmdParams.DROP_SCHEMA);
		summParams.setSchemaTriplesFilepath(cmdParams.RDFS_FILEPATH);
		summParams.setIndexing(cmdParams.INDEXING);		
		summParams.setTriplesTable(cmdParams.TRIPLES_TABLE);
		summParams.setDictionaryTableName(cmdParams.DATASET_DICTIONARY_TABLE);
		summParams.setFetchSize(cmdParams.FETCH_SIZE);
		summParams.setSaturateInput(cmdParams.SATURATE_INPUT);
		summParams.setSaturationBatchSize(cmdParams.SATURATION_BATCH_SIZE);

		ExportParams exportParams = new ExportParams(cmdParams.OUTPUT_FOLDER);
		exportParams.setGenerateDot(cmdParams.GENERATE_DOT);
		exportParams.setConvertDotToPdf(cmdParams.CONVERT_DOT_TO_PDF);
		exportParams.setConvertDotToPng(cmdParams.CONVERT_DOT_TO_PNG);
		exportParams.setExportToRdfFile(cmdParams.EXPORT_TO_RDF_FILE);
		exportParams.setExportStatsToFile(cmdParams.EXPORT_STATS_TO_FILE);
		exportParams.setExportRepresentationTables(cmdParams.EXPORT_REPRESENTATION_TABLES);
		exportParams.setDotExecutableFilepath(cmdParams.DOT_EXECUTABLE_FILEPATH); 
		exportParams.setDotStyleParamsFilepath(cmdParams.DOT_STYLE_PARAMETERS_FILEPATH);

		log.info("Summarizer parameters loaded from the command line.");
		return new SummarizerParams(connParams, summParams, exportParams);
	}

	private static void displayUsage() {
		if (jc != null) {
			jc.usage();
		}
	}

	/**
	 * Validates the parameters loaded to the Parameters class
	 * @param params 
	 */
	public void validateSummarizerParameters(final SummarizerParams params) {
		Preconditions.checkNotNull(params);
		log.info("Validating parameters...");

		if (StringUtils.isNullOrBlank(params.getSummarizationParams().getSummaryType()))
			throw new ParameterException(ParameterName.SUMMARY_TYPE + " is required.");

		if (!params.getSummarizationParams().getSummaryType().equals(SummaryType.WEAK) && !params.getSummarizationParams().getSummaryType().equals(SummaryType.TYPED_WEAK) && 
				!params.getSummarizationParams().getSummaryType().equals(SummaryType.STRONG) && !params.getSummarizationParams().getSummaryType().equals(SummaryType.TYPED_STRONG))
			throw new ParameterException("Invalid " + ParameterName.SUMMARY_TYPE + ": " + params.getSummarizationParams().getSummaryType() + 
					". Supported: " + SummaryType.WEAK + ", " + SummaryType.TYPED_WEAK + ", " + 
					SummaryType.STRONG + ", " + SummaryType.TYPED_STRONG);

		if (StringUtils.isNullOrBlank(params.getConnectionParams().getDataSourceName()))
			throw new ParameterException(ParameterName.DATA_SOURCE_NAME + " is required. Supported: " + DataSourceName.POSTGRES);

		if (!params.getConnectionParams().getDataSourceName().equals(DataSourceName.POSTGRES))
			throw new ParameterException("Invalid " + ParameterName.DATA_SOURCE_NAME + ". Supported: " + DataSourceName.POSTGRES);

		if (params.getConnectionParams().getDataSourceName().equals(DataSourceName.POSTGRES)) {
			if (StringUtils.isNullOrBlank(params.getConnectionParams().getServerName()))
				throw new ParameterException(ParameterName.SERVER_NAME + " is required.");

			if (StringUtils.isNullOrBlank(params.getConnectionParams().getUsername()))
				throw new ParameterException("Database " + ParameterName.USERNAME + " is required.");

			if (StringUtils.isNullOrBlank(params.getConnectionParams().getPassword()))
				throw new ParameterException("Database " + ParameterName.PASSWORD + " is required.");


			if (StringUtils.isNullOrBlank(params.getConnectionParams().getDatabaseName()))
				throw new ParameterException(ParameterName.DATABASE_NAME + " is required.");

			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getSchemaName()))
				throw new ParameterException(ParameterName.PSQL_SCHEMA_NAME + " (schema name) is required.");

			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getTriplesTable()))
				throw new ParameterException(ParameterName.TRIPLES_TABLE + " indicating the table to store input graph triples) is required.");				

			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getDictionaryTable()))
				throw new ParameterException(ParameterName.DICTIONARY_TABLE + " indicating the Postgres dictionary table for the input dataset is required.");

			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getDataLoader()))
				throw new ParameterException(ParameterName.DATA_LOADER + " is required. Supported: " + DataLoader.PSQL_COPY + ", " + DataLoader.JENA);
			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getDataTriplesFilepath()))
				throw new ParameterException(ParameterName.DATA_FILEPATH + " is required");
			if (StringUtils.isNullOrBlank(params.getSummarizationParams().getSchemaTriplesFilepath()) && 
					params.getSummarizationParams().saturateInput())
				throw new ParameterException("Since there is no schema specified, " + ParameterName.SATURATE_INPUT + " must be set to false.");
		}

		if (StringUtils.isNullOrBlank(params.getExportParams().getOutputFolder()))
			throw new ParameterException(ParameterName.OUTPUT_FOLDER + " is required.");

		if (StringUtils.isNullOrBlank(params.getSummarizationParams().getBerkeleyDbDirectory()))
			throw new ParameterException(ParameterName.BERKELEY_DB_DIR + " is required.");
		
		if (params.getExportParams().generateDot()) {
			if (StringUtils.isNullOrBlank(params.getExportParams().getDotStyleParamsFilepath()))
				throw new ParameterException(ParameterName.DOT_STYLE_PARAMETERS_FILEPATH + " is required.");
		}

		if (params.getExportParams().convertDotToPdf() || params.getExportParams().convertDotToPng()) {
			if (StringUtils.isNullOrBlank(params.getExportParams().getDotExecutableFilepath()))
				throw new ParameterException(ParameterName.DOT_EXECUTABLE_FILEPATH + " is required.");
		}

		log.info("Parameters validated.");
	}

	/**
	 * Loads those parameters to properties which are needed to inject beans when setting up the application context
	 * 
	 * @param summParams
	 * @return The {@link Properties} object
	 */
	public Properties loadPropertiesFromSummarizerParams(SummarizerParams summParams) {
		Preconditions.checkNotNull(summParams);
		Properties properties = new Properties();
		properties.put(ParameterName.SUMMARY_TYPE, summParams.getSummarizationParams().getSummaryType());
		properties.put(ParameterName.OUTPUT_FOLDER, summParams.getExportParams().getOutputFolder());
		properties.put(ParameterName.DATA_SOURCE_NAME, summParams.getConnectionParams().getDataSourceName());
		properties.put(ParameterName.SERVER_NAME, summParams.getConnectionParams().getServerName());
		properties.put(ParameterName.PORT, summParams.getConnectionParams().getPort());
		properties.put(ParameterName.DATABASE_NAME, summParams.getConnectionParams().getDatabaseName());
		properties.put(ParameterName.USERNAME, summParams.getConnectionParams().getUsername());
		properties.put(ParameterName.PASSWORD, summParams.getConnectionParams().getPassword());
		properties.put(ParameterName.PSQL_SCHEMA_NAME, summParams.getSummarizationParams().getSchemaName());
		properties.put(ParameterName.DATA_LOADER, summParams.getSummarizationParams().getDataLoader());
		properties.put(ParameterName.TRIPLES_TABLE, summParams.getSummarizationParams().getTriplesTable());
		properties.put(ParameterName.DICTIONARY_TABLE, summParams.getSummarizationParams().getDictionaryTable());
		properties.put(ParameterName.DROP_SCHEMA, summParams.getSummarizationParams().dropSchema());
		properties.put(ParameterName.FETCH_SIZE, summParams.getSummarizationParams().getFetchSize());
		properties.put(ParameterName.BERKELEY_DB_DIR, summParams.getSummarizationParams().getBerkeleyDbDirectory());

		return properties;
	}

	/**
	 * Loads those parameters to properties which are needed to inject beans when setting up the application context
	 * 
	 * @param expParams
	 * @return The {@link Properties} object
	 */
	public Properties loadPropertiesFromExperimentsParams(ExperimentsParams expParams) {
		return this.loadPropertiesFromSummarizerParams(expParams.getSummarizerParams());
	}

	/**
	 * @param filepath
	 * @return The dot parameters loaded from a configuration file at the specified filepath
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 */
	public DotStyleParams load(final String filepath) throws ParametersException, ConfigurationException {
		log.info("Loading dot configuration parameters from: " + filepath);
		DotStyleParams dotParams = null;
		Configuration config = new PropertiesConfiguration(filepath);
		if (!config.isEmpty()) {
			boolean showGraphLabel = config.getBoolean("SHOW_GRAPH_LABEL", true);
			String classNodeShape = config.getString("CLASS_NODE_SHAPE");
			String classNodeStyle = config.getString("CLASS_NODE_STYLE");
			String classNodeColor = config.getString("CLASS_NODE_COLOR");
			String dataNodeShape = config.getString("DATA_NODE_SHAPE");
			String dataNodeStyle = config.getString("DATA_NODE_STYLE");
			String dataNodeColor = config.getString("DATA_NODE_COLOR");
			boolean descriptiveDataNodeLabel = config.getBoolean("DESCRIPTIVE_DATA_NODE_LABEL", false);
			String propertyNodeShape = config.getString("PROPERTY_NODE_SHAPE");
			String propertyNodeStyle = config.getString("PROPERTY_NODE_STYLE");
			String propertyNodeColor = config.getString("PROPERTY_NODE_COLOR");
			String dataPropertyEdgeStyle = config.getString("DATA_PROPERTY_EDGE_STYLE");
			String dataPropertyEdgeColor = config.getString("DATA_PROPERTY_EDGE_COLOR");
			String typeEdgeStyle = config.getString("TYPE_EDGE_STYLE");
			String typeEdgeColor = config.getString("TYPE_EDGE_COLOR");
			String schemaEdgeColor = config.getString("SCHEMA_EDGE_COLOR");
			String schemaLabelColor = config.getString("SCHEMA_LABEL_COLOR");

			dotParams = new DotStyleParams(showGraphLabel, classNodeShape, classNodeStyle, classNodeColor, 
					dataNodeShape, dataNodeStyle, dataNodeColor, descriptiveDataNodeLabel, 
					propertyNodeShape, propertyNodeStyle, propertyNodeColor, 
					dataPropertyEdgeStyle, dataPropertyEdgeColor, typeEdgeStyle, typeEdgeColor, schemaEdgeColor, schemaLabelColor);
		}
		else
			throw new ParametersException("The dot configuration parameters file is empty.");

		return dotParams;
	}
}
