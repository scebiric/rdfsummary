package fr.inria.oak.RDFSummary.util;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class NameUtils {

	private static StringBuilder StrBuilder;
	
	/**
	 * Adds the schema prefix to a table name
	 * 
	 * @param schemaName
	 * @param tableName
	 * @return The qualified table name
	 */
	public static String qualify(String schemaName, String tableName) {
		final StringBuilder StrBuilder = new StringBuilder(schemaName);
		StrBuilder.append(Chars.DOT).append(tableName);
		
		return StrBuilder.toString();
	}

	/**
	 * Generates an index name by removing the schema prefix from the specified table name
	 * and adding a suffix with the column name and "idx".
	 *
	 * @param table
	 * @param columns
	 * @return The generated index name
	 */
	public static String getIndexName(final String table, final String columns) {		
		int index = table.indexOf(Chars.DOT);
		String str = table;
		if (index != -1) 
			str = table.substring(index+1);		

		StrBuilder = new StringBuilder(str);
		StrBuilder.append(Chars.UNDERSCORE).append("i").append(Chars.UNDERSCORE).append(columns);
		
		return StrBuilder.toString();
	}

	/**
	 * Retrieves the table name from the specified qualified name by removing the schema prefix. 
	 * 
	 * @param qualifiedTableName
	 * @return The table name without the schema prefix
	 */
	public static String getTableName(final String qualifiedTableName) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT)));
		Preconditions.checkArgument(!qualifiedTableName.contains(Character.toString(Chars.SPACE)));
		Preconditions.checkArgument(qualifiedTableName.length() >= 3);
		
		int index = qualifiedTableName.indexOf(Chars.DOT);
		return qualifiedTableName.substring(index+1);		
	}

	/**
	 * Retrieves the Postgres schema name from the qualified table name
	 * 
	 * @param qualifiedTableName
	 * @return The schema prefix
	 */
	public static String getSchemaName(final String qualifiedTableName) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(qualifiedTableName));
		Preconditions.checkArgument(qualifiedTableName.contains(Character.toString(Chars.DOT)));
		Preconditions.checkArgument(!qualifiedTableName.contains(Character.toString(Chars.SPACE)));
		Preconditions.checkArgument(qualifiedTableName.length() >= 3);
		
		int index = qualifiedTableName.indexOf(Chars.DOT);		
		return qualifiedTableName.substring(0, index);
	}

	/**
	 * Prefixes the storage layout filename with the dataset name
	 * 
	 * @param datasetName
	 * @return The output filename for the storage layout
	 */
	public static String getStorageLayoutFilename(String datasetName) {
		return qualify(datasetName, "storage-layout");
	}

	/**
	 * Prefixes the index layout filename with the dataset name
	 * 
	 * @param datasetName
	 * @return The output filename for the index layout
	 */
	public static String getIndexLayoutFilename(String datasetName) {
		return qualify(datasetName, "index-layout");
	}

	/**
	 * Generates a table name by adding the saturated suffix to the input table name,
	 * and qualifying the resulting string with the schema name.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The table name for saturated encoded triples
	 */
	public static String getSaturatedTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.SATURATED);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the schema name
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated triples table name
	 */
	public static String getTriplesTableName(String schemaName, String inputTableName) {
		return qualify(schemaName, inputTableName);
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the schema suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated schema table name
	 */
	public static String getSchemaTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.SCHEMA);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded schema suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated encoded schema table name
	 */
	public static String getEncodedSchemaTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED_SCHEMA);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded triples suffix.
	 * 
	 * @param schemaName
	 * @param tableName
	 * @return The generated encoded triples table name
	 */
	public static String getEncodedTriplesTableName(String schemaName, String tableName) {
		StrBuilder = new StringBuilder(tableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded types suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated encoded types table name
	 */
	public static String getEncodedTypesTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED_TYPES);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the encoded data suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated encoded data table name
	 */
	public static String getEncodedDataTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED_DATA);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	public static String getSaturatedEncodedDataTableName(String saturatedEncodedTriplesTable) {
		StrBuilder = new StringBuilder(saturatedEncodedTriplesTable);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED_DATA);
		
		return StrBuilder.toString();
	}

	public static String getSaturatedEncodedTypesTableName(String saturatedEncodedTriplesTable) {
		StrBuilder = new StringBuilder(saturatedEncodedTriplesTable);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.ENCODED_TYPES);
		
		return StrBuilder.toString();
	}
	
	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the data suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated data table name
	 */
	public static String getDataTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE);
		StrBuilder.append(Constants.DATA);
		
		return qualify(schemaName, StrBuilder.toString());
	}

	/**
	 * Generates a table name by prefixing the input table name with the PostgreSQL schema name (dataset name) 
	 * and adding the types suffix.
	 * 
	 * @param schemaName
	 * @param inputTableName
	 * @return The generated types table name
	 */
	public static String getTypesTableName(String schemaName, String inputTableName) {
		StrBuilder = new StringBuilder(inputTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.TYPES);
		
		return qualify(schemaName, StrBuilder.toString());
	}
	
	public static String getClassesTableName(String postgresSchemaName) {
		return NameUtils.qualify(postgresSchemaName, "cls");
	}

	public static String getStatsFilename(String outputPrefix) {
		StrBuilder = new StringBuilder(outputPrefix);
		StrBuilder.append(Chars.UNDERSCORE).append("stats");
		
		return StrBuilder.toString();
	}

	public static String getSaturatedEncodedTriplesTableName(final String encodedTriplesTableName) {
		StrBuilder = new StringBuilder(encodedTriplesTableName);
		StrBuilder.append(Chars.UNDERSCORE).append(Constants.SATURATED);
		
		return StrBuilder.toString();
	}

	public static String getOutputPrefix(final String encodedTriplesTable, final String summaryType) {
		StrBuilder = new StringBuilder(encodedTriplesTable);
		StrBuilder.append(Chars.UNDERSCORE).append(summaryType);
		
		return StrBuilder.toString();
	}
}
