package fr.inria.oak.RDFSummary.data.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

import fr.inria.oak.RDFSummary.data.ResultSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface DmlDao {
	
	/**
	 * Copies the content from the input stream to the specified Postgres table
	 * 
	 * @param tableName
	 * @param inputStream
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public void copy(String tableName, InputStream inputStream) throws SQLException, IOException;

	/** Retrieves the number of rows in the table 
	 * @throws SQLException */
	public int getRowCount(String tableName) throws SQLException;
	
	/**
	 * @param columns
	 * @param distinct
	 * @param fromTable
	 * @param orderByColumn
	 * @return The result set of the select query
	 * @throws SQLException
	 */
	public ResultSet getSelectionResultSet(String[] columns, boolean distinct, String fromTable, String orderByColumn) throws SQLException;
	
	/**
	 * @param columns
	 * @param distinct
	 * @param fromTable
	 * @param orderByColumn
	 * @param performanceLabel
	 * @return The result set of the select query
	 * @throws SQLException
	 */
	public ResultSet getSelectionResultSet(String[] columns, boolean distinct, String fromTable, String orderByColumn, String performanceLabel) throws SQLException;
	
	/**
	 * Executes the query and returns the result set.
	 * 
	 * @param query
	 * @return {@link ResultSet}
	 * @throws SQLException 
	 */
	public ResultSet getResultSet(String query) throws SQLException;
	
	/** Inserts values to the specified table 
	 * @param <T>
	 * @throws SQLException */
	public <T> void insert(String tableName, String[] columnNames, List<T> values) throws SQLException;
	
	/**
	 * Inserts values to the specified table
	 * 
	 * @param summaryEncodedTypesTable
	 * @param typesColumns
	 * @param values
	 * @throws SQLException 
	 */
	public void insert(String tableName, String[] columnNames, int[] values) throws SQLException;
	
	/** Checks if the specified schema exists in Postgres database 
	 * @throws SQLException */
	public boolean existsSchema(String postgresSchemaName) throws SQLException;
	
	/** Checks if the specified table exists in the schema 
	 * @throws SQLException */
	public boolean existsTable(String qualifiedTableName) throws SQLException;
	
	/** Checks if the specified index exists in the database 
	 * @throws SQLException */
	public boolean existsIndex(String indexName, String schemaName) throws SQLException;
	
	/**
	 * @param schemaName
	 * @param uniqueTablePrefix
	 * @return True if there exist any tables in the specified schema having the specified prefix in their name; otherwise false.
	 * @throws SQLException 
	 */
	public boolean existTablesWithPrefix(String schemaName, String tablePrefix) throws SQLException;
	
	/** Checks if the specified table is clustered on the specified index 
	 * @throws SQLException */
	public boolean isClusteredUsingIndex(String qualifiedTableName, String indexName) throws SQLException;
	
	/** Retrieves the name of the index used for clustering the specified table */
	public String getClusterIndex(String qualifiedTableNames, int fetchSize) throws SQLException;
	
	/** Retrieves tables in the specified schema whose name starts with the specified prefix */
	public List<String> getTablesByPrefix(String schemaName, String prefix, int fetchSize) throws SQLException;
	
	/** Gets schemas which contain the specified string in their name 
	 * @throws SQLException */
	public List<String> getSchemasContainingString(String str) throws SQLException;
	
	/**
	 * @param table
	 * @param column
	 * @return The sequence name of the column for the specified table
	 * @throws SQLException 
	 */
	public String getSequenceName(String table, String column) throws SQLException;

	/**
	 * @param tableName
	 * @return The number of distinct values in the specified column of the specified table
	 * @throws SQLException 
	 * @throws QueryException 
	 */
	public int getDistinctColumnValuesCount(String tableName, String columnName) throws SQLException, QueryException;
	
	/**
	 * @param table
	 * @param id
	 * @return The ResultSet with all the columns from the specified table for the row with the given id.
	 * @throws SQLException
	 */
	public ResultSet selectAllById(String table, int id) throws SQLException;
	
	/**
	 * @param table
	 * @param column
	 * @param value
	 * @return True if there exists a row in the specified table having the given value in the specified column.
	 * @throws SQLException 
	 */
	public boolean existsValueInColumn(String table, String column, String value) throws SQLException;
}
