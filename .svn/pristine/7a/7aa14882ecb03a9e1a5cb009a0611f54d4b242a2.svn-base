package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.main.Summarizer;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.TroveSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongTypeEquivalenceSummaryTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String TYPED_STRONG_SUMMARY = SummaryType.TYPED_STRONG;
	private static final String TYPED_STRONG_DATASETS_DIR = "/typstrong/";

	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
	}

	/**
	 * Resources from G have the same source AND target clique as their representative data node in the summary.
	 */
	@Test
	public void summarizeExample2() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(TYPED_STRONG_SUMMARY, TYPED_STRONG_DATASETS_DIR.concat("example2.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongTypeEquivalenceSummary summary = (StrongTypeEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		int o4Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o2Rep, o4Rep);
		assertNotEquals(o1Rep, o2Rep);
		assertNotEquals(o1Rep, o3Rep);
		assertNotEquals(o1Rep, o4Rep);
		assertNotEquals(o2Rep, o3Rep);
		assertNotEquals(o3Rep, o4Rep);
		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);

		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());		
		assertEquals(6, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
	}

	@Test
	public void summarizeExample3() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(TYPED_STRONG_SUMMARY, TYPED_STRONG_DATASETS_DIR.concat("example3.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongTypeEquivalenceSummary summary = (StrongTypeEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertNotEquals(o1Rep, o2Rep);
		assertNotEquals(o1Rep, o3Rep);

		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);

		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());		
		assertEquals(6, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
	}

	@Test
	public void classesAndPropertiesRepresentedByThemselves() throws ConfigurationException, BeansException, SQLException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		SummarizerParams params = TestUtils.getTestSummarizerParams(TYPED_STRONG_SUMMARY, TYPED_STRONG_DATASETS_DIR.concat("classes_and_props.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveSummaryResult summaryResult = summarizer.summarize(params);

		StrongTypeEquivalenceSummary summary = (StrongTypeEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClassNodes(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));
		assertEquals(3, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());

		// Property nodes
		DmlDao dmlDao = context.getBean(DmlDao.class);
		assertTrue(TestUtils.containsPropertyNode(dmlDao, summaryResult.getPropertyNodesTable(), dictionary.getKey("<p1>")));
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getPropertyNodeCount());

		// Data nodes count
		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertTrue(s1Rep != o1Rep);
		assertTrue(s1Rep != o2Rep);
		assertTrue(s1Rep != o3Rep);
		assertTrue(o1Rep != o2Rep);
		assertTrue(o1Rep != o3Rep);
		assertTrue(o2Rep != o3Rep);
		assertEquals(4, summary.getSummaryDataNodes().size());

		// Edges 
		assertEquals(4, summaryResult.getStatistics().getSummaryTripleStats().getDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());

		// Total nodes
		assertEquals(8, summaryResult.getStatistics().getSummaryNodeStats().getTotalDistinctNodeCount());
	}

	@Test
	public void testRepresentingDataNodes() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(TYPED_STRONG_SUMMARY, TYPED_STRONG_DATASETS_DIR.concat("repr.nt"), null);
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		TroveSummaryResult summaryResult = summarizer.summarize(params);

		StrongTypeEquivalenceSummary summary = (StrongTypeEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);
		final String qualifiedEncodedSummaryTable = NameUtils.getQualifiedEncodedSummaryTableName(summaryResult.getStorageLayout().getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		String encReprTable = NameUtils.getEncodedRepresentationTableName(qualifiedEncodedSummaryTable);

		// Representatives of all nodes are distinct and represent only one node
		int s1Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int o1Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		int o4Repr = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		TIntSet set = new TIntHashSet();
		set.add(s1Repr);
		set.add(s2Repr);
		set.add(o1Repr);
		set.add(o2Repr);
		set.add(o3Repr);
		set.add(o4Repr);
		assertEquals(6, set.size()); // true if nodes are distinct
		// Represent only one node
		set = summary.getRepresentedInputDataNodes(s1Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(s2Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o1Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o2Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o3Repr);
		assertEquals(1, set.size());
		set = summary.getRepresentedInputDataNodes(o4Repr);
		assertEquals(1, set.size());
		// Entries exist in the table
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s1>"), s1Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<s2>"), s2Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o1>"), o1Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o2>"), o2Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o3>"), o3Repr));
		assertTrue(rdfTablesDao.existsRepresentationMapping(encReprTable, dictionary.getKey("<o4>"), o4Repr));
	}
}
