package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * Integer representation of an RDF summary based Trove collections
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface RdfSummary {
	
	/** Creates a summary data node representing gDataNode 
	 * @throws SQLException 
	 * @throws BerkeleyDbException */
	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException;	
	
	/**
	 * Creates a data triple (source, dataProperty, target).
	 * @param source
	 * @param dataProperty
	 * @param target
	 * @throws BerkeleyDbException
	 */
	public DataTriple createDataTriple(final int source, final int dataProperty, final int target) throws BerkeleyDbException;
	
	/** Returns true if the specified data triple exists in the summary, otherwise false */
	public boolean existsDataTriple(int subject, int dataProperty, int object);
	
	/** Returns the class set of the specified node or null if the node is untyped */
	public TIntSet getClassesForSummaryNode(int sNode);
	
	/** Returns the mapping of data nodes and their class sets */
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap();
	
	/**
	 * @return The highest key integer in the dictionary table incremented by 1.
	 * @throws SQLException
	 */
	public int getNextNodeId() throws SQLException;
	
	/** Returns the last created data node */
	public int getLastCreatedDataNode();
	
	/**
	 * Sets the specified node as the last created data node.
	 * @param dataNode
	 */
	public void setLastCreatedDataNode(int dataNode);
	
	/** Returns the set of data triples having the specified data property or null if the property is not contained in any data triples */
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty);
	
	/** Returns the set of G data nodes represented by the specified summary data node or 
	 * null if sDataNode doesn't represent any node. */
	public TIntSet getRepresentedInputDataNodes(int sDataNode);
	
	/** Returns the number of data nodes in the summary */
	public int getSummaryDataNodeCount();
	
	/** Returns a set of summary data triples */
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap();
	
	/** Returns the set of all data nodes */
	public TIntSet getSummaryDataNodes();
	
	/** Get the number of resources from the input graph represented by the specified data node */
	public int getSummaryDataNodeSupport(int sDataNode);
	
	/** Retrieves the summary data node representing the gDataNode, or 0 if gDataNode is unrepresented */
	public int getSummaryDataNodeForInputDataNode(int gDataNode);
		
	/** Stores sDataNode as the representative of gDataNode */
	public void representInputDataNode(int gDataNode, int sDataNode);

	/**
	 * Maps the summary node (data, class or property) and its classes.
	 * 
	 * @param sNode
	 * @param classes
	 */
	public void storeClassesBySummaryNode(int sNode, TIntSet classes);
	
	/** Returns true if the specified iri is a class IRI, otherwise false.
	 *  An iri is a class if it appears as the object of a type triple in the input graph. 
	 * @throws BerkeleyDbException 
	 */
	public boolean isClass(int iri) throws BerkeleyDbException;
	
	/** Returns true if the specified iri is a property IRI, otherwise false.
	 *  An iri is a property if it's the rdf:type or RDFS property, or if it appears in the property position of a triple in the input graph. 
	 * @throws BerkeleyDbException 
	 */
	public boolean isProperty(int iri) throws BerkeleyDbException;
	
	/** Returns true if the specified iri is a class or property IRI, otherwise false. 
	 * @throws BerkeleyDbException 
	 */
	public boolean isClassOrProperty(int iri) throws BerkeleyDbException;
	
	/** Removes the resource mappings for sDataNode from the multimap if existing */
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode);
}
