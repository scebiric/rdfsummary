package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.old.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakSummaryImpl implements WeakEquivalenceSummary, PureCliqueEquivalenceSummary {

	private final WeakEquivalenceSummary weakEquivSummary;
	private final PureCliqueEquivalenceSummary pureCliqueEquivSummary;
	private final RdfSummary rdfSummary;
	
	/**
	 * @param weakEquivSummary
	 * @param pureCliqueEquivSummary
	 * @param rdfSummary
	 */
	public WeakSummaryImpl(final WeakEquivalenceSummary weakEquivSummary, PureCliqueEquivalenceSummary pureCliqueEquivSummary,
			final RdfSummary rdfSummary) {
		this.weakEquivSummary = weakEquivSummary;
		this.pureCliqueEquivSummary = pureCliqueEquivSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public int unifyDataNodes(int dataNode1, int dataNode2) throws BerkeleyDbException {
		return weakEquivSummary.unifyDataNodes(dataNode1, dataNode2);
	}

	@Override
	public int getDegreeForDataNode(int dataNode) throws BerkeleyDbException {
		return weakEquivSummary.getDegreeForDataNode(dataNode);
	}

	@Override
	public int getIncomingDegreeForDataNode(int dataNode) throws BerkeleyDbException {
		return weakEquivSummary.getIncomingDegreeForDataNode(dataNode);
	}

	@Override
	public int getOutgoingDegreeForDataNode(int dataNode) throws BerkeleyDbException {
		return weakEquivSummary.getOutgoingDegreeForDataNode(dataNode);
	}

	@Override
	public void setSourceDataNodeForDataProperty(int dataProperty, int dataNode) throws BerkeleyDbException {
		weakEquivSummary.setSourceDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public void setTargetDataNodeForDataProperty(int dataProperty, int dataNode) throws BerkeleyDbException {
		weakEquivSummary.setTargetDataNodeForDataProperty(dataProperty, dataNode);
	}

	@Override
	public int getSourceDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getSourceDataNodeForDataProperty(dataProperty);
	}

	@Override
	public int getTargetDataNodeForDataProperty(int dataProperty) {
		return weakEquivSummary.getTargetDataNodeForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode) throws BerkeleyDbException {
		return weakEquivSummary.getDataPropertiesForSourceDataNode(dataNode);
	}

	@Override
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode) throws BerkeleyDbException {
		return weakEquivSummary.getDataPropertiesForTargetDataNode(dataNode);
	}

	@Override
	public int createDataNode(TIntSet gDataNodes, TIntSet classes) throws SQLException, BerkeleyDbException {
		return pureCliqueEquivSummary.createDataNode(gDataNodes, classes);
	}

	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		rdfSummary.createTypeTriples(sNode, classes);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrProperty(int iri) throws BerkeleyDbException {
		return rdfSummary.isClassOrProperty(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) throws BerkeleyDbException {
		rdfSummary.createDataTriple(source, dataProperty, target);
		
		if (!rdfSummary.isClassOrProperty(source))
			this.setSourceDataNodeForDataProperty(dataProperty, source);
		if (!rdfSummary.isClassOrProperty(target))
			this.setTargetDataNodeForDataProperty(dataProperty, target);
	}

	@Override
	public void createTypeTriple(int sNode, int classIRI) {
		pureCliqueEquivSummary.createTypeTriple(sNode, classIRI);
	}

	@Override
	public boolean existsTypeTriple(int sNode, int classIRI) {
		return pureCliqueEquivSummary.existsTypeTriple(sNode, classIRI);
	}
}
