package fr.inria.oak.RDFSummary.summary.trove.triplecreator.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.Summarizer;
import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.TroveSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTripleCreatorImplTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;
	
	private static final String WEAK_PURE_CLIQUE_SUMMARY = SummaryType.WEAK;
	private static final String WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR = "/weak/";
	
	@Before
	public void setUp() throws BeansException, SQLException, ConfigurationException, IOException, ParametersException {
		context = new AnnotationConfigApplicationContext();
		TestUtils.deleteBerkeleyDbDirectory();
	}
	
	@Test
	public void weakPureCliqueEquivalenceSummary_representTypeTriple_subjectsOfSameDataPropertyOfSameType_summaryContainsOnlyOneTypeTriple() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_PURE_CLIQUE_SUMMARY, WEAK_PURE_CLIQUE_SUMMARY_DATASETS_DIR.concat("unique_type_triples.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());

		// Run
		TroveSummaryResult summaryResult = summarizer.summarize(params);
				
		// Assert
		dictionary = TestUtils.loadMemoryDictionary(context);
		RdfTablesDao rdfTablesDao = context.getBean(RdfTablesDao.class);
		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();						
		assertEquals(1, summaryResult.getStatistics().getSummaryNodeStats().getClassNodeCount());
		ResultSet resultSet = rdfTablesDao.distinctClassesFromTypeComponent(NameUtils.getEncodedTypesTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable())); 
		resultSet.next();
		int classIRI = resultSet.getInt(1);
		assertEquals(dictionary.getKey("<c1>").intValue(), classIRI);
		assertEquals(1, summaryResult.getStatistics().getSummaryTripleStats().getTypeTripleCount());
		int sDataNode = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<x1>"));
		assertEquals(sDataNode, summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<x3>")));
		assertTrue(summary.existsTypeTriple(sDataNode, classIRI));
	}

}
