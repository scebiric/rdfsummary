package fr.inria.oak.RDFSummary;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.ExperimentsParams;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.SummaryResult;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Experiments {

	private static Logger log = Logger.getLogger(fr.inria.oak.RDFSummary.summary.Summarizer.class);
	private static AnnotationConfigApplicationContext Context = null;
	private static Summarizer Summarizer = null;

	/**
	 * Constructor of {@link Experiments}
	 */
	public Experiments() {
		Summarizer = new Summarizer();
	}

	/**
	 * 
	 * @param args
	 * @throws ConfigurationException
	 * @throws SQLException
	 * @throws JenaException
	 * @throws PsqlStorageException
	 * @throws SchemaException
	 * @throws UnsupportedDatabaseEngineException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws InexistentKeyException
	 * @throws IOException
	 * @throws ParametersException
	 * @throws QueryException
	 * @throws UnsupportedStorageSchemaException 
	 * @throws BerkeleyDbException 
	 */
	public static void main(String[] args) throws ConfigurationException, SQLException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		// Setup
		Configurator configurator = new Configurator();		
		ExperimentsParams expParams = configurator.loadExperimentsParameters(args);
		if (!expParams.logging()) {
			Logger.getRootLogger().removeAllAppenders();
			Logger.getRootLogger().addAppender(new NullAppender());
		}
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, expParams);

		// Run
		log.info("Number of runs: " + expParams.getNumberOfRuns());
		SummaryResult summaryResult = null;
		long totalTime = 0;
		for (int i = 1; i <= expParams.getNumberOfRuns(); i++) {
			log.info("Run " + i);
			summaryResult = Summarizer.summarize(expParams.getSummarizerParams());
			totalTime += summaryResult.getStatistics().getSummarizationTime();
		}

		float averageTime = totalTime / expParams.getNumberOfRuns();

		String filepath = Utils.getFilepath(expParams.getSummarizerParams().getExportParams().getOutputFolder(), 
				expParams.getSummarizerParams().getSummarizationParams().getPostgresSchemaName() + "_" + 
						expParams.getSummarizerParams().getSummarizationParams().getSummaryType() + "_experiments" , "txt");
		PrintWriter writer = new PrintWriter(new FileWriter(filepath));
		writer.println("Number of runs: " + expParams.getNumberOfRuns());
		writer.println("Fetch size: " + expParams.getSummarizerParams().getSummarizationParams().getFetchSize());
		writer.println("Average summarization time: " + averageTime + " ms");
		writer.close();
		log.info("Experiments results written to: " + filepath);

		// Close context
		closeApplicationContext();
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}

}
