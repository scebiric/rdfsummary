package fr.inria.oak.RDFSummary.summary.trove.summarizer;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.summary.SummaryResult;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummary;

/**
 * The result of building a Trove summary
 * 
 * @author Sejla CEBIRIC * 
 */
public class TroveSummaryResult extends SummaryResult {

	private final RdfSummary summary;	
	private final String classNodesTable;
	private final String propertyNodesTable;
		
	/**
	 * @param summaryType
	 * @param summary
	 * @param classNodesTable
	 * @param propertyNodesTable
	 */
	public TroveSummaryResult(final String summaryType, final RdfSummary summary, final String classNodesTable, final String propertyNodesTable) {
		super(summaryType);
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(summary instanceof PureCliqueEquivalenceSummary || summary instanceof TypeEquivalenceSummary);
		Preconditions.checkArgument(summary instanceof WeakEquivalenceSummary || summary instanceof StrongEquivalenceSummary);
		this.summary = summary;
		this.classNodesTable = classNodesTable;
		this.propertyNodesTable = propertyNodesTable;
	}
	
	public RdfSummary getSummary() {
		return this.summary;
	}
	
	public String getClassNodesTable() {
		return classNodesTable;
	}
	
	public String getPropertyNodesTable() {
		return propertyNodesTable;
	}
}
