package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.Summarizer;
import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.TroveSummaryResult;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.StrongSummary;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Integration tests for the {@link StrongSummary} on PostgreSQL
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PsqlStrongSummaryTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	@Before
	public void setUp() {
		context = new AnnotationConfigApplicationContext();
	}
	
	@Test
	public void summarizeExample() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/example.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		StrongSummary summary = (StrongSummary) summaryResult.getSummary(); 
		assertEquals(12, summaryResult.getStatistics().getTotalSummaryNodeCount()); 
		
		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<Book>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<Journal>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<Spec>")));

		// Data nodes count
		assertEquals(9, summary.getDataNodes().size());

		// Edges
		assertEquals(9, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(4, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}
	
	@Test
	public void summarizeCommonSources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/cs.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c3>")));
		
		// Common source patterns
		// Pattern 1
		int dataNode = summary.getDataNodeForResource(dictionary.getKey("<s1>"));
		assertEquals(3, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s1>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s2>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s3>")));
		
		// Typing of the common source
		assertEquals(2, summary.getClassesForNode(dataNode).size());
		assertTrue(summary.getClassesForNode(dataNode).contains(dictionary.getKey("<c1>")));
		assertTrue(summary.getClassesForNode(dataNode).contains(dictionary.getKey("<c2>")));
		
		// Properties of the common source
		DataTriple dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p1>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p2>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p4>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p5>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Pattern 2
		dataNode = summary.getDataNodeForResource(dictionary.getKey("<s4>"));
		assertEquals(2, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s4>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s5>")));
		
		// Typing of the common source
		assertNull(summary.getClassesForNode(dataNode));
		
		// Properties of the common source
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p6>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p7>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p8>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Pattern 3
		dataNode = summary.getDataNodeForResource(dictionary.getKey("<s6>"));
		assertEquals(1, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<s6>")));
		
		// Typing of the common source
		assertNull(summary.getClassesForNode(dataNode));
		
		// Properties of the common source
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p9>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p10>")).iterator().next();
		assertEquals(dataNode, dataTriple.getSubject());
		
		// Data nodes count
		assertEquals(13, summary.getDataNodes().size());
		// Edges count		 
		assertEquals(10, summaryResult.getStatistics().getSummaryDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}
	
	@Test
	public void summarizeCommonTargets() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/ct.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForNodeMap()).contains(dictionary.getKey("<c3>")));
		
		// Common target patterns
		// Pattern 1
		int dataNode = summary.getDataNodeForResource(dictionary.getKey("<o1>"));
		assertEquals(3, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o1>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o2>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o3>")));
		
		// Typing of the common target
		assertEquals(2, summary.getClassesForNode(dataNode).size());
		assertTrue(summary.getClassesForNode(dataNode).contains(dictionary.getKey("<c1>")));
		assertTrue(summary.getClassesForNode(dataNode).contains(dictionary.getKey("<c2>")));
		
		// Properties of the common target
		DataTriple dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p1>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p2>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p4>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p5>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Pattern 2
		dataNode = summary.getDataNodeForResource(dictionary.getKey("<o4>"));
		assertEquals(2, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o4>")));
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o5>")));
		
		// Typing of the common target
		assertNull(summary.getClassesForNode(dataNode));
		
		// Properties of the common target
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p6>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p7>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p8>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Pattern 3
		dataNode = summary.getDataNodeForResource(dictionary.getKey("<o6>"));
		assertEquals(1, summary.getRepresentedResources(dataNode).size());
		assertTrue(summary.getRepresentedResources(dataNode).contains(dictionary.getKey("<o6>")));
		
		// Typing of the common target
		assertNull(summary.getClassesForNode(dataNode));
		
		// Properties of the common target
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p9>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		dataTriple = summary.getDataTriplesForProperty(dictionary.getKey("<p10>")).iterator().next();
		assertEquals(dataNode, dataTriple.getObject());
		
		// Data nodes count
		assertEquals(13, summary.getDataNodes().size());
		// Edges count		 
		assertEquals(10, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(3, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}
	
	/**
	 * Resources from G have the same source AND target clique as their representative data node in the summary.
	 */
	@Test
	public void summarizeExample2() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/example2.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getDataNodeForResource(dictionary.getKey("<s1>"));
		int s2Rep = summary.getDataNodeForResource(dictionary.getKey("<s2>"));
		int s3Rep = summary.getDataNodeForResource(dictionary.getKey("<s3>"));
		int o1Rep = summary.getDataNodeForResource(dictionary.getKey("<o1>"));
		int o2Rep = summary.getDataNodeForResource(dictionary.getKey("<o2>"));
		int o3Rep = summary.getDataNodeForResource(dictionary.getKey("<o3>"));
		int o4Rep = summary.getDataNodeForResource(dictionary.getKey("<o4>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o1Rep, o3Rep);
		assertEquals(o2Rep, o4Rep);
		assertNotEquals(o1Rep, o2Rep);
		DataTriple dt = summary.getDataTriplesForProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		
		assertEquals(0, summaryResult.getStatistics().getSummaryClassNodeCount());		
		assertEquals(5, summary.getDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());
	}
	
	@Test
	public void summarizeExample3() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/example3.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getDataNodeForResource(dictionary.getKey("<s1>"));
		int s2Rep = summary.getDataNodeForResource(dictionary.getKey("<s2>"));
		int s3Rep = summary.getDataNodeForResource(dictionary.getKey("<s3>"));
		int o1Rep = summary.getDataNodeForResource(dictionary.getKey("<o1>"));
		int o2Rep = summary.getDataNodeForResource(dictionary.getKey("<o2>"));
		int o3Rep = summary.getDataNodeForResource(dictionary.getKey("<o3>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o1Rep, o2Rep);
		assertEquals(o1Rep, o3Rep);
		
		DataTriple dt = summary.getDataTriplesForProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		assertEquals(0, summaryResult.getStatistics().getSummaryClassNodeCount());		
		assertEquals(4, summary.getDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());
	}
}
