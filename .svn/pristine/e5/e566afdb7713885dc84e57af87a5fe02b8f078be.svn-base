package fr.inria.oak.RDFSummary.data.saturation.rules;

import java.util.Collection;
import java.util.Set;

import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.rdfschema.Schema;

/**
 * @author Damian Bursztyn
 */
public interface Rule {
	/**
	 * Indicates if the given triple matches the premise of the rule with
	 * respect to the given schema.
	 *
	 * @param triple
	 * @param schema
	 * @return true, if the given triple matches the premise of the rule, with
	 *         respect to the given schema
	 */
	public boolean matches(final Triple triple, final Schema schema);

	/**
	 * Produce the triples according to the rule, for the given triple with
	 * respect to the given schemas.
	 *
	 * @param triple
	 * @param schemaa
	 * @return the triples produced by the rule
	 */
	public Set<Triple> produce(final Triple triple, final Collection<Schema> schemas);
}