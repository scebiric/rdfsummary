package fr.inria.oak.RDFSummary.summary.trove.objectcreator;

import java.sql.SQLException;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.SummaryException;
import fr.inria.oak.RDFSummary.summary.trove.WeakSummary;
import gnu.trove.set.TIntSet;

public class WeakTroveObjectCreatorImpl implements WeakTroveObjectCreator {

	@Override
	public void representDataTriple(final WeakSummary summary, final int subject, final int property, final int object) throws SQLException, SummaryException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(subject > 0);
		Preconditions.checkArgument(property > 0);
		Preconditions.checkArgument(object > 0);

		int source = subject;
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);

		int target = object;
		if (!summary.isClassOrProperty(object))
			target = this.getTargetDataNode(summary, object, property);

		// In the case of paths, the getTarget method may change the source and vice-versa
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);
		if (!summary.isClassOrProperty(object))
			target = this.getTargetDataNode(summary, object, property);

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}

	/**
	 * Obtains a data node representing the subject and the property source.
	 * 
	 * There should be only one data node source per property. For this reason it does not suffice to 
	 * just keep track of data nodes and the resources they represent, but also of property sources - 
	 * if the two differ unification occurs. So for example, s1 p1 o1, s2 p1 o2, s1 and s2 should be represented
	 * by the same source in the summary.
	 * 
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the property source 
	 * @throws SQLException 
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public int getSourceDataNode(final WeakSummary summary, final int subject, final int dataProperty) throws SQLException, SummaryException, BerkeleyDbException {
		int propertySource = summary.getSourceDataNodeForDataProperty(dataProperty);		
		int subjectNode = summary.getSummaryDataNodeForInputDataNode(subject);

		if (propertySource > 0 && subjectNode > 0) {
			if (propertySource == subjectNode)
				return subjectNode;
			else 
				return summary.unifyDataNodes(propertySource, subjectNode);				
		}

		if (propertySource > 0 && subjectNode == 0) {
			summary.representInputDataNode(subject, propertySource);
			return propertySource;
		}

		if (propertySource == 0 && subjectNode > 0) {
			return subjectNode;
		}

		// untypedPropertySource == 0 && subjectNode == 0
		return summary.createDataNode(subject);
	}

	/**
	 * Obtains a data node representing the subject and the property target.
	 * 
	 * There should be only one data node target per property. For this reason it does not suffice to 
	 * just keep track of data nodes and the resources they represent, but also of property targets - 
	 * if the two differ unification occurs. So for example, s1 p1 o1, s2 p1 o2, o1 and o2 should be represented
	 * by the same target in the summary.
	 * 
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the subject and the property target
	 * @throws SQLException 
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public int getTargetDataNode(final WeakSummary summary, final int object, final int dataProperty) throws SQLException, SummaryException, BerkeleyDbException {
		int propertyTarget = summary.getTargetDataNodeForDataProperty(dataProperty);
		int objectNode = summary.getSummaryDataNodeForInputDataNode(object);

		if (propertyTarget > 0 && objectNode > 0) {
			if (propertyTarget == objectNode)
				return objectNode;
			else 
				return summary.unifyDataNodes(propertyTarget, objectNode);
		}

		if (propertyTarget > 0 && objectNode == 0) {
			summary.representInputDataNode(object, propertyTarget);
			return propertyTarget;
		}

		if (propertyTarget == 0 && objectNode > 0) {
			return objectNode;
		}

		// untypedPropertyTarget == 0 && objectNode == 0
		return summary.createDataNode(object);
	}

	/**
	 * Returns true if the type triple has been represented in the summary, otherwise false.
	 * 
	 * Assumes that data triples have already been summarized.
	 * Therefore, if gNode is a data node and has some data properties in G, then it has already been represented by 
	 * a data node in the summary when summarizing data triples.
	 * Otherwise, gNode doesn't have any data properties in G,
	 * i.e. it is typed-only, and will be represented later, thus the method returns false. 
	 * 
	 * If the type triple from G should be represented,
	 * a corresponding type triple is created in the summary, with the representative of gNode
	 * as the source and the specified class IRI as the target.
	 * If gNode is a class or a property, it is represented by itself.
	 *  
	 * The method ensures that the summary type triples are unique.
	 * 
	 * Same as for the strong summary. 
	 *     
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public boolean representTypeTriple(final WeakSummary summary, final int gNode, final int classIRI) throws SummaryException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkArgument(classIRI > 0);

		if (summary.isClassOrProperty(gNode)) {
			if (!summary.existsTypeTriple(gNode, classIRI))
				summary.createTypeTriple(gNode, classIRI);

			return true;
		}
		else {
			int sDataNode = summary.getSummaryDataNodeForInputDataNode(gNode);
			if (sDataNode == 0) 
				return false;

			if (!summary.existsTypeTriple(sDataNode, classIRI)) 
				summary.createTypeTriple(sDataNode, classIRI);

			return true;
		}
	}

	/**
	 * A single data node is created, representing all gDataNodes and having all the specified classes.
	 * @throws SQLException 
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void representTypeTriples(final WeakSummary summary, final TIntSet gDataNodes, final TIntSet classes) throws SQLException, SummaryException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		Preconditions.checkArgument(gDataNodes.size() > 0);

		int sDataNode = summary.createDataNode(gDataNodes, classes);
		// Uniqueness: Since the data node is newly created and the classes in the specified set are unique there couldn't have
		// already existed a type edge to any given class
		summary.createTypeTriples(sDataNode, classes);	
	}
}
