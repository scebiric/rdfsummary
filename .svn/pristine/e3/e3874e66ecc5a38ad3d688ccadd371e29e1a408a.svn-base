package fr.inria.oak.RDFSummary.data.storage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import fr.inria.oak.RDFSummary.config.params.ConnectionParams;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.encoding.Encoding;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.saturation.SaturationGenerator;
import fr.inria.oak.RDFSummary.data.storage.splitter.StorageSplitter;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.SchemaUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.StorageSchema;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Implementation of {@link EncodedPsqlStorage} based on an in-memory dictionary.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class MemoryDictTriplesTableEncodedPsqlStorageImpl implements EncodedPsqlStorage {

	private static final Logger log = Logger.getLogger(EncodedPsqlStorage.class);
	private static PsqlStorage PsqlStorage;
	private static DictionaryDao DictDao;
	private static Encoding Encoding;
	private static SaturationGenerator SaturationGenerator;
	private static StorageSplitter StorageSplitter;
	private static DmlDao DmlDao;
	private static RdfTablesDao RdfTablesDao;
	private static Timer Timer;

	/**
	 * @param psqlStorage
	 * @param dictDao
	 * @param encoding
	 * @param saturationGenerator
	 * @param storageSplitter
	 * @param dmlDao
	 * @param rdfTablesDao
	 * @param timer
	 */
	public MemoryDictTriplesTableEncodedPsqlStorageImpl(final PsqlStorage psqlStorage, final DictionaryDao dictDao, final Encoding encoding, 
			final SaturationGenerator saturationGenerator, final StorageSplitter storageSplitter, 
			final DmlDao dmlDao, final RdfTablesDao rdfTablesDao, final Timer timer) {
		PsqlStorage = psqlStorage;
		DictDao = dictDao;
		Encoding = encoding;
		SaturationGenerator = saturationGenerator;
		StorageSplitter = storageSplitter;
		DmlDao = dmlDao;
		RdfTablesDao = rdfTablesDao;
		Timer = timer;
	}

	/**
	 * Stores an RDF dataset to Postgres: loading data and schema, encoding, splitting based on type property.
	 * Table names converted to lowercase since Postgres forces lowercase names of database objects.
	 */
	@Override
	public DatasetPreparationResult prepareDataset(final ConnectionParams connParams, final String schemaName, final boolean dropSchema, final String dictionaryTable,
			final String inputTriplesTable, final String dataTriplesFilepath, final String schemaTriplesFilepath, final boolean indexing) throws SQLException, UnsupportedDatabaseEngineException, JenaException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(inputTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTriplesFilepath));	

		log.info("Storing the RDF dataset to Postgres in " + StorageSchema.TRIPLES_TABLE + " layout...");

		// Load non-encoded triples
		TriplesLoadingResult triplesLoadingResult = PsqlStorage.loadTriplesFromFilesToTables(schemaName, dropSchema, inputTriplesTable, dataTriplesFilepath, schemaTriplesFilepath, indexing);

		// Encode data and types
		String encodedTriplesTableName = NameUtils.getEncodedTriplesTableName(schemaName, inputTriplesTable);
		String dataAndTypesEncodingTime = encodeDataAndTypes(encodedTriplesTableName, dictionaryTable, triplesLoadingResult.getStorageLayout(), connParams);
		// Encode schema if it exists
		String schemaEncodingTime = encodeSchema(triplesLoadingResult.getSchema(), connParams, dictionaryTable);

		// Load dictionary entries for rdf:type and RDFS properties to memory
		Dictionary dictionary = DictDao.loadDictionaryForTypeAndRdfs();

		// Split storage		
		String encodedDataTableName = NameUtils.getEncodedDataTableName(schemaName, inputTriplesTable);
		String encodedTypesTableName = NameUtils.getEncodedTypesTableName(schemaName, inputTriplesTable);
		log.info("Splitting the encoded triples table...");		
		String createEncodedTypesTableTime = StorageSplitter.createEncodedTypesTable(encodedTypesTableName, triplesLoadingResult.getStorageLayout(), dictionaryTable);
		String createEncodedDataTableTime = StorageSplitter.createEncodedDataTable(encodedDataTableName, triplesLoadingResult.getStorageLayout(), dictionary);

		// Create indexes
		// Data table indexes
		final Map<String, String> indexes = Maps.newHashMap();
		indexes.put("spo", "s, p, o");
		indexes.put("osp", "o, s, p");
		Utils.createIndexesIfNotExists(indexes, true, encodedDataTableName, schemaName);
		// Types table indexes
		indexes.clear();
		indexes.put("so", "s, o");
		Utils.createIndexesIfNotExists(indexes, true, encodedTypesTableName, schemaName);
		
		// Load dictionary entries for schema constants, type and RDFS properties
		Set<String> constants = SchemaUtils.getConstants(triplesLoadingResult.getSchema());
		dictionary = DictDao.loadDictionaryForConstants(constants);
		String encodedSchemaTable = NameUtils.getEncodedSchemaTableName(schemaName, inputTriplesTable);
		String createEncodedSchemaTableTime = storeSchemaFromMemoryToEncodedTable(triplesLoadingResult.getSchema(), encodedSchemaTable, dictionary, triplesLoadingResult.getStorageLayout());

		EncodingTimes encodedTriplesLoadingTimes = new EncodingTimes(dataAndTypesEncodingTime, schemaEncodingTime, createEncodedSchemaTableTime, createEncodedTypesTableTime, createEncodedDataTableTime);

		return new DatasetPreparationResult(triplesLoadingResult, encodedTriplesLoadingTimes);
	}

	/**
	 * Stores an RDF dataset to Postgres: loading data and schema, encoding, splitting based on type property.
	 * Table names converted to lowercase since Postgres forces lowercase names of database objects.
	 * 
	 * Saturates the encoded triples table before splitting it based on the type property.
	 */
	@Override
	public DatasetPreparationResult prepareDataset(final ConnectionParams connParams, final String schemaName, final boolean dropSchema, final String dictionaryTable,
			final String inputTriplesTable, final String dataTriplesFilepath, final String schemaTriplesFilepath, final boolean indexing, final int saturationBatchSize) throws SQLException, UnsupportedDatabaseEngineException, JenaException, IOException, PsqlStorageException, SchemaException, DictionaryException, InexistentValueException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(inputTriplesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTriplesFilepath));	

		log.info("Storing the RDF dataset to Postgres in " + StorageSchema.TRIPLES_TABLE + " layout...");

		// Load non-encoded triples
		TriplesLoadingResult triplesLoadingResult = PsqlStorage.loadTriplesFromFilesToTables(schemaName, dropSchema, inputTriplesTable, dataTriplesFilepath, schemaTriplesFilepath, indexing);
		if (triplesLoadingResult.getSchema() == null)
			throw new PsqlStorageException("This method requires a schema in order to saturate the input.");

		// Encode data and types
		String encodedTriplesTable = NameUtils.getEncodedTriplesTableName(schemaName, inputTriplesTable);
		String dataAndTypesEncodingTime = encodeDataAndTypes(encodedTriplesTable, dictionaryTable, triplesLoadingResult.getStorageLayout(), connParams);
		// Encode schema if it exists
		String schemaEncodingTime = encodeSchema(triplesLoadingResult.getSchema(), connParams, dictionaryTable);

		// Saturate the encoded triples table
		
		final String saturatedEncodedTriplesTable = NameUtils.getSaturatedEncodedTriplesTableName(encodedTriplesTable);
		SaturationGenerator.saturate(saturatedEncodedTriplesTable, encodedTriplesTable, dictionaryTable, Lists.newArrayList(triplesLoadingResult.getSchema()), saturationBatchSize);
		triplesLoadingResult.getStorageLayout().setEncodedTriplesTable(saturatedEncodedTriplesTable);
		
		// Load dictionary entries for rdf:type and RDFS properties to memory
		Dictionary dictionary = DictDao.loadDictionaryForTypeAndRdfs();

		// Split storage		
		String encodedDataTableName = NameUtils.getSaturatedEncodedDataTableName(saturatedEncodedTriplesTable);
		String encodedTypesTableName = NameUtils.getSaturatedEncodedTypesTableName(saturatedEncodedTriplesTable);
		log.info("Splitting the encoded triples table...");		
		String createEncodedTypesTableTime = StorageSplitter.createEncodedTypesTable(encodedTypesTableName, triplesLoadingResult.getStorageLayout(), dictionaryTable);
		String createEncodedDataTableTime = StorageSplitter.createEncodedDataTable(encodedDataTableName, triplesLoadingResult.getStorageLayout(), dictionary);

		// Load dictionary entries for schema constants, type and RDFS properties
		Set<String> constants = SchemaUtils.getConstants(triplesLoadingResult.getSchema());
		dictionary = DictDao.loadDictionaryForConstants(constants);
		String encodedSchemaTable = NameUtils.getEncodedSchemaTableName(schemaName, inputTriplesTable);
		String createEncodedSchemaTableTime = storeSchemaFromMemoryToEncodedTable(triplesLoadingResult.getSchema(), encodedSchemaTable, dictionary, triplesLoadingResult.getStorageLayout());

		EncodingTimes encodedTriplesLoadingTimes = new EncodingTimes(dataAndTypesEncodingTime, schemaEncodingTime, createEncodedSchemaTableTime, createEncodedTypesTableTime, createEncodedDataTableTime);

		return new DatasetPreparationResult(triplesLoadingResult, encodedTriplesLoadingTimes);
	}

	/**
	 * Encodes data and type triples which are assumed to be in a single triples table.
	 * Sets the encoded triples name in the storage layout.
	 * 
	 * @param encodedTriplesTableName
	 * @param dictionaryTable
	 * @param storageLayout
	 * @param connParams
	 * @return The encoding time.
	 * @throws SQLException
	 * @throws UnsupportedDatabaseEngineException
	 */
	private String encodeDataAndTypes(final String encodedTriplesTableName, final String dictionaryTable, final StorageLayout storageLayout,
			final ConnectionParams connParams) throws SQLException, UnsupportedDatabaseEngineException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedTriplesTableName));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(storageLayout.getTriplesTable()));

		String dataAndTypesEncodingTime = Constants.DEFAULT_TIME;
		if (DmlDao.existsTable(dictionaryTable)) 
			log.info("Data already encoded.");
		else 
			dataAndTypesEncodingTime = Encoding.encodeDataAndTypes(connParams, storageLayout.getTriplesTable(), encodedTriplesTableName, dictionaryTable);

		storageLayout.setEncodedTriplesTable(encodedTriplesTableName);

		return dataAndTypesEncodingTime;
	}

	/**
	 * Encodes the schema component if it exists
	 * @return The encoding time 
	 * @throws DictionaryException 
	 * @throws SQLException 
	 * @throws UnsupportedDatabaseEngineException
	 */
	private String encodeSchema(final Schema schema, final ConnectionParams connParams, final String dictionaryTable) throws UnsupportedDatabaseEngineException, SQLException, DictionaryException {
		Preconditions.checkNotNull(connParams);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dictionaryTable));

		String schemaEncodingTime = Constants.DEFAULT_TIME;
		if (schema != null) 
			schemaEncodingTime = Encoding.encodeSchema(connParams, schema, dictionaryTable);

		return schemaEncodingTime;
	}

	/**
	 * Stores schema triples from the Schema object into a Postgres table. 
	 * 
	 * @param schema
	 * @param encodedSchemaTable
	 * @param dictionary
	 * @param storageLayout
	 * @return The schema storing time.
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 */
	@Override
	public String storeSchemaFromMemoryToEncodedTable(final Schema schema, final String encodedSchemaTable, final Dictionary dictionary, final StorageLayout storageLayout) throws SQLException, DictionaryException, InexistentValueException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTable));
		Preconditions.checkNotNull(dictionary);
		Preconditions.checkNotNull(storageLayout); 

		if (DmlDao.existsTable(encodedSchemaTable))
			log.info(encodedSchemaTable + " table already exists.");
		else {
			log.info("Storing schema triples from memory to table...");
			Timer.reset();		
			RdfTablesDao.createEncodedTriplesTableWithId(encodedSchemaTable);
			if (schema != null) {			
				Timer.start();
				Set<fr.inria.oak.commons.reasoning.rdfs.saturation.Triple> rdfsTriples = schema.getAllTriples();
				RdfTablesDao.insertEncodedSchemaTriples(rdfsTriples, encodedSchemaTable, dictionary);			
				Timer.stop();
				log.info("Encoded schema materialized in " + Timer.getTimeAsString());
			}
		}

		storageLayout.setEncodedSchemaTable(encodedSchemaTable);

		return Timer.getTimeAsString();
	}
}
