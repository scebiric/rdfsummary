package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.Summarizer;
import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.TroveSummaryResult;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.StrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypedStrongSummary;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Integration tests for the {@link TypedStrongSummary} on PostgreSQL
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PsqlTypedStrongSummaryTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	@Before
	public void setUp() {
		context = new AnnotationConfigApplicationContext();
	}
	
	/**
	 * Resources from G have the same source AND target clique as their representative data node in the summary.
	 */
	@Test
	public void summarizeExample2() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.TYPED_STRONG, "/typstrong/example2.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		int o4Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o4>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertEquals(o2Rep, o4Rep);
		assertNotEquals(o1Rep, o2Rep);
		assertNotEquals(o1Rep, o3Rep);
		assertNotEquals(o1Rep, o4Rep);
		assertNotEquals(o2Rep, o3Rep);
		assertNotEquals(o3Rep, o4Rep);
		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		
		assertEquals(1, summaryResult.getStatistics().getSummaryClassNodeCount());		
		assertEquals(6, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());
	}
	
	@Test
	public void summarizeExample3() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.TYPED_STRONG, "/typstrong/example3.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		StrongSummary summary = (StrongSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
		int s2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s2>"));
		int s3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s3>"));
		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
		int o3Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o3>"));
		assertNotEquals(s1Rep, s2Rep);
		assertNotEquals(s1Rep, s3Rep);
		assertNotEquals(s2Rep, s3Rep);
		assertNotEquals(o1Rep, o2Rep);
		assertNotEquals(o1Rep, o3Rep);
		
		DataTriple dt = summary.getDataTriplesForDataProperty(dictionary.getKey("<p3>")).iterator().next();
		assertEquals(dt.getObject(), s1Rep);
		
		assertEquals(1, summaryResult.getStatistics().getSummaryClassNodeCount());		
		assertEquals(6, summary.getSummaryDataNodeCount());
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());
	}

}
