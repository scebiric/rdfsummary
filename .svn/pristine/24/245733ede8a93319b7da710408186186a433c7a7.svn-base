package fr.inria.oak.RDFSummary.rdfschema;

import java.io.FileNotFoundException;
import java.sql.SQLException;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Columns;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesTableSchemaParserImpl implements SchemaParser {

	private static DmlDao DmlDao;
	private static DictionaryDao DictDao;

	private final UriReconstructingJenaSchemaParserImpl jenaParser;

	/**
	 * @param dmlDao
	 * @param dictDao
	 */
	public TriplesTableSchemaParserImpl(final DmlDao dmlDao, final DictionaryDao dictDao) {
		super();
		DmlDao = dmlDao;
		DictDao = dictDao;
		jenaParser = new UriReconstructingJenaSchemaParserImpl();
	}

	@Override
	public void loadSchemaTriplesFromTable(final Schema schema, final String encodedSchemaTriplesTable) throws SchemaException, SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(encodedSchemaTriplesTable));
		
		String subject = null;
		String property = null;
		String object = null;
		final ResultSet resultSet = DmlDao.getSelectionResultSet(Columns.TRIPLES, false, encodedSchemaTriplesTable, null);
		while (resultSet.next()) {
			subject = DictDao.getValue(resultSet.getInt(1));
			property = DictDao.getValue(resultSet.getInt(2));
			object = DictDao.getValue(resultSet.getInt(3));
			
			if (property.equals(Rdfs.FULL_SUBCLASS) || property.equals(Rdfs.SUBCLASS))
				schema.addSubClassOfTriple(subject, object);
			else if (property.equals(Rdfs.FULL_SUBPROPERTY) || property.equals(Rdfs.SUBPROPERTY))
				schema.addSubPropertyOfTriple(subject, object);
			else if (property.equals(Rdfs.FULL_DOMAIN) || property.equals(Rdfs.DOMAIN))
				schema.addDomainTriple(subject, object);
			else if (property.equals(Rdfs.FULL_RANGE) || property.equals(Rdfs.RANGE))
				schema.addRangeTriple(subject, object);
			else 
				continue;
		}
		resultSet.close();
	}

	@Override
	public Schema parseSchema() throws SchemaException {
		return jenaParser.parseSchema();
	}

	@Override
	public Schema parseSchema(String filepath, String base, String lang) throws FileNotFoundException, SchemaException {
		return jenaParser.parseSchema(filepath, base, lang);
	}
}
