package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PureCliqueEquivalenceSummaryImpl implements PureCliqueEquivalenceSummary {

	private final RdfSummary rdfSummary;
	private final UriGenerator uriGenerator;
	private final DictionaryDao dictDao;

	private TIntSet classes;
	private TIntIterator iterator = null;
	private int gDataNode;

	/**
	 * @param rdfSummary
	 * @param uriGenerator
	 * @param dictDao
	 */
	public PureCliqueEquivalenceSummaryImpl(final RdfSummary rdfSummary, final UriGenerator uriGenerator, final DictionaryDao dictDao) {
		this.rdfSummary = rdfSummary;
		this.uriGenerator = uriGenerator;
		this.dictDao = dictDao;
	}

	@Override
	public int createDataNode(final TIntSet gDataNodes, final TIntSet classes) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkArgument(gDataNodes.size() > 0);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);

		int sDataNode = dictDao.getExistingOrNewKey(uriGenerator.generateDataNodeUri(rdfSummary.getNextNodeId()));
		this.representInputDataNodes(gDataNodes, sDataNode);
		rdfSummary.setLastCreatedDataNode(sDataNode);

		return sDataNode;
	}

	private void representInputDataNodes(final TIntSet gDataNodes, final int sDataNode) throws BerkeleyDbException {
		iterator = gDataNodes.iterator();
		while (iterator.hasNext()) {
			gDataNode = iterator.next();
			if (rdfSummary.isClassOrProperty(gDataNode))
				throw new IllegalArgumentException("Invalid node type. Only data nodes can be represented in this method.");

			rdfSummary.representInputDataNode(gDataNode, sDataNode);
		}
	}

	@Override
	public void createTypeTriple(final int sNode, final int classIRI) {
		TIntSet classes = rdfSummary.getClassesForSummaryNode(sNode);
		if (classes != null)
			classes.add(classIRI);
		else {
			classes = new TIntHashSet();
			classes.add(classIRI);
			rdfSummary.storeClassesBySummaryNode(sNode, classes);
		}
	}

	@Override
	public boolean existsTypeTriple(final int sNode, final int classIRI) {
		classes = getClassesForSummaryNode(sNode);

		return classes != null && classes.contains(classIRI);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		rdfSummary.createTypeTriples(sNode, classes);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrProperty(int iri) throws BerkeleyDbException {
		return rdfSummary.isClassOrProperty(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) throws BerkeleyDbException {
		rdfSummary.createDataTriple(source, dataProperty, target); 
	}
}
