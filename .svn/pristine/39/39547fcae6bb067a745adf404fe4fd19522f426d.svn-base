package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 * Unit tests for {@link RdfSummaryImpl}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class RdfSummaryImplTest {

	@Test
	public void createDataTriple_newProperty_dataTripleMappedToProperty() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int dataProperty = 2;
		int target = 3;
		UriGenerator uriGenerator = mock(UriGenerator.class);
		DictionaryDao dictDao = mock(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = mock(BerkeleyDbHandler.class);
		String classAndPropertyDb = "testDb";
		RdfSummary summary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		
		// Run
		DataTriple dataTriple = summary.createDataTriple(source, dataProperty, target);
		
		// Assert
		assertEquals(source, dataTriple.getSubject());
		assertEquals(dataProperty, dataTriple.getDataProperty());
		assertEquals(target, dataTriple.getObject());
		List<DataTriple> triples = Lists.newArrayList(summary.getDataTriplesForDataPropertyMap().get(dataProperty));
		assertEquals(1, triples.size());
		assertEquals(dataTriple, triples.get(0));
	}
	
	@Test
	public void createDataTriple_existingProperty_dataTripleMappedToProperty() throws BerkeleyDbException {
		// Set up
		int dataProperty = 2;
		int source1 = 1;
		int target1 = 3;
		int source2 = 4;
		int target2 = 5;
		UriGenerator uriGenerator = mock(UriGenerator.class);
		DictionaryDao dictDao = mock(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = mock(BerkeleyDbHandler.class);
		String classAndPropertyDb = "testDb";
		RdfSummary summary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		DataTriple dataTriple1 = summary.createDataTriple(source1, dataProperty, target1);
		
		// Run
		DataTriple dataTriple2 = summary.createDataTriple(source2, dataProperty, target2);
		
		// Assert
		assertEquals(source2, dataTriple2.getSubject());
		assertEquals(dataProperty, dataTriple2.getDataProperty());
		assertEquals(target2, dataTriple2.getObject());
		List<DataTriple> triples = Lists.newArrayList(summary.getDataTriplesForDataPropertyMap().get(dataProperty));
		assertTrue(triples.contains(dataTriple1));
		assertTrue(triples.contains(dataTriple2));
	}
	
	@Test
	public void representInputDataNode_newGDataNode_newEntryExistsInDirectAndInverseRepresentationMaps() {
		// Set up
		final int gDataNode = 1;
		final int sDataNode = 2;
		RdfSummaryImplStub summary = new RdfSummaryImplStub();
		
		// Call
		summary.representInputDataNode(gDataNode, sDataNode);
		
		// Assert
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(gDataNode));
		assertEquals(1, summary.getRepresentedInputDataNodes(sDataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(1));
	}
	
	@Test
	public void representInputDataNode_existingDataNode_newEntryExistsInDirectAndInverseRepresentationMaps() {
		// Set up
		final int gDataNode = 1;
		final int sDataNode = 2;
		RdfSummaryImplStub summary = new RdfSummaryImplStub();
		summary.representInputDataNode(3, 2);
		
		// Call
		summary.representInputDataNode(gDataNode, sDataNode);
		
		// Assert
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(gDataNode));
		assertEquals(2, summary.getSummaryDataNodeForInputDataNode(3));
		assertEquals(2, summary.getRepresentedInputDataNodes(sDataNode).size());
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(1));
		assertTrue(summary.getRepresentedInputDataNodes(sDataNode).contains(3));
	}

}
