package fr.inria.oak.RDFSummary.config.params;

import java.util.List;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class ExperimentsParams extends Params {

	private final SummarizerParams summarizerParams;
	private final List<String> summaryTypes;
	private final int numberOfRuns;
	private boolean logging;
	
	/**
	 * Constructor of {@link ExperimentsParams}
	 * 
	 * @param summarizerParams
	 * @param summaryTypes
	 * @param numberOfRuns
	 */
	public ExperimentsParams(final SummarizerParams summarizerParams, final List<String> summaryTypes, final int numberOfRuns) {
		this.summarizerParams = summarizerParams;
		this.summaryTypes = summaryTypes;
		this.numberOfRuns = numberOfRuns;
		this.logging = false;
	}
	
	public SummarizerParams getSummarizerParams() {
		return summarizerParams;
	}
	
	public List<String> getSummaryTypes() {
		return summaryTypes;
	}
	
	public int getNumberOfRuns() {
		return numberOfRuns;
	}
	
	public boolean logging() {
		return logging;
	}
	
	public void setLogging(boolean value) {
		logging = value;
	}

}