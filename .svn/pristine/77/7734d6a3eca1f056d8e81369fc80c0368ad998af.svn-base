package fr.inria.oak.RDFSummary.constants;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Constants {
	
	public static final String SUBJECT = "s";
	public static final String PROPERTY = "p";
	public static final String OBJECT = "o";
	
	public static final String DATA = "data";
	public static final String TYPES = "typ";
	public static final String SCHEMA = "schema";
	public static final String ENCODED = "enc";
	public static final String ENCODED_DATA = "enc_data";
	public static final String ENCODED_TYPES = "enc_typ";	
	public static final String ENCODED_SCHEMA = "enc_schema";
	
	public static final String SATURATED = "sat";
	public static final String AUX_SATURATED = "sat_aux";
	public static final String SEQUENCE = "seq";
	public static final String PARTITION = "part";
	public static final String SKIPPED_CHARS = "skipped_chars";
	public static final String REPRESENTATION_TABLE = "repr";
	public static final String ENCODED_REPRESENTATION_TABLE = "enc_repr";
	public static final String INPUT_DATA_NODE = "g_data_node";
	public static final String SUMMARY_DATA_NODE = "s_data_node";
	public static final String SUMMARY_DATA_NODE_ENCODED = "s_data_node_enc";
	public static final String DATA_NODE_MAPPING = "data_node_mapping";
	public static final String TYPE_ALIAS = "type";	
	public static final String HAS_ANSWERS = "has_answers";
	public static final String ENCODED_SUMMARY_TABLE = "enc_sum_table";
	public static final String x = "x";
	public static final String KEY = "key";
	public static final String VALUE = "value";
	public static final String STAR = "*";
	public static final String ID = "id";	
	public static final String DEFAULT_TIME = "0 ms";	
	public static final String NOT_EQUALS = " <> ";
	public static final String EQUALS = " = ";
	public static final String AND = " AND ";
	public static final String OR = " OR ";
	public static final String WHERE = " WHERE ";	
	public static final String CREATE = "CREATE";
	public static final String UNIQUE = "UNIQUE";
	public static final String INDEX = "INDEX";
	public static final String ON = "ON";
	public static final String CLASSES_DB = "classes";
	public static final String PROPERTIES_DB = "properties";
	public static final String THREE_DOTS = "...";
	public static final String ENCODED_DISTINCT_NODES_TABLE = "distinct_nodes_enc";
	public static final String AS = " AS ";
	public static final String JOIN = " JOIN ";
	public static final String GROUP_BY = " GROUP BY ";
	public static final String FROM = " FROM ";
	public static final String SELECT = "SELECT ";
	public static final String DISTINCT = "DISTINCT ";
	public static final String ORDER_BY = " ORDER BY ";
	public static final String INSERT_INTO = "INSERT INTO ";
	public static final String VALUES = " VALUES ";
	public static final String DATA_NODE_PREFIX = "d";
	
	public static final String TEST_CONFIG_FILEPATH = "src/test/resources/config/testconfig.properties";
	public static final String TEST_DATASETS_FOLDER = "/src/test/resources/datasets/";
	public static final String TRIPLES_TABLE_NAME = "g";
	public static final String DICTIONARY_TABLE_NAME = "g_dict";
	public static final String EXISTS = "EXISTS ";
	public static final String UNION = " UNION ";
	public static final String URI = "uri";
	public static final String COUNT = "COUNT";
	public static final String DOT_NODE_PREFIX = "r";
	public static final String APOSTROPHE = "'";
	
	public static final String EXPERIMENTS_SUMMARIZATION_TIME = "exp_summ_time";
	public static final String EXPERIMENTS_COMPLETENESS = "exp_completeness";
	public static final String MILLISECONDS = " ms";
}
