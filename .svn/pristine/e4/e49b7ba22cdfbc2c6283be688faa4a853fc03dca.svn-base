package fr.inria.oak.RDFSummary.writer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.data.storage.TriplesLoadingTimes;
import fr.inria.oak.RDFSummary.data.storage.EncodingTimes;
import fr.inria.oak.RDFSummary.data.storage.StorageLayout;
import fr.inria.oak.RDFSummary.rdf.EncodedTriple;
import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.summary.DotGraph;
import fr.inria.oak.RDFSummary.summary.stats.Stats;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.commons.rdfconjunctivequery.IRI;

/**
 * WriterImpl implements the Writer interface.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WriterImpl implements Writer {
	private static Logger log = Logger.getLogger(Writer.class);
	private String filepath = null;
	private PrintWriter writer = null;
	private PrintWriter dotWriter = null;
	private PrintWriter rdfWriter = null;

	private static final String TXT_EXTENSION = "txt";
	private static final String DOT_EXTENSION = "dot";
	
	/**
	 * Writes the specified dot graph object to a .dot file
	 * @throws IOException 
	 */
	public String writeToDot(DotGraph dot, String outputFolder, String outputName) throws IOException {
		Preconditions.checkNotNull(dot);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputName));

		filepath = Utils.getFilepath(outputFolder, outputName, DOT_EXTENSION);
		writer = new PrintWriter(new FileWriter(filepath));
		writer.println("digraph " + dot.getGraphName() + " {");
		writer.println("graph [margin=\"0,0\", label=\"" + dot.getGraphLabel() + "\", labeljust=left];");
		for (String node : dot.getNodes()) 
			writer.println(node);
		for (String edge : dot.getEdges())
			writer.println(edge);

		writer.println("}");
		writer.close();
		log.info("Generated .dot file " + filepath + ". Generate .pdf with dot -Tpdf -o filename.pdf " + dot.getGraphName() + ".dot");

		return filepath;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Initializes new print writer, writer the graph definition (digraph) and the graph label.
	 */
	@Override
	public String startDotGraph(final String outputFolder, final String outputFileName, final String graphLabel) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFileName));
		Preconditions.checkArgument(!outputFileName.contains(" ") && !outputFileName.contains("-"));
		Preconditions.checkNotNull(graphLabel);
		
		filepath = Utils.getFilepath(outputFolder, outputFileName, DOT_EXTENSION);
		dotWriter = new PrintWriter(new FileWriter(filepath));
		dotWriter.println("digraph " + outputFileName + " {");
		dotWriter.println("graph [margin=\"0,0\", label=\"" + graphLabel + "\", labeljust=left];");

		return filepath;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeToDot(final String entry) {
		dotWriter.println(entry);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeToRdfFile(String subject, String property, String object) {
		rdfWriter.printf("%s %s %s .\n", subject, property, object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String startSummaryFile(final String outputFolder, final String outputFileName, final String rdfFormat) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFileName));
		Preconditions.checkArgument(rdfFormat.equals(RdfFormat.N_TRIPLES));

		filepath = Utils.getFilepath(outputFolder, outputFileName, RdfUtils.getFileExtensionByRDFFormat(rdfFormat));
		rdfWriter = new PrintWriter(new FileWriter(filepath));

		return filepath;
	}

	/**
	 * Decodes the triples and writes them to an n-triples file
	 * @throws IOException 
	 */
	public String writeSaturationTriplesToRDF(List<EncodedTriple> encodedTriples, List<Triple> schemaTriples, String outputFolder, String outputName, String rdfFormat) throws IOException {
		Preconditions.checkNotNull(encodedTriples);
		Preconditions.checkNotNull(schemaTriples);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputName));
		Preconditions.checkArgument(rdfFormat.equals(RdfFormat.N_TRIPLES), "The only supported output format is n-triples.");

		filepath = Utils.getFilepath(outputFolder, outputName, RdfUtils.getFileExtensionByRDFFormat(rdfFormat));
		writer = new PrintWriter(new FileWriter(filepath));
		for (EncodedTriple encodedTriple : encodedTriples)
			writer.printf("%s %s %s .\n", encodedTriple.getSubject(), encodedTriple.getProperty(), encodedTriple.getObject());

		for (Triple triple : schemaTriples)
			writer.printf("%s %s %s .\n", triple.getSubject(), triple.getProperty(), triple.getObject());

		writer.close();
		log.info(encodedTriples.size() + schemaTriples.size() + " triples written to file: " + filepath);

		return filepath;
	}

	/**
	 * Writes the specified RDFS triples to an N-triples file
	 * @throws IOException 
	 */
	public void writeSchemaToNTriplesFile(Set<fr.inria.oak.commons.reasoning.rdfs.saturation.Triple> rdfsTriples, String outputFolder, String outputName) throws IOException {
		Preconditions.checkNotNull(rdfsTriples);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputName));

		filepath = Utils.getFilepath(outputFolder, outputName, "nt");
		writer = new PrintWriter(new FileWriter(filepath));

		for (fr.inria.oak.commons.reasoning.rdfs.saturation.Triple triple : rdfsTriples)
			writer.printf("%s %s %s . \n", ((IRI) triple.subject).toString(), ((IRI) triple.property).toString(), ((IRI) triple.object).toString());

		writer.close();
		log.info("N-triples schema written to file: " + filepath);	
	}

	/**
	 * Initializes the writer
	 * @throws IOException 
	 */
	@Override
	public void initializeWriter(final String filepath) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(filepath));
		writer = new PrintWriter(new FileWriter(filepath));
	}

	/**
	 * Closes the writer
	 */
	@Override
	public void closeWriter() {
		this.writer.close();
	}

	/**
	 * Writes the triple to a file.
	 * The writer has to be already initialized
	 */
	@Override
	public void writeTriple(Triple triple) {
		Preconditions.checkNotNull(triple);
		Preconditions.checkNotNull(writer);
		writer.printf("%s %s %s . \n", triple.getSubject(), triple.getProperty(), triple.getObject());
	}	

	/**
	 * Writes the triple to a file.
	 * The writer has to be already initialized
	 */
	@Override
	public void writeTriple(String subject, String property, String object) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(subject));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(property));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(object));
		Preconditions.checkNotNull(writer);
		writer.printf("%s %s %s . \n", subject, property, object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeDotWriter() {
		this.dotWriter.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeRdfWriter() {
		this.rdfWriter.close();
	}

	@Override
	public void writeStatsToFile(final String outputFolder, final String outputName, final StorageLayout storageLayout, 
			final Stats stats, final TriplesLoadingTimes triplesLoadingTimes, final EncodingTimes encodedTriplesLoadingTimes) throws IOException {
		log.info("Writing stats to file...");
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFolder));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputName));
		Preconditions.checkNotNull(storageLayout);
		Preconditions.checkNotNull(stats);
		Preconditions.checkNotNull(triplesLoadingTimes);
		Preconditions.checkNotNull(encodedTriplesLoadingTimes);

		filepath = Utils.getFilepath(outputFolder, outputName, TXT_EXTENSION);
		writer = new PrintWriter(new FileWriter(filepath));

		writer.printf("\nSchema triples: %s", stats.getSchemaTripleCount());
		writer.printf("\nInput data triples triples: %s", stats.getInputDataTripleCount());
		writer.printf("\nInput type triples triples: %s", stats.getInputTypeTripleCount());
		writer.printf("\nInput non-schema triples: %s", stats.getInputNonSchemaTripleCount());
		writer.printf("\nSummary data triples: %s", stats.getSummaryDataTripleCount());
		writer.printf("\nSummary type triples: %s", stats.getSummaryTypeTripleCount());
		writer.printf("\nSummary non-schema triples: %s", stats.getSummaryNonSchemaTripleCount());
		writer.printf("\nTotal summary nodes: %s", stats.getTotalSummaryNodeCount());
		writer.printf("\nClass nodes: %s", stats.getSummaryClassNodeCount());
		writer.printf("\nData nodes: %s", stats.getSummaryDataNodeCount());
		writer.printf("\nProperty nodes: %s", stats.getPropertyNodeCount());

		writer.printf("\nData and types loading time: %s", triplesLoadingTimes.getDataAndTypesLoadingTime());
		writer.printf("\nSchema loading time: %s", triplesLoadingTimes.getSchemaLoadingTime());
		writer.printf("\nData encoding time: %s", encodedTriplesLoadingTimes.getDataAndTypesEncodingTime());
		writer.printf("\nSchema encoding time: %s", encodedTriplesLoadingTimes.getSchemaEncodingTime());
		writer.printf("\nEncoded schema table creation time: %s", encodedTriplesLoadingTimes.getCreateEncodedSchemaTableTime());
		
		writer.printf("\nSummarization time: %s ms", stats.getSummarizationTime());

		writer.close();
		log.info("Stats written to file: " + filepath);	
	}
}
