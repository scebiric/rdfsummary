package fr.inria.oak.RDFSummary.summary.trove.builder;

import java.sql.SQLException;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.summary.trove.StrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypedStrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeBasedSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypedWeakSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakSummary;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.trove.objectcreator.TroveObjectCreator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Implements {@link TroveSummaryBuilder}.
 * 
 * Integer summary representation.
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PsqlJdbcBuilderImpl implements TroveSummaryBuilder {

	private final static Logger log = Logger.getLogger(TroveSummaryBuilder.class);
	private ResultSet result;
	private String[] columns = null;

	private static RdfTablesDao RdfTablesDao;
	private static DmlDao DmlDao;
	private static TroveObjectCreator ObjectCreator;
	private static TroveCliqueBuilder CliqueBuilder;

	/**
	 * {@link PsqlJdbcBuilderImpl}
	 * 
	 * @param rdfTablesDao
	 * @param dmlDao
	 * @param objectCreator
	 * @param cliqueBuilder
	 */
	public PsqlJdbcBuilderImpl(final RdfTablesDao rdfTablesDao, final DmlDao dmlDao, final TroveObjectCreator objectCreator, final TroveCliqueBuilder cliqueBuilder) {
		RdfTablesDao = rdfTablesDao;
		DmlDao = dmlDao;
		ObjectCreator = objectCreator;
		CliqueBuilder = cliqueBuilder;
	}

	@Override
	public void summarizeDataTriples(final WeakSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!(summary instanceof TypeBasedSummary));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		result = DmlDao.getSelectionResultSet(columns, false, dataTable, null);
		log.info("Representing data triples...");
		while (result.next()) {			
			ObjectCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}

	@Override
	public void summarizeDataTriples(final StrongSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!(summary instanceof TypedStrongSummary));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		CliqueBuilder.buildSourceCliques(dataTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, summary);

		summary.clearSourceCliques();
		summary.clearTargetCliques();

		log.info("Representing distinct data nodes from " + dataTable + "...");
		final String schemaName = NameUtils.getSchemaName(dataTable);
		final String encodedDistinctNodesTable = NameUtils.qualify(schemaName, Constants.ENCODED_DISTINCT_NODES_TABLE);
		if (!DmlDao.existsTable(encodedDistinctNodesTable)) {
			RdfTablesDao.createAndPopulateEncodedDistinctNodesTable(encodedDistinctNodesTable, dataTable);
		}

		result = DmlDao.getSelectionResultSet(new String[] { Character.toString(Chars.STAR) }, false, encodedDistinctNodesTable, null);
		while (result.next()) {
			summary.representInputDataNode(result.getInt(1));
		}
		result.close();

		summary.clearCliqueMaps();

		log.info("Representing data triples...");
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		result = DmlDao.getSelectionResultSet(columns, false, dataTable, null);
		while (result.next()) {
			ObjectCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}

	@Override
	public void summarizeTypeTriples(final WeakSummary summary, final String typesTable) throws SQLException, BerkeleyDbException {	
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!(summary instanceof TypeBasedSummary));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));

		log.info("Retrieving type triples...");
		columns = new String[] { Constants.SUBJECT, Constants.OBJECT }; 		
		log.info("Representing type triples...");
		int subject = -1;
		int classIRI = -1;
		TIntSet typedOnlyDataNodes = new TIntHashSet();
		TIntSet classesOfTypedOnlyDataNodes = new TIntHashSet();
		result = DmlDao.getSelectionResultSet(columns, false, typesTable, null);
		while (result.next()) {			
			subject = result.getInt(1);
			classIRI = result.getInt(2);
			boolean represented = ObjectCreator.representTypeTriple(summary, subject, classIRI);
			if (!represented) {
				// The subject is a data node and it has only type properties
				typedOnlyDataNodes.add(subject);
				classesOfTypedOnlyDataNodes.add(classIRI);
			}
		}
		result.close();

		if (typedOnlyDataNodes.size() > 0) {
			log.info("Representing typed-only resources...");
			ObjectCreator.representTypeTriples(summary, typedOnlyDataNodes, classesOfTypedOnlyDataNodes);
		} 
	}

	/**
	 * Same as for the weak summary.
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void summarizeTypeTriples(final StrongSummary summary, final String typesTable) throws SQLException, BerkeleyDbException {	
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!(summary instanceof TypeBasedSummary));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));

		log.info("Retrieving type triples...");
		columns = new String[] { Constants.SUBJECT, Constants.OBJECT }; 		
		log.info("Representing type triples...");
		int subject = -1;
		int classIRI = -1;
		TIntSet typedOnlyDataNodes = new TIntHashSet();
		TIntSet classesOfTypedOnlyDataNodes = new TIntHashSet();
		result = DmlDao.getSelectionResultSet(columns, false, typesTable, null);
		while (result.next()) {			
			subject = result.getInt(1);
			classIRI = result.getInt(2);
			boolean represented = ObjectCreator.representTypeTriple(summary, subject, classIRI);
			if (!represented) {
				// The subject is a data node and it has only type properties
				typedOnlyDataNodes.add(subject);
				classesOfTypedOnlyDataNodes.add(classIRI);
			}
		}
		result.close();

		if (typedOnlyDataNodes.size() > 0) {
			log.info("Representing typed-only resources...");
			ObjectCreator.representTypeTriples(summary, typedOnlyDataNodes, classesOfTypedOnlyDataNodes);
		} 
	}

	@Override
	public void summarizeTypeTriplesInTypePrimeSummary(final TypeBasedSummary summary, final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	

		log.info("Retrieving type triples...");
		columns = new String[] { Constants.SUBJECT, Constants.OBJECT };
		result = DmlDao.getSelectionResultSet(columns, false, typesTable, Constants.SUBJECT);
		boolean hasResults = result.next();
		if (hasResults) {		
			log.info("Representing type triples...");
			int subject = result.getInt(1);
			TIntSet classes = new TIntHashSet();
			classes.add(result.getInt(2));							
			while (result.next()) {					
				if (result.getInt(1) == subject) 					
					classes.add(result.getInt(2));
				else {							
					ObjectCreator.representTypeTriples(summary, subject, classes);

					// New subject
					subject = result.getInt(1);
					classes = new TIntHashSet();
					classes.add(result.getInt(2));						
				}				
			}

			if (classes.size() > 0) 
				ObjectCreator.representTypeTriples(summary, subject, classes);
		}
		else 
			log.info("No classes found.");
		
		result.close();
	}

	@Override
	public void summarizeDataTriples(final TypedWeakSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		result = DmlDao.getSelectionResultSet(columns, false, dataTable, null);
		log.info("Representing data triples...");
		while (result.next()) {			
			ObjectCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}

	@Override
	public void summarizeDataTriples(final TypedStrongSummary summary, final String dataTable, final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));

		CliqueBuilder.buildSourceCliques(dataTable, typesTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, typesTable, summary);

		summary.clearSourceCliques();
		summary.clearTargetCliques();

		log.info("Representing distinct data nodes from " + dataTable + "...");
		final String schemaName = NameUtils.getSchemaName(dataTable);
		final String encodedDistinctNodesTable = NameUtils.qualify(schemaName, Constants.ENCODED_DISTINCT_NODES_TABLE);
		if (!DmlDao.existsTable(encodedDistinctNodesTable)) {
			RdfTablesDao.createAndPopulateEncodedDistinctNodesTable(encodedDistinctNodesTable, dataTable);
		}

		result = DmlDao.getSelectionResultSet(new String[] { Character.toString(Chars.STAR) }, false, encodedDistinctNodesTable, null);
		while (result.next()) {
			summary.representInputDataNode(result.getInt(1));
		}
		result.close();

		summary.clearCliqueMaps();

		log.info("Representing data triples...");
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		result = DmlDao.getSelectionResultSet(columns, false, dataTable, null);
		while (result.next()) {
			ObjectCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}
}
