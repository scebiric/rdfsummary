package fr.inria.oak.RDFSummary.data.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.Lists;

import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.data.saturation.Saturator;
import fr.inria.oak.RDFSummary.data.saturation.TriplesBatch;
import fr.inria.oak.RDFSummary.rdf.EncodedTriple;
import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.commons.db.Dictionary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SaturatorDaoImpl extends DaoImpl implements SaturatorDao {

	private static final Logger log = Logger.getLogger(SaturatorDao.class);
	private static Saturator Saturator;
	private static RdfTablesQueries RdfTablesQueries;
	private static DmlQueries DmlQueries;
	
	private PreparedStatement insertTripleStatement = null; 
	private String[] columns = null;
	
	/**
	 * Constructor of {@link SaturatorDaoImpl}
	 * 
	 * @param saturator
	 * @param connHandler
	 * @param rdfTablesQueries
	 * @param dmlQueries
	 * @param timer
	 * @throws SQLException 
	 */
	public SaturatorDaoImpl(final Saturator saturator, final SqlConnectionHandler connHandler, 
			final RdfTablesQueries rdfTablesQueries, final DmlQueries dmlQueries, final Timer timer) throws SQLException {
		super(connHandler, 0, timer);
		Saturator = saturator;
		RdfTablesQueries = rdfTablesQueries;
		DmlQueries = dmlQueries;
	}
	
	@Override
	public void releaseResources() throws SQLException {
		super.closeStatement();
		if (insertTripleStatement != null) 
			insertTripleStatement.close();
	}
	
	/**
	 * Creates the auxiliary saturated triples table
	 * @throws SQLException 
	 */
	public void createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable) throws SQLException {
		log.info("Creating table " + auxiliarySaturatedTriplesTable + "...");
		Timer.reset();
		query = RdfTablesQueries.createAuxiliarySaturatedTriplesTable(auxiliarySaturatedTriplesTable, triplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(auxiliarySaturatedTriplesTable + " table created in " + Timer.getTimeAsString());
	}

	/**
	 * Creates the saturated triples table
	 * @throws SQLException 
	 */
	public void createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable) throws SQLException {
		Timer.reset();
		query = RdfTablesQueries.createSaturatedTriplesTable(saturatedTriplesTable, auxiliarySaturatedTriplesTable);
		log.info(query);
		Timer.start();
		statement.executeUpdate(query);
		Timer.stop();

		log.info(saturatedTriplesTable + " table created in " + Timer.getTimeAsString());
	}

	/**
	 * Runs batch saturation
	 * @throws SQLException 
	 */
	public void runBatchSaturation(String triplesTable, String auxiliarySaturatedTriplesTable, 
			Collection<Schema> encodedSchemas, int batchSize, String rdfTypeIRI, Dictionary dictionary) throws SQLException {
		log.info("Running batch saturation...");
		Timer.reset();
		log.info("Retrieving triples from the " + triplesTable + " table...");
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		query = DmlQueries.select(columns, false, triplesTable, null);
		log.info(query);

		Timer.start();
		Statement statement = connHandler.createStatement(batchSize, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		final TriplesBatch batch = new TriplesBatch(rdfTypeIRI);	
		connHandler.disableAutoCommit();
		Set<Triple> saturatedTriples = null;
		insertTripleStatement = connHandler.prepareStatement(RdfTablesQueries.insertTriple(auxiliarySaturatedTriplesTable));
		while (resultSet.next()) {
			batch.add(new Triple(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3)));
			if (batch.size() == batchSize) {
				log.info("Saturating a batch of triples...");
				saturatedTriples = Saturator.saturate(encodedSchemas, batch, rdfTypeIRI, dictionary);
				insert(saturatedTriples);
				batch.clear();
			}
		}

		if (!batch.isEmpty()) {
			log.info("Saturating a batch of triples (smaller than the batch size)...");
			saturatedTriples = Saturator.saturate(encodedSchemas, batch, rdfTypeIRI, dictionary);
			insert(saturatedTriples);
			batch.clear();
		}
		insertTripleStatement.close();
		connHandler.enableAutoCommit();
		Timer.stop();

		log.info("Batch saturation done in " + Timer.getTimeAsString());
	}

	/**
	 * Retrieves encoded triples from the specified table
	 * @throws SQLException 
	 */
	@Override
	public List<EncodedTriple> getEncodedTriples(String tableName, Dictionary dictionary, int fetchSize) throws SQLException {
		log.info("Loading encoded triples from " + tableName + " table...");
		List<EncodedTriple> triples = Lists.newArrayList();
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		query = DmlQueries.select(columns, false, tableName, null);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			String subject = fr.inria.oak.RDFSummary.util.DictionaryUtils.getValue(dictionary, resultSet.getInt(1));
			String property = fr.inria.oak.RDFSummary.util.DictionaryUtils.getValue(dictionary, resultSet.getInt(2));
			String object = fr.inria.oak.RDFSummary.util.DictionaryUtils.getValue(dictionary, resultSet.getInt(3));
			triples.add(new EncodedTriple(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), subject, property, object));
		}

		log.info("Loaded " + triples.size() + " triples from " + tableName + " table.");

		return triples;
	}
	
	/**
	 * Inserts the triples to the auxiliary saturated triples table
	 * 
	 * @param saturatedTriples
	 * @throws SQLException 
	 */
	private void insert(Set<Triple> saturatedTriples) throws SQLException {
		for (Triple triple : saturatedTriples) {
			insertTripleStatement.setInt(1, Integer.parseInt(triple.getSubject()));
			insertTripleStatement.setInt(2, Integer.parseInt(triple.getProperty()));
			insertTripleStatement.setInt(3, Integer.parseInt(triple.getObject()));
			insertTripleStatement.executeUpdate();
		}
	}
	
	/**
	 * Retrieves (non-encoded) triples from the specified table
	 * @throws SQLException 
	 */
	@Override
	public List<Triple> getTriples(String tableName, int fetchSize) throws SQLException {
		log.info("Loading triples from " + tableName + " table...");
		List<Triple> triples = Lists.newArrayList();
		columns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
		query = DmlQueries.select(columns, false, tableName, null);
		log.info(query);
		final java.sql.ResultSet resultSet = statement.executeQuery(query);
		while (resultSet.next()) {
			triples.add(new Triple(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3)));
		}

		log.info("Loaded " + triples.size() + " triples from " + tableName + " table.");

		return triples;
	}
}
