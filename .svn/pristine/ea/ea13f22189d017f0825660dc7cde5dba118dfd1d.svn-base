package fr.inria.oak.RDFSummary.summary.trove.objectcreator;

import java.sql.SQLException;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.summary.trove.StrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.SummaryException;
import fr.inria.oak.RDFSummary.summary.trove.TypeBasedSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypedWeakSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakSummary;
import gnu.trove.set.TIntSet;

/**
 * Implements {@link TroveObjectCreator}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveObjectCreatorImpl implements TroveObjectCreator {

	@Override
	public void representDataTriple(final WeakSummary summary, final int subject, final int property, final int object) throws SQLException, SummaryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(subject > 0);
		Preconditions.checkArgument(property > 0);
		Preconditions.checkArgument(object > 0);

		int source = subject;
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);

		int target = object;
		if (!summary.isClassOrProperty(object))
			target = this.getTargetDataNode(summary, object, property);

		// In the case of paths, the getTarget method may change the source and vice-versa
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);
		if (!summary.isClassOrProperty(object))
			target = this.getTargetDataNode(summary, object, property);

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}

	/**
	 * Obtains a data node representing the subject and the property source.
	 * 
	 * There should be only one data node source per property. For this reason it does not suffice to 
	 * just keep track of data nodes and the resources they represent, but also of property sources - 
	 * if the two differ unification occurs. So for example, s1 p1 o1, s2 p1 o2, s1 and s2 should be represented
	 * by the same source in the summary.
	 * 
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the property source 
	 * @throws SQLException 
	 * @throws SummaryException 
	 */
	public int getSourceDataNode(final WeakSummary summary, final int subject, final int dataProperty) throws SQLException, SummaryException {
		int untypedPropertySource = summary.getSourceDataNodeForDataProperty(dataProperty);		
		int subjectNode = summary.getSummaryDataNodeForInputDataNode(subject);

		if (untypedPropertySource > 0 && subjectNode > 0) {
			if (untypedPropertySource == subjectNode)
				return subjectNode;
			else 
				return summary.unifyDataNodes(untypedPropertySource, subjectNode);				
		}

		if (untypedPropertySource > 0 && subjectNode == 0) {
			summary.representInputDataNode(subject, untypedPropertySource);
			return untypedPropertySource;
		}

		if (untypedPropertySource == 0 && subjectNode > 0) {
			return subjectNode;
		}

		// untypedPropertySource == 0 && subjectNode == 0
		return summary.createDataNode(subject);
	}

	/**
	 * Obtains a data node representing the subject and the property target.
	 * 
	 * There should be only one data node target per property. For this reason it does not suffice to 
	 * just keep track of data nodes and the resources they represent, but also of property targets - 
	 * if the two differ unification occurs. So for example, s1 p1 o1, s2 p1 o2, o1 and o2 should be represented
	 * by the same target in the summary.
	 * 
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the subject and the property target
	 * @throws SQLException 
	 * @throws SummaryException 
	 */
	public int getTargetDataNode(final WeakSummary summary, final int object, final int dataProperty) throws SQLException, SummaryException {
		int untypedPropertyTarget = summary.getTargetDataNodeForDataProperty(dataProperty);
		int objectNode = summary.getSummaryDataNodeForInputDataNode(object);

		if (untypedPropertyTarget > 0 && objectNode > 0) {
			if (untypedPropertyTarget == objectNode)
				return objectNode;
			else 
				return summary.unifyDataNodes(untypedPropertyTarget, objectNode);
		}

		if (untypedPropertyTarget > 0 && objectNode == 0) {
			summary.representInputDataNode(object, untypedPropertyTarget);
			return untypedPropertyTarget;
		}

		if (untypedPropertyTarget == 0 && objectNode > 0) {
			return objectNode;
		}

		// untypedPropertyTarget == 0 && objectNode == 0
		return summary.createDataNode(object);
	}

	/**
	 * Returns true if the type triple has been represented in the summary, otherwise false.
	 * 
	 * Assumes that data triples have already been summarized.
	 * Therefore, if gNode is a data node and has some data properties in G, then it has already been represented by 
	 * a data node in the summary when summarizing data triples.
	 * Otherwise, gNode doesn't have any data properties in G,
	 * i.e. it is typed-only, and will be represented later, thus the method returns false. 
	 * 
	 * If the type triple from G should be represented,
	 * a corresponding type triple is created in the summary, with the representative of gNode
	 * as the source and the specified class IRI as the target.
	 * If gNode is a class or a property, it is represented by itself.
	 *  
	 * The method ensures that the summary type triples are unique.
	 * 
	 * Same as for the strong summary. 
	 *     
	 * @throws SummaryException 
	 */
	@Override
	public boolean representTypeTriple(final WeakSummary summary, final int gNode, final int classIRI) throws SummaryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkArgument(classIRI > 0);

		if (summary.isClassOrProperty(gNode)) {
			if (!summary.existsTypeTriple(gNode, classIRI))
				summary.createTypeTriple(gNode, classIRI);

			return true;
		}
		else {
			int sDataNode = summary.getSummaryDataNodeForInputDataNode(gNode);
			if (sDataNode == 0) 
				return false;

			if (!summary.existsTypeTriple(sDataNode, classIRI)) 
				summary.createTypeTriple(sDataNode, classIRI);

			return true;
		}
	}

	/**
	 * A single data node is created, representing all gDataNodes and having all the specified classes.
	 * @throws SQLException 
	 * @throws SummaryException 
	 */
	@Override
	public void representTypeTriples(final WeakSummary summary, final TIntSet gDataNodes, final TIntSet classes) throws SQLException, SummaryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		Preconditions.checkArgument(gDataNodes.size() > 0);

		int sDataNode = summary.createDataNode(gDataNodes, classes);
		// Uniqueness: Since the data node is newly created and the classes in the specified set are unique there couldn't have
		// already existed a type edge to any given class
		summary.createTypeTriples(sDataNode, classes);	
	}

	/**
	 * Retrieves the representative data nodes of the subject and of the object.
	 * Creates a data triple between the representative nodes, with the specified property, if it doesn't already exist in the summary.
	 * @throws SummaryException 
	 */
	@Override
	public void representDataTriple(final StrongSummary summary, final int subject, final int property, final int object) throws SummaryException {
		Preconditions.checkNotNull(summary);		

		int source = subject;
		if (!summary.isClassOrProperty(subject))
			source = summary.getSummaryDataNodeForInputDataNode(subject);

		int target = object;
		if (!summary.isClassOrProperty(object))
			target = summary.getSummaryDataNodeForInputDataNode(object);		

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}

	/**
	 * Same as for the weak summary.
	 * @throws SummaryException 
	 */
	@Override
	public boolean representTypeTriple(final StrongSummary summary, final int gNode, final int classIRI) throws SummaryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkArgument(classIRI > 0);

		if (summary.isClassOrProperty(gNode)) {
			if (!summary.existsTypeTriple(gNode, classIRI))
				summary.createTypeTriple(gNode, classIRI);

			return true;
		}
		else {
			int sDataNode = summary.getSummaryDataNodeForInputDataNode(gNode);
			if (sDataNode == 0) 
				return false;

			if (!summary.existsTypeTriple(sDataNode, classIRI)) 
				summary.createTypeTriple(sDataNode, classIRI);

			return true;
		}
	}

	/**
	 * Same as for the weak summary.
	 * @throws SummaryException 
	 */
	@Override
	public void representTypeTriples(final StrongSummary summary, final TIntSet gDataNodes, final TIntSet classes) throws SQLException, SummaryException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		Preconditions.checkArgument(gDataNodes.size() > 0);

		int sDataNode = summary.createDataNode(gDataNodes, classes);
		// Uniqueness: Since the data node is newly created and the classes in the specified set are unique there couldn't have
		// already existed a type edge to any given class
		summary.createTypeTriples(sDataNode, classes);	
	}

	/**
	 * If gNode is a class or property node the type triple is simply created.
	 * The maps ensures that the summary type triples are unique.
	 * 
	 * If gNode is a data node, the method first tries to retrieve a data node "representing" the specified class set 
	 * (i.e. having all the classes in the class set). 
	 * 
	 * If this node already exists, it is assigned as the representative data node of the specified subject.
	 * Otherwise, a new data node is created, representing the subject and the class set; type triples
	 * are created in the summary between the new data node and all the classes in the class set.
	 * 
	 * @throws SummaryException 
	 * @throws SQLException 
	 */
	@Override
	public void representTypeTriples(final TypeBasedSummary summary, final int gNode, final TIntSet classes) throws SQLException, SummaryException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);

		if (summary.isClassOrProperty(gNode)) 
			summary.createTypeTriples(gNode, classes);
		else {
			int sDataNode = summary.getSummaryDataNodeForClasses(classes);
			if (sDataNode == 0) {
				sDataNode = summary.createDataNode(gNode, classes);
				summary.createTypeTriples(sDataNode, classes);
			}
			else 
				summary.representInputDataNode(gNode, sDataNode);

			summary.setHighestTypedDataNode(sDataNode);
		}
	}

	@Override
	public void representDataTriple(final TypedWeakSummary summary, final int subject, final int property, final int object) throws SQLException, SummaryException {
		Preconditions.checkNotNull(summary);

		int source = subject;
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);

		int target = object;
		if (!summary.isClassOrProperty(object)) 
			target = this.getTargetDataNode(summary, object, property);		

		// In the case of paths, the getTarget method may change the source and vice-versa
		if (!summary.isClassOrProperty(subject))
			source = this.getSourceDataNode(summary, subject, property);

		if (!summary.isClassOrProperty(object))
			target = this.getTargetDataNode(summary, object, property);

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}

	/**
	 * Obtains a data node representing the subject and the property source.
	 * If the subject is typed, this will be the data node with the same class set as the subject.
	 * If the subject is untyped, this will be the untyped data node representing the untyped source of the specified property.
	 * 
	 * There should be only one untyped data node source per property. For this reason it does not suffice to 
	 * just keep track of data nodes and the resources they represent, but also of untyped property sources - 
	 * if the two differ unification occurs. So for example, s1 p1 o1, s2 p1 o2, s1 and s2 should be represented
	 * by the same source in the summary.
	 * 
	 * Similar for common targets.
	 * 
	 * @param summary
	 * @param subject
	 * @param dataProperty
	 * @return The data node representing the subject and the property source
	 * @throws SummaryException 
	 * @throws SQLException 
	 */
	public int getSourceDataNode(final TypedWeakSummary summary, final int subject, final int dataProperty) throws SQLException, SummaryException {
		int untypedPropertySource = summary.getSourceDataNodeForDataProperty(dataProperty);
		// If the subject node is null, the subject is untyped (otherwise it would have been assigned a data node when representing type triples)
		int subjectNode = summary.getSummaryDataNodeForInputDataNode(subject);

		if (untypedPropertySource > 0 && subjectNode > 0) {
			if (summary.isTypedDataNode(subjectNode) || untypedPropertySource == subjectNode)
				return subjectNode;
			else  
				return summary.unifyDataNodes(untypedPropertySource, subjectNode);				
		}

		if (untypedPropertySource > 0 && subjectNode == 0) {
			summary.representInputDataNode(subject, untypedPropertySource);
			return untypedPropertySource;
		}

		if (untypedPropertySource == 0 && subjectNode > 0) {
			return subjectNode;
		}

		// untypedPropertySource == 0 && subjectNode == 0
		return summary.createDataNode(subject);
	}

	/**
	 * Obtains a data node representing the object and the property target.
	 * If the object is typed, this will be the data node with the same class set as the object.
	 * If the object is untyped, this will be the untyped data node representing the untyped target of the specified property.
	 * There is only one untyped data node target per property.
	 * 
	 * @param summary
	 * @param object
	 * @param dataProperty
	 * @return The data node representing the object and the property target
	 * @throws SummaryException 
	 * @throws SQLException 
	 */
	public int getTargetDataNode(final TypedWeakSummary summary, final int object, final int dataProperty) throws SQLException, SummaryException {
		int untypedPropertyTarget = summary.getTargetDataNodeForDataProperty(dataProperty);
		// If the object node is null, the object is untyped (otherwise it would have been assigned a data node when representing type triples)
		int objectNode = summary.getSummaryDataNodeForInputDataNode(object);

		if (untypedPropertyTarget > 0 && objectNode > 0) {
			if (summary.isTypedDataNode(objectNode) || untypedPropertyTarget == objectNode)
				return objectNode;
			else if (untypedPropertyTarget != objectNode)
				return summary.unifyDataNodes(untypedPropertyTarget, objectNode);
		}

		if (untypedPropertyTarget > 0 && objectNode == 0) {
			summary.representInputDataNode(object, untypedPropertyTarget);
			return untypedPropertyTarget;
		}

		if (untypedPropertyTarget == 0 && objectNode > 0) {
			return objectNode;
		}

		// untypedPropertyTarget == 0 && objectNode == 0
		return summary.createDataNode(object);
	}
}
