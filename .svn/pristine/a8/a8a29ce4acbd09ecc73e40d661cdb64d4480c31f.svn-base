package fr.inria.oak.RDFSummary.data.storage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * Storing (unencoded) data to Postgres
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface PsqlStorage {
	
	/**
	 * Invokes methods for loading the data, type and schema triples from files to Postgres tables.
	 * 
	 * @param schemaName
	 * @param dropSchema
	 * @param inputTriplesTable
	 * @param dataTriplesFilepath
	 * @param schemaTriplesFilepath
	 * @param indexing
	 * @return {@link TriplesLoadingResult}
	 * @throws JenaException
	 * @throws SQLException
	 * @throws IOException
	 * @throws PsqlStorageException
	 * @throws SchemaException
	 * @throws UnsupportedDatabaseEngineException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 */
	public TriplesLoadingResult loadTriplesFromFilesToTables(final String schemaName, final boolean dropSchema, final String inputTriplesTable, final String dataTriplesFilepath,
			final String schemaTriplesFilepath, final boolean indexing) throws JenaException, SQLException, IOException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException;
	
	/** 
	 * Loads the data and type triples from a file to a triples table.
	 * 
	 * @param dropSchema
	 * @param schemaName
	 * @param qualifiedTriplesTableName
	 * @param dataTriplesFilepath
	 * @return The loading time.
	 * @throws JenaException
	 * @throws SQLException
	 * @throws IOException
	 * @throws PsqlStorageException
	 */
	public String loadDataAndTypesFromFileToTriplesTable(boolean dropSchema, String schemaName, String qualifiedTriplesTableName, String dataTriplesFilepath) throws JenaException, SQLException, IOException, PsqlStorageException;
	
	/**
	 * Loads schema triples from a file to a table.
	 * 
	 * @param schemaTable
	 * @param schemaTriplesFilepath
	 * @return Schema loading time.
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws SchemaException
	 */
	public Schema loadSchemaFromFileToSchemaTriplesTable(String schemaTable, String schemaTriplesFilepath) throws SQLException, FileNotFoundException, SchemaException;
}
