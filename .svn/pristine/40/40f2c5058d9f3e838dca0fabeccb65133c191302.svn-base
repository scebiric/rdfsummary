package fr.inria.oak.RDFSummary.data.saturation.encoded.rules;

import java.sql.SQLException;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.data.saturation.encoded.EncodedSchema;
import fr.inria.oak.RDFSummary.data.saturation.encoded.EncodedTriple;
import fr.inria.oak.RDFSummary.util.Utils;
import gnu.trove.set.TIntSet;

/**
 * Saturates a Triple with respect to the property ranges defined in the
 * schemas.
 *
 * @link http://www.w3.org/TR/rdf-schema/#ch_range
 *
 *       Premise: (_, P, *) & Schema has (P, range, C)
 *
 *       Produces: ($1, type, C)
 *
 * @author Sejla CEBIRIC
 *
 */
public class RuleRange extends RuleBase implements Rule {

	private TIntSet ranges;

	public RuleRange() {
	}

	/**
	 * ($1, type, C)
	 * @throws SQLException 
	 */
	@Override
	public Set<EncodedTriple> produce(final EncodedTriple triple, final EncodedSchema schema) throws SQLException {
		Preconditions.checkNotNull(triple);
		Preconditions.checkNotNull(schema);

		final Set<EncodedTriple> triples = Sets.newLinkedHashSet();
		if (Utils.getKey(Rdfs.FULL_RANGE) != null) { 
			triples.addAll(produceTypeTriples(triple, schema));
		}

		if (Utils.getKey(Rdfs.RANGE) != null) {
			triples.addAll(produceTypeTriples(triple, schema));
		}

		return triples;
	}

	private Set<EncodedTriple> produceTypeTriples(final EncodedTriple triple, final EncodedSchema schema) throws SQLException {
		final Set<EncodedTriple> triples = Sets.newLinkedHashSet();
		
		ranges = schema.getRangesForProperty(triple.getProperty());
		if (ranges != null) {
			iterator = ranges.iterator();
			while (iterator.hasNext()) {
				final int range = iterator.next();
				EncodedTriple produced = new EncodedTriple(triple.getObject(), Utils.getRdfTypeKey(), range);
				triples.add(produced);
			}
		}
		
		return triples;
	}
}
