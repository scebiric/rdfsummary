package fr.inria.oak.RDFSummary.summary.trove.export;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.DotStyleParams;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.Columns;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.storage.StorageLayout;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.summary.Exporter;
import fr.inria.oak.RDFSummary.summary.stats.Stats;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class TroveSummaryExporterImpl implements TroveSummaryExporter {

	private static final Logger log = Logger.getLogger(Exporter.class);

	private static DotSummaryViz DotSummaryViz;
	private static TriplesWriter TriplesWriter;
	private final SqlConnectionHandler connHandler;
	private final RdfTablesDao rdfTablesDao;
	private final DictionaryDao dictDao;
	private static DdlDao DdlDao;
	private static DmlDao DmlDao;
	private static DmlQueries DmlQueries;
	private static Timer Timer;

	private ResultSet resultSet = null;	

	/**
	 * Constructor of {@link TroveSummaryExporterImpl}
	 * 
	 * @param dotSummaryViz
	 * @param triplesWriter
	 * @param connHandler
	 * @param rdfTablesDao
	 * @param dictDao
	 * @param ddlDao
	 * @param dmldao
	 * @param dmlQueries
	 * @param timer
	 */
	public TroveSummaryExporterImpl(final DotSummaryViz dotSummaryViz, final TriplesWriter triplesWriter,
			final SqlConnectionHandler connHandler, final RdfTablesDao rdfTablesDao, final DictionaryDao dictDao, final DdlDao ddlDao, final DmlDao dmldao,
			final DmlQueries dmlQueries, final Timer timer) {
		DotSummaryViz = dotSummaryViz;
		TriplesWriter = triplesWriter;
		this.connHandler = connHandler;
		this.rdfTablesDao = rdfTablesDao;
		this.dictDao = dictDao;
		DdlDao = ddlDao;
		DmlDao = dmldao;
		DmlQueries = dmlQueries;
		Timer = timer;
	}

	@Override
	public void exportRdfSummaryToPostgres(final RdfSummary summary, final SummarizerParams params, final StorageLayout storageLayout) throws SQLException, DictionaryException, InexistentValueException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(summary instanceof RdfSummary);
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(storageLayout);

		log.info("Exporting summary to PostgreSQL...");
		Timer.reset();
		Timer.start();
		final String outputPrefix = NameUtils.getOutputPrefix(storageLayout.getEncodedTriplesTable(), params.getSummarizationParams().getSummaryType());
		final String summaryEncodedDataTable = this.createEmptyEncodedDataTable(outputPrefix, storageLayout);
		final String summaryEncodedTypesTable = this.createEmptyEncodedTypesTable(outputPrefix, storageLayout);

		String encReprTable = null;
		if (params.getExportParams().exportRepresentationTables()) {
			encReprTable = this.createEmptyEncodedRepresentationTable(outputPrefix);
			this.insertEncodedRepresentationMappings(encReprTable, Columns.ENC_REPRESENTATION_TABLE, summary);
		}

		connHandler.disableAutoCommit();
		this.insertEncodedSummaryTypeTriples(summaryEncodedTypesTable, summary, params);
		this.insertEncodedSummaryDataTriples(summaryEncodedDataTable, summary, params);
		this.exportAllEncodedSummaryTriples(outputPrefix, summaryEncodedDataTable, summaryEncodedTypesTable);
		connHandler.enableAutoCommit();

		this.decodeSummaryTriples(outputPrefix, params.getSummarizationParams().getDictionaryTable(), storageLayout);
		if (params.getExportParams().exportRepresentationTables())
			this.decodeRepresentationTable(encReprTable, outputPrefix, params.getSummarizationParams().getDictionaryTable());

		Timer.stop();

		log.info("Summary exported to PostgreSQL in " + Timer.getTimeAsString());
	}

	/**
	 * @param encReprTable
	 * @param reprColumns
	 * @param summary
	 * @throws SQLException
	 */
	private void insertEncodedRepresentationMappings(final String encReprTable, final String[] reprColumns, final RdfSummary summary) throws SQLException {
		log.info("Inserting representation mappings...");
		final PreparedStatement insertPrepStatement = connHandler.prepareStatement(DmlQueries.insertValuesPreparedStatement(encReprTable, reprColumns));
		final TIntSet sDataNodes = summary.getSummaryDataNodes();
		TIntSet representedNodes = null;
		int sNode = -1;
		final TIntIterator iterator = sDataNodes.iterator();
		while (iterator.hasNext()) {
			sNode = iterator.next();
			representedNodes = summary.getRepresentedInputDataNodes(sNode);
			rdfTablesDao.insertToEncodedRepresentationTable(insertPrepStatement, encReprTable, reprColumns, representedNodes, sNode);
		}
		
		insertPrepStatement.close();
	}

	private String createEmptyEncodedDataTable(final String outputPrefix, final StorageLayout storageLayout) throws SQLException {
		log.info("Creating empty encoded data table...");
		final String summaryEncodedDataTable = NameUtils.getEncodedSummaryDataTableName(outputPrefix); 
		DdlDao.dropTableIfExists(summaryEncodedDataTable);
		rdfTablesDao.createEncodedTriplesTableWithId(summaryEncodedDataTable);
		storageLayout.setEncodedSummaryDataTable(summaryEncodedDataTable);

		return summaryEncodedDataTable;
	}
	
	private String createEmptyEncodedTypesTable(final String outputPrefix, final StorageLayout storageLayout) throws SQLException {
		log.info("Creating empty encoded types table...");
		final String summaryEncodedTypesTable = NameUtils.getEncodedSummaryTypesTableName(outputPrefix);
		DdlDao.dropTableIfExists(summaryEncodedTypesTable);
		rdfTablesDao.createEncodedTypesTableWithId(summaryEncodedTypesTable);
		storageLayout.setEncodedSummaryTypesTable(summaryEncodedTypesTable);
		
		return summaryEncodedTypesTable;
	}

	private String createEmptyEncodedRepresentationTable(final String outputPrefix) throws SQLException {
		log.info("Creating empty encoded representation table...");
		final String encReprTable = NameUtils.getEncodedRepresentationTableName(outputPrefix); 
		DdlDao.dropTableIfExists(encReprTable);
		rdfTablesDao.createEncodedRepresentationTable(encReprTable, Constants.INPUT_DATA_NODE, Constants.SUMMARY_DATA_NODE);
		
		return encReprTable;
	}

	/**
	 * @param summaryEncodedTypesTable
	 * @param summary
	 * @param params
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws SQLException
	 * @throws BerkeleyDbException
	 */
	private void insertEncodedSummaryTypeTriples(final String summaryEncodedTypesTable, final RdfSummary summary, final SummarizerParams params) throws DictionaryException, InexistentValueException, SQLException, BerkeleyDbException {
		log.info("Inserting encoded summary type triples...");	
		TIntIterator classesIterator = null;
		TIntSet classes = null;
		int[] values = new int[2];

		TIntObjectMap<TIntSet> classesForSummaryNodeMap = summary.getClassesForSummaryNodeMap();
		for (int sNode : classesForSummaryNodeMap.keys()) {
			classes = classesForSummaryNodeMap.get(sNode);
			if (classes != null) {
				classesIterator = classes.iterator();
				while (classesIterator.hasNext()) {
					values[0] = sNode;
					values[1] = classesIterator.next();
					DmlDao.insert(summaryEncodedTypesTable, Columns.TYPE_TRIPLES, values);
				}
			}
		}
	}

	/**
	 * @param summaryEncodedDataTable 
	 * @param summary
	 * @param params
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws SQLException
	 */
	private void insertEncodedSummaryDataTriples(final String summaryEncodedDataTable, final RdfSummary summary, final SummarizerParams params) throws DictionaryException, InexistentValueException, SQLException {
		log.info("Inserting encoded summary data triples...");
		TIntObjectMap<Collection<DataTriple>> dataTriplesForPropertyMap = summary.getDataTriplesForDataPropertyMap();
		Collection<DataTriple> dataTriples = null;
		int[] tripleAtoms = new int[3];
		for (int dataProperty : dataTriplesForPropertyMap.keys()) {
			dataTriples = dataTriplesForPropertyMap.get(dataProperty);
			for (DataTriple dataTriple : dataTriples) {
				tripleAtoms[0] = dataTriple.getSubject();
				tripleAtoms[1] = dataTriple.getDataProperty();
				tripleAtoms[2] = dataTriple.getObject();
				DmlDao.insert(summaryEncodedDataTable, Columns.DATA_TRIPLES, tripleAtoms); 
			}
		}
	}
	
	/**
	 * Creates a single table comprising both data and type encoded summary triples
	 * 
	 * @param outputPrefix
	 * @param summaryEncodedDataTable
	 * @param summaryEncodedTypesTable
	 * @throws SQLException 
	 */
	private void exportAllEncodedSummaryTriples(final String outputPrefix, final String summaryEncodedDataTable, final String summaryEncodedTypesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryEncodedDataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryEncodedTypesTable));

		final String encodedSummary = NameUtils.getEncodedSummaryTableName(outputPrefix);
		DdlDao.dropTableIfExists(encodedSummary);
		rdfTablesDao.createEncodedTriplesTableWithId(encodedSummary);

		final int typePropertyKey = dictDao.getExistingOrNewKey(Rdf.FULL_TYPE);
		rdfTablesDao.populateEncodedSummaryTable(encodedSummary, summaryEncodedDataTable, summaryEncodedTypesTable, typePropertyKey);
	}
	
	/**
	 * Recreates a decoded representation table based on the encoded one and the dictionary table
	 * 
	 * @param encReprTable
	 * @param outputPrefix
	 * @param datasetDictionaryTable
	 * @throws SQLException
	 */
	private void decodeRepresentationTable(final String encReprTable, final String outputPrefix, final String datasetDictionaryTable) throws SQLException {
		String reprTable = NameUtils.getDecodedRepresentationTableName(outputPrefix);
		DdlDao.dropTableIfExists(reprTable);
		rdfTablesDao.createDecodedRepresentationTable(reprTable, encReprTable, datasetDictionaryTable);
	}

	/**
	 * Decodes the encoded summary data and types tables by joining them with the dictionary table in Postgres.
	 * The decoded tables' names are then saved in the storage layout.
	 * Before decoding, if the decoded tables already exist they will be dropped.
	 * 
	 * @param outputPrefix
	 * @param datasetDictionaryTable
	 * @param summaryType 
	 * @param storageLayout
	 * @throws SQLException
	 */
	private void decodeSummaryTriples(final String outputPrefix, final String datasetDictionaryTable, final StorageLayout storageLayout) throws SQLException {
		log.info("Decoding summary data and type triples in PostgreSQL...");
		final String summaryDataTable = NameUtils.underscoreConcat(outputPrefix, Constants.DATA);
		final String summaryTypesTable = NameUtils.underscoreConcat(outputPrefix, Constants.TYPES);
		String summaryTable = outputPrefix;

		DdlDao.dropTableIfExists(summaryDataTable);
		DdlDao.dropTableIfExists(summaryTypesTable);
		DdlDao.dropTableIfExists(summaryTable);
		rdfTablesDao.decodeSummaryData(summaryDataTable, storageLayout.getEncodedSummaryDataTable(), datasetDictionaryTable);
		rdfTablesDao.decodeSummaryTypes(summaryTypesTable, storageLayout.getEncodedSummaryTypesTable(), datasetDictionaryTable);
		rdfTablesDao.createDecodedSummaryTable(summaryTable, summaryDataTable, summaryTypesTable);

		storageLayout.setSummaryDataTable(summaryDataTable);
		storageLayout.setSummaryTypesTable(summaryTypesTable);
	}

	/**
	 * Encodes each summary data node.
	 * Loads decoded summary triples from PostgreSQL and writes them to a .dot file and/or n-triples file.
	 * Only one triple from PostgreSQL is loaded to memory at a time.
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws DictionaryException 
	 * @throws InexistentValueException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void exportDecodedTriplesToFiles(final SummarizerParams params, final RdfSummary summary, final Schema schema, 
			final Stats stats, final String outputFilename, final StorageLayout storageLayout) throws ConfigurationException, ParametersException, SQLException, IOException, DictionaryException, InexistentValueException, BerkeleyDbException {
		Preconditions.checkNotNull(params);
		Preconditions.checkNotNull(stats);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(outputFilename));
		Preconditions.checkArgument(params.getExportParams().generateDot() || params.getExportParams().exportToRdfFile());

		Timer.reset();
		Timer.start();

		log.info("Exporting decoded summary triples to files...");
		String dotFilepath = null;
		DotStyleParams dotParams = null;
		if (params.getExportParams().generateDot()) {
			dotParams = params.getExportParams().getDotStyleParams();		

			// Start DOT file
			String graphLabel = "";
			if (dotParams.showGraphLabel()) 
				graphLabel = DotSummaryViz.getGraphLabel(params.getSummarizationParams().getSchemaName(), stats);

			dotFilepath = DotSummaryViz.startDotGraph(params.getExportParams().getOutputFolder(), outputFilename, graphLabel);
		}

		String rdfFilepath = null;
		if (params.getExportParams().exportToRdfFile()) {
			// Start n-triples file
			rdfFilepath = TriplesWriter.startFile(params.getExportParams().getOutputFolder(), outputFilename, RdfFormat.N_TRIPLES);
		}

		// Write nodes
		Dictionary schemaDictionary = null;
		int dataNode = -1;
		if (params.getExportParams().generateDot()) {
			// Data nodes
			TIntSet dataNodes = summary.getSummaryDataNodes();
			TIntIterator dataNodesIterator = dataNodes.iterator();
			while (dataNodesIterator.hasNext()) {	 
				dataNode = dataNodesIterator.next();
				DotSummaryViz.writeToFile(getDotNodeEntry(dataNode, getDotNodeLabel(dataNode), dotParams, summary));
			}

			if (schema != null) {
				schemaDictionary = dictDao.loadDictionaryForConstants(schema.getConstants());
				// Schema property nodes
				int propertyNodeKey = -1;
				for (String propertyNode : schema.getAllProperties()) {
					propertyNodeKey = schemaDictionary.getKey(propertyNode);
					if (!summary.isProperty(propertyNodeKey))
						continue;
					
					DotSummaryViz.writeToFile(getDotNodeEntry(propertyNodeKey, getDotNodeLabel(propertyNodeKey), dotParams, summary));
				}

				// Schema class nodes
				int classNodeKey = -1;
				for (String classIRI : schema.getAllClasses()) {
					classNodeKey = schemaDictionary.getKey(classIRI);
					if (!summary.isClass(classNodeKey))
						continue;
					
					DotSummaryViz.writeToFile(getDotNodeEntry(classNodeKey, getDotNodeLabel(classNodeKey), dotParams, summary));			
				}
			}

			// Class nodes from type component
			int classNode = -1;
			resultSet = rdfTablesDao.distinctClassesFromTypeComponent(storageLayout.getEncodedSummaryTypesTable());
			while (resultSet.next()) {
				classNode = resultSet.getInt(1);
				DotSummaryViz.writeToFile(getDotNodeEntry(classNode, getDotNodeLabel(classNode), dotParams, summary));
			}			
			resultSet.close();

		}

		// Write data triples
		int source = -1;
		int target = -1;		
		String edgeLabel = null;
		String edgeColor = DotSummaryViz.getEdgeColor(dotParams.getDataPropertyEdgeColor());
		String edgeStyle = dotParams.getDataPropertyEdgeStyle();
		resultSet = DmlDao.getSelectionResultSet(Columns.DATA_TRIPLES, false, storageLayout.getEncodedSummaryDataTable(), null);
		while (resultSet.next()) {
			if (params.getExportParams().generateDot()) {
				source = resultSet.getInt(1);
				target = resultSet.getInt(3);
				// Data nodes have already been written to dot. Properties and classes which appear in both data and schema will be duplicated.
				if (summary.isClassOrProperty(source))
					DotSummaryViz.writeToFile(getDotNodeEntry(source, getDotNodeLabel(source), dotParams, summary));
				if (summary.isClassOrProperty(target))
					DotSummaryViz.writeToFile(getDotNodeEntry(target, getDotNodeLabel(target), dotParams, summary));

				edgeLabel = DotSummaryViz.getEdgeLabel(getEdgeLabel(resultSet.getInt(2)));
				DotSummaryViz.writeToFile(DotSummaryViz.getDotTriple(getDotNodeId(source),  getDotNodeId(target), edgeColor, edgeStyle, edgeLabel));
			}

			if (params.getExportParams().exportToRdfFile()) {
				TriplesWriter.write(dictDao.getValue(resultSet.getInt(1)), dictDao.getValue(resultSet.getInt(2)), dictDao.getValue(resultSet.getInt(3)));
			}	
		}
		resultSet.close();

		// Write type triples
		edgeColor = DotSummaryViz.getEdgeColor(dotParams.getTypeEdgeColor());
		edgeStyle = dotParams.getTypeEdgeStyle();
		edgeLabel = "\"\"";
		resultSet = DmlDao.getSelectionResultSet(Columns.TYPE_TRIPLES, false, storageLayout.getEncodedSummaryTypesTable(), null);
		while (resultSet.next()) {
			if (params.getExportParams().generateDot()) {
				source = resultSet.getInt(1);
				target = resultSet.getInt(2);
				DotSummaryViz.writeToFile(DotSummaryViz.getDotTriple(getDotNodeId(source), getDotNodeId(target), edgeColor, edgeStyle, edgeLabel));
			}

			if (params.getExportParams().exportToRdfFile()) {
				TriplesWriter.write(dictDao.getValue(resultSet.getInt(1)), Rdf.FULL_TYPE, dictDao.getValue(resultSet.getInt(2)));
			}	
		}
		resultSet.close();

		// Write schema triples
		edgeColor = DotSummaryViz.getEdgeColor(dotParams.getSchemaEdgeColor());
		if (schema != null) {
			for (Triple schemaTriple : schema.getAllTriples()) {
				if (params.getExportParams().generateDot()) {
					source = schemaDictionary.getKey(schemaTriple.subject.toString());
					target = schemaDictionary.getKey(schemaTriple.object.toString());
					DotSummaryViz.writeToFile(DotSummaryViz.getSchemaTriple(getDotNodeId(source), getDotNodeId(target), schemaTriple, edgeColor, dotParams.getSchemaLabelColor()));
				}

				if (params.getExportParams().exportToRdfFile())
					TriplesWriter.write(schemaTriple.subject.toString(), schemaTriple.property.toString(), schemaTriple.object.toString());
			}
		}

		if (params.getExportParams().generateDot()) 
			DotSummaryViz.endDotGraph();

		if (params.getExportParams().exportToRdfFile())
			TriplesWriter.endFile();

		Timer.stop();
		if (params.getExportParams().generateDot() || params.getExportParams().exportToRdfFile())
			log.info("Decoded summary triples exported to file(s) in " + Timer.getTimeAsString());
		if (params.getExportParams().generateDot())
			log.info("Summary DOT filepath: " + dotFilepath);
		if (params.getExportParams().exportToRdfFile())
			log.info("Summary RDF filepath: " + rdfFilepath);

		if (params.getExportParams().generateDot() && params.getExportParams().convertDotToPdf())
			Utils.dotToPdf(dotFilepath, params.getExportParams().getDotExecutableFilepath());

		if (params.getExportParams().generateDot() && params.getExportParams().convertDotToPng())
			Utils.dotToPng(dotFilepath, params.getExportParams().getDotExecutableFilepath());
	}
	
	/**
	 * @param nodeKey
	 * @return The nodeKey prefixed with dot node prefix
	 */
	private String getDotNodeId(int nodeKey) {
		return NameUtils.addDotNodePrefix(nodeKey);
	}

	/**
	 * @param propertyKey
	 * @return The decoded value of propertyKey, stripped of its namespace if it has one.
	 * @throws SQLException
	 */
	private String getEdgeLabel(int propertyKey) throws SQLException {
		return RdfUtils.stripNamespace(dictDao.getValue(propertyKey));
	}

	/**
	 * @param nodeKey
	 * @return The decoded value of nodeKey, stripped of its namespace if it has one.
	 * @throws SQLException
	 * @throws BerkeleyDbException
	 */
	private String getDotNodeLabel(int nodeKey) throws SQLException, BerkeleyDbException {
		return RdfUtils.stripNamespace(dictDao.getValue(nodeKey));
	}

	/**
	 * @param nodeId
	 * @param label
	 * @param dotParams
	 * @param summary
	 * @return Dot entry for the node based on whether it is a data, class, property or class and property node.
	 * @throws BerkeleyDbException
	 */
	private String getDotNodeEntry(int nodeId, String label, DotStyleParams dotParams, RdfSummary summary) throws BerkeleyDbException {
		if (!summary.isClassOrProperty(nodeId)) {
			if (!label.startsWith("d")) {
				log.info(label);
				System.exit(0);
			}
			// Data node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getDataNodeShape(), dotParams.getDataNodeStyle(), dotParams.getDataNodeColor());
		}
		else if (summary.isClass(nodeId) && !summary.isProperty(nodeId)) {
			// Class node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getClassNodeShape(), dotParams.getClassNodeStyle(), dotParams.getClassNodeColor());
		}
		else if (summary.isProperty(nodeId) && !summary.isClass(nodeId)) {
			// Property node
			return DotSummaryViz.getDotNode(nodeId, label, dotParams.getPropertyNodeShape(), dotParams.getPropertyNodeStyle(), dotParams.getPropertyNodeColor());
		}

		// Class and property node
		return DotSummaryViz.getDotNode(nodeId, label, dotParams.getClassAndPropertyNodeShape(), dotParams.getClassAndPropertyNodeStyle(), dotParams.getClassAndPropertyNodeColor()); 
	}
}
