package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongTypeEquivalenceSummaryImpl implements StrongTypeEquivalenceSummary {

	private final StrongEquivalenceSummary strongSummary;
	private final TypeEquivalenceSummary typeEquivSummary;
	private final RdfSummary rdfSummary;

	/**
	 * @param strongEquivalenceSummary
	 * @param typeEquivalenceSummary
	 * @param rdfSummary
	 */
	public StrongTypeEquivalenceSummaryImpl(final StrongEquivalenceSummary strongEquivalenceSummary,
			final TypeEquivalenceSummary typeEquivalenceSummary,
			final RdfSummary rdfSummary) {
		this.strongSummary = strongEquivalenceSummary;
		this.typeEquivSummary = typeEquivalenceSummary;
		this.rdfSummary = rdfSummary;
	}

	@Override
	public List<TIntSet> getSourceCliques() {
		return strongSummary.getSourceCliques();
	}

	@Override
	public List<TIntSet> getTargetCliques() {
		return strongSummary.getTargetCliques();
	}

	@Override
	public int getSourceCliqueIdBySubject(int subject) {
		return strongSummary.getSourceCliqueIdBySubject(subject);
	}

	@Override
	public int getTargetCliqueIdByObject(int object) {
		return strongSummary.getTargetCliqueIdByObject(object);
	}

	@Override
	public void setSourceCliqueIdForSubject(int subject, int sourceClique) {
		strongSummary.setSourceCliqueIdForSubject(subject, sourceClique);
	}

	@Override
	public void setTargetCliqueIdForObject(int object, int targetClique) {
		strongSummary.setTargetCliqueIdForObject(object, targetClique);
	}

	@Override
	public TIntSet getDataNodesForSourceCliqueId(int sourceCliqueId) {
		return strongSummary.getDataNodesForSourceCliqueId(sourceCliqueId);
	}

	@Override
	public TIntSet getDataNodesForTargetCliqueId(int targetCliqueId) {
		return strongSummary.getDataNodesForTargetCliqueId(targetCliqueId);
	}

	@Override
	public void storeDataNodeForSourceCliqueId(int sourceCliqueId, int dataNode) throws BerkeleyDbException {
		strongSummary.storeDataNodeForSourceCliqueId(sourceCliqueId, dataNode);
	}

	@Override
	public void storeDataNodeForTargetCliqueId(int targetCliqueId, int dataNode) throws BerkeleyDbException {
		strongSummary.storeDataNodeForTargetCliqueId(targetCliqueId, dataNode);
	}

	@Override
	public void clearSourceCliques() {
		strongSummary.clearSourceCliques();
	}

	@Override
	public void clearTargetCliques() {
		strongSummary.clearTargetCliques();
	}

	@Override
	public void clearCliqueMaps() {
		strongSummary.clearCliqueMaps();
	}
	
	@Override
	public boolean isTyped(int sNode) {
		return rdfSummary.isTyped(sNode);
	}

	@Override
	public boolean isTypedDataNode(int sDataNode) throws BerkeleyDbException {
		return typeEquivSummary.isTypedDataNode(sDataNode);
	}

	@Override
	public void setHighestTypedDataNode(int sNode) throws BerkeleyDbException {
		typeEquivSummary.setHighestTypedDataNode(sNode);
	}

	@Override
	public int getSummaryDataNodeForClasses(TIntSet classes) {
		return typeEquivSummary.getSummaryDataNodeForClasses(classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		rdfSummary.createTypeTriples(sNode, classes);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrProperty(int iri) throws BerkeleyDbException {
		return rdfSummary.isClassOrProperty(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) throws BerkeleyDbException {
		return rdfSummary.createDataTriple(source, dataProperty, target);
	}
	
	@Override
	public void storeSummaryDataNodeByClassSet(TIntSet classes, int sDataNode) throws BerkeleyDbException {
		typeEquivSummary.storeSummaryDataNodeByClassSet(classes, sDataNode);
	}
}
