<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>fr.inria.oak</groupId>
	<artifactId>RDFSummary</artifactId>
	<version>0.0.12</version>
	<packaging>jar</packaging>

	<name>RDFSummary</name>
	<description>Summarizing RDF graphs</description>
	<inceptionYear>2014</inceptionYear>
	<organization>
		<name>INRIA - team OAK</name>
		<url>https://team.inria.fr/oak/</url>
	</organization>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<jdk.version>1.8</jdk.version>
	</properties>

	<!-- Build -->
	<build>
		<finalName>RDFSummary</finalName>
		<!-- Plugins -->
		<plugins>

			<!-- Download source code in Eclipse, best practice -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>false</downloadJavadocs>
				</configuration>
			</plugin>

			<!-- Set a compiler level -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>${jdk.version}</source>
					<target>${jdk.version}</target>
					<fork>true</fork>
					<executable>/Library/Java/JavaVirtualMachines/jdk1.8.0_65.jdk/Contents/Home/bin/javac</executable>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<version>2.4.3</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<configuration>
							<transformers>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
									<mainClass>fr.inria.oak.RDFSummary.main.Summarizer</mainClass>
								</transformer>
							</transformers>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<!-- Repositories -->
	<repositories>
		<repository>
			<id>INRIA</id>
			<name>http://maven.inria.fr-releases</name>
			<url>http://maven.inria.fr/artifactory/oakcommons-private-release</url>
		</repository>
	</repositories>

	<!-- Dependencies -->
	<dependencies>

		<!-- http://mvnrepository.com/artifact/com.sleepycat/je -->
		<dependency>
			<groupId>com.sleepycat</groupId>
			<artifactId>je</artifactId>
			<version>5.0.73</version>
		</dependency>

		<!-- commons-relationalschema -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-relationalschema</artifactId>
			<version>0.0.5</version>
		</dependency>

		<!-- commons-translation -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-translation</artifactId>
			<version>0.0.4</version>
		</dependency>

		<!-- commons-conjunctivequery -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-conjunctivequery</artifactId>
			<version>0.0.9</version>
		</dependency>

		<!-- commons-dictionary -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-dictionary</artifactId>
			<version>0.0.7</version>
		</dependency>

		<!-- commons-reasoning -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-reasoning</artifactId>
			<version>0.0.4</version>
			<exclusions>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- commons-rdfdb -->
		<dependency>
			<groupId>fr.inria.oak.commons</groupId>
			<artifactId>commons-rdfdb</artifactId>
			<version>0.0.4</version>
			<exclusions>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.13</version>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>2.4.1</version>
		</dependency>

		<!-- Apache Commons Configuration -->
		<dependency>
			<groupId>commons-configuration</groupId>
			<artifactId>commons-configuration</artifactId>
			<version>1.10</version>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Apache Commons Logging -->
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.2</version>
		</dependency>

		<!-- Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
			<version>4.2.4.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>4.2.4.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>4.2.4.RELEASE</version>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>4.2.4.RELEASE</version>
		</dependency>

		<!-- Postgres JDBC -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>9.3-1102-jdbc41</version>
		</dependency>

		<!-- Google Core Libraries for Java -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>18.0</version>
		</dependency>

		<!-- JUnit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>

		<!-- Mockito -->
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.9.5</version>
		</dependency>

		<!-- Apache POI -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>3.15</version>
		</dependency>

		<!-- Apache POI OOXML -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>3.11</version>
		</dependency>

		<!-- Apache Commons IO -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>

		<!-- JCommander -->
		<dependency>
			<groupId>com.beust</groupId>
			<artifactId>jcommander</artifactId>
			<version>1.47</version>
		</dependency>

		<!-- Jena -->
		<dependency>
			<groupId>com.hp.hpl.jena</groupId>
			<artifactId>jena</artifactId>
			<version>2.6.4</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Trove -->
		<dependency>
			<groupId>net.sf.trove4j</groupId>
			<artifactId>trove4j</artifactId>
			<version>3.0.3</version>
		</dependency>
	</dependencies>
</project>
