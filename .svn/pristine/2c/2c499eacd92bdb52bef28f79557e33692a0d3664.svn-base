package fr.inria.oak.RDFSummary.summary.trove.componentsummarizer;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.trove.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.TroveTripleCreator;
import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTroveRdfComponentSummarizerImpl implements TroveRdfComponentSummarizer {

	private final static Logger log = Logger.getLogger(TroveRdfComponentSummarizer.class);
	private ResultSet result;
	private String[] dataColumns = new String[] { Constants.SUBJECT, Constants.PROPERTY, Constants.OBJECT };
	private String[] typeColumns = new String[] { Constants.SUBJECT, Constants.OBJECT };
	
	private static DmlDao DmlDao;
	private static RdfTablesDao RdfTablesDao;
	private static TroveTripleCreator TripleCreator;
	private static TroveNodeRepresenter NodeRepresenter;
	private static TroveCliqueBuilder CliqueBuilder;
	
	/**
	 * @param dmlDao
	 * @param rdfTablesDao
	 * @param tripleCreator
	 * @param nodeRepresenter
	 * @param cliqueBuilder
	 */
	public PostgresTroveRdfComponentSummarizerImpl(final DmlDao dmlDao, final RdfTablesDao rdfTablesDao,
			final TroveTripleCreator tripleCreator, final TroveNodeRepresenter nodeRepresenter,
			final TroveCliqueBuilder cliqueBuilder) {
		DmlDao = dmlDao;
		RdfTablesDao = rdfTablesDao;
		TripleCreator = tripleCreator;
		NodeRepresenter = nodeRepresenter;
		CliqueBuilder = cliqueBuilder;
	}

	@Override
	public void summarizeDataTriples(final WeakPureCliqueEquivalenceSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		result = getDataTriples(dataTable);
		log.info("Representing data triples...");
		while (result.next()) {			
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}
	
	@Override
	public void summarizeDataTriples(final WeakTypeEquivalenceSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		log.info("Retrieving data triples...");
		result = getDataTriples(dataTable);
		log.info("Representing data triples...");
		while (result.next()) {			
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}

	@Override
	public void summarizeDataTriples(final StrongPureCliqueEquivalenceSummary summary, final String dataTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));

		CliqueBuilder.buildSourceCliques(dataTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, summary);
		summary.clearSourceCliques();
		summary.clearTargetCliques();
		
		representDistinctDataNodesFromInput(dataTable, summary);
		summary.clearCliqueMaps();

		representDataTriples(dataTable, summary);
	}

	@Override
	public void summarizeDataTriples(final StrongTypeEquivalenceSummary summary, final String dataTable, final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		
		CliqueBuilder.buildSourceCliques(dataTable, typesTable, summary);
		CliqueBuilder.buildTargetCliques(dataTable, typesTable, summary);
		
		summary.clearSourceCliques();
		summary.clearTargetCliques();
		
		representDistinctDataNodesFromInput(dataTable, summary);
		summary.clearCliqueMaps();

		representDataTriples(dataTable, summary);
	}
	
	private void representDataTriples(final String dataTable, final StrongEquivalenceSummary summary) throws SQLException, BerkeleyDbException {
		log.info("Representing data triples...");
		result = getDataTriples(dataTable);
		while (result.next()) {
			TripleCreator.representDataTriple(summary, result.getInt(1), result.getInt(2), result.getInt(3));
		}
		result.close();
	}
	
	private void representDistinctDataNodesFromInput(final String dataTable, final StrongEquivalenceSummary summary) throws SQLException, BerkeleyDbException {
		log.info("Representing distinct data nodes from " + dataTable + "...");
		final String schemaName = NameUtils.getSchemaName(dataTable);
		final String encodedDistinctNodesTable = NameUtils.qualify(schemaName, Constants.ENCODED_DISTINCT_NODES_TABLE);
		if (!DmlDao.existsTable(encodedDistinctNodesTable)) {
			RdfTablesDao.createAndPopulateEncodedDistinctNodesTable(encodedDistinctNodesTable, dataTable);
		}

		result = DmlDao.getSelectionResultSet(new String[] { Character.toString(Chars.STAR) }, false, encodedDistinctNodesTable, null);
		while (result.next()) {
			NodeRepresenter.representInputDataNode(summary, result.getInt(1));
		}
		result.close();
	}

	@Override
	public void summarizeTypeTriples(final PureCliqueEquivalenceSummary summary, final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));

		log.info("Retrieving type triples...");
		result = getTypeTriples(typesTable); 		
		log.info("Representing type triples...");
		int subject = -1;
		int classIRI = -1;
		TIntSet typedOnlyDataNodes = new TIntHashSet();
		TIntSet classesOfTypedOnlyDataNodes = new TIntHashSet();
		while (result.next()) {			
			subject = result.getInt(1);
			classIRI = result.getInt(2);
			boolean represented = TripleCreator.representTypeTriple(summary, subject, classIRI);
			if (!represented) {
				// The subject is a data node and it has only type properties
				typedOnlyDataNodes.add(subject);
				classesOfTypedOnlyDataNodes.add(classIRI);
			}
		}
		result.close();

		if (typedOnlyDataNodes.size() > 0) {
			log.info("Representing typed-only resources...");
			TripleCreator.representTypeTriples(summary, typedOnlyDataNodes, classesOfTypedOnlyDataNodes);
		} 
	}

	@Override
	public void summarizeTypeTriples(final TypeEquivalenceSummary summary, final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	

		log.info("Retrieving type triples...");
		result = getTypeTriples(typesTable);
		boolean hasResults = result.next();
		if (hasResults) {		
			log.info("Representing type triples...");
			int subject = result.getInt(1);
			TIntSet classes = new TIntHashSet();
			classes.add(result.getInt(2));							
			while (result.next()) {					
				if (result.getInt(1) == subject) 					
					classes.add(result.getInt(2));
				else {							
					TripleCreator.representTypeTriples(summary, subject, classes);

					// New subject
					subject = result.getInt(1);
					classes = new TIntHashSet();
					classes.add(result.getInt(2));						
				}				
			}

			if (classes.size() > 0) 
				TripleCreator.representTypeTriples(summary, subject, classes);
		}
		else 
			log.info("No classes found.");
		
		result.close();
	}

	private ResultSet getDataTriples(final String dataTable) throws SQLException {
		return DmlDao.getSelectionResultSet(dataColumns, false, dataTable, null);
	}
	
	private ResultSet getTypeTriples(final String typesTable) throws SQLException {
		return DmlDao.getSelectionResultSet(typeColumns, false, typesTable, Constants.SUBJECT);
	}
}
