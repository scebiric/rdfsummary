package fr.inria.oak.RDFSummary.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import fr.inria.oak.RDFSummary.rdf.manager.JenaRdfManager;
import fr.inria.oak.RDFSummary.rdfschema.CustomJenaSchemaParser;
import fr.inria.oak.RDFSummary.rdfschema.JenaSchemaParser;
import fr.inria.oak.RDFSummary.timer.Timer;
import fr.inria.oak.RDFSummary.timer.TimerImpl;
import fr.inria.oak.RDFSummary.util.SchemaUtils;
import fr.inria.oak.RDFSummary.writer.WriterImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;

/**
 * Bean definitions for summarization 
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
public class SummaryBeanConfig {

	@Autowired
	protected Environment env;
	
	@Bean
	public WriterImpl writerImpl() {
		return new WriterImpl();
	}
	
	@Bean fr.inria.oak.RDFSummary.summary.trove.objectcreator.TroveObjectCreator troveObjectCreator() {
		return new fr.inria.oak.RDFSummary.summary.trove.objectcreator.TroveObjectCreatorImplTest();
	}

	@Bean
	public JenaSchemaParser jenaSchemaParser() {
		return new JenaSchemaParser();
	}

	@Bean
	public CustomJenaSchemaParser customJenaSchemaParser() {
		return new CustomJenaSchemaParser();
	}

	@Bean
	public JenaRdfManager jenaRdfManager() {
		return new JenaRdfManager();
	}

	@Bean
	public SchemaUtils schemaUtils() {
		return new SchemaUtils();
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Timer timerImpl() {
		return new TimerImpl();
	}
}
