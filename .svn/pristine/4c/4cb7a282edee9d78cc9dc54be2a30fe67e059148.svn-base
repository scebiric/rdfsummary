package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

public class PureCliqueEquivalenceSummaryImplTest {

	private UriGenerator uriGenerator;
	private DictionaryDao dictDao;
	private BerkeleyDbHandler bdbHandler;

	private String classAndPropertyDb = "testDb";

	@Before
	public void setUp() {
		uriGenerator = mock(UriGenerator.class);
		dictDao = mock(DictionaryDao.class);
		bdbHandler = mock(BerkeleyDbHandler.class);
	}

	@Test
	public void createDataNode_eachGDataNodeRepresentedBySDataNodeAndSDataNodeReturned() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode1 = 1;
		int gDataNode2 = 2;
		TIntSet gDataNodes = new TIntHashSet();
		gDataNodes.add(gDataNode1);
		gDataNodes.add(gDataNode2);
		int sDataNode = 3;
		String uri = "d" + sDataNode;
		when(dictDao.getHighestKey()).thenReturn(gDataNode2);
		when(uriGenerator.generateDataNodeUri(sDataNode)).thenReturn(uri);
		when(dictDao.getExistingOrNewKey(uri)).thenReturn(sDataNode);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		PureCliqueEquivalenceSummary summary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);

		// Call
		int representative = summary.createDataNode(gDataNodes);

		// Assert
		assertEquals(sDataNode, representative);
		TIntIterator iterator = gDataNodes.iterator();
		while (iterator.hasNext())
			assertEquals(sDataNode, summary.getSummaryDataNodeForInputDataNode(iterator.next()));
	}

	@Test
	public void createDataNode_representativeOfGDataNodeStoredAsLastCreatedDataNode() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode1 = 1;
		int gDataNode2 = 2;
		TIntSet gDataNodes = new TIntHashSet();
		gDataNodes.add(gDataNode1);
		gDataNodes.add(gDataNode2);
		int sDataNode = 3;
		String uri = "d" + sDataNode;
		when(dictDao.getHighestKey()).thenReturn(gDataNode2);
		when(uriGenerator.generateDataNodeUri(sDataNode)).thenReturn(uri);
		when(dictDao.getExistingOrNewKey(uri)).thenReturn(sDataNode);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		PureCliqueEquivalenceSummary summary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);

		// Call
		int representative = summary.createDataNode(gDataNodes);

		// Assert
		assertEquals(representative, summary.getLastCreatedDataNode());
	}

	@Test(expected=IllegalArgumentException.class)
	public void createDataNode_inputNodeIsAClassOrProperty_exceptionThrown() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode1 = 1;
		int gDataNode2 = 2;
		TIntSet gDataNodes = new TIntHashSet();
		gDataNodes.add(gDataNode1);
		gDataNodes.add(gDataNode2);
		int sDataNode = 3;
		String uri = "d" + sDataNode;
		when(dictDao.getHighestKey()).thenReturn(gDataNode2);
		when(uriGenerator.generateDataNodeUri(sDataNode)).thenReturn(uri);
		when(dictDao.getExistingOrNewKey(uri)).thenReturn(sDataNode);
		when(bdbHandler.get(classAndPropertyDb, gDataNode2, Boolean.class)).thenReturn(true);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		PureCliqueEquivalenceSummary summary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);

		// Call
		summary.createDataNode(gDataNodes);
	}

	@Test
	public void createTypeTriple_sNodeDoesNotHaveTypesYet_sNodeAttachedToClass() {
		// Set up
		int sNode = 1;
		int classIRI = 2;
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		PureCliqueEquivalenceSummary summary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);

		// Call
		summary.createTypeTriple(sNode, classIRI);

		// Assert
		assertTrue(summary.existsTypeTriple(sNode, classIRI));
	}
	
	@Test
	public void createTypeTriple_sNodeDoesHasAnotherType_sNodeAttachedToNewClass() {
		// Set up
		int classIRI1 = 1;
		int classIRI2 = 2;
		int sNode = 3;
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		PureCliqueEquivalenceSummary summary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);
		summary.createTypeTriple(sNode, classIRI1);

		// Call
		summary.createTypeTriple(sNode, classIRI2);

		// Assert
		assertTrue(summary.existsTypeTriple(sNode, classIRI1));
		assertTrue(summary.existsTypeTriple(sNode, classIRI2));
	}

}
