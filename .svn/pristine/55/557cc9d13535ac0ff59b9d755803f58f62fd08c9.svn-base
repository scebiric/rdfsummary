package fr.inria.oak.RDFSummary.data.saturation.encoded;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.saturation.encoded.rules.Rule;
import fr.inria.oak.RDFSummary.data.saturation.encoded.rules.RuleDomain;
import fr.inria.oak.RDFSummary.data.saturation.encoded.rules.RuleRange;
import fr.inria.oak.RDFSummary.data.saturation.encoded.rules.RuleSubClass;
import fr.inria.oak.RDFSummary.data.saturation.encoded.rules.RuleSubProperty;
import fr.inria.oak.commons.db.DictionaryException;

/**
 *
 * @author Sejla CEBIRIC
 *
 */
public class EncodedSaturatorImpl implements EncodedSaturator {
	
	/**
	 * Saturates the triples from the triple batch
	 * @throws SQLException 
	 * @throws DictionaryException 
	 */
	@Override
	public Set<EncodedTriple> saturate(final EncodedSchema schema, final EncodedTriplesBatch batch) throws SQLException, DictionaryException {
		Preconditions.checkNotNull(schema);
		Preconditions.checkNotNull(batch);
		
		final Set<EncodedTriple> saturatedTriples = Sets.newHashSet();
		saturatedTriples.addAll(transitiveSaturationWithRule(schema, batch, batch.getPropertyAssertions(), new RuleSubProperty()));
		saturatedTriples.addAll(saturationWithRule(schema, batch, new RuleDomain()));
		saturatedTriples.addAll(saturationWithRule(schema, batch, new RuleRange()));
		saturatedTriples.addAll(transitiveSaturationWithRule(schema, batch, batch.getClassAssertions(), new RuleSubClass()));
	
		return saturatedTriples;
	}
	
	private Set<EncodedTriple> transitiveSaturationWithRule(final EncodedSchema schema, final EncodedTriplesBatch batch, final Set<EncodedTriple> assertions, final Rule rule) throws SQLException, DictionaryException {
		final Set<EncodedTriple> saturatedTriples = Sets.newHashSet();
		final Set<EncodedTriple> unchecked = Sets.newHashSet();

		for (final EncodedTriple triple : assertions) {
			final Set<EncodedTriple> producedTriples = rule.produce(triple, schema);
			for (final EncodedTriple producedTriple : producedTriples) {
				if (!batch.containsAssertion(producedTriple)) {
					unchecked.add(producedTriple);
				}
			}
		}
		
		Set<EncodedTriple> inProcess = null;
		// Transitive saturation
		while (!unchecked.isEmpty()) {
			saturatedTriples.addAll(unchecked);
			batch.addAll(unchecked);
			inProcess = new HashSet<EncodedTriple>(unchecked);
			unchecked.clear();
			for (final EncodedTriple triple : inProcess) {
				final Set<EncodedTriple> producedTriples = rule.produce(triple, schema);
				for (final EncodedTriple producedTriple : producedTriples) {
					if (!batch.containsAssertion(producedTriple)) {
						unchecked.add(producedTriple);
					}
				}
			}
		}
		
		return saturatedTriples;
	}

	private Set<EncodedTriple> saturationWithRule(EncodedSchema schema, final EncodedTriplesBatch batch, final Rule rule) throws SQLException, DictionaryException {
		final Set<EncodedTriple> saturatedTriples = Sets.newHashSet();
		
		for (final EncodedTriple triple : batch.getPropertyAssertions()) {
			final Set<EncodedTriple> producedTriples = rule.produce(triple, schema);
			for (final EncodedTriple producedTriple : producedTriples) {
				if (!batch.containsClassAssertion(producedTriple)) {
					saturatedTriples.add(producedTriple);
				}
			}
		}
		batch.addAll(saturatedTriples);
		
		return saturatedTriples;
	}
}
