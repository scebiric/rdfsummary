package fr.inria.oak.RDFSummary.summary.stats;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.storage.StorageLayout;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.summary.trove.old.RdfSummary;
import fr.inria.oak.commons.db.DictionaryException;

/**
 * Stats info on the input dataset
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface StatsCollector {
	
	/** Collects statistics 
	 * @throws QueryException 
	 * @throws SQLException 
	 * @throws DictionaryException */
	public Stats collectStats(RdfSummary summary, String datasetName, StorageLayout storageLayout, Schema schema, long summarizationTime) throws DictionaryException, SQLException, QueryException;
	
	/**
	 * @param classIRI
	 * @param dataTable
	 * @param typesTable
	 * @return The number of distinct resources of the specified class appearing in the subject or object position of a data triple.
	 * @throws SQLException
	 * @throws StatisticsException
	 * @throws DictionaryException 
	 */
	public int getClassSupport(String classIRI, String dataTable, String typesTable) throws SQLException, StatisticsException, DictionaryException;
	
	/**
	 * @param dataProperty
	 * @param dataTable
	 * @return The number of data triples having the specified data property.
	 * @throws SQLException
	 * @throws StatisticsException
	 * @throws DictionaryException 
	 */
	public int getDataPropertySupport(final String dataProperty, final String dataTable) throws SQLException, StatisticsException, DictionaryException;
}
