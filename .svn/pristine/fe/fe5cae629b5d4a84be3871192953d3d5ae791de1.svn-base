package fr.inria.oak.RDFSummary.summary.trove.cliquebuilder;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.summary.trove.StrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypePrimeStrongSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypePrimeSummary;
import fr.inria.oak.RDFSummary.util.SetUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * Implements {@link TroveCliqueBuilder}
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveCliqueBuilderImpl implements TroveCliqueBuilder {

	private static final Logger log = Logger.getLogger(TroveCliqueBuilder.class);
	private static RdfTablesDao RdfTablesDao;

	private ResultSet result = null;
	private TIntSet properties = null;

	/**
	 * Constructor of {@link TroveCliqueBuilderImpl}
	 * 
	 * @param rdfTablesDao
	 */
	public TroveCliqueBuilderImpl(final RdfTablesDao rdfTablesDao) {
		RdfTablesDao = rdfTablesDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void buildSourceCliques(final String dataTable, final StrongSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!(summary instanceof TypePrimeSummary));
		
		log.info("Summarizing source cliques...");
		result = RdfTablesDao.distinctSpOrderByS(dataTable);		 
		this.computeSourceCliques(dataTable, summary, result);
		result.close();
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public void buildSourceCliques(final String dataTable, final String typesTable, final TypePrimeStrongSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkNotNull(summary);
		
		log.info("Summarizing source cliques...");
		result = RdfTablesDao.distinctSpOrderBySWithSUntyped(dataTable, typesTable);		 
		this.computeSourceCliques(dataTable, summary, result);
		result.close();
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public void buildTargetCliques(final String dataTable, final StrongSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(!(summary instanceof TypePrimeSummary));
		
		log.info("Summarizing target cliques...");
		result = RdfTablesDao.distinctOpOrderByO(dataTable);
		this.computeTargetCliques(dataTable, summary, result);
		result.close();
	}

	/**
	 * {@inheritDoc}
	 * @throws SQLException 
	 */
	@Override
	public void buildTargetCliques(final String dataTable, final String typesTable, final TypePrimeStrongSummary summary) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkNotNull(summary);
		
		log.info("Summarizing target cliques...");
		result = RdfTablesDao.distinctOpOrderByOWithOUntyped(dataTable, typesTable);
		this.computeTargetCliques(dataTable, summary, result);
		result.close();
	}
	
	/**
	 * @param dataTable
	 * @param summary
	 * @param resultSet
	 * @throws SQLException
	 */
	private void computeSourceCliques(final String dataTable, final StrongSummary summary, final ResultSet result) throws SQLException {
		boolean hasResults = result.next();
		if (!hasResults) {
			log.info(dataTable + " is empty.");
			return;
		}
		int resource = result.getInt(1);
		properties = new TIntHashSet();
		properties.add(result.getInt(2));							
		while (result.next()) {		
			if (result.getInt(1) == resource) 					
				properties.add(result.getInt(2));
			else {	
				this.computeSourceClique(summary, resource, properties);
				resource = result.getInt(1);
				properties = new TIntHashSet();
				properties.add(result.getInt(2));						
			}				
		}

		if (properties.size() > 0) 
			this.computeSourceClique(summary, resource, properties);
	}
	
	/**
	 * @param dataTable
	 * @param summary
	 * @param result
	 * @throws SQLException
	 */
	private void computeTargetCliques(final String dataTable, final StrongSummary summary, final ResultSet result) throws SQLException {
		boolean hasResults = result.next();
		if (!hasResults) {
			log.info(dataTable + " is empty.");
			return;
		}
		int resource = result.getInt(1);
		properties = new TIntHashSet();
		properties.add(result.getInt(2));							
		while (result.next()) {					
			if (result.getInt(1) == resource) 					
				properties.add(result.getInt(2));
			else {	
				this.computeTargetClique(summary, resource, properties);
				resource = result.getInt(1);
				properties = new TIntHashSet();
				properties.add(result.getInt(2));						
			}				
		}

		if (properties.size() > 0) 
			this.computeTargetClique(summary, resource, properties);	
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void computeSourceClique(final StrongSummary summary, final int subject, final TIntSet sProps) {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(sProps);

		List<TIntSet> sourceCliques = summary.getSourceCliques();
		TIntSet sourceClique = null;
		int commonElement = -1;
		for (TIntSet clique : sourceCliques) {
			commonElement = SetUtils.findFirstCommonElement(clique, sProps);
			if (commonElement == -1)
				continue;

			sourceClique = clique;
			break; // There is only one clique with which sProps can intersect and we have found it	
		}

		if (sourceClique == null) {
			sourceClique = new TIntHashSet(sProps);
			sourceCliques.add(sourceClique);
		}
		else
			sourceClique.addAll(sProps);

		summary.setSourceCliqueIdForSubject(subject, sourceCliques.indexOf(sourceClique) + 1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void computeTargetClique(final StrongSummary summary, final int object, final TIntSet oProps) {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(oProps);

		List<TIntSet> targetCliques = summary.getTargetCliques();
		TIntSet targetClique = null;
		int commonElement = -1;
		for (TIntSet clique : targetCliques) {
			commonElement = SetUtils.findFirstCommonElement(clique, oProps);
			if (commonElement == -1)
				continue;

			targetClique = clique;
			break; // There is only one clique with which sProps can intersect and we have found it	
		}

		if (targetClique == null) {
			targetClique = new TIntHashSet(oProps);
			targetCliques.add(targetClique);
		}
		else
			targetClique.addAll(oProps);

		summary.setTargetCliqueIdForObject(object, targetCliques.indexOf(targetClique) + 1);
	}


}
