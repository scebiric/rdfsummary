package fr.inria.oak.RDFSummary.summary.trove;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 *
 * @author Sejla CEBIRIC
 *
 */
public class TypedStrongSummaryImpl extends StrongSummaryImpl implements TypedStrongSummary {

	/** The id of the highest typed node in the summary */
	private int highestTypedDataNode = -1;
	
	/**
	 * @param uriGenerator
	 * @param dictDao
	 * @param bdbHandler
	 * @param classAndPropertyDb
	 */
	public TypedStrongSummaryImpl(final UriGenerator uriGenerator, final DictionaryDao dictDao, final BerkeleyDbHandler bdbHandler, final String classAndPropertyDb) {
		super(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
	}

	/**
	 * In the "type prime summaries" all typed data nodes are created before any untyped nodes,
	 * and their value comes from a sequence starting from 1.
	 * So, a data node is untyped if its value is higher than the highest typed data node.
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public boolean isTypedDataNode(final int sDataNode) throws SummaryException, BerkeleyDbException {
		Preconditions.checkArgument(!isClassOrProperty(sDataNode));
		
		return highestTypedDataNode > 0 && sDataNode <= highestTypedDataNode;
	}
	
	@Override
	public void setHighestTypedDataNode(int sDataNode) throws SummaryException, BerkeleyDbException {
		Preconditions.checkArgument(!isClassOrProperty(sDataNode));
		
		highestTypedDataNode = sDataNode;
	}
}
