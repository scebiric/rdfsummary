package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.OperationNotSupportedException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.google.common.collect.Lists;
import fr.inria.oak.RDFSummary.Summarizer;
import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.TroveSummaryResult;
import fr.inria.oak.RDFSummary.summary.util.AssertUtils;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class WeakPureCliqueEquivalenceSummaryTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	private static final String WEAK_SUMMARY = SummaryType.WEAK;
	private static final String WEAK_DATASETS_DIR = "/weak/";
	
	@Before
	public void setUp() throws ConfigurationException, IOException {
		context = new AnnotationConfigApplicationContext();
		TestUtils.deleteBerkeleyDbDirectory();
	}

	@Test
	public void summarizeTypeTriples() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("types.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);
		dictionary = TestUtils.loadMemoryDictionary(context);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		assertEquals(5, summaryResult.getStatistics().getTotalSummaryNodeCount()); 
		
		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));

		// Data nodes count
		assertEquals(2, summary.getSummaryDataNodes().size());

		// Edges
		assertEquals(1, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(4, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}

	@Test
	public void summarizeCommonSources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("cs.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();

		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(2, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
	
		// Common source patterns
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));
		DataTriple dataTriple4 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p4>"));
		DataTriple dataTriple5 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonSubject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5)));

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getObject()).contains(dictionary.getKey("<c2>")));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple2.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple2.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getObject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple3.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple3.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getObject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple4.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple4.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple4.getObject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple5.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple5.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple5.getObject()));

		assertEquals(8, summaryResult.getStatistics().getTotalSummaryNodeCount());
		// Data nodes count
		assertEquals(6, summary.getSummaryDataNodes().size());
		// Edges		 
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(2, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}

	@Test
	public void summarizeCommonTargets() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("ct.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(2, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));

		// Common source patterns 
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));
		DataTriple dataTriple4 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p4>"));
		DataTriple dataTriple5 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p5>"));
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		assertNotNull(dataTriple4);
		assertNotNull(dataTriple5);
		AssertUtils.assertCommonObject((Lists.newArrayList(dataTriple1, dataTriple2, dataTriple3, dataTriple4, dataTriple5)));

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c2>")));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getObject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getSubject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple2.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple2.getObject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getSubject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple3.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple3.getObject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple4.getSubject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple4.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple4.getObject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple5.getSubject()));
		
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple5.getObject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple5.getObject()).contains(dictionary.getKey("<c1>")));

		assertEquals(8, summaryResult.getStatistics().getTotalSummaryNodeCount());
		// Data nodes count
		assertEquals(6, summary.getSummaryDataNodes().size());
		// Edges	 
		assertEquals(5, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(2, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}

	@Test
	public void summarizePropertyPaths() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("pp.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(1, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));

		// Path patterns 
		DataTriple dataTriple1 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p1>"));
		DataTriple dataTriple2 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p2>"));
		DataTriple dataTriple3 = TestUtils.getDataTripleForProperty(summary, dictionary.getKey("<p3>"));		
		assertNotNull(dataTriple1);
		assertNotNull(dataTriple2);
		assertNotNull(dataTriple3);
		AssertUtils.assertCommonSubject((Lists.newArrayList(dataTriple2, dataTriple3)));
		assertEquals(dataTriple1.getObject(), dataTriple2.getSubject());

		// Typing of data nodes
		assertEquals(1, summary.getClassesForSummaryNode(dataTriple1.getSubject()).size());
		assertTrue(summary.getClassesForSummaryNode(dataTriple1.getSubject()).contains(dictionary.getKey("<c1>")));
		
		assertNull(summary.getClassesForSummaryNode(dataTriple1.getObject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getSubject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple2.getObject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getSubject()));
		assertNull(summary.getClassesForSummaryNode(dataTriple3.getObject()));
		
		assertEquals(5, summaryResult.getStatistics().getTotalSummaryNodeCount());
		// Data nodes count
		assertEquals(4, summary.getSummaryDataNodes().size());
		// Edges
		assertEquals(3, summaryResult.getStatistics().getSummaryDataTripleCount());		
		assertEquals(1, summaryResult.getStatistics().getSummaryTypeTripleCount());
	}

	@Test
	public void summarizeTypedOnlyResources() throws ConfigurationException, SQLException, DictionaryException, InexistentKeyException, ParametersException, JenaException, UnsupportedDatabaseEngineException, IOException, SchemaException, PsqlStorageException, InexistentValueException, OperationNotSupportedException, QueryException, Exception {
		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("typedonly.nt"));
		Configurator.javaSetUpApplicationContext(context, params);
		Summarizer summarizer = context.getBean(Summarizer.class);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		TroveSummaryResult summaryResult = summarizer.summarize(params);

		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Class nodes
		assertEquals(3, summaryResult.getStatistics().getSummaryClassNodeCount());
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c3>")));

		assertEquals(4, summaryResult.getStatistics().getTotalSummaryNodeCount());
		// Data nodes count
		assertEquals(1, summary.getSummaryDataNodes().size());
		// Edges 
		assertEquals(0, summaryResult.getStatistics().getSummaryDataTripleCount());
		assertEquals(3, summaryResult.getStatistics().getSummaryTypeTripleCount());

		assertEquals(1, summary.getClassesForSummaryNodeMap().keys().length);
	}
	
//	@Test
//	public void propertiesRepresentedByThemselves() throws ConfigurationException, BeansException, SQLException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
//		SummarizerParams params = TestUtils.getTestSummarizerParams(WEAK_SUMMARY, WEAK_DATASETS_DIR.concat("props.nt"));
//		Configurator.javaSetUpApplicationContext(context, params);
//		Summarizer summarizer = context.getBean(Summarizer.class);
//		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
//		
//		TroveSummaryResult summaryResult = summarizer.summarize(params);
//
//		WeakPureCliqueEquivalenceSummary summary = (WeakPureCliqueEquivalenceSummary) summaryResult.getSummary();
//		dictionary = TestUtils.loadMemoryDictionary(context);
//
//		// Class nodes
//		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c1>")));
//		assertTrue(TestUtils.getDistinctClasses(summary.getClassesForSummaryNodeMap()).contains(dictionary.getKey("<c2>")));
//		assertEquals(2, summaryResult.getStatistics().getSummaryClassNodeCount());
//		
//		// Data nodes count
//		int s1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<s1>"));
//		int o1Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o1>"));
//		int o2Rep = summary.getSummaryDataNodeForInputDataNode(dictionary.getKey("<o2>"));
//		assertTrue(s1Rep != o1Rep);
//		assertTrue(s1Rep != o2Rep);
//		assertTrue(o1Rep != o2Rep); 
//		assertEquals(3, summary.getSummaryDataNodes().size());
//		
//		// Total nodes
//		assertEquals(5, summaryResult.getStatistics().getTotalSummaryNodeCount());
//		
//		// Edges 
//		assertEquals(3, summaryResult.getStatistics().getSummaryDataTripleCount());
//		assertEquals(2, summaryResult.getStatistics().getSummaryTypeTripleCount());
//	}
}
