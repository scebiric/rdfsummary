package fr.inria.oak.RDFSummary.summary.trove.export;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;

import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.data.storage.StorageLayout;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.summary.Exporter;
import fr.inria.oak.RDFSummary.summary.stats.Stats;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * {@link Exporter} functionalities for an integer representation of the 
 * summary based on Trove collections and an encoded input RDF dataset.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveSummaryExporter extends Exporter {
	
	/**
	 * Exports {@link RdfSummary} to PostgreSQL
	 * 
	 * @param summary
	 * @param params
	 * @param storageLayout
	 * @throws SQLException
	 * @throws DictionaryException
	 * @throws InexistentValueException 
	 */
	public void exportRdfSummaryToPostgres(RdfSummary summary, SummarizerParams params, StorageLayout storageLayout) throws SQLException, DictionaryException, InexistentValueException;
	
	/**
	 * Exports decoded summary triples to DOT and/or an RDF n-triples files
	 * 
	 * @param params
	 * @param summary
	 * @param schema
	 * @param stats
	 * @param outputFilename
	 * @param storageLayout
	 * @throws ParametersException 
	 * @throws ConfigurationException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws DictionaryException 
	 * @throws InexistentValueException 
	 */
	public void exportDecodedTriplesToFiles(SummarizerParams params, RdfSummary summary, Schema schema, Stats stats,
			String outputFilename, StorageLayout storageLayout) throws ConfigurationException, ParametersException, SQLException, IOException, DictionaryException, InexistentValueException;
}
