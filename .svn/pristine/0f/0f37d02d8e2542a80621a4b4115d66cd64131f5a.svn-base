package fr.inria.oak.RDFSummary.summary.trove;

import java.util.Collection;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.set.TIntSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class TypePrimeWeakSummaryImpl extends WeakSummaryImpl implements TypePrimeWeakSummary {

	/** The id of the highest typed data node in the summary */
	private int highestTypedDataNode = -1;
	
	/**
	 * @param uriGenerator
	 * @param dictDao
	 * @param bdbHandler
	 * @param classAndPropertyDb
	 */
	public TypePrimeWeakSummaryImpl(final UriGenerator uriGenerator, final DictionaryDao dictDao, final BerkeleyDbHandler bdbHandler, final String classAndPropertyDb) {	
		super(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createDataTriple(final int source, final int dataProperty, final int target) {
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples == null) {
			dataTriples = Lists.newArrayList();
			dataTriples.add(new DataTriple(source, dataProperty, target));
			dataTriplesForDataProperty.put(dataProperty, dataTriples);
		}
		else
			dataTriples.add(new DataTriple(source, dataProperty, target));

		if (!this.isTyped(source))
			this.setSourceForDataProperty(dataProperty, source);
		if (!this.isTyped(target))
			this.setTargetForDataProperty(dataProperty, target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsDataTriple(final int subject, final int dataProperty, final int object) {
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples != null) {
			for (DataTriple dataTriple : dataTriples) {
				if (dataTriple.getSubject() == subject && dataTriple.getObject() == object)
					return true;
			}
		}

		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int unifyDataNodes(final int dataNode1, final int dataNode2) {
		Preconditions.checkArgument(!this.isTyped(dataNode1));
		Preconditions.checkArgument(!this.isTyped(dataNode2));

		int remainingNode;
		int vanishingNode;

		if (this.getDegreeForDataNode(dataNode1) < this.getDegreeForDataNode(dataNode2)) {
			vanishingNode = dataNode1;
			remainingNode = dataNode2;
		}
		else {
			vanishingNode = dataNode2;
			remainingNode = dataNode1;
		}

		TIntSet dataProps = dataPropertiesForSource.get(vanishingNode);
		if (dataProps != null) {
			iterator = dataProps.iterator();
			while (iterator.hasNext()) {
				dataTriples = dataTriplesForDataProperty.get(iterator.next());
				if (dataTriples != null) {
					for (DataTriple dataTriple : dataTriples) {
						dataTriple.setSubject(remainingNode);
					}
				}
			}
		}

		dataProps = dataPropertiesForTarget.get(vanishingNode);
		if (dataProps != null) {
			iterator = dataProps.iterator();
			while (iterator.hasNext()) {
				dataTriples = dataTriplesForDataProperty.get(iterator.next());
				if (dataTriples != null) {
					for (DataTriple dataTriple : dataTriples) {
						dataTriple.setObject(remainingNode);
					}
				}
			}
		}

		TIntSet set = this.getRepresentedInputDataNodes(vanishingNode);
		iterator = set.iterator();
		while (iterator.hasNext()) {
			this.representInputDataNode(iterator.next(), remainingNode);
		}
		resourcesForDataNode.remove(vanishingNode);

		set = dataPropertiesForSource.get(vanishingNode);
		if (set != null) {
			iterator = set.iterator();
			while (iterator.hasNext()) {
				this.setSourceForDataProperty(iterator.next(), remainingNode);
			}
			dataPropertiesForSource.remove(vanishingNode);
		}

		set = dataPropertiesForTarget.get(vanishingNode);
		if (set != null) {
			iterator = set.iterator();
			while (iterator.hasNext()) {
				this.setTargetForDataProperty(iterator.next(), remainingNode);
			}
			dataPropertiesForTarget.remove(vanishingNode);
		}

		return remainingNode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHighestTypedDataNode(final int highestTypedDataNode) {
		this.highestTypedDataNode = highestTypedDataNode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * In the "type prime summaries" all typed data nodes are created before any untyped nodes,
	 * and their value comes from a sequence starting from 1.
	 * So, a data node is untyped if its value is higher than the highest typed data node.
	 */
	@Override
	public boolean isTyped(final int dataNode) {
		Preconditions.checkArgument(dataNode > 0);
		return highestTypedDataNode > 0 && dataNode <= highestTypedDataNode;
	}
}
