package fr.inria.oak.RDFSummary.config;

import java.sql.SQLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.EnvironmentConfig;

import fr.inria.oak.RDFSummary.cleaner.Cleaner;
import fr.inria.oak.RDFSummary.cleaner.CleanerImpl;
import fr.inria.oak.RDFSummary.constants.DataLoader;
import fr.inria.oak.RDFSummary.constants.ParameterName;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandlerImpl;
import fr.inria.oak.RDFSummary.data.dao.DdlDao;
import fr.inria.oak.RDFSummary.data.dao.DdlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDaoImpl;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDao;
import fr.inria.oak.RDFSummary.data.dao.SaturatorDaoImpl;
import fr.inria.oak.RDFSummary.data.encoding.Encoding;
import fr.inria.oak.RDFSummary.data.encoding.EncodingImpl;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandlerImpl;
import fr.inria.oak.RDFSummary.data.loader.JenaModelDataLoader;
import fr.inria.oak.RDFSummary.data.loader.PsqlCopyDataLoader;
import fr.inria.oak.RDFSummary.data.loader.PsqlDataLoader;
import fr.inria.oak.RDFSummary.data.queries.DdlQueries;
import fr.inria.oak.RDFSummary.data.queries.DdlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueries;
import fr.inria.oak.RDFSummary.data.queries.DictionaryQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.DmlQueries;
import fr.inria.oak.RDFSummary.data.queries.DmlQueriesImpl;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueries;
import fr.inria.oak.RDFSummary.data.queries.RdfTablesQueriesImpl;
import fr.inria.oak.RDFSummary.data.saturation.BatchedReasoner;
import fr.inria.oak.RDFSummary.data.saturation.Reasoner;
import fr.inria.oak.RDFSummary.data.saturation.SaturationGenerator;
import fr.inria.oak.RDFSummary.data.saturation.SaturationGeneratorImpl;
import fr.inria.oak.RDFSummary.data.saturation.Saturator;
import fr.inria.oak.RDFSummary.data.saturation.SaturatorImpl;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorage;
import fr.inria.oak.RDFSummary.data.storage.TriplesTablePsqlStorageImpl;
import fr.inria.oak.RDFSummary.data.storage.EncodedPsqlStorage;
import fr.inria.oak.RDFSummary.data.storage.MemoryDictTriplesTableEncodedPsqlStorageImpl;
import fr.inria.oak.RDFSummary.data.storage.splitter.PsqlStorageSplitter;
import fr.inria.oak.RDFSummary.data.storage.splitter.StorageSplitter;
import fr.inria.oak.RDFSummary.run.JdbcTest;
import fr.inria.oak.RDFSummary.run.experiments.SummarizationTime;
import fr.inria.oak.RDFSummary.summary.stats.PostgresStatsCollector;
import fr.inria.oak.RDFSummary.summary.stats.PostgresStatsCollectorImpl;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilderImpl;
import fr.inria.oak.RDFSummary.summary.trove.componentsummarizer.PostgresTroveRdfComponentSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.trove.componentsummarizer.TroveRdfComponentSummarizer;
import fr.inria.oak.RDFSummary.summary.trove.export.DotSummaryViz;
import fr.inria.oak.RDFSummary.summary.trove.export.DotSummaryVizImpl;
import fr.inria.oak.RDFSummary.summary.trove.export.TroveSummaryExporterImpl;
import fr.inria.oak.RDFSummary.summary.trove.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.trove.noderepresenter.TroveNodeRepresenterImpl;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.PostgresTroveSummarizerImpl;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.TroveSummarizer;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.TroveTripleCreator;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.TroveTripleCreatorImpl;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.urigenerator.UriGeneratorImpl;
import fr.inria.oak.RDFSummary.util.BerkeleyDbUtils;
import fr.inria.oak.RDFSummary.util.Utils;
import fr.inria.oak.RDFSummary.writer.TriplesWriter;
import fr.inria.oak.RDFSummary.writer.TriplesWriterImpl;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.PostgresDataSource;

/**
 * Bean definitions for summarization on PostgreSQL
 * 
 * @author Sejla CEBIRIC
 *
 */
@Configuration
@Component
public class PsqlSummaryBeanConfig extends SummaryBeanConfig {
	
	@Bean
	public PostgresDataSource postgresDataSource() {
		return new PostgresDataSource(env.getProperty(ParameterName.SERVER_NAME), Integer.parseInt(env.getProperty(ParameterName.PORT)), env.getProperty(ParameterName.DATABASE_NAME), 
				env.getProperty(ParameterName.USERNAME), env.getProperty(ParameterName.PASSWORD));
	}

	@Bean
	public RdfTablesDao rdfTablesDaoImpl() throws SQLException, DictionaryException {
		return new RdfTablesDaoImpl(sqlConnectionHandlerImpl(), Integer.parseInt(env.getProperty(ParameterName.FETCH_SIZE)), rdfTablesQueriesImpl(), 
				dictDaoImpl(), ddlDaoImpl(), dmlDaoImpl(), timerImpl());		
	}

	@Bean 
	public DmlQueries dmlQueriesImpl() {
		return new DmlQueriesImpl();
	}
	
	@Bean
	public DdlQueries ddlQueriesImpl() {
		return new DdlQueriesImpl();
	}
	
	@Bean 
	public DmlDao dmlDaoImpl() throws NumberFormatException, SQLException {
		return new DmlDaoImpl(sqlConnectionHandlerImpl(), Integer.parseInt(env.getProperty(ParameterName.FETCH_SIZE)), timerImpl(), dmlQueriesImpl());
	}
	
	@Bean
	public DdlDao ddlDaoImpl() throws NumberFormatException, SQLException {
		return new DdlDaoImpl(sqlConnectionHandlerImpl(), Integer.parseInt(env.getProperty(ParameterName.FETCH_SIZE)), timerImpl(), ddlQueriesImpl());
	}
	
	@Bean
	public SqlConnectionHandler sqlConnectionHandlerImpl() throws SQLException {
		return new SqlConnectionHandlerImpl(postgresDataSource().getConnection());
	}
	
	@Bean
	public DictionaryDao dictDaoImpl() throws SQLException {
		return new DictionaryDaoImpl(env.getProperty(ParameterName.DICTIONARY_TABLE), sqlConnectionHandlerImpl(), 
				Integer.parseInt(env.getProperty(ParameterName.FETCH_SIZE)), dictionaryQueryProviderImpl(), timerImpl());
	}
	
	@Bean
	public DictionaryQueries dictionaryQueryProviderImpl() {
		return new DictionaryQueriesImpl();
	}
	
	@Bean
	public SaturatorDao saturatorDaoImpl() throws SQLException {
		return new SaturatorDaoImpl(saturator(), sqlConnectionHandlerImpl(), rdfTablesQueriesImpl(), dmlQueriesImpl(), timerImpl());
	}
	
	@Bean
	public Saturator saturator() {
		return new SaturatorImpl();
	}

	@Bean
	public RdfTablesQueries rdfTablesQueriesImpl() {
		return new RdfTablesQueriesImpl();
	}

	@Bean
	public Encoding encodingImpl() throws SQLException, DictionaryException {
		return new EncodingImpl(dictDaoImpl(), timerImpl());
	}

	@Bean
	public PsqlDataLoader jenaModelDataLoader() throws SQLException, DictionaryException {
		return new JenaModelDataLoader(ddlDaoImpl(), dmlDaoImpl(), rdfTablesDaoImpl(), jenaRdfManager(), timerImpl());
	}

	@Bean
	public PsqlDataLoader psqlCopyDataLoader() throws SQLException, DictionaryException {
		return new PsqlCopyDataLoader(ddlDaoImpl(), dmlDaoImpl(), rdfTablesDaoImpl(), timerImpl());
	}	

	@Bean
	public BerkeleyDbHandler berkeleyDbHandlerImpl() throws BerkeleyDbException {		
		return new BerkeleyDbHandlerImpl(env.getProperty(ParameterName.BERKELEY_DB_DIR), envConfig());
	}
	
	@Bean
	public EnvironmentConfig envConfig() {
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);

		return envConfig;
	}

	@Bean
	public DatabaseConfig dbConfig() {
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setAllowCreate(true);
		
		return dbConfig;
	}
	
	@Bean
	public TroveCliqueBuilder troveCliqueBuilderImpl() throws SQLException, DictionaryException {
		return new TroveCliqueBuilderImpl(rdfTablesDaoImpl());
	}

	@Bean
	public StorageSplitter postgresStorageSplitter() throws SQLException, DictionaryException {
		return new PsqlStorageSplitter(dmlDaoImpl(), rdfTablesDaoImpl());
	}

	@Bean
	public Reasoner batchedReasoner() throws SQLException, DictionaryException {
		return new BatchedReasoner(saturatorDaoImpl(), ddlDaoImpl());
	}

	@Bean
	public SaturationGenerator saturationGeneratorImpl() throws SQLException, DictionaryException {
		return new SaturationGeneratorImpl(batchedReasoner(), dmlDaoImpl(), ddlDaoImpl(), dictDaoImpl(), timerImpl());
	}

	@Bean
	public EncodedPsqlStorage memoryDictEncodedTriplesTablePsqlStorageImpl() throws ParametersException, NumberFormatException, SQLException, DictionaryException {
		return new MemoryDictTriplesTableEncodedPsqlStorageImpl(triplesTablePsqlStorageImpl(), dictDaoImpl(), encodingImpl(), 
				saturationGeneratorImpl(), postgresStorageSplitter(), dmlDaoImpl(), rdfTablesDaoImpl(), timerImpl());
	}
	
	@Bean
	public PsqlStorage triplesTablePsqlStorageImpl() throws NumberFormatException, SQLException, DictionaryException, ParametersException {
		return new TriplesTablePsqlStorageImpl(dmlDaoImpl(), ddlDaoImpl(), rdfTablesDaoImpl(), 
				getDataLoader(env.getProperty(ParameterName.DATA_LOADER)), customJenaSchemaParser(), timerImpl());
	}

	private PsqlDataLoader getDataLoader(String dataLoader) throws NumberFormatException, SQLException, DictionaryException, ParametersException {
		PsqlDataLoader loader = null;
		if (dataLoader.equals(DataLoader.PSQL_COPY))
			loader = psqlCopyDataLoader();
		else if (dataLoader.equals(DataLoader.JENA))
			loader = jenaModelDataLoader();
		else
			throw new ParametersException("Invalid data loader specified.");

		return loader;
	}

	@Bean
	public fr.inria.oak.RDFSummary.summary.trove.export.TroveSummaryExporter troveSummaryExporterImpl() throws SQLException, DictionaryException {
		return new TroveSummaryExporterImpl(dotSummaryVizImpl(), triplesWriterImpl(), sqlConnectionHandlerImpl(), rdfTablesDaoImpl(), dictDaoImpl(), ddlDaoImpl(), dmlDaoImpl(), dmlQueriesImpl(), timerImpl());
	}
	
	@Bean
	public fr.inria.oak.RDFSummary.run.Summarizer summarizer() throws SQLException, DictionaryException, NumberFormatException, ParametersException {
		return new fr.inria.oak.RDFSummary.run.Summarizer();
	}

	@Bean
	public SummarizationTime experiments() throws NumberFormatException, ParametersException, SQLException, DictionaryException {
		return new SummarizationTime();
	}

	@Bean
	public Utils utils() throws SQLException, DictionaryException {
		return new Utils(dictDaoImpl(), dmlDaoImpl(), ddlDaoImpl());
	}

	@Bean
	public JdbcTest jdbcTest() throws SQLException, DictionaryException {
		return new JdbcTest(rdfTablesDaoImpl(), sqlConnectionHandlerImpl());
	}
	
	@Bean
	public Cleaner cleanerImpl() throws SQLException, DictionaryException {
		return new CleanerImpl(sqlConnectionHandlerImpl(), ddlDaoImpl(), dictDaoImpl(), dmlDaoImpl(), rdfTablesDaoImpl(), saturatorDaoImpl());
	}
	
	@Bean
	public PostgresStatsCollector postgresStatsCollectorImpl() throws SQLException, DictionaryException {
		return new PostgresStatsCollectorImpl(rdfTablesDaoImpl(), dmlDaoImpl());
	}
	
	@Bean
	public BerkeleyDbUtils berkeleyDbUtils() throws NumberFormatException, BerkeleyDbException, SQLException {
		return new BerkeleyDbUtils(berkeleyDbHandlerImpl(), dbConfig(), dmlDaoImpl());
	}
	
	@Bean 
	public UriGenerator uriGeneratorImpl() {
		return new UriGeneratorImpl(env.getProperty(ParameterName.PSQL_SCHEMA_NAME), env.getProperty(ParameterName.SUMMARY_TYPE));
	}
	
	@Bean
	public TroveSummarizer postgresTroveSummarizerImpl() throws SQLException, NumberFormatException, DictionaryException {
		return new PostgresTroveSummarizerImpl(sqlConnectionHandlerImpl(), postgresTroveRdfComponentSummarizerImpl(), uriGeneratorImpl(), dmlDaoImpl(), dictDaoImpl(), rdfTablesDaoImpl());
	}
	
	@Bean
	public TroveRdfComponentSummarizer postgresTroveRdfComponentSummarizerImpl() throws NumberFormatException, SQLException, DictionaryException {
		return new PostgresTroveRdfComponentSummarizerImpl(dmlDaoImpl(), rdfTablesDaoImpl(), 
				troveTripleCreatorImpl(), troveNodeRepresenterImpl(), troveCliqueBuilderImpl());
	}
	
	@Bean
	public TroveTripleCreator troveTripleCreatorImpl() {
		return new TroveTripleCreatorImpl(troveNodeRepresenterImpl());
	}
	
	@Bean
	public TroveNodeRepresenter troveNodeRepresenterImpl() {
		return new TroveNodeRepresenterImpl();
	}
	
	@Bean
	public DotSummaryViz dotSummaryVizImpl() {
		return new DotSummaryVizImpl();
	}
	
	@Bean
	public TriplesWriter triplesWriterImpl() {
		return new TriplesWriterImpl();
	}
}
