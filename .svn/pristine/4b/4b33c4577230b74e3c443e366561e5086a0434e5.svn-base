package fr.inria.oak.RDFSummary.summary.trove;

import java.util.Collection;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class TypedWeakSummaryImpl extends WeakSummaryImpl implements TypedWeakSummary {

	/** The id of the highest typed data node in the summary */
	private int highestTypedDataNode = -1;
	
	/**
	 * @param uriGenerator
	 * @param dictDao
	 * @param bdbHandler
	 * @param classAndPropertyDb
	 */
	public TypedWeakSummaryImpl(final UriGenerator uriGenerator, final DictionaryDao dictDao, final BerkeleyDbHandler bdbHandler, final String classAndPropertyDb) {	
		super(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
	}

	@Override
	public void createDataTriple(final int source, final int dataProperty, final int target) throws BerkeleyDbException {
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples == null) {
			dataTriples = Lists.newArrayList();
			dataTriples.add(new DataTriple(source, dataProperty, target));
			dataTriplesForDataProperty.put(dataProperty, dataTriples);
		}
		else
			dataTriples.add(new DataTriple(source, dataProperty, target));

		if (!isClassOrProperty(source) && !this.isTypedDataNode(source))
			super.setSourceDataNodeForDataProperty(dataProperty, source);
		if (!isClassOrProperty(target) && !this.isTypedDataNode(target))
			super.setTargetDataNodeForDataProperty(dataProperty, target);
	}

	@Override
	public boolean existsDataTriple(final int subject, final int dataProperty, final int object) {
		Collection<DataTriple> dataTriples = dataTriplesForDataProperty.get(dataProperty);
		if (dataTriples != null) {
			for (DataTriple dataTriple : dataTriples) {
				if (dataTriple.getSubject() == subject && dataTriple.getObject() == object)
					return true;
			}
		}

		return false;
	}
	
	@Override
	public int unifyUntypedDataNodes(final int node1, final int node2) throws BerkeleyDbException {
		Preconditions.checkArgument(!this.isTypedDataNode(node1));
		Preconditions.checkArgument(!this.isTypedDataNode(node2));

		return super.unifyDataNodes(node1, node2);
	}

	/** 
	 * In the "type prime summaries" all typed data nodes are created before any untyped nodes,
	 * and their value comes from a sequence starting from 1.
	 * So, a data node is untyped if its value is higher than the highest typed node.
	 * @throws BerkeleyDbException 
	 */
	@Override
	public boolean isTypedDataNode(final int sDataNode) throws BerkeleyDbException {
		Preconditions.checkArgument(!isClassOrProperty(sDataNode));
		
		return highestTypedDataNode > 0 && sDataNode <= highestTypedDataNode;
	}

	@Override
	public void setHighestTypedDataNode(int sDataNode) throws BerkeleyDbException {
		Preconditions.checkArgument(!isClassOrProperty(sDataNode));
		
		highestTypedDataNode = sDataNode;
	}
}
