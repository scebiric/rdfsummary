package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.old.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongSummaryImpl implements StrongEquivalenceSummary, PureCliqueEquivalenceSummary {

	private final StrongEquivalenceSummaryImpl strongImpl;
	private final PureCliqueEquivalenceSummaryImpl pureCliqueImpl;
//	private final RdfSummaryImpl rdfSummaryImpl;
	
	/**
	 * @param strongEquivalenceSummaryImpl
	 * @param pureCliqueEquivalenceSummaryImpl
	 * @param rdfSummaryImpl
	 */
	public StrongSummaryImpl(final StrongEquivalenceSummaryImpl strongEquivalenceSummaryImpl,
			final PureCliqueEquivalenceSummaryImpl pureCliqueEquivalenceSummaryImpl,
			final RdfSummaryImpl rdfSummaryImpl) {
		this.strongImpl = strongEquivalenceSummaryImpl;
		this.pureCliqueImpl = pureCliqueEquivalenceSummaryImpl;
//		this.rdfSummaryImpl = rdfSummaryImpl;
	}

	@Override
	public List<TIntSet> getSourceCliques() {
		return strongImpl.getSourceCliques();
	}

	@Override
	public List<TIntSet> getTargetCliques() {
		return strongImpl.getTargetCliques();
	}

	@Override
	public int getSourceCliqueIdBySubject(int subject) {
		return strongImpl.getSourceCliqueIdBySubject(subject);
	}

	@Override
	public int getTargetCliqueIdByObject(int object) {
		return strongImpl.getTargetCliqueIdByObject(object);
	}

	@Override
	public void setSourceCliqueIdForSubject(int subject, int sourceClique) {
		strongImpl.setSourceCliqueIdForSubject(subject, sourceClique);
	}

	@Override
	public void setTargetCliqueIdForObject(int object, int targetClique) {
		strongImpl.setTargetCliqueIdForObject(object, targetClique);
	}

	@Override
	public TIntSet getDataNodesForSourceCliqueId(int sourceCliqueId) {
		return strongImpl.getDataNodesForSourceCliqueId(sourceCliqueId);
	}

	@Override
	public TIntSet getDataNodesForTargetCliqueId(int targetCliqueId) {
		return strongImpl.getDataNodesForTargetCliqueId(targetCliqueId);
	}

	@Override
	public void storeDataNodeForSourceCliqueId(int sourceCliqueId, int dataNode) throws BerkeleyDbException {
		strongImpl.storeDataNodeForSourceCliqueId(sourceCliqueId, dataNode);
	}

	@Override
	public void storeDataNodeForTargetCliqueId(int targetCliqueId, int dataNode) throws BerkeleyDbException {
		strongImpl.storeDataNodeForTargetCliqueId(targetCliqueId, dataNode);
	}

	@Override
	public void clearSourceCliques() {
		strongImpl.clearSourceCliques();
	}

	@Override
	public void clearTargetCliques() {
		strongImpl.clearTargetCliques();
	}

	@Override
	public void clearCliqueMaps() {
		strongImpl.clearCliqueMaps();
	}

	@Override
	public int representInputDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
		return strongImpl.representInputDataNode(gDataNode);
	}

	@Override
	public int createDataNode(TIntSet gDataNodes, TIntSet classes) throws SQLException, BerkeleyDbException {
		return pureCliqueImpl.createDataNode(gDataNodes, classes);
	}
	
//	@Override
//	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
//		return rdfSummaryImpl.getClassesForSummaryNodeMap();
//	}
//
//	@Override
//	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
//		return rdfSummaryImpl.getDataTriplesForDataPropertyMap();
//	}
//
//	@Override
//	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
//		return rdfSummaryImpl.getDataTriplesForDataProperty(dataProperty);
//	}
//
//	@Override
//	public TIntSet getSummaryDataNodes() {
//		return rdfSummaryImpl.getSummaryDataNodes();
//	}
//
//	@Override
//	public int getSummaryDataNodeSupport(int sDataNode) {
//		return rdfSummaryImpl.getSummaryDataNodeSupport(sDataNode);
//	}
//
//	@Override
//	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
//		return rdfSummaryImpl.getSummaryDataNodeForInputDataNode(gDataNode);
//	}
//
//	@Override
//	public TIntSet getClassesForSummaryNode(int sNode) {
//		return rdfSummaryImpl.getClassesForSummaryNode(sNode);
//	}
//
//	@Override
//	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
//		return rdfSummaryImpl.getRepresentedInputDataNodes(sDataNode);
//	}
//
//	@Override
//	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
//		return rdfSummaryImpl.createDataNode(gDataNode);
//	}
//
//	@Override
//	public void createTypeTriples(int sNode, TIntSet classes) {
//		rdfSummaryImpl.createTypeTriples(sNode, classes);
//	}
//
//	@Override
//	public boolean existsDataTriple(int subject, int dataProperty, int object) {
//		return rdfSummaryImpl.existsDataTriple(subject, dataProperty, object);
//	}
//
//	@Override
//	public void representInputDataNode(int gDataNode, int sDataNode) {
//		rdfSummaryImpl.representInputDataNode(gDataNode, sDataNode);
//	}
//
//	@Override
//	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
//		rdfSummaryImpl.unrepresentAllNodesForSummaryDataNode(sDataNode);
//	}
//
//	@Override
//	public boolean isClassOrProperty(int iri) throws BerkeleyDbException {
//		return rdfSummaryImpl.isClassOrProperty(iri);
//	}
//
//	@Override
//	public int getLastCreatedDataNode() {
//		return rdfSummaryImpl.getLastCreatedDataNode();
//	}
//
//	@Override
//	public int getSummaryDataNodeCount() {
//		return rdfSummaryImpl.getSummaryDataNodeCount();
//	}
//
//	@Override
//	public int getNextNodeId() throws SQLException {
//		return rdfSummaryImpl.getNextNodeId();
//	}
//
//	@Override
//	public void setLastCreatedDataNode(int dataNode) {
//		rdfSummaryImpl.setLastCreatedDataNode(dataNode);
//	}
//
//	@Override
//	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
//		rdfSummaryImpl.storeClassesBySummaryNode(sNode, classes);
//	}
//
//	@Override
//	public void storeDataTripleByProperty(int source, int dataProperty, int target) {
//		rdfSummaryImpl.storeDataTripleByProperty(source, dataProperty, target);
//	}
}
