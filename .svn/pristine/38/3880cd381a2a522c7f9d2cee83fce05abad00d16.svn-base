package fr.inria.oak.RDFSummary.config.params;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class SummarizationParams extends Params {

	private boolean dropSchema = false;
	private boolean indexing = false;
	private String summaryType;
	private String psqlSchemaName;
	private String dataTriplesFilepath;
	private String schemaTriplesFilepath;
	private final String dataLoader;
	private int fetchSize;
	private boolean saturateInput = false;
	private boolean dropSaturatedTable = false;
	private int saturationBatchSize;
	private final String berkeleyDbDirectory;

	/**
	 * Constructor of {@link SummarizationParams}
	 * 
	 * @param summaryType
	 * @param psqlSchemaName
	 * @param dataTriplesFilepath
	 * @param dataLoader
	 * @param berkeleyDbDirectory
	 */
	public SummarizationParams(final String summaryType, final String psqlSchemaName, final String dataTriplesFilepath, 
			final String dataLoader, final String berkeleyDbDirectory) { 
		this.summaryType = StringUtils.toLower(summaryType);
		this.psqlSchemaName = StringUtils.toLower(psqlSchemaName);
		this.dataTriplesFilepath = dataTriplesFilepath;
		this.dataLoader = StringUtils.toLower(dataLoader);
		this.berkeleyDbDirectory = berkeleyDbDirectory;
	}

	public boolean dropSchema() {
		return dropSchema;
	}

	public boolean indexing() {
		return indexing;
	}

	public String getSummaryType() {
		return summaryType;
	}

	public String getSchemaName() {
		return psqlSchemaName;
	}

	public String getDataTriplesFilepath() {
		return dataTriplesFilepath;
	}

	public String getSchemaTriplesFilepath() {
		return schemaTriplesFilepath;
	}

	public String getDataLoader() {
		return dataLoader;
	}

	public String getTriplesTable() {
		return Constants.TRIPLES_TABLE_NAME;
	}

	public String getDictionaryTable() {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(psqlSchemaName));
		
		return NameUtils.qualify(psqlSchemaName, Constants.DICTIONARY_TABLE_NAME);
	}

	public int getFetchSize() {
		return fetchSize;
	}

	public boolean saturateInput() {
		return saturateInput;
	}
	
	public boolean dropSaturatedTable() {
		return dropSaturatedTable;
	}

	public int getSaturationBatchSize() {
		return saturationBatchSize;
	}

	public String getBerkeleyDbDirectory() {
		return berkeleyDbDirectory;
	}
	
	public void setIndexing(boolean value) {
		indexing = value;
	}

	public void setDropSchema(boolean value) {
		dropSchema = value;
	}

	public void setSchemaTriplesFilepath(String value) {
		schemaTriplesFilepath = value;
	}

	public void setFetchSize(int value) {
		fetchSize = value;
	}

	public void setSummaryType(String value) {
		summaryType = value;
	}

	public void setDataTriplesFilepath(String value) {
		dataTriplesFilepath = value;
	}

	public void setSchemaName(String value) {
		psqlSchemaName = value;
	}

	public void setSaturateInput(boolean value) {
		saturateInput = value;
	}

	public void setDropSaturatedTable(boolean value) {
		dropSaturatedTable = value;
	}
	
	public void setSaturationBatchSize(int value) {
		saturationBatchSize = value;
	}
}
