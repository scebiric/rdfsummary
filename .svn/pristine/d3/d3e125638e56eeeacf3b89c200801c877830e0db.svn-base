package fr.inria.oak.RDFSummary.writer;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import fr.inria.oak.RDFSummary.data.storage.TriplesLoadingTimes;
import fr.inria.oak.RDFSummary.data.storage.EncodingTimes;
import fr.inria.oak.RDFSummary.data.storage.StorageLayout;
import fr.inria.oak.RDFSummary.rdf.EncodedTriple;
import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.summary.stats.Stats;

/**
 * Writer interface provides functionalities for writing objects to files.
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface Writer {
	
	/** Decodes the triples and writes them to an n-triples file */
	public String writeSaturationTriplesToRDF(List<EncodedTriple> encodedTriples, List<Triple> schemaTriples, String outputFolder, String outputName, String rdfFormat) throws IOException;
	
	/** Writes the specified RDFS triples to an N-triples file */
	public void writeSchemaToNTriplesFile(Set<fr.inria.oak.commons.reasoning.rdfs.saturation.Triple> rdfsTriples, String outputFolder, String outputName) throws IOException;
	
	/**
	 * Writes the summary stats to a file
	 * 
	 * @param outputFolder
	 * @param outputName
	 * @param storageLayout
	 * @param stats
	 * @param timing
	 * @throws IOException 
	 */
	public void writeStatsToFile(final String outputFolder, final String outputName, final StorageLayout storageLayout, 
			final Stats stats, final TriplesLoadingTimes triplesLoadingTimes, final EncodingTimes encodedTriplesLoadingTimes) throws IOException;

	/** Initializes the writer 
	 * @throws IOException */
	public void initializeWriter(final String filepath) throws IOException;

	/** Closes the writer */
	public void closeWriter();

	/** Writes the triple to a file */
	public void writeTriple(Triple triple);
	
	/** Writes the triple to a file */
	public void writeTriple(String subject, String property, String object);

	/**
	 * Starts an n-triples file for summary triples
	 * 
	 * @param outputFolder
	 * @param outputFileName
	 * @param rdfFormat
	 * @return The filepath to the RDF file
	 * @throws IOException 
	 */
	public String startSummaryFile(final String outputFolder, final String outputFileName, final String rdfFormat) throws IOException;

	/**
	 * Writes the triple to an (already open) RDF file
	 * 
	 * @param subject
	 * @param property
	 * @param object
	 */
	public void writeToRdfFile(String subject, String property, String object);

	/** Closes the writer for RDF */
	public void closeRdfWriter();		
}
