package fr.inria.oak.RDFSummary.summary.trove.componentsummarizer;

import java.sql.SQLException;

import fr.inria.oak.RDFSummary.summary.RdfComponentSummarizer;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface TroveRdfComponentSummarizer extends RdfComponentSummarizer {
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(WeakPureCliqueEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(WeakTypeEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(StrongPureCliqueEquivalenceSummary summary, String dataTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param dataTable
	 * @param typesTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeDataTriples(StrongTypeEquivalenceSummary summary, String dataTable, String typesTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param typesTable
	 *  
	 * @throws SQLException 
	 */
	public void summarizeTypeTriples(PureCliqueEquivalenceSummary summary, String typesTable) throws SQLException;
	
	/**
	 * @param summary
	 * @param typesTable
	 * @throws SQLException 
	 *  
	 */
	public void summarizeTypeTriples(TypeEquivalenceSummary summary, String typesTable) throws SQLException;
}
