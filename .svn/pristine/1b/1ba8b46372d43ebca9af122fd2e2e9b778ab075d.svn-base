package fr.inria.oak.RDFSummary.summary.trove.summarizer;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Constant;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.ResultSet;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.DmlDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.logger.PerformanceLogger;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.componentsummarizer.TroveRdfComponentSummarizer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTroveSummarizerImpl implements TroveSummarizer {

	private static final Logger log = Logger.getLogger(TroveSummarizer.class);
	private final SqlConnectionHandler connHandler;
	private static TroveRdfComponentSummarizer RdfComponentSummarizer;
	private static UriGenerator UriGenerator;
	private static DmlDao DmlDao;
	private static DictionaryDao DictDao;
	private static RdfTablesDao RdfTablesDao;
	private static PerformanceLogger PerformanceLogger;
	
	private static StringBuilder Builder;
	private RdfSummary rdfSummary = null;
	private WeakPureCliqueEquivalenceSummary weakPureSummary = null;
	private WeakTypeEquivalenceSummary weakTypeSummary = null;
	private StrongPureCliqueEquivalenceSummary strongPureSummary = null;
	private StrongTypeEquivalenceSummary strongTypeSummary = null;
	
	private String classNodesTable = null;
	private String propertyNodesTable = null;
	
	/**
	 * @param connHandler
	 * @param rdfComponentSummarizer
	 * @param uriGenerator
	 * @param dmlDao
	 * @param dictDao
	 * @param rdfTablesDao
	 * @param performanceLogger
	 */
	public PostgresTroveSummarizerImpl(final SqlConnectionHandler connHandler, final TroveRdfComponentSummarizer rdfComponentSummarizer,
			final UriGenerator uriGenerator, final DmlDao dmlDao, final DictionaryDao dictDao, final RdfTablesDao rdfTablesDao, final PerformanceLogger performanceLogger) {
		this.connHandler = connHandler;
		RdfComponentSummarizer = rdfComponentSummarizer;
		UriGenerator = uriGenerator;
		DmlDao = dmlDao;
		DictDao = dictDao;
		RdfTablesDao = rdfTablesDao;
		PerformanceLogger = performanceLogger;
	}

	/**
	 * Tables assumed encoded. 
	 */
	@Override
	public TroveSummaryResult summarize(final String summaryType, final String dataTable, final String typesTable, final String schemaTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.WEAK) || summaryType.equals(SummaryType.TYPED_WEAK) ||
				summaryType.equals(SummaryType.STRONG) || summaryType.equals(SummaryType.TYPED_STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		
		PerformanceLogger.reset();
		rdfSummary = new RdfSummaryImpl(UriGenerator, DictDao);
		final String schemaName = NameUtils.getSchemaName(dataTable);
		classNodesTable = NameUtils.getClassNodesTableName(schemaName);
		propertyNodesTable = NameUtils.getPropertyNodesTableName(schemaName);
		final String schemaPropertyNodesTable = NameUtils.getSchemaPropertyNodesTableName(schemaName);
		final String possiblePropertyNodesTable = propertyNodesTable + "_possible";
		
		RdfTablesDao.recreateEncodedUriTable(classNodesTable);	
		RdfTablesDao.recreateEncodedUriTable(propertyNodesTable);
		RdfTablesDao.recreateEncodedUriTable(schemaPropertyNodesTable);
		RdfTablesDao.recreateEncodedUriTable(possiblePropertyNodesTable);
		
		PerformanceLogger.startSummarizationTimer();
		RdfTablesDao.extractAllClassNodes(classNodesTable, typesTable, schemaTable);
		RdfTablesDao.extractAllPropertyNodes(propertyNodesTable, schemaPropertyNodesTable, possiblePropertyNodesTable, dataTable, typesTable, schemaTable);
		loadClassNodes(rdfSummary, classNodesTable);
		loadPropertyNodes(rdfSummary, propertyNodesTable);
		
		TroveSummaryResult result = null;
		if (summaryType.equals(SummaryType.WEAK)) 
			result = this.generateWeakPureCliqueEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_WEAK)) 
			result = this.generateWeakTypeEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.STRONG))
			result = this.generateStrongPureCliqueEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_STRONG))
			result = this.generateStrongTypeEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		
		PerformanceLogger.stopSummarizationTimer();
		
		return result;
	}

	/**
	 * Loads class nodes from the table into the summary.
	 * 
	 * @param rdfSummary
	 * @param classNodesTable
	 * @throws SQLException
	 */
	private void loadClassNodes(RdfSummary rdfSummary, String classNodesTable) throws SQLException {
		final ResultSet resultSet = DmlDao.getSelectionResultSet(new String[] { Constant.URI }, true, classNodesTable, null);
		while (resultSet.next()) 
			rdfSummary.addClassNode(resultSet.getInt(1));
	}
	
	/**
	 * Loads property nodes from the table into the summary.
	 * 
	 * @param rdfSummary
	 * @param propertyNodesTable
	 * @throws SQLException
	 */
	private void loadPropertyNodes(RdfSummary rdfSummary, String propertyNodesTable) throws SQLException {
		final ResultSet resultSet = DmlDao.getSelectionResultSet(new String[] { Constant.URI }, true, propertyNodesTable, null);
		while (resultSet.next()) 
			rdfSummary.addPropertyNode(resultSet.getInt(1));
	}
	
	@Override
	public TroveSummaryResult generateWeakPureCliqueEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, DictionaryException, InexistentValueException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.WEAK));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));
		
		log.info(getSummaryTypeMessage(summaryType));
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		weakPureSummary = new WeakPureCliqueEquivalenceSummaryImpl(weakEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(weakPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(weakPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, weakPureSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	public TroveSummaryResult generateStrongPureCliqueEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		strongPureSummary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(strongPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(strongPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, strongPureSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	@Override
	public TroveSummaryResult generateWeakTypeEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.TYPED_WEAK));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		weakTypeSummary = new WeakTypeEquivalenceSummaryImpl(weakEquivSummary, typeEquivSummary, rdfSummary); 

		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(weakTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(weakTypeSummary, dataTable);					
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, weakTypeSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}

	@Override
	public TroveSummaryResult generateStrongTypeEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.TYPED_STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		strongTypeSummary = new StrongTypeEquivalenceSummaryImpl(strongEquivSummary, typeEquivSummary, rdfSummary); 
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(strongTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(strongTypeSummary, dataTable, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, strongTypeSummary, classNodesTable, propertyNodesTable, PerformanceLogger.getPerformanceResult());
	}
	
	private String getSummaryTypeMessage(final String summaryType) {
		Builder = new StringBuilder("Building the ");
		Builder.append(summaryType).append(" summary...");		
		
		return Builder.toString();
	}
}
