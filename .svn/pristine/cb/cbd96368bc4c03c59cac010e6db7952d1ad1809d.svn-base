package fr.inria.oak.RDFSummary.summary.trove.triplecreator;

import java.sql.SQLException;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.noderepresenter.TroveNodeRepresenter;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTripleCreatorImpl implements TroveTripleCreator {

	private static TroveNodeRepresenter NodeRepresenter;
	
	/**
	 * @param nodeRepresenter
	 */
	public TroveTripleCreatorImpl(final TroveNodeRepresenter nodeRepresenter) {
		NodeRepresenter = nodeRepresenter;
	}
	
	/**
	 * Retrieves the representative data nodes of the subject and of the object.
	 * If the subject/object are data nodes in G, then they will already have a representative node in the summary,
	 * because in a strong summary all data nodes are represented before passing through the triples.
	 * Therefore, if there does not exist a representative data node for the subject/object, then they must be a class or a property node.
	 * Class and property nodes are represented by themselves.
	 * 
	 * A data triple is created between the representative nodes, with the specified property, if it doesn't already exist in the summary.
	 * 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void representDataTriple(final StrongEquivalenceSummary summary, final int subject, final int property, final int object) throws BerkeleyDbException {
		Preconditions.checkNotNull(summary);		
		
		int source = summary.getSummaryDataNodeForInputDataNode(subject);
		if (source == 0)
			source = subject;
		
		int target = summary.getSummaryDataNodeForInputDataNode(object);
		if (target == 0)
			target = object;		

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}
	
	@Override
	public void representDataTriple(final WeakPureCliqueEquivalenceSummary summary, final int subject, final int property, final int object) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(subject > 0);
		Preconditions.checkArgument(property > 0);
		Preconditions.checkArgument(object > 0);

		boolean subjectIsClassOrProperty = summary.isClassOrProperty(subject);
		boolean objectIsClassOrProperty = summary.isClassOrProperty(object);
		
		int source = subject;
		if (!subjectIsClassOrProperty)
			source = NodeRepresenter.getRepresentativeSourceDataNode(summary, subject, property);

		int target = object;
		if (!objectIsClassOrProperty)
			target = NodeRepresenter.getRepresentativeTargetDataNode(summary, object, property);

		// In the case of paths, the getTarget method may change the source and vice-versa
		if (!subjectIsClassOrProperty)
			source = NodeRepresenter.getRepresentativeSourceDataNode(summary, subject, property);
		
		if (!objectIsClassOrProperty)
			target = NodeRepresenter.getRepresentativeTargetDataNode(summary, object, property);

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}
	
	@Override
	public void representDataTriple(final WeakTypeEquivalenceSummary summary, final int subject, final int property, final int object) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(subject > 0);
		Preconditions.checkArgument(property > 0);
		Preconditions.checkArgument(object > 0);

		int source = subject;
		if (!summary.isClassOrProperty(subject))
			source = NodeRepresenter.getRepresentativeSourceDataNode(summary, subject, property);

		int target = object;
		if (!summary.isClassOrProperty(object))
			target = NodeRepresenter.getRepresentativeTargetDataNode(summary, object, property);

		// In the case of paths, the getTarget method may change the source and vice-versa
		if (!summary.isClassOrProperty(subject))
			source = NodeRepresenter.getRepresentativeSourceDataNode(summary, subject, property);
		if (!summary.isClassOrProperty(object))
			target = NodeRepresenter.getRepresentativeTargetDataNode(summary, object, property);

		if (!summary.existsDataTriple(source, property, target))
			summary.createDataTriple(source, property, target);
	}
	
	/**
	 * Returns true if the type triple has been represented in the summary, otherwise false.
	 * 
	 * Assumes that data triples have already been summarized.
	 * Therefore, if gNode is a data node and has some data properties in G, then it has already been represented by 
	 * a data node in the summary when summarizing data triples.
	 * Otherwise, gNode doesn't have any data properties in G,
	 * i.e. it is typed-only, and will be represented later, thus the method returns false. 
	 * 
	 * If the type triple from G should be represented,
	 * a corresponding type triple is created in the summary, with the representative of gNode
	 * as the source and the specified class IRI as the target.
	 * If gNode is a class or a property, it is represented by itself.
	 *  
	 * The method ensures that the summary type triples are unique.
	 * 
	 * Same as for the strong summary. 
	 *     
	 * @throws BerkeleyDbException 
	 */
	@Override
	public boolean representTypeTriple(final PureCliqueEquivalenceSummary summary, final int gNode, final int classIRI) throws BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkArgument(classIRI > 0);

		if (summary.isClassOrProperty(gNode)) {
			if (!summary.existsTypeTriple(gNode, classIRI))
				summary.createTypeTriple(gNode, classIRI);

			return true;
		}
		else {
			int sDataNode = summary.getSummaryDataNodeForInputDataNode(gNode);
			if (sDataNode == 0) 
				return false;

			if (!summary.existsTypeTriple(sDataNode, classIRI)) 
				summary.createTypeTriple(sDataNode, classIRI);

			return true;
		}
	}

	/**
	 * A single data node is created, representing all gDataNodes and having all the specified classes.
	 * @throws SQLException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void representTypeTriples(final PureCliqueEquivalenceSummary summary, final TIntSet gDataNodes, final TIntSet classes) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);
		Preconditions.checkNotNull(gDataNodes);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		Preconditions.checkArgument(gDataNodes.size() > 0);

		int sDataNode = summary.createDataNode(gDataNodes);
		// Uniqueness: Since the data node is newly created and the classes in the specified set are unique there couldn't have
		// already existed a type edge to any given class
		summary.createTypeTriples(sDataNode, classes);
	}
	
	/**
	 * If gNode is a class or property node the type triple is simply created.
	 * The maps ensures that the summary type triples are unique.
	 * 
	 * If gNode is a data node, the method first tries to retrieve a data node "representing" the specified class set 
	 * (i.e. having all the classes in the class set). 
	 * 
	 * If this node already exists, it is assigned as the representative data node of the specified subject.
	 * Otherwise, a new data node is created, representing the subject and the class set; type triples
	 * are created in the summary between the new data node and all the classes in the class set.
	 * 
	 * 
	 * @throws SQLException 
	 * @throws BerkeleyDbException 
	 */
	@Override
	public void representTypeTriples(final TypeEquivalenceSummary summary, final int gNode, final TIntSet classes) throws SQLException, BerkeleyDbException {
		Preconditions.checkNotNull(summary);		
		Preconditions.checkArgument(gNode > 0);
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);

		if (summary.isClassOrProperty(gNode))
			summary.createTypeTriples(gNode, classes);
		else {
			int sDataNode = summary.getSummaryDataNodeForClasses(classes);
			if (sDataNode == 0) { 
				sDataNode = summary.createDataNode(gNode);
				summary.createTypeTriples(sDataNode, classes);
				summary.storeSummaryDataNodeByClassSet(classes, sDataNode);
				summary.setHighestTypedDataNode(sDataNode);
			}
			else 
				summary.representInputDataNode(gNode, sDataNode);
		}
	}
}
