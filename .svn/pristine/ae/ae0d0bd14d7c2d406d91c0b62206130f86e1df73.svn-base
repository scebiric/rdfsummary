package fr.inria.oak.RDFSummary.data.saturation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Sets;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.saturation.rules.Rule;
import fr.inria.oak.RDFSummary.data.saturation.rules.RuleDomain;
import fr.inria.oak.RDFSummary.data.saturation.rules.RuleRange;
import fr.inria.oak.RDFSummary.data.saturation.rules.RuleSubClass;
import fr.inria.oak.RDFSummary.data.saturation.rules.RuleSubProperty;
import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.termparser.RelaxedTermParser;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.Dictionary;

/**
 * Implementation of the {@link Saturator} interface
 * 
 * @author Damian BURSZTYN
 * @author Sejla CEBIRIC
 *
 */
public class SaturatorImpl implements Saturator {

	private static final Logger log = Logger.getLogger(Saturator.class);
	
	/**
	 * Saturates the triples from the triple batch
	 */
	public Set<Triple> saturate(Collection<Schema> encodedSchemas, TriplesBatch batch, String rdfTypeIri, Dictionary dictionary) {
		Preconditions.checkNotNull(encodedSchemas);
		Preconditions.checkNotNull(batch);
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(rdfTypeIri));
		Preconditions.checkNotNull(dictionary);
		
		final Set<Triple> saturatedTriples = Sets.newHashSet();
		log.info("Saturating subproperties...");
		saturatedTriples.addAll(transitiveSaturationWithRule(encodedSchemas, batch, batch.getPropertyAssertions(), new RuleSubProperty(dictionary)));
		
		log.info("Saturating domains...");
		saturatedTriples.addAll(saturationWithRule(encodedSchemas, batch, new RuleDomain(rdfTypeIri, dictionary)));
		
		log.info("Saturating ranges...");
		saturatedTriples.addAll(saturationWithRule(encodedSchemas, batch, new RuleRange(rdfTypeIri, new RelaxedTermParser(), dictionary)));
		
		log.info("Saturating subclasses...");
		saturatedTriples.addAll(transitiveSaturationWithRule(encodedSchemas, batch, batch.getClassAssertions(), 
				new RuleSubClass(rdfTypeIri, new RelaxedTermParser(), dictionary)));
	
		return saturatedTriples;
	}
	
	private Set<Triple> transitiveSaturationWithRule(Collection<Schema> encodedSchemas, final TriplesBatch batch, final Set<Triple> assertions, final Rule rule) {
		final Set<Triple> saturatedTriples = new HashSet<Triple>();
		final Set<Triple> unchecked = new HashSet<Triple>();

		for (final Triple triple : assertions) {
			for (final Triple producedTriple : rule.produce(triple, encodedSchemas)) {
				if (!batch.containsAssertion(producedTriple)) {
					unchecked.add(producedTriple);
				}
			}
		}
		
		Set<Triple> inProcess = null;
		// Transitive saturation
		while (!unchecked.isEmpty()) {
			saturatedTriples.addAll(unchecked);
			batch.addAll(unchecked);
			inProcess = new HashSet<Triple>(unchecked);
			unchecked.clear();
			for (final Triple triple : inProcess) {
				for (final Triple producedTriple : rule.produce(triple, encodedSchemas)) {
					if (!batch.containsAssertion(producedTriple)) {
						unchecked.add(producedTriple);
					}
				}
			}
		}
		
		return saturatedTriples;
	}

	private Set<Triple> saturationWithRule(Collection<Schema> encodedSchemas, final TriplesBatch batch, final Rule rule) {
		final Set<Triple> saturatedTriples = new HashSet<Triple>();
		for (final Triple triple : batch.getPropertyAssertions()) {
			for (final Triple producedTriple : rule.produce(triple, encodedSchemas)) {
				if (!batch.containsClassAssertion(producedTriple)) {
					saturatedTriples.add(producedTriple);
				}
			}
		}
		batch.addAll(saturatedTriples);
		
		return saturatedTriples;
	}
}
