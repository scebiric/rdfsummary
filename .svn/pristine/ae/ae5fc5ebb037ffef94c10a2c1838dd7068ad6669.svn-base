package fr.inria.oak.RDFSummary.data.saturation.rules;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.RDFSummary.rdfschema.Schema;
import fr.inria.oak.RDFSummary.termparser.TermParser;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.commons.db.Dictionary;

/**
 * Saturates a Triple with respect to the property ranges defined in the
 * schemas.
 *
 * @link http://www.w3.org/TR/rdf-schema/#ch_range
 *
 *       Premise: (_, P, *) & Schema has (P, range, C)
 *
 *       Produces: ($1, type, C)
 *
 * @author Damian Bursztyn
 *
 */
public class RuleRange extends fr.inria.oak.commons.reasoning.rdfs.Rule implements Rule {

	private static final Logger log = Logger.getLogger(Rule.class);
	private static TermParser TermParser;
	private Dictionary dictionary;

	public RuleRange(final String rdfTypeIri, TermParser termParser, Dictionary dictionary) {
		super(rdfTypeIri);
		TermParser = termParser;
		this.dictionary = dictionary;
	}

	/**
	 * Default constructor.
	 */
	public RuleRange() {
		super();
	}

	/**
	 * Premise: (_, P, *) & Schema has (P, range, C)
	 *
	 * @link www.w3.org/TR/rdf11-mt/#rdfs-entailment
	 *       "This process is clearly correct, in that if it gives a positive result then indeed S does RDF (RDFS) entail E. It is not, however, complete: *there are cases of S entailing E which are not detectable by this process*."
	 *       "these can be handled by allowing the rules to apply to a generalization of the RDF syntax in which literals may occur in subject position and blank nodes may occur in predicate position ."
	 * @link http://www.w3.org/TR/rdf11-concepts/#section-Datatypes
	 * @link http://www.w3.org/TR/rdf11-concepts/#section-generalized-rdf
	 *       "Any users of generalized RDF triples, graphs or datasets need to be aware that these notions are non-standard extensions of RDF and their use may cause interoperability problems. There is no requirement on the part of any RDF tool to accept, process, or produce anything beyond standard RDF triples, graphs, and datasets."
	 *       We don't want generalized RDF triples!
	 */
	public boolean matches(final Triple triple, final Schema schema) {
		Preconditions.checkNotNull(triple);
		Preconditions.checkNotNull(schema);
		Triple decoded = RdfUtils.decodeTriple(triple, dictionary);
		log.info("Matching triple: " + decoded.getSubject() + " " + decoded.getProperty() + " " + decoded.getObject());
		
		int object = Integer.parseInt(triple.getObject());
		boolean matches = TermParser.isIRI(DictionaryUtils.getValue(dictionary, object)) && schema.hasRange(triple.getProperty());
		RdfUtils.logMatchingInfo(matches);
		
		return matches;
	}

	/**
	 * ($1, type, C)
	 */
	public Set<Triple> produce(final Triple triple, final Collection<Schema> schemas) {
		Preconditions.checkNotNull(triple);
		Preconditions.checkNotNull(schemas);
		final Set<Triple> triples = new LinkedHashSet<Triple>();
		for (final Schema schema : schemas) {
			if (matches(triple, schema)) {
				for (final String classIRI : schema.getRangesForProperty(triple.getProperty())) { 
					Triple produced = new Triple(triple.getObject(), rdfTypeIri, classIRI);
					triples.add(produced);
					Triple decodedProduced = RdfUtils.decodeTriple(produced, dictionary);
					log.info("Produced triple: " + decodedProduced.getSubject() + " " + decodedProduced.getProperty() + " " + decodedProduced.getObject());
				}
			}
		}

		return triples;
	}
}
