package fr.inria.oak.RDFSummary.data.storage;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TriplesLoadingTimes {

	/** The time to load the data and type triples of an input graph */
	private final String dataAndTypesLoadingTime;
	
	/** The time to load the schema */
	private final String schemaLoadingTime;
	
	/**
	 * Constructor of {@link TriplesLoadingTimes}
	 * 
	 * @param dataAndTypesLoadingTime
	 * @param schemaLoadingTime
	 */
	public TriplesLoadingTimes(final String dataAndTypesLoadingTime, final String schemaLoadingTime) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataAndTypesLoadingTime));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(schemaLoadingTime));
		this.dataAndTypesLoadingTime = dataAndTypesLoadingTime;
		this.schemaLoadingTime = schemaLoadingTime;
	}
	
	public String getDataAndTypesLoadingTime() {
		return this.dataAndTypesLoadingTime;
	}
	
	public String getSchemaLoadingTime() {
		return this.schemaLoadingTime;
	}
}
