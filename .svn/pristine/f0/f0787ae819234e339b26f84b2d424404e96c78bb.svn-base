package fr.inria.oak.RDFSummary.summary.util;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import fr.inria.oak.RDFSummary.summary.trove.SummaryException;
import fr.inria.oak.RDFSummary.summary.trove.TypedWeakSummary;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class AssertUtils {

	/**
	 * Asserts that all the data triples have the same subject
	 * 
	 * @param dataTriples
	 * @return The common subject
	 */
	public static int assertCommonSubject(final List<DataTriple> dataTriples) {
		Preconditions.checkNotNull(dataTriples);
		Preconditions.checkNotNull(dataTriples.size() > 0);

		int source = dataTriples.get(0).getSubject();
		for (DataTriple dataTriple : dataTriples)
			assertEquals(source, dataTriple.getSubject());

		return source;
	}
	
	/**
	 * Asserts that all the data triples have the same object
	 * 
	 * @param dataTriples
	 */
	public static int assertCommonObject(final List<DataTriple> dataTriples) {
		Preconditions.checkNotNull(dataTriples.size() > 0);

		int target = dataTriples.get(0).getObject();
		for (DataTriple dataTriple : dataTriples)
			assertEquals(target, dataTriple.getObject());

		return target;
	}
	
	/**
	 * Asserts that all the data nodes are untyped
	 * 
	 * @param summary
	 * @param dataNodes
	 * @throws SummaryException 
	 * @throws BerkeleyDbException 
	 */
	public static void assertDataNodesUntyped(final TypedWeakSummary summary, final List<Integer> dataNodes) throws SummaryException, BerkeleyDbException {
		Preconditions.checkNotNull(dataNodes);
		Preconditions.checkNotNull(dataNodes.size() > 0);
		for (int dataNode : dataNodes)
			Assert.assertFalse(summary.isTypedDataNode(dataNode));
	}
}
