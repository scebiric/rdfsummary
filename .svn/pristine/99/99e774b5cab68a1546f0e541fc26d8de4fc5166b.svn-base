package fr.inria.oak.RDFSummary.summary.trove.integration;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.params.SummarizerParams;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.EncodedPsqlStorage;
import fr.inria.oak.RDFSummary.data.storage.DatasetPreparationResult;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilder;
import fr.inria.oak.RDFSummary.summary.trove.cliquebuilder.TroveCliqueBuilderImpl;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.commons.db.Dictionary;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveCliqueBuilderImplTest {

	private static final Configurator Configurator = new Configurator();
	private AnnotationConfigApplicationContext context;
	private Dictionary dictionary = null;

	@Before
	public void setUp() throws BeansException, SQLException, ConfigurationException, IOException {
		context = new AnnotationConfigApplicationContext();
		TestUtils.deleteBerkeleyDbDirectory();
	}

	@Test
	public void buildSourceCliques_strongPureCliqueEquivalenceSummary_sourceCliques1() throws ConfigurationException, BeansException, SQLException, FileNotFoundException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, IOException, UnsupportedStorageSchemaException {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/example_sc.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		
		DatasetPreparationResult loadingResult = (context.getBean(EncodedPsqlStorage.class)).prepareDataset(params.getConnectionParams(), params.getSummarizationParams().getSchemaName(), 
					params.getSummarizationParams().dropSchema(), params.getSummarizationParams().getDictionaryTable(), params.getSummarizationParams().getTriplesTable(), 
					params.getSummarizationParams().getDataTriplesFilepath(), params.getSummarizationParams().getSchemaTriplesFilepath(), params.getSummarizationParams().indexing());
		TroveCliqueBuilder builder = new TroveCliqueBuilderImpl(context.getBean(RdfTablesDao.class));
		UriGenerator uriGenerator = context.getBean(UriGenerator.class);
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = context.getBean(BerkeleyDbHandler.class);
	
		String dataTable = NameUtils.getEncodedDataTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable());
		String classesDb = NameUtils.getClassesDbName(dataTable);
		String propertiesDb = NameUtils.getPropertiesDbName(dataTable);				
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivalenceSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivalenceSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);
		StrongPureCliqueEquivalenceSummary summary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivalenceSummary, pureCliqueEquivalenceSummary, rdfSummary); 

		// Call
		builder.buildSourceCliques(loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedDataTable(), summary);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert	
		assertEquals(1, summary.getSourceCliques().size());

		TIntSet sourceClique = summary.getSourceCliques().get(0);
		assertEquals(3, sourceClique.size());
		assertTrue(sourceClique.contains(dictionary.getKey("<author>")));
		assertTrue(sourceClique.contains(dictionary.getKey("<title>")));
		assertTrue(sourceClique.contains(dictionary.getKey("<editor>")));
	}
	
	@Test
	public void buildSourceCliques_strongPureCliqueEquivalenceSummary_sourceCliques2() throws ConfigurationException, BeansException, SQLException, FileNotFoundException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, IOException, UnsupportedStorageSchemaException {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/cs.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		DatasetPreparationResult loadingResult = (context.getBean(EncodedPsqlStorage.class)).prepareDataset(params.getConnectionParams(), params.getSummarizationParams().getSchemaName(), 
					params.getSummarizationParams().dropSchema(), params.getSummarizationParams().getDictionaryTable(), params.getSummarizationParams().getTriplesTable(), 
					params.getSummarizationParams().getDataTriplesFilepath(), params.getSummarizationParams().getSchemaTriplesFilepath(), params.getSummarizationParams().indexing());
		TroveCliqueBuilder builder = new TroveCliqueBuilderImpl(context.getBean(RdfTablesDao.class));
		UriGenerator uriGenerator = context.getBean(UriGenerator.class);
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = context.getBean(BerkeleyDbHandler.class);
		
		String dataTable = NameUtils.getEncodedDataTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable());
		String classesDb = NameUtils.getClassesDbName(dataTable);
		String propertiesDb = NameUtils.getPropertiesDbName(dataTable);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivalenceSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivalenceSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);
		StrongPureCliqueEquivalenceSummary summary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivalenceSummary, pureCliqueEquivalenceSummary, rdfSummary); 

		// Call
		builder.buildSourceCliques(loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedDataTable(), summary);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert	
		assertEquals(3, summary.getSourceCliques().size());
		TIntSet clique = new TIntHashSet();
		clique.add(dictionary.getKey("<p1>"));
		clique.add(dictionary.getKey("<p2>"));
		clique.add(dictionary.getKey("<p3>"));
		clique.add(dictionary.getKey("<p4>"));
		clique.add(dictionary.getKey("<p5>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), clique));
		
		clique.clear();
		clique.add(dictionary.getKey("<p6>"));
		clique.add(dictionary.getKey("<p7>"));
		clique.add(dictionary.getKey("<p8>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), clique));
		
		clique.clear();
		clique.add(dictionary.getKey("<p9>"));
		clique.add(dictionary.getKey("<p10>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), clique));	
	}

	@Test
	public void buildTargetCliques_strongPureCliqueEquivalenceSummary_targetCliques() throws ConfigurationException, BeansException, SQLException, FileNotFoundException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, IOException, UnsupportedStorageSchemaException {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.STRONG, "/strong/example_tc.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		DatasetPreparationResult loadingResult = (context.getBean(EncodedPsqlStorage.class)).prepareDataset(params.getConnectionParams(), params.getSummarizationParams().getSchemaName(), 
					params.getSummarizationParams().dropSchema(), params.getSummarizationParams().getDictionaryTable(), params.getSummarizationParams().getTriplesTable(), 
					params.getSummarizationParams().getDataTriplesFilepath(), params.getSummarizationParams().getSchemaTriplesFilepath(), params.getSummarizationParams().indexing());
		TroveCliqueBuilder builder = new TroveCliqueBuilderImpl(context.getBean(RdfTablesDao.class));
		UriGenerator uriGenerator = context.getBean(UriGenerator.class);
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = context.getBean(BerkeleyDbHandler.class);
		
		String dataTable = NameUtils.getEncodedDataTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable());
		String classesDb = NameUtils.getClassesDbName(dataTable);
		String propertiesDb = NameUtils.getPropertiesDbName(dataTable);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivalenceSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivalenceSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, uriGenerator, dictDao);
		StrongPureCliqueEquivalenceSummary summary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivalenceSummary, pureCliqueEquivalenceSummary, rdfSummary); 

		// Call
		builder.buildTargetCliques(loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedDataTable(), summary);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		assertEquals(1, summary.getTargetCliques().size());
		TIntSet targetClique = summary.getTargetCliques().get(0);
		assertEquals(3, targetClique.size());
		assertTrue(targetClique.contains(dictionary.getKey("<reviewed>")));
		assertTrue(targetClique.contains(dictionary.getKey("<published>")));
		assertTrue(targetClique.contains(dictionary.getKey("<edited>")));
	}

	@Test
	public void buildSourceCliques_strongTypeEquivalenceSummary_sourceCliques() throws ConfigurationException, BeansException, SQLException, FileNotFoundException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, IOException, UnsupportedStorageSchemaException {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.TYPED_STRONG, "/typstrong/example_sc_untyped.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		DatasetPreparationResult loadingResult = (context.getBean(EncodedPsqlStorage.class)).prepareDataset(params.getConnectionParams(), params.getSummarizationParams().getSchemaName(), 
					params.getSummarizationParams().dropSchema(), params.getSummarizationParams().getDictionaryTable(), params.getSummarizationParams().getTriplesTable(), 
					params.getSummarizationParams().getDataTriplesFilepath(), params.getSummarizationParams().getSchemaTriplesFilepath(), params.getSummarizationParams().indexing());
		TroveCliqueBuilder builder = new TroveCliqueBuilderImpl(context.getBean(RdfTablesDao.class));
		UriGenerator uriGenerator = context.getBean(UriGenerator.class);
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = context.getBean(BerkeleyDbHandler.class);
		
		String dataTable = NameUtils.getEncodedDataTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable());
		String classesDb = NameUtils.getClassesDbName(dataTable);
		String propertiesDb = NameUtils.getPropertiesDbName(dataTable);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivalenceSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);		
		StrongTypeEquivalenceSummary summary = new StrongTypeEquivalenceSummaryImpl(strongEquivalenceSummary, typeEquivalenceSummary, rdfSummary);

		// Call
		builder.buildSourceCliques(loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedDataTable(), loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedTypesTable(), summary);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert	
		assertEquals(4, summary.getSourceCliques().size());

		TIntSet set = new TIntHashSet();
		set.add(dictionary.getKey("<editor>"));
		set.add(dictionary.getKey("<comment>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<author>"));
		set.add(dictionary.getKey("<title>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<published>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<reviewed>"));
		assertTrue(TestUtils.contains(summary.getSourceCliques(), set));
	}

	@Test
	public void buildTargetCliques_strongTypeEquivalenceSummary_targetCliques() throws ConfigurationException, BeansException, SQLException, FileNotFoundException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, IOException, UnsupportedStorageSchemaException {
		// Set up
		SummarizerParams params = TestUtils.getTestSummarizerParams(SummaryType.TYPED_STRONG, "/typstrong/example_tc_untyped.nt");
		Configurator.javaSetUpApplicationContext(context, params);
		TestUtils.createStatement(context, params.getSummarizationParams().getFetchSize());
		DatasetPreparationResult loadingResult = (context.getBean(EncodedPsqlStorage.class)).prepareDataset(params.getConnectionParams(), params.getSummarizationParams().getSchemaName(), 
					params.getSummarizationParams().dropSchema(), params.getSummarizationParams().getDictionaryTable(), params.getSummarizationParams().getTriplesTable(), 
					params.getSummarizationParams().getDataTriplesFilepath(), params.getSummarizationParams().getSchemaTriplesFilepath(), params.getSummarizationParams().indexing());
		TroveCliqueBuilder builder = new TroveCliqueBuilderImpl(context.getBean(RdfTablesDao.class));
		UriGenerator uriGenerator = context.getBean(UriGenerator.class);
		DictionaryDao dictDao = context.getBean(DictionaryDao.class);
		BerkeleyDbHandler bdbHandler = context.getBean(BerkeleyDbHandler.class);
		
		String dataTable = NameUtils.getEncodedDataTableName(params.getSummarizationParams().getSchemaName(), params.getSummarizationParams().getTriplesTable());
		String classesDb = NameUtils.getClassesDbName(dataTable);
		String propertiesDb = NameUtils.getPropertiesDbName(dataTable);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivalenceSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);		
		StrongTypeEquivalenceSummary summary = new StrongTypeEquivalenceSummaryImpl(strongEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
	
		// Call
		builder.buildTargetCliques(loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedDataTable(), loadingResult.getTriplesLoadingResult().getStorageLayout().getEncodedTypesTable(), summary);
		dictionary = TestUtils.loadMemoryDictionary(context);

		// Assert
		assertEquals(5, summary.getTargetCliques().size());

		TIntSet set = new TIntHashSet();
		set.add(dictionary.getKey("<editor>"));
		assertTrue(TestUtils.contains(summary.getTargetCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<comment>"));
		assertTrue(TestUtils.contains(summary.getTargetCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<author>"));
		assertTrue(TestUtils.contains(summary.getTargetCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<title>"));
		assertTrue(TestUtils.contains(summary.getTargetCliques(), set));

		set = new TIntHashSet();
		set.add(dictionary.getKey("<reviewed>"));
		set.add(dictionary.getKey("<published>"));
		assertTrue(TestUtils.contains(summary.getTargetCliques(), set));
	}
}
