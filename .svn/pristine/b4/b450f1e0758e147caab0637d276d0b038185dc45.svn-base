package fr.inria.oak.RDFSummary.summary.trove.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 *  
 * @author Sejla CEBIRIC
 *
 */
public class WeakTypeEquivalenceSummaryImplTest {
	
	private UriGenerator uriGenerator;
	private DictionaryDao dictDao;
	private BerkeleyDbHandler bdbHandler;

	private String classAndPropertyDb = "testDb";
	
	@Before
	public void setUp() throws ConfigurationException, IOException {
		uriGenerator = mock(UriGenerator.class);
		dictDao = mock(DictionaryDao.class);
		bdbHandler = mock(BerkeleyDbHandler.class);
		
		TestUtils.deleteBerkeleyDbDirectory();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void unifyUntypedDataNodes_typedDataNodes_exceptionThrown() throws BerkeleyDbException {
		// Setup
		int dataNode1 = 10; 
		int dataNode2 = 20;	
		int dataNode3 = 30;		
		when(bdbHandler.get(classAndPropertyDb, dataNode1, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classAndPropertyDb, dataNode2, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classAndPropertyDb, dataNode3, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		rdfSummary.representInputDataNode(1, dataNode1);
		rdfSummary.representInputDataNode(2, dataNode2);
		rdfSummary.representInputDataNode(3, dataNode3);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		summary.setHighestTypedDataNode(dataNode2);
		
		// Call
		summary.unifyDataNodes(dataNode3, dataNode2);
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreClassOrProperty_dataTripleExistsAndPropertyDoesNotHaveSourceNorTargetDataNode() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		when(bdbHandler.get(classAndPropertyDb, source, Boolean.class)).thenReturn(true);
		when(bdbHandler.get(classAndPropertyDb, target, Boolean.class)).thenReturn(true);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		
		// Run
		summary.createDataTriple(source, property, target);
		
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target));
		assertEquals(0, summary.getSourceDataNodeForDataProperty(property));
		assertEquals(0, summary.getTargetDataNodeForDataProperty(property));
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreTypedDataNodes_dataTripleExistsAndPropertyDoesNotHaveUntypedSourceNorTargetDataNode() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		TIntSet sourceClasses = new TIntHashSet();		
		TIntSet targetClasses = new TIntHashSet();
		sourceClasses.add(4);
		targetClasses.add(5);
		when(bdbHandler.get(classAndPropertyDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classAndPropertyDb, target, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		summary.createTypeTriples(source, sourceClasses);
		summary.createTypeTriples(target, targetClasses);
		summary.setHighestTypedDataNode(target);
		
		// Run
		summary.createDataTriple(source, property, target);
		
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target)); 
		assertEquals(0, summary.getSourceDataNodeForDataProperty(property));
		assertEquals(0, summary.getTargetDataNodeForDataProperty(property));
	}
	
	@Test
	public void createDataTriple_sourceAndTargetAreUntypedDataNodes_dataTripleExistsAndPropertyMappedToSourceAndTargetDataNode() throws BerkeleyDbException {
		// Set up
		int source = 1;
		int property = 2;
		int target = 3;
		TIntSet sourceClasses = new TIntHashSet();		
		TIntSet targetClasses = new TIntHashSet();
		sourceClasses.add(4);
		targetClasses.add(5);
		when(bdbHandler.get(classAndPropertyDb, source, Boolean.class)).thenReturn(false);
		when(bdbHandler.get(classAndPropertyDb, target, Boolean.class)).thenReturn(false);
		RdfSummary rdfSummary = new RdfSummaryImpl(uriGenerator, dictDao, bdbHandler, classAndPropertyDb);
		WeakEquivalenceSummary weakEquivalenceSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivalenceSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		WeakTypeEquivalenceSummary summary = new WeakTypeEquivalenceSummaryImpl(weakEquivalenceSummary, typeEquivalenceSummary, rdfSummary);
		
		// Run
		summary.createDataTriple(source, property, target);
		
		// Assert
		assertTrue(summary.existsDataTriple(source, property, target));
		assertEquals(source, summary.getSourceDataNodeForDataProperty(property));
		assertEquals(target, summary.getTargetDataNodeForDataProperty(property));
	}
}
