package fr.inria.oak.RDFSummary.util;

import java.util.Map;

import org.apache.log4j.Logger;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.RdfFormat;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.rdf.Triple;
import fr.inria.oak.commons.db.Dictionary;

public class RdfUtils {

	private static Logger log = Logger.getLogger(RdfUtils.class);
	
	/**
	 * The specified triple is assumed to have integer values represented as strings.
	 * The methods parses integers from the subject, property and object, and
	 * decodes them by looking up their key in the dictionary. 
	 * 
	 * @param triple
	 * @param dictionary
	 * @return The decoded triple
	 */
	public static Triple decodeTriple(Triple triple, Dictionary dictionary) {
		Triple decoded = null;
		try {
			int subject = Integer.parseInt(triple.getSubject());
			int property = Integer.parseInt(triple.getProperty());
			int object = Integer.parseInt(triple.getObject());
			String subjectIRI = DictionaryUtils.getValue(dictionary, subject);
			String propertyIRI = DictionaryUtils.getValue(dictionary, property);		
			String objectIRI = DictionaryUtils.getValue(dictionary, object);

			decoded = new Triple(subjectIRI, propertyIRI, objectIRI);

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		return decoded;
	}

	public static String getSignature(Triple triple) {
		return triple.getSubject() + "_" + triple.getProperty() + "_" + triple.getObject();
	}

	public static void logMatchingInfo(boolean matches) {
		if (matches)
			log.info("Triple matches");
		else
			log.info("Triple doesn't match.");
	}

	/**
	 * @param subject
	 * @param predicate
	 * @param object
	 * @return
	 */
	public static Triple getTriple(Resource subject, Property predicate, RDFNode object) {
		String subjectStr = subject.getURI();
		String propertyStr = predicate.getURI();
		String objectStr = object.toString();

		if (StringUtils.isNullOrBlank(subjectStr) || StringUtils.isNullOrBlank(propertyStr) || StringUtils.isNullOrBlank(objectStr))
			return null;

		if (subject.isURIResource())
			subjectStr = StringUtils.addBrackets(subjectStr);

		if (!propertyStr.equals(Rdf.TYPE) && !propertyStr.equals(Rdfs.SUBCLASS) && !propertyStr.equals(Rdfs.SUBPROPERTY)
				&& !propertyStr.equals(Rdfs.DOMAIN) && !propertyStr.equals(Rdfs.RANGE))
			propertyStr = StringUtils.addBrackets(propertyStr);

		if (object.isURIResource())
			objectStr = StringUtils.addBrackets(objectStr);
		else if (object.isLiteral()) {
			Literal literal = object.asLiteral();
			objectStr = literal.getValue().toString();
			if (objectStr.contains("\"")) 
				objectStr = objectStr.replaceAll("\"", "''");
			objectStr = StringUtils.addDoubleQuotes(objectStr);
			if (!StringUtils.isNullOrBlank(literal.getDatatypeURI())) 
				objectStr += "^^" + StringUtils.addBrackets(literal.getDatatypeURI());			
		}

		if (subjectStr.contains("'")) 
			subjectStr = subjectStr.replaceAll("'", "''");
		if (propertyStr.contains("'"))
			propertyStr = propertyStr.replaceAll("'", "''");
		if (objectStr.contains("'")) 
			objectStr = objectStr.replaceAll("'", "''");

		if (subjectStr.contains("\"")) 
			subjectStr = subjectStr.replaceAll("\"", "''");
		if (propertyStr.contains("\""))
			propertyStr = propertyStr.replaceAll("\"", "''");

		return new Triple(subjectStr, propertyStr, objectStr);
	}

	/**
	 * @param filename
	 * @return The Jena RDF language based on the filename extension
	 */
	public static String getRDFLanguage(String filename) {
		Map<String, String> extensionLanguageMap = Maps.newHashMap();
		extensionLanguageMap = Maps.newHashMap();
		extensionLanguageMap.put("ttl", "Turtle");
		extensionLanguageMap.put("nt", RdfFormat.N_TRIPLES);
		extensionLanguageMap.put("n3", "N3");
		extensionLanguageMap.put("nq", "N-Quads");
		extensionLanguageMap.put("trig", "TriG");
		extensionLanguageMap.put("rdf", "RDF");
		extensionLanguageMap.put("owl", "RDF/XML");
		extensionLanguageMap.put("jsonld", "JSON-LD");
		extensionLanguageMap.put("trdf", "RDF Thrift");
		extensionLanguageMap.put("rt", "RDF Thrift");
		extensionLanguageMap.put("rj", "RDF/JSON");
		extensionLanguageMap.put("trix", "TriX");
		String ext = Files.getFileExtension(filename);

		return extensionLanguageMap.get(ext);
	}

	/**
	 * @param filename
	 * @return The Jena RDF language based on the filename extension
	 */
	public static String getFileExtensionByRDFFormat(String rdfFormat) {
		Map<String, String> languageExtensionMap = Maps.newHashMap();
		languageExtensionMap = Maps.newHashMap();
		languageExtensionMap.put("Turtle", "ttl");
		languageExtensionMap.put(RdfFormat.N_TRIPLES, "nt");
		languageExtensionMap.put("N3", "n3");
		languageExtensionMap.put("N-Quads", "nq");
		languageExtensionMap.put("TriG", "trig");
		languageExtensionMap.put("RDF/XML", "rdf");
		languageExtensionMap.put("RDF/XML", "owl");
		languageExtensionMap.put("JSON-LD", "jsonld");
		languageExtensionMap.put("RDF Thrift", "trdf");
		languageExtensionMap.put("RDF Thrift", "rt");
		languageExtensionMap.put("RDF/JSON", "rj");
		languageExtensionMap.put("TriX", "trix");

		return languageExtensionMap.get(rdfFormat);
	}

	public static String getShortRDFSProperty(final String propertyIRI) {
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_SUBCLASS)) || propertyIRI.equals(Rdfs.SUBCLASS))
			return "subClass";
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_SUBPROPERTY)) || propertyIRI.equals(Rdfs.SUBPROPERTY))
			return "subProperty";
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_DOMAIN)) || propertyIRI.equals(Rdfs.DOMAIN))
			return "domain";
		if (propertyIRI.contains(StringUtils.removeBrackets(Rdfs.FULL_RANGE)) || propertyIRI.equals(Rdfs.RANGE))
			return "range";

		return null;
	}

	/**
	 * @param str
	 * @return The given string with the namespace removed
	 */
	public static String stripNamespace(String str) {
		if (StringUtils.isNullOrBlank(str))
			return str;

		if (str.startsWith("<") && str.endsWith(">"))
			str = StringUtils.removeBrackets(str);

		if (str.equals(Rdf.FULL_TYPE))
			return "rdf:type";

		int index = str.lastIndexOf("#");
		if (index == -1) {
			index = str.lastIndexOf("/");
			if (index == -1)
				return str;
		}

		return str.substring(index+1, str.length());
	}

	/**
	 * @param propertyIRI
	 * @return True if the specified property is an RDFS property, otherwise false
	 */
	public static boolean isRDFSProperty(String propertyIRI) {
		return propertyIRI.equals(Rdfs.SUBCLASS) || propertyIRI.equals(Rdfs.FULL_SUBCLASS) ||
				propertyIRI.equals(Rdfs.SUBPROPERTY) || propertyIRI.equals(Rdfs.FULL_SUBPROPERTY) ||
				propertyIRI.equals(Rdfs.DOMAIN) || propertyIRI.equals(Rdfs.FULL_DOMAIN) ||
				propertyIRI.equals(Rdfs.RANGE) || propertyIRI.equals(Rdfs.FULL_RANGE);
	}

	/**
	 * @param propertyIRI
	 * @return True if the specified property is an RDF type property, otherwise false
	 */
	public static boolean isRDFTypeProperty(String propertyIRI) {
		return propertyIRI.equals(Rdf.TYPE) || propertyIRI.equals(Rdf.FULL_TYPE);
	}
	
	public static String getTypePropertyFilter(final String propertyColumn, final Integer fullTypeKey, final Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null); 
		
		String filter = "";
		if (fullTypeKey != null)
			filter += propertyColumn + Constants.EQUALS + Integer.toString(fullTypeKey.intValue()) + Constants.OR;
		if (shortTypeKey != null)
			filter += propertyColumn + Constants.EQUALS + Integer.toString(shortTypeKey.intValue());
		if (filter.endsWith(Constants.OR))
			filter = filter.substring(0, filter.length() - Constants.OR.length());		
		
		return filter;
	}
}
