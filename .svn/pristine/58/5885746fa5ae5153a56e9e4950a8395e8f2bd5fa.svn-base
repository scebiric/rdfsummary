package fr.inria.oak.RDFSummary.run.experiments;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.varia.NullAppender;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.inria.oak.RDFSummary.config.Configurator;
import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.config.params.ExperimentsParams;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.dao.QueryException;
import fr.inria.oak.RDFSummary.data.loader.JenaException;
import fr.inria.oak.RDFSummary.data.storage.PsqlStorageException;
import fr.inria.oak.RDFSummary.run.Summarizer;
import fr.inria.oak.RDFSummary.summary.trove.summarizer.TroveSummaryResult;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;
import fr.inria.oak.commons.db.UnsupportedDatabaseEngineException;
import fr.inria.oak.commons.rdfdb.UnsupportedStorageSchemaException;
import fr.inria.oak.commons.reasoning.rdfs.SchemaException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class Completeness {

	private static Logger log = Logger.getLogger(SummarizationTime.class);
	private static AnnotationConfigApplicationContext Context = null;

	/**
	 * @param args
	 * @throws ConfigurationException
	 * @throws SQLException
	 * @throws JenaException
	 * @throws PsqlStorageException
	 * @throws SchemaException
	 * @throws UnsupportedDatabaseEngineException
	 * @throws DictionaryException
	 * @throws InexistentValueException
	 * @throws InexistentKeyException
	 * @throws IOException
	 * @throws ParametersException
	 * @throws QueryException
	 * @throws UnsupportedStorageSchemaException 
	 * @throws BerkeleyDbException 
	 * @throws SummaryException 
	 */
	public static void main(String[] args) throws ConfigurationException, SQLException, JenaException, PsqlStorageException, SchemaException, UnsupportedDatabaseEngineException, DictionaryException, InexistentValueException, InexistentKeyException, IOException, ParametersException, QueryException, UnsupportedStorageSchemaException, BerkeleyDbException {
		// Setup
		Configurator configurator = new Configurator();		
		ExperimentsParams params = configurator.loadExperimentsParameters(args);
		if (!params.logging()) {
			Logger.getRootLogger().removeAllAppenders();
			Logger.getRootLogger().addAppender(new NullAppender());
		}
		Context = new AnnotationConfigApplicationContext();
		configurator.javaSetUpApplicationContext(Context, params);
		CompletenessResult completenessResult = new CompletenessResult();
		
		if (StringUtils.isNullOrBlank(params.getSummarizerParams().getSummarizationParams().getSchemaTriplesFilepath()))
			throw new IllegalArgumentException("Schema is required.");
		
		// sum(sat(G))
		params.getSummarizerParams().getSummarizationParams().setSaturateInput(true);
		Summarizer summarizer = new Summarizer();
		TroveSummaryResult summaryResult = summarizer.summarize(params.getSummarizerParams());
		long satTime = summaryResult.getDatasetPreparationResult().getSaturationTime();
		long summTime = summaryResult.getStatistics().getSummarizationTime();		
		completenessResult.setTimeToSaturateG(satTime);
		completenessResult.setTimeToSummarizeSaturatedG(summTime);
		
		// sum(G)
		params.getSummarizerParams().getSummarizationParams().setSaturateInput(false);
		params.getSummarizerParams().getExportParams().setExportToRdfFile(true);
		summaryResult = summarizer.summarize(params.getSummarizerParams());
		summTime = summaryResult.getStatistics().getSummarizationTime();
		completenessResult.setTimeToSummarizeG(summTime);
		
		// sum(sat(sum(G)))
		params.getSummarizerParams().getSummarizationParams().setSaturateInput(true);
		params.getSummarizerParams().getSummarizationParams().setDataTriplesFilepath(summaryResult.getSummaryDataFilepath());
		params.getSummarizerParams().getSummarizationParams().setSchemaName(summaryResult.getSummaryName());
		summaryResult = summarizer.summarize(params.getSummarizerParams());
		summTime = summaryResult.getStatistics().getSummarizationTime();
		completenessResult.setTimeToSaturateSummarizedG(summaryResult.getDatasetPreparationResult().getSaturationTime());
		completenessResult.setTimeToSummarizeSaturatedSummaryOfG(summTime);
		
		log.info("Time to saturate G: " + appendMilliseconds(completenessResult.getTimeToSaturateG()));
		log.info("Time to summarize saturated G: " + appendMilliseconds(completenessResult.getTimeToSummarizeSaturatedG()));
		log.info("Total sum(sat(G)) time: " + appendMilliseconds(completenessResult.getTotalTimeToSumSatG()));
		log.info(""); 
		log.info("Time to summarize G: " + appendMilliseconds(completenessResult.getTimeToSummarizeG()));
		log.info("Time to saturate summarized G: " + appendMilliseconds(completenessResult.getTimeToSaturateSummarizedG()));
		log.info("Time to summarize saturated summary of G: " + appendMilliseconds(completenessResult.getTimeToSummarizeSaturatedSummaryOfG()));
		log.info("Total sum(sat(sum(G))) time: " + appendMilliseconds(completenessResult.getTotalTimeToSumSatSumG()));
	
		// Close context
		closeApplicationContext();
	}

	private static String appendMilliseconds(final long time) {
		return time + Constants.MILLISECONDS;
	}

	/**
	 * Closes the application context
	 */
	public static void closeApplicationContext() {
		Context.close();
	}

}
