package fr.inria.oak.RDFSummary.data.queries;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.constants.Constants;
import fr.inria.oak.RDFSummary.constants.Rdf;
import fr.inria.oak.RDFSummary.constants.Rdfs;
import fr.inria.oak.RDFSummary.util.DictionaryUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.Dictionary;

/**
 * @author Sejla CEBIRIC
 *
 */
public class RdfTablesQueriesImpl implements RdfTablesQueries {

	private static StringBuilder Builder = null;

	private static final String URI = "uri";
	
	@Override
	public String distinctSpOrderByS(String dataTable) {
		return "SELECT DISTINCT s, p FROM " + dataTable + " ORDER BY s;";
	}

	@Override
	public String distinctOpOrderByO(String dataTable) {
		return "SELECT DISTINCT o, p FROM " + dataTable + " ORDER BY o;";
	}

	@Override
	public String distinctSpOrderBySWithSUntyped(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.s, data.p, typ.o AS " + Constants.TYPE_ALIAS + " " 
				+ "FROM " + dataTable + " data "
				+ "LEFT OUTER JOIN " + typesTable + " typ "
				+ "ON data.s=typ.s "
				+ "WHERE typ.o IS NULL "
				+ "ORDER BY data.s;";
	}

	@Override
	public String distinctOpOrderByOWithOUntyped(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.o, data.p, typ.o AS " + Constants.TYPE_ALIAS + " " 
				+ "FROM " + dataTable + " data "
				+ "LEFT OUTER JOIN " + typesTable + " typ "
				+ "ON data.o=typ.s "
				+ "WHERE typ.o IS NULL "
				+ "ORDER BY data.o;";
	}	

	@Override
	public String subjectTypesByProperties(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.p, type.o "
				+ "FROM " + dataTable + " data JOIN " + typesTable + " type ON data.s = type.s;";
	}

	@Override
	public String objectTypesByProperties(String dataTable, String typesTable) {
		return "SELECT DISTINCT data.p, type.o "
				+ "FROM " + dataTable + " data JOIN " + typesTable + " type ON data.o = type.s;";
	}

	@Override
	public String createTriplesTable(String table) {
		return "CREATE TABLE " + table + "(" + Constants.SUBJECT + " text NOT NULL, " + Constants.PROPERTY + " text NOT NULL, " + Constants.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createTriplesTableWithSkippedChars(String table) {
		return "CREATE TABLE " + table + "(" + Constants.SUBJECT + " text NOT NULL, " + Constants.PROPERTY + " text NOT NULL, " + Constants.OBJECT +" text NOT NULL, " + Constants.SKIPPED_CHARS + " text);";
	}

	@Override
	public String createEncodedTriplesTable(String encodedTableName) {
		return "CREATE TABLE " + encodedTableName + "(" + Constants.SUBJECT + " integer NOT NULL, " + Constants.PROPERTY + " integer NOT NULL, " + Constants.OBJECT +" integer NOT NULL);";
	}
	
	@Override
	public String createEncodedTriplesTableWithId(String encodedTableName) {
		return "CREATE TABLE " + encodedTableName + "(id SERIAL UNIQUE NOT NULL, " + Constants.SUBJECT + " integer NOT NULL, " + Constants.PROPERTY + " integer NOT NULL, " + Constants.OBJECT +" integer NOT NULL);";
	}

	@Override
	public String createDataTriplesTable(String table) {
		return "CREATE TABLE " + table + "(id SERIAL UNIQUE NOT NULL, " + Constants.SUBJECT + " text NOT NULL, " + Constants.PROPERTY + " text NOT NULL, " + Constants.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createTypesTable(String typesTable) {
		return "CREATE TABLE " + typesTable + "(id SERIAL UNIQUE NOT NULL, " + Constants.SUBJECT + " text NOT NULL, " + Constants.OBJECT +" text NOT NULL);";
	}

	@Override
	public String createEncodedTypesTableWithId(String encodedTypesTable) {
		return "CREATE TABLE " + encodedTypesTable + "(id SERIAL UNIQUE NOT NULL, " + Constants.SUBJECT + " integer NOT NULL, " + Constants.OBJECT +" integer NOT NULL);";
	}

	@Override
	public String createEncodedUriTable(String table) {
		return "CREATE TABLE " + table + "(uri integer NOT NULL);";
	}

	@Override
	public String populateTypesTable(String typesTable, String triplesTable) {
		return "INSERT INTO " + typesTable + " (s, o) (SELECT s, o FROM " + triplesTable + " WHERE p='" + Rdf.FULL_TYPE + "' OR p='" + Rdf.TYPE + "')";
	}

	@Override
	public String insert(String targetTable, String sourceTable, String sourceColumn, int propertyKey) {
		return "INSERT INTO " + targetTable + " (SELECT DISTINCT " + sourceColumn + " FROM " + sourceTable + " WHERE p=" + propertyKey + ");";
	}

	@Override
	public String insertClasses(String clsTable, String encodedTypesTable, String column) {
		return "INSERT INTO " + clsTable + " (SELECT DISTINCT " + column + " FROM " + encodedTypesTable + ");";
	}

	@Override
	public String populateEncodedTypesTable(String encodedTypesTable, String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null);

		return "INSERT INTO " + encodedTypesTable + " (s, o) (SELECT s, o FROM " + encodedTriplesTable + 
				" WHERE " + RdfUtils.getTypePropertyFilter(Constants.PROPERTY, fullTypeKey, shortTypeKey) + ");";
	}
	
	@Override
	public String populateDataTriplesTable(String dataTable, String triplesTable) {
		return "INSERT INTO " + dataTable + " (s, p, o) (SELECT s, p, o FROM " + triplesTable + " WHERE " + 
				"p <> '" + Rdf.FULL_TYPE + "' AND " + 
				"p <> '" + Rdf.TYPE + "' AND " + 
				"p <> '" + Rdfs.FULL_DOMAIN + "' AND " +
				"p <> '" + Rdfs.DOMAIN + "' AND " +
				"p <> '" + Rdfs.FULL_RANGE + "' AND " +
				"p <> '" + Rdfs.RANGE + "' AND " +
				"p <> '" + Rdfs.FULL_SUBCLASS + "' AND " +
				"p <> '" + Rdfs.SUBCLASS + "' AND " +
				"p <> '" + Rdfs.FULL_SUBPROPERTY + "' AND " +
				"p <> '" + Rdfs.SUBPROPERTY + "')";
	}

	@Override
	public String populateEncodedDataTriplesTable(String encodedDataTable, String encodedTriplesTable, Dictionary dictionary) {
		String query = "INSERT INTO " + encodedDataTable + " (s, p, o) (SELECT s, p, o FROM " + encodedTriplesTable;
		String filter = getDataPropertyFilter(Constants.PROPERTY, dictionary);
		if (!StringUtils.isNullOrBlank(filter))
			query += " WHERE " + filter;

		return query.concat(");");
	}

	@Override
	public String createClassesAndPropertiesTable(String clsPropsTable, String encodedDataTable, String encodedTypesTable) {
		return "CREATE TABLE " + clsPropsTable + 
				" AS (SELECT DISTINCT p AS uri FROM " + encodedDataTable + 
				" UNION SELECT DISTINCT o AS uri FROM " + encodedTypesTable + ");";
	}

	@Override
	public String createTypeSubjectsAndClassesTable(String subjClsTable, String summaryTypesTable) {
		return "CREATE TABLE " + subjClsTable + 
				" AS (SELECT DISTINCT o AS uri FROM " + summaryTypesTable + 
				" UNION SELECT DISTINCT s AS uri FROM " + summaryTypesTable + ");";
	}

	@Override
	public String createDataPropertiesTable(String propsTable, String encodedDataTable) {
		return "CREATE TABLE " + propsTable + 
				" AS (SELECT DISTINCT p AS uri FROM " + encodedDataTable + ");";
	}

	@Override
	public String createClassesTable(String clsTable, String encodedTypesTable) {
		return "CREATE TABLE " + clsTable + 
				" AS (SELECT DISTINCT o AS uri FROM " + encodedTypesTable + ");";
	}

	@Override
	public String distinctDataProperties(String encodedTriplesTable, Dictionary dictionary) {
		Builder = new StringBuilder("SELECT DISTINCT p FROM " + encodedTriplesTable);
		final String filter = getDataPropertyFilter(Constants.PROPERTY, dictionary);
		if (!StringUtils.isNullOrBlank(filter))
			Builder.append(Constants.WHERE).append(filter);
		
		Builder.append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
	
	@Override
	public String distinctClasses(String encodedTriplesTable, Integer fullTypeKey, Integer shortTypeKey) {
		Preconditions.checkArgument(fullTypeKey != null || shortTypeKey != null);
		
		Builder = new StringBuilder("SELECT DISTINCT o FROM " + encodedTriplesTable);
		Builder.append(Constants.WHERE).append(RdfUtils.getTypePropertyFilter(Constants.PROPERTY, fullTypeKey, shortTypeKey));
		Builder.append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	private String getDataPropertyFilter(String propertyColumn, Dictionary dictionary) {
		String filter = "";
		Integer type = DictionaryUtils.getKey(dictionary, Rdf.TYPE);
		Integer subclass = DictionaryUtils.getKey(dictionary, Rdfs.SUBCLASS);
		Integer subproperty = DictionaryUtils.getKey(dictionary, Rdfs.SUBPROPERTY);
		Integer domain = DictionaryUtils.getKey(dictionary, Rdfs.DOMAIN);
		Integer range = DictionaryUtils.getKey(dictionary, Rdfs.RANGE);
		Integer fullType = DictionaryUtils.getKey(dictionary, Rdf.FULL_TYPE);
		Integer fullSubclass = DictionaryUtils.getKey(dictionary, Rdfs.FULL_SUBCLASS);
		Integer fullSubproperty = DictionaryUtils.getKey(dictionary, Rdfs.FULL_SUBPROPERTY);
		Integer fullDomain = DictionaryUtils.getKey(dictionary, Rdfs.FULL_DOMAIN);
		Integer fullRange = DictionaryUtils.getKey(dictionary, Rdfs.FULL_RANGE);

		if (type != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + type.intValue() + Constants.AND);
		if (subclass != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + subclass.intValue() + Constants.AND);
		if (subproperty != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + subproperty.intValue() + Constants.AND);
		if (domain != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + domain.intValue() + Constants.AND);
		if (range != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + range.intValue() + Constants.AND);
		if (fullType != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + fullType.intValue() + Constants.AND);
		if (fullSubclass != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + fullSubclass.intValue() + Constants.AND);
		if (fullSubproperty != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + fullSubproperty.intValue() + Constants.AND);
		if (fullDomain != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + fullDomain.intValue() + Constants.AND);
		if (fullRange != null)
			filter = filter.concat(propertyColumn + Constants.NOT_EQUALS + fullRange.intValue() + Constants.AND);

		if (filter.endsWith(Constants.AND))
			filter = filter.substring(0, filter.length() - Constants.AND.length());

		return filter;
	}

	@Override
	public String createDecodedRepresentationTable(String reprTable, String encReprTable, String dictionaryTable) {
		return "CREATE TABLE " + reprTable + " AS "
				+ "(SELECT dict1.value AS " + Constants.INPUT_DATA_NODE + ", dict2.value AS " + Constants.SUMMARY_DATA_NODE + 
				" FROM " + encReprTable + " enc_repr" + 
				" JOIN " + dictionaryTable + " dict1 ON dict1.key=enc_repr." + Constants.INPUT_DATA_NODE +
				" JOIN " + dictionaryTable + " dict2 ON dict2.key=enc_repr." + Constants.SUMMARY_DATA_NODE + ");";
	}

	@Override
	public String createEncodedRepresentationTable(String table, String column1, String column2) {
		return "CREATE TABLE " + table + " (" + column1 + " integer NOT NULL, " + column2 + " integer NOT NULL);";
	}

	@Override
	public String existsCommonSource(String encodedTableName, int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.s = table2.s "
				+ "AND (table1.p = '" + p1 + "' AND table2.p = '" + p2 + 
				"' OR table1.p = '" + p2 + "' AND table2.p = '" + p1 + "'));";
	}

	@Override
	public String existsCommonTarget(String encodedTableName,  int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.o = table2.o "
				+ "AND (table1.p = '" + p1 + "' AND table2.p = '" + p2 + 
				"' OR table1.p = '" + p2 + "' AND table2.p = '" + p1 + "'));";
	}

	@Override
	public String existsPropertyPath(String encodedTableName,  int p1, int p2) {
		return "SELECT EXISTS (SELECT 1 FROM " + encodedTableName + " table1, " + encodedTableName + " table2 "
				+ "WHERE table1.o = table2.s AND table1.p = '" + p1 + 
				"' AND table2.p = '" + p2 + "');";
	}

	@Override
	public String insertTriple(String table) {
		return "INSERT INTO " + table + " (s, p, o) VALUES (?, ?, ?);";
	}

	@Override
	public String isEncoded(String constant, String dictionaryTableName) {
		return "SELECT EXISTS (SELECT 1 FROM " + dictionaryTableName + " WHERE value = '" + constant + "');";
	}

	@Override
	public String createAuxiliarySaturatedTriplesTable(String auxiliarySaturatedTriplesTable, String triplesTable) {
		return "CREATE TABLE " + auxiliarySaturatedTriplesTable + " AS (SELECT s, p, o FROM " + triplesTable + ");";
	}

	@Override
	public String createSaturatedTriplesTable(String saturatedTriplesTable, String auxiliarySaturatedTriplesTable) {
		return "CREATE TABLE " + saturatedTriplesTable + 
				" AS (SELECT s, p, o FROM " + auxiliarySaturatedTriplesTable + " GROUP BY s, p, o);";
	}

	@Override
	public String decodeSummaryData(String dataTable, String encodedDataTable, String dictionaryTable) {
		return "CREATE TABLE " + dataTable + " AS (SELECT DISTINCT dict1.value AS s, dict2.value AS p, dict3.value AS o "
				+ "FROM " + encodedDataTable + " data "
				+ "JOIN " + dictionaryTable + " dict1 on data.s=dict1.key "
				+ "JOIN " + dictionaryTable + " dict2 on data.p=dict2.key "
				+ "JOIN " + dictionaryTable + " dict3 on data.o=dict3.key);";
	}

	@Override
	public String decodeSummaryTypes(String typesTable, String encodedTypesTable, String dictionaryTable) {
		return "CREATE TABLE " + typesTable + " AS (SELECT DISTINCT dict1.value AS s, dict2.value AS o "
				+ "FROM " + encodedTypesTable + " data "
				+ "JOIN " + dictionaryTable + " dict1 on data.s=dict1.key "
				+ "JOIN " + dictionaryTable + " dict2 on data.o=dict2.key);";
	}

	@Override
	public String classesFromTypeComponent(String encodedTypesTable) {
		return "SELECT DISTINCT o FROM " + encodedTypesTable + ";";
	}

	@Override
	public String copyTriplesToCsv(String table, String[] columns, String filepath) {
		Preconditions.checkNotNull(columns);
		Preconditions.checkArgument(columns.length > 0);

		Builder = new StringBuilder("COPY " + table + " (");
		for (int i = 0; i < columns.length - 1; i++)
			Builder.append(columns[i]).append(", ");

		Builder.append(columns[columns.length - 1]).append(") TO '").append(filepath).append("' WITH csv;");

		return Builder.toString();
	}

	@Override
	public String createAndPopulateEncodedDistinctNodesTable(String encodedDistinctNodesTable, String encodedDataTable) {
		Builder = new StringBuilder("CREATE TABLE ");
		Builder.append(encodedDistinctNodesTable).append(Constants.AS).append(Chars.LEFT_PAREN);
		Builder.append("SELECT DISTINCT ").append(URI);
		Builder.append(" FROM (SELECT DISTINCT s AS ").append(URI).append(" FROM ").append(encodedDataTable);
		Builder.append(" UNION SELECT DISTINCT o AS ").append(URI).append(" FROM ").append(encodedDataTable).append(Chars.RIGHT_PAREN);
		Builder.append(Constants.AS).append(URI).append(Chars.SPACE);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String populateEncodedSummaryTable(String encodedSummary, String summaryEncodedDataTable, String summaryEncodedTypesTable,
			int typePropertyKey) {
		return "INSERT INTO " + encodedSummary + " (s, p, o) (SELECT s, p, o FROM " + summaryEncodedDataTable + 
				" UNION SELECT s, " + typePropertyKey + ", o FROM " + summaryEncodedTypesTable + ");";
	}

	@Override
	public String createDecodedSummaryTable(String summaryTable, String summaryDataTable, String summaryTypesTable) {
		return "CREATE TABLE " + summaryTable + " AS (SELECT s, p, o FROM " + summaryDataTable + 
				" UNION SELECT s, '" + Rdf.FULL_TYPE + "', o FROM " + summaryTypesTable + ");";
	}

	@Override
	public String createEncodedSoTable(String encodedSoTable, String decodedTriplesTable, String dictionaryTable) {
		return "CREATE TABLE " + encodedSoTable + " AS (SELECT dict1.key AS s, dict2.key AS o "
				+ "FROM " + decodedTriplesTable + " triples " 
				+ "JOIN " + dictionaryTable + " dict1 ON triples.s=dict1.value " 
				+ "JOIN " + dictionaryTable + " dict2 ON triples.o=dict2.value);";
	}

	@Override
	public String classSupport(int encodedClassIRI, String encodedDistinctNodesTable, String encodedTypesTable) {
		Builder = new StringBuilder("SELECT COUNT(*) FROM ");
		Builder.append(encodedDistinctNodesTable).append(Chars.SPACE).append(Constants.ENCODED_DISTINCT_NODES_TABLE);
		Builder.append(Constants.JOIN);
		Builder.append(encodedTypesTable).append(Chars.SPACE).append(Constants.TYPES);
		Builder.append(Constants.WHERE).append(Constants.ENCODED_DISTINCT_NODES_TABLE).append(Chars.DOT).append(URI);
		Builder.append(Chars.EQUALS).append(Constants.TYPES).append(Chars.DOT).append(Constants.OBJECT).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String dataPropertySupport(int encodedDataProperty, String encodedDataTable) {
		Builder = new StringBuilder("SELECT COUNT(").append(Constants.DATA).append(Chars.DOT).append(Constants.PROPERTY).append(Chars.RIGHT_PAREN);
		Builder.append(Constants.FROM).append(encodedDataTable).append(Chars.SPACE).append(Constants.DATA);
		Builder.append(Constants.WHERE).append(Constants.DATA).append(Chars.DOT).append(Constants.PROPERTY).append(Chars.EQUALS).append(encodedDataProperty);
		Builder.append(Constants.GROUP_BY).append(Constants.DATA).append(Chars.DOT).append(Constants.PROPERTY).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String exists(String sourceTable, String sourceColumn, int propertyKey) {
		Builder = new StringBuilder(Constants.SELECT);
		Builder.append(Constants.EXISTS).append(Chars.LEFT_PAREN);
		Builder.append(Constants.SELECT).append(1);
		Builder.append(Constants.FROM).append(sourceTable);
		Builder.append(Constants.WHERE).append(sourceColumn).append(Chars.EQUALS).append(propertyKey);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String insertPossiblePropertyNodes(String possiblePropertyNodesTable, String schemaPropertiesTable, String encodedDataTable) {
		Builder = new StringBuilder(Constants.INSERT_INTO);
		Builder.append(possiblePropertyNodesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		
		Builder.append(Constants.SELECT).append(Constants.DISTINCT).append(Constants.URI).append(Constants.AS).append(Constants.URI);
		Builder.append(Constants.FROM).append(Chars.LEFT_PAREN);
				
		Builder.append(Constants.SELECT).append(Constants.DISTINCT).append(Constants.URI).append(Constants.AS).append(Constants.URI);
		Builder.append(Constants.FROM).append(schemaPropertiesTable);
		Builder.append(Constants.UNION);
		Builder.append(Constants.SELECT).append(Constants.DISTINCT).append(Constants.PROPERTY).append(Constants.AS).append(Constants.URI);
		Builder.append(Constants.FROM).append(encodedDataTable);
				
		Builder.append(Chars.RIGHT_PAREN); // closes SELECT DISTINCT uri as uri FROM (
		Builder.append(Constants.AS).append(Constants.URI);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON); // closes INSERT INTO (
		
		return Builder.toString();
	}

	@Override
	public String copySchemaPropertyNodes(String propertiesTable, String schemaPropertiesTable) {
		Builder = new StringBuilder(Constants.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		Builder.append(Constants.SELECT).append(Chars.STAR).append(Constants.FROM).append(schemaPropertiesTable);
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String getPropertyNodesFromDataTable(String propertiesTable, String possiblePropertyNodesTable, String encodedDataTable, String dataColumn) {
		final String pnodesAlias = "pnodes";
		Builder = new StringBuilder(Constants.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		
		Builder.append(Constants.SELECT).append(prefixedColumn(pnodesAlias, Constants.URI)).append(Constants.AS).append(Constants.URI);
		Builder.append(Constants.FROM).append(possiblePropertyNodesTable).append(Chars.SPACE).append(pnodesAlias).append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(encodedDataTable).append(Chars.SPACE).append(Constants.DATA);
		Builder.append(Constants.WHERE).append(prefixedColumn(pnodesAlias, Constants.URI)).append(Chars.EQUALS).append(prefixedColumn(Constants.DATA, dataColumn));
		
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
		
	}
	
	@Override
	public String getPropertyNodesFromTypesTable(String propertiesTable, String possiblePropertyNodesTable, String encodedTypesTable, String typesColumn) {
		final String pnodesAlias = "pnodes";
		Builder = new StringBuilder(Constants.INSERT_INTO);
		Builder.append(propertiesTable).append(Chars.SPACE).append(Chars.LEFT_PAREN);
		
		Builder.append(Constants.SELECT).append(prefixedColumn(pnodesAlias, Constants.URI)).append(Constants.AS).append(Constants.URI);
		Builder.append(Constants.FROM).append(possiblePropertyNodesTable).append(Chars.SPACE).append(pnodesAlias).append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(encodedTypesTable).append(Chars.SPACE).append(Constants.TYPES);
		Builder.append(Constants.WHERE).append(prefixedColumn(pnodesAlias, Constants.URI)).append(Chars.EQUALS).append(prefixedColumn(Constants.TYPES, typesColumn));
		
		Builder.append(Chars.RIGHT_PAREN).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	private String prefixedColumn(String alias, String columnName) {
		final StringBuilder builder = new StringBuilder(alias);
		builder.append(Chars.DOT).append(columnName);
		
		return builder.toString();
	}

	@Override
	public String getDistinctClassAndPropertyNodeCount(String classesTable, String propertiesTable) {
		Builder = new StringBuilder();
		Builder.append(Constants.SELECT).append(Constants.COUNT).append(Chars.LEFT_PAREN).append(Chars.STAR).append(Chars.RIGHT_PAREN);
		Builder.append(Constants.FROM).append(Chars.LEFT_PAREN);
		
		Builder.append(Constants.SELECT).append(Constants.URI).append(Constants.FROM).append(classesTable);
		Builder.append(Constants.UNION);
		Builder.append(Constants.SELECT).append(Constants.URI).append(Constants.FROM).append(propertiesTable);
		
		Builder.append(Chars.RIGHT_PAREN).append(Constants.AS).append("count").append(Chars.SEMICOLON);
		
		return Builder.toString();
	}
}