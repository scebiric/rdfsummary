package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.old.DataTriple;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class StrongEquivalenceSummaryImpl implements StrongEquivalenceSummary {

	private final RdfSummary rdfSummary;
	
	/** The list of source cliques */
	private final List<TIntSet> sourceCliques;
	
	/** The list of target cliques */
	private final List<TIntSet> targetCliques;
	
	/** The mapping of subjects from G and the source clique of their properties */
	private final TIntIntMap sourceCliqueIdForSubject;
	
	/** The mapping of objects from G and the target clique of their properties */
	private final TIntIntMap targetCliqueIdForObject;
	
	/** The mapping of source clique IDs and the summary data nodes having the source clique */
	private final TIntObjectMap<TIntSet> dataNodesForSourceCliqueId;
	
	/** The mapping of target clique IDs and the summary data nodes having the target clique */
	private final TIntObjectMap<TIntSet> dataNodesForTargetCliqueId;
	
	/**
	 * @param rdfSummary
	 */
	public StrongEquivalenceSummaryImpl(final RdfSummary rdfSummary) {
		this.rdfSummary = rdfSummary;
		this.sourceCliques = Lists.newArrayList();
		this.targetCliques = Lists.newArrayList();
		this.sourceCliqueIdForSubject = new TIntIntHashMap();
		this.targetCliqueIdForObject = new TIntIntHashMap();
		this.dataNodesForSourceCliqueId = new TIntObjectHashMap<TIntSet>();
		this.dataNodesForTargetCliqueId = new TIntObjectHashMap<TIntSet>();		
	}

	@Override
	public int getSourceCliqueIdBySubject(final int subject) {
		return this.sourceCliqueIdForSubject.get(subject);
	}
	
	@Override
	public int getTargetCliqueIdByObject(final int object) {
		return this.targetCliqueIdForObject.get(object);
	}
	
	@Override 
	public void setSourceCliqueIdForSubject(final int subject, final int sourceClique) {
		this.sourceCliqueIdForSubject.put(subject, sourceClique);
	}
	
	@Override 
	public void setTargetCliqueIdForObject(final int object, final int targetClique) {
		this.targetCliqueIdForObject.put(object, targetClique);
	}
	
	@Override
	public void clearSourceCliques() {
		this.sourceCliques.clear();
	}

	@Override
	public void clearTargetCliques() {
		this.targetCliques.clear();
	}
	
	@Override
	public TIntSet getDataNodesForSourceCliqueId(final int sourceCliqueId) {
		return this.dataNodesForSourceCliqueId.get(sourceCliqueId);
	}
	
	@Override
	public TIntSet getDataNodesForTargetCliqueId(final int targetCliqueId) {
		return this.dataNodesForTargetCliqueId.get(targetCliqueId);
	}
	
	@Override
	public void storeDataNodeForSourceCliqueId(final int sourceCliqueId, final int dataNode) throws BerkeleyDbException {
		Preconditions.checkArgument(!rdfSummary.isClassOrProperty(dataNode));
		
		TIntSet dataNodes = dataNodesForSourceCliqueId.get(sourceCliqueId);
		if (dataNodes == null) {
			dataNodes = new TIntHashSet();
			dataNodes.add(dataNode);
			dataNodesForSourceCliqueId.put(sourceCliqueId, dataNodes);
		}
		else
			dataNodes.add(dataNode);	
	}
	
	@Override
	public void storeDataNodeForTargetCliqueId(final int targetCliqueId, final int dataNode) throws BerkeleyDbException {
		Preconditions.checkArgument(!rdfSummary.isClassOrProperty(dataNode));
		
		TIntSet dataNodes = dataNodesForTargetCliqueId.get(targetCliqueId);
		if (dataNodes == null) {
			dataNodes = new TIntHashSet();
			dataNodes.add(dataNode);
			dataNodesForTargetCliqueId.put(targetCliqueId, dataNodes);
		}
		else
			dataNodes.add(dataNode);
	}

	@Override
	public List<TIntSet> getSourceCliques() {
		return this.sourceCliques;
	}

	@Override
	public List<TIntSet> getTargetCliques() {
		return this.targetCliques;
	}

	@Override
	public void clearCliqueMaps() {
		sourceCliqueIdForSubject.clear();
		targetCliqueIdForObject.clear();
		dataNodesForSourceCliqueId.clear();
		dataNodesForTargetCliqueId.clear();
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException, BerkeleyDbException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		rdfSummary.createTypeTriples(sNode, classes);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrProperty(int iri) throws BerkeleyDbException {
		return rdfSummary.isClassOrProperty(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}

	@Override
	public void createDataTriple(int source, int dataProperty, int target) throws BerkeleyDbException {
		rdfSummary.createDataTriple(source, dataProperty, target); 
	}
}
