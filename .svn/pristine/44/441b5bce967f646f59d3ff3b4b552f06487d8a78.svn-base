package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.TIntSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TypeEquivalenceSummaryImpl implements TypeEquivalenceSummary {

	private final RdfSummary rdfSummary;
	
	/** The id of the highest typed node in the summary */
	private int highestTypedDataNode = -1;
	
	/** The mapping of class sets and summary data nodes having the corresponding class set */
	private final TObjectIntMap<TIntSet> sDataNodeForClassSet;
	
	/**
	 * @param rdfSummary
	 */
	public TypeEquivalenceSummaryImpl(final RdfSummary rdfSummary) {
		this.rdfSummary = rdfSummary;
		sDataNodeForClassSet = new TObjectIntHashMap<TIntSet>();
	}
	
	@Override
	public void createTypeTriples(int sNode, TIntSet classes) {
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);
		
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public int getSummaryDataNodeForClasses(final TIntSet classes) {
		Preconditions.checkNotNull(classes);
		Preconditions.checkArgument(classes.size() > 0);

		return sDataNodeForClassSet.get(classes);
	}
	
	/**
	 * In type equivalence summaries, all typed data nodes are created before any untyped nodes.
	 * So, a data node is untyped if its value is higher than the highest typed data node.
	 * @throws BerkeleyDbException 
	 */
	@Override
	public boolean isTypedDataNode(final int sDataNode) {
		return highestTypedDataNode > 0 && sDataNode <= highestTypedDataNode;
	}
	
	@Override
	public void setHighestTypedDataNode(int sDataNode) {
		highestTypedDataNode = sDataNode;
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}
	
	@Override
	public void storeSummaryDataNodeByClassSet(TIntSet classes, int sDataNode) {
		sDataNodeForClassSet.put(classes, sDataNode);
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target); 
	}

	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
