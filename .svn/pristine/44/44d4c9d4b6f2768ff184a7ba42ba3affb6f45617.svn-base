package fr.inria.oak.RDFSummary.summary.trove.export;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.constants.Chars;
import fr.inria.oak.RDFSummary.summary.stats.Stats;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.RdfUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.reasoning.rdfs.saturation.Triple;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class DotSummaryVizImpl implements DotSummaryViz {

	private static StringBuilder Builder;
	private static PrintWriter PrintWriter = null; 
	
	private final String SHAPE = " [shape=";
	private final String LABEL = "label=";
	private final String STYLE = "style=";
	private final String COLOR = "color=";
	private final String ARROW = " -> ";
	private final String LABEL_FONT_COLOR = "label=<<font color=";
	private final String CLOSE_FONT_TAG = "</font>>";

	@Override
	public void startDotGraph(final String dotFilepath, final String graphName, final String graphLabel) throws IOException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dotFilepath));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(graphName));
		Preconditions.checkArgument(!graphName.contains(" ") && !graphName.contains("-"));
		Preconditions.checkNotNull(graphLabel);
		
		if (PrintWriter != null)
			throw new IOException("Another dot graph has already been started. Invoke endDotGraph.");
		
		PrintWriter = new PrintWriter(new FileWriter(dotFilepath));
		PrintWriter.println("digraph " + graphName + " {");
		PrintWriter.println("graph [margin=\"0,0\", label=\"" + graphLabel + "\", labeljust=left];");
	}
	
	/**
	 * Writes to the last opened file.
	 */
	@Override
	public void writeToFile(final String entry) {
		PrintWriter.println(entry);
	}
	
	@Override
	public String getDotNode(final int id, final String label, final String shape, final String style, final String color) {
		Builder = new StringBuilder();
		Builder.append(NameUtils.addDotNodePrefix(id));
		Builder.append(SHAPE).append(shape);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(LABEL).append(Chars.QUOTE).append(label).append(Chars.QUOTE);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(STYLE).append(style);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(COLOR).append(Chars.QUOTE).append(color).append(Chars.QUOTE);
		Builder.append(Chars.RIGHT_SQUARE_BRACKET).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String getGraphLabel(final String schemaName, final Stats stats) {
		Builder = new StringBuilder();
		Builder.append("\n\\lDataset: ").append(schemaName);
		Builder.append("\\lSchema triples: ").append(stats.getSummaryTripleStats().getSchemaTripleCount());
		Builder.append("\\lInput non-schema triples: ").append(stats.getInputGraphTripleStats().getNonSchemaTripleCount());
		Builder.append(" (Data: ").append(stats.getInputGraphTripleStats().getDataTripleCount()).append(", Type: ").append(stats.getInputGraphTripleStats().getTypeTripleCount()).append(Chars.RIGHT_PAREN);
		Builder.append("\\lSummary non-schema triples: ").append(stats.getSummaryTripleStats().getNonSchemaTripleCount());
		Builder.append(" (Data: ").append(stats.getSummaryTripleStats().getDataTripleCount()).append(", Type: ").append(stats.getSummaryTripleStats().getTypeTripleCount()).append(Chars.RIGHT_PAREN);		
		Builder.append("\\lSummary nodes: ").append(stats.getSummaryNodeStats().getTotalDistinctNodeCount());
		Builder.append(" (Class: ").append(stats.getSummaryNodeStats().getClassNodeCount());
		Builder.append(", Data: ").append(stats.getSummaryNodeStats().getDataNodeCount());
		Builder.append(", Property: ").append(stats.getSummaryNodeStats().getPropertyNodeCount()).append(Chars.RIGHT_PAREN).append("\\l");
		
		return Builder.toString();
	}
	
	@Override
	public String getEdgeColor(final String edgeColor) {
		Builder = new StringBuilder(Chars.BACKSLASH);
		Builder.append(Chars.QUOTE).append(edgeColor).append(Chars.QUOTE);
		
		return Builder.toString();
	}

	@Override
	public String getEdgeLabel(final String shortPropertyIRI) {
		Builder = new StringBuilder(Chars.BACKSLASH);
		Builder.append(Chars.QUOTE).append(shortPropertyIRI).append(Chars.QUOTE);
		
		return Builder.toString();
	}

	@Override
	public String getDotTriple(final String sourceLabel, final String targetLabel, final String edgeColor, final String edgeStyle, final String edgeLabel) {
		Builder = new StringBuilder();
		Builder.append(sourceLabel).append(ARROW).append(targetLabel);
		Builder.append(Chars.SPACE).append(Chars.LEFT_SQUARE_BRACKET);
		Builder.append(COLOR).append(edgeColor);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(STYLE).append(edgeStyle);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(LABEL).append(edgeLabel);
		Builder.append(Chars.RIGHT_SQUARE_BRACKET).append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public String getSchemaTriple(String sourceLabel, String targetLabel, final Triple schemaTriple, final String edgeColor, final String schemaLabelColor) {
		Builder = new StringBuilder();
		Builder.append(sourceLabel).append(ARROW).append(targetLabel);
		Builder.append(Chars.SPACE).append(Chars.LEFT_SQUARE_BRACKET);
		Builder.append(COLOR).append(edgeColor);
		Builder.append(Chars.COMMA).append(Chars.SPACE);
		Builder.append(LABEL_FONT_COLOR).append(Chars.QUOTE).append(schemaLabelColor).append(Chars.QUOTE);
		Builder.append(Chars.RIGHT_ANGLE_BRACKET);
		Builder.append(RdfUtils.getShortRDFSProperty(schemaTriple.property.toString()));
		Builder.append(CLOSE_FONT_TAG).append(Chars.RIGHT_SQUARE_BRACKET);
		Builder.append(Chars.SEMICOLON);
		
		return Builder.toString();
	}

	@Override
	public void endDotGraph() {
		writeToFile(Character.toString(Chars.RIGHT_CURLY_BRACE));
		PrintWriter.close(); 
		PrintWriter = null;
	}
}
