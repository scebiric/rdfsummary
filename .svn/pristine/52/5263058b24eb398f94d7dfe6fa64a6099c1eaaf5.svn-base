package fr.inria.oak.RDFSummary.summary.trove;

import gnu.trove.set.TIntSet;

/**
 * Weak {@link RdfSummary}
 * 
 * @author Sejla CEBIRIC
 *
 */
public interface WeakSummary extends RdfSummary {
	/** 
	 * Unifies the two data nodes 
	 *   
	 * @param dataNode1
	 * @param dataNode2
	 * @return The remaining node
	 * @throws SummaryException 
	 */
	public int unifyDataNodes(int dataNode1, int dataNode2) throws SummaryException;
	
	/** Returns the degree of the specified data node as the sum of incoming and outgoing data edges 
	 * @throws SummaryException */
	public int getDegreeForDataNode(int dataNode) throws SummaryException;
	
	/** Returns the incoming degree of the specified data node as the number of incoming data edges 
	 * @throws SummaryException */
	public int getIncomingDegreeForDataNode(int dataNode) throws SummaryException;
	
	/** Returns the outgoing degree of the specified data node as the number of outgoing data edges 
	 * @throws SummaryException */
	public int getOutgoingDegreeForDataNode(int dataNode) throws SummaryException;
	
	/** Stores the specified data node as the source of the data property 
	 * @throws SummaryException */
	public void setSourceDataNodeForDataProperty(int dataProperty, int dataNode) throws SummaryException;
	
	/** Stores the specified data node as the target of the data property 
	 * @throws SummaryException */
	public void setTargetDataNodeForDataProperty(int dataProperty, int dataNode) throws SummaryException;
	
	/** Returns the source node for the specified data property or 0 if there is no such node */
	public int getSourceDataNodeForDataProperty(int dataProperty);
	
	/** Returns the target node for the specified data property or 0 if there is no such node */
	public int getTargetDataNodeForDataProperty(int dataProperty);	
	
	/** Returns the set of data properties of which the specified summary data node is the source 
	 * @throws SummaryException */
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode) throws SummaryException;
	
	/** Returns the set of data properties of which the specified summary data node is the target 
	 * @throws SummaryException */
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode) throws SummaryException;
}
