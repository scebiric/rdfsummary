package fr.inria.oak.RDFSummary.summary.trove.triplecreator.unit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;

import fr.inria.oak.RDFSummary.config.ParametersException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.noderepresenter.TroveNodeRepresenter;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.TroveTripleCreator;
import fr.inria.oak.RDFSummary.summary.trove.triplecreator.TroveTripleCreatorImpl;
import fr.inria.oak.RDFSummary.summary.util.TestUtils;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class TroveTripleCreatorImplTest {

	private TroveNodeRepresenter nodeRepresenter;

	@Before
	public void setUp() throws ConfigurationException, IOException, ParametersException {
		nodeRepresenter = mock(TroveNodeRepresenter.class);	
		TestUtils.deleteBerkeleyDbDirectory();
	}
	
	@Test
	public void representDataTriple_weakPureCliqueEquivalenceSummary_subjectAndObjectAreClassOrProperty_invokesCreateDataTripleWithSpecifiedPropertyAndSubjectObjectUnchanged() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(subject)).thenReturn(true);
		when(weak.isClassOrProperty(object)).thenReturn(true);	
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(weak, subject, dataProperty, object);
		
		// Assert
		verify(weak).createDataTriple(subject, dataProperty, object);
	}
	
	@Test
	public void representDataTriple_weakTypeEquivalenceSummary_subjectAndObjectAreClassOrProperty_sourceAndTargetDataRepresentativesAreNotSet() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		WeakTypeEquivalenceSummary summary = mock(WeakTypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(subject)).thenReturn(true);
		when(summary.isClassOrProperty(object)).thenReturn(true);	
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(summary, subject, dataProperty, object);
		
		// Assert
		verify(summary).createDataTriple(subject, dataProperty, object);
		verify(summary, times(0)).setSourceDataNodeForDataProperty(anyInt(), anyInt());
		verify(summary, times(0)).setTargetDataNodeForDataProperty(anyInt(), anyInt());
	}
	
	@Test
	public void representDataTriple_weakPureCliqueEquivalenceSummary_subjectAndObjectAreClassOrProperty_sourceAndTargetDataRepresentativesAreNotSetForProperty() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(subject)).thenReturn(true);
		when(weak.isClassOrProperty(object)).thenReturn(true);	
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(weak, subject, dataProperty, object);
		
		// Assert
		verify(weak).createDataTriple(subject, dataProperty, object);
		verify(weak, times(0)).setSourceDataNodeForDataProperty(anyInt(), anyInt());
		verify(weak, times(0)).setTargetDataNodeForDataProperty(anyInt(), anyInt());
	}
	
	@Test
	public void representDataTriple_weakPureCliqueEquivalenceSummary_subjectAndObjectAreDataNodes_invokesCreateDataTripleWithSpecifiedPropertyAndRepresentativeDataNodes() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		int subjectRepresentative = 4;
		int objectRepresentative = 5;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(subject)).thenReturn(false);
		when(weak.isClassOrProperty(object)).thenReturn(false);	
		when(nodeRepresenter.getRepresentativeSourceDataNode(weak, subject, dataProperty)).thenReturn(subjectRepresentative);
		when(nodeRepresenter.getRepresentativeTargetDataNode(weak, object, dataProperty)).thenReturn(objectRepresentative); 
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(weak, subject, dataProperty, object);
		
		// Assert
		verify(weak).createDataTriple(subjectRepresentative, dataProperty, objectRepresentative);
	}
	
	@Test
	public void representDataTriple_weakPureCliqueEquivalenceSummary_subjectAndObjectAreDataNodes_sourceAndTargetDataRepresentativesAreSetForProperty() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		int subjectRepresentative = 4;
		int objectRepresentative = 5;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(subject)).thenReturn(false);
		when(weak.isClassOrProperty(object)).thenReturn(false);	
		when(nodeRepresenter.getRepresentativeSourceDataNode(weak, subject, dataProperty)).thenReturn(subjectRepresentative);
		when(nodeRepresenter.getRepresentativeTargetDataNode(weak, object, dataProperty)).thenReturn(objectRepresentative); 
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(weak, subject, dataProperty, object);
		
		// Assert
		verify(weak).createDataTriple(subjectRepresentative, dataProperty, objectRepresentative);
		verify(weak, times(1)).setSourceDataNodeForDataProperty(dataProperty, subjectRepresentative);
		verify(weak, times(1)).setTargetDataNodeForDataProperty(dataProperty, objectRepresentative);
	}
	
	@Test
	public void representDataTriple_weakTypeEquivalenceSummary_subjectAndObjectAreTypedDataNodes_sourceAndTargetDataRepresentativesAreNotSetForProperty() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		int subjectRepresentative = 4;
		int objectRepresentative = 5;
		WeakTypeEquivalenceSummary summary = mock(WeakTypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(subject)).thenReturn(false);
		when(summary.isClassOrProperty(object)).thenReturn(false);
		when(summary.isTypedDataNode(subjectRepresentative)).thenReturn(true);
		when(summary.isTypedDataNode(objectRepresentative)).thenReturn(true);
		when(nodeRepresenter.getRepresentativeSourceDataNode(summary, subject, dataProperty)).thenReturn(subjectRepresentative);
		when(nodeRepresenter.getRepresentativeTargetDataNode(summary, object, dataProperty)).thenReturn(objectRepresentative); 
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(summary, subject, dataProperty, object);
		
		// Assert
		verify(summary).createDataTriple(subjectRepresentative, dataProperty, objectRepresentative);
		verify(summary, times(0)).setSourceDataNodeForDataProperty(anyInt(), anyInt());
		verify(summary, times(0)).setTargetDataNodeForDataProperty(anyInt(), anyInt());
	}
	
	@Test
	public void representDataTriple_weakTypeEquivalenceSummary_subjectAndObjectAreUntypedDataNodes_sourceAndTargetDataRepresentativesAreSetForProperty() throws SQLException, BerkeleyDbException {
		// Set up
		int subject = 1;
		int dataProperty = 2;
		int object = 3;
		int subjectRepresentative = 4;
		int objectRepresentative = 5;
		WeakTypeEquivalenceSummary summary = mock(WeakTypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(subject)).thenReturn(false);
		when(summary.isClassOrProperty(object)).thenReturn(false);
		when(summary.isTypedDataNode(subjectRepresentative)).thenReturn(false);
		when(summary.isTypedDataNode(objectRepresentative)).thenReturn(false);
		when(nodeRepresenter.getRepresentativeSourceDataNode(summary, subject, dataProperty)).thenReturn(subjectRepresentative);
		when(nodeRepresenter.getRepresentativeTargetDataNode(summary, object, dataProperty)).thenReturn(objectRepresentative); 
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		tripleCreator.representDataTriple(summary, subject, dataProperty, object);
		
		// Assert
		verify(summary).createDataTriple(subjectRepresentative, dataProperty, objectRepresentative);
		verify(summary).setSourceDataNodeForDataProperty(dataProperty, subjectRepresentative);
		verify(summary).setTargetDataNodeForDataProperty(dataProperty, objectRepresentative);
	}
	
	@Test
	public void representTypeTriple_weakPureCliqueEquivalenceSummary_gNodeIsClassOrPropertyTypeTripleNotInSummary_invokesCreateTypeTripleWithGNodeUnchangedAndReturnsTrue() throws SQLException, BerkeleyDbException {
		// Set up
		int gNode = 1;
		int classIRI = 2;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(gNode)).thenReturn(true);
		when(weak.existsTypeTriple(gNode, classIRI)).thenReturn(false);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		boolean represented = tripleCreator.representTypeTriple(weak, gNode, classIRI);
		
		// Assert
		verify(weak).createTypeTriple(gNode, classIRI);
		assertTrue(represented);
	}
	
	@Test
	public void representTypeTriple_weakPureCliqueEquivalenceSummary_gNodeIsClassOrPropertyTypeTripleAlreadyInSummary_tripleCreationNotInvokedReturnsTrue() throws SQLException, BerkeleyDbException {
		// Set up
		int gNode = 1;
		int classIRI = 2;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(gNode)).thenReturn(true);
		when(weak.existsTypeTriple(gNode, classIRI)).thenReturn(true);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		boolean represented = tripleCreator.representTypeTriple(weak, gNode, classIRI);
		
		// Assert
		verify(weak, never()).createTypeTriple(gNode, classIRI);
		assertTrue(represented);
	}
	
	@Test
	public void representTypeTriple_weakPureCliqueEquivalenceSummary_gNodeIsDataNodeAndUnrepresented_tripleCreationNotInvokedReturnsFalse() throws SQLException, BerkeleyDbException {
		// Set up
		int gNode = 1;
		int classIRI = 2;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(gNode)).thenReturn(false);
		when(weak.getSummaryDataNodeForInputDataNode(gNode)).thenReturn(0);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		boolean represented = tripleCreator.representTypeTriple(weak, gNode, classIRI);
		
		// Assert
		verify(weak, never()).createTypeTriple(gNode, classIRI);
		assertFalse(represented);
	}
	
	@Test
	public void representTypeTriple_weakPureCliqueEquivalenceSummary_gNodeIsDataNodeAndRepresentedTypeTripleNotInSummary_invokesCreateTypeTripleWithRepresentativeNodeAndReturnsTrue() throws SQLException, BerkeleyDbException {
		// Set up
		int gNode = 1;
		int classIRI = 2;
		int sNode = 3;
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		when(weak.isClassOrProperty(gNode)).thenReturn(false);
		when(weak.getSummaryDataNodeForInputDataNode(gNode)).thenReturn(sNode);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Call
		boolean represented = tripleCreator.representTypeTriple(weak, gNode, classIRI);
		
		// Assert
		verify(weak).createTypeTriple(sNode, classIRI);
		assertTrue(represented);
	}
	
	@Test
	public void representTypeTriples_weakPureCliqueEquivalenceSummary_oneDataNodeCreatedForAllNodesAndTypeTriplesCreatedWithTheNewNode() throws SQLException, BerkeleyDbException {
		// Set up
		TIntSet gDataNodes = new TIntHashSet();
		int gDataNode1 = 1;
		int gDataNode2 = 2;
		gDataNodes.add(gDataNode1);
		gDataNodes.add(gDataNode2);
		TIntSet classes = new TIntHashSet();
		int class1 = 3;
		int class2 = 4;
		int class3 = 5;
		classes.add(class1);
		classes.add(class2);
		classes.add(class3);
		WeakPureCliqueEquivalenceSummary weak = mock(WeakPureCliqueEquivalenceSummary.class);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
				
		// Call
		tripleCreator.representTypeTriples(weak, gDataNodes, classes);
		
		// Assert
		verify(weak).createDataNode(gDataNodes);
		int sDataNode = weak.getSummaryDataNodeForInputDataNode(gDataNode1);
		assertEquals(sDataNode, weak.getSummaryDataNodeForInputDataNode(gDataNode2));
		weak.existsTypeTriple(sDataNode, class1);
		weak.existsTypeTriple(sDataNode, class2);
		weak.existsTypeTriple(sDataNode, class3);
	}

	@Test
	public void representTypeTriples_typeEquivalenceSummary_gNodeIsClassOrProperty_createTypeTriplesInvokedWithGNodeUnchanged() throws SQLException, BerkeleyDbException {
		// Set up
		int gNode = 1;
		TIntSet classes = new TIntHashSet();
		classes.add(2);
		classes.add(3);
		classes.add(4);
		TypeEquivalenceSummary summary = mock(TypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(gNode)).thenReturn(true);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Run
		tripleCreator.representTypeTriples(summary, gNode, classes);
				
		// Assert
		verify(summary, times(1)).createTypeTriples(gNode, classes);
	}
	
	@Test
	public void representTypeTriples_typeEquivalenceSummary_gNodeIsDataNodeAndItsClassSetIsRepresented_gNodeRepresentedWithRepresentativeOfItsClassSetAndOtherMethodsNotInvoked() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 5;
		TIntSet classes = new TIntHashSet();
		classes.add(2);
		classes.add(3);
		classes.add(4);
		TypeEquivalenceSummary summary = mock(TypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(gDataNode)).thenReturn(false);
		when(summary.getSummaryDataNodeForClasses(classes)).thenReturn(sDataNode);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Run
		tripleCreator.representTypeTriples(summary, gDataNode, classes);
				
		// Assert
		verify(summary, times(1)).representInputDataNode(gDataNode, sDataNode);
		verify(summary, times(0)).createDataNode(anyInt());
		verify(summary, times(0)).createTypeTriples(anyInt(), any(TIntSet.class)); 
		verify(summary, times(0)).storeSummaryDataNodeByClassSet(any(TIntSet.class), anyInt());
		verify(summary, times(0)).setHighestTypedDataNode(anyInt());		
	}
	
	@Test
	public void representTypeTriples_typeEquivalenceSummary_gNodeIsDataNodeAndItsClassSetIsUnrepresented_createsSDataNodeToRepresentClasses() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 5;
		TIntSet classes = new TIntHashSet();
		classes.add(2);
		classes.add(3);
		classes.add(4);
		TypeEquivalenceSummary summary = mock(TypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(gDataNode)).thenReturn(false);
		when(summary.getSummaryDataNodeForClasses(classes)).thenReturn(0);
		when(summary.createDataNode(gDataNode)).thenReturn(sDataNode);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Run
		tripleCreator.representTypeTriples(summary, gDataNode, classes);
				
		// Assert		
		verify(summary, times(1)).createDataNode(gDataNode);				
	}
	
	@Test
	public void representTypeTriples_typeEquivalenceSummary_gNodeIsDataNodeAndItsClassSetIsUnrepresented_representsClassesWithSDataNodeAndSetsSDataNodeAsHighestTypedDataNode() throws SQLException, BerkeleyDbException {
		// Set up
		int gDataNode = 1;
		int sDataNode = 5;
		TIntSet classes = new TIntHashSet();
		classes.add(2);
		classes.add(3);
		classes.add(4);
		TypeEquivalenceSummary summary = mock(TypeEquivalenceSummary.class);
		when(summary.isClassOrProperty(gDataNode)).thenReturn(false);
		when(summary.getSummaryDataNodeForClasses(classes)).thenReturn(0);
		when(summary.createDataNode(gDataNode)).thenReturn(sDataNode);
		TroveTripleCreator tripleCreator = new TroveTripleCreatorImpl(nodeRepresenter);
		
		// Run
		tripleCreator.representTypeTriples(summary, gDataNode, classes);
				
		// Assert		 
		verify(summary, times(1)).storeSummaryDataNodeByClassSet(classes, sDataNode);
		verify(summary, times(1)).setHighestTypedDataNode(sDataNode);		
	}	
	
}
