package fr.inria.oak.RDFSummary.summary.trove.summarizer;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import fr.inria.oak.RDFSummary.constants.SummaryType;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbException;
import fr.inria.oak.RDFSummary.data.berkeleydb.BerkeleyDbHandler;
import fr.inria.oak.RDFSummary.data.dao.DictionaryDao;
import fr.inria.oak.RDFSummary.data.dao.RdfTablesDao;
import fr.inria.oak.RDFSummary.data.handler.SqlConnectionHandler;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.PureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummary;
import fr.inria.oak.RDFSummary.summary.trove.RdfSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.StrongTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.TypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakPureCliqueEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummary;
import fr.inria.oak.RDFSummary.summary.trove.WeakTypeEquivalenceSummaryImpl;
import fr.inria.oak.RDFSummary.summary.trove.componentsummarizer.TroveRdfComponentSummarizer;
import fr.inria.oak.RDFSummary.urigenerator.UriGenerator;
import fr.inria.oak.RDFSummary.util.BerkeleyDbUtils;
import fr.inria.oak.RDFSummary.util.NameUtils;
import fr.inria.oak.RDFSummary.util.StringUtils;
import fr.inria.oak.commons.db.DictionaryException;
import fr.inria.oak.commons.db.InexistentKeyException;
import fr.inria.oak.commons.db.InexistentValueException;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PostgresTroveSummarizerImpl implements TroveSummarizer {

	private static final Logger log = Logger.getLogger(TroveSummarizer.class);
	private final SqlConnectionHandler connHandler;
	private static TroveRdfComponentSummarizer RdfComponentSummarizer;
	private static UriGenerator UriGenerator;
	private static DictionaryDao DictDao;
	private static RdfTablesDao RdfTablesDao;
	private static BerkeleyDbHandler BerkeleyDbHandler;
	
	private static StringBuilder Builder;
	private WeakPureCliqueEquivalenceSummary weakPureSummary = null;
	private WeakTypeEquivalenceSummary weakTypeSummary = null;
	private StrongPureCliqueEquivalenceSummary strongPureSummary = null;
	private StrongTypeEquivalenceSummary strongTypeSummary = null;
	
	private String classesDb = null;
	private String propertiesDb = null;
	private String classNodesTable = null;
	private String propertyNodesTable = null;

	/**
	 * @param connHandler
	 * @param rdfComponentSummarizer
	 * @param uriGenerator
	 * @param dictDao
	 * @param rdfTablesDao
	 * @param bdbHandler
	 */
	public PostgresTroveSummarizerImpl(final SqlConnectionHandler connHandler, final TroveRdfComponentSummarizer rdfComponentSummarizer,
			final UriGenerator uriGenerator, final DictionaryDao dictDao, final RdfTablesDao rdfTablesDao, final BerkeleyDbHandler bdbHandler) {
		this.connHandler = connHandler;
		RdfComponentSummarizer = rdfComponentSummarizer;
		UriGenerator = uriGenerator;
		DictDao = dictDao;
		RdfTablesDao = rdfTablesDao;
		BerkeleyDbHandler = bdbHandler;
	}

	/**
	 * Tables assumed encoded. 
	 */
	@Override
	public TroveSummaryResult summarize(final String summaryType, final String dataTable, final String typesTable, final String schemaTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException, BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.WEAK) || summaryType.equals(SummaryType.TYPED_WEAK) ||
				summaryType.equals(SummaryType.STRONG) || summaryType.equals(SummaryType.TYPED_STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		
		classesDb = NameUtils.getClassesDbName(dataTable);
		propertiesDb = NameUtils.getPropertiesDbName(dataTable);
		
		final String schemaName = NameUtils.getSchemaName(dataTable);
		classNodesTable = NameUtils.getClassNodesTableName(schemaName);
		propertyNodesTable = NameUtils.getPropertyNodesTableName(schemaName);
		RdfTablesDao.extractAllClassNodes(classNodesTable, typesTable, schemaTable);
		RdfTablesDao.extractAllPropertyNodes(propertyNodesTable, dataTable, typesTable, schemaTable);
		BerkeleyDbUtils.storeClassAndPropertyNodes(classesDb, propertiesDb, classNodesTable, propertyNodesTable);
		BerkeleyDbUtils.storeDataProperties(propertiesDb, dataTable);
		
		if (summaryType.equals(SummaryType.WEAK)) 
			return this.generateWeakPureCliqueEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_WEAK)) 
			return this.generateWeakTypeEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.STRONG))
			return this.generateStrongPureCliqueEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		if (summaryType.equals(SummaryType.TYPED_STRONG))
			return this.generateStrongTypeEquivalenceSummaryOnPostgres(summaryType, dataTable, typesTable);
		
		return null;
	}
	
	@Override
	public TroveSummaryResult generateWeakPureCliqueEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, DictionaryException, InexistentValueException, BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.WEAK));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		RdfSummary rdfSummary = new RdfSummaryImpl(UriGenerator, DictDao, BerkeleyDbHandler, classesDb, propertiesDb);
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		weakPureSummary = new WeakPureCliqueEquivalenceSummaryImpl(weakEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(weakPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(weakPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, weakPureSummary, classNodesTable, propertyNodesTable);
	}
	
	public TroveSummaryResult generateStrongPureCliqueEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		RdfSummary rdfSummary = new RdfSummaryImpl(UriGenerator, DictDao, BerkeleyDbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		PureCliqueEquivalenceSummary pureCliqueEquivSummary = new PureCliqueEquivalenceSummaryImpl(rdfSummary, UriGenerator, DictDao);
		strongPureSummary = new StrongPureCliqueEquivalenceSummaryImpl(strongEquivSummary, pureCliqueEquivSummary, rdfSummary);
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeDataTriples(strongPureSummary, dataTable);
		RdfComponentSummarizer.summarizeTypeTriples(strongPureSummary, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, strongPureSummary, classNodesTable, propertyNodesTable);
	}
	
	@Override
	public TroveSummaryResult generateWeakTypeEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, DictionaryException, InexistentValueException, InexistentKeyException, BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.TYPED_WEAK));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		RdfSummary rdfSummary = new RdfSummaryImpl(UriGenerator, DictDao, BerkeleyDbHandler, classesDb, propertiesDb);
		WeakEquivalenceSummary weakEquivSummary = new WeakEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		weakTypeSummary = new WeakTypeEquivalenceSummaryImpl(weakEquivSummary, typeEquivSummary, rdfSummary); 

		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(weakTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(weakTypeSummary, dataTable);					
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, weakTypeSummary, classNodesTable, propertyNodesTable);
	}

	@Override
	public TroveSummaryResult generateStrongTypeEquivalenceSummaryOnPostgres(final String summaryType, final String dataTable,
			final String typesTable) throws SQLException, BerkeleyDbException {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(summaryType));
		Preconditions.checkArgument(summaryType.equals(SummaryType.TYPED_STRONG));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(dataTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(typesTable));	
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(classNodesTable));
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(propertyNodesTable));

		log.info(getSummaryTypeMessage(summaryType));
		RdfSummary rdfSummary = new RdfSummaryImpl(UriGenerator, DictDao, BerkeleyDbHandler, classesDb, propertiesDb);
		StrongEquivalenceSummary strongEquivSummary = new StrongEquivalenceSummaryImpl(rdfSummary);
		TypeEquivalenceSummary typeEquivSummary = new TypeEquivalenceSummaryImpl(rdfSummary);
		strongTypeSummary = new StrongTypeEquivalenceSummaryImpl(strongEquivSummary, typeEquivSummary, rdfSummary); 
		
		connHandler.disableAutoCommit();
		RdfComponentSummarizer.summarizeTypeTriples(strongTypeSummary, typesTable);
		RdfComponentSummarizer.summarizeDataTriples(strongTypeSummary, dataTable, typesTable);
		connHandler.enableAutoCommit();
		
		return new TroveSummaryResult(summaryType, strongTypeSummary, classNodesTable, propertyNodesTable);
	}
	
	private String getSummaryTypeMessage(final String summaryType) {
		Builder = new StringBuilder("Building the ");
		Builder.append(summaryType).append(" summary...");		
		
		return Builder.toString();
	}
}
