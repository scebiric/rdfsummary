package fr.inria.oak.RDFSummary.logger;

import java.util.Map;

import com.google.common.base.Preconditions;

import fr.inria.oak.RDFSummary.util.StringUtils;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class PerformanceResult {

	private final Map<String, Long> timingMap;
	private final Map<String, String> queriesMap;
	
	/**
	 * @param timingMap
	 * @param queriesMap
	 */
	public PerformanceResult(final Map<String, Long> timingMap, final Map<String, String> queriesMap) {
		this.timingMap = timingMap;
		this.queriesMap = queriesMap;
	}
	
	public long getTiming(final String label) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(label));
		
		return timingMap.get(label);
	}
	
	public String getQueryByLabel(final String label) {
		Preconditions.checkArgument(!StringUtils.isNullOrBlank(label));
		
		return queriesMap.get(label);
	}
}
