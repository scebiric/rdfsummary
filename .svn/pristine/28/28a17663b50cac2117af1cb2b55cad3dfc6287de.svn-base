package fr.inria.oak.RDFSummary.summary.trove;

import java.sql.SQLException;
import java.util.Collection;

import fr.inria.oak.RDFSummary.summary.trove.DataTriple;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

/**
 * 
 * @author Sejla CEBIRIC
 *
 */
public class WeakEquivalenceSummaryImpl implements WeakEquivalenceSummary {

	private final RdfSummary rdfSummary;
	
	/** Mapping of properties and their source nodes */
	private final TIntIntMap sourceDataNodeForDataProperty;

	/** The mapping of data nodes and all the data properties of which they are the source */
	private final TIntObjectMap<TIntSet> dataPropertiesForSourceDataNode;

	/** Mapping of properties and their target nodes */
	private final TIntIntMap targetDataNodeForDataProperty;

	/** The mapping of data nodes and all the data properties of which they are the target */
	private final TIntObjectMap<TIntSet> dataPropertiesForTargetDataNode;
	
	private TIntIterator iterator = null;
	private Collection<DataTriple> dataTriples = null;
	private int remainingNode;
	private int vanishingNode;
	
	/**
	 * @param rdfSummary
	 */
	public WeakEquivalenceSummaryImpl(final RdfSummary rdfSummary) {
		this.rdfSummary = rdfSummary;	
		sourceDataNodeForDataProperty = new TIntIntHashMap();
		targetDataNodeForDataProperty = new TIntIntHashMap();
		dataPropertiesForSourceDataNode = new TIntObjectHashMap<TIntSet>();
		dataPropertiesForTargetDataNode = new TIntObjectHashMap<TIntSet>();
	}	

	/**
	 * Unifies the specified nodes, such that one node receives all the other node's edges and represented resources, and the latter node is removed from the summary.
	 * The node with less edges will be removed from the summary.
	 * 
	 * Assumes that there may exist at most one data triple per distinct property in a weak summary.
	 * @throws BerkeleyDbException 
	 */
	@Override
	public int unifyDataNodes(final int dataNode1, final int dataNode2) {		
		if (this.getDegreeForDataNode(dataNode1) < this.getDegreeForDataNode(dataNode2)) {
			vanishingNode = dataNode1;
			remainingNode = dataNode2;
		}
		else {
			vanishingNode = dataNode2;
			remainingNode = dataNode1;
		}

		TIntSet dataProps = getDataPropertiesForSourceDataNode(vanishingNode);
		if (dataProps != null) {
			iterator = dataProps.iterator();
			while (iterator.hasNext()) {
				dataTriples = rdfSummary.getDataTriplesForDataProperty(iterator.next());
				if (dataTriples != null) {
					for (DataTriple dataTriple : dataTriples) {
						dataTriple.setSubject(remainingNode);
					}
				}
			}
		}

		dataProps = getDataPropertiesForTargetDataNode(vanishingNode);
		if (dataProps != null) {
			iterator = dataProps.iterator();
			while (iterator.hasNext()) {
				dataTriples = rdfSummary.getDataTriplesForDataProperty(iterator.next());
				if (dataTriples != null) {
					for (DataTriple dataTriple : dataTriples) {
						dataTriple.setObject(remainingNode);
					}
				}
			}
		}

		TIntSet set = rdfSummary.getRepresentedInputDataNodes(vanishingNode);
		iterator = set.iterator();
		while (iterator.hasNext()) {
			rdfSummary.representInputDataNode(iterator.next(), remainingNode);
		}
		rdfSummary.unrepresentAllNodesForSummaryDataNode(vanishingNode);

		set = getDataPropertiesForSourceDataNode(vanishingNode);
		if (set != null) {
			iterator = set.iterator();
			while (iterator.hasNext()) {
				setSourceDataNodeForDataProperty(iterator.next(), remainingNode);
			}
			removeDataPropertiesForSourceDataNode(vanishingNode);			;
		}

		set = getDataPropertiesForTargetDataNode(vanishingNode);
		if (set != null) {
			iterator = set.iterator();
			while (iterator.hasNext()) {
				setTargetDataNodeForDataProperty(iterator.next(), remainingNode);
			}
			removeDataPropertiesForTargetDataNode(vanishingNode);			
		}

		return remainingNode;
	}
	
	@Override
	public void setSourceDataNodeForDataProperty(final int dataProperty, final int dataNode) {
		sourceDataNodeForDataProperty.put(dataProperty, dataNode);
		TIntSet dataProperties = dataPropertiesForSourceDataNode.get(dataNode);
		if (dataProperties == null) {
			dataProperties = new TIntHashSet();
			dataProperties.add(dataProperty);
			dataPropertiesForSourceDataNode.put(dataNode, dataProperties);
		}
		else
			dataProperties.add(dataProperty);
	}

	@Override
	public void setTargetDataNodeForDataProperty(final int dataProperty, final int dataNode) {
		targetDataNodeForDataProperty.put(dataProperty, dataNode);
		TIntSet dataProperties = dataPropertiesForTargetDataNode.get(dataNode);
		if (dataProperties == null) {
			dataProperties = new TIntHashSet();
			dataProperties.add(dataProperty);
			dataPropertiesForTargetDataNode.put(dataNode, dataProperties);
		}
		else
			dataProperties.add(dataProperty);
	}

	@Override
	public int getSourceDataNodeForDataProperty(final int dataProperty) {
		return sourceDataNodeForDataProperty.get(dataProperty);
	}

	@Override
	public int getTargetDataNodeForDataProperty(final int dataProperty) {
		return targetDataNodeForDataProperty.get(dataProperty);
	}

	@Override
	public int getDegreeForDataNode(final int dataNode) {
		int degree = 0;
		TIntSet dataProps = dataPropertiesForSourceDataNode.get(dataNode);
		if (dataProps != null)
			degree += dataProps.size();

		dataProps = dataPropertiesForTargetDataNode.get(dataNode);
		if (dataProps != null)
			degree += dataProps.size();

		return degree;		
	}

	@Override
	public int getIncomingDegreeForDataNode(final int dataNode) {
		TIntSet dataProps = dataPropertiesForTargetDataNode.get(dataNode);
		if (dataProps == null)
			return 0;

		return dataProps.size();
	}

	@Override
	public int getOutgoingDegreeForDataNode(final int dataNode) {
		TIntSet dataProps = dataPropertiesForSourceDataNode.get(dataNode);
		if (dataProps == null)
			return 0;

		return dataProps.size();
	}

	@Override
	public TIntSet getDataPropertiesForSourceDataNode(int dataNode) {
		return dataPropertiesForSourceDataNode.get(dataNode);
	}

	@Override
	public TIntSet getDataPropertiesForTargetDataNode(int dataNode) {
		return dataPropertiesForTargetDataNode.get(dataNode);
	}

	@Override
	public TIntObjectMap<Collection<DataTriple>> getDataTriplesForDataPropertyMap() {
		return rdfSummary.getDataTriplesForDataPropertyMap();
	}

	@Override
	public Collection<DataTriple> getDataTriplesForDataProperty(int dataProperty) {
		return rdfSummary.getDataTriplesForDataProperty(dataProperty);
	}

	@Override
	public TIntSet getSummaryDataNodes() {
		return rdfSummary.getSummaryDataNodes();
	}

	@Override
	public int getSummaryDataNodeSupport(int sDataNode) {
		return rdfSummary.getSummaryDataNodeSupport(sDataNode);
	}

	@Override
	public int getSummaryDataNodeForInputDataNode(int gDataNode) {
		return rdfSummary.getSummaryDataNodeForInputDataNode(gDataNode);
	}

	@Override
	public TIntSet getRepresentedInputDataNodes(int sDataNode) {
		return rdfSummary.getRepresentedInputDataNodes(sDataNode);
	}

	@Override
	public int createDataNode(int gDataNode) throws SQLException {
		return rdfSummary.createDataNode(gDataNode);
	}

	@Override
	public boolean existsDataTriple(int subject, int dataProperty, int object) {
		return rdfSummary.existsDataTriple(subject, dataProperty, object);
	}

	@Override
	public void representInputDataNode(int gDataNode, int sDataNode) {
		rdfSummary.representInputDataNode(gDataNode, sDataNode);
	}

	@Override
	public void unrepresentAllNodesForSummaryDataNode(int sDataNode) {
		rdfSummary.unrepresentAllNodesForSummaryDataNode(sDataNode);
	}

	@Override
	public boolean isClassOrPropertyNode(int iri) {
		return rdfSummary.isClassOrPropertyNode(iri);
	}

	@Override
	public int getLastCreatedDataNode() {
		return rdfSummary.getLastCreatedDataNode();
	}

	@Override
	public int getSummaryDataNodeCount() {
		return rdfSummary.getSummaryDataNodeCount();
	}

	@Override
	public int getNextNodeId() throws SQLException {
		return rdfSummary.getNextNodeId();
	}

	@Override
	public void setLastCreatedDataNode(int dataNode) {
		rdfSummary.setLastCreatedDataNode(dataNode);
	}

	@Override
	public DataTriple createDataTriple(int source, int dataProperty, int target) {
		return rdfSummary.createDataTriple(source, dataProperty, target); 
	}
	
	@Override
	public void removeDataPropertiesForSourceDataNode(int source) {
		dataPropertiesForSourceDataNode.remove(source);
	}
	
	@Override
	public void removeDataPropertiesForTargetDataNode(int target) {
		dataPropertiesForTargetDataNode.remove(target);
	}
	
	@Override
	public TIntSet getClassesForSummaryNode(int sNode) {
		return rdfSummary.getClassesForSummaryNode(sNode);
	}

	@Override
	public void storeClassesBySummaryNode(int sNode, TIntSet classes) {
		rdfSummary.storeClassesBySummaryNode(sNode, classes);
	}
	
	@Override
	public TIntObjectMap<TIntSet> getClassesForSummaryNodeMap() {
		return rdfSummary.getClassesForSummaryNodeMap();
	}
	
	@Override
	public boolean isClassNode(int iri) {
		return rdfSummary.isClassNode(iri);
	}

	@Override
	public boolean isPropertyNode(int iri) {
		return rdfSummary.isPropertyNode(iri);
	}
	
	@Override
	public void addClassNode(int iri) {
		rdfSummary.addClassNode(iri);
	}

	@Override
	public void addPropertyNode(int iri) {
		rdfSummary.addPropertyNode(iri);
	}
	
	@Override
	public int getClassNodeCount() {
		return rdfSummary.getClassNodeCount();
	}

	@Override
	public int getPropertyNodeCount() {
		return rdfSummary.getPropertyNodeCount();
	}
	
	@Override
	public TIntSet getClassNodes() {
		return rdfSummary.getClassNodes();
	}

	@Override
	public TIntSet getPropertyNodes() {
		return rdfSummary.getPropertyNodes();
	}
}
