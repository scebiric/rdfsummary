# Synopsis

**RDFSummary** is a tool for generating summaries of RDF graphs: https://team.inria.fr/cedar/projects/rdfsummary/

# Example

The tool expects an RDF file as the input and generates an RDF summary graph, which is also an n-triples file.

For example, for the sample input RDF graph:

```<article2> <hasAuthor> <author2> .
<book1> <hasAuthor> <author1> .
<article1> <hasAuthor> <author2> .
<article1> <hasTitle> "Article 1 Title" .
<author3> <livesIn> <Sarajevo> .
<author2> <bornIn> <Paris> .
<book1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <Book> .
<article1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <Article> .
<Paris> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <City> .
<Sarajevo> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <City> .```

the tool will generate an RDF summary graph such as this one:

![demo weak RDF summary](https://team.inria.fr/cedar/files/2015/04/demo.png "RDF summary example")

# Requirements

* PostgreSQL 9.4 and up
* Java 1.8

# Set up

## OAK commons repository access

**RDFSummary** depends on a private-release Maven repository hosted by INRIA. 
Therefore, you need to update your `settings.xml` file, which authentication information for the repository.

There are two usual locations where the file `settings.xml` could be stored in your computer:
* `${maven.home}/conf/settings.xml`
* `${user.home}/.m2/settings.xml`

In your `settings.xml` file, add the following entry between `<servers>` and `</servers>` tags:

    <server>
      <id>oakcommons-releases</id>
     <username>oak-private-reader</username>
     <password>oakreader!</password>
    </server>

Further, add the repository under the profiles:

    <profiles>
    	<profile>
	    	<id>profile</id>
	    	<activation>
	    		<activeByDefault>true</activeByDefault>
	    	</activation>
	    	<repositories>
	    		<repository>
	    			<id>oakcommons-releases</id>
	    			<name>OAK Commons Maven Repository</name>
			    	<url>
			    		http://maven.inria.fr/artifactory/oakcommons-private-release
			    	</url>
			    	<layout>default</layout>
			    	<releases>
				    	<enabled>true</enabled>
			    	</releases>
			    	<snapshots>
				    	<enabled>true</enabled>
			    	</snapshots>
			    </repository>
		    </repositories>
	    </profile>
    </profiles>

## RDFSummary jar

### Option 1: Command line

To obtain the RDFSummary jar, run ```mvn package``` from the project root provided on Gitlab.

The full list of parameters and options to set up can be retrieved with ```java -jar RDFSummary.jar -help```.

Alternatively, parameters can be specified in a .properties configuration file and passed through the command line: ```java -jar RDFSummary.jar -config config.properties```.

The amount of RAM available to the summary tool can be controlled through the ```-Xmx``` argument.

### Option 2: INRIA Maven repository

In order to use the RDFSummary code in another Java project, add the dependency to the `pom.xml` file:

    <dependency>
      <groupId>fr.inria.oak</groupId>
      <artifactId>RDFSummary</artifactId>
      <version>1.0</version>
    </dependency>

In addition to authentication information for OAK commons repository, described above, 
RDFSummary repository must be added to the `settings.xml` file:

    <server>
        <id>rdfsummary-releases</id>
        <username>rdfsummary-private-reader</username>
        <password>rfdsummary4maven6525766</password>
    </server>

Under `<profiles>` and `<repositories>` (similar to above), add:

    <repository>
        <id>rdfsummary-releases</id>
        <name>RDF Summary Maven Repository</name>
        <url>http://maven.inria.fr/artifactory/rdfsummary-private-release</url>
        <layout>default</layout>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>

The summarization can be invoked programmatically by calling the methods in `fr.inria.oak.RDFSummary.main` package.

## Parameters

**EQUIVALENCE** Node equivalence relation: ```clique```, ```bisim```

**SUMMARY_TYPE** The possible clique summary types are: ```weak```, ```typweak```, ```strong``` and ```typstrong```. The possible bisimulation summary types are: ```fw```, ```bw``` and ```fb```. See: https://hal.inria.fr/hal-01325900v5

**PSQL_SCHEMA_NAME** The PostgreSQL schema name should be set to the dataset name, e.g. ```demo```.

**DROP_SCHEMA** If set to ```true```, the PostgreSQL schema specified with ```PSQL_SCHEMA_NAME``` will be dropped if it exists; this will cause reloading and re-encoding of the dataset. Recommended value is ```false```.

**DATA_FILEPATH** Filepath to input triples (data, type or schema). The only supported format is n-triples.

**SCHEMA_FILEPATH** Schema triples may also be provided in a single separate file, in any valid RDF format.

**DATA_SOURCE_NAME** The only supported data source currently is ```postgres```.

**DATABASE_NAME** The PostgreSQL database name.

**USERNAME** The PostgreSQL user. The user must be granted privileges on the database specified with ```DATABASE_NAME```.

**PASSWORD** The password for the PostgreSQL user.

**SERVER_NAME** The PostgreSQL server, e.g. ```localhost```.

**PORT** The PostgreSQL port.

**FETCH_SIZE** Fetch size for JDBC statements. Recommended value is ```100000```.

**OUTPUT_FOLDER** Filepath to the output folder (summary graph, stats, etc.)

**MAP_FOLDER** Only for bisimulation: Filepath to the folder for storing mapdb files.

**GENERATE_DOT** If set to ```true```, a DOT graph will be generated for the resulting summary.

**CONVERT_DOT_TO_PDF** If set to ```true``` (and if ```GENERATE_DOT=true```) the generated DOT graph will be converted to PDF.

**DOT_EXECUTABLE_FILEPATH** Filepath to the DOT executable.

**DOT_STYLE_PARAMETERS_FILEPATH** Filepath to the DOT configuration file. Sample: https://gitlab.inria.fr/scebiric/rdfsummary/blob/master/src/main/resources/config/dot-config.properties

**EXPORT_TO_RDF_FILE** If set to ```true```, the generated summary will be written to an n-triples RDF file.

**EXPORT_STATS_TO_FILE** If set to ```true```, the statistics will be written to a file (e.g. input and summary sizes).


# Contact

E-mail: sejla [dot] cebiric [at] inria [dot] fr
